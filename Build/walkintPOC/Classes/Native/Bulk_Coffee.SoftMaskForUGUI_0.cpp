﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// Coffee.UIExtensions.SoftMask
struct SoftMask_t1817791576;
// Coffee.UIExtensions.SoftMask[]
struct SoftMaskU5BU5D_t1621360457;
// Coffee.UIExtensions.SoftMaskable
struct SoftMaskable_t353105572;
// Coffee.UIExtensions.SoftMaskable[]
struct SoftMaskableU5BU5D_t1601685069;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>
struct List_1_t3289866318;
// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>[]
struct List_1U5BU5D_t521977531;
// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMaskable>
struct List_1_t1825180314;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t4072576034;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t496136383;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Predicate`1<Coffee.UIExtensions.SoftMask>
struct Predicate_1_t2643085700;
// System.Predicate`1<System.Object>
struct Predicate_1_t3905400288;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t190067161;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t3309123499;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3213117958;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t2206337031;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.Mask
struct Mask_t1803652131;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;

extern RuntimeClass* ColorU5BU5D_t941916413_il2cpp_TypeInfo_var;
extern RuntimeClass* CommandBuffer_t2206337031_il2cpp_TypeInfo_var;
extern RuntimeClass* Graphic_t1660335611_il2cpp_TypeInfo_var;
extern RuntimeClass* Graphics_t783367614_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32U5BU5D_t385246372_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1U5BU5D_t521977531_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1825180314_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t3289866318_il2cpp_TypeInfo_var;
extern RuntimeClass* MaterialPropertyBlock_t3213117958_il2cpp_TypeInfo_var;
extern RuntimeClass* Material_t340375123_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* Matrix4x4_t1817901843_il2cpp_TypeInfo_var;
extern RuntimeClass* Mesh_t3648964284_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* Predicate_1_t2643085700_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern RuntimeClass* RectTransformUtility_t1743242446_il2cpp_TypeInfo_var;
extern RuntimeClass* RectTransform_t3704657025_il2cpp_TypeInfo_var;
extern RuntimeClass* SoftMask_t1817791576_il2cpp_TypeInfo_var;
extern RuntimeClass* SoftMaskable_t353105572_il2cpp_TypeInfo_var;
extern RuntimeClass* StencilMaterial_t3850132571_il2cpp_TypeInfo_var;
extern RuntimeClass* Texture2D_t3840446185_il2cpp_TypeInfo_var;
extern RuntimeClass* WillRenderCanvases_t3309123499_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral120066430;
extern String_t* _stringLiteral1499807536;
extern String_t* _stringLiteral2160052589;
extern String_t* _stringLiteral2836376834;
extern String_t* _stringLiteral3184621405;
extern String_t* _stringLiteral383173083;
extern String_t* _stringLiteral489978276;
extern String_t* _stringLiteral873708475;
extern const RuntimeMethod* Component_GetComponent_TisGraphic_t1660335611_m1118939870_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisSoftMask_t1817791576_m4226683470_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponentsInChildren_TisSoftMask_t1817791576_m1497230955_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m153542923_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m660175762_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2347303577_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2471691335_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m589637811_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m923421119_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Contains_m2410279580_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m3631405139_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAll_m3895672821_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m1687504378_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m616773178_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m3675240295_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m731543265_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m120286467_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m2136910319_RuntimeMethod_var;
extern const RuntimeMethod* Predicate_1__ctor_m1967950527_RuntimeMethod_var;
extern const RuntimeMethod* Resources_Load_TisShader_t4151988712_m741163411_RuntimeMethod_var;
extern const RuntimeMethod* SoftMask_U3CSetParentU3Em__0_m2620602347_RuntimeMethod_var;
extern const RuntimeMethod* SoftMask_UpdateMaskTextures_m3160689611_RuntimeMethod_var;
extern const uint32_t SoftMask_GetDesamplingSize_m2102729222_MetadataUsageId;
extern const uint32_t SoftMask_GetModifiedMaterial_m1571737707_MetadataUsageId;
extern const uint32_t SoftMask_GetPixelValue_m3927220954_MetadataUsageId;
extern const uint32_t SoftMask_IsRaycastLocationValid_m4269246139_MetadataUsageId;
extern const uint32_t SoftMask_OnDisable_m259598144_MetadataUsageId;
extern const uint32_t SoftMask_OnEnable_m1417838140_MetadataUsageId;
extern const uint32_t SoftMask_OnTransformParentChanged_m2418922873_MetadataUsageId;
extern const uint32_t SoftMask_ReleaseObject_m16266906_MetadataUsageId;
extern const uint32_t SoftMask_ReleaseRT_m1409116420_MetadataUsageId;
extern const uint32_t SoftMask_SetParent_m1377869388_MetadataUsageId;
extern const uint32_t SoftMask_U3CSetParentU3Em__0_m2620602347_MetadataUsageId;
extern const uint32_t SoftMask_UpdateMaskTexture_m4144841572_MetadataUsageId;
extern const uint32_t SoftMask_UpdateMaskTextures_m3160689611_MetadataUsageId;
extern const uint32_t SoftMask__cctor_m1781826879_MetadataUsageId;
extern const uint32_t SoftMask__ctor_m3442446782_MetadataUsageId;
extern const uint32_t SoftMask_get_hasChanged_m3337097011_MetadataUsageId;
extern const uint32_t SoftMask_get_material_m4230810281_MetadataUsageId;
extern const uint32_t SoftMask_get_mesh_m903007999_MetadataUsageId;
extern const uint32_t SoftMask_get_softMaskBuffer_m514739908_MetadataUsageId;
extern const uint32_t SoftMask_set_hasChanged_m583714446_MetadataUsageId;
extern const uint32_t SoftMask_set_softness_m3479775679_MetadataUsageId;
extern const uint32_t SoftMaskable_GetModifiedMaterial_m1938042998_MetadataUsageId;
extern const uint32_t SoftMaskable_IsRaycastLocationValid_m3671775487_MetadataUsageId;
extern const uint32_t SoftMaskable_OnDisable_m3111229774_MetadataUsageId;
extern const uint32_t SoftMaskable_OnEnable_m2016091490_MetadataUsageId;
extern const uint32_t SoftMaskable_ReleaseMaterial_m86784683_MetadataUsageId;
extern const uint32_t SoftMaskable_SetMaskInteraction_m4089080_MetadataUsageId;
extern const uint32_t SoftMaskable__cctor_m3931341446_MetadataUsageId;
extern const uint32_t SoftMaskable_get_graphic_m2211451753_MetadataUsageId;

struct ByteU5BU5D_t4116647657;
struct List_1U5BU5D_t521977531;
struct Int32U5BU5D_t385246372;
struct ColorU5BU5D_t941916413;


#ifndef U3CMODULEU3E_T692745564_H
#define U3CMODULEU3E_T692745564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745564 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745564_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T3289866318_H
#define LIST_1_T3289866318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>
struct  List_1_t3289866318  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SoftMaskU5BU5D_t1621360457* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3289866318, ____items_1)); }
	inline SoftMaskU5BU5D_t1621360457* get__items_1() const { return ____items_1; }
	inline SoftMaskU5BU5D_t1621360457** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SoftMaskU5BU5D_t1621360457* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3289866318, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3289866318, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3289866318, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t3289866318_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SoftMaskU5BU5D_t1621360457* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3289866318_StaticFields, ____emptyArray_5)); }
	inline SoftMaskU5BU5D_t1621360457* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SoftMaskU5BU5D_t1621360457** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SoftMaskU5BU5D_t1621360457* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3289866318_H
#ifndef LIST_1_T1825180314_H
#define LIST_1_T1825180314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMaskable>
struct  List_1_t1825180314  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SoftMaskableU5BU5D_t1601685069* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1825180314, ____items_1)); }
	inline SoftMaskableU5BU5D_t1601685069* get__items_1() const { return ____items_1; }
	inline SoftMaskableU5BU5D_t1601685069** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SoftMaskableU5BU5D_t1601685069* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1825180314, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1825180314, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t1825180314, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t1825180314_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SoftMaskableU5BU5D_t1601685069* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t1825180314_StaticFields, ____emptyArray_5)); }
	inline SoftMaskableU5BU5D_t1601685069* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SoftMaskableU5BU5D_t1601685069** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SoftMaskableU5BU5D_t1601685069* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1825180314_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef ENUMERATOR_T884142899_H
#define ENUMERATOR_T884142899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<Coffee.UIExtensions.SoftMask>
struct  Enumerator_t884142899 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3289866318 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	SoftMask_t1817791576 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t884142899, ___list_0)); }
	inline List_1_t3289866318 * get_list_0() const { return ___list_0; }
	inline List_1_t3289866318 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3289866318 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t884142899, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t884142899, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t884142899, ___current_3)); }
	inline SoftMask_t1817791576 * get_current_3() const { return ___current_3; }
	inline SoftMask_t1817791576 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(SoftMask_t1817791576 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T884142899_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t257213610 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___list_0)); }
	inline List_1_t257213610 * get_list_0() const { return ___list_0; }
	inline List_1_t257213610 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t257213610 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef DESAMPLINGRATE_T2371977868_H
#define DESAMPLINGRATE_T2371977868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Coffee.UIExtensions.SoftMask/DesamplingRate
struct  DesamplingRate_t2371977868 
{
public:
	// System.Int32 Coffee.UIExtensions.SoftMask/DesamplingRate::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DesamplingRate_t2371977868, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESAMPLINGRATE_T2371977868_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef CUBEMAPFACE_T1358225318_H
#define CUBEMAPFACE_T1358225318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CubemapFace
struct  CubemapFace_t1358225318 
{
public:
	// System.Int32 UnityEngine.CubemapFace::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CubemapFace_t1358225318, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBEMAPFACE_T1358225318_H
#ifndef HIDEFLAGS_T4250555765_H
#define HIDEFLAGS_T4250555765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t4250555765 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HideFlags_t4250555765, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T4250555765_H
#ifndef MATERIALPROPERTYBLOCK_T3213117958_H
#define MATERIALPROPERTYBLOCK_T3213117958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MaterialPropertyBlock
struct  MaterialPropertyBlock_t3213117958  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.MaterialPropertyBlock::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(MaterialPropertyBlock_t3213117958, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALPROPERTYBLOCK_T3213117958_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RENDERMODE_T4077056833_H
#define RENDERMODE_T4077056833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderMode
struct  RenderMode_t4077056833 
{
public:
	// System.Int32 UnityEngine.RenderMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderMode_t4077056833, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERMODE_T4077056833_H
#ifndef RENDERTEXTUREFORMAT_T962350765_H
#define RENDERTEXTUREFORMAT_T962350765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureFormat
struct  RenderTextureFormat_t962350765 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t962350765, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFORMAT_T962350765_H
#ifndef RENDERTEXTUREREADWRITE_T1793271918_H
#define RENDERTEXTUREREADWRITE_T1793271918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureReadWrite
struct  RenderTextureReadWrite_t1793271918 
{
public:
	// System.Int32 UnityEngine.RenderTextureReadWrite::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureReadWrite_t1793271918, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREREADWRITE_T1793271918_H
#ifndef BUILTINRENDERTEXTURETYPE_T2399837169_H
#define BUILTINRENDERTEXTURETYPE_T2399837169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.BuiltinRenderTextureType
struct  BuiltinRenderTextureType_t2399837169 
{
public:
	// System.Int32 UnityEngine.Rendering.BuiltinRenderTextureType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BuiltinRenderTextureType_t2399837169, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINRENDERTEXTURETYPE_T2399837169_H
#ifndef COMMANDBUFFER_T2206337031_H
#define COMMANDBUFFER_T2206337031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CommandBuffer
struct  CommandBuffer_t2206337031  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Rendering.CommandBuffer::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CommandBuffer_t2206337031, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDBUFFER_T2206337031_H
#ifndef SPRITEMASKINTERACTION_T4184346524_H
#define SPRITEMASKINTERACTION_T4184346524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteMaskInteraction
struct  SpriteMaskInteraction_t4184346524 
{
public:
	// System.Int32 UnityEngine.SpriteMaskInteraction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpriteMaskInteraction_t4184346524, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEMASKINTERACTION_T4184346524_H
#ifndef TEXTUREFORMAT_T2701165832_H
#define TEXTUREFORMAT_T2701165832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2701165832 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_t2701165832, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2701165832_H
#ifndef VERTEXHELPER_T2453304189_H
#define VERTEXHELPER_T2453304189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t2453304189  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t899420910 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t4072576034 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t3628304265 * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t3628304265 * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t3628304265 * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t3628304265 * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t899420910 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t496136383 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t128053199 * ___m_Indices_8;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Positions_0)); }
	inline List_1_t899420910 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t899420910 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t899420910 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Colors_1)); }
	inline List_1_t4072576034 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t4072576034 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t4072576034 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv0S_2)); }
	inline List_1_t3628304265 * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t3628304265 ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t3628304265 * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv1S_3)); }
	inline List_1_t3628304265 * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t3628304265 ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t3628304265 * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv2S_4)); }
	inline List_1_t3628304265 * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t3628304265 ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t3628304265 * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv3S_5)); }
	inline List_1_t3628304265 * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t3628304265 ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t3628304265 * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Normals_6)); }
	inline List_1_t899420910 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t899420910 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t899420910 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Tangents_7)); }
	inline List_1_t496136383 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t496136383 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t496136383 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Indices_8)); }
	inline List_1_t128053199 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t128053199 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t128053199 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}
};

struct VertexHelper_t2453304189_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_t3319028937  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t3722313464  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t3319028937  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t3319028937 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t3319028937  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t3722313464  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t3722313464 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t3722313464  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T2453304189_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MATERIAL_T340375123_H
#define MATERIAL_T340375123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t340375123  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T340375123_H
#ifndef MESH_T3648964284_H
#define MESH_T3648964284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh
struct  Mesh_t3648964284  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_T3648964284_H
#ifndef RENDERTARGETIDENTIFIER_T2079184500_H
#define RENDERTARGETIDENTIFIER_T2079184500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.RenderTargetIdentifier
struct  RenderTargetIdentifier_t2079184500 
{
public:
	// UnityEngine.Rendering.BuiltinRenderTextureType UnityEngine.Rendering.RenderTargetIdentifier::m_Type
	int32_t ___m_Type_0;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_NameID
	int32_t ___m_NameID_1;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_InstanceID
	int32_t ___m_InstanceID_2;
	// System.IntPtr UnityEngine.Rendering.RenderTargetIdentifier::m_BufferPointer
	intptr_t ___m_BufferPointer_3;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_MipLevel
	int32_t ___m_MipLevel_4;
	// UnityEngine.CubemapFace UnityEngine.Rendering.RenderTargetIdentifier::m_CubeFace
	int32_t ___m_CubeFace_5;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_DepthSlice
	int32_t ___m_DepthSlice_6;

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_NameID_1() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_NameID_1)); }
	inline int32_t get_m_NameID_1() const { return ___m_NameID_1; }
	inline int32_t* get_address_of_m_NameID_1() { return &___m_NameID_1; }
	inline void set_m_NameID_1(int32_t value)
	{
		___m_NameID_1 = value;
	}

	inline static int32_t get_offset_of_m_InstanceID_2() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_InstanceID_2)); }
	inline int32_t get_m_InstanceID_2() const { return ___m_InstanceID_2; }
	inline int32_t* get_address_of_m_InstanceID_2() { return &___m_InstanceID_2; }
	inline void set_m_InstanceID_2(int32_t value)
	{
		___m_InstanceID_2 = value;
	}

	inline static int32_t get_offset_of_m_BufferPointer_3() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_BufferPointer_3)); }
	inline intptr_t get_m_BufferPointer_3() const { return ___m_BufferPointer_3; }
	inline intptr_t* get_address_of_m_BufferPointer_3() { return &___m_BufferPointer_3; }
	inline void set_m_BufferPointer_3(intptr_t value)
	{
		___m_BufferPointer_3 = value;
	}

	inline static int32_t get_offset_of_m_MipLevel_4() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_MipLevel_4)); }
	inline int32_t get_m_MipLevel_4() const { return ___m_MipLevel_4; }
	inline int32_t* get_address_of_m_MipLevel_4() { return &___m_MipLevel_4; }
	inline void set_m_MipLevel_4(int32_t value)
	{
		___m_MipLevel_4 = value;
	}

	inline static int32_t get_offset_of_m_CubeFace_5() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_CubeFace_5)); }
	inline int32_t get_m_CubeFace_5() const { return ___m_CubeFace_5; }
	inline int32_t* get_address_of_m_CubeFace_5() { return &___m_CubeFace_5; }
	inline void set_m_CubeFace_5(int32_t value)
	{
		___m_CubeFace_5 = value;
	}

	inline static int32_t get_offset_of_m_DepthSlice_6() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_DepthSlice_6)); }
	inline int32_t get_m_DepthSlice_6() const { return ___m_DepthSlice_6; }
	inline int32_t* get_address_of_m_DepthSlice_6() { return &___m_DepthSlice_6; }
	inline void set_m_DepthSlice_6(int32_t value)
	{
		___m_DepthSlice_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTARGETIDENTIFIER_T2079184500_H
#ifndef SHADER_T4151988712_H
#define SHADER_T4151988712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Shader
struct  Shader_t4151988712  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADER_T4151988712_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef PREDICATE_1_T2643085700_H
#define PREDICATE_1_T2643085700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<Coffee.UIExtensions.SoftMask>
struct  Predicate_1_t2643085700  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T2643085700_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef WILLRENDERCANVASES_T3309123499_H
#define WILLRENDERCANVASES_T3309123499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas/WillRenderCanvases
struct  WillRenderCanvases_t3309123499  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WILLRENDERCANVASES_T3309123499_H
#ifndef RENDERTEXTURE_T2108887433_H
#define RENDERTEXTURE_T2108887433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTexture
struct  RenderTexture_t2108887433  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURE_T2108887433_H
#ifndef TEXTURE2D_T3840446185_H
#define TEXTURE2D_T3840446185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3840446185  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3840446185_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef CAMERA_T4157153871_H
#define CAMERA_T4157153871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t4157153871  : public Behaviour_t1437897464
{
public:

public:
};

struct Camera_t4157153871_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t190067161 * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t190067161 * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t190067161 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t190067161 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t190067161 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t190067161 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_4), value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t190067161 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t190067161 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t190067161 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_5), value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t190067161 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t190067161 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t190067161 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4157153871_H
#ifndef CANVAS_T3310196443_H
#define CANVAS_T3310196443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas
struct  Canvas_t3310196443  : public Behaviour_t1437897464
{
public:

public:
};

struct Canvas_t3310196443_StaticFields
{
public:
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t3309123499 * ___willRenderCanvases_4;

public:
	inline static int32_t get_offset_of_willRenderCanvases_4() { return static_cast<int32_t>(offsetof(Canvas_t3310196443_StaticFields, ___willRenderCanvases_4)); }
	inline WillRenderCanvases_t3309123499 * get_willRenderCanvases_4() const { return ___willRenderCanvases_4; }
	inline WillRenderCanvases_t3309123499 ** get_address_of_willRenderCanvases_4() { return &___willRenderCanvases_4; }
	inline void set_willRenderCanvases_4(WillRenderCanvases_t3309123499 * value)
	{
		___willRenderCanvases_4 = value;
		Il2CppCodeGenWriteBarrier((&___willRenderCanvases_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVAS_T3310196443_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef RECTTRANSFORM_T3704657025_H
#define RECTTRANSFORM_T3704657025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t3704657025  : public Transform_t3600365921
{
public:

public:
};

struct RectTransform_t3704657025_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1258266594 * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t1258266594 * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t1258266594 * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T3704657025_H
#ifndef SOFTMASKABLE_T353105572_H
#define SOFTMASKABLE_T353105572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Coffee.UIExtensions.SoftMaskable
struct  SoftMaskable_t353105572  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Coffee.UIExtensions.SoftMaskable::m_Inverse
	bool ___m_Inverse_6;
	// System.Int32 Coffee.UIExtensions.SoftMaskable::m_MaskInteraction
	int32_t ___m_MaskInteraction_7;
	// System.Boolean Coffee.UIExtensions.SoftMaskable::m_UseStencil
	bool ___m_UseStencil_8;
	// UnityEngine.UI.Graphic Coffee.UIExtensions.SoftMaskable::_graphic
	Graphic_t1660335611 * ____graphic_9;
	// Coffee.UIExtensions.SoftMask Coffee.UIExtensions.SoftMaskable::_softMask
	SoftMask_t1817791576 * ____softMask_10;
	// UnityEngine.Material Coffee.UIExtensions.SoftMaskable::_maskMaterial
	Material_t340375123 * ____maskMaterial_11;

public:
	inline static int32_t get_offset_of_m_Inverse_6() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572, ___m_Inverse_6)); }
	inline bool get_m_Inverse_6() const { return ___m_Inverse_6; }
	inline bool* get_address_of_m_Inverse_6() { return &___m_Inverse_6; }
	inline void set_m_Inverse_6(bool value)
	{
		___m_Inverse_6 = value;
	}

	inline static int32_t get_offset_of_m_MaskInteraction_7() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572, ___m_MaskInteraction_7)); }
	inline int32_t get_m_MaskInteraction_7() const { return ___m_MaskInteraction_7; }
	inline int32_t* get_address_of_m_MaskInteraction_7() { return &___m_MaskInteraction_7; }
	inline void set_m_MaskInteraction_7(int32_t value)
	{
		___m_MaskInteraction_7 = value;
	}

	inline static int32_t get_offset_of_m_UseStencil_8() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572, ___m_UseStencil_8)); }
	inline bool get_m_UseStencil_8() const { return ___m_UseStencil_8; }
	inline bool* get_address_of_m_UseStencil_8() { return &___m_UseStencil_8; }
	inline void set_m_UseStencil_8(bool value)
	{
		___m_UseStencil_8 = value;
	}

	inline static int32_t get_offset_of__graphic_9() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572, ____graphic_9)); }
	inline Graphic_t1660335611 * get__graphic_9() const { return ____graphic_9; }
	inline Graphic_t1660335611 ** get_address_of__graphic_9() { return &____graphic_9; }
	inline void set__graphic_9(Graphic_t1660335611 * value)
	{
		____graphic_9 = value;
		Il2CppCodeGenWriteBarrier((&____graphic_9), value);
	}

	inline static int32_t get_offset_of__softMask_10() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572, ____softMask_10)); }
	inline SoftMask_t1817791576 * get__softMask_10() const { return ____softMask_10; }
	inline SoftMask_t1817791576 ** get_address_of__softMask_10() { return &____softMask_10; }
	inline void set__softMask_10(SoftMask_t1817791576 * value)
	{
		____softMask_10 = value;
		Il2CppCodeGenWriteBarrier((&____softMask_10), value);
	}

	inline static int32_t get_offset_of__maskMaterial_11() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572, ____maskMaterial_11)); }
	inline Material_t340375123 * get__maskMaterial_11() const { return ____maskMaterial_11; }
	inline Material_t340375123 ** get_address_of__maskMaterial_11() { return &____maskMaterial_11; }
	inline void set__maskMaterial_11(Material_t340375123 * value)
	{
		____maskMaterial_11 = value;
		Il2CppCodeGenWriteBarrier((&____maskMaterial_11), value);
	}
};

struct SoftMaskable_t353105572_StaticFields
{
public:
	// System.Int32 Coffee.UIExtensions.SoftMaskable::s_SoftMaskTexId
	int32_t ___s_SoftMaskTexId_12;
	// System.Int32 Coffee.UIExtensions.SoftMaskable::s_StencilCompId
	int32_t ___s_StencilCompId_13;
	// System.Int32 Coffee.UIExtensions.SoftMaskable::s_MaskInteractionId
	int32_t ___s_MaskInteractionId_14;
	// System.Int32 Coffee.UIExtensions.SoftMaskable::s_GameVPId
	int32_t ___s_GameVPId_15;
	// System.Int32 Coffee.UIExtensions.SoftMaskable::s_GameTVPId
	int32_t ___s_GameTVPId_16;
	// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMaskable> Coffee.UIExtensions.SoftMaskable::s_ActiveSoftMaskables
	List_1_t1825180314 * ___s_ActiveSoftMaskables_17;
	// System.Int32[] Coffee.UIExtensions.SoftMaskable::s_Interactions
	Int32U5BU5D_t385246372* ___s_Interactions_18;
	// UnityEngine.Material Coffee.UIExtensions.SoftMaskable::s_DefaultMaterial
	Material_t340375123 * ___s_DefaultMaterial_19;

public:
	inline static int32_t get_offset_of_s_SoftMaskTexId_12() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572_StaticFields, ___s_SoftMaskTexId_12)); }
	inline int32_t get_s_SoftMaskTexId_12() const { return ___s_SoftMaskTexId_12; }
	inline int32_t* get_address_of_s_SoftMaskTexId_12() { return &___s_SoftMaskTexId_12; }
	inline void set_s_SoftMaskTexId_12(int32_t value)
	{
		___s_SoftMaskTexId_12 = value;
	}

	inline static int32_t get_offset_of_s_StencilCompId_13() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572_StaticFields, ___s_StencilCompId_13)); }
	inline int32_t get_s_StencilCompId_13() const { return ___s_StencilCompId_13; }
	inline int32_t* get_address_of_s_StencilCompId_13() { return &___s_StencilCompId_13; }
	inline void set_s_StencilCompId_13(int32_t value)
	{
		___s_StencilCompId_13 = value;
	}

	inline static int32_t get_offset_of_s_MaskInteractionId_14() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572_StaticFields, ___s_MaskInteractionId_14)); }
	inline int32_t get_s_MaskInteractionId_14() const { return ___s_MaskInteractionId_14; }
	inline int32_t* get_address_of_s_MaskInteractionId_14() { return &___s_MaskInteractionId_14; }
	inline void set_s_MaskInteractionId_14(int32_t value)
	{
		___s_MaskInteractionId_14 = value;
	}

	inline static int32_t get_offset_of_s_GameVPId_15() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572_StaticFields, ___s_GameVPId_15)); }
	inline int32_t get_s_GameVPId_15() const { return ___s_GameVPId_15; }
	inline int32_t* get_address_of_s_GameVPId_15() { return &___s_GameVPId_15; }
	inline void set_s_GameVPId_15(int32_t value)
	{
		___s_GameVPId_15 = value;
	}

	inline static int32_t get_offset_of_s_GameTVPId_16() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572_StaticFields, ___s_GameTVPId_16)); }
	inline int32_t get_s_GameTVPId_16() const { return ___s_GameTVPId_16; }
	inline int32_t* get_address_of_s_GameTVPId_16() { return &___s_GameTVPId_16; }
	inline void set_s_GameTVPId_16(int32_t value)
	{
		___s_GameTVPId_16 = value;
	}

	inline static int32_t get_offset_of_s_ActiveSoftMaskables_17() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572_StaticFields, ___s_ActiveSoftMaskables_17)); }
	inline List_1_t1825180314 * get_s_ActiveSoftMaskables_17() const { return ___s_ActiveSoftMaskables_17; }
	inline List_1_t1825180314 ** get_address_of_s_ActiveSoftMaskables_17() { return &___s_ActiveSoftMaskables_17; }
	inline void set_s_ActiveSoftMaskables_17(List_1_t1825180314 * value)
	{
		___s_ActiveSoftMaskables_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_ActiveSoftMaskables_17), value);
	}

	inline static int32_t get_offset_of_s_Interactions_18() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572_StaticFields, ___s_Interactions_18)); }
	inline Int32U5BU5D_t385246372* get_s_Interactions_18() const { return ___s_Interactions_18; }
	inline Int32U5BU5D_t385246372** get_address_of_s_Interactions_18() { return &___s_Interactions_18; }
	inline void set_s_Interactions_18(Int32U5BU5D_t385246372* value)
	{
		___s_Interactions_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_Interactions_18), value);
	}

	inline static int32_t get_offset_of_s_DefaultMaterial_19() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572_StaticFields, ___s_DefaultMaterial_19)); }
	inline Material_t340375123 * get_s_DefaultMaterial_19() const { return ___s_DefaultMaterial_19; }
	inline Material_t340375123 ** get_address_of_s_DefaultMaterial_19() { return &___s_DefaultMaterial_19; }
	inline void set_s_DefaultMaterial_19(Material_t340375123 * value)
	{
		___s_DefaultMaterial_19 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultMaterial_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOFTMASKABLE_T353105572_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_6)); }
	inline Material_t340375123 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t340375123 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t340375123 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_7)); }
	inline Color_t2555686324  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t2555686324 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t2555686324  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_9)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_11)); }
	inline Canvas_t3310196443 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t3310196443 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t340375123 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t340375123 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t3648964284 * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t3648964284 * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef MASK_T1803652131_H
#define MASK_T1803652131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Mask
struct  Mask_t1803652131  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Mask::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_4;
	// System.Boolean UnityEngine.UI.Mask::m_ShowMaskGraphic
	bool ___m_ShowMaskGraphic_5;
	// UnityEngine.UI.Graphic UnityEngine.UI.Mask::m_Graphic
	Graphic_t1660335611 * ___m_Graphic_6;
	// UnityEngine.Material UnityEngine.UI.Mask::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_7;
	// UnityEngine.Material UnityEngine.UI.Mask::m_UnmaskMaterial
	Material_t340375123 * ___m_UnmaskMaterial_8;

public:
	inline static int32_t get_offset_of_m_RectTransform_4() { return static_cast<int32_t>(offsetof(Mask_t1803652131, ___m_RectTransform_4)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_4() const { return ___m_RectTransform_4; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_4() { return &___m_RectTransform_4; }
	inline void set_m_RectTransform_4(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_4), value);
	}

	inline static int32_t get_offset_of_m_ShowMaskGraphic_5() { return static_cast<int32_t>(offsetof(Mask_t1803652131, ___m_ShowMaskGraphic_5)); }
	inline bool get_m_ShowMaskGraphic_5() const { return ___m_ShowMaskGraphic_5; }
	inline bool* get_address_of_m_ShowMaskGraphic_5() { return &___m_ShowMaskGraphic_5; }
	inline void set_m_ShowMaskGraphic_5(bool value)
	{
		___m_ShowMaskGraphic_5 = value;
	}

	inline static int32_t get_offset_of_m_Graphic_6() { return static_cast<int32_t>(offsetof(Mask_t1803652131, ___m_Graphic_6)); }
	inline Graphic_t1660335611 * get_m_Graphic_6() const { return ___m_Graphic_6; }
	inline Graphic_t1660335611 ** get_address_of_m_Graphic_6() { return &___m_Graphic_6; }
	inline void set_m_Graphic_6(Graphic_t1660335611 * value)
	{
		___m_Graphic_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_6), value);
	}

	inline static int32_t get_offset_of_m_MaskMaterial_7() { return static_cast<int32_t>(offsetof(Mask_t1803652131, ___m_MaskMaterial_7)); }
	inline Material_t340375123 * get_m_MaskMaterial_7() const { return ___m_MaskMaterial_7; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_7() { return &___m_MaskMaterial_7; }
	inline void set_m_MaskMaterial_7(Material_t340375123 * value)
	{
		___m_MaskMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_7), value);
	}

	inline static int32_t get_offset_of_m_UnmaskMaterial_8() { return static_cast<int32_t>(offsetof(Mask_t1803652131, ___m_UnmaskMaterial_8)); }
	inline Material_t340375123 * get_m_UnmaskMaterial_8() const { return ___m_UnmaskMaterial_8; }
	inline Material_t340375123 ** get_address_of_m_UnmaskMaterial_8() { return &___m_UnmaskMaterial_8; }
	inline void set_m_UnmaskMaterial_8(Material_t340375123 * value)
	{
		___m_UnmaskMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_UnmaskMaterial_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASK_T1803652131_H
#ifndef SOFTMASK_T1817791576_H
#define SOFTMASK_T1817791576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Coffee.UIExtensions.SoftMask
struct  SoftMask_t1817791576  : public Mask_t1803652131
{
public:
	// Coffee.UIExtensions.SoftMask/DesamplingRate Coffee.UIExtensions.SoftMask::m_DesamplingRate
	int32_t ___m_DesamplingRate_11;
	// System.Single Coffee.UIExtensions.SoftMask::m_Softness
	float ___m_Softness_12;
	// System.Boolean Coffee.UIExtensions.SoftMask::m_IgnoreParent
	bool ___m_IgnoreParent_13;
	// System.Boolean Coffee.UIExtensions.SoftMask::m_PartOfParent
	bool ___m_PartOfParent_14;
	// UnityEngine.MaterialPropertyBlock Coffee.UIExtensions.SoftMask::_mpb
	MaterialPropertyBlock_t3213117958 * ____mpb_23;
	// UnityEngine.Rendering.CommandBuffer Coffee.UIExtensions.SoftMask::_cb
	CommandBuffer_t2206337031 * ____cb_24;
	// UnityEngine.Material Coffee.UIExtensions.SoftMask::_material
	Material_t340375123 * ____material_25;
	// UnityEngine.RenderTexture Coffee.UIExtensions.SoftMask::_softMaskBuffer
	RenderTexture_t2108887433 * ____softMaskBuffer_26;
	// System.Int32 Coffee.UIExtensions.SoftMask::_stencilDepth
	int32_t ____stencilDepth_27;
	// UnityEngine.Mesh Coffee.UIExtensions.SoftMask::_mesh
	Mesh_t3648964284 * ____mesh_28;
	// Coffee.UIExtensions.SoftMask Coffee.UIExtensions.SoftMask::_parent
	SoftMask_t1817791576 * ____parent_29;
	// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask> Coffee.UIExtensions.SoftMask::_children
	List_1_t3289866318 * ____children_30;
	// System.Boolean Coffee.UIExtensions.SoftMask::_hasChanged
	bool ____hasChanged_31;
	// System.Boolean Coffee.UIExtensions.SoftMask::_hasStencilStateChanged
	bool ____hasStencilStateChanged_32;

public:
	inline static int32_t get_offset_of_m_DesamplingRate_11() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ___m_DesamplingRate_11)); }
	inline int32_t get_m_DesamplingRate_11() const { return ___m_DesamplingRate_11; }
	inline int32_t* get_address_of_m_DesamplingRate_11() { return &___m_DesamplingRate_11; }
	inline void set_m_DesamplingRate_11(int32_t value)
	{
		___m_DesamplingRate_11 = value;
	}

	inline static int32_t get_offset_of_m_Softness_12() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ___m_Softness_12)); }
	inline float get_m_Softness_12() const { return ___m_Softness_12; }
	inline float* get_address_of_m_Softness_12() { return &___m_Softness_12; }
	inline void set_m_Softness_12(float value)
	{
		___m_Softness_12 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreParent_13() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ___m_IgnoreParent_13)); }
	inline bool get_m_IgnoreParent_13() const { return ___m_IgnoreParent_13; }
	inline bool* get_address_of_m_IgnoreParent_13() { return &___m_IgnoreParent_13; }
	inline void set_m_IgnoreParent_13(bool value)
	{
		___m_IgnoreParent_13 = value;
	}

	inline static int32_t get_offset_of_m_PartOfParent_14() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ___m_PartOfParent_14)); }
	inline bool get_m_PartOfParent_14() const { return ___m_PartOfParent_14; }
	inline bool* get_address_of_m_PartOfParent_14() { return &___m_PartOfParent_14; }
	inline void set_m_PartOfParent_14(bool value)
	{
		___m_PartOfParent_14 = value;
	}

	inline static int32_t get_offset_of__mpb_23() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____mpb_23)); }
	inline MaterialPropertyBlock_t3213117958 * get__mpb_23() const { return ____mpb_23; }
	inline MaterialPropertyBlock_t3213117958 ** get_address_of__mpb_23() { return &____mpb_23; }
	inline void set__mpb_23(MaterialPropertyBlock_t3213117958 * value)
	{
		____mpb_23 = value;
		Il2CppCodeGenWriteBarrier((&____mpb_23), value);
	}

	inline static int32_t get_offset_of__cb_24() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____cb_24)); }
	inline CommandBuffer_t2206337031 * get__cb_24() const { return ____cb_24; }
	inline CommandBuffer_t2206337031 ** get_address_of__cb_24() { return &____cb_24; }
	inline void set__cb_24(CommandBuffer_t2206337031 * value)
	{
		____cb_24 = value;
		Il2CppCodeGenWriteBarrier((&____cb_24), value);
	}

	inline static int32_t get_offset_of__material_25() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____material_25)); }
	inline Material_t340375123 * get__material_25() const { return ____material_25; }
	inline Material_t340375123 ** get_address_of__material_25() { return &____material_25; }
	inline void set__material_25(Material_t340375123 * value)
	{
		____material_25 = value;
		Il2CppCodeGenWriteBarrier((&____material_25), value);
	}

	inline static int32_t get_offset_of__softMaskBuffer_26() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____softMaskBuffer_26)); }
	inline RenderTexture_t2108887433 * get__softMaskBuffer_26() const { return ____softMaskBuffer_26; }
	inline RenderTexture_t2108887433 ** get_address_of__softMaskBuffer_26() { return &____softMaskBuffer_26; }
	inline void set__softMaskBuffer_26(RenderTexture_t2108887433 * value)
	{
		____softMaskBuffer_26 = value;
		Il2CppCodeGenWriteBarrier((&____softMaskBuffer_26), value);
	}

	inline static int32_t get_offset_of__stencilDepth_27() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____stencilDepth_27)); }
	inline int32_t get__stencilDepth_27() const { return ____stencilDepth_27; }
	inline int32_t* get_address_of__stencilDepth_27() { return &____stencilDepth_27; }
	inline void set__stencilDepth_27(int32_t value)
	{
		____stencilDepth_27 = value;
	}

	inline static int32_t get_offset_of__mesh_28() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____mesh_28)); }
	inline Mesh_t3648964284 * get__mesh_28() const { return ____mesh_28; }
	inline Mesh_t3648964284 ** get_address_of__mesh_28() { return &____mesh_28; }
	inline void set__mesh_28(Mesh_t3648964284 * value)
	{
		____mesh_28 = value;
		Il2CppCodeGenWriteBarrier((&____mesh_28), value);
	}

	inline static int32_t get_offset_of__parent_29() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____parent_29)); }
	inline SoftMask_t1817791576 * get__parent_29() const { return ____parent_29; }
	inline SoftMask_t1817791576 ** get_address_of__parent_29() { return &____parent_29; }
	inline void set__parent_29(SoftMask_t1817791576 * value)
	{
		____parent_29 = value;
		Il2CppCodeGenWriteBarrier((&____parent_29), value);
	}

	inline static int32_t get_offset_of__children_30() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____children_30)); }
	inline List_1_t3289866318 * get__children_30() const { return ____children_30; }
	inline List_1_t3289866318 ** get_address_of__children_30() { return &____children_30; }
	inline void set__children_30(List_1_t3289866318 * value)
	{
		____children_30 = value;
		Il2CppCodeGenWriteBarrier((&____children_30), value);
	}

	inline static int32_t get_offset_of__hasChanged_31() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____hasChanged_31)); }
	inline bool get__hasChanged_31() const { return ____hasChanged_31; }
	inline bool* get_address_of__hasChanged_31() { return &____hasChanged_31; }
	inline void set__hasChanged_31(bool value)
	{
		____hasChanged_31 = value;
	}

	inline static int32_t get_offset_of__hasStencilStateChanged_32() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____hasStencilStateChanged_32)); }
	inline bool get__hasStencilStateChanged_32() const { return ____hasStencilStateChanged_32; }
	inline bool* get_address_of__hasStencilStateChanged_32() { return &____hasStencilStateChanged_32; }
	inline void set__hasStencilStateChanged_32(bool value)
	{
		____hasStencilStateChanged_32 = value;
	}
};

struct SoftMask_t1817791576_StaticFields
{
public:
	// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>[] Coffee.UIExtensions.SoftMask::s_TmpSoftMasks
	List_1U5BU5D_t521977531* ___s_TmpSoftMasks_9;
	// UnityEngine.Color[] Coffee.UIExtensions.SoftMask::s_ClearColors
	ColorU5BU5D_t941916413* ___s_ClearColors_10;
	// UnityEngine.Shader Coffee.UIExtensions.SoftMask::s_SoftMaskShader
	Shader_t4151988712 * ___s_SoftMaskShader_15;
	// UnityEngine.Texture2D Coffee.UIExtensions.SoftMask::s_ReadTexture
	Texture2D_t3840446185 * ___s_ReadTexture_16;
	// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask> Coffee.UIExtensions.SoftMask::s_ActiveSoftMasks
	List_1_t3289866318 * ___s_ActiveSoftMasks_17;
	// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask> Coffee.UIExtensions.SoftMask::s_TempRelatables
	List_1_t3289866318 * ___s_TempRelatables_18;
	// System.Int32 Coffee.UIExtensions.SoftMask::s_StencilCompId
	int32_t ___s_StencilCompId_19;
	// System.Int32 Coffee.UIExtensions.SoftMask::s_ColorMaskId
	int32_t ___s_ColorMaskId_20;
	// System.Int32 Coffee.UIExtensions.SoftMask::s_MainTexId
	int32_t ___s_MainTexId_21;
	// System.Int32 Coffee.UIExtensions.SoftMask::s_SoftnessId
	int32_t ___s_SoftnessId_22;
	// UnityEngine.Canvas/WillRenderCanvases Coffee.UIExtensions.SoftMask::<>f__mg$cache0
	WillRenderCanvases_t3309123499 * ___U3CU3Ef__mgU24cache0_33;
	// UnityEngine.Canvas/WillRenderCanvases Coffee.UIExtensions.SoftMask::<>f__mg$cache1
	WillRenderCanvases_t3309123499 * ___U3CU3Ef__mgU24cache1_34;
	// System.Predicate`1<Coffee.UIExtensions.SoftMask> Coffee.UIExtensions.SoftMask::<>f__am$cache0
	Predicate_1_t2643085700 * ___U3CU3Ef__amU24cache0_35;

public:
	inline static int32_t get_offset_of_s_TmpSoftMasks_9() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_TmpSoftMasks_9)); }
	inline List_1U5BU5D_t521977531* get_s_TmpSoftMasks_9() const { return ___s_TmpSoftMasks_9; }
	inline List_1U5BU5D_t521977531** get_address_of_s_TmpSoftMasks_9() { return &___s_TmpSoftMasks_9; }
	inline void set_s_TmpSoftMasks_9(List_1U5BU5D_t521977531* value)
	{
		___s_TmpSoftMasks_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_TmpSoftMasks_9), value);
	}

	inline static int32_t get_offset_of_s_ClearColors_10() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_ClearColors_10)); }
	inline ColorU5BU5D_t941916413* get_s_ClearColors_10() const { return ___s_ClearColors_10; }
	inline ColorU5BU5D_t941916413** get_address_of_s_ClearColors_10() { return &___s_ClearColors_10; }
	inline void set_s_ClearColors_10(ColorU5BU5D_t941916413* value)
	{
		___s_ClearColors_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_ClearColors_10), value);
	}

	inline static int32_t get_offset_of_s_SoftMaskShader_15() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_SoftMaskShader_15)); }
	inline Shader_t4151988712 * get_s_SoftMaskShader_15() const { return ___s_SoftMaskShader_15; }
	inline Shader_t4151988712 ** get_address_of_s_SoftMaskShader_15() { return &___s_SoftMaskShader_15; }
	inline void set_s_SoftMaskShader_15(Shader_t4151988712 * value)
	{
		___s_SoftMaskShader_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_SoftMaskShader_15), value);
	}

	inline static int32_t get_offset_of_s_ReadTexture_16() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_ReadTexture_16)); }
	inline Texture2D_t3840446185 * get_s_ReadTexture_16() const { return ___s_ReadTexture_16; }
	inline Texture2D_t3840446185 ** get_address_of_s_ReadTexture_16() { return &___s_ReadTexture_16; }
	inline void set_s_ReadTexture_16(Texture2D_t3840446185 * value)
	{
		___s_ReadTexture_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReadTexture_16), value);
	}

	inline static int32_t get_offset_of_s_ActiveSoftMasks_17() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_ActiveSoftMasks_17)); }
	inline List_1_t3289866318 * get_s_ActiveSoftMasks_17() const { return ___s_ActiveSoftMasks_17; }
	inline List_1_t3289866318 ** get_address_of_s_ActiveSoftMasks_17() { return &___s_ActiveSoftMasks_17; }
	inline void set_s_ActiveSoftMasks_17(List_1_t3289866318 * value)
	{
		___s_ActiveSoftMasks_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_ActiveSoftMasks_17), value);
	}

	inline static int32_t get_offset_of_s_TempRelatables_18() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_TempRelatables_18)); }
	inline List_1_t3289866318 * get_s_TempRelatables_18() const { return ___s_TempRelatables_18; }
	inline List_1_t3289866318 ** get_address_of_s_TempRelatables_18() { return &___s_TempRelatables_18; }
	inline void set_s_TempRelatables_18(List_1_t3289866318 * value)
	{
		___s_TempRelatables_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_TempRelatables_18), value);
	}

	inline static int32_t get_offset_of_s_StencilCompId_19() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_StencilCompId_19)); }
	inline int32_t get_s_StencilCompId_19() const { return ___s_StencilCompId_19; }
	inline int32_t* get_address_of_s_StencilCompId_19() { return &___s_StencilCompId_19; }
	inline void set_s_StencilCompId_19(int32_t value)
	{
		___s_StencilCompId_19 = value;
	}

	inline static int32_t get_offset_of_s_ColorMaskId_20() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_ColorMaskId_20)); }
	inline int32_t get_s_ColorMaskId_20() const { return ___s_ColorMaskId_20; }
	inline int32_t* get_address_of_s_ColorMaskId_20() { return &___s_ColorMaskId_20; }
	inline void set_s_ColorMaskId_20(int32_t value)
	{
		___s_ColorMaskId_20 = value;
	}

	inline static int32_t get_offset_of_s_MainTexId_21() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_MainTexId_21)); }
	inline int32_t get_s_MainTexId_21() const { return ___s_MainTexId_21; }
	inline int32_t* get_address_of_s_MainTexId_21() { return &___s_MainTexId_21; }
	inline void set_s_MainTexId_21(int32_t value)
	{
		___s_MainTexId_21 = value;
	}

	inline static int32_t get_offset_of_s_SoftnessId_22() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_SoftnessId_22)); }
	inline int32_t get_s_SoftnessId_22() const { return ___s_SoftnessId_22; }
	inline int32_t* get_address_of_s_SoftnessId_22() { return &___s_SoftnessId_22; }
	inline void set_s_SoftnessId_22(int32_t value)
	{
		___s_SoftnessId_22 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_33() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___U3CU3Ef__mgU24cache0_33)); }
	inline WillRenderCanvases_t3309123499 * get_U3CU3Ef__mgU24cache0_33() const { return ___U3CU3Ef__mgU24cache0_33; }
	inline WillRenderCanvases_t3309123499 ** get_address_of_U3CU3Ef__mgU24cache0_33() { return &___U3CU3Ef__mgU24cache0_33; }
	inline void set_U3CU3Ef__mgU24cache0_33(WillRenderCanvases_t3309123499 * value)
	{
		___U3CU3Ef__mgU24cache0_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_34() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___U3CU3Ef__mgU24cache1_34)); }
	inline WillRenderCanvases_t3309123499 * get_U3CU3Ef__mgU24cache1_34() const { return ___U3CU3Ef__mgU24cache1_34; }
	inline WillRenderCanvases_t3309123499 ** get_address_of_U3CU3Ef__mgU24cache1_34() { return &___U3CU3Ef__mgU24cache1_34; }
	inline void set_U3CU3Ef__mgU24cache1_34(WillRenderCanvases_t3309123499 * value)
	{
		___U3CU3Ef__mgU24cache1_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_35() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___U3CU3Ef__amU24cache0_35)); }
	inline Predicate_1_t2643085700 * get_U3CU3Ef__amU24cache0_35() const { return ___U3CU3Ef__amU24cache0_35; }
	inline Predicate_1_t2643085700 ** get_address_of_U3CU3Ef__amU24cache0_35() { return &___U3CU3Ef__amU24cache0_35; }
	inline void set_U3CU3Ef__amU24cache0_35(Predicate_1_t2643085700 * value)
	{
		___U3CU3Ef__amU24cache0_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOFTMASK_T1817791576_H
// System.Int32[]
struct Int32U5BU5D_t385246372  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>[]
struct List_1U5BU5D_t521977531  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) List_1_t3289866318 * m_Items[1];

public:
	inline List_1_t3289866318 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline List_1_t3289866318 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, List_1_t3289866318 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline List_1_t3289866318 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline List_1_t3289866318 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, List_1_t3289866318 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_t2555686324  m_Items[1];

public:
	inline Color_t2555686324  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t2555686324  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean,System.Collections.Generic.List`1<!!0>)
extern "C" IL2CPP_METHOD_ATTR void Component_GetComponentsInChildren_TisRuntimeObject_m3870661263_gshared (Component_t1923634451 * __this, bool p0, List_1_t257213610 * p1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_m1328026504_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" IL2CPP_METHOD_ATTR void List_1_Clear_m3697625829_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C" IL2CPP_METHOD_ATTR bool List_1_Remove_m2390619627_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Resources_Load_TisRuntimeObject_m1502289511_gshared (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_t2146457487  List_1_GetEnumerator_m816315209_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m337713592_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
extern "C" IL2CPP_METHOD_ATTR bool List_1_Contains_m784383322_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Predicate_1__ctor_m327447107_gshared (Predicate_1_t3905400288 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<!0>)
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_RemoveAll_m2696662810_gshared (List_1_t257213610 * __this, Predicate_1_t3905400288 * p0, const RuntimeMethod* method);

// System.Void System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::.ctor()
inline void List_1__ctor_m3675240295 (List_1_t3289866318 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3289866318 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void UnityEngine.UI.Mask::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Mask__ctor_m1269055150 (Mask_t1803652131 * __this, const RuntimeMethod* method);
// System.Void Coffee.UIExtensions.SoftMask::set_hasChanged(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void SoftMask_set_hasChanged_m583714446 (SoftMask_t1817791576 * __this, bool ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Clamp01_m56433566 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Implicit_m3574996620 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// System.Void Coffee.UIExtensions.SoftMask::ReleaseRT(UnityEngine.RenderTexture&)
extern "C" IL2CPP_METHOD_ATTR void SoftMask_ReleaseRT_m1409116420 (SoftMask_t1817791576 * __this, RenderTexture_t2108887433 ** ___tmpRT0, const RuntimeMethod* method);
// UnityEngine.RenderTexture Coffee.UIExtensions.SoftMask::get_softMaskBuffer()
extern "C" IL2CPP_METHOD_ATTR RenderTexture_t2108887433 * SoftMask_get_softMaskBuffer_m514739908 (SoftMask_t1817791576 * __this, const RuntimeMethod* method);
// System.Void Coffee.UIExtensions.SoftMask::GetDesamplingSize(Coffee.UIExtensions.SoftMask/DesamplingRate,System.Int32&,System.Int32&)
extern "C" IL2CPP_METHOD_ATTR void SoftMask_GetDesamplingSize_m2102729222 (SoftMask_t1817791576 * __this, int32_t ___rate0, int32_t* ___w1, int32_t* ___h2, const RuntimeMethod* method);
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
extern "C" IL2CPP_METHOD_ATTR RenderTexture_t2108887433 * RenderTexture_GetTemporary_m1363375227 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
// System.Boolean Coffee.UIExtensions.SoftMask::get_hasChanged()
extern "C" IL2CPP_METHOD_ATTR bool SoftMask_get_hasChanged_m3337097011 (SoftMask_t1817791576 * __this, const RuntimeMethod* method);
// UnityEngine.Material UnityEngine.UI.Mask::GetModifiedMaterial(UnityEngine.Material)
extern "C" IL2CPP_METHOD_ATTR Material_t340375123 * Mask_GetModifiedMaterial_m2692272647 (Mask_t1803652131 * __this, Material_t340375123 * p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetInt(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Material_SetInt_m475299667 (Material_t340375123 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
extern "C" IL2CPP_METHOD_ATTR bool Behaviour_get_isActiveAndEnabled_m3143666263 (Behaviour_t1437897464 * __this, const RuntimeMethod* method);
// UnityEngine.Mesh Coffee.UIExtensions.SoftMask::get_mesh()
extern "C" IL2CPP_METHOD_ATTR Mesh_t3648964284 * SoftMask_get_mesh_m903007999 (SoftMask_t1817791576 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.VertexHelper::FillMesh(UnityEngine.Mesh)
extern "C" IL2CPP_METHOD_ATTR void VertexHelper_FillMesh_m1654068917 (VertexHelper_t2453304189 * __this, Mesh_t3648964284 * p0, const RuntimeMethod* method);
// UnityEngine.UI.Graphic UnityEngine.UI.Mask::get_graphic()
extern "C" IL2CPP_METHOD_ATTR Graphic_t1660335611 * Mask_get_graphic_m2572620787 (Mask_t1803652131 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_width()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_width_m345039817 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_height()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_height_m1623532518 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Single Coffee.UIExtensions.SoftMask::GetPixelValue(System.Int32,System.Int32,System.Int32[])
extern "C" IL2CPP_METHOD_ATTR float SoftMask_GetPixelValue_m3927220954 (SoftMask_t1817791576 * __this, int32_t ___x0, int32_t ___y1, Int32U5BU5D_t385246372* ___interactions2, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::get_Count()
inline int32_t List_1_get_Count_m120286467 (List_1_t3289866318 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t3289866318 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method);
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void WillRenderCanvases__ctor_m147079397 (WillRenderCanvases_t3309123499 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Canvas::add_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern "C" IL2CPP_METHOD_ATTR void Canvas_add_willRenderCanvases_m1234571137 (RuntimeObject * __this /* static, unused */, WillRenderCanvases_t3309123499 * p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C" IL2CPP_METHOD_ATTR int32_t Shader_PropertyToID_m1030499873 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::Add(!0)
inline void List_1_Add_m2471691335 (List_1_t3289866318 * __this, SoftMask_t1817791576 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3289866318 *, SoftMask_t1817791576 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Void UnityEngine.Component::GetComponentsInChildren<Coffee.UIExtensions.SoftMask>(System.Boolean,System.Collections.Generic.List`1<!!0>)
inline void Component_GetComponentsInChildren_TisSoftMask_t1817791576_m1497230955 (Component_t1923634451 * __this, bool p0, List_1_t3289866318 * p1, const RuntimeMethod* method)
{
	((  void (*) (Component_t1923634451 *, bool, List_1_t3289866318 *, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m3870661263_gshared)(__this, p0, p1, method);
}
// !0 System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::get_Item(System.Int32)
inline SoftMask_t1817791576 * List_1_get_Item_m2136910319 (List_1_t3289866318 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  SoftMask_t1817791576 * (*) (List_1_t3289866318 *, int32_t, const RuntimeMethod*))List_1_get_Item_m1328026504_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::Clear()
inline void List_1_Clear_m923421119 (List_1_t3289866318 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3289866318 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method);
}
// System.Void UnityEngine.MaterialPropertyBlock::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MaterialPropertyBlock__ctor_m3898279695 (MaterialPropertyBlock_t3213117958 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.CommandBuffer::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CommandBuffer__ctor_m3028411456 (CommandBuffer_t2206337031 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Mask::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void Mask_OnEnable_m2186824439 (Mask_t1803652131 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::Remove(!0)
inline bool List_1_Remove_m1687504378 (List_1_t3289866318 * __this, SoftMask_t1817791576 * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t3289866318 *, SoftMask_t1817791576 *, const RuntimeMethod*))List_1_Remove_m2390619627_gshared)(__this, p0, method);
}
// System.Void UnityEngine.Canvas::remove_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern "C" IL2CPP_METHOD_ATTR void Canvas_remove_willRenderCanvases_m3663577790 (RuntimeObject * __this /* static, unused */, WillRenderCanvases_t3309123499 * p0, const RuntimeMethod* method);
// System.Void Coffee.UIExtensions.SoftMask::SetParent(Coffee.UIExtensions.SoftMask)
extern "C" IL2CPP_METHOD_ATTR void SoftMask_SetParent_m1377869388 (SoftMask_t1817791576 * __this, SoftMask_t1817791576 * ___newParent0, const RuntimeMethod* method);
// System.Void UnityEngine.MaterialPropertyBlock::Clear()
extern "C" IL2CPP_METHOD_ATTR void MaterialPropertyBlock_Clear_m1909512031 (MaterialPropertyBlock_t3213117958 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.CommandBuffer::Release()
extern "C" IL2CPP_METHOD_ATTR void CommandBuffer_Release_m529480289 (CommandBuffer_t2206337031 * __this, const RuntimeMethod* method);
// System.Void Coffee.UIExtensions.SoftMask::ReleaseObject(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void SoftMask_ReleaseObject_m16266906 (SoftMask_t1817791576 * __this, Object_t631007953 * ___obj0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Mask::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void Mask_OnDisable_m1270951829 (Mask_t1803652131 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Transform_get_parent_m835071599 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Coffee.UIExtensions.SoftMask>()
inline SoftMask_t1817791576 * Component_GetComponent_TisSoftMask_t1817791576_m4226683470 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  SoftMask_t1817791576 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C" IL2CPP_METHOD_ATTR bool Behaviour_get_enabled_m753527255 (Behaviour_t1437897464 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<UnityEngine.Shader>(System.String)
inline Shader_t4151988712 * Resources_Load_TisShader_t4151988712_m741163411 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method)
{
	return ((  Shader_t4151988712 * (*) (RuntimeObject * /* static, unused */, String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_m1502289511_gshared)(__this /* static, unused */, p0, method);
}
// System.Void UnityEngine.Material::.ctor(UnityEngine.Shader)
extern "C" IL2CPP_METHOD_ATTR void Material__ctor_m1662457592 (Material_t340375123 * __this, Shader_t4151988712 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C" IL2CPP_METHOD_ATTR void Object_set_hideFlags_m1648752846 (Object_t631007953 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Mesh__ctor_m2533762929 (Mesh_t3648964284 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::GetEnumerator()
inline Enumerator_t884142899  List_1_GetEnumerator_m3631405139 (List_1_t3289866318 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t884142899  (*) (List_1_t3289866318 *, const RuntimeMethod*))List_1_GetEnumerator_m816315209_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<Coffee.UIExtensions.SoftMask>::get_Current()
inline SoftMask_t1817791576 * Enumerator_get_Current_m2347303577 (Enumerator_t884142899 * __this, const RuntimeMethod* method)
{
	return ((  SoftMask_t1817791576 * (*) (Enumerator_t884142899 *, const RuntimeMethod*))Enumerator_get_Current_m337713592_gshared)(__this, method);
}
// UnityEngine.Canvas UnityEngine.UI.Graphic::get_canvas()
extern "C" IL2CPP_METHOD_ATTR Canvas_t3310196443 * Graphic_get_canvas_m3320066409 (Graphic_t1660335611 * __this, const RuntimeMethod* method);
// UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
extern "C" IL2CPP_METHOD_ATTR int32_t Canvas_get_renderMode_m841659411 (Canvas_t3310196443 * __this, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
extern "C" IL2CPP_METHOD_ATTR Camera_t4157153871 * Canvas_get_worldCamera_m3516267897 (Canvas_t3310196443 * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_projectionMatrix()
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t1817901843  Camera_get_projectionMatrix_m667780853 (Camera_t4157153871 * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_worldToCameraMatrix()
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t1817901843  Camera_get_worldToCameraMatrix_m22661425 (Camera_t4157153871 * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t1817901843  Matrix4x4_op_Multiply_m1876492807 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  p0, Matrix4x4_t1817901843  p1, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_previousViewProjectionMatrix()
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t1817901843  Camera_get_previousViewProjectionMatrix_m3358791387 (Camera_t4157153871 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Matrix4x4::op_Inequality(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR bool Matrix4x4_op_Inequality_m334399549 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  p0, Matrix4x4_t1817901843  p1, const RuntimeMethod* method);
// UnityEngine.RectTransform UnityEngine.UI.Mask::get_rectTransform()
extern "C" IL2CPP_METHOD_ATTR RectTransform_t3704657025 * Mask_get_rectTransform_m440704215 (Mask_t1803652131 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Transform::get_hasChanged()
extern "C" IL2CPP_METHOD_ATTR bool Transform_get_hasChanged_m186929804 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_hasChanged(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_hasChanged_m4213723989 (Transform_t3600365921 * __this, bool p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<Coffee.UIExtensions.SoftMask>::MoveNext()
inline bool Enumerator_MoveNext_m660175762 (Enumerator_t884142899 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t884142899 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Coffee.UIExtensions.SoftMask>::Dispose()
inline void Enumerator_Dispose_m153542923 (Enumerator_t884142899 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t884142899 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method);
}
// System.Void Coffee.UIExtensions.SoftMask::UpdateMaskTexture()
extern "C" IL2CPP_METHOD_ATTR void SoftMask_UpdateMaskTexture_m4144841572 (SoftMask_t1817791576 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.MaskUtilities::NotifyStencilStateChanged(UnityEngine.Component)
extern "C" IL2CPP_METHOD_ATTR void MaskUtilities_NotifyStencilStateChanged_m3969722420 (RuntimeObject * __this /* static, unused */, Component_t1923634451 * p0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.UI.MaskUtilities::FindRootSortOverrideCanvas(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * MaskUtilities_FindRootSortOverrideCanvas_m1053047732 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.UI.MaskUtilities::GetStencilDepth(UnityEngine.Transform,UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR int32_t MaskUtilities_GetStencilDepth_m402474137 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * p0, Transform_t3600365921 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.CommandBuffer::Clear()
extern "C" IL2CPP_METHOD_ATTR void CommandBuffer_Clear_m3260707516 (CommandBuffer_t2206337031 * __this, const RuntimeMethod* method);
// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.RenderTargetIdentifier::op_Implicit(UnityEngine.Texture)
extern "C" IL2CPP_METHOD_ATTR RenderTargetIdentifier_t2079184500  RenderTargetIdentifier_op_Implicit_m3327331520 (RuntimeObject * __this /* static, unused */, Texture_t3661962703 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.CommandBuffer::SetRenderTarget(UnityEngine.Rendering.RenderTargetIdentifier)
extern "C" IL2CPP_METHOD_ATTR void CommandBuffer_SetRenderTarget_m2373313166 (CommandBuffer_t2206337031 * __this, RenderTargetIdentifier_t2079184500  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.CommandBuffer::ClearRenderTarget(System.Boolean,System.Boolean,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void CommandBuffer_ClearRenderTarget_m1436931375 (CommandBuffer_t2206337031 * __this, bool p0, bool p1, Color_t2555686324  p2, const RuntimeMethod* method);
// UnityEngine.Canvas UnityEngine.Canvas::get_rootCanvas()
extern "C" IL2CPP_METHOD_ATTR Canvas_t3310196443 * Canvas_get_rootCanvas_m3165691493 (Canvas_t3310196443 * __this, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C" IL2CPP_METHOD_ATTR Camera_t4157153871 * Camera_get_main_m3643453163 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.GL::GetGPUProjectionMatrix(UnityEngine.Matrix4x4,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t1817901843  GL_GetGPUProjectionMatrix_m628855021 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  p0, bool p1, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.CommandBuffer::SetViewProjectionMatrices(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR void CommandBuffer_SetViewProjectionMatrices_m1816539966 (CommandBuffer_t2206337031 * __this, Matrix4x4_t1817901843  p0, Matrix4x4_t1817901843  p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Quaternion_get_identity_m3722672781 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t1817901843  Matrix4x4_TRS_m3801934620 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Quaternion_t2301928331  p1, Vector3_t3722313464  p2, const RuntimeMethod* method);
// UnityEngine.Material Coffee.UIExtensions.SoftMask::get_material()
extern "C" IL2CPP_METHOD_ATTR Material_t340375123 * SoftMask_get_material_m4230810281 (SoftMask_t1817791576 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MaterialPropertyBlock::SetTexture(System.Int32,UnityEngine.Texture)
extern "C" IL2CPP_METHOD_ATTR void MaterialPropertyBlock_SetTexture_m3027584768 (MaterialPropertyBlock_t3213117958 * __this, int32_t p0, Texture_t3661962703 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.MaterialPropertyBlock::SetFloat(System.Int32,System.Single)
extern "C" IL2CPP_METHOD_ATTR void MaterialPropertyBlock_SetFloat_m3255723079 (MaterialPropertyBlock_t3213117958 * __this, int32_t p0, float p1, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t1817901843  Transform_get_localToWorldMatrix_m4155710351 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.CommandBuffer::DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32,System.Int32,UnityEngine.MaterialPropertyBlock)
extern "C" IL2CPP_METHOD_ATTR void CommandBuffer_DrawMesh_m2147885662 (CommandBuffer_t2206337031 * __this, Mesh_t3648964284 * p0, Matrix4x4_t1817901843  p1, Material_t340375123 * p2, int32_t p3, int32_t p4, MaterialPropertyBlock_t3213117958 * p5, const RuntimeMethod* method);
// System.Void UnityEngine.Graphics::ExecuteCommandBuffer(UnityEngine.Rendering.CommandBuffer)
extern "C" IL2CPP_METHOD_ATTR void Graphics_ExecuteCommandBuffer_m581468154 (RuntimeObject * __this /* static, unused */, CommandBuffer_t2206337031 * p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::ClosestPowerOfTwo(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Mathf_ClosestPowerOfTwo_m1106515315 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::CeilToInt(System.Single)
extern "C" IL2CPP_METHOD_ATTR int32_t Mathf_CeilToInt_m432108984 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method);
// System.Void UnityEngine.RenderTexture::Release()
extern "C" IL2CPP_METHOD_ATTR void RenderTexture_Release_m1749927881 (RenderTexture_t2108887433 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
extern "C" IL2CPP_METHOD_ATTR void RenderTexture_ReleaseTemporary_m2400081536 (RuntimeObject * __this /* static, unused */, RenderTexture_t2108887433 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::Contains(!0)
inline bool List_1_Contains_m2410279580 (List_1_t3289866318 * __this, SoftMask_t1817791576 * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t3289866318 *, SoftMask_t1817791576 *, const RuntimeMethod*))List_1_Contains_m784383322_gshared)(__this, p0, method);
}
// System.Void System.Predicate`1<Coffee.UIExtensions.SoftMask>::.ctor(System.Object,System.IntPtr)
inline void Predicate_1__ctor_m1967950527 (Predicate_1_t2643085700 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Predicate_1_t2643085700 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Predicate_1__ctor_m327447107_gshared)(__this, p0, p1, method);
}
// System.Int32 System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::RemoveAll(System.Predicate`1<!0>)
inline int32_t List_1_RemoveAll_m3895672821 (List_1_t3289866318 * __this, Predicate_1_t2643085700 * p0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t3289866318 *, Predicate_1_t2643085700 *, const RuntimeMethod*))List_1_RemoveAll_m2696662810_gshared)(__this, p0, method);
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Texture2D__ctor_m2862217990 (Texture2D_t3840446185 * __this, int32_t p0, int32_t p1, int32_t p2, bool p3, const RuntimeMethod* method);
// UnityEngine.RenderTexture UnityEngine.RenderTexture::get_active()
extern "C" IL2CPP_METHOD_ATTR RenderTexture_t2108887433 * RenderTexture_get_active_m2427925032 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
extern "C" IL2CPP_METHOD_ATTR void RenderTexture_set_active_m1437732586 (RuntimeObject * __this /* static, unused */, RenderTexture_t2108887433 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Rect__ctor_m2614021312 (Rect_t2360479859 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Texture2D_ReadPixels_m3395504488 (Texture2D_t3840446185 * __this, Rect_t2360479859  p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Texture2D_Apply_m2470606565 (Texture2D_t3840446185 * __this, bool p0, bool p1, const RuntimeMethod* method);
// System.Byte[] UnityEngine.Texture2D::GetRawTextureData()
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_t4116647657* Texture2D_GetRawTextureData_m2891685427 (Texture2D_t3840446185 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Color__ctor_m2943235014 (Color_t2555686324 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
extern "C" IL2CPP_METHOD_ATTR void Material__ctor_m249231841 (Material_t340375123 * __this, Material_t340375123 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
extern "C" IL2CPP_METHOD_ATTR void Material_SetTexture_m3009528825 (Material_t340375123 * __this, int32_t p0, Texture_t3661962703 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector4__ctor_m2498754347 (Vector4_t3319028937 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetVector(System.Int32,UnityEngine.Vector4)
extern "C" IL2CPP_METHOD_ATTR void Material_SetVector_m2633010038 (Material_t340375123 * __this, int32_t p0, Vector4_t3319028937  p1, const RuntimeMethod* method);
// System.Void UnityEngine.UI.StencilMaterial::Remove(UnityEngine.Material)
extern "C" IL2CPP_METHOD_ATTR void StencilMaterial_Remove_m1301487727 (RuntimeObject * __this /* static, unused */, Material_t340375123 * p0, const RuntimeMethod* method);
// System.Void Coffee.UIExtensions.SoftMaskable::ReleaseMaterial(UnityEngine.Material&)
extern "C" IL2CPP_METHOD_ATTR void SoftMaskable_ReleaseMaterial_m86784683 (SoftMaskable_t353105572 * __this, Material_t340375123 ** ___mat0, const RuntimeMethod* method);
// UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
extern "C" IL2CPP_METHOD_ATTR Texture2D_t3840446185 * Texture2D_get_whiteTexture_m2176011403 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
extern "C" IL2CPP_METHOD_ATTR bool RectTransformUtility_RectangleContainsScreenPoint_m4031431712 (RuntimeObject * __this /* static, unused */, RectTransform_t3704657025 * p0, Vector2_t2156229523  p1, Camera_t4157153871 * p2, const RuntimeMethod* method);
// Coffee.UIExtensions.SoftMask Coffee.UIExtensions.SoftMask::get_parent()
extern "C" IL2CPP_METHOD_ATTR SoftMask_t1817791576 * SoftMask_get_parent_m3860326613 (SoftMask_t1817791576 * __this, const RuntimeMethod* method);
// UnityEngine.UI.Graphic Coffee.UIExtensions.SoftMaskable::get_graphic()
extern "C" IL2CPP_METHOD_ATTR Graphic_t1660335611 * SoftMaskable_get_graphic_m2211451753 (SoftMaskable_t353105572 * __this, const RuntimeMethod* method);
// System.Boolean Coffee.UIExtensions.SoftMask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.UI.Graphic,System.Int32[])
extern "C" IL2CPP_METHOD_ATTR bool SoftMask_IsRaycastLocationValid_m4269246139 (SoftMask_t1817791576 * __this, Vector2_t2156229523  ___sp0, Camera_t4157153871 * ___eventCamera1, Graphic_t1660335611 * ___g2, Int32U5BU5D_t385246372* ___interactions3, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Graphic>()
inline Graphic_t1660335611 * Component_GetComponent_TisGraphic_t1660335611_m1118939870 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  Graphic_t1660335611 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// System.Void Coffee.UIExtensions.SoftMaskable::SetMaskInteraction(UnityEngine.SpriteMaskInteraction,UnityEngine.SpriteMaskInteraction,UnityEngine.SpriteMaskInteraction,UnityEngine.SpriteMaskInteraction)
extern "C" IL2CPP_METHOD_ATTR void SoftMaskable_SetMaskInteraction_m4089080 (SoftMaskable_t353105572 * __this, int32_t ___layer00, int32_t ___layer11, int32_t ___layer22, int32_t ___layer33, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMaskable>::.ctor()
inline void List_1__ctor_m731543265 (List_1_t1825180314 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1825180314 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMaskable>::Add(!0)
inline void List_1_Add_m589637811 (List_1_t1825180314 * __this, SoftMaskable_t353105572 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1825180314 *, SoftMaskable_t353105572 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// UnityEngine.Material UnityEngine.UI.Graphic::get_defaultGraphicMaterial()
extern "C" IL2CPP_METHOD_ATTR Material_t340375123 * Graphic_get_defaultGraphicMaterial_m3107284931 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMaskable>::Remove(!0)
inline bool List_1_Remove_m616773178 (List_1_t1825180314 * __this, SoftMaskable_t353105572 * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t1825180314 *, SoftMaskable_t353105572 *, const RuntimeMethod*))List_1_Remove_m2390619627_gshared)(__this, p0, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Coffee.UIExtensions.SoftMask::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SoftMask__ctor_m3442446782 (SoftMask_t1817791576 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask__ctor_m3442446782_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Softness_12((1.0f));
		List_1_t3289866318 * L_0 = (List_1_t3289866318 *)il2cpp_codegen_object_new(List_1_t3289866318_il2cpp_TypeInfo_var);
		List_1__ctor_m3675240295(L_0, /*hidden argument*/List_1__ctor_m3675240295_RuntimeMethod_var);
		__this->set__children_30(L_0);
		Mask__ctor_m1269055150(__this, /*hidden argument*/NULL);
		return;
	}
}
// Coffee.UIExtensions.SoftMask/DesamplingRate Coffee.UIExtensions.SoftMask::get_desamplingRate()
extern "C" IL2CPP_METHOD_ATTR int32_t SoftMask_get_desamplingRate_m151627739 (SoftMask_t1817791576 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_DesamplingRate_11();
		return L_0;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::set_desamplingRate(Coffee.UIExtensions.SoftMask/DesamplingRate)
extern "C" IL2CPP_METHOD_ATTR void SoftMask_set_desamplingRate_m1578942681 (SoftMask_t1817791576 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_DesamplingRate_11();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_2 = ___value0;
		__this->set_m_DesamplingRate_11(L_2);
		SoftMask_set_hasChanged_m583714446(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return;
	}
}
// System.Single Coffee.UIExtensions.SoftMask::get_softness()
extern "C" IL2CPP_METHOD_ATTR float SoftMask_get_softness_m4199035078 (SoftMask_t1817791576 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_m_Softness_12();
		return L_0;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::set_softness(System.Single)
extern "C" IL2CPP_METHOD_ATTR void SoftMask_set_softness_m3479775679 (SoftMask_t1817791576 * __this, float ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_set_softness_m3479775679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m56433566(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___value0 = L_1;
		float L_2 = __this->get_m_Softness_12();
		float L_3 = ___value0;
		if ((((float)L_2) == ((float)L_3)))
		{
			goto IL_0022;
		}
	}
	{
		float L_4 = ___value0;
		__this->set_m_Softness_12(L_4);
		SoftMask_set_hasChanged_m583714446(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Boolean Coffee.UIExtensions.SoftMask::get_ignoreParent()
extern "C" IL2CPP_METHOD_ATTR bool SoftMask_get_ignoreParent_m3602871349 (SoftMask_t1817791576 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_IgnoreParent_13();
		return L_0;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::set_ignoreParent(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void SoftMask_set_ignoreParent_m3489688087 (SoftMask_t1817791576 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_IgnoreParent_13();
		bool L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0020;
		}
	}
	{
		bool L_2 = ___value0;
		__this->set_m_IgnoreParent_13(L_2);
		SoftMask_set_hasChanged_m583714446(__this, (bool)1, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(12 /* System.Void UnityEngine.EventSystems.UIBehaviour::OnTransformParentChanged() */, __this);
	}

IL_0020:
	{
		return;
	}
}
// System.Boolean Coffee.UIExtensions.SoftMask::get_partOfParent()
extern "C" IL2CPP_METHOD_ATTR bool SoftMask_get_partOfParent_m689768004 (SoftMask_t1817791576 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_PartOfParent_14();
		return L_0;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::set_partOfParent(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void SoftMask_set_partOfParent_m4142753027 (SoftMask_t1817791576 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_PartOfParent_14();
		bool L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0020;
		}
	}
	{
		bool L_2 = ___value0;
		__this->set_m_PartOfParent_14(L_2);
		SoftMask_set_hasChanged_m583714446(__this, (bool)1, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(12 /* System.Void UnityEngine.EventSystems.UIBehaviour::OnTransformParentChanged() */, __this);
	}

IL_0020:
	{
		return;
	}
}
// UnityEngine.RenderTexture Coffee.UIExtensions.SoftMask::get_softMaskBuffer()
extern "C" IL2CPP_METHOD_ATTR RenderTexture_t2108887433 * SoftMask_get_softMaskBuffer_m514739908 (SoftMask_t1817791576 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_get_softMaskBuffer_m514739908_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		SoftMask_t1817791576 * L_0 = __this->get__parent_29();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		RenderTexture_t2108887433 ** L_2 = __this->get_address_of__softMaskBuffer_26();
		SoftMask_ReleaseRT_m1409116420(__this, (RenderTexture_t2108887433 **)L_2, /*hidden argument*/NULL);
		SoftMask_t1817791576 * L_3 = __this->get__parent_29();
		NullCheck(L_3);
		RenderTexture_t2108887433 * L_4 = SoftMask_get_softMaskBuffer_m514739908(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0028:
	{
		int32_t L_5 = __this->get_m_DesamplingRate_11();
		SoftMask_GetDesamplingSize_m2102729222(__this, L_5, (int32_t*)(&V_0), (int32_t*)(&V_1), /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_6 = __this->get__softMaskBuffer_26();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0076;
		}
	}
	{
		RenderTexture_t2108887433 * L_8 = __this->get__softMaskBuffer_26();
		NullCheck(L_8);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_8);
		int32_t L_10 = V_0;
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_006a;
		}
	}
	{
		RenderTexture_t2108887433 * L_11 = __this->get__softMaskBuffer_26();
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_11);
		int32_t L_13 = V_1;
		if ((((int32_t)L_12) == ((int32_t)L_13)))
		{
			goto IL_0076;
		}
	}

IL_006a:
	{
		RenderTexture_t2108887433 ** L_14 = __this->get_address_of__softMaskBuffer_26();
		SoftMask_ReleaseRT_m1409116420(__this, (RenderTexture_t2108887433 **)L_14, /*hidden argument*/NULL);
	}

IL_0076:
	{
		RenderTexture_t2108887433 * L_15 = __this->get__softMaskBuffer_26();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_00a4;
		}
	}
	{
		int32_t L_17 = V_0;
		int32_t L_18 = V_1;
		RenderTexture_t2108887433 * L_19 = RenderTexture_GetTemporary_m1363375227(NULL /*static, unused*/, L_17, L_18, 0, 0, 0, /*hidden argument*/NULL);
		__this->set__softMaskBuffer_26(L_19);
		SoftMask_set_hasChanged_m583714446(__this, (bool)1, /*hidden argument*/NULL);
		__this->set__hasStencilStateChanged_32((bool)1);
	}

IL_00a4:
	{
		RenderTexture_t2108887433 * L_20 = __this->get__softMaskBuffer_26();
		return L_20;
	}
}
// System.Boolean Coffee.UIExtensions.SoftMask::get_hasChanged()
extern "C" IL2CPP_METHOD_ATTR bool SoftMask_get_hasChanged_m3337097011 (SoftMask_t1817791576 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_get_hasChanged_m3337097011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool G_B3_0 = false;
	{
		SoftMask_t1817791576 * L_0 = __this->get__parent_29();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		SoftMask_t1817791576 * L_2 = __this->get__parent_29();
		NullCheck(L_2);
		bool L_3 = SoftMask_get_hasChanged_m3337097011(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0026;
	}

IL_0020:
	{
		bool L_4 = __this->get__hasChanged_31();
		G_B3_0 = L_4;
	}

IL_0026:
	{
		return G_B3_0;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::set_hasChanged(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void SoftMask_set_hasChanged_m583714446 (SoftMask_t1817791576 * __this, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_set_hasChanged_m583714446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SoftMask_t1817791576 * L_0 = __this->get__parent_29();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		SoftMask_t1817791576 * L_2 = __this->get__parent_29();
		bool L_3 = ___value0;
		NullCheck(L_2);
		SoftMask_set_hasChanged_m583714446(L_2, L_3, /*hidden argument*/NULL);
	}

IL_001c:
	{
		bool L_4 = ___value0;
		__this->set__hasChanged_31(L_4);
		return;
	}
}
// Coffee.UIExtensions.SoftMask Coffee.UIExtensions.SoftMask::get_parent()
extern "C" IL2CPP_METHOD_ATTR SoftMask_t1817791576 * SoftMask_get_parent_m3860326613 (SoftMask_t1817791576 * __this, const RuntimeMethod* method)
{
	{
		SoftMask_t1817791576 * L_0 = __this->get__parent_29();
		return L_0;
	}
}
// UnityEngine.Material Coffee.UIExtensions.SoftMask::GetModifiedMaterial(UnityEngine.Material)
extern "C" IL2CPP_METHOD_ATTR Material_t340375123 * SoftMask_GetModifiedMaterial_m1571737707 (SoftMask_t1817791576 * __this, Material_t340375123 * ___baseMaterial0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_GetModifiedMaterial_m1571737707_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t340375123 * V_0 = NULL;
	{
		SoftMask_set_hasChanged_m583714446(__this, (bool)1, /*hidden argument*/NULL);
		Material_t340375123 * L_0 = ___baseMaterial0;
		Material_t340375123 * L_1 = Mask_GetModifiedMaterial_m2692272647(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = __this->get_m_IgnoreParent_13();
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		Material_t340375123 * L_3 = V_0;
		Material_t340375123 * L_4 = ___baseMaterial0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0032;
		}
	}
	{
		Material_t340375123 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		int32_t L_7 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_StencilCompId_19();
		NullCheck(L_6);
		Material_SetInt_m475299667(L_6, L_7, 8, /*hidden argument*/NULL);
	}

IL_0032:
	{
		Material_t340375123 * L_8 = V_0;
		return L_8;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::UnityEngine.UI.IMeshModifier.ModifyMesh(UnityEngine.Mesh)
extern "C" IL2CPP_METHOD_ATTR void SoftMask_UnityEngine_UI_IMeshModifier_ModifyMesh_m1244432509 (SoftMask_t1817791576 * __this, Mesh_t3648964284 * ___mesh0, const RuntimeMethod* method)
{
	{
		SoftMask_set_hasChanged_m583714446(__this, (bool)1, /*hidden argument*/NULL);
		Mesh_t3648964284 * L_0 = ___mesh0;
		__this->set__mesh_28(L_0);
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::UnityEngine.UI.IMeshModifier.ModifyMesh(UnityEngine.UI.VertexHelper)
extern "C" IL2CPP_METHOD_ATTR void SoftMask_UnityEngine_UI_IMeshModifier_ModifyMesh_m3086669530 (SoftMask_t1817791576 * __this, VertexHelper_t2453304189 * ___verts0, const RuntimeMethod* method)
{
	{
		bool L_0 = Behaviour_get_isActiveAndEnabled_m3143666263(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		VertexHelper_t2453304189 * L_1 = ___verts0;
		Mesh_t3648964284 * L_2 = SoftMask_get_mesh_m903007999(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		VertexHelper_FillMesh_m1654068917(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		SoftMask_set_hasChanged_m583714446(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Coffee.UIExtensions.SoftMask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.UI.Graphic,System.Int32[])
extern "C" IL2CPP_METHOD_ATTR bool SoftMask_IsRaycastLocationValid_m4269246139 (SoftMask_t1817791576 * __this, Vector2_t2156229523  ___sp0, Camera_t4157153871 * ___eventCamera1, Graphic_t1660335611 * ___g2, Int32U5BU5D_t385246372* ___interactions3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_IsRaycastLocationValid_m4269246139_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = Behaviour_get_isActiveAndEnabled_m3143666263(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Graphic_t1660335611 * L_1 = ___g2;
		Graphic_t1660335611 * L_2 = Mask_get_graphic_m2572620787(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		Graphic_t1660335611 * L_4 = ___g2;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean UnityEngine.UI.Graphic::get_raycastTarget() */, L_4);
		if (L_5)
		{
			goto IL_0029;
		}
	}

IL_0027:
	{
		return (bool)1;
	}

IL_0029:
	{
		RenderTexture_t2108887433 * L_6 = SoftMask_get_softMaskBuffer_m514739908(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_6);
		float L_8 = (&___sp0)->get_x_0();
		int32_t L_9 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_10 = Mathf_Clamp01_m56433566(NULL /*static, unused*/, ((float)((float)L_8/(float)(((float)((float)L_9))))), /*hidden argument*/NULL);
		V_0 = (((int32_t)((int32_t)((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)1))))), (float)L_10)))));
		RenderTexture_t2108887433 * L_11 = SoftMask_get_softMaskBuffer_m514739908(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_11);
		float L_13 = (&___sp0)->get_y_1();
		int32_t L_14 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_15 = Mathf_Clamp01_m56433566(NULL /*static, unused*/, ((float)((float)L_13/(float)(((float)((float)L_14))))), /*hidden argument*/NULL);
		V_1 = (((int32_t)((int32_t)((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)1))))), (float)L_15)))));
		int32_t L_16 = V_0;
		int32_t L_17 = V_1;
		Int32U5BU5D_t385246372* L_18 = ___interactions3;
		float L_19 = SoftMask_GetPixelValue_m3927220954(__this, L_16, L_17, L_18, /*hidden argument*/NULL);
		return (bool)((((float)(0.5f)) < ((float)L_19))? 1 : 0);
	}
}
// System.Boolean Coffee.UIExtensions.SoftMask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern "C" IL2CPP_METHOD_ATTR bool SoftMask_IsRaycastLocationValid_m3765102717 (SoftMask_t1817791576 * __this, Vector2_t2156229523  ___sp0, Camera_t4157153871 * ___eventCamera1, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void SoftMask_OnEnable_m1417838140 (SoftMask_t1817791576 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_OnEnable_m1417838140_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		SoftMask_set_hasChanged_m583714446(__this, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		List_1_t3289866318 * L_0 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_ActiveSoftMasks_17();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m120286467(L_0, /*hidden argument*/List_1_get_Count_m120286467_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_007e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		WillRenderCanvases_t3309123499 * L_2 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_33();
		if (L_2)
		{
			goto IL_002e;
		}
	}
	{
		intptr_t L_3 = (intptr_t)SoftMask_UpdateMaskTextures_m3160689611_RuntimeMethod_var;
		WillRenderCanvases_t3309123499 * L_4 = (WillRenderCanvases_t3309123499 *)il2cpp_codegen_object_new(WillRenderCanvases_t3309123499_il2cpp_TypeInfo_var);
		WillRenderCanvases__ctor_m147079397(L_4, NULL, (intptr_t)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_33(L_4);
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		WillRenderCanvases_t3309123499 * L_5 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_33();
		Canvas_add_willRenderCanvases_m1234571137(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		int32_t L_6 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_StencilCompId_19();
		if (L_6)
		{
			goto IL_007e;
		}
	}
	{
		int32_t L_7 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral2160052589, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->set_s_StencilCompId_19(L_7);
		int32_t L_8 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral873708475, /*hidden argument*/NULL);
		((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->set_s_ColorMaskId_20(L_8);
		int32_t L_9 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral3184621405, /*hidden argument*/NULL);
		((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->set_s_MainTexId_21(L_9);
		int32_t L_10 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral2836376834, /*hidden argument*/NULL);
		((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->set_s_SoftnessId_22(L_10);
	}

IL_007e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		List_1_t3289866318 * L_11 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_ActiveSoftMasks_17();
		NullCheck(L_11);
		List_1_Add_m2471691335(L_11, __this, /*hidden argument*/List_1_Add_m2471691335_RuntimeMethod_var);
		List_1_t3289866318 * L_12 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_TempRelatables_18();
		Component_GetComponentsInChildren_TisSoftMask_t1817791576_m1497230955(__this, (bool)0, L_12, /*hidden argument*/Component_GetComponentsInChildren_TisSoftMask_t1817791576_m1497230955_RuntimeMethod_var);
		List_1_t3289866318 * L_13 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_TempRelatables_18();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m120286467(L_13, /*hidden argument*/List_1_get_Count_m120286467_RuntimeMethod_var);
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_14, (int32_t)1));
		goto IL_00bb;
	}

IL_00a7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		List_1_t3289866318 * L_15 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_TempRelatables_18();
		int32_t L_16 = V_0;
		NullCheck(L_15);
		SoftMask_t1817791576 * L_17 = List_1_get_Item_m2136910319(L_15, L_16, /*hidden argument*/List_1_get_Item_m2136910319_RuntimeMethod_var);
		NullCheck(L_17);
		VirtActionInvoker0::Invoke(12 /* System.Void UnityEngine.EventSystems.UIBehaviour::OnTransformParentChanged() */, L_17);
		int32_t L_18 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_18, (int32_t)1));
	}

IL_00bb:
	{
		int32_t L_19 = V_0;
		if ((((int32_t)0) <= ((int32_t)L_19)))
		{
			goto IL_00a7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		List_1_t3289866318 * L_20 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_TempRelatables_18();
		NullCheck(L_20);
		List_1_Clear_m923421119(L_20, /*hidden argument*/List_1_Clear_m923421119_RuntimeMethod_var);
		MaterialPropertyBlock_t3213117958 * L_21 = (MaterialPropertyBlock_t3213117958 *)il2cpp_codegen_object_new(MaterialPropertyBlock_t3213117958_il2cpp_TypeInfo_var);
		MaterialPropertyBlock__ctor_m3898279695(L_21, /*hidden argument*/NULL);
		__this->set__mpb_23(L_21);
		CommandBuffer_t2206337031 * L_22 = (CommandBuffer_t2206337031 *)il2cpp_codegen_object_new(CommandBuffer_t2206337031_il2cpp_TypeInfo_var);
		CommandBuffer__ctor_m3028411456(L_22, /*hidden argument*/NULL);
		__this->set__cb_24(L_22);
		Graphic_t1660335611 * L_23 = Mask_get_graphic_m2572620787(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_23);
		Mask_OnEnable_m2186824439(__this, /*hidden argument*/NULL);
		__this->set__hasStencilStateChanged_32((bool)0);
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void SoftMask_OnDisable_m259598144 (SoftMask_t1817791576 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_OnDisable_m259598144_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		List_1_t3289866318 * L_0 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_ActiveSoftMasks_17();
		NullCheck(L_0);
		List_1_Remove_m1687504378(L_0, __this, /*hidden argument*/List_1_Remove_m1687504378_RuntimeMethod_var);
		List_1_t3289866318 * L_1 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_ActiveSoftMasks_17();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m120286467(L_1, /*hidden argument*/List_1_get_Count_m120286467_RuntimeMethod_var);
		if (L_2)
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		WillRenderCanvases_t3309123499 * L_3 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_34();
		if (L_3)
		{
			goto IL_0033;
		}
	}
	{
		intptr_t L_4 = (intptr_t)SoftMask_UpdateMaskTextures_m3160689611_RuntimeMethod_var;
		WillRenderCanvases_t3309123499 * L_5 = (WillRenderCanvases_t3309123499 *)il2cpp_codegen_object_new(WillRenderCanvases_t3309123499_il2cpp_TypeInfo_var);
		WillRenderCanvases__ctor_m147079397(L_5, NULL, (intptr_t)L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache1_34(L_5);
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		WillRenderCanvases_t3309123499 * L_6 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_34();
		Canvas_remove_willRenderCanvases_m3663577790(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_003d:
	{
		List_1_t3289866318 * L_7 = __this->get__children_30();
		NullCheck(L_7);
		int32_t L_8 = List_1_get_Count_m120286467(L_7, /*hidden argument*/List_1_get_Count_m120286467_RuntimeMethod_var);
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1));
		goto IL_006b;
	}

IL_0050:
	{
		List_1_t3289866318 * L_9 = __this->get__children_30();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		SoftMask_t1817791576 * L_11 = List_1_get_Item_m2136910319(L_9, L_10, /*hidden argument*/List_1_get_Item_m2136910319_RuntimeMethod_var);
		SoftMask_t1817791576 * L_12 = __this->get__parent_29();
		NullCheck(L_11);
		SoftMask_SetParent_m1377869388(L_11, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)1));
	}

IL_006b:
	{
		int32_t L_14 = V_0;
		if ((((int32_t)0) <= ((int32_t)L_14)))
		{
			goto IL_0050;
		}
	}
	{
		List_1_t3289866318 * L_15 = __this->get__children_30();
		NullCheck(L_15);
		List_1_Clear_m923421119(L_15, /*hidden argument*/List_1_Clear_m923421119_RuntimeMethod_var);
		SoftMask_SetParent_m1377869388(__this, (SoftMask_t1817791576 *)NULL, /*hidden argument*/NULL);
		MaterialPropertyBlock_t3213117958 * L_16 = __this->get__mpb_23();
		NullCheck(L_16);
		MaterialPropertyBlock_Clear_m1909512031(L_16, /*hidden argument*/NULL);
		__this->set__mpb_23((MaterialPropertyBlock_t3213117958 *)NULL);
		CommandBuffer_t2206337031 * L_17 = __this->get__cb_24();
		NullCheck(L_17);
		CommandBuffer_Release_m529480289(L_17, /*hidden argument*/NULL);
		__this->set__cb_24((CommandBuffer_t2206337031 *)NULL);
		Mesh_t3648964284 * L_18 = __this->get__mesh_28();
		SoftMask_ReleaseObject_m16266906(__this, L_18, /*hidden argument*/NULL);
		__this->set__mesh_28((Mesh_t3648964284 *)NULL);
		Material_t340375123 * L_19 = __this->get__material_25();
		SoftMask_ReleaseObject_m16266906(__this, L_19, /*hidden argument*/NULL);
		__this->set__material_25((Material_t340375123 *)NULL);
		RenderTexture_t2108887433 ** L_20 = __this->get_address_of__softMaskBuffer_26();
		SoftMask_ReleaseRT_m1409116420(__this, (RenderTexture_t2108887433 **)L_20, /*hidden argument*/NULL);
		Mask_OnDisable_m1270951829(__this, /*hidden argument*/NULL);
		__this->set__hasStencilStateChanged_32((bool)0);
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::OnTransformParentChanged()
extern "C" IL2CPP_METHOD_ATTR void SoftMask_OnTransformParentChanged_m2418922873 (SoftMask_t1817791576 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_OnTransformParentChanged_m2418922873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SoftMask_t1817791576 * V_0 = NULL;
	Transform_t3600365921 * V_1 = NULL;
	{
		SoftMask_set_hasChanged_m583714446(__this, (bool)1, /*hidden argument*/NULL);
		V_0 = (SoftMask_t1817791576 *)NULL;
		bool L_0 = Behaviour_get_isActiveAndEnabled_m3143666263(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_005f;
		}
	}
	{
		bool L_1 = __this->get_m_IgnoreParent_13();
		if (L_1)
		{
			goto IL_005f;
		}
	}
	{
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Transform_get_parent_m835071599(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_003e;
	}

IL_0030:
	{
		Transform_t3600365921 * L_4 = V_1;
		NullCheck(L_4);
		SoftMask_t1817791576 * L_5 = Component_GetComponent_TisSoftMask_t1817791576_m4226683470(L_4, /*hidden argument*/Component_GetComponent_TisSoftMask_t1817791576_m4226683470_RuntimeMethod_var);
		V_0 = L_5;
		Transform_t3600365921 * L_6 = V_1;
		NullCheck(L_6);
		Transform_t3600365921 * L_7 = Transform_get_parent_m835071599(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
	}

IL_003e:
	{
		Transform_t3600365921 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_005f;
		}
	}
	{
		SoftMask_t1817791576 * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0030;
		}
	}
	{
		SoftMask_t1817791576 * L_12 = V_0;
		NullCheck(L_12);
		bool L_13 = Behaviour_get_enabled_m753527255(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0030;
		}
	}

IL_005f:
	{
		SoftMask_t1817791576 * L_14 = V_0;
		SoftMask_SetParent_m1377869388(__this, L_14, /*hidden argument*/NULL);
		SoftMask_set_hasChanged_m583714446(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::OnRectTransformDimensionsChange()
extern "C" IL2CPP_METHOD_ATTR void SoftMask_OnRectTransformDimensionsChange_m1517770982 (SoftMask_t1817791576 * __this, const RuntimeMethod* method)
{
	{
		SoftMask_set_hasChanged_m583714446(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material Coffee.UIExtensions.SoftMask::get_material()
extern "C" IL2CPP_METHOD_ATTR Material_t340375123 * SoftMask_get_material_m4230810281 (SoftMask_t1817791576 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_get_material_m4230810281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t340375123 * V_0 = NULL;
	Material_t340375123 * G_B6_0 = NULL;
	SoftMask_t1817791576 * G_B4_0 = NULL;
	SoftMask_t1817791576 * G_B3_0 = NULL;
	Shader_t4151988712 * G_B5_0 = NULL;
	SoftMask_t1817791576 * G_B5_1 = NULL;
	{
		Material_t340375123 * L_0 = __this->get__material_25();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t340375123 * L_2 = __this->get__material_25();
		G_B6_0 = L_2;
		goto IL_005c;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		Shader_t4151988712 * L_3 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_SoftMaskShader_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B3_0 = __this;
		if (!L_4)
		{
			G_B4_0 = __this;
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		Shader_t4151988712 * L_5 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_SoftMaskShader_15();
		G_B5_0 = L_5;
		G_B5_1 = G_B3_0;
		goto IL_0045;
	}

IL_0035:
	{
		Shader_t4151988712 * L_6 = Resources_Load_TisShader_t4151988712_m741163411(NULL /*static, unused*/, _stringLiteral383173083, /*hidden argument*/Resources_Load_TisShader_t4151988712_m741163411_RuntimeMethod_var);
		Shader_t4151988712 * L_7 = L_6;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->set_s_SoftMaskShader_15(L_7);
		G_B5_0 = L_7;
		G_B5_1 = G_B4_0;
	}

IL_0045:
	{
		Material_t340375123 * L_8 = (Material_t340375123 *)il2cpp_codegen_object_new(Material_t340375123_il2cpp_TypeInfo_var);
		Material__ctor_m1662457592(L_8, G_B5_0, /*hidden argument*/NULL);
		V_0 = L_8;
		Material_t340375123 * L_9 = V_0;
		NullCheck(L_9);
		Object_set_hideFlags_m1648752846(L_9, ((int32_t)61), /*hidden argument*/NULL);
		Material_t340375123 * L_10 = V_0;
		Material_t340375123 * L_11 = L_10;
		V_0 = L_11;
		NullCheck(G_B5_1);
		G_B5_1->set__material_25(L_11);
		Material_t340375123 * L_12 = V_0;
		G_B6_0 = L_12;
	}

IL_005c:
	{
		return G_B6_0;
	}
}
// UnityEngine.Mesh Coffee.UIExtensions.SoftMask::get_mesh()
extern "C" IL2CPP_METHOD_ATTR Mesh_t3648964284 * SoftMask_get_mesh_m903007999 (SoftMask_t1817791576 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_get_mesh_m903007999_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Mesh_t3648964284 * V_0 = NULL;
	Mesh_t3648964284 * G_B3_0 = NULL;
	{
		Mesh_t3648964284 * L_0 = __this->get__mesh_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Mesh_t3648964284 * L_2 = __this->get__mesh_28();
		G_B3_0 = L_2;
		goto IL_0033;
	}

IL_001b:
	{
		Mesh_t3648964284 * L_3 = (Mesh_t3648964284 *)il2cpp_codegen_object_new(Mesh_t3648964284_il2cpp_TypeInfo_var);
		Mesh__ctor_m2533762929(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		Mesh_t3648964284 * L_4 = V_0;
		NullCheck(L_4);
		Object_set_hideFlags_m1648752846(L_4, ((int32_t)61), /*hidden argument*/NULL);
		Mesh_t3648964284 * L_5 = V_0;
		Mesh_t3648964284 * L_6 = L_5;
		V_0 = L_6;
		__this->set__mesh_28(L_6);
		Mesh_t3648964284 * L_7 = V_0;
		G_B3_0 = L_7;
	}

IL_0033:
	{
		return G_B3_0;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::UpdateMaskTextures()
extern "C" IL2CPP_METHOD_ATTR void SoftMask_UpdateMaskTextures_m3160689611 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_UpdateMaskTextures_m3160689611_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SoftMask_t1817791576 * V_0 = NULL;
	Enumerator_t884142899  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Canvas_t3310196443 * V_2 = NULL;
	Camera_t4157153871 * V_3 = NULL;
	Matrix4x4_t1817901843  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Matrix4x4_t1817901843  V_5;
	memset(&V_5, 0, sizeof(V_5));
	RectTransform_t3704657025 * V_6 = NULL;
	SoftMask_t1817791576 * V_7 = NULL;
	Enumerator_t884142899  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		List_1_t3289866318 * L_0 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_ActiveSoftMasks_17();
		NullCheck(L_0);
		Enumerator_t884142899  L_1 = List_1_GetEnumerator_m3631405139(L_0, /*hidden argument*/List_1_GetEnumerator_m3631405139_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a5;
		}

IL_0010:
		{
			SoftMask_t1817791576 * L_2 = Enumerator_get_Current_m2347303577((Enumerator_t884142899 *)(&V_1), /*hidden argument*/Enumerator_get_Current_m2347303577_RuntimeMethod_var);
			V_0 = L_2;
			SoftMask_t1817791576 * L_3 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
			bool L_4 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			if (!L_4)
			{
				goto IL_002e;
			}
		}

IL_0023:
		{
			SoftMask_t1817791576 * L_5 = V_0;
			NullCheck(L_5);
			bool L_6 = L_5->get__hasChanged_31();
			if (!L_6)
			{
				goto IL_0033;
			}
		}

IL_002e:
		{
			goto IL_00a5;
		}

IL_0033:
		{
			SoftMask_t1817791576 * L_7 = V_0;
			NullCheck(L_7);
			Graphic_t1660335611 * L_8 = Mask_get_graphic_m2572620787(L_7, /*hidden argument*/NULL);
			NullCheck(L_8);
			Canvas_t3310196443 * L_9 = Graphic_get_canvas_m3320066409(L_8, /*hidden argument*/NULL);
			V_2 = L_9;
			Canvas_t3310196443 * L_10 = V_2;
			NullCheck(L_10);
			int32_t L_11 = Canvas_get_renderMode_m841659411(L_10, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_11) == ((uint32_t)2))))
			{
				goto IL_0082;
			}
		}

IL_004b:
		{
			Canvas_t3310196443 * L_12 = V_2;
			NullCheck(L_12);
			Camera_t4157153871 * L_13 = Canvas_get_worldCamera_m3516267897(L_12, /*hidden argument*/NULL);
			V_3 = L_13;
			Camera_t4157153871 * L_14 = V_3;
			NullCheck(L_14);
			Matrix4x4_t1817901843  L_15 = Camera_get_projectionMatrix_m667780853(L_14, /*hidden argument*/NULL);
			Camera_t4157153871 * L_16 = V_3;
			NullCheck(L_16);
			Matrix4x4_t1817901843  L_17 = Camera_get_worldToCameraMatrix_m22661425(L_16, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
			Matrix4x4_t1817901843  L_18 = Matrix4x4_op_Multiply_m1876492807(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
			V_4 = L_18;
			Camera_t4157153871 * L_19 = V_3;
			NullCheck(L_19);
			Matrix4x4_t1817901843  L_20 = Camera_get_previousViewProjectionMatrix_m3358791387(L_19, /*hidden argument*/NULL);
			V_5 = L_20;
			Matrix4x4_t1817901843  L_21 = V_5;
			Matrix4x4_t1817901843  L_22 = V_4;
			bool L_23 = Matrix4x4_op_Inequality_m334399549(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
			if (!L_23)
			{
				goto IL_0082;
			}
		}

IL_007b:
		{
			SoftMask_t1817791576 * L_24 = V_0;
			NullCheck(L_24);
			SoftMask_set_hasChanged_m583714446(L_24, (bool)1, /*hidden argument*/NULL);
		}

IL_0082:
		{
			SoftMask_t1817791576 * L_25 = V_0;
			NullCheck(L_25);
			RectTransform_t3704657025 * L_26 = Mask_get_rectTransform_m440704215(L_25, /*hidden argument*/NULL);
			V_6 = L_26;
			RectTransform_t3704657025 * L_27 = V_6;
			NullCheck(L_27);
			bool L_28 = Transform_get_hasChanged_m186929804(L_27, /*hidden argument*/NULL);
			if (!L_28)
			{
				goto IL_00a5;
			}
		}

IL_0096:
		{
			RectTransform_t3704657025 * L_29 = V_6;
			NullCheck(L_29);
			Transform_set_hasChanged_m4213723989(L_29, (bool)0, /*hidden argument*/NULL);
			SoftMask_t1817791576 * L_30 = V_0;
			NullCheck(L_30);
			SoftMask_set_hasChanged_m583714446(L_30, (bool)1, /*hidden argument*/NULL);
		}

IL_00a5:
		{
			bool L_31 = Enumerator_MoveNext_m660175762((Enumerator_t884142899 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m660175762_RuntimeMethod_var);
			if (L_31)
			{
				goto IL_0010;
			}
		}

IL_00b1:
		{
			IL2CPP_LEAVE(0xC4, FINALLY_00b6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00b6;
	}

FINALLY_00b6:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m153542923((Enumerator_t884142899 *)(&V_1), /*hidden argument*/Enumerator_Dispose_m153542923_RuntimeMethod_var);
		IL2CPP_END_FINALLY(182)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(182)
	{
		IL2CPP_JUMP_TBL(0xC4, IL_00c4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00c4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		List_1_t3289866318 * L_32 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_ActiveSoftMasks_17();
		NullCheck(L_32);
		Enumerator_t884142899  L_33 = List_1_GetEnumerator_m3631405139(L_32, /*hidden argument*/List_1_GetEnumerator_m3631405139_RuntimeMethod_var);
		V_8 = L_33;
	}

IL_00d0:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0136;
		}

IL_00d5:
		{
			SoftMask_t1817791576 * L_34 = Enumerator_get_Current_m2347303577((Enumerator_t884142899 *)(&V_8), /*hidden argument*/Enumerator_get_Current_m2347303577_RuntimeMethod_var);
			V_7 = L_34;
			SoftMask_t1817791576 * L_35 = V_7;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
			bool L_36 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
			if (!L_36)
			{
				goto IL_00f6;
			}
		}

IL_00ea:
		{
			SoftMask_t1817791576 * L_37 = V_7;
			NullCheck(L_37);
			bool L_38 = L_37->get__hasChanged_31();
			if (L_38)
			{
				goto IL_00fb;
			}
		}

IL_00f6:
		{
			goto IL_0136;
		}

IL_00fb:
		{
			SoftMask_t1817791576 * L_39 = V_7;
			NullCheck(L_39);
			L_39->set__hasChanged_31((bool)0);
			SoftMask_t1817791576 * L_40 = V_7;
			NullCheck(L_40);
			SoftMask_t1817791576 * L_41 = L_40->get__parent_29();
			IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
			bool L_42 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
			if (L_42)
			{
				goto IL_0136;
			}
		}

IL_0114:
		{
			SoftMask_t1817791576 * L_43 = V_7;
			NullCheck(L_43);
			SoftMask_UpdateMaskTexture_m4144841572(L_43, /*hidden argument*/NULL);
			SoftMask_t1817791576 * L_44 = V_7;
			NullCheck(L_44);
			bool L_45 = L_44->get__hasStencilStateChanged_32();
			if (!L_45)
			{
				goto IL_0136;
			}
		}

IL_0127:
		{
			SoftMask_t1817791576 * L_46 = V_7;
			NullCheck(L_46);
			L_46->set__hasStencilStateChanged_32((bool)0);
			SoftMask_t1817791576 * L_47 = V_7;
			MaskUtilities_NotifyStencilStateChanged_m3969722420(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		}

IL_0136:
		{
			bool L_48 = Enumerator_MoveNext_m660175762((Enumerator_t884142899 *)(&V_8), /*hidden argument*/Enumerator_MoveNext_m660175762_RuntimeMethod_var);
			if (L_48)
			{
				goto IL_00d5;
			}
		}

IL_0142:
		{
			IL2CPP_LEAVE(0x155, FINALLY_0147);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0147;
	}

FINALLY_0147:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m153542923((Enumerator_t884142899 *)(&V_8), /*hidden argument*/Enumerator_Dispose_m153542923_RuntimeMethod_var);
		IL2CPP_END_FINALLY(327)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(327)
	{
		IL2CPP_JUMP_TBL(0x155, IL_0155)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0155:
	{
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::UpdateMaskTexture()
extern "C" IL2CPP_METHOD_ATTR void SoftMask_UpdateMaskTexture_m4144841572 (SoftMask_t1817791576 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_UpdateMaskTexture_m4144841572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	List_1_t3289866318 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	SoftMask_t1817791576 * V_6 = NULL;
	int32_t V_7 = 0;
	Canvas_t3310196443 * V_8 = NULL;
	Camera_t4157153871 * V_9 = NULL;
	Vector3_t3722313464  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Matrix4x4_t1817901843  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Matrix4x4_t1817901843  V_12;
	memset(&V_12, 0, sizeof(V_12));
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	SoftMask_t1817791576 * V_16 = NULL;
	int32_t G_B9_0 = 0;
	Camera_t4157153871 * G_B17_0 = NULL;
	Camera_t4157153871 * G_B16_0 = NULL;
	{
		Graphic_t1660335611 * L_0 = Mask_get_graphic_m2572620787(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Graphic_t1660335611 * L_2 = Mask_get_graphic_m2572620787(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Canvas_t3310196443 * L_3 = Graphic_get_canvas_m3320066409(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0026;
		}
	}

IL_0025:
	{
		return;
	}

IL_0026:
	{
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_6 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_7 = MaskUtilities_FindRootSortOverrideCanvas_m1053047732(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		int32_t L_8 = MaskUtilities_GetStencilDepth_m402474137(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		__this->set__stencilDepth_27(L_8);
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		List_1U5BU5D_t521977531* L_9 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_TmpSoftMasks_9();
		NullCheck(L_9);
		int32_t L_10 = 0;
		List_1_t3289866318 * L_11 = (List_1_t3289866318 *)(L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		List_1_Add_m2471691335(L_11, __this, /*hidden argument*/List_1_Add_m2471691335_RuntimeMethod_var);
		goto IL_00db;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		List_1U5BU5D_t521977531* L_12 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_TmpSoftMasks_9();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		List_1_t3289866318 * L_15 = (List_1_t3289866318 *)(L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m120286467(L_15, /*hidden argument*/List_1_get_Count_m120286467_RuntimeMethod_var);
		V_1 = L_16;
		V_2 = 0;
		goto IL_00d0;
	}

IL_006a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		List_1U5BU5D_t521977531* L_17 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_TmpSoftMasks_9();
		int32_t L_18 = V_0;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		List_1_t3289866318 * L_20 = (List_1_t3289866318 *)(L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		int32_t L_21 = V_2;
		NullCheck(L_20);
		SoftMask_t1817791576 * L_22 = List_1_get_Item_m2136910319(L_20, L_21, /*hidden argument*/List_1_get_Item_m2136910319_RuntimeMethod_var);
		NullCheck(L_22);
		List_1_t3289866318 * L_23 = L_22->get__children_30();
		V_3 = L_23;
		List_1_t3289866318 * L_24 = V_3;
		NullCheck(L_24);
		int32_t L_25 = List_1_get_Count_m120286467(L_24, /*hidden argument*/List_1_get_Count_m120286467_RuntimeMethod_var);
		V_4 = L_25;
		V_5 = 0;
		goto IL_00c3;
	}

IL_008d:
	{
		List_1_t3289866318 * L_26 = V_3;
		int32_t L_27 = V_5;
		NullCheck(L_26);
		SoftMask_t1817791576 * L_28 = List_1_get_Item_m2136910319(L_26, L_27, /*hidden argument*/List_1_get_Item_m2136910319_RuntimeMethod_var);
		V_6 = L_28;
		SoftMask_t1817791576 * L_29 = V_6;
		NullCheck(L_29);
		bool L_30 = L_29->get_m_PartOfParent_14();
		if (!L_30)
		{
			goto IL_00a9;
		}
	}
	{
		int32_t L_31 = V_0;
		G_B9_0 = L_31;
		goto IL_00ac;
	}

IL_00a9:
	{
		int32_t L_32 = V_0;
		G_B9_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)1));
	}

IL_00ac:
	{
		V_7 = G_B9_0;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		List_1U5BU5D_t521977531* L_33 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_TmpSoftMasks_9();
		int32_t L_34 = V_7;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		List_1_t3289866318 * L_36 = (List_1_t3289866318 *)(L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		SoftMask_t1817791576 * L_37 = V_6;
		NullCheck(L_36);
		List_1_Add_m2471691335(L_36, L_37, /*hidden argument*/List_1_Add_m2471691335_RuntimeMethod_var);
		int32_t L_38 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_38, (int32_t)1));
	}

IL_00c3:
	{
		int32_t L_39 = V_5;
		int32_t L_40 = V_4;
		if ((((int32_t)L_39) < ((int32_t)L_40)))
		{
			goto IL_008d;
		}
	}
	{
		int32_t L_41 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_41, (int32_t)1));
	}

IL_00d0:
	{
		int32_t L_42 = V_2;
		int32_t L_43 = V_1;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_006a;
		}
	}
	{
		int32_t L_44 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_44, (int32_t)1));
	}

IL_00db:
	{
		int32_t L_45 = __this->get__stencilDepth_27();
		int32_t L_46 = V_0;
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_45, (int32_t)L_46))) < ((int32_t)3)))
		{
			goto IL_0056;
		}
	}
	{
		CommandBuffer_t2206337031 * L_47 = __this->get__cb_24();
		NullCheck(L_47);
		CommandBuffer_Clear_m3260707516(L_47, /*hidden argument*/NULL);
		CommandBuffer_t2206337031 * L_48 = __this->get__cb_24();
		RenderTexture_t2108887433 * L_49 = SoftMask_get_softMaskBuffer_m514739908(__this, /*hidden argument*/NULL);
		RenderTargetIdentifier_t2079184500  L_50 = RenderTargetIdentifier_op_Implicit_m3327331520(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		NullCheck(L_48);
		CommandBuffer_SetRenderTarget_m2373313166(L_48, L_50, /*hidden argument*/NULL);
		CommandBuffer_t2206337031 * L_51 = __this->get__cb_24();
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		ColorU5BU5D_t941916413* L_52 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_ClearColors_10();
		int32_t L_53 = __this->get__stencilDepth_27();
		NullCheck(L_52);
		NullCheck(L_51);
		CommandBuffer_ClearRenderTarget_m1436931375(L_51, (bool)0, (bool)1, (*(Color_t2555686324 *)((L_52)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_53)))), /*hidden argument*/NULL);
		Graphic_t1660335611 * L_54 = Mask_get_graphic_m2572620787(__this, /*hidden argument*/NULL);
		NullCheck(L_54);
		Canvas_t3310196443 * L_55 = Graphic_get_canvas_m3320066409(L_54, /*hidden argument*/NULL);
		NullCheck(L_55);
		Canvas_t3310196443 * L_56 = Canvas_get_rootCanvas_m3165691493(L_55, /*hidden argument*/NULL);
		V_8 = L_56;
		Canvas_t3310196443 * L_57 = V_8;
		NullCheck(L_57);
		Camera_t4157153871 * L_58 = Canvas_get_worldCamera_m3516267897(L_57, /*hidden argument*/NULL);
		Camera_t4157153871 * L_59 = L_58;
		G_B16_0 = L_59;
		if (L_59)
		{
			G_B17_0 = L_59;
			goto IL_0151;
		}
	}
	{
		Camera_t4157153871 * L_60 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B17_0 = L_60;
	}

IL_0151:
	{
		V_9 = G_B17_0;
		Canvas_t3310196443 * L_61 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_62 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		if (!L_62)
		{
			goto IL_019b;
		}
	}
	{
		Canvas_t3310196443 * L_63 = V_8;
		NullCheck(L_63);
		int32_t L_64 = Canvas_get_renderMode_m841659411(L_63, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_019b;
		}
	}
	{
		Camera_t4157153871 * L_65 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_66 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_019b;
		}
	}
	{
		CommandBuffer_t2206337031 * L_67 = __this->get__cb_24();
		Camera_t4157153871 * L_68 = V_9;
		NullCheck(L_68);
		Matrix4x4_t1817901843  L_69 = Camera_get_worldToCameraMatrix_m22661425(L_68, /*hidden argument*/NULL);
		Camera_t4157153871 * L_70 = V_9;
		NullCheck(L_70);
		Matrix4x4_t1817901843  L_71 = Camera_get_projectionMatrix_m667780853(L_70, /*hidden argument*/NULL);
		Matrix4x4_t1817901843  L_72 = GL_GetGPUProjectionMatrix_m628855021(NULL /*static, unused*/, L_71, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_67);
		CommandBuffer_SetViewProjectionMatrices_m1816539966(L_67, L_69, L_72, /*hidden argument*/NULL);
		goto IL_0236;
	}

IL_019b:
	{
		Canvas_t3310196443 * L_73 = V_8;
		NullCheck(L_73);
		Transform_t3600365921 * L_74 = Component_get_transform_m3162698980(L_73, /*hidden argument*/NULL);
		NullCheck(L_74);
		Vector3_t3722313464  L_75 = Transform_get_position_m36019626(L_74, /*hidden argument*/NULL);
		V_10 = L_75;
		float L_76 = (&V_10)->get_x_2();
		float L_77 = (&V_10)->get_y_3();
		Vector3_t3722313464  L_78;
		memset(&L_78, 0, sizeof(L_78));
		Vector3__ctor_m3353183577((&L_78), ((-L_76)), ((-L_77)), (-1000.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_79 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_80;
		memset(&L_80, 0, sizeof(L_80));
		Vector3__ctor_m3353183577((&L_80), (1.0f), (1.0f), (-1.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Matrix4x4_t1817901843  L_81 = Matrix4x4_TRS_m3801934620(NULL /*static, unused*/, L_78, L_79, L_80, /*hidden argument*/NULL);
		V_11 = L_81;
		Vector3_t3722313464  L_82;
		memset(&L_82, 0, sizeof(L_82));
		Vector3__ctor_m3353183577((&L_82), (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_83 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_84 = (&V_10)->get_x_2();
		float L_85 = (&V_10)->get_y_3();
		Vector3_t3722313464  L_86;
		memset(&L_86, 0, sizeof(L_86));
		Vector3__ctor_m3353183577((&L_86), ((float)((float)(1.0f)/(float)L_84)), ((float)((float)(1.0f)/(float)L_85)), (-0.0002f), /*hidden argument*/NULL);
		Matrix4x4_t1817901843  L_87 = Matrix4x4_TRS_m3801934620(NULL /*static, unused*/, L_82, L_83, L_86, /*hidden argument*/NULL);
		V_12 = L_87;
		CommandBuffer_t2206337031 * L_88 = __this->get__cb_24();
		Matrix4x4_t1817901843  L_89 = V_11;
		Matrix4x4_t1817901843  L_90 = V_12;
		NullCheck(L_88);
		CommandBuffer_SetViewProjectionMatrices_m1816539966(L_88, L_89, L_90, /*hidden argument*/NULL);
	}

IL_0236:
	{
		V_13 = 0;
		goto IL_0332;
	}

IL_023e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		List_1U5BU5D_t521977531* L_91 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_TmpSoftMasks_9();
		int32_t L_92 = V_13;
		NullCheck(L_91);
		int32_t L_93 = L_92;
		List_1_t3289866318 * L_94 = (List_1_t3289866318 *)(L_91)->GetAt(static_cast<il2cpp_array_size_t>(L_93));
		NullCheck(L_94);
		int32_t L_95 = List_1_get_Count_m120286467(L_94, /*hidden argument*/List_1_get_Count_m120286467_RuntimeMethod_var);
		V_14 = L_95;
		V_15 = 0;
		goto IL_0316;
	}

IL_0255:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		List_1U5BU5D_t521977531* L_96 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_TmpSoftMasks_9();
		int32_t L_97 = V_13;
		NullCheck(L_96);
		int32_t L_98 = L_97;
		List_1_t3289866318 * L_99 = (List_1_t3289866318 *)(L_96)->GetAt(static_cast<il2cpp_array_size_t>(L_98));
		int32_t L_100 = V_15;
		NullCheck(L_99);
		SoftMask_t1817791576 * L_101 = List_1_get_Item_m2136910319(L_99, L_100, /*hidden argument*/List_1_get_Item_m2136910319_RuntimeMethod_var);
		V_16 = L_101;
		int32_t L_102 = V_13;
		if (!L_102)
		{
			goto IL_028c;
		}
	}
	{
		SoftMask_t1817791576 * L_103 = V_16;
		SoftMask_t1817791576 * L_104 = V_16;
		NullCheck(L_104);
		Transform_t3600365921 * L_105 = Component_get_transform_m3162698980(L_104, /*hidden argument*/NULL);
		SoftMask_t1817791576 * L_106 = V_16;
		NullCheck(L_106);
		Transform_t3600365921 * L_107 = Component_get_transform_m3162698980(L_106, /*hidden argument*/NULL);
		Transform_t3600365921 * L_108 = MaskUtilities_FindRootSortOverrideCanvas_m1053047732(NULL /*static, unused*/, L_107, /*hidden argument*/NULL);
		int32_t L_109 = MaskUtilities_GetStencilDepth_m402474137(NULL /*static, unused*/, L_105, L_108, /*hidden argument*/NULL);
		NullCheck(L_103);
		L_103->set__stencilDepth_27(L_109);
	}

IL_028c:
	{
		SoftMask_t1817791576 * L_110 = V_16;
		NullCheck(L_110);
		Material_t340375123 * L_111 = SoftMask_get_material_m4230810281(L_110, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		int32_t L_112 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_ColorMaskId_20();
		int32_t L_113 = __this->get__stencilDepth_27();
		int32_t L_114 = V_13;
		NullCheck(L_111);
		Material_SetInt_m475299667(L_111, L_112, ((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)3, (int32_t)L_113)), (int32_t)L_114))&(int32_t)((int32_t)31))))), /*hidden argument*/NULL);
		SoftMask_t1817791576 * L_115 = V_16;
		NullCheck(L_115);
		MaterialPropertyBlock_t3213117958 * L_116 = L_115->get__mpb_23();
		int32_t L_117 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_MainTexId_21();
		SoftMask_t1817791576 * L_118 = V_16;
		NullCheck(L_118);
		Graphic_t1660335611 * L_119 = Mask_get_graphic_m2572620787(L_118, /*hidden argument*/NULL);
		NullCheck(L_119);
		Texture_t3661962703 * L_120 = VirtFuncInvoker0< Texture_t3661962703 * >::Invoke(35 /* UnityEngine.Texture UnityEngine.UI.Graphic::get_mainTexture() */, L_119);
		NullCheck(L_116);
		MaterialPropertyBlock_SetTexture_m3027584768(L_116, L_117, L_120, /*hidden argument*/NULL);
		SoftMask_t1817791576 * L_121 = V_16;
		NullCheck(L_121);
		MaterialPropertyBlock_t3213117958 * L_122 = L_121->get__mpb_23();
		int32_t L_123 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_SoftnessId_22();
		SoftMask_t1817791576 * L_124 = V_16;
		NullCheck(L_124);
		float L_125 = L_124->get_m_Softness_12();
		NullCheck(L_122);
		MaterialPropertyBlock_SetFloat_m3255723079(L_122, L_123, L_125, /*hidden argument*/NULL);
		CommandBuffer_t2206337031 * L_126 = __this->get__cb_24();
		SoftMask_t1817791576 * L_127 = V_16;
		NullCheck(L_127);
		Mesh_t3648964284 * L_128 = SoftMask_get_mesh_m903007999(L_127, /*hidden argument*/NULL);
		SoftMask_t1817791576 * L_129 = V_16;
		NullCheck(L_129);
		Transform_t3600365921 * L_130 = Component_get_transform_m3162698980(L_129, /*hidden argument*/NULL);
		NullCheck(L_130);
		Matrix4x4_t1817901843  L_131 = Transform_get_localToWorldMatrix_m4155710351(L_130, /*hidden argument*/NULL);
		SoftMask_t1817791576 * L_132 = V_16;
		NullCheck(L_132);
		Material_t340375123 * L_133 = SoftMask_get_material_m4230810281(L_132, /*hidden argument*/NULL);
		SoftMask_t1817791576 * L_134 = V_16;
		NullCheck(L_134);
		MaterialPropertyBlock_t3213117958 * L_135 = L_134->get__mpb_23();
		NullCheck(L_126);
		CommandBuffer_DrawMesh_m2147885662(L_126, L_128, L_131, L_133, 0, 0, L_135, /*hidden argument*/NULL);
		int32_t L_136 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_136, (int32_t)1));
	}

IL_0316:
	{
		int32_t L_137 = V_15;
		int32_t L_138 = V_14;
		if ((((int32_t)L_137) < ((int32_t)L_138)))
		{
			goto IL_0255;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		List_1U5BU5D_t521977531* L_139 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_TmpSoftMasks_9();
		int32_t L_140 = V_13;
		NullCheck(L_139);
		int32_t L_141 = L_140;
		List_1_t3289866318 * L_142 = (List_1_t3289866318 *)(L_139)->GetAt(static_cast<il2cpp_array_size_t>(L_141));
		NullCheck(L_142);
		List_1_Clear_m923421119(L_142, /*hidden argument*/List_1_Clear_m923421119_RuntimeMethod_var);
		int32_t L_143 = V_13;
		V_13 = ((int32_t)il2cpp_codegen_add((int32_t)L_143, (int32_t)1));
	}

IL_0332:
	{
		int32_t L_144 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		List_1U5BU5D_t521977531* L_145 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_TmpSoftMasks_9();
		NullCheck(L_145);
		if ((((int32_t)L_144) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_145)->max_length)))))))
		{
			goto IL_023e;
		}
	}
	{
		CommandBuffer_t2206337031 * L_146 = __this->get__cb_24();
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t783367614_il2cpp_TypeInfo_var);
		Graphics_ExecuteCommandBuffer_m581468154(NULL /*static, unused*/, L_146, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::GetDesamplingSize(Coffee.UIExtensions.SoftMask/DesamplingRate,System.Int32&,System.Int32&)
extern "C" IL2CPP_METHOD_ATTR void SoftMask_GetDesamplingSize_m2102729222 (SoftMask_t1817791576 * __this, int32_t ___rate0, int32_t* ___w1, int32_t* ___h2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_GetDesamplingSize_m2102729222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t* L_0 = ___w1;
		int32_t L_1 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		*((int32_t*)L_0) = (int32_t)L_1;
		int32_t* L_2 = ___h2;
		int32_t L_3 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		*((int32_t*)L_2) = (int32_t)L_3;
		int32_t L_4 = ___rate0;
		if (L_4)
		{
			goto IL_0015;
		}
	}
	{
		return;
	}

IL_0015:
	{
		int32_t* L_5 = ___w1;
		int32_t L_6 = *((int32_t*)L_5);
		int32_t* L_7 = ___h2;
		int32_t L_8 = *((int32_t*)L_7);
		V_0 = ((float)((float)(((float)((float)L_6)))/(float)(((float)((float)L_8)))));
		int32_t* L_9 = ___w1;
		int32_t L_10 = *((int32_t*)L_9);
		int32_t* L_11 = ___h2;
		int32_t L_12 = *((int32_t*)L_11);
		if ((((int32_t)L_10) >= ((int32_t)L_12)))
		{
			goto IL_0042;
		}
	}
	{
		int32_t* L_13 = ___h2;
		int32_t* L_14 = ___h2;
		int32_t L_15 = *((int32_t*)L_14);
		int32_t L_16 = ___rate0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_17 = Mathf_ClosestPowerOfTwo_m1106515315(NULL /*static, unused*/, ((int32_t)((int32_t)L_15/(int32_t)L_16)), /*hidden argument*/NULL);
		*((int32_t*)L_13) = (int32_t)L_17;
		int32_t* L_18 = ___w1;
		int32_t* L_19 = ___h2;
		int32_t L_20 = *((int32_t*)L_19);
		float L_21 = V_0;
		int32_t L_22 = Mathf_CeilToInt_m432108984(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)(((float)((float)L_20))), (float)L_21)), /*hidden argument*/NULL);
		*((int32_t*)L_18) = (int32_t)L_22;
		goto IL_0059;
	}

IL_0042:
	{
		int32_t* L_23 = ___w1;
		int32_t* L_24 = ___w1;
		int32_t L_25 = *((int32_t*)L_24);
		int32_t L_26 = ___rate0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_27 = Mathf_ClosestPowerOfTwo_m1106515315(NULL /*static, unused*/, ((int32_t)((int32_t)L_25/(int32_t)L_26)), /*hidden argument*/NULL);
		*((int32_t*)L_23) = (int32_t)L_27;
		int32_t* L_28 = ___h2;
		int32_t* L_29 = ___w1;
		int32_t L_30 = *((int32_t*)L_29);
		float L_31 = V_0;
		int32_t L_32 = Mathf_CeilToInt_m432108984(NULL /*static, unused*/, ((float)((float)(((float)((float)L_30)))/(float)L_31)), /*hidden argument*/NULL);
		*((int32_t*)L_28) = (int32_t)L_32;
	}

IL_0059:
	{
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::ReleaseRT(UnityEngine.RenderTexture&)
extern "C" IL2CPP_METHOD_ATTR void SoftMask_ReleaseRT_m1409116420 (SoftMask_t1817791576 * __this, RenderTexture_t2108887433 ** ___tmpRT0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_ReleaseRT_m1409116420_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RenderTexture_t2108887433 ** L_0 = ___tmpRT0;
		RenderTexture_t2108887433 * L_1 = *((RenderTexture_t2108887433 **)L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		RenderTexture_t2108887433 ** L_3 = ___tmpRT0;
		RenderTexture_t2108887433 * L_4 = *((RenderTexture_t2108887433 **)L_3);
		NullCheck(L_4);
		RenderTexture_Release_m1749927881(L_4, /*hidden argument*/NULL);
		RenderTexture_t2108887433 ** L_5 = ___tmpRT0;
		RenderTexture_t2108887433 * L_6 = *((RenderTexture_t2108887433 **)L_5);
		RenderTexture_ReleaseTemporary_m2400081536(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		RenderTexture_t2108887433 ** L_7 = ___tmpRT0;
		*((RuntimeObject **)L_7) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_7, (RuntimeObject *)NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::ReleaseObject(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void SoftMask_ReleaseObject_m16266906 (SoftMask_t1817791576 * __this, Object_t631007953 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_ReleaseObject_m16266906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object_t631007953 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		Object_t631007953 * L_2 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		___obj0 = (Object_t631007953 *)NULL;
	}

IL_0014:
	{
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::SetParent(Coffee.UIExtensions.SoftMask)
extern "C" IL2CPP_METHOD_ATTR void SoftMask_SetParent_m1377869388 (SoftMask_t1817791576 * __this, SoftMask_t1817791576 * ___newParent0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_SetParent_m1377869388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3289866318 * G_B6_0 = NULL;
	List_1_t3289866318 * G_B5_0 = NULL;
	{
		SoftMask_t1817791576 * L_0 = __this->get__parent_29();
		SoftMask_t1817791576 * L_1 = ___newParent0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_008a;
		}
	}
	{
		SoftMask_t1817791576 * L_3 = ___newParent0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, __this, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_008a;
		}
	}
	{
		SoftMask_t1817791576 * L_5 = __this->get__parent_29();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0083;
		}
	}
	{
		SoftMask_t1817791576 * L_7 = __this->get__parent_29();
		NullCheck(L_7);
		List_1_t3289866318 * L_8 = L_7->get__children_30();
		NullCheck(L_8);
		bool L_9 = List_1_Contains_m2410279580(L_8, __this, /*hidden argument*/List_1_Contains_m2410279580_RuntimeMethod_var);
		if (!L_9)
		{
			goto IL_0083;
		}
	}
	{
		SoftMask_t1817791576 * L_10 = __this->get__parent_29();
		NullCheck(L_10);
		List_1_t3289866318 * L_11 = L_10->get__children_30();
		NullCheck(L_11);
		List_1_Remove_m1687504378(L_11, __this, /*hidden argument*/List_1_Remove_m1687504378_RuntimeMethod_var);
		SoftMask_t1817791576 * L_12 = __this->get__parent_29();
		NullCheck(L_12);
		List_1_t3289866318 * L_13 = L_12->get__children_30();
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		Predicate_1_t2643085700 * L_14 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_35();
		G_B5_0 = L_13;
		if (L_14)
		{
			G_B6_0 = L_13;
			goto IL_0078;
		}
	}
	{
		intptr_t L_15 = (intptr_t)SoftMask_U3CSetParentU3Em__0_m2620602347_RuntimeMethod_var;
		Predicate_1_t2643085700 * L_16 = (Predicate_1_t2643085700 *)il2cpp_codegen_object_new(Predicate_1_t2643085700_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m1967950527(L_16, NULL, (intptr_t)L_15, /*hidden argument*/Predicate_1__ctor_m1967950527_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache0_35(L_16);
		G_B6_0 = G_B5_0;
	}

IL_0078:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		Predicate_1_t2643085700 * L_17 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_35();
		NullCheck(G_B6_0);
		List_1_RemoveAll_m3895672821(G_B6_0, L_17, /*hidden argument*/List_1_RemoveAll_m3895672821_RuntimeMethod_var);
	}

IL_0083:
	{
		SoftMask_t1817791576 * L_18 = ___newParent0;
		__this->set__parent_29(L_18);
	}

IL_008a:
	{
		SoftMask_t1817791576 * L_19 = __this->get__parent_29();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00c1;
		}
	}
	{
		SoftMask_t1817791576 * L_21 = __this->get__parent_29();
		NullCheck(L_21);
		List_1_t3289866318 * L_22 = L_21->get__children_30();
		NullCheck(L_22);
		bool L_23 = List_1_Contains_m2410279580(L_22, __this, /*hidden argument*/List_1_Contains_m2410279580_RuntimeMethod_var);
		if (L_23)
		{
			goto IL_00c1;
		}
	}
	{
		SoftMask_t1817791576 * L_24 = __this->get__parent_29();
		NullCheck(L_24);
		List_1_t3289866318 * L_25 = L_24->get__children_30();
		NullCheck(L_25);
		List_1_Add_m2471691335(L_25, __this, /*hidden argument*/List_1_Add_m2471691335_RuntimeMethod_var);
	}

IL_00c1:
	{
		return;
	}
}
// System.Single Coffee.UIExtensions.SoftMask::GetPixelValue(System.Int32,System.Int32,System.Int32[])
extern "C" IL2CPP_METHOD_ATTR float SoftMask_GetPixelValue_m3927220954 (SoftMask_t1817791576 * __this, int32_t ___x0, int32_t ___y1, Int32U5BU5D_t385246372* ___interactions2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_GetPixelValue_m3927220954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RenderTexture_t2108887433 * V_0 = NULL;
	ByteU5BU5D_t4116647657* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		Texture2D_t3840446185 * L_0 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_ReadTexture_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		Texture2D_t3840446185 * L_2 = (Texture2D_t3840446185 *)il2cpp_codegen_object_new(Texture2D_t3840446185_il2cpp_TypeInfo_var);
		Texture2D__ctor_m2862217990(L_2, 1, 1, 5, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->set_s_ReadTexture_16(L_2);
	}

IL_001d:
	{
		RenderTexture_t2108887433 * L_3 = RenderTexture_get_active_m2427925032(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
		RenderTexture_t2108887433 * L_4 = SoftMask_get_softMaskBuffer_m514739908(__this, /*hidden argument*/NULL);
		RenderTexture_set_active_m1437732586(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t1817791576_il2cpp_TypeInfo_var);
		Texture2D_t3840446185 * L_5 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_ReadTexture_16();
		int32_t L_6 = ___x0;
		int32_t L_7 = ___y1;
		Rect_t2360479859  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Rect__ctor_m2614021312((&L_8), (((float)((float)L_6))), (((float)((float)L_7))), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		Texture2D_ReadPixels_m3395504488(L_5, L_8, 0, 0, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_9 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_ReadTexture_16();
		NullCheck(L_9);
		Texture2D_Apply_m2470606565(L_9, (bool)0, (bool)0, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_10 = V_0;
		RenderTexture_set_active_m1437732586(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_11 = ((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->get_s_ReadTexture_16();
		NullCheck(L_11);
		ByteU5BU5D_t4116647657* L_12 = Texture2D_GetRawTextureData_m2891685427(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		V_2 = 0;
		goto IL_00ae;
	}

IL_0071:
	{
		Int32U5BU5D_t385246372* L_13 = ___interactions2;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)3))%(int32_t)4));
		int32_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_3 = L_16;
		int32_t L_17 = V_3;
		if (!L_17)
		{
			goto IL_008b;
		}
	}
	{
		int32_t L_18 = V_3;
		if ((((int32_t)L_18) == ((int32_t)2)))
		{
			goto IL_0098;
		}
	}
	{
		goto IL_00aa;
	}

IL_008b:
	{
		ByteU5BU5D_t4116647657* L_19 = V_1;
		int32_t L_20 = V_2;
		NullCheck(L_19);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (uint8_t)((int32_t)255));
		goto IL_00aa;
	}

IL_0098:
	{
		ByteU5BU5D_t4116647657* L_21 = V_1;
		int32_t L_22 = V_2;
		ByteU5BU5D_t4116647657* L_23 = V_1;
		int32_t L_24 = V_2;
		NullCheck(L_23);
		int32_t L_25 = L_24;
		uint8_t L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (uint8_t)(((int32_t)((uint8_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)255), (int32_t)L_26))))));
		goto IL_00aa;
	}

IL_00aa:
	{
		int32_t L_27 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_27, (int32_t)1));
	}

IL_00ae:
	{
		int32_t L_28 = V_2;
		if ((((int32_t)L_28) < ((int32_t)4)))
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_29 = __this->get__stencilDepth_27();
		V_4 = L_29;
		int32_t L_30 = V_4;
		switch (L_30)
		{
			case 0:
			{
				goto IL_00d9;
			}
			case 1:
			{
				goto IL_00e4;
			}
			case 2:
			{
				goto IL_00fa;
			}
			case 3:
			{
				goto IL_011b;
			}
		}
	}
	{
		goto IL_0147;
	}

IL_00d9:
	{
		ByteU5BU5D_t4116647657* L_31 = V_1;
		NullCheck(L_31);
		int32_t L_32 = 1;
		uint8_t L_33 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		return ((float)((float)(((float)((float)L_33)))/(float)(255.0f)));
	}

IL_00e4:
	{
		ByteU5BU5D_t4116647657* L_34 = V_1;
		NullCheck(L_34);
		int32_t L_35 = 1;
		uint8_t L_36 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		ByteU5BU5D_t4116647657* L_37 = V_1;
		NullCheck(L_37);
		int32_t L_38 = 2;
		uint8_t L_39 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		return ((float)il2cpp_codegen_multiply((float)((float)((float)(((float)((float)L_36)))/(float)(255.0f))), (float)((float)((float)(((float)((float)L_39)))/(float)(255.0f)))));
	}

IL_00fa:
	{
		ByteU5BU5D_t4116647657* L_40 = V_1;
		NullCheck(L_40);
		int32_t L_41 = 1;
		uint8_t L_42 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		ByteU5BU5D_t4116647657* L_43 = V_1;
		NullCheck(L_43);
		int32_t L_44 = 2;
		uint8_t L_45 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		ByteU5BU5D_t4116647657* L_46 = V_1;
		NullCheck(L_46);
		int32_t L_47 = 3;
		uint8_t L_48 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		return ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((float)((float)(((float)((float)L_42)))/(float)(255.0f))), (float)((float)((float)(((float)((float)L_45)))/(float)(255.0f))))), (float)((float)((float)(((float)((float)L_48)))/(float)(255.0f)))));
	}

IL_011b:
	{
		ByteU5BU5D_t4116647657* L_49 = V_1;
		NullCheck(L_49);
		int32_t L_50 = 1;
		uint8_t L_51 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		ByteU5BU5D_t4116647657* L_52 = V_1;
		NullCheck(L_52);
		int32_t L_53 = 2;
		uint8_t L_54 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_53));
		ByteU5BU5D_t4116647657* L_55 = V_1;
		NullCheck(L_55);
		int32_t L_56 = 3;
		uint8_t L_57 = (L_55)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		ByteU5BU5D_t4116647657* L_58 = V_1;
		NullCheck(L_58);
		int32_t L_59 = 0;
		uint8_t L_60 = (L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_59));
		return ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((float)((float)(((float)((float)L_51)))/(float)(255.0f))), (float)((float)((float)(((float)((float)L_54)))/(float)(255.0f))))), (float)((float)((float)(((float)((float)L_57)))/(float)(255.0f))))), (float)((float)((float)(((float)((float)L_60)))/(float)(255.0f)))));
	}

IL_0147:
	{
		return (0.0f);
	}
}
// System.Void Coffee.UIExtensions.SoftMask::.cctor()
extern "C" IL2CPP_METHOD_ATTR void SoftMask__cctor_m1781826879 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask__cctor_m1781826879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1U5BU5D_t521977531* L_0 = (List_1U5BU5D_t521977531*)SZArrayNew(List_1U5BU5D_t521977531_il2cpp_TypeInfo_var, (uint32_t)4);
		List_1U5BU5D_t521977531* L_1 = L_0;
		List_1_t3289866318 * L_2 = (List_1_t3289866318 *)il2cpp_codegen_object_new(List_1_t3289866318_il2cpp_TypeInfo_var);
		List_1__ctor_m3675240295(L_2, /*hidden argument*/List_1__ctor_m3675240295_RuntimeMethod_var);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (List_1_t3289866318 *)L_2);
		List_1U5BU5D_t521977531* L_3 = L_1;
		List_1_t3289866318 * L_4 = (List_1_t3289866318 *)il2cpp_codegen_object_new(List_1_t3289866318_il2cpp_TypeInfo_var);
		List_1__ctor_m3675240295(L_4, /*hidden argument*/List_1__ctor_m3675240295_RuntimeMethod_var);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (List_1_t3289866318 *)L_4);
		List_1U5BU5D_t521977531* L_5 = L_3;
		List_1_t3289866318 * L_6 = (List_1_t3289866318 *)il2cpp_codegen_object_new(List_1_t3289866318_il2cpp_TypeInfo_var);
		List_1__ctor_m3675240295(L_6, /*hidden argument*/List_1__ctor_m3675240295_RuntimeMethod_var);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (List_1_t3289866318 *)L_6);
		List_1U5BU5D_t521977531* L_7 = L_5;
		List_1_t3289866318 * L_8 = (List_1_t3289866318 *)il2cpp_codegen_object_new(List_1_t3289866318_il2cpp_TypeInfo_var);
		List_1__ctor_m3675240295(L_8, /*hidden argument*/List_1__ctor_m3675240295_RuntimeMethod_var);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (List_1_t3289866318 *)L_8);
		((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->set_s_TmpSoftMasks_9(L_7);
		ColorU5BU5D_t941916413* L_9 = (ColorU5BU5D_t941916413*)SZArrayNew(ColorU5BU5D_t941916413_il2cpp_TypeInfo_var, (uint32_t)4);
		ColorU5BU5D_t941916413* L_10 = L_9;
		NullCheck(L_10);
		Color_t2555686324  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Color__ctor_m2943235014((&L_11), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		*(Color_t2555686324 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_11;
		ColorU5BU5D_t941916413* L_12 = L_10;
		NullCheck(L_12);
		Color_t2555686324  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Color__ctor_m2943235014((&L_13), (1.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		*(Color_t2555686324 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_13;
		ColorU5BU5D_t941916413* L_14 = L_12;
		NullCheck(L_14);
		Color_t2555686324  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Color__ctor_m2943235014((&L_15), (1.0f), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		*(Color_t2555686324 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_15;
		ColorU5BU5D_t941916413* L_16 = L_14;
		NullCheck(L_16);
		Color_t2555686324  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Color__ctor_m2943235014((&L_17), (1.0f), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		*(Color_t2555686324 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))) = L_17;
		((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->set_s_ClearColors_10(L_16);
		List_1_t3289866318 * L_18 = (List_1_t3289866318 *)il2cpp_codegen_object_new(List_1_t3289866318_il2cpp_TypeInfo_var);
		List_1__ctor_m3675240295(L_18, /*hidden argument*/List_1__ctor_m3675240295_RuntimeMethod_var);
		((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->set_s_ActiveSoftMasks_17(L_18);
		List_1_t3289866318 * L_19 = (List_1_t3289866318 *)il2cpp_codegen_object_new(List_1_t3289866318_il2cpp_TypeInfo_var);
		List_1__ctor_m3675240295(L_19, /*hidden argument*/List_1__ctor_m3675240295_RuntimeMethod_var);
		((SoftMask_t1817791576_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t1817791576_il2cpp_TypeInfo_var))->set_s_TempRelatables_18(L_19);
		return;
	}
}
// System.Boolean Coffee.UIExtensions.SoftMask::<SetParent>m__0(Coffee.UIExtensions.SoftMask)
extern "C" IL2CPP_METHOD_ATTR bool SoftMask_U3CSetParentU3Em__0_m2620602347 (RuntimeObject * __this /* static, unused */, SoftMask_t1817791576 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMask_U3CSetParentU3Em__0_m2620602347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SoftMask_t1817791576 * L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Coffee.UIExtensions.SoftMaskable::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SoftMaskable__ctor_m2397673674 (SoftMaskable_t353105572 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_MaskInteraction_7(((int32_t)85));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material Coffee.UIExtensions.SoftMaskable::GetModifiedMaterial(UnityEngine.Material)
extern "C" IL2CPP_METHOD_ATTR Material_t340375123 * SoftMaskable_GetModifiedMaterial_m1938042998 (SoftMaskable_t353105572 * __this, Material_t340375123 * ___baseMaterial0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMaskable_GetModifiedMaterial_m1938042998_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3600365921 * V_0 = NULL;
	SoftMask_t1817791576 * V_1 = NULL;
	Material_t340375123 * V_2 = NULL;
	int32_t G_B11_0 = 0;
	Material_t340375123 * G_B11_1 = NULL;
	int32_t G_B10_0 = 0;
	Material_t340375123 * G_B10_1 = NULL;
	int32_t G_B12_0 = 0;
	int32_t G_B12_1 = 0;
	Material_t340375123 * G_B12_2 = NULL;
	{
		__this->set__softMask_10((SoftMask_t1817791576 *)NULL);
		bool L_0 = Behaviour_get_isActiveAndEnabled_m3143666263(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Material_t340375123 * L_1 = ___baseMaterial0;
		return L_1;
	}

IL_0014:
	{
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Transform_get_parent_m835071599(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0055;
	}

IL_0025:
	{
		Transform_t3600365921 * L_4 = V_0;
		NullCheck(L_4);
		SoftMask_t1817791576 * L_5 = Component_GetComponent_TisSoftMask_t1817791576_m4226683470(L_4, /*hidden argument*/Component_GetComponent_TisSoftMask_t1817791576_m4226683470_RuntimeMethod_var);
		V_1 = L_5;
		SoftMask_t1817791576 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		SoftMask_t1817791576 * L_8 = V_1;
		NullCheck(L_8);
		bool L_9 = Behaviour_get_enabled_m753527255(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004e;
		}
	}
	{
		SoftMask_t1817791576 * L_10 = V_1;
		__this->set__softMask_10(L_10);
		goto IL_0060;
	}

IL_004e:
	{
		Transform_t3600365921 * L_11 = V_0;
		NullCheck(L_11);
		Transform_t3600365921 * L_12 = Transform_get_parent_m835071599(L_11, /*hidden argument*/NULL);
		V_0 = L_12;
	}

IL_0055:
	{
		Transform_t3600365921 * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0025;
		}
	}

IL_0060:
	{
		Material_t340375123 * L_15 = ___baseMaterial0;
		V_2 = L_15;
		SoftMask_t1817791576 * L_16 = __this->get__softMask_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_010c;
		}
	}
	{
		Material_t340375123 * L_18 = ___baseMaterial0;
		Material_t340375123 * L_19 = (Material_t340375123 *)il2cpp_codegen_object_new(Material_t340375123_il2cpp_TypeInfo_var);
		Material__ctor_m249231841(L_19, L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		Material_t340375123 * L_20 = V_2;
		NullCheck(L_20);
		Object_set_hideFlags_m1648752846(L_20, ((int32_t)61), /*hidden argument*/NULL);
		Material_t340375123 * L_21 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_t353105572_il2cpp_TypeInfo_var);
		int32_t L_22 = ((SoftMaskable_t353105572_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_t353105572_il2cpp_TypeInfo_var))->get_s_SoftMaskTexId_12();
		SoftMask_t1817791576 * L_23 = __this->get__softMask_10();
		NullCheck(L_23);
		RenderTexture_t2108887433 * L_24 = SoftMask_get_softMaskBuffer_m514739908(L_23, /*hidden argument*/NULL);
		NullCheck(L_21);
		Material_SetTexture_m3009528825(L_21, L_22, L_24, /*hidden argument*/NULL);
		Material_t340375123 * L_25 = V_2;
		int32_t L_26 = ((SoftMaskable_t353105572_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_t353105572_il2cpp_TypeInfo_var))->get_s_StencilCompId_13();
		bool L_27 = __this->get_m_UseStencil_8();
		G_B10_0 = L_26;
		G_B10_1 = L_25;
		if (!L_27)
		{
			G_B11_0 = L_26;
			G_B11_1 = L_25;
			goto IL_00ae;
		}
	}
	{
		G_B12_0 = 3;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		goto IL_00af;
	}

IL_00ae:
	{
		G_B12_0 = 8;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
	}

IL_00af:
	{
		NullCheck(G_B12_2);
		Material_SetInt_m475299667(G_B12_2, G_B12_1, G_B12_0, /*hidden argument*/NULL);
		Material_t340375123 * L_28 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_t353105572_il2cpp_TypeInfo_var);
		int32_t L_29 = ((SoftMaskable_t353105572_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_t353105572_il2cpp_TypeInfo_var))->get_s_MaskInteractionId_14();
		int32_t L_30 = __this->get_m_MaskInteraction_7();
		int32_t L_31 = __this->get_m_MaskInteraction_7();
		int32_t L_32 = __this->get_m_MaskInteraction_7();
		int32_t L_33 = __this->get_m_MaskInteraction_7();
		Vector4_t3319028937  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector4__ctor_m2498754347((&L_34), (((float)((float)((int32_t)((int32_t)L_30&(int32_t)3))))), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_31>>(int32_t)2))&(int32_t)3))))), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_32>>(int32_t)4))&(int32_t)3))))), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_33>>(int32_t)6))&(int32_t)3))))), /*hidden argument*/NULL);
		NullCheck(L_28);
		Material_SetVector_m2633010038(L_28, L_29, L_34, /*hidden argument*/NULL);
		Material_t340375123 * L_35 = ___baseMaterial0;
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t3850132571_il2cpp_TypeInfo_var);
		StencilMaterial_Remove_m1301487727(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		Material_t340375123 ** L_36 = __this->get_address_of__maskMaterial_11();
		SoftMaskable_ReleaseMaterial_m86784683(__this, (Material_t340375123 **)L_36, /*hidden argument*/NULL);
		Material_t340375123 * L_37 = V_2;
		__this->set__maskMaterial_11(L_37);
		goto IL_011c;
	}

IL_010c:
	{
		Material_t340375123 * L_38 = ___baseMaterial0;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_t353105572_il2cpp_TypeInfo_var);
		int32_t L_39 = ((SoftMaskable_t353105572_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_t353105572_il2cpp_TypeInfo_var))->get_s_SoftMaskTexId_12();
		Texture2D_t3840446185 * L_40 = Texture2D_get_whiteTexture_m2176011403(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_38);
		Material_SetTexture_m3009528825(L_38, L_39, L_40, /*hidden argument*/NULL);
	}

IL_011c:
	{
		Material_t340375123 * L_41 = V_2;
		return L_41;
	}
}
// System.Boolean Coffee.UIExtensions.SoftMaskable::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern "C" IL2CPP_METHOD_ATTR bool SoftMaskable_IsRaycastLocationValid_m3671775487 (SoftMaskable_t353105572 * __this, Vector2_t2156229523  ___sp0, Camera_t4157153871 * ___eventCamera1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMaskable_IsRaycastLocationValid_m3671775487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SoftMask_t1817791576 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B8_0 = 0;
	Int32U5BU5D_t385246372* G_B8_1 = NULL;
	int32_t G_B7_0 = 0;
	Int32U5BU5D_t385246372* G_B7_1 = NULL;
	int32_t G_B9_0 = 0;
	int32_t G_B9_1 = 0;
	Int32U5BU5D_t385246372* G_B9_2 = NULL;
	SoftMask_t1817791576 * G_B12_0 = NULL;
	{
		bool L_0 = Behaviour_get_isActiveAndEnabled_m3143666263(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		SoftMask_t1817791576 * L_1 = __this->get__softMask_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001d;
		}
	}

IL_001b:
	{
		return (bool)1;
	}

IL_001d:
	{
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_4 = ___sp0;
		Camera_t4157153871 * L_5 = ___eventCamera1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		bool L_6 = RectTransformUtility_RectangleContainsScreenPoint_m4031431712(NULL /*static, unused*/, ((RectTransform_t3704657025 *)IsInstSealed((RuntimeObject*)L_3, RectTransform_t3704657025_il2cpp_TypeInfo_var)), L_4, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0036;
		}
	}
	{
		return (bool)0;
	}

IL_0036:
	{
		SoftMask_t1817791576 * L_7 = __this->get__softMask_10();
		V_0 = L_7;
		V_1 = 0;
		goto IL_0087;
	}

IL_0044:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_t353105572_il2cpp_TypeInfo_var);
		Int32U5BU5D_t385246372* L_8 = ((SoftMaskable_t353105572_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_t353105572_il2cpp_TypeInfo_var))->get_s_Interactions_18();
		int32_t L_9 = V_1;
		SoftMask_t1817791576 * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		G_B7_0 = L_9;
		G_B7_1 = L_8;
		if (!L_11)
		{
			G_B8_0 = L_9;
			G_B8_1 = L_8;
			goto IL_0069;
		}
	}
	{
		int32_t L_12 = __this->get_m_MaskInteraction_7();
		int32_t L_13 = V_1;
		G_B9_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_12>>(int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_13, (int32_t)2))&(int32_t)((int32_t)31)))))&(int32_t)3));
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		goto IL_006a;
	}

IL_0069:
	{
		G_B9_0 = 0;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
	}

IL_006a:
	{
		NullCheck(G_B9_2);
		(G_B9_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B9_1), (int32_t)G_B9_0);
		SoftMask_t1817791576 * L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0081;
		}
	}
	{
		SoftMask_t1817791576 * L_16 = V_0;
		NullCheck(L_16);
		SoftMask_t1817791576 * L_17 = SoftMask_get_parent_m3860326613(L_16, /*hidden argument*/NULL);
		G_B12_0 = L_17;
		goto IL_0082;
	}

IL_0081:
	{
		G_B12_0 = ((SoftMask_t1817791576 *)(NULL));
	}

IL_0082:
	{
		V_0 = G_B12_0;
		int32_t L_18 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_0087:
	{
		int32_t L_19 = V_1;
		if ((((int32_t)L_19) < ((int32_t)4)))
		{
			goto IL_0044;
		}
	}
	{
		SoftMask_t1817791576 * L_20 = __this->get__softMask_10();
		Vector2_t2156229523  L_21 = ___sp0;
		Camera_t4157153871 * L_22 = ___eventCamera1;
		Graphic_t1660335611 * L_23 = SoftMaskable_get_graphic_m2211451753(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_t353105572_il2cpp_TypeInfo_var);
		Int32U5BU5D_t385246372* L_24 = ((SoftMaskable_t353105572_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_t353105572_il2cpp_TypeInfo_var))->get_s_Interactions_18();
		NullCheck(L_20);
		bool L_25 = SoftMask_IsRaycastLocationValid_m4269246139(L_20, L_21, L_22, L_23, L_24, /*hidden argument*/NULL);
		return L_25;
	}
}
// System.Boolean Coffee.UIExtensions.SoftMaskable::get_inverse()
extern "C" IL2CPP_METHOD_ATTR bool SoftMaskable_get_inverse_m3490011408 (SoftMaskable_t353105572 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_MaskInteraction_7();
		return (bool)((((int32_t)L_0) == ((int32_t)((int32_t)170)))? 1 : 0);
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::set_inverse(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void SoftMaskable_set_inverse_m2059752761 (SoftMaskable_t353105572 * __this, bool ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		G_B3_0 = ((int32_t)170);
		goto IL_0012;
	}

IL_0010:
	{
		G_B3_0 = ((int32_t)85);
	}

IL_0012:
	{
		V_0 = G_B3_0;
		int32_t L_1 = __this->get_m_MaskInteraction_7();
		int32_t L_2 = V_0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_3 = V_0;
		__this->set_m_MaskInteraction_7(L_3);
		Graphic_t1660335611 * L_4 = SoftMaskable_get_graphic_m2211451753(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(29 /* System.Void UnityEngine.UI.Graphic::SetMaterialDirty() */, L_4);
	}

IL_0031:
	{
		return;
	}
}
// UnityEngine.UI.Graphic Coffee.UIExtensions.SoftMaskable::get_graphic()
extern "C" IL2CPP_METHOD_ATTR Graphic_t1660335611 * SoftMaskable_get_graphic_m2211451753 (SoftMaskable_t353105572 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMaskable_get_graphic_m2211451753_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Graphic_t1660335611 * V_0 = NULL;
	Graphic_t1660335611 * G_B3_0 = NULL;
	{
		Graphic_t1660335611 * L_0 = __this->get__graphic_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Graphic_t1660335611 * L_2 = __this->get__graphic_9();
		G_B3_0 = L_2;
		goto IL_002a;
	}

IL_001b:
	{
		Graphic_t1660335611 * L_3 = Component_GetComponent_TisGraphic_t1660335611_m1118939870(__this, /*hidden argument*/Component_GetComponent_TisGraphic_t1660335611_m1118939870_RuntimeMethod_var);
		Graphic_t1660335611 * L_4 = L_3;
		V_0 = L_4;
		__this->set__graphic_9(L_4);
		Graphic_t1660335611 * L_5 = V_0;
		G_B3_0 = L_5;
	}

IL_002a:
	{
		return G_B3_0;
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::SetMaskInteraction(UnityEngine.SpriteMaskInteraction)
extern "C" IL2CPP_METHOD_ATTR void SoftMaskable_SetMaskInteraction_m1818791856 (SoftMaskable_t353105572 * __this, int32_t ___intr0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___intr0;
		int32_t L_1 = ___intr0;
		int32_t L_2 = ___intr0;
		int32_t L_3 = ___intr0;
		SoftMaskable_SetMaskInteraction_m4089080(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::SetMaskInteraction(UnityEngine.SpriteMaskInteraction,UnityEngine.SpriteMaskInteraction,UnityEngine.SpriteMaskInteraction,UnityEngine.SpriteMaskInteraction)
extern "C" IL2CPP_METHOD_ATTR void SoftMaskable_SetMaskInteraction_m4089080 (SoftMaskable_t353105572 * __this, int32_t ___layer00, int32_t ___layer11, int32_t ___layer22, int32_t ___layer33, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMaskable_SetMaskInteraction_m4089080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___layer00;
		int32_t L_1 = ___layer11;
		int32_t L_2 = ___layer22;
		int32_t L_3 = ___layer33;
		__this->set_m_MaskInteraction_7(((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)((int32_t)((int32_t)L_1<<(int32_t)2)))), (int32_t)((int32_t)((int32_t)L_2<<(int32_t)4)))), (int32_t)((int32_t)((int32_t)L_3<<(int32_t)6)))));
		Graphic_t1660335611 * L_4 = SoftMaskable_get_graphic_m2211451753(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		Graphic_t1660335611 * L_6 = SoftMaskable_get_graphic_m2211451753(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(29 /* System.Void UnityEngine.UI.Graphic::SetMaterialDirty() */, L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void SoftMaskable_OnEnable_m2016091490 (SoftMaskable_t353105572 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMaskable_OnEnable_m2016091490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Graphic_t1660335611 * V_0 = NULL;
	Material_t340375123 * V_1 = NULL;
	Material_t340375123 * G_B7_0 = NULL;
	Graphic_t1660335611 * G_B7_1 = NULL;
	Material_t340375123 * G_B6_0 = NULL;
	Graphic_t1660335611 * G_B6_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_t353105572_il2cpp_TypeInfo_var);
		List_1_t1825180314 * L_0 = ((SoftMaskable_t353105572_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_t353105572_il2cpp_TypeInfo_var))->get_s_ActiveSoftMaskables_17();
		if (L_0)
		{
			goto IL_0041;
		}
	}
	{
		List_1_t1825180314 * L_1 = (List_1_t1825180314 *)il2cpp_codegen_object_new(List_1_t1825180314_il2cpp_TypeInfo_var);
		List_1__ctor_m731543265(L_1, /*hidden argument*/List_1__ctor_m731543265_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_t353105572_il2cpp_TypeInfo_var);
		((SoftMaskable_t353105572_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_t353105572_il2cpp_TypeInfo_var))->set_s_ActiveSoftMaskables_17(L_1);
		int32_t L_2 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral1499807536, /*hidden argument*/NULL);
		((SoftMaskable_t353105572_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_t353105572_il2cpp_TypeInfo_var))->set_s_SoftMaskTexId_12(L_2);
		int32_t L_3 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral2160052589, /*hidden argument*/NULL);
		((SoftMaskable_t353105572_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_t353105572_il2cpp_TypeInfo_var))->set_s_StencilCompId_13(L_3);
		int32_t L_4 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral489978276, /*hidden argument*/NULL);
		((SoftMaskable_t353105572_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_t353105572_il2cpp_TypeInfo_var))->set_s_MaskInteractionId_14(L_4);
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_t353105572_il2cpp_TypeInfo_var);
		List_1_t1825180314 * L_5 = ((SoftMaskable_t353105572_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_t353105572_il2cpp_TypeInfo_var))->get_s_ActiveSoftMaskables_17();
		NullCheck(L_5);
		List_1_Add_m589637811(L_5, __this, /*hidden argument*/List_1_Add_m589637811_RuntimeMethod_var);
		Graphic_t1660335611 * L_6 = SoftMaskable_get_graphic_m2211451753(__this, /*hidden argument*/NULL);
		V_0 = L_6;
		Graphic_t1660335611 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00ba;
		}
	}
	{
		Graphic_t1660335611 * L_9 = V_0;
		NullCheck(L_9);
		Material_t340375123 * L_10 = VirtFuncInvoker0< Material_t340375123 * >::Invoke(32 /* UnityEngine.Material UnityEngine.UI.Graphic::get_material() */, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0083;
		}
	}
	{
		Graphic_t1660335611 * L_12 = V_0;
		NullCheck(L_12);
		Material_t340375123 * L_13 = VirtFuncInvoker0< Material_t340375123 * >::Invoke(32 /* UnityEngine.Material UnityEngine.UI.Graphic::get_material() */, L_12);
		IL2CPP_RUNTIME_CLASS_INIT(Graphic_t1660335611_il2cpp_TypeInfo_var);
		Material_t340375123 * L_14 = Graphic_get_defaultGraphicMaterial_m3107284931(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00b4;
		}
	}

IL_0083:
	{
		Graphic_t1660335611 * L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_t353105572_il2cpp_TypeInfo_var);
		Material_t340375123 * L_17 = ((SoftMaskable_t353105572_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_t353105572_il2cpp_TypeInfo_var))->get_s_DefaultMaterial_19();
		Material_t340375123 * L_18 = L_17;
		G_B6_0 = L_18;
		G_B6_1 = L_16;
		if (L_18)
		{
			G_B7_0 = L_18;
			G_B7_1 = L_16;
			goto IL_00af;
		}
	}
	{
		Shader_t4151988712 * L_19 = Resources_Load_TisShader_t4151988712_m741163411(NULL /*static, unused*/, _stringLiteral120066430, /*hidden argument*/Resources_Load_TisShader_t4151988712_m741163411_RuntimeMethod_var);
		Material_t340375123 * L_20 = (Material_t340375123 *)il2cpp_codegen_object_new(Material_t340375123_il2cpp_TypeInfo_var);
		Material__ctor_m1662457592(L_20, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		Material_t340375123 * L_21 = V_1;
		NullCheck(L_21);
		Object_set_hideFlags_m1648752846(L_21, ((int32_t)61), /*hidden argument*/NULL);
		Material_t340375123 * L_22 = V_1;
		Material_t340375123 * L_23 = L_22;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_t353105572_il2cpp_TypeInfo_var);
		((SoftMaskable_t353105572_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_t353105572_il2cpp_TypeInfo_var))->set_s_DefaultMaterial_19(L_23);
		G_B7_0 = L_23;
		G_B7_1 = G_B6_1;
	}

IL_00af:
	{
		NullCheck(G_B7_1);
		VirtActionInvoker1< Material_t340375123 * >::Invoke(33 /* System.Void UnityEngine.UI.Graphic::set_material(UnityEngine.Material) */, G_B7_1, G_B7_0);
	}

IL_00b4:
	{
		Graphic_t1660335611 * L_24 = V_0;
		NullCheck(L_24);
		VirtActionInvoker0::Invoke(29 /* System.Void UnityEngine.UI.Graphic::SetMaterialDirty() */, L_24);
	}

IL_00ba:
	{
		__this->set__softMask_10((SoftMask_t1817791576 *)NULL);
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void SoftMaskable_OnDisable_m3111229774 (SoftMaskable_t353105572 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMaskable_OnDisable_m3111229774_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Graphic_t1660335611 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_t353105572_il2cpp_TypeInfo_var);
		List_1_t1825180314 * L_0 = ((SoftMaskable_t353105572_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_t353105572_il2cpp_TypeInfo_var))->get_s_ActiveSoftMaskables_17();
		NullCheck(L_0);
		List_1_Remove_m616773178(L_0, __this, /*hidden argument*/List_1_Remove_m616773178_RuntimeMethod_var);
		Graphic_t1660335611 * L_1 = SoftMaskable_get_graphic_m2211451753(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		Graphic_t1660335611 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0040;
		}
	}
	{
		Graphic_t1660335611 * L_4 = V_0;
		NullCheck(L_4);
		Material_t340375123 * L_5 = VirtFuncInvoker0< Material_t340375123 * >::Invoke(32 /* UnityEngine.Material UnityEngine.UI.Graphic::get_material() */, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_t353105572_il2cpp_TypeInfo_var);
		Material_t340375123 * L_6 = ((SoftMaskable_t353105572_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_t353105572_il2cpp_TypeInfo_var))->get_s_DefaultMaterial_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		Graphic_t1660335611 * L_8 = V_0;
		NullCheck(L_8);
		VirtActionInvoker1< Material_t340375123 * >::Invoke(33 /* System.Void UnityEngine.UI.Graphic::set_material(UnityEngine.Material) */, L_8, (Material_t340375123 *)NULL);
	}

IL_003a:
	{
		Graphic_t1660335611 * L_9 = V_0;
		NullCheck(L_9);
		VirtActionInvoker0::Invoke(29 /* System.Void UnityEngine.UI.Graphic::SetMaterialDirty() */, L_9);
	}

IL_0040:
	{
		Material_t340375123 ** L_10 = __this->get_address_of__maskMaterial_11();
		SoftMaskable_ReleaseMaterial_m86784683(__this, (Material_t340375123 **)L_10, /*hidden argument*/NULL);
		__this->set__softMask_10((SoftMask_t1817791576 *)NULL);
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::ReleaseMaterial(UnityEngine.Material&)
extern "C" IL2CPP_METHOD_ATTR void SoftMaskable_ReleaseMaterial_m86784683 (SoftMaskable_t353105572 * __this, Material_t340375123 ** ___mat0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMaskable_ReleaseMaterial_m86784683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t340375123 ** L_0 = ___mat0;
		Material_t340375123 * L_1 = *((Material_t340375123 **)L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		Material_t340375123 ** L_3 = ___mat0;
		Material_t340375123 * L_4 = *((Material_t340375123 **)L_3);
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t3850132571_il2cpp_TypeInfo_var);
		StencilMaterial_Remove_m1301487727(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Material_t340375123 ** L_5 = ___mat0;
		Material_t340375123 * L_6 = *((Material_t340375123 **)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Material_t340375123 ** L_7 = ___mat0;
		*((RuntimeObject **)L_7) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_7, (RuntimeObject *)NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern "C" IL2CPP_METHOD_ATTR void SoftMaskable_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1951694638 (SoftMaskable_t353105572 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern "C" IL2CPP_METHOD_ATTR void SoftMaskable_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2549518519 (SoftMaskable_t353105572 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_Inverse_6();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		__this->set_m_Inverse_6((bool)0);
		__this->set_m_MaskInteraction_7(((int32_t)170));
	}

IL_001d:
	{
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::.cctor()
extern "C" IL2CPP_METHOD_ATTR void SoftMaskable__cctor_m3931341446 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftMaskable__cctor_m3931341446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t385246372* L_0 = (Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)4);
		((SoftMaskable_t353105572_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_t353105572_il2cpp_TypeInfo_var))->set_s_Interactions_18(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
