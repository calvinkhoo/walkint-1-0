﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct InterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// DigitsNFCToolkit.AbsoluteUriRecord
struct AbsoluteUriRecord_t2525168784;
// DigitsNFCToolkit.EmptyRecord
struct EmptyRecord_t1486430273;
// DigitsNFCToolkit.ExternalTypeRecord
struct ExternalTypeRecord_t4087466745;
// DigitsNFCToolkit.IOSNFC
struct IOSNFC_t293874181;
// DigitsNFCToolkit.JSON.JSONArray
struct JSONArray_t4024675823;
// DigitsNFCToolkit.JSON.JSONObject
struct JSONObject_t321714843;
// DigitsNFCToolkit.JSON.JSONValue
struct JSONValue_t4275860644;
// DigitsNFCToolkit.JSON.JSONValue[]
struct JSONValueU5BU5D_t1620021325;
// DigitsNFCToolkit.MimeMediaRecord
struct MimeMediaRecord_t736820488;
// DigitsNFCToolkit.MimeMediaRecord[]
struct MimeMediaRecordU5BU5D_t1073055193;
// DigitsNFCToolkit.NDEFMessage
struct NDEFMessage_t279637043;
// DigitsNFCToolkit.NDEFPushResult
struct NDEFPushResult_t3422827153;
// DigitsNFCToolkit.NDEFReadResult
struct NDEFReadResult_t3483243621;
// DigitsNFCToolkit.NDEFRecord
struct NDEFRecord_t2690545556;
// DigitsNFCToolkit.NDEFRecord[]
struct NDEFRecordU5BU5D_t4010013597;
// DigitsNFCToolkit.NDEFWriteResult
struct NDEFWriteResult_t4210562629;
// DigitsNFCToolkit.NFCTag
struct NFCTag_t2820711232;
// DigitsNFCToolkit.NFCTechnology[]
struct NFCTechnologyU5BU5D_t328493574;
// DigitsNFCToolkit.NativeNFC
struct NativeNFC_t1941597496;
// DigitsNFCToolkit.NativeNFCManager
struct NativeNFCManager_t351225459;
// DigitsNFCToolkit.OnNDEFPushFinished
struct OnNDEFPushFinished_t4279917764;
// DigitsNFCToolkit.OnNDEFReadFinished
struct OnNDEFReadFinished_t1327886840;
// DigitsNFCToolkit.OnNDEFWriteFinished
struct OnNDEFWriteFinished_t4102039599;
// DigitsNFCToolkit.OnNFCTagDetected
struct OnNFCTagDetected_t3189675727;
// DigitsNFCToolkit.Samples.ImageRecordItem
struct ImageRecordItem_t2104316047;
// DigitsNFCToolkit.Samples.MessageScreenView
struct MessageScreenView_t146641597;
// DigitsNFCToolkit.Samples.NavigationManager
struct NavigationManager_t1939391727;
// DigitsNFCToolkit.Samples.ReadScreenControl
struct ReadScreenControl_t3483866810;
// DigitsNFCToolkit.Samples.ReadScreenView
struct ReadScreenView_t239900869;
// DigitsNFCToolkit.Samples.RecordItem
struct RecordItem_t1075151419;
// DigitsNFCToolkit.Samples.WriteScreenControl
struct WriteScreenControl_t1506090515;
// DigitsNFCToolkit.Samples.WriteScreenControl/IconID[]
struct IconIDU5BU5D_t2629431601;
// DigitsNFCToolkit.Samples.WriteScreenView
struct WriteScreenView_t2350253495;
// DigitsNFCToolkit.SmartPosterRecord
struct SmartPosterRecord_t1640848801;
// DigitsNFCToolkit.TextRecord
struct TextRecord_t2313697623;
// DigitsNFCToolkit.TextRecord/TextEncoding[]
struct TextEncodingU5BU5D_t2855432831;
// DigitsNFCToolkit.TextRecord[]
struct TextRecordU5BU5D_t1894259566;
// DigitsNFCToolkit.UnknownRecord
struct UnknownRecord_t3228240714;
// DigitsNFCToolkit.UriRecord
struct UriRecord_t2230063309;
// DigitsNFCToolkit.Util
struct Util_t4025012431;
// System.Action`1<UnityEngine.U2D.SpriteAtlas>
struct Action_1_t819399007;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2/Entry<System.String,DigitsNFCToolkit.JSON.JSONValue>[]
struct EntryU5BU5D_t1223258574;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,DigitsNFCToolkit.JSON.JSONValue>
struct KeyCollection_t4250792414;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,DigitsNFCToolkit.JSON.JSONValue>
struct ValueCollection_t1482193965;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo>
struct Dictionary_2_t3046556399;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// System.Collections.Generic.Dictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>
struct Dictionary_2_t4061116943;
// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo>
struct Dictionary_2_t3943099367;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t380635627;
// System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>
struct IDictionary_2_t2524968334;
// System.Collections.Generic.IEnumerable`1<DigitsNFCToolkit.Samples.WriteScreenControl/IconID>
struct IEnumerable_1_t3857558761;
// System.Collections.Generic.IEnumerable`1<DigitsNFCToolkit.TextRecord/TextEncoding>
struct IEnumerable_1_t2190366515;
// System.Collections.Generic.IEnumerator`1<DigitsNFCToolkit.JSON.JSONValue>
struct IEnumerator_1_t413463816;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,DigitsNFCToolkit.JSON.JSONValue>>
struct IEnumerator_1_t2596392282;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Collections.Generic.LinkedList`1<System.Text.RegularExpressions.CachedCodeEntry>
struct LinkedList_1_t3068621835;
// System.Collections.Generic.List`1<DigitsNFCToolkit.JSON.JSONValue>
struct List_1_t1452968090;
// System.Collections.Generic.List`1<DigitsNFCToolkit.MimeMediaRecord>
struct List_1_t2208895230;
// System.Collections.Generic.List`1<DigitsNFCToolkit.NDEFRecord>
struct List_1_t4162620298;
// System.Collections.Generic.List`1<DigitsNFCToolkit.TextRecord>
struct List_1_t3785772365;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>
struct List_1_t2924027637;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct List_1_t447389798;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t4142344393;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3923495619;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t2690840144;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.IEnumerable
struct IEnumerable_t1941168011;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Delegate
struct Delegate_t1188392813;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Globalization.Calendar
struct Calendar_t1661121569;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t2285235057;
// System.Globalization.CompareInfo
struct CompareInfo_t1092934962;
// System.Globalization.CultureData
struct CultureData_t1899656083;
// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t2405853701;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t435877138;
// System.Globalization.TextInfo
struct TextInfo_t3810425522;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.IFormatProvider
struct IFormatProvider_t2518567562;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t3365920845;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.Binder
struct Binder_t2999457153;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Text.DecoderFallback
struct DecoderFallback_t3123823036;
// System.Text.EncoderFallback
struct EncoderFallback_t1188251036;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.Text.RegularExpressions.Capture
struct Capture_t2232016050;
// System.Text.RegularExpressions.CaptureCollection
struct CaptureCollection_t1760593541;
// System.Text.RegularExpressions.Capture[]
struct CaptureU5BU5D_t183267399;
// System.Text.RegularExpressions.ExclusiveReference
struct ExclusiveReference_t1927754563;
// System.Text.RegularExpressions.Group
struct Group_t2468205786;
// System.Text.RegularExpressions.GroupCollection
struct GroupCollection_t69770484;
// System.Text.RegularExpressions.Group[]
struct GroupU5BU5D_t1880820351;
// System.Text.RegularExpressions.Match
struct Match_t3408321083;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;
// System.Text.RegularExpressions.RegexCode
struct RegexCode_t4293407246;
// System.Text.RegularExpressions.RegexRunnerFactory
struct RegexRunnerFactory_t51159052;
// System.Text.RegularExpressions.SharedReference
struct SharedReference_t2916547576;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Void
struct Void_t1185182177;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UnityEngine.Event
struct Event_t2956885303;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t731888065;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t48803504;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_t3520241082;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// UnityEngine.UI.Dropdown/DropdownEvent
struct DropdownEvent_t4040729994;
// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t3270282352;
// UnityEngine.UI.Dropdown/OptionDataList
struct OptionDataList_t1438173104;
// UnityEngine.UI.Dropdown/OptionData[]
struct OptionDataU5BU5D_t3600483281;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.InputField/OnChangeEvent
struct OnChangeEvent_t467195904;
// UnityEngine.UI.InputField/OnValidateInput
struct OnValidateInput_t2355412304;
// UnityEngine.UI.InputField/SubmitEvent
struct SubmitEvent_t648412432;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.UI.ScrollRect/ScrollRectEvent
struct ScrollRectEvent_t343079324;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;

extern RuntimeClass* AbsoluteUriRecord_t2525168784_il2cpp_TypeInfo_var;
extern RuntimeClass* Boolean_t97287965_il2cpp_TypeInfo_var;
extern RuntimeClass* ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var;
extern RuntimeClass* Char_t3634460470_il2cpp_TypeInfo_var;
extern RuntimeClass* Convert_t2465617642_il2cpp_TypeInfo_var;
extern RuntimeClass* CultureInfo_t4157843068_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t4061116943_il2cpp_TypeInfo_var;
extern RuntimeClass* Double_t594665363_il2cpp_TypeInfo_var;
extern RuntimeClass* EmptyRecord_t1486430273_il2cpp_TypeInfo_var;
extern RuntimeClass* Enum_t4135868527_il2cpp_TypeInfo_var;
extern RuntimeClass* Enumerator_t3342211967_il2cpp_TypeInfo_var;
extern RuntimeClass* ExternalTypeRecord_t4087466745_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_t1113636619_il2cpp_TypeInfo_var;
extern RuntimeClass* ICollection_1_t697006752_il2cpp_TypeInfo_var;
extern RuntimeClass* IDictionary_2_t2524968334_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerable_1_t1143674703_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_1_t2596392282_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern RuntimeClass* IconID_t582738576_il2cpp_TypeInfo_var;
extern RuntimeClass* ImageRecordItem_t2104316047_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* JSONArray_t4024675823_il2cpp_TypeInfo_var;
extern RuntimeClass* JSONObject_t321714843_il2cpp_TypeInfo_var;
extern RuntimeClass* JSONValue_t4275860644_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1452968090_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2208895230_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t3785772365_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t4162620298_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t447389798_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* MimeMediaRecord_t736820488_il2cpp_TypeInfo_var;
extern RuntimeClass* NDEFMessage_t279637043_il2cpp_TypeInfo_var;
extern RuntimeClass* NDEFPushResult_t3422827153_il2cpp_TypeInfo_var;
extern RuntimeClass* NDEFReadError_t886852181_il2cpp_TypeInfo_var;
extern RuntimeClass* NDEFReadResult_t3483243621_il2cpp_TypeInfo_var;
extern RuntimeClass* NDEFRecordType_t4294622310_il2cpp_TypeInfo_var;
extern RuntimeClass* NDEFWriteError_t890528595_il2cpp_TypeInfo_var;
extern RuntimeClass* NDEFWriteResult_t4210562629_il2cpp_TypeInfo_var;
extern RuntimeClass* NFCTag_t2820711232_il2cpp_TypeInfo_var;
extern RuntimeClass* NFCTechnologyU5BU5D_t328493574_il2cpp_TypeInfo_var;
extern RuntimeClass* NFCTechnology_t1376062623_il2cpp_TypeInfo_var;
extern RuntimeClass* NativeNFCManager_t351225459_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* OnNDEFPushFinished_t4279917764_il2cpp_TypeInfo_var;
extern RuntimeClass* OnNDEFReadFinished_t1327886840_il2cpp_TypeInfo_var;
extern RuntimeClass* OnNDEFWriteFinished_t4102039599_il2cpp_TypeInfo_var;
extern RuntimeClass* OnNFCTagDetected_t3189675727_il2cpp_TypeInfo_var;
extern RuntimeClass* OptionData_t3270282352_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern RuntimeClass* RecommendedAction_t1182550772_il2cpp_TypeInfo_var;
extern RuntimeClass* Regex_t3657309853_il2cpp_TypeInfo_var;
extern RuntimeClass* SmartPosterRecord_t1640848801_il2cpp_TypeInfo_var;
extern RuntimeClass* Stack_1_t2690840144_il2cpp_TypeInfo_var;
extern RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* TextEncoding_t3210513626_il2cpp_TypeInfo_var;
extern RuntimeClass* TextRecord_t2313697623_il2cpp_TypeInfo_var;
extern RuntimeClass* Texture2D_t3840446185_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* UnknownRecord_t3228240714_il2cpp_TypeInfo_var;
extern RuntimeClass* UriRecord_t2230063309_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral100159630;
extern String_t* _stringLiteral1005157632;
extern String_t* _stringLiteral1036118513;
extern String_t* _stringLiteral1091168510;
extern String_t* _stringLiteral110252601;
extern String_t* _stringLiteral1202628576;
extern String_t* _stringLiteral1218818254;
extern String_t* _stringLiteral1239137644;
extern String_t* _stringLiteral1274537113;
extern String_t* _stringLiteral1281245125;
extern String_t* _stringLiteral1299802695;
extern String_t* _stringLiteral1346367280;
extern String_t* _stringLiteral1377815833;
extern String_t* _stringLiteral1410634818;
extern String_t* _stringLiteral1504015411;
extern String_t* _stringLiteral150590343;
extern String_t* _stringLiteral1526924809;
extern String_t* _stringLiteral1553682333;
extern String_t* _stringLiteral1558351666;
extern String_t* _stringLiteral1559920598;
extern String_t* _stringLiteral1773568958;
extern String_t* _stringLiteral1780417286;
extern String_t* _stringLiteral1783094721;
extern String_t* _stringLiteral1993607875;
extern String_t* _stringLiteral1997836753;
extern String_t* _stringLiteral2045074213;
extern String_t* _stringLiteral2059862158;
extern String_t* _stringLiteral2075513268;
extern String_t* _stringLiteral2127062272;
extern String_t* _stringLiteral2129789834;
extern String_t* _stringLiteral2142701798;
extern String_t* _stringLiteral224826876;
extern String_t* _stringLiteral2308089002;
extern String_t* _stringLiteral2329350324;
extern String_t* _stringLiteral2350814669;
extern String_t* _stringLiteral2361935695;
extern String_t* _stringLiteral2365897554;
extern String_t* _stringLiteral2378602798;
extern String_t* _stringLiteral2455323115;
extern String_t* _stringLiteral2468127799;
extern String_t* _stringLiteral2477980183;
extern String_t* _stringLiteral2535715514;
extern String_t* _stringLiteral259035310;
extern String_t* _stringLiteral2628391645;
extern String_t* _stringLiteral2634224520;
extern String_t* _stringLiteral2734644804;
extern String_t* _stringLiteral2793114053;
extern String_t* _stringLiteral2803516718;
extern String_t* _stringLiteral2803871764;
extern String_t* _stringLiteral2854237347;
extern String_t* _stringLiteral2863031003;
extern String_t* _stringLiteral2917620724;
extern String_t* _stringLiteral2983490506;
extern String_t* _stringLiteral3008852884;
extern String_t* _stringLiteral3037589779;
extern String_t* _stringLiteral3042993421;
extern String_t* _stringLiteral3070491693;
extern String_t* _stringLiteral3089059158;
extern String_t* _stringLiteral3160991853;
extern String_t* _stringLiteral3165397527;
extern String_t* _stringLiteral3221230196;
extern String_t* _stringLiteral3243520166;
extern String_t* _stringLiteral3253941996;
extern String_t* _stringLiteral3313977880;
extern String_t* _stringLiteral3434468235;
extern String_t* _stringLiteral3439933068;
extern String_t* _stringLiteral3450386420;
extern String_t* _stringLiteral3450517380;
extern String_t* _stringLiteral3450583028;
extern String_t* _stringLiteral3452614526;
extern String_t* _stringLiteral3452614529;
extern String_t* _stringLiteral3452614531;
extern String_t* _stringLiteral3452614533;
extern String_t* _stringLiteral3452614535;
extern String_t* _stringLiteral3452614547;
extern String_t* _stringLiteral3452614563;
extern String_t* _stringLiteral3452614564;
extern String_t* _stringLiteral3452614566;
extern String_t* _stringLiteral3452614567;
extern String_t* _stringLiteral3452614568;
extern String_t* _stringLiteral3452614641;
extern String_t* _stringLiteral3452614644;
extern String_t* _stringLiteral3452811252;
extern String_t* _stringLiteral3452876788;
extern String_t* _stringLiteral3453007860;
extern String_t* _stringLiteral3453073396;
extern String_t* _stringLiteral3453138932;
extern String_t* _stringLiteral3454449607;
extern String_t* _stringLiteral3454842811;
extern String_t* _stringLiteral3454973890;
extern String_t* _stringLiteral3458119668;
extern String_t* _stringLiteral3530183375;
extern String_t* _stringLiteral3594963313;
extern String_t* _stringLiteral3619508179;
extern String_t* _stringLiteral363185568;
extern String_t* _stringLiteral363395119;
extern String_t* _stringLiteral3680104728;
extern String_t* _stringLiteral3685564639;
extern String_t* _stringLiteral3707480139;
extern String_t* _stringLiteral3723456887;
extern String_t* _stringLiteral3774377648;
extern String_t* _stringLiteral3779069074;
extern String_t* _stringLiteral3820123721;
extern String_t* _stringLiteral3860635076;
extern String_t* _stringLiteral3875954633;
extern String_t* _stringLiteral3923321062;
extern String_t* _stringLiteral3941463922;
extern String_t* _stringLiteral3952973286;
extern String_t* _stringLiteral3987835854;
extern String_t* _stringLiteral4002445229;
extern String_t* _stringLiteral4007973390;
extern String_t* _stringLiteral4012561394;
extern String_t* _stringLiteral4041715024;
extern String_t* _stringLiteral4049040645;
extern String_t* _stringLiteral4081923444;
extern String_t* _stringLiteral4086469251;
extern String_t* _stringLiteral4087621358;
extern String_t* _stringLiteral410768231;
extern String_t* _stringLiteral4182101087;
extern String_t* _stringLiteral453214562;
extern String_t* _stringLiteral570303280;
extern String_t* _stringLiteral605846768;
extern String_t* _stringLiteral672945067;
extern String_t* _stringLiteral673134560;
extern String_t* _stringLiteral685789873;
extern String_t* _stringLiteral690725853;
extern String_t* _stringLiteral721440197;
extern String_t* _stringLiteral731054790;
extern String_t* _stringLiteral731656311;
extern String_t* _stringLiteral771524720;
extern String_t* _stringLiteral820751583;
extern String_t* _stringLiteral863860273;
extern String_t* _stringLiteral881278981;
extern String_t* _stringLiteral891850287;
extern String_t* _stringLiteral95342995;
extern String_t* _stringLiteral992023537;
extern const RuntimeMethod* Component_GetComponentInChildren_TisText_t1901882714_m396351542_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisButton_t4055032469_m1381873113_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisDropdown_t2274391225_m782479209_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisInputField_t3762917431_m3342128916_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisScrollRect_t4137855814_m3636214290_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisText_t1901882714_m4196288697_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m691717352_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Cast_TisIconID_t582738576_m3494484091_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Cast_TisTextEncoding_t3210513626_m2305854455_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ToArray_TisIconID_t582738576_m2108801283_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ToArray_TisTextEncoding_t3210513626_m1218064777_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3644548256_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m4065387915_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m976429252_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisIOSNFC_t293874181_m566898834_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisNativeNFCManager_t351225459_m2397571266_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Key_m1640885509_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m1594442022_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m119444007_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2718096786_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3311994978_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m350525082_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3700962105_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m613793141_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m3485391654_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAt_m2053294765_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1332560254_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1438953653_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m2367744050_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m2409190732_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m452167397_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m1758644702_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m2774962351_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m3731320739_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m3842818989_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m1387264933_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m3864448256_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m489380024_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m740911558_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m988656695_RuntimeMethod_var;
extern const RuntimeMethod* List_1_set_Item_m1423443710_RuntimeMethod_var;
extern const RuntimeMethod* Object_FindObjectOfType_TisNativeNFCManager_t351225459_m2303328554_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisRecordItem_t1075151419_m880386264_RuntimeMethod_var;
extern const RuntimeMethod* ReadScreenControl_OnNDEFReadFinished_m3925533757_RuntimeMethod_var;
extern const RuntimeMethod* ReadScreenControl_OnNFCTagDetected_m4030544015_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Pop_m513619580_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Push_m43534404_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1__ctor_m2770587905_RuntimeMethod_var;
extern const RuntimeMethod* WriteScreenControl_OnNDEFPushFinished_m3165364698_RuntimeMethod_var;
extern const RuntimeMethod* WriteScreenControl_OnNDEFWriteFinished_m3757117548_RuntimeMethod_var;
extern const RuntimeType* IconID_t582738576_0_0_0_var;
extern const RuntimeType* NDEFRecordType_t4294622310_0_0_0_var;
extern const RuntimeType* TextEncoding_t3210513626_0_0_0_var;
extern const uint32_t AbsoluteUriRecord_ParseJSON_m530352929_MetadataUsageId;
extern const uint32_t AbsoluteUriRecord_ToJSON_m1384635261_MetadataUsageId;
extern const uint32_t ExternalTypeRecord_ParseJSON_m2193836732_MetadataUsageId;
extern const uint32_t ExternalTypeRecord_ToJSON_m2957991546_MetadataUsageId;
extern const uint32_t ImageRecordItem_Awake_m810633560_MetadataUsageId;
extern const uint32_t ImageRecordItem_LoadImage_m2052998847_MetadataUsageId;
extern const uint32_t JSONArray_Add_m1346855851_MetadataUsageId;
extern const uint32_t JSONArray_Clear_m3405071474_MetadataUsageId;
extern const uint32_t JSONArray_GetEnumerator_m2591971435_MetadataUsageId;
extern const uint32_t JSONArray_Parse_m572560914_MetadataUsageId;
extern const uint32_t JSONArray_Remove_m1539378238_MetadataUsageId;
extern const uint32_t JSONArray_System_Collections_IEnumerable_GetEnumerator_m3127926955_MetadataUsageId;
extern const uint32_t JSONArray_ToString_m3591961336_MetadataUsageId;
extern const uint32_t JSONArray__ctor_m3612236623_MetadataUsageId;
extern const uint32_t JSONArray__ctor_m4099340829_MetadataUsageId;
extern const uint32_t JSONArray_get_Item_m1052458067_MetadataUsageId;
extern const uint32_t JSONArray_get_Length_m1921089944_MetadataUsageId;
extern const uint32_t JSONArray_op_Addition_m1422077166_MetadataUsageId;
extern const uint32_t JSONArray_set_Item_m1103846076_MetadataUsageId;
extern const uint32_t JSONObject_Add_m177659171_MetadataUsageId;
extern const uint32_t JSONObject_Add_m2412535806_MetadataUsageId;
extern const uint32_t JSONObject_Clear_m476800019_MetadataUsageId;
extern const uint32_t JSONObject_ContainsKey_m2085965128_MetadataUsageId;
extern const uint32_t JSONObject_Fail_m1137956452_MetadataUsageId;
extern const uint32_t JSONObject_Fail_m3595512628_MetadataUsageId;
extern const uint32_t JSONObject_GetArray_m3732897099_MetadataUsageId;
extern const uint32_t JSONObject_GetBoolean_m3047886088_MetadataUsageId;
extern const uint32_t JSONObject_GetEnumerator_m1190645326_MetadataUsageId;
extern const uint32_t JSONObject_GetKeys_m698843428_MetadataUsageId;
extern const uint32_t JSONObject_GetNumber_m2560286787_MetadataUsageId;
extern const uint32_t JSONObject_GetObject_m1756406420_MetadataUsageId;
extern const uint32_t JSONObject_GetString_m3932807855_MetadataUsageId;
extern const uint32_t JSONObject_GetValue_m193839943_MetadataUsageId;
extern const uint32_t JSONObject_ParseNumber_m2501283307_MetadataUsageId;
extern const uint32_t JSONObject_ParseString_m1617168844_MetadataUsageId;
extern const uint32_t JSONObject_Parse_m2486419426_MetadataUsageId;
extern const uint32_t JSONObject_Remove_m1786693770_MetadataUsageId;
extern const uint32_t JSONObject_SanitizeJSONString_m479242491_MetadataUsageId;
extern const uint32_t JSONObject_SkipWhitespace_m1442147541_MetadataUsageId;
extern const uint32_t JSONObject_System_Collections_IEnumerable_GetEnumerator_m1937787778_MetadataUsageId;
extern const uint32_t JSONObject_ToString_m2895087238_MetadataUsageId;
extern const uint32_t JSONObject__cctor_m723892856_MetadataUsageId;
extern const uint32_t JSONObject__ctor_m2883999366_MetadataUsageId;
extern const uint32_t JSONObject__ctor_m3436253305_MetadataUsageId;
extern const uint32_t JSONObject_set_Item_m3960241132_MetadataUsageId;
extern const uint32_t JSONValue_ToString_m620812479_MetadataUsageId;
extern const uint32_t JSONValue__ctor_m1222737883_MetadataUsageId;
extern const uint32_t JSONValue_op_Implicit_m1136109141_MetadataUsageId;
extern const uint32_t JSONValue_op_Implicit_m216610386_MetadataUsageId;
extern const uint32_t JSONValue_op_Implicit_m2321309250_MetadataUsageId;
extern const uint32_t JSONValue_op_Implicit_m3141527758_MetadataUsageId;
extern const uint32_t JSONValue_op_Implicit_m357672470_MetadataUsageId;
extern const uint32_t MessageScreenView_Initialize_m784771305_MetadataUsageId;
extern const uint32_t MessageScreenView_SwitchToPendingPush_m1103692928_MetadataUsageId;
extern const uint32_t MessageScreenView_SwitchToPendingWrite_m2230352620_MetadataUsageId;
extern const uint32_t MimeMediaRecord_ParseJSON_m4043513565_MetadataUsageId;
extern const uint32_t MimeMediaRecord_ToJSON_m500662920_MetadataUsageId;
extern const uint32_t NDEFMessage_ParseJSON_m112885689_MetadataUsageId;
extern const uint32_t NDEFMessage_ToJSON_m895517323_MetadataUsageId;
extern const uint32_t NDEFMessage__ctor_m2200006917_MetadataUsageId;
extern const uint32_t NDEFPushResult_ParseJSON_m806318507_MetadataUsageId;
extern const uint32_t NDEFReadResult_ParseJSON_m4234342281_MetadataUsageId;
extern const uint32_t NDEFRecord_ParseJSON_m1625700843_MetadataUsageId;
extern const uint32_t NDEFRecord_ToJSON_m164142245_MetadataUsageId;
extern const uint32_t NDEFWriteResult_ParseJSON_m4251555459_MetadataUsageId;
extern const uint32_t NFCTag_ParseJSON_m4194543403_MetadataUsageId;
extern const uint32_t NFCTag_ToJSON_m1354994276_MetadataUsageId;
extern const uint32_t NativeNFCManager_Awake_m2031265650_MetadataUsageId;
extern const uint32_t NativeNFCManager_Enable_m3630587251_MetadataUsageId;
extern const uint32_t NativeNFCManager_OnDestroy_m1812899065_MetadataUsageId;
extern const uint32_t NativeNFCManager_TryDestroy_m3564277066_MetadataUsageId;
extern const uint32_t NativeNFCManager_get_Instance_m711561572_MetadataUsageId;
extern const uint32_t NativeNFC_OnNDEFPushFinished_m516808229_MetadataUsageId;
extern const uint32_t NativeNFC_OnNDEFReadFinished_m4172086545_MetadataUsageId;
extern const uint32_t NativeNFC_OnNDEFWriteFinished_m400958761_MetadataUsageId;
extern const uint32_t NativeNFC_OnNFCTagDetected_m3702881045_MetadataUsageId;
extern const uint32_t NativeNFC_add_onNDEFPushFinished_m4177872223_MetadataUsageId;
extern const uint32_t NativeNFC_add_onNDEFReadFinished_m1875928045_MetadataUsageId;
extern const uint32_t NativeNFC_add_onNDEFWriteFinished_m3934953293_MetadataUsageId;
extern const uint32_t NativeNFC_add_onNFCTagDetected_m1360338169_MetadataUsageId;
extern const uint32_t NativeNFC_remove_onNDEFPushFinished_m1864805945_MetadataUsageId;
extern const uint32_t NativeNFC_remove_onNDEFReadFinished_m3344582477_MetadataUsageId;
extern const uint32_t NativeNFC_remove_onNDEFWriteFinished_m1549713225_MetadataUsageId;
extern const uint32_t NativeNFC_remove_onNFCTagDetected_m4260315192_MetadataUsageId;
extern const uint32_t ReadScreenControl_OnDisable_m1933725734_MetadataUsageId;
extern const uint32_t ReadScreenControl_OnNDEFReadFinished_m3925533757_MetadataUsageId;
extern const uint32_t ReadScreenControl_Start_m1589085894_MetadataUsageId;
extern const uint32_t ReadScreenView_Awake_m1822767483_MetadataUsageId;
extern const uint32_t ReadScreenView_CleanupRecordItems_m1746468235_MetadataUsageId;
extern const uint32_t ReadScreenView_CreateRecordItem_m3952030190_MetadataUsageId;
extern const uint32_t ReadScreenView_UpdateNDEFMessage_m814396461_MetadataUsageId;
extern const uint32_t ReadScreenView_UpdateTagInfo_m2808310696_MetadataUsageId;
extern const uint32_t RecordItem_Awake_m673876687_MetadataUsageId;
extern const uint32_t SmartPosterRecord_ParseJSON_m2944093550_MetadataUsageId;
extern const uint32_t SmartPosterRecord_ToJSON_m1372760268_MetadataUsageId;
extern const uint32_t SmartPosterRecord__ctor_m1413753803_MetadataUsageId;
extern const uint32_t SmartPosterRecord__ctor_m3562119446_MetadataUsageId;
extern const uint32_t SmartPosterRecord__ctor_m3727071704_MetadataUsageId;
extern const uint32_t SmartPosterRecord__ctor_m608988587_MetadataUsageId;
extern const uint32_t TextRecord_ParseJSON_m1315260603_MetadataUsageId;
extern const uint32_t TextRecord_ToJSON_m2400708536_MetadataUsageId;
extern const uint32_t TextRecord__ctor_m2466027249_MetadataUsageId;
extern const uint32_t UriRecord_ParseJSON_m3469570412_MetadataUsageId;
extern const uint32_t UriRecord_ToJSON_m1049998470_MetadataUsageId;
extern const uint32_t Util_DecodeBase64UrlSafe_m4209975833_MetadataUsageId;
extern const uint32_t Util_EncodeBase64UrlSafe_m2760074679_MetadataUsageId;
extern const uint32_t WriteScreenControl_CreateTestSmartPosterRecord_m2515146452_MetadataUsageId;
extern const uint32_t WriteScreenControl_OnAddRecordClick_m2006621214_MetadataUsageId;
extern const uint32_t WriteScreenControl_OnDisable_m2989907286_MetadataUsageId;
extern const uint32_t WriteScreenControl_OnNDEFPushFinished_m3165364698_MetadataUsageId;
extern const uint32_t WriteScreenControl_OnNDEFWriteFinished_m3757117548_MetadataUsageId;
extern const uint32_t WriteScreenControl_OnRecordTypeChanged_m3725801358_MetadataUsageId;
extern const uint32_t WriteScreenControl_Start_m777714958_MetadataUsageId;
extern const uint32_t WriteScreenView_Awake_m1065143798_MetadataUsageId;
extern const uint32_t WriteScreenView_CleanupRecordItems_m619684253_MetadataUsageId;
extern const uint32_t WriteScreenView_ClearTextInput_m1742933371_MetadataUsageId;
extern const uint32_t WriteScreenView_ClearUriInput_m2839071453_MetadataUsageId;
extern const uint32_t WriteScreenView_CreateRecordItem_m4278698087_MetadataUsageId;
extern const uint32_t WriteScreenView_ResetLanguageCodeInput_m1307137580_MetadataUsageId;
extern const uint32_t WriteScreenView_Start_m109890395_MetadataUsageId;
extern const uint32_t WriteScreenView_UpdateIconDropdownOptions_m304314056_MetadataUsageId;
extern const uint32_t WriteScreenView_UpdateNDEFMessage_m4183170585_MetadataUsageId;
extern const uint32_t WriteScreenView_UpdateTextEncodingDropdownOptions_m2236664622_MetadataUsageId;
extern const uint32_t WriteScreenView_UpdateTypeDropdownOptions_m2278996124_MetadataUsageId;
struct CultureData_t1899656083_marshaled_com;
struct CultureData_t1899656083_marshaled_pinvoke;
struct CultureInfo_t4157843068_marshaled_com;
struct CultureInfo_t4157843068_marshaled_pinvoke;

struct NFCTechnologyU5BU5D_t328493574;
struct IconIDU5BU5D_t2629431601;
struct TextEncodingU5BU5D_t2855432831;
struct ByteU5BU5D_t4116647657;
struct DelegateU5BU5D_t1703627840;
struct ObjectU5BU5D_t2843939325;
struct StringU5BU5D_t1281789340;


#ifndef U3CMODULEU3E_T692745566_H
#define U3CMODULEU3E_T692745566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745566 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745566_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef JSONARRAY_T4024675823_H
#define JSONARRAY_T4024675823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.JSON.JSONArray
struct  JSONArray_t4024675823  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<DigitsNFCToolkit.JSON.JSONValue> DigitsNFCToolkit.JSON.JSONArray::values
	List_1_t1452968090 * ___values_0;

public:
	inline static int32_t get_offset_of_values_0() { return static_cast<int32_t>(offsetof(JSONArray_t4024675823, ___values_0)); }
	inline List_1_t1452968090 * get_values_0() const { return ___values_0; }
	inline List_1_t1452968090 ** get_address_of_values_0() { return &___values_0; }
	inline void set_values_0(List_1_t1452968090 * value)
	{
		___values_0 = value;
		Il2CppCodeGenWriteBarrier((&___values_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONARRAY_T4024675823_H
#ifndef JSONOBJECT_T321714843_H
#define JSONOBJECT_T321714843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.JSON.JSONObject
struct  JSONObject_t321714843  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue> DigitsNFCToolkit.JSON.JSONObject::values
	RuntimeObject* ___values_0;

public:
	inline static int32_t get_offset_of_values_0() { return static_cast<int32_t>(offsetof(JSONObject_t321714843, ___values_0)); }
	inline RuntimeObject* get_values_0() const { return ___values_0; }
	inline RuntimeObject** get_address_of_values_0() { return &___values_0; }
	inline void set_values_0(RuntimeObject* value)
	{
		___values_0 = value;
		Il2CppCodeGenWriteBarrier((&___values_0), value);
	}
};

struct JSONObject_t321714843_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex DigitsNFCToolkit.JSON.JSONObject::unicodeRegex
	Regex_t3657309853 * ___unicodeRegex_1;
	// System.Byte[] DigitsNFCToolkit.JSON.JSONObject::unicodeBytes
	ByteU5BU5D_t4116647657* ___unicodeBytes_2;

public:
	inline static int32_t get_offset_of_unicodeRegex_1() { return static_cast<int32_t>(offsetof(JSONObject_t321714843_StaticFields, ___unicodeRegex_1)); }
	inline Regex_t3657309853 * get_unicodeRegex_1() const { return ___unicodeRegex_1; }
	inline Regex_t3657309853 ** get_address_of_unicodeRegex_1() { return &___unicodeRegex_1; }
	inline void set_unicodeRegex_1(Regex_t3657309853 * value)
	{
		___unicodeRegex_1 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeRegex_1), value);
	}

	inline static int32_t get_offset_of_unicodeBytes_2() { return static_cast<int32_t>(offsetof(JSONObject_t321714843_StaticFields, ___unicodeBytes_2)); }
	inline ByteU5BU5D_t4116647657* get_unicodeBytes_2() const { return ___unicodeBytes_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_unicodeBytes_2() { return &___unicodeBytes_2; }
	inline void set_unicodeBytes_2(ByteU5BU5D_t4116647657* value)
	{
		___unicodeBytes_2 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeBytes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECT_T321714843_H
#ifndef NDEFPUSHRESULT_T3422827153_H
#define NDEFPUSHRESULT_T3422827153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFPushResult
struct  NDEFPushResult_t3422827153  : public RuntimeObject
{
public:
	// System.Boolean DigitsNFCToolkit.NDEFPushResult::success
	bool ___success_0;
	// DigitsNFCToolkit.NDEFMessage DigitsNFCToolkit.NDEFPushResult::message
	NDEFMessage_t279637043 * ___message_1;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(NDEFPushResult_t3422827153, ___success_0)); }
	inline bool get_success_0() const { return ___success_0; }
	inline bool* get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(bool value)
	{
		___success_0 = value;
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(NDEFPushResult_t3422827153, ___message_1)); }
	inline NDEFMessage_t279637043 * get_message_1() const { return ___message_1; }
	inline NDEFMessage_t279637043 ** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(NDEFMessage_t279637043 * value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFPUSHRESULT_T3422827153_H
#ifndef NFCTAG_T2820711232_H
#define NFCTAG_T2820711232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NFCTag
struct  NFCTag_t2820711232  : public RuntimeObject
{
public:
	// System.String DigitsNFCToolkit.NFCTag::id
	String_t* ___id_0;
	// DigitsNFCToolkit.NFCTechnology[] DigitsNFCToolkit.NFCTag::technologies
	NFCTechnologyU5BU5D_t328493574* ___technologies_1;
	// System.String DigitsNFCToolkit.NFCTag::manufacturer
	String_t* ___manufacturer_2;
	// System.Boolean DigitsNFCToolkit.NFCTag::writable
	bool ___writable_3;
	// System.Int32 DigitsNFCToolkit.NFCTag::maxWriteSize
	int32_t ___maxWriteSize_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(NFCTag_t2820711232, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_technologies_1() { return static_cast<int32_t>(offsetof(NFCTag_t2820711232, ___technologies_1)); }
	inline NFCTechnologyU5BU5D_t328493574* get_technologies_1() const { return ___technologies_1; }
	inline NFCTechnologyU5BU5D_t328493574** get_address_of_technologies_1() { return &___technologies_1; }
	inline void set_technologies_1(NFCTechnologyU5BU5D_t328493574* value)
	{
		___technologies_1 = value;
		Il2CppCodeGenWriteBarrier((&___technologies_1), value);
	}

	inline static int32_t get_offset_of_manufacturer_2() { return static_cast<int32_t>(offsetof(NFCTag_t2820711232, ___manufacturer_2)); }
	inline String_t* get_manufacturer_2() const { return ___manufacturer_2; }
	inline String_t** get_address_of_manufacturer_2() { return &___manufacturer_2; }
	inline void set_manufacturer_2(String_t* value)
	{
		___manufacturer_2 = value;
		Il2CppCodeGenWriteBarrier((&___manufacturer_2), value);
	}

	inline static int32_t get_offset_of_writable_3() { return static_cast<int32_t>(offsetof(NFCTag_t2820711232, ___writable_3)); }
	inline bool get_writable_3() const { return ___writable_3; }
	inline bool* get_address_of_writable_3() { return &___writable_3; }
	inline void set_writable_3(bool value)
	{
		___writable_3 = value;
	}

	inline static int32_t get_offset_of_maxWriteSize_4() { return static_cast<int32_t>(offsetof(NFCTag_t2820711232, ___maxWriteSize_4)); }
	inline int32_t get_maxWriteSize_4() const { return ___maxWriteSize_4; }
	inline int32_t* get_address_of_maxWriteSize_4() { return &___maxWriteSize_4; }
	inline void set_maxWriteSize_4(int32_t value)
	{
		___maxWriteSize_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NFCTAG_T2820711232_H
#ifndef UTIL_T4025012431_H
#define UTIL_T4025012431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Util
struct  Util_t4025012431  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTIL_T4025012431_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef DICTIONARY_2_T4061116943_H
#define DICTIONARY_2_T4061116943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>
struct  Dictionary_2_t4061116943  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t385246372* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t1223258574* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t4250792414 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t1482193965 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t4061116943, ___buckets_0)); }
	inline Int32U5BU5D_t385246372* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t385246372** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t385246372* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t4061116943, ___entries_1)); }
	inline EntryU5BU5D_t1223258574* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t1223258574** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t1223258574* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t4061116943, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t4061116943, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t4061116943, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t4061116943, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t4061116943, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t4061116943, ___keys_7)); }
	inline KeyCollection_t4250792414 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t4250792414 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t4250792414 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((&___keys_7), value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t4061116943, ___values_8)); }
	inline ValueCollection_t1482193965 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t1482193965 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t1482193965 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((&___values_8), value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t4061116943, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T4061116943_H
#ifndef LIST_1_T1452968090_H
#define LIST_1_T1452968090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<DigitsNFCToolkit.JSON.JSONValue>
struct  List_1_t1452968090  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	JSONValueU5BU5D_t1620021325* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1452968090, ____items_1)); }
	inline JSONValueU5BU5D_t1620021325* get__items_1() const { return ____items_1; }
	inline JSONValueU5BU5D_t1620021325** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(JSONValueU5BU5D_t1620021325* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1452968090, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1452968090, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t1452968090, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t1452968090_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	JSONValueU5BU5D_t1620021325* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t1452968090_StaticFields, ____emptyArray_5)); }
	inline JSONValueU5BU5D_t1620021325* get__emptyArray_5() const { return ____emptyArray_5; }
	inline JSONValueU5BU5D_t1620021325** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(JSONValueU5BU5D_t1620021325* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1452968090_H
#ifndef LIST_1_T2208895230_H
#define LIST_1_T2208895230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<DigitsNFCToolkit.MimeMediaRecord>
struct  List_1_t2208895230  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	MimeMediaRecordU5BU5D_t1073055193* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2208895230, ____items_1)); }
	inline MimeMediaRecordU5BU5D_t1073055193* get__items_1() const { return ____items_1; }
	inline MimeMediaRecordU5BU5D_t1073055193** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(MimeMediaRecordU5BU5D_t1073055193* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2208895230, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2208895230, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t2208895230, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t2208895230_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	MimeMediaRecordU5BU5D_t1073055193* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t2208895230_StaticFields, ____emptyArray_5)); }
	inline MimeMediaRecordU5BU5D_t1073055193* get__emptyArray_5() const { return ____emptyArray_5; }
	inline MimeMediaRecordU5BU5D_t1073055193** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(MimeMediaRecordU5BU5D_t1073055193* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2208895230_H
#ifndef LIST_1_T4162620298_H
#define LIST_1_T4162620298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<DigitsNFCToolkit.NDEFRecord>
struct  List_1_t4162620298  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	NDEFRecordU5BU5D_t4010013597* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4162620298, ____items_1)); }
	inline NDEFRecordU5BU5D_t4010013597* get__items_1() const { return ____items_1; }
	inline NDEFRecordU5BU5D_t4010013597** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(NDEFRecordU5BU5D_t4010013597* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4162620298, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4162620298, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4162620298, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t4162620298_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	NDEFRecordU5BU5D_t4010013597* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4162620298_StaticFields, ____emptyArray_5)); }
	inline NDEFRecordU5BU5D_t4010013597* get__emptyArray_5() const { return ____emptyArray_5; }
	inline NDEFRecordU5BU5D_t4010013597** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(NDEFRecordU5BU5D_t4010013597* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4162620298_H
#ifndef LIST_1_T3785772365_H
#define LIST_1_T3785772365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<DigitsNFCToolkit.TextRecord>
struct  List_1_t3785772365  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TextRecordU5BU5D_t1894259566* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3785772365, ____items_1)); }
	inline TextRecordU5BU5D_t1894259566* get__items_1() const { return ____items_1; }
	inline TextRecordU5BU5D_t1894259566** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TextRecordU5BU5D_t1894259566* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3785772365, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3785772365, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3785772365, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t3785772365_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TextRecordU5BU5D_t1894259566* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3785772365_StaticFields, ____emptyArray_5)); }
	inline TextRecordU5BU5D_t1894259566* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TextRecordU5BU5D_t1894259566** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TextRecordU5BU5D_t1894259566* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3785772365_H
#ifndef LIST_1_T447389798_H
#define LIST_1_T447389798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct  List_1_t447389798  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	OptionDataU5BU5D_t3600483281* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t447389798, ____items_1)); }
	inline OptionDataU5BU5D_t3600483281* get__items_1() const { return ____items_1; }
	inline OptionDataU5BU5D_t3600483281** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(OptionDataU5BU5D_t3600483281* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t447389798, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t447389798, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t447389798, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t447389798_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	OptionDataU5BU5D_t3600483281* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t447389798_StaticFields, ____emptyArray_5)); }
	inline OptionDataU5BU5D_t3600483281* get__emptyArray_5() const { return ____emptyArray_5; }
	inline OptionDataU5BU5D_t3600483281** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(OptionDataU5BU5D_t3600483281* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T447389798_H
#ifndef STACK_1_T2690840144_H
#define STACK_1_T2690840144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1<System.String>
struct  Stack_1_t2690840144  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	StringU5BU5D_t1281789340* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;
	// System.Object System.Collections.Generic.Stack`1::_syncRoot
	RuntimeObject * ____syncRoot_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t2690840144, ____array_0)); }
	inline StringU5BU5D_t1281789340* get__array_0() const { return ____array_0; }
	inline StringU5BU5D_t1281789340** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(StringU5BU5D_t1281789340* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t2690840144, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t2690840144, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(Stack_1_t2690840144, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_1_T2690840144_H
#ifndef CULTUREINFO_T4157843068_H
#define CULTUREINFO_T4157843068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CultureInfo
struct  CultureInfo_t4157843068  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_3;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_4;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_5;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_6;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_7;
	// System.Int32 System.Globalization.CultureInfo::default_calendar_type
	int32_t ___default_calendar_type_8;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_9;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t435877138 * ___numInfo_10;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t2405853701 * ___dateTimeInfo_11;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_t3810425522 * ___textInfo_12;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_13;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_14;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_15;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_16;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_17;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_18;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_19;
	// System.String[] System.Globalization.CultureInfo::native_calendar_names
	StringU5BU5D_t1281789340* ___native_calendar_names_20;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t1092934962 * ___compareInfo_21;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_22;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_23;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t1661121569 * ___calendar_24;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t4157843068 * ___parent_culture_25;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_26;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_t4116647657* ___cached_serialized_form_27;
	// System.Globalization.CultureData System.Globalization.CultureInfo::m_cultureData
	CultureData_t1899656083 * ___m_cultureData_28;
	// System.Boolean System.Globalization.CultureInfo::m_isInherited
	bool ___m_isInherited_29;

public:
	inline static int32_t get_offset_of_m_isReadOnly_3() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_isReadOnly_3)); }
	inline bool get_m_isReadOnly_3() const { return ___m_isReadOnly_3; }
	inline bool* get_address_of_m_isReadOnly_3() { return &___m_isReadOnly_3; }
	inline void set_m_isReadOnly_3(bool value)
	{
		___m_isReadOnly_3 = value;
	}

	inline static int32_t get_offset_of_cultureID_4() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___cultureID_4)); }
	inline int32_t get_cultureID_4() const { return ___cultureID_4; }
	inline int32_t* get_address_of_cultureID_4() { return &___cultureID_4; }
	inline void set_cultureID_4(int32_t value)
	{
		___cultureID_4 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_5() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___parent_lcid_5)); }
	inline int32_t get_parent_lcid_5() const { return ___parent_lcid_5; }
	inline int32_t* get_address_of_parent_lcid_5() { return &___parent_lcid_5; }
	inline void set_parent_lcid_5(int32_t value)
	{
		___parent_lcid_5 = value;
	}

	inline static int32_t get_offset_of_datetime_index_6() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___datetime_index_6)); }
	inline int32_t get_datetime_index_6() const { return ___datetime_index_6; }
	inline int32_t* get_address_of_datetime_index_6() { return &___datetime_index_6; }
	inline void set_datetime_index_6(int32_t value)
	{
		___datetime_index_6 = value;
	}

	inline static int32_t get_offset_of_number_index_7() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___number_index_7)); }
	inline int32_t get_number_index_7() const { return ___number_index_7; }
	inline int32_t* get_address_of_number_index_7() { return &___number_index_7; }
	inline void set_number_index_7(int32_t value)
	{
		___number_index_7 = value;
	}

	inline static int32_t get_offset_of_default_calendar_type_8() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___default_calendar_type_8)); }
	inline int32_t get_default_calendar_type_8() const { return ___default_calendar_type_8; }
	inline int32_t* get_address_of_default_calendar_type_8() { return &___default_calendar_type_8; }
	inline void set_default_calendar_type_8(int32_t value)
	{
		___default_calendar_type_8 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_9() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_useUserOverride_9)); }
	inline bool get_m_useUserOverride_9() const { return ___m_useUserOverride_9; }
	inline bool* get_address_of_m_useUserOverride_9() { return &___m_useUserOverride_9; }
	inline void set_m_useUserOverride_9(bool value)
	{
		___m_useUserOverride_9 = value;
	}

	inline static int32_t get_offset_of_numInfo_10() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___numInfo_10)); }
	inline NumberFormatInfo_t435877138 * get_numInfo_10() const { return ___numInfo_10; }
	inline NumberFormatInfo_t435877138 ** get_address_of_numInfo_10() { return &___numInfo_10; }
	inline void set_numInfo_10(NumberFormatInfo_t435877138 * value)
	{
		___numInfo_10 = value;
		Il2CppCodeGenWriteBarrier((&___numInfo_10), value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_11() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___dateTimeInfo_11)); }
	inline DateTimeFormatInfo_t2405853701 * get_dateTimeInfo_11() const { return ___dateTimeInfo_11; }
	inline DateTimeFormatInfo_t2405853701 ** get_address_of_dateTimeInfo_11() { return &___dateTimeInfo_11; }
	inline void set_dateTimeInfo_11(DateTimeFormatInfo_t2405853701 * value)
	{
		___dateTimeInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___dateTimeInfo_11), value);
	}

	inline static int32_t get_offset_of_textInfo_12() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___textInfo_12)); }
	inline TextInfo_t3810425522 * get_textInfo_12() const { return ___textInfo_12; }
	inline TextInfo_t3810425522 ** get_address_of_textInfo_12() { return &___textInfo_12; }
	inline void set_textInfo_12(TextInfo_t3810425522 * value)
	{
		___textInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_12), value);
	}

	inline static int32_t get_offset_of_m_name_13() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_name_13)); }
	inline String_t* get_m_name_13() const { return ___m_name_13; }
	inline String_t** get_address_of_m_name_13() { return &___m_name_13; }
	inline void set_m_name_13(String_t* value)
	{
		___m_name_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_13), value);
	}

	inline static int32_t get_offset_of_englishname_14() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___englishname_14)); }
	inline String_t* get_englishname_14() const { return ___englishname_14; }
	inline String_t** get_address_of_englishname_14() { return &___englishname_14; }
	inline void set_englishname_14(String_t* value)
	{
		___englishname_14 = value;
		Il2CppCodeGenWriteBarrier((&___englishname_14), value);
	}

	inline static int32_t get_offset_of_nativename_15() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___nativename_15)); }
	inline String_t* get_nativename_15() const { return ___nativename_15; }
	inline String_t** get_address_of_nativename_15() { return &___nativename_15; }
	inline void set_nativename_15(String_t* value)
	{
		___nativename_15 = value;
		Il2CppCodeGenWriteBarrier((&___nativename_15), value);
	}

	inline static int32_t get_offset_of_iso3lang_16() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___iso3lang_16)); }
	inline String_t* get_iso3lang_16() const { return ___iso3lang_16; }
	inline String_t** get_address_of_iso3lang_16() { return &___iso3lang_16; }
	inline void set_iso3lang_16(String_t* value)
	{
		___iso3lang_16 = value;
		Il2CppCodeGenWriteBarrier((&___iso3lang_16), value);
	}

	inline static int32_t get_offset_of_iso2lang_17() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___iso2lang_17)); }
	inline String_t* get_iso2lang_17() const { return ___iso2lang_17; }
	inline String_t** get_address_of_iso2lang_17() { return &___iso2lang_17; }
	inline void set_iso2lang_17(String_t* value)
	{
		___iso2lang_17 = value;
		Il2CppCodeGenWriteBarrier((&___iso2lang_17), value);
	}

	inline static int32_t get_offset_of_win3lang_18() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___win3lang_18)); }
	inline String_t* get_win3lang_18() const { return ___win3lang_18; }
	inline String_t** get_address_of_win3lang_18() { return &___win3lang_18; }
	inline void set_win3lang_18(String_t* value)
	{
		___win3lang_18 = value;
		Il2CppCodeGenWriteBarrier((&___win3lang_18), value);
	}

	inline static int32_t get_offset_of_territory_19() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___territory_19)); }
	inline String_t* get_territory_19() const { return ___territory_19; }
	inline String_t** get_address_of_territory_19() { return &___territory_19; }
	inline void set_territory_19(String_t* value)
	{
		___territory_19 = value;
		Il2CppCodeGenWriteBarrier((&___territory_19), value);
	}

	inline static int32_t get_offset_of_native_calendar_names_20() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___native_calendar_names_20)); }
	inline StringU5BU5D_t1281789340* get_native_calendar_names_20() const { return ___native_calendar_names_20; }
	inline StringU5BU5D_t1281789340** get_address_of_native_calendar_names_20() { return &___native_calendar_names_20; }
	inline void set_native_calendar_names_20(StringU5BU5D_t1281789340* value)
	{
		___native_calendar_names_20 = value;
		Il2CppCodeGenWriteBarrier((&___native_calendar_names_20), value);
	}

	inline static int32_t get_offset_of_compareInfo_21() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___compareInfo_21)); }
	inline CompareInfo_t1092934962 * get_compareInfo_21() const { return ___compareInfo_21; }
	inline CompareInfo_t1092934962 ** get_address_of_compareInfo_21() { return &___compareInfo_21; }
	inline void set_compareInfo_21(CompareInfo_t1092934962 * value)
	{
		___compareInfo_21 = value;
		Il2CppCodeGenWriteBarrier((&___compareInfo_21), value);
	}

	inline static int32_t get_offset_of_textinfo_data_22() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___textinfo_data_22)); }
	inline void* get_textinfo_data_22() const { return ___textinfo_data_22; }
	inline void** get_address_of_textinfo_data_22() { return &___textinfo_data_22; }
	inline void set_textinfo_data_22(void* value)
	{
		___textinfo_data_22 = value;
	}

	inline static int32_t get_offset_of_m_dataItem_23() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_dataItem_23)); }
	inline int32_t get_m_dataItem_23() const { return ___m_dataItem_23; }
	inline int32_t* get_address_of_m_dataItem_23() { return &___m_dataItem_23; }
	inline void set_m_dataItem_23(int32_t value)
	{
		___m_dataItem_23 = value;
	}

	inline static int32_t get_offset_of_calendar_24() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___calendar_24)); }
	inline Calendar_t1661121569 * get_calendar_24() const { return ___calendar_24; }
	inline Calendar_t1661121569 ** get_address_of_calendar_24() { return &___calendar_24; }
	inline void set_calendar_24(Calendar_t1661121569 * value)
	{
		___calendar_24 = value;
		Il2CppCodeGenWriteBarrier((&___calendar_24), value);
	}

	inline static int32_t get_offset_of_parent_culture_25() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___parent_culture_25)); }
	inline CultureInfo_t4157843068 * get_parent_culture_25() const { return ___parent_culture_25; }
	inline CultureInfo_t4157843068 ** get_address_of_parent_culture_25() { return &___parent_culture_25; }
	inline void set_parent_culture_25(CultureInfo_t4157843068 * value)
	{
		___parent_culture_25 = value;
		Il2CppCodeGenWriteBarrier((&___parent_culture_25), value);
	}

	inline static int32_t get_offset_of_constructed_26() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___constructed_26)); }
	inline bool get_constructed_26() const { return ___constructed_26; }
	inline bool* get_address_of_constructed_26() { return &___constructed_26; }
	inline void set_constructed_26(bool value)
	{
		___constructed_26 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_27() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___cached_serialized_form_27)); }
	inline ByteU5BU5D_t4116647657* get_cached_serialized_form_27() const { return ___cached_serialized_form_27; }
	inline ByteU5BU5D_t4116647657** get_address_of_cached_serialized_form_27() { return &___cached_serialized_form_27; }
	inline void set_cached_serialized_form_27(ByteU5BU5D_t4116647657* value)
	{
		___cached_serialized_form_27 = value;
		Il2CppCodeGenWriteBarrier((&___cached_serialized_form_27), value);
	}

	inline static int32_t get_offset_of_m_cultureData_28() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_cultureData_28)); }
	inline CultureData_t1899656083 * get_m_cultureData_28() const { return ___m_cultureData_28; }
	inline CultureData_t1899656083 ** get_address_of_m_cultureData_28() { return &___m_cultureData_28; }
	inline void set_m_cultureData_28(CultureData_t1899656083 * value)
	{
		___m_cultureData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_cultureData_28), value);
	}

	inline static int32_t get_offset_of_m_isInherited_29() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_isInherited_29)); }
	inline bool get_m_isInherited_29() const { return ___m_isInherited_29; }
	inline bool* get_address_of_m_isInherited_29() { return &___m_isInherited_29; }
	inline void set_m_isInherited_29(bool value)
	{
		___m_isInherited_29 = value;
	}
};

struct CultureInfo_t4157843068_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t4157843068 * ___invariant_culture_info_0;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_1;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::default_current_culture
	CultureInfo_t4157843068 * ___default_current_culture_2;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentUICulture
	CultureInfo_t4157843068 * ___s_DefaultThreadCurrentUICulture_33;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentCulture
	CultureInfo_t4157843068 * ___s_DefaultThreadCurrentCulture_34;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_number
	Dictionary_2_t3046556399 * ___shared_by_number_35;
	// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_name
	Dictionary_2_t3943099367 * ___shared_by_name_36;
	// System.Boolean System.Globalization.CultureInfo::IsTaiwanSku
	bool ___IsTaiwanSku_37;

public:
	inline static int32_t get_offset_of_invariant_culture_info_0() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___invariant_culture_info_0)); }
	inline CultureInfo_t4157843068 * get_invariant_culture_info_0() const { return ___invariant_culture_info_0; }
	inline CultureInfo_t4157843068 ** get_address_of_invariant_culture_info_0() { return &___invariant_culture_info_0; }
	inline void set_invariant_culture_info_0(CultureInfo_t4157843068 * value)
	{
		___invariant_culture_info_0 = value;
		Il2CppCodeGenWriteBarrier((&___invariant_culture_info_0), value);
	}

	inline static int32_t get_offset_of_shared_table_lock_1() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___shared_table_lock_1)); }
	inline RuntimeObject * get_shared_table_lock_1() const { return ___shared_table_lock_1; }
	inline RuntimeObject ** get_address_of_shared_table_lock_1() { return &___shared_table_lock_1; }
	inline void set_shared_table_lock_1(RuntimeObject * value)
	{
		___shared_table_lock_1 = value;
		Il2CppCodeGenWriteBarrier((&___shared_table_lock_1), value);
	}

	inline static int32_t get_offset_of_default_current_culture_2() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___default_current_culture_2)); }
	inline CultureInfo_t4157843068 * get_default_current_culture_2() const { return ___default_current_culture_2; }
	inline CultureInfo_t4157843068 ** get_address_of_default_current_culture_2() { return &___default_current_culture_2; }
	inline void set_default_current_culture_2(CultureInfo_t4157843068 * value)
	{
		___default_current_culture_2 = value;
		Il2CppCodeGenWriteBarrier((&___default_current_culture_2), value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentUICulture_33() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___s_DefaultThreadCurrentUICulture_33)); }
	inline CultureInfo_t4157843068 * get_s_DefaultThreadCurrentUICulture_33() const { return ___s_DefaultThreadCurrentUICulture_33; }
	inline CultureInfo_t4157843068 ** get_address_of_s_DefaultThreadCurrentUICulture_33() { return &___s_DefaultThreadCurrentUICulture_33; }
	inline void set_s_DefaultThreadCurrentUICulture_33(CultureInfo_t4157843068 * value)
	{
		___s_DefaultThreadCurrentUICulture_33 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultThreadCurrentUICulture_33), value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentCulture_34() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___s_DefaultThreadCurrentCulture_34)); }
	inline CultureInfo_t4157843068 * get_s_DefaultThreadCurrentCulture_34() const { return ___s_DefaultThreadCurrentCulture_34; }
	inline CultureInfo_t4157843068 ** get_address_of_s_DefaultThreadCurrentCulture_34() { return &___s_DefaultThreadCurrentCulture_34; }
	inline void set_s_DefaultThreadCurrentCulture_34(CultureInfo_t4157843068 * value)
	{
		___s_DefaultThreadCurrentCulture_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultThreadCurrentCulture_34), value);
	}

	inline static int32_t get_offset_of_shared_by_number_35() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___shared_by_number_35)); }
	inline Dictionary_2_t3046556399 * get_shared_by_number_35() const { return ___shared_by_number_35; }
	inline Dictionary_2_t3046556399 ** get_address_of_shared_by_number_35() { return &___shared_by_number_35; }
	inline void set_shared_by_number_35(Dictionary_2_t3046556399 * value)
	{
		___shared_by_number_35 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_number_35), value);
	}

	inline static int32_t get_offset_of_shared_by_name_36() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___shared_by_name_36)); }
	inline Dictionary_2_t3943099367 * get_shared_by_name_36() const { return ___shared_by_name_36; }
	inline Dictionary_2_t3943099367 ** get_address_of_shared_by_name_36() { return &___shared_by_name_36; }
	inline void set_shared_by_name_36(Dictionary_2_t3943099367 * value)
	{
		___shared_by_name_36 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_name_36), value);
	}

	inline static int32_t get_offset_of_IsTaiwanSku_37() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___IsTaiwanSku_37)); }
	inline bool get_IsTaiwanSku_37() const { return ___IsTaiwanSku_37; }
	inline bool* get_address_of_IsTaiwanSku_37() { return &___IsTaiwanSku_37; }
	inline void set_IsTaiwanSku_37(bool value)
	{
		___IsTaiwanSku_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Globalization.CultureInfo
struct CultureInfo_t4157843068_marshaled_pinvoke
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t435877138 * ___numInfo_10;
	DateTimeFormatInfo_t2405853701 * ___dateTimeInfo_11;
	TextInfo_t3810425522 * ___textInfo_12;
	char* ___m_name_13;
	char* ___englishname_14;
	char* ___nativename_15;
	char* ___iso3lang_16;
	char* ___iso2lang_17;
	char* ___win3lang_18;
	char* ___territory_19;
	char** ___native_calendar_names_20;
	CompareInfo_t1092934962 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t1661121569 * ___calendar_24;
	CultureInfo_t4157843068_marshaled_pinvoke* ___parent_culture_25;
	int32_t ___constructed_26;
	uint8_t* ___cached_serialized_form_27;
	CultureData_t1899656083_marshaled_pinvoke* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
// Native definition for COM marshalling of System.Globalization.CultureInfo
struct CultureInfo_t4157843068_marshaled_com
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t435877138 * ___numInfo_10;
	DateTimeFormatInfo_t2405853701 * ___dateTimeInfo_11;
	TextInfo_t3810425522 * ___textInfo_12;
	Il2CppChar* ___m_name_13;
	Il2CppChar* ___englishname_14;
	Il2CppChar* ___nativename_15;
	Il2CppChar* ___iso3lang_16;
	Il2CppChar* ___iso2lang_17;
	Il2CppChar* ___win3lang_18;
	Il2CppChar* ___territory_19;
	Il2CppChar** ___native_calendar_names_20;
	CompareInfo_t1092934962 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t1661121569 * ___calendar_24;
	CultureInfo_t4157843068_marshaled_com* ___parent_culture_25;
	int32_t ___constructed_26;
	uint8_t* ___cached_serialized_form_27;
	CultureData_t1899656083_marshaled_com* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
#endif // CULTUREINFO_T4157843068_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef ENCODING_T1523322056_H
#define ENCODING_T1523322056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding
struct  Encoding_t1523322056  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t2285235057 * ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_t1188251036 * ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t3123823036 * ___decoderFallback_14;

public:
	inline static int32_t get_offset_of_m_codePage_9() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___m_codePage_9)); }
	inline int32_t get_m_codePage_9() const { return ___m_codePage_9; }
	inline int32_t* get_address_of_m_codePage_9() { return &___m_codePage_9; }
	inline void set_m_codePage_9(int32_t value)
	{
		___m_codePage_9 = value;
	}

	inline static int32_t get_offset_of_dataItem_10() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___dataItem_10)); }
	inline CodePageDataItem_t2285235057 * get_dataItem_10() const { return ___dataItem_10; }
	inline CodePageDataItem_t2285235057 ** get_address_of_dataItem_10() { return &___dataItem_10; }
	inline void set_dataItem_10(CodePageDataItem_t2285235057 * value)
	{
		___dataItem_10 = value;
		Il2CppCodeGenWriteBarrier((&___dataItem_10), value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_11() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___m_deserializedFromEverett_11)); }
	inline bool get_m_deserializedFromEverett_11() const { return ___m_deserializedFromEverett_11; }
	inline bool* get_address_of_m_deserializedFromEverett_11() { return &___m_deserializedFromEverett_11; }
	inline void set_m_deserializedFromEverett_11(bool value)
	{
		___m_deserializedFromEverett_11 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_12() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___m_isReadOnly_12)); }
	inline bool get_m_isReadOnly_12() const { return ___m_isReadOnly_12; }
	inline bool* get_address_of_m_isReadOnly_12() { return &___m_isReadOnly_12; }
	inline void set_m_isReadOnly_12(bool value)
	{
		___m_isReadOnly_12 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_13() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___encoderFallback_13)); }
	inline EncoderFallback_t1188251036 * get_encoderFallback_13() const { return ___encoderFallback_13; }
	inline EncoderFallback_t1188251036 ** get_address_of_encoderFallback_13() { return &___encoderFallback_13; }
	inline void set_encoderFallback_13(EncoderFallback_t1188251036 * value)
	{
		___encoderFallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___encoderFallback_13), value);
	}

	inline static int32_t get_offset_of_decoderFallback_14() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___decoderFallback_14)); }
	inline DecoderFallback_t3123823036 * get_decoderFallback_14() const { return ___decoderFallback_14; }
	inline DecoderFallback_t3123823036 ** get_address_of_decoderFallback_14() { return &___decoderFallback_14; }
	inline void set_decoderFallback_14(DecoderFallback_t3123823036 * value)
	{
		___decoderFallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___decoderFallback_14), value);
	}
};

struct Encoding_t1523322056_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t1523322056 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t1523322056 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t1523322056 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t1523322056 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t1523322056 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t1523322056 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t1523322056 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t1523322056 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t1853889766 * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_15;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_t1523322056 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_t1523322056 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_t1523322056 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEncoding_0), value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_t1523322056 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_t1523322056 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_t1523322056 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeEncoding_1), value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_t1523322056 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_t1523322056 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_t1523322056 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianUnicode_2), value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_t1523322056 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_t1523322056 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_t1523322056 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___utf7Encoding_3), value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_t1523322056 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_t1523322056 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_t1523322056 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((&___utf8Encoding_4), value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_t1523322056 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_t1523322056 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_t1523322056 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((&___utf32Encoding_5), value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_t1523322056 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_t1523322056 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_t1523322056 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___asciiEncoding_6), value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_t1523322056 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_t1523322056 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_t1523322056 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((&___latin1Encoding_7), value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___encodings_8)); }
	inline Hashtable_t1853889766 * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t1853889766 ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t1853889766 * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((&___encodings_8), value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_15() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___s_InternalSyncObject_15)); }
	inline RuntimeObject * get_s_InternalSyncObject_15() const { return ___s_InternalSyncObject_15; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_15() { return &___s_InternalSyncObject_15; }
	inline void set_s_InternalSyncObject_15(RuntimeObject * value)
	{
		___s_InternalSyncObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODING_T1523322056_H
#ifndef CAPTURE_T2232016050_H
#define CAPTURE_T2232016050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Capture
struct  Capture_t2232016050  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.Capture::_text
	String_t* ____text_0;
	// System.Int32 System.Text.RegularExpressions.Capture::_index
	int32_t ____index_1;
	// System.Int32 System.Text.RegularExpressions.Capture::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__text_0() { return static_cast<int32_t>(offsetof(Capture_t2232016050, ____text_0)); }
	inline String_t* get__text_0() const { return ____text_0; }
	inline String_t** get_address_of__text_0() { return &____text_0; }
	inline void set__text_0(String_t* value)
	{
		____text_0 = value;
		Il2CppCodeGenWriteBarrier((&____text_0), value);
	}

	inline static int32_t get_offset_of__index_1() { return static_cast<int32_t>(offsetof(Capture_t2232016050, ____index_1)); }
	inline int32_t get__index_1() const { return ____index_1; }
	inline int32_t* get_address_of__index_1() { return &____index_1; }
	inline void set__index_1(int32_t value)
	{
		____index_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(Capture_t2232016050, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURE_T2232016050_H
#ifndef CAPTURECOLLECTION_T1760593541_H
#define CAPTURECOLLECTION_T1760593541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.CaptureCollection
struct  CaptureCollection_t1760593541  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Group System.Text.RegularExpressions.CaptureCollection::_group
	Group_t2468205786 * ____group_0;
	// System.Int32 System.Text.RegularExpressions.CaptureCollection::_capcount
	int32_t ____capcount_1;
	// System.Text.RegularExpressions.Capture[] System.Text.RegularExpressions.CaptureCollection::_captures
	CaptureU5BU5D_t183267399* ____captures_2;

public:
	inline static int32_t get_offset_of__group_0() { return static_cast<int32_t>(offsetof(CaptureCollection_t1760593541, ____group_0)); }
	inline Group_t2468205786 * get__group_0() const { return ____group_0; }
	inline Group_t2468205786 ** get_address_of__group_0() { return &____group_0; }
	inline void set__group_0(Group_t2468205786 * value)
	{
		____group_0 = value;
		Il2CppCodeGenWriteBarrier((&____group_0), value);
	}

	inline static int32_t get_offset_of__capcount_1() { return static_cast<int32_t>(offsetof(CaptureCollection_t1760593541, ____capcount_1)); }
	inline int32_t get__capcount_1() const { return ____capcount_1; }
	inline int32_t* get_address_of__capcount_1() { return &____capcount_1; }
	inline void set__capcount_1(int32_t value)
	{
		____capcount_1 = value;
	}

	inline static int32_t get_offset_of__captures_2() { return static_cast<int32_t>(offsetof(CaptureCollection_t1760593541, ____captures_2)); }
	inline CaptureU5BU5D_t183267399* get__captures_2() const { return ____captures_2; }
	inline CaptureU5BU5D_t183267399** get_address_of__captures_2() { return &____captures_2; }
	inline void set__captures_2(CaptureU5BU5D_t183267399* value)
	{
		____captures_2 = value;
		Il2CppCodeGenWriteBarrier((&____captures_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURECOLLECTION_T1760593541_H
#ifndef GROUPCOLLECTION_T69770484_H
#define GROUPCOLLECTION_T69770484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.GroupCollection
struct  GroupCollection_t69770484  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.GroupCollection::_match
	Match_t3408321083 * ____match_0;
	// System.Collections.Hashtable System.Text.RegularExpressions.GroupCollection::_captureMap
	Hashtable_t1853889766 * ____captureMap_1;
	// System.Text.RegularExpressions.Group[] System.Text.RegularExpressions.GroupCollection::_groups
	GroupU5BU5D_t1880820351* ____groups_2;

public:
	inline static int32_t get_offset_of__match_0() { return static_cast<int32_t>(offsetof(GroupCollection_t69770484, ____match_0)); }
	inline Match_t3408321083 * get__match_0() const { return ____match_0; }
	inline Match_t3408321083 ** get_address_of__match_0() { return &____match_0; }
	inline void set__match_0(Match_t3408321083 * value)
	{
		____match_0 = value;
		Il2CppCodeGenWriteBarrier((&____match_0), value);
	}

	inline static int32_t get_offset_of__captureMap_1() { return static_cast<int32_t>(offsetof(GroupCollection_t69770484, ____captureMap_1)); }
	inline Hashtable_t1853889766 * get__captureMap_1() const { return ____captureMap_1; }
	inline Hashtable_t1853889766 ** get_address_of__captureMap_1() { return &____captureMap_1; }
	inline void set__captureMap_1(Hashtable_t1853889766 * value)
	{
		____captureMap_1 = value;
		Il2CppCodeGenWriteBarrier((&____captureMap_1), value);
	}

	inline static int32_t get_offset_of__groups_2() { return static_cast<int32_t>(offsetof(GroupCollection_t69770484, ____groups_2)); }
	inline GroupU5BU5D_t1880820351* get__groups_2() const { return ____groups_2; }
	inline GroupU5BU5D_t1880820351** get_address_of__groups_2() { return &____groups_2; }
	inline void set__groups_2(GroupU5BU5D_t1880820351* value)
	{
		____groups_2 = value;
		Il2CppCodeGenWriteBarrier((&____groups_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPCOLLECTION_T69770484_H
#ifndef STRINGBUILDER_T_H
#define STRINGBUILDER_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.StringBuilder
struct  StringBuilder_t  : public RuntimeObject
{
public:
	// System.Char[] System.Text.StringBuilder::m_ChunkChars
	CharU5BU5D_t3528271667* ___m_ChunkChars_0;
	// System.Text.StringBuilder System.Text.StringBuilder::m_ChunkPrevious
	StringBuilder_t * ___m_ChunkPrevious_1;
	// System.Int32 System.Text.StringBuilder::m_ChunkLength
	int32_t ___m_ChunkLength_2;
	// System.Int32 System.Text.StringBuilder::m_ChunkOffset
	int32_t ___m_ChunkOffset_3;
	// System.Int32 System.Text.StringBuilder::m_MaxCapacity
	int32_t ___m_MaxCapacity_4;

public:
	inline static int32_t get_offset_of_m_ChunkChars_0() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkChars_0)); }
	inline CharU5BU5D_t3528271667* get_m_ChunkChars_0() const { return ___m_ChunkChars_0; }
	inline CharU5BU5D_t3528271667** get_address_of_m_ChunkChars_0() { return &___m_ChunkChars_0; }
	inline void set_m_ChunkChars_0(CharU5BU5D_t3528271667* value)
	{
		___m_ChunkChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChunkChars_0), value);
	}

	inline static int32_t get_offset_of_m_ChunkPrevious_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkPrevious_1)); }
	inline StringBuilder_t * get_m_ChunkPrevious_1() const { return ___m_ChunkPrevious_1; }
	inline StringBuilder_t ** get_address_of_m_ChunkPrevious_1() { return &___m_ChunkPrevious_1; }
	inline void set_m_ChunkPrevious_1(StringBuilder_t * value)
	{
		___m_ChunkPrevious_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChunkPrevious_1), value);
	}

	inline static int32_t get_offset_of_m_ChunkLength_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkLength_2)); }
	inline int32_t get_m_ChunkLength_2() const { return ___m_ChunkLength_2; }
	inline int32_t* get_address_of_m_ChunkLength_2() { return &___m_ChunkLength_2; }
	inline void set_m_ChunkLength_2(int32_t value)
	{
		___m_ChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_m_ChunkOffset_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkOffset_3)); }
	inline int32_t get_m_ChunkOffset_3() const { return ___m_ChunkOffset_3; }
	inline int32_t* get_address_of_m_ChunkOffset_3() { return &___m_ChunkOffset_3; }
	inline void set_m_ChunkOffset_3(int32_t value)
	{
		___m_ChunkOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_MaxCapacity_4)); }
	inline int32_t get_m_MaxCapacity_4() const { return ___m_MaxCapacity_4; }
	inline int32_t* get_address_of_m_MaxCapacity_4() { return &___m_MaxCapacity_4; }
	inline void set_m_MaxCapacity_4(int32_t value)
	{
		___m_MaxCapacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGBUILDER_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef OPTIONDATA_T3270282352_H
#define OPTIONDATA_T3270282352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Dropdown/OptionData
struct  OptionData_t3270282352  : public RuntimeObject
{
public:
	// System.String UnityEngine.UI.Dropdown/OptionData::m_Text
	String_t* ___m_Text_0;
	// UnityEngine.Sprite UnityEngine.UI.Dropdown/OptionData::m_Image
	Sprite_t280657092 * ___m_Image_1;

public:
	inline static int32_t get_offset_of_m_Text_0() { return static_cast<int32_t>(offsetof(OptionData_t3270282352, ___m_Text_0)); }
	inline String_t* get_m_Text_0() const { return ___m_Text_0; }
	inline String_t** get_address_of_m_Text_0() { return &___m_Text_0; }
	inline void set_m_Text_0(String_t* value)
	{
		___m_Text_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_0), value);
	}

	inline static int32_t get_offset_of_m_Image_1() { return static_cast<int32_t>(offsetof(OptionData_t3270282352, ___m_Image_1)); }
	inline Sprite_t280657092 * get_m_Image_1() const { return ___m_Image_1; }
	inline Sprite_t280657092 ** get_address_of_m_Image_1() { return &___m_Image_1; }
	inline void set_m_Image_1(Sprite_t280657092 * value)
	{
		___m_Image_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTIONDATA_T3270282352_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_t4116647657* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_t4116647657* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_t4116647657* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((&___categoryForLatin1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef KEYVALUEPAIR_2_T2530217319_H
#define KEYVALUEPAIR_2_T2530217319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t2530217319 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2530217319_H
#ifndef KEYVALUEPAIR_2_T2163821814_H
#define KEYVALUEPAIR_2_T2163821814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,DigitsNFCToolkit.JSON.JSONValue>
struct  KeyValuePair_2_t2163821814 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	JSONValue_t4275860644 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2163821814, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2163821814, ___value_1)); }
	inline JSONValue_t4275860644 * get_value_1() const { return ___value_1; }
	inline JSONValue_t4275860644 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(JSONValue_t4275860644 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2163821814_H
#ifndef ENUMERATOR_T3342211967_H
#define ENUMERATOR_T3342211967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<DigitsNFCToolkit.JSON.JSONValue>
struct  Enumerator_t3342211967 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t1452968090 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	JSONValue_t4275860644 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t3342211967, ___list_0)); }
	inline List_1_t1452968090 * get_list_0() const { return ___list_0; }
	inline List_1_t1452968090 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t1452968090 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t3342211967, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t3342211967, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3342211967, ___current_3)); }
	inline JSONValue_t4275860644 * get_current_3() const { return ___current_3; }
	inline JSONValue_t4275860644 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(JSONValue_t4275860644 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3342211967_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t257213610 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___list_0)); }
	inline List_1_t257213610 * get_list_0() const { return ___list_0; }
	inline List_1_t257213610 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t257213610 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t594665363_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t594665363_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef GROUP_T2468205786_H
#define GROUP_T2468205786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Group
struct  Group_t2468205786  : public Capture_t2232016050
{
public:
	// System.Int32[] System.Text.RegularExpressions.Group::_caps
	Int32U5BU5D_t385246372* ____caps_4;
	// System.Int32 System.Text.RegularExpressions.Group::_capcount
	int32_t ____capcount_5;
	// System.Text.RegularExpressions.CaptureCollection System.Text.RegularExpressions.Group::_capcoll
	CaptureCollection_t1760593541 * ____capcoll_6;
	// System.String System.Text.RegularExpressions.Group::_name
	String_t* ____name_7;

public:
	inline static int32_t get_offset_of__caps_4() { return static_cast<int32_t>(offsetof(Group_t2468205786, ____caps_4)); }
	inline Int32U5BU5D_t385246372* get__caps_4() const { return ____caps_4; }
	inline Int32U5BU5D_t385246372** get_address_of__caps_4() { return &____caps_4; }
	inline void set__caps_4(Int32U5BU5D_t385246372* value)
	{
		____caps_4 = value;
		Il2CppCodeGenWriteBarrier((&____caps_4), value);
	}

	inline static int32_t get_offset_of__capcount_5() { return static_cast<int32_t>(offsetof(Group_t2468205786, ____capcount_5)); }
	inline int32_t get__capcount_5() const { return ____capcount_5; }
	inline int32_t* get_address_of__capcount_5() { return &____capcount_5; }
	inline void set__capcount_5(int32_t value)
	{
		____capcount_5 = value;
	}

	inline static int32_t get_offset_of__capcoll_6() { return static_cast<int32_t>(offsetof(Group_t2468205786, ____capcoll_6)); }
	inline CaptureCollection_t1760593541 * get__capcoll_6() const { return ____capcoll_6; }
	inline CaptureCollection_t1760593541 ** get_address_of__capcoll_6() { return &____capcoll_6; }
	inline void set__capcoll_6(CaptureCollection_t1760593541 * value)
	{
		____capcoll_6 = value;
		Il2CppCodeGenWriteBarrier((&____capcoll_6), value);
	}

	inline static int32_t get_offset_of__name_7() { return static_cast<int32_t>(offsetof(Group_t2468205786, ____name_7)); }
	inline String_t* get__name_7() const { return ____name_7; }
	inline String_t** get_address_of__name_7() { return &____name_7; }
	inline void set__name_7(String_t* value)
	{
		____name_7 = value;
		Il2CppCodeGenWriteBarrier((&____name_7), value);
	}
};

struct Group_t2468205786_StaticFields
{
public:
	// System.Text.RegularExpressions.Group System.Text.RegularExpressions.Group::_emptygroup
	Group_t2468205786 * ____emptygroup_3;

public:
	inline static int32_t get_offset_of__emptygroup_3() { return static_cast<int32_t>(offsetof(Group_t2468205786_StaticFields, ____emptygroup_3)); }
	inline Group_t2468205786 * get__emptygroup_3() const { return ____emptygroup_3; }
	inline Group_t2468205786 ** get_address_of__emptygroup_3() { return &____emptygroup_3; }
	inline void set__emptygroup_3(Group_t2468205786 * value)
	{
		____emptygroup_3 = value;
		Il2CppCodeGenWriteBarrier((&____emptygroup_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUP_T2468205786_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_t2562230146__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef JSONPARSINGSTATE_T2722137651_H
#define JSONPARSINGSTATE_T2722137651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.JSON.JSONObject/JSONParsingState
struct  JSONParsingState_t2722137651 
{
public:
	// System.Int32 DigitsNFCToolkit.JSON.JSONObject/JSONParsingState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JSONParsingState_t2722137651, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPARSINGSTATE_T2722137651_H
#ifndef JSONVALUETYPE_T3019243058_H
#define JSONVALUETYPE_T3019243058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.JSON.JSONValueType
struct  JSONValueType_t3019243058 
{
public:
	// System.Int32 DigitsNFCToolkit.JSON.JSONValueType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JSONValueType_t3019243058, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONVALUETYPE_T3019243058_H
#ifndef NDEFMESSAGEWRITEERROR_T164821916_H
#define NDEFMESSAGEWRITEERROR_T164821916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFMessageWriteError
struct  NDEFMessageWriteError_t164821916 
{
public:
	// System.Int32 DigitsNFCToolkit.NDEFMessageWriteError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NDEFMessageWriteError_t164821916, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFMESSAGEWRITEERROR_T164821916_H
#ifndef NDEFMESSAGEWRITESTATE_T2549748319_H
#define NDEFMESSAGEWRITESTATE_T2549748319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFMessageWriteState
struct  NDEFMessageWriteState_t2549748319 
{
public:
	// System.Int32 DigitsNFCToolkit.NDEFMessageWriteState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NDEFMessageWriteState_t2549748319, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFMESSAGEWRITESTATE_T2549748319_H
#ifndef NDEFREADERROR_T886852181_H
#define NDEFREADERROR_T886852181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFReadError
struct  NDEFReadError_t886852181 
{
public:
	// System.Int32 DigitsNFCToolkit.NDEFReadError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NDEFReadError_t886852181, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFREADERROR_T886852181_H
#ifndef NDEFRECORDTYPE_T4294622310_H
#define NDEFRECORDTYPE_T4294622310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFRecordType
struct  NDEFRecordType_t4294622310 
{
public:
	// System.Int32 DigitsNFCToolkit.NDEFRecordType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NDEFRecordType_t4294622310, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFRECORDTYPE_T4294622310_H
#ifndef NDEFWRITEERROR_T890528595_H
#define NDEFWRITEERROR_T890528595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFWriteError
struct  NDEFWriteError_t890528595 
{
public:
	// System.Int32 DigitsNFCToolkit.NDEFWriteError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NDEFWriteError_t890528595, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFWRITEERROR_T890528595_H
#ifndef NFCTECHNOLOGY_T1376062623_H
#define NFCTECHNOLOGY_T1376062623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NFCTechnology
struct  NFCTechnology_t1376062623 
{
public:
	// System.Int32 DigitsNFCToolkit.NFCTechnology::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NFCTechnology_t1376062623, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NFCTECHNOLOGY_T1376062623_H
#ifndef ICONID_T582738576_H
#define ICONID_T582738576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.WriteScreenControl/IconID
struct  IconID_t582738576 
{
public:
	// System.Int32 DigitsNFCToolkit.Samples.WriteScreenControl/IconID::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IconID_t582738576, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICONID_T582738576_H
#ifndef RECOMMENDEDACTION_T1182550772_H
#define RECOMMENDEDACTION_T1182550772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.SmartPosterRecord/RecommendedAction
struct  RecommendedAction_t1182550772 
{
public:
	// System.Int32 DigitsNFCToolkit.SmartPosterRecord/RecommendedAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RecommendedAction_t1182550772, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECOMMENDEDACTION_T1182550772_H
#ifndef TEXTENCODING_T3210513626_H
#define TEXTENCODING_T3210513626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.TextRecord/TextEncoding
struct  TextEncoding_t3210513626 
{
public:
	// System.Int32 DigitsNFCToolkit.TextRecord/TextEncoding::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextEncoding_t3210513626, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTENCODING_T3210513626_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef NUMBERSTYLES_T617258130_H
#define NUMBERSTYLES_T617258130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.NumberStyles
struct  NumberStyles_t617258130 
{
public:
	// System.Int32 System.Globalization.NumberStyles::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NumberStyles_t617258130, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMBERSTYLES_T617258130_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef MATCH_T3408321083_H
#define MATCH_T3408321083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Match
struct  Match_t3408321083  : public Group_t2468205786
{
public:
	// System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::_groupcoll
	GroupCollection_t69770484 * ____groupcoll_9;
	// System.Text.RegularExpressions.Regex System.Text.RegularExpressions.Match::_regex
	Regex_t3657309853 * ____regex_10;
	// System.Int32 System.Text.RegularExpressions.Match::_textbeg
	int32_t ____textbeg_11;
	// System.Int32 System.Text.RegularExpressions.Match::_textpos
	int32_t ____textpos_12;
	// System.Int32 System.Text.RegularExpressions.Match::_textend
	int32_t ____textend_13;
	// System.Int32 System.Text.RegularExpressions.Match::_textstart
	int32_t ____textstart_14;
	// System.Int32[][] System.Text.RegularExpressions.Match::_matches
	Int32U5BU5DU5BU5D_t3365920845* ____matches_15;
	// System.Int32[] System.Text.RegularExpressions.Match::_matchcount
	Int32U5BU5D_t385246372* ____matchcount_16;
	// System.Boolean System.Text.RegularExpressions.Match::_balancing
	bool ____balancing_17;

public:
	inline static int32_t get_offset_of__groupcoll_9() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____groupcoll_9)); }
	inline GroupCollection_t69770484 * get__groupcoll_9() const { return ____groupcoll_9; }
	inline GroupCollection_t69770484 ** get_address_of__groupcoll_9() { return &____groupcoll_9; }
	inline void set__groupcoll_9(GroupCollection_t69770484 * value)
	{
		____groupcoll_9 = value;
		Il2CppCodeGenWriteBarrier((&____groupcoll_9), value);
	}

	inline static int32_t get_offset_of__regex_10() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____regex_10)); }
	inline Regex_t3657309853 * get__regex_10() const { return ____regex_10; }
	inline Regex_t3657309853 ** get_address_of__regex_10() { return &____regex_10; }
	inline void set__regex_10(Regex_t3657309853 * value)
	{
		____regex_10 = value;
		Il2CppCodeGenWriteBarrier((&____regex_10), value);
	}

	inline static int32_t get_offset_of__textbeg_11() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____textbeg_11)); }
	inline int32_t get__textbeg_11() const { return ____textbeg_11; }
	inline int32_t* get_address_of__textbeg_11() { return &____textbeg_11; }
	inline void set__textbeg_11(int32_t value)
	{
		____textbeg_11 = value;
	}

	inline static int32_t get_offset_of__textpos_12() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____textpos_12)); }
	inline int32_t get__textpos_12() const { return ____textpos_12; }
	inline int32_t* get_address_of__textpos_12() { return &____textpos_12; }
	inline void set__textpos_12(int32_t value)
	{
		____textpos_12 = value;
	}

	inline static int32_t get_offset_of__textend_13() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____textend_13)); }
	inline int32_t get__textend_13() const { return ____textend_13; }
	inline int32_t* get_address_of__textend_13() { return &____textend_13; }
	inline void set__textend_13(int32_t value)
	{
		____textend_13 = value;
	}

	inline static int32_t get_offset_of__textstart_14() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____textstart_14)); }
	inline int32_t get__textstart_14() const { return ____textstart_14; }
	inline int32_t* get_address_of__textstart_14() { return &____textstart_14; }
	inline void set__textstart_14(int32_t value)
	{
		____textstart_14 = value;
	}

	inline static int32_t get_offset_of__matches_15() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____matches_15)); }
	inline Int32U5BU5DU5BU5D_t3365920845* get__matches_15() const { return ____matches_15; }
	inline Int32U5BU5DU5BU5D_t3365920845** get_address_of__matches_15() { return &____matches_15; }
	inline void set__matches_15(Int32U5BU5DU5BU5D_t3365920845* value)
	{
		____matches_15 = value;
		Il2CppCodeGenWriteBarrier((&____matches_15), value);
	}

	inline static int32_t get_offset_of__matchcount_16() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____matchcount_16)); }
	inline Int32U5BU5D_t385246372* get__matchcount_16() const { return ____matchcount_16; }
	inline Int32U5BU5D_t385246372** get_address_of__matchcount_16() { return &____matchcount_16; }
	inline void set__matchcount_16(Int32U5BU5D_t385246372* value)
	{
		____matchcount_16 = value;
		Il2CppCodeGenWriteBarrier((&____matchcount_16), value);
	}

	inline static int32_t get_offset_of__balancing_17() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____balancing_17)); }
	inline bool get__balancing_17() const { return ____balancing_17; }
	inline bool* get_address_of__balancing_17() { return &____balancing_17; }
	inline void set__balancing_17(bool value)
	{
		____balancing_17 = value;
	}
};

struct Match_t3408321083_StaticFields
{
public:
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Match::_empty
	Match_t3408321083 * ____empty_8;

public:
	inline static int32_t get_offset_of__empty_8() { return static_cast<int32_t>(offsetof(Match_t3408321083_StaticFields, ____empty_8)); }
	inline Match_t3408321083 * get__empty_8() const { return ____empty_8; }
	inline Match_t3408321083 ** get_address_of__empty_8() { return &____empty_8; }
	inline void set__empty_8(Match_t3408321083 * value)
	{
		____empty_8 = value;
		Il2CppCodeGenWriteBarrier((&____empty_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCH_T3408321083_H
#ifndef REGEXOPTIONS_T92845595_H
#define REGEXOPTIONS_T92845595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexOptions
struct  RegexOptions_t92845595 
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RegexOptions_t92845595, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXOPTIONS_T92845595_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_19)); }
	inline TimeSpan_t881159249  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_t881159249 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_t881159249  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_t881159249  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_t881159249  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_21)); }
	inline TimeSpan_t881159249  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_t881159249  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef FILTERMODE_T3761284007_H
#define FILTERMODE_T3761284007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t3761284007 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterMode_t3761284007, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T3761284007_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TEXTUREFORMAT_T2701165832_H
#define TEXTUREFORMAT_T2701165832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2701165832 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_t2701165832, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2701165832_H
#ifndef TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#define TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboardType
struct  TouchScreenKeyboardType_t1530597702 
{
public:
	// System.Int32 UnityEngine.TouchScreenKeyboardType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchScreenKeyboardType_t1530597702, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef FILLMETHOD_T1167457570_H
#define FILLMETHOD_T1167457570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1167457570 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_t1167457570, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1167457570_H
#ifndef TYPE_T1152881528_H
#define TYPE_T1152881528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t1152881528 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t1152881528, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1152881528_H
#ifndef CHARACTERVALIDATION_T4051914437_H
#define CHARACTERVALIDATION_T4051914437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/CharacterValidation
struct  CharacterValidation_t4051914437 
{
public:
	// System.Int32 UnityEngine.UI.InputField/CharacterValidation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CharacterValidation_t4051914437, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERVALIDATION_T4051914437_H
#ifndef CONTENTTYPE_T1787303396_H
#define CONTENTTYPE_T1787303396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/ContentType
struct  ContentType_t1787303396 
{
public:
	// System.Int32 UnityEngine.UI.InputField/ContentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ContentType_t1787303396, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTTYPE_T1787303396_H
#ifndef INPUTTYPE_T1770400679_H
#define INPUTTYPE_T1770400679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/InputType
struct  InputType_t1770400679 
{
public:
	// System.Int32 UnityEngine.UI.InputField/InputType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputType_t1770400679, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTTYPE_T1770400679_H
#ifndef LINETYPE_T4214648469_H
#define LINETYPE_T4214648469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/LineType
struct  LineType_t4214648469 
{
public:
	// System.Int32 UnityEngine.UI.InputField/LineType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LineType_t4214648469, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETYPE_T4214648469_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef MOVEMENTTYPE_T4072922106_H
#define MOVEMENTTYPE_T4072922106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect/MovementType
struct  MovementType_t4072922106 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/MovementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MovementType_t4072922106, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTTYPE_T4072922106_H
#ifndef SCROLLBARVISIBILITY_T705693775_H
#define SCROLLBARVISIBILITY_T705693775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect/ScrollbarVisibility
struct  ScrollbarVisibility_t705693775 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/ScrollbarVisibility::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScrollbarVisibility_t705693775, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLBARVISIBILITY_T705693775_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef JSONVALUE_T4275860644_H
#define JSONVALUE_T4275860644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.JSON.JSONValue
struct  JSONValue_t4275860644  : public RuntimeObject
{
public:
	// DigitsNFCToolkit.JSON.JSONValueType DigitsNFCToolkit.JSON.JSONValue::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_0;
	// System.String DigitsNFCToolkit.JSON.JSONValue::<String>k__BackingField
	String_t* ___U3CStringU3Ek__BackingField_1;
	// System.Double DigitsNFCToolkit.JSON.JSONValue::<Double>k__BackingField
	double ___U3CDoubleU3Ek__BackingField_2;
	// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.JSON.JSONValue::<Object>k__BackingField
	JSONObject_t321714843 * ___U3CObjectU3Ek__BackingField_3;
	// DigitsNFCToolkit.JSON.JSONArray DigitsNFCToolkit.JSON.JSONValue::<Array>k__BackingField
	JSONArray_t4024675823 * ___U3CArrayU3Ek__BackingField_4;
	// System.Boolean DigitsNFCToolkit.JSON.JSONValue::<Boolean>k__BackingField
	bool ___U3CBooleanU3Ek__BackingField_5;
	// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONValue::<Parent>k__BackingField
	JSONValue_t4275860644 * ___U3CParentU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JSONValue_t4275860644, ___U3CTypeU3Ek__BackingField_0)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_0() const { return ___U3CTypeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_0() { return &___U3CTypeU3Ek__BackingField_0; }
	inline void set_U3CTypeU3Ek__BackingField_0(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStringU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JSONValue_t4275860644, ___U3CStringU3Ek__BackingField_1)); }
	inline String_t* get_U3CStringU3Ek__BackingField_1() const { return ___U3CStringU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CStringU3Ek__BackingField_1() { return &___U3CStringU3Ek__BackingField_1; }
	inline void set_U3CStringU3Ek__BackingField_1(String_t* value)
	{
		___U3CStringU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStringU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDoubleU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(JSONValue_t4275860644, ___U3CDoubleU3Ek__BackingField_2)); }
	inline double get_U3CDoubleU3Ek__BackingField_2() const { return ___U3CDoubleU3Ek__BackingField_2; }
	inline double* get_address_of_U3CDoubleU3Ek__BackingField_2() { return &___U3CDoubleU3Ek__BackingField_2; }
	inline void set_U3CDoubleU3Ek__BackingField_2(double value)
	{
		___U3CDoubleU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CObjectU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(JSONValue_t4275860644, ___U3CObjectU3Ek__BackingField_3)); }
	inline JSONObject_t321714843 * get_U3CObjectU3Ek__BackingField_3() const { return ___U3CObjectU3Ek__BackingField_3; }
	inline JSONObject_t321714843 ** get_address_of_U3CObjectU3Ek__BackingField_3() { return &___U3CObjectU3Ek__BackingField_3; }
	inline void set_U3CObjectU3Ek__BackingField_3(JSONObject_t321714843 * value)
	{
		___U3CObjectU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjectU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CArrayU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(JSONValue_t4275860644, ___U3CArrayU3Ek__BackingField_4)); }
	inline JSONArray_t4024675823 * get_U3CArrayU3Ek__BackingField_4() const { return ___U3CArrayU3Ek__BackingField_4; }
	inline JSONArray_t4024675823 ** get_address_of_U3CArrayU3Ek__BackingField_4() { return &___U3CArrayU3Ek__BackingField_4; }
	inline void set_U3CArrayU3Ek__BackingField_4(JSONArray_t4024675823 * value)
	{
		___U3CArrayU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArrayU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CBooleanU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(JSONValue_t4275860644, ___U3CBooleanU3Ek__BackingField_5)); }
	inline bool get_U3CBooleanU3Ek__BackingField_5() const { return ___U3CBooleanU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CBooleanU3Ek__BackingField_5() { return &___U3CBooleanU3Ek__BackingField_5; }
	inline void set_U3CBooleanU3Ek__BackingField_5(bool value)
	{
		___U3CBooleanU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CParentU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JSONValue_t4275860644, ___U3CParentU3Ek__BackingField_6)); }
	inline JSONValue_t4275860644 * get_U3CParentU3Ek__BackingField_6() const { return ___U3CParentU3Ek__BackingField_6; }
	inline JSONValue_t4275860644 ** get_address_of_U3CParentU3Ek__BackingField_6() { return &___U3CParentU3Ek__BackingField_6; }
	inline void set_U3CParentU3Ek__BackingField_6(JSONValue_t4275860644 * value)
	{
		___U3CParentU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONVALUE_T4275860644_H
#ifndef NDEFMESSAGE_T279637043_H
#define NDEFMESSAGE_T279637043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFMessage
struct  NDEFMessage_t279637043  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<DigitsNFCToolkit.NDEFRecord> DigitsNFCToolkit.NDEFMessage::records
	List_1_t4162620298 * ___records_0;
	// System.String DigitsNFCToolkit.NDEFMessage::tagID
	String_t* ___tagID_1;
	// DigitsNFCToolkit.NDEFMessageWriteState DigitsNFCToolkit.NDEFMessage::writeState
	int32_t ___writeState_2;
	// DigitsNFCToolkit.NDEFMessageWriteError DigitsNFCToolkit.NDEFMessage::writeError
	int32_t ___writeError_3;

public:
	inline static int32_t get_offset_of_records_0() { return static_cast<int32_t>(offsetof(NDEFMessage_t279637043, ___records_0)); }
	inline List_1_t4162620298 * get_records_0() const { return ___records_0; }
	inline List_1_t4162620298 ** get_address_of_records_0() { return &___records_0; }
	inline void set_records_0(List_1_t4162620298 * value)
	{
		___records_0 = value;
		Il2CppCodeGenWriteBarrier((&___records_0), value);
	}

	inline static int32_t get_offset_of_tagID_1() { return static_cast<int32_t>(offsetof(NDEFMessage_t279637043, ___tagID_1)); }
	inline String_t* get_tagID_1() const { return ___tagID_1; }
	inline String_t** get_address_of_tagID_1() { return &___tagID_1; }
	inline void set_tagID_1(String_t* value)
	{
		___tagID_1 = value;
		Il2CppCodeGenWriteBarrier((&___tagID_1), value);
	}

	inline static int32_t get_offset_of_writeState_2() { return static_cast<int32_t>(offsetof(NDEFMessage_t279637043, ___writeState_2)); }
	inline int32_t get_writeState_2() const { return ___writeState_2; }
	inline int32_t* get_address_of_writeState_2() { return &___writeState_2; }
	inline void set_writeState_2(int32_t value)
	{
		___writeState_2 = value;
	}

	inline static int32_t get_offset_of_writeError_3() { return static_cast<int32_t>(offsetof(NDEFMessage_t279637043, ___writeError_3)); }
	inline int32_t get_writeError_3() const { return ___writeError_3; }
	inline int32_t* get_address_of_writeError_3() { return &___writeError_3; }
	inline void set_writeError_3(int32_t value)
	{
		___writeError_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFMESSAGE_T279637043_H
#ifndef NDEFREADRESULT_T3483243621_H
#define NDEFREADRESULT_T3483243621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFReadResult
struct  NDEFReadResult_t3483243621  : public RuntimeObject
{
public:
	// System.Boolean DigitsNFCToolkit.NDEFReadResult::success
	bool ___success_0;
	// DigitsNFCToolkit.NDEFReadError DigitsNFCToolkit.NDEFReadResult::error
	int32_t ___error_1;
	// DigitsNFCToolkit.NDEFMessage DigitsNFCToolkit.NDEFReadResult::message
	NDEFMessage_t279637043 * ___message_2;
	// System.String DigitsNFCToolkit.NDEFReadResult::tagID
	String_t* ___tagID_3;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(NDEFReadResult_t3483243621, ___success_0)); }
	inline bool get_success_0() const { return ___success_0; }
	inline bool* get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(bool value)
	{
		___success_0 = value;
	}

	inline static int32_t get_offset_of_error_1() { return static_cast<int32_t>(offsetof(NDEFReadResult_t3483243621, ___error_1)); }
	inline int32_t get_error_1() const { return ___error_1; }
	inline int32_t* get_address_of_error_1() { return &___error_1; }
	inline void set_error_1(int32_t value)
	{
		___error_1 = value;
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(NDEFReadResult_t3483243621, ___message_2)); }
	inline NDEFMessage_t279637043 * get_message_2() const { return ___message_2; }
	inline NDEFMessage_t279637043 ** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(NDEFMessage_t279637043 * value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_tagID_3() { return static_cast<int32_t>(offsetof(NDEFReadResult_t3483243621, ___tagID_3)); }
	inline String_t* get_tagID_3() const { return ___tagID_3; }
	inline String_t** get_address_of_tagID_3() { return &___tagID_3; }
	inline void set_tagID_3(String_t* value)
	{
		___tagID_3 = value;
		Il2CppCodeGenWriteBarrier((&___tagID_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFREADRESULT_T3483243621_H
#ifndef NDEFRECORD_T2690545556_H
#define NDEFRECORD_T2690545556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFRecord
struct  NDEFRecord_t2690545556  : public RuntimeObject
{
public:
	// DigitsNFCToolkit.NDEFRecordType DigitsNFCToolkit.NDEFRecord::type
	int32_t ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(NDEFRecord_t2690545556, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFRECORD_T2690545556_H
#ifndef NDEFWRITERESULT_T4210562629_H
#define NDEFWRITERESULT_T4210562629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFWriteResult
struct  NDEFWriteResult_t4210562629  : public RuntimeObject
{
public:
	// System.Boolean DigitsNFCToolkit.NDEFWriteResult::success
	bool ___success_0;
	// DigitsNFCToolkit.NDEFWriteError DigitsNFCToolkit.NDEFWriteResult::error
	int32_t ___error_1;
	// DigitsNFCToolkit.NDEFMessage DigitsNFCToolkit.NDEFWriteResult::message
	NDEFMessage_t279637043 * ___message_2;
	// System.String DigitsNFCToolkit.NDEFWriteResult::tagID
	String_t* ___tagID_3;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(NDEFWriteResult_t4210562629, ___success_0)); }
	inline bool get_success_0() const { return ___success_0; }
	inline bool* get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(bool value)
	{
		___success_0 = value;
	}

	inline static int32_t get_offset_of_error_1() { return static_cast<int32_t>(offsetof(NDEFWriteResult_t4210562629, ___error_1)); }
	inline int32_t get_error_1() const { return ___error_1; }
	inline int32_t* get_address_of_error_1() { return &___error_1; }
	inline void set_error_1(int32_t value)
	{
		___error_1 = value;
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(NDEFWriteResult_t4210562629, ___message_2)); }
	inline NDEFMessage_t279637043 * get_message_2() const { return ___message_2; }
	inline NDEFMessage_t279637043 ** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(NDEFMessage_t279637043 * value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_tagID_3() { return static_cast<int32_t>(offsetof(NDEFWriteResult_t4210562629, ___tagID_3)); }
	inline String_t* get_tagID_3() const { return ___tagID_3; }
	inline String_t** get_address_of_tagID_3() { return &___tagID_3; }
	inline void set_tagID_3(String_t* value)
	{
		___tagID_3 = value;
		Il2CppCodeGenWriteBarrier((&___tagID_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFWRITERESULT_T4210562629_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef REGEX_T3657309853_H
#define REGEX_T3657309853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Regex
struct  Regex_t3657309853  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.Regex::pattern
	String_t* ___pattern_0;
	// System.Text.RegularExpressions.RegexRunnerFactory System.Text.RegularExpressions.Regex::factory
	RegexRunnerFactory_t51159052 * ___factory_1;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.Regex::roptions
	int32_t ___roptions_2;
	// System.TimeSpan System.Text.RegularExpressions.Regex::internalMatchTimeout
	TimeSpan_t881159249  ___internalMatchTimeout_5;
	// System.Collections.Hashtable System.Text.RegularExpressions.Regex::caps
	Hashtable_t1853889766 * ___caps_9;
	// System.Collections.Hashtable System.Text.RegularExpressions.Regex::capnames
	Hashtable_t1853889766 * ___capnames_10;
	// System.String[] System.Text.RegularExpressions.Regex::capslist
	StringU5BU5D_t1281789340* ___capslist_11;
	// System.Int32 System.Text.RegularExpressions.Regex::capsize
	int32_t ___capsize_12;
	// System.Text.RegularExpressions.ExclusiveReference System.Text.RegularExpressions.Regex::runnerref
	ExclusiveReference_t1927754563 * ___runnerref_13;
	// System.Text.RegularExpressions.SharedReference System.Text.RegularExpressions.Regex::replref
	SharedReference_t2916547576 * ___replref_14;
	// System.Text.RegularExpressions.RegexCode System.Text.RegularExpressions.Regex::code
	RegexCode_t4293407246 * ___code_15;
	// System.Boolean System.Text.RegularExpressions.Regex::refsInitialized
	bool ___refsInitialized_16;

public:
	inline static int32_t get_offset_of_pattern_0() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___pattern_0)); }
	inline String_t* get_pattern_0() const { return ___pattern_0; }
	inline String_t** get_address_of_pattern_0() { return &___pattern_0; }
	inline void set_pattern_0(String_t* value)
	{
		___pattern_0 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_0), value);
	}

	inline static int32_t get_offset_of_factory_1() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___factory_1)); }
	inline RegexRunnerFactory_t51159052 * get_factory_1() const { return ___factory_1; }
	inline RegexRunnerFactory_t51159052 ** get_address_of_factory_1() { return &___factory_1; }
	inline void set_factory_1(RegexRunnerFactory_t51159052 * value)
	{
		___factory_1 = value;
		Il2CppCodeGenWriteBarrier((&___factory_1), value);
	}

	inline static int32_t get_offset_of_roptions_2() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___roptions_2)); }
	inline int32_t get_roptions_2() const { return ___roptions_2; }
	inline int32_t* get_address_of_roptions_2() { return &___roptions_2; }
	inline void set_roptions_2(int32_t value)
	{
		___roptions_2 = value;
	}

	inline static int32_t get_offset_of_internalMatchTimeout_5() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___internalMatchTimeout_5)); }
	inline TimeSpan_t881159249  get_internalMatchTimeout_5() const { return ___internalMatchTimeout_5; }
	inline TimeSpan_t881159249 * get_address_of_internalMatchTimeout_5() { return &___internalMatchTimeout_5; }
	inline void set_internalMatchTimeout_5(TimeSpan_t881159249  value)
	{
		___internalMatchTimeout_5 = value;
	}

	inline static int32_t get_offset_of_caps_9() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___caps_9)); }
	inline Hashtable_t1853889766 * get_caps_9() const { return ___caps_9; }
	inline Hashtable_t1853889766 ** get_address_of_caps_9() { return &___caps_9; }
	inline void set_caps_9(Hashtable_t1853889766 * value)
	{
		___caps_9 = value;
		Il2CppCodeGenWriteBarrier((&___caps_9), value);
	}

	inline static int32_t get_offset_of_capnames_10() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___capnames_10)); }
	inline Hashtable_t1853889766 * get_capnames_10() const { return ___capnames_10; }
	inline Hashtable_t1853889766 ** get_address_of_capnames_10() { return &___capnames_10; }
	inline void set_capnames_10(Hashtable_t1853889766 * value)
	{
		___capnames_10 = value;
		Il2CppCodeGenWriteBarrier((&___capnames_10), value);
	}

	inline static int32_t get_offset_of_capslist_11() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___capslist_11)); }
	inline StringU5BU5D_t1281789340* get_capslist_11() const { return ___capslist_11; }
	inline StringU5BU5D_t1281789340** get_address_of_capslist_11() { return &___capslist_11; }
	inline void set_capslist_11(StringU5BU5D_t1281789340* value)
	{
		___capslist_11 = value;
		Il2CppCodeGenWriteBarrier((&___capslist_11), value);
	}

	inline static int32_t get_offset_of_capsize_12() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___capsize_12)); }
	inline int32_t get_capsize_12() const { return ___capsize_12; }
	inline int32_t* get_address_of_capsize_12() { return &___capsize_12; }
	inline void set_capsize_12(int32_t value)
	{
		___capsize_12 = value;
	}

	inline static int32_t get_offset_of_runnerref_13() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___runnerref_13)); }
	inline ExclusiveReference_t1927754563 * get_runnerref_13() const { return ___runnerref_13; }
	inline ExclusiveReference_t1927754563 ** get_address_of_runnerref_13() { return &___runnerref_13; }
	inline void set_runnerref_13(ExclusiveReference_t1927754563 * value)
	{
		___runnerref_13 = value;
		Il2CppCodeGenWriteBarrier((&___runnerref_13), value);
	}

	inline static int32_t get_offset_of_replref_14() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___replref_14)); }
	inline SharedReference_t2916547576 * get_replref_14() const { return ___replref_14; }
	inline SharedReference_t2916547576 ** get_address_of_replref_14() { return &___replref_14; }
	inline void set_replref_14(SharedReference_t2916547576 * value)
	{
		___replref_14 = value;
		Il2CppCodeGenWriteBarrier((&___replref_14), value);
	}

	inline static int32_t get_offset_of_code_15() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___code_15)); }
	inline RegexCode_t4293407246 * get_code_15() const { return ___code_15; }
	inline RegexCode_t4293407246 ** get_address_of_code_15() { return &___code_15; }
	inline void set_code_15(RegexCode_t4293407246 * value)
	{
		___code_15 = value;
		Il2CppCodeGenWriteBarrier((&___code_15), value);
	}

	inline static int32_t get_offset_of_refsInitialized_16() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___refsInitialized_16)); }
	inline bool get_refsInitialized_16() const { return ___refsInitialized_16; }
	inline bool* get_address_of_refsInitialized_16() { return &___refsInitialized_16; }
	inline void set_refsInitialized_16(bool value)
	{
		___refsInitialized_16 = value;
	}
};

struct Regex_t3657309853_StaticFields
{
public:
	// System.TimeSpan System.Text.RegularExpressions.Regex::MaximumMatchTimeout
	TimeSpan_t881159249  ___MaximumMatchTimeout_3;
	// System.TimeSpan System.Text.RegularExpressions.Regex::InfiniteMatchTimeout
	TimeSpan_t881159249  ___InfiniteMatchTimeout_4;
	// System.TimeSpan System.Text.RegularExpressions.Regex::FallbackDefaultMatchTimeout
	TimeSpan_t881159249  ___FallbackDefaultMatchTimeout_7;
	// System.TimeSpan System.Text.RegularExpressions.Regex::DefaultMatchTimeout
	TimeSpan_t881159249  ___DefaultMatchTimeout_8;
	// System.Collections.Generic.LinkedList`1<System.Text.RegularExpressions.CachedCodeEntry> System.Text.RegularExpressions.Regex::livecode
	LinkedList_1_t3068621835 * ___livecode_17;
	// System.Int32 System.Text.RegularExpressions.Regex::cacheSize
	int32_t ___cacheSize_18;

public:
	inline static int32_t get_offset_of_MaximumMatchTimeout_3() { return static_cast<int32_t>(offsetof(Regex_t3657309853_StaticFields, ___MaximumMatchTimeout_3)); }
	inline TimeSpan_t881159249  get_MaximumMatchTimeout_3() const { return ___MaximumMatchTimeout_3; }
	inline TimeSpan_t881159249 * get_address_of_MaximumMatchTimeout_3() { return &___MaximumMatchTimeout_3; }
	inline void set_MaximumMatchTimeout_3(TimeSpan_t881159249  value)
	{
		___MaximumMatchTimeout_3 = value;
	}

	inline static int32_t get_offset_of_InfiniteMatchTimeout_4() { return static_cast<int32_t>(offsetof(Regex_t3657309853_StaticFields, ___InfiniteMatchTimeout_4)); }
	inline TimeSpan_t881159249  get_InfiniteMatchTimeout_4() const { return ___InfiniteMatchTimeout_4; }
	inline TimeSpan_t881159249 * get_address_of_InfiniteMatchTimeout_4() { return &___InfiniteMatchTimeout_4; }
	inline void set_InfiniteMatchTimeout_4(TimeSpan_t881159249  value)
	{
		___InfiniteMatchTimeout_4 = value;
	}

	inline static int32_t get_offset_of_FallbackDefaultMatchTimeout_7() { return static_cast<int32_t>(offsetof(Regex_t3657309853_StaticFields, ___FallbackDefaultMatchTimeout_7)); }
	inline TimeSpan_t881159249  get_FallbackDefaultMatchTimeout_7() const { return ___FallbackDefaultMatchTimeout_7; }
	inline TimeSpan_t881159249 * get_address_of_FallbackDefaultMatchTimeout_7() { return &___FallbackDefaultMatchTimeout_7; }
	inline void set_FallbackDefaultMatchTimeout_7(TimeSpan_t881159249  value)
	{
		___FallbackDefaultMatchTimeout_7 = value;
	}

	inline static int32_t get_offset_of_DefaultMatchTimeout_8() { return static_cast<int32_t>(offsetof(Regex_t3657309853_StaticFields, ___DefaultMatchTimeout_8)); }
	inline TimeSpan_t881159249  get_DefaultMatchTimeout_8() const { return ___DefaultMatchTimeout_8; }
	inline TimeSpan_t881159249 * get_address_of_DefaultMatchTimeout_8() { return &___DefaultMatchTimeout_8; }
	inline void set_DefaultMatchTimeout_8(TimeSpan_t881159249  value)
	{
		___DefaultMatchTimeout_8 = value;
	}

	inline static int32_t get_offset_of_livecode_17() { return static_cast<int32_t>(offsetof(Regex_t3657309853_StaticFields, ___livecode_17)); }
	inline LinkedList_1_t3068621835 * get_livecode_17() const { return ___livecode_17; }
	inline LinkedList_1_t3068621835 ** get_address_of_livecode_17() { return &___livecode_17; }
	inline void set_livecode_17(LinkedList_1_t3068621835 * value)
	{
		___livecode_17 = value;
		Il2CppCodeGenWriteBarrier((&___livecode_17), value);
	}

	inline static int32_t get_offset_of_cacheSize_18() { return static_cast<int32_t>(offsetof(Regex_t3657309853_StaticFields, ___cacheSize_18)); }
	inline int32_t get_cacheSize_18() const { return ___cacheSize_18; }
	inline int32_t* get_address_of_cacheSize_18() { return &___cacheSize_18; }
	inline void set_cacheSize_18(int32_t value)
	{
		___cacheSize_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEX_T3657309853_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2999457153 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_0), value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t426314064 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t426314064 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_1), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_3), value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_5), value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2999457153 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2999457153 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2999457153 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef SPRITE_T280657092_H
#define SPRITE_T280657092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t280657092  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T280657092_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef ABSOLUTEURIRECORD_T2525168784_H
#define ABSOLUTEURIRECORD_T2525168784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.AbsoluteUriRecord
struct  AbsoluteUriRecord_t2525168784  : public NDEFRecord_t2690545556
{
public:
	// System.String DigitsNFCToolkit.AbsoluteUriRecord::uri
	String_t* ___uri_1;

public:
	inline static int32_t get_offset_of_uri_1() { return static_cast<int32_t>(offsetof(AbsoluteUriRecord_t2525168784, ___uri_1)); }
	inline String_t* get_uri_1() const { return ___uri_1; }
	inline String_t** get_address_of_uri_1() { return &___uri_1; }
	inline void set_uri_1(String_t* value)
	{
		___uri_1 = value;
		Il2CppCodeGenWriteBarrier((&___uri_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSOLUTEURIRECORD_T2525168784_H
#ifndef EMPTYRECORD_T1486430273_H
#define EMPTYRECORD_T1486430273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.EmptyRecord
struct  EmptyRecord_t1486430273  : public NDEFRecord_t2690545556
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYRECORD_T1486430273_H
#ifndef EXTERNALTYPERECORD_T4087466745_H
#define EXTERNALTYPERECORD_T4087466745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.ExternalTypeRecord
struct  ExternalTypeRecord_t4087466745  : public NDEFRecord_t2690545556
{
public:
	// System.String DigitsNFCToolkit.ExternalTypeRecord::domainName
	String_t* ___domainName_1;
	// System.String DigitsNFCToolkit.ExternalTypeRecord::domainType
	String_t* ___domainType_2;
	// System.Byte[] DigitsNFCToolkit.ExternalTypeRecord::domainData
	ByteU5BU5D_t4116647657* ___domainData_3;

public:
	inline static int32_t get_offset_of_domainName_1() { return static_cast<int32_t>(offsetof(ExternalTypeRecord_t4087466745, ___domainName_1)); }
	inline String_t* get_domainName_1() const { return ___domainName_1; }
	inline String_t** get_address_of_domainName_1() { return &___domainName_1; }
	inline void set_domainName_1(String_t* value)
	{
		___domainName_1 = value;
		Il2CppCodeGenWriteBarrier((&___domainName_1), value);
	}

	inline static int32_t get_offset_of_domainType_2() { return static_cast<int32_t>(offsetof(ExternalTypeRecord_t4087466745, ___domainType_2)); }
	inline String_t* get_domainType_2() const { return ___domainType_2; }
	inline String_t** get_address_of_domainType_2() { return &___domainType_2; }
	inline void set_domainType_2(String_t* value)
	{
		___domainType_2 = value;
		Il2CppCodeGenWriteBarrier((&___domainType_2), value);
	}

	inline static int32_t get_offset_of_domainData_3() { return static_cast<int32_t>(offsetof(ExternalTypeRecord_t4087466745, ___domainData_3)); }
	inline ByteU5BU5D_t4116647657* get_domainData_3() const { return ___domainData_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_domainData_3() { return &___domainData_3; }
	inline void set_domainData_3(ByteU5BU5D_t4116647657* value)
	{
		___domainData_3 = value;
		Il2CppCodeGenWriteBarrier((&___domainData_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNALTYPERECORD_T4087466745_H
#ifndef MIMEMEDIARECORD_T736820488_H
#define MIMEMEDIARECORD_T736820488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.MimeMediaRecord
struct  MimeMediaRecord_t736820488  : public NDEFRecord_t2690545556
{
public:
	// System.String DigitsNFCToolkit.MimeMediaRecord::mimeType
	String_t* ___mimeType_1;
	// System.Byte[] DigitsNFCToolkit.MimeMediaRecord::mimeData
	ByteU5BU5D_t4116647657* ___mimeData_2;

public:
	inline static int32_t get_offset_of_mimeType_1() { return static_cast<int32_t>(offsetof(MimeMediaRecord_t736820488, ___mimeType_1)); }
	inline String_t* get_mimeType_1() const { return ___mimeType_1; }
	inline String_t** get_address_of_mimeType_1() { return &___mimeType_1; }
	inline void set_mimeType_1(String_t* value)
	{
		___mimeType_1 = value;
		Il2CppCodeGenWriteBarrier((&___mimeType_1), value);
	}

	inline static int32_t get_offset_of_mimeData_2() { return static_cast<int32_t>(offsetof(MimeMediaRecord_t736820488, ___mimeData_2)); }
	inline ByteU5BU5D_t4116647657* get_mimeData_2() const { return ___mimeData_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_mimeData_2() { return &___mimeData_2; }
	inline void set_mimeData_2(ByteU5BU5D_t4116647657* value)
	{
		___mimeData_2 = value;
		Il2CppCodeGenWriteBarrier((&___mimeData_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIMEMEDIARECORD_T736820488_H
#ifndef ONNDEFPUSHFINISHED_T4279917764_H
#define ONNDEFPUSHFINISHED_T4279917764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.OnNDEFPushFinished
struct  OnNDEFPushFinished_t4279917764  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONNDEFPUSHFINISHED_T4279917764_H
#ifndef ONNDEFREADFINISHED_T1327886840_H
#define ONNDEFREADFINISHED_T1327886840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.OnNDEFReadFinished
struct  OnNDEFReadFinished_t1327886840  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONNDEFREADFINISHED_T1327886840_H
#ifndef ONNDEFWRITEFINISHED_T4102039599_H
#define ONNDEFWRITEFINISHED_T4102039599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.OnNDEFWriteFinished
struct  OnNDEFWriteFinished_t4102039599  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONNDEFWRITEFINISHED_T4102039599_H
#ifndef ONNFCTAGDETECTED_T3189675727_H
#define ONNFCTAGDETECTED_T3189675727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.OnNFCTagDetected
struct  OnNFCTagDetected_t3189675727  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONNFCTAGDETECTED_T3189675727_H
#ifndef SMARTPOSTERRECORD_T1640848801_H
#define SMARTPOSTERRECORD_T1640848801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.SmartPosterRecord
struct  SmartPosterRecord_t1640848801  : public NDEFRecord_t2690545556
{
public:
	// DigitsNFCToolkit.UriRecord DigitsNFCToolkit.SmartPosterRecord::uriRecord
	UriRecord_t2230063309 * ___uriRecord_1;
	// System.Collections.Generic.List`1<DigitsNFCToolkit.TextRecord> DigitsNFCToolkit.SmartPosterRecord::titleRecords
	List_1_t3785772365 * ___titleRecords_2;
	// System.Collections.Generic.List`1<DigitsNFCToolkit.MimeMediaRecord> DigitsNFCToolkit.SmartPosterRecord::iconRecords
	List_1_t2208895230 * ___iconRecords_3;
	// System.Collections.Generic.List`1<DigitsNFCToolkit.NDEFRecord> DigitsNFCToolkit.SmartPosterRecord::extraRecords
	List_1_t4162620298 * ___extraRecords_4;
	// DigitsNFCToolkit.SmartPosterRecord/RecommendedAction DigitsNFCToolkit.SmartPosterRecord::action
	int32_t ___action_5;
	// System.Int32 DigitsNFCToolkit.SmartPosterRecord::size
	int32_t ___size_6;
	// System.String DigitsNFCToolkit.SmartPosterRecord::mimeType
	String_t* ___mimeType_7;

public:
	inline static int32_t get_offset_of_uriRecord_1() { return static_cast<int32_t>(offsetof(SmartPosterRecord_t1640848801, ___uriRecord_1)); }
	inline UriRecord_t2230063309 * get_uriRecord_1() const { return ___uriRecord_1; }
	inline UriRecord_t2230063309 ** get_address_of_uriRecord_1() { return &___uriRecord_1; }
	inline void set_uriRecord_1(UriRecord_t2230063309 * value)
	{
		___uriRecord_1 = value;
		Il2CppCodeGenWriteBarrier((&___uriRecord_1), value);
	}

	inline static int32_t get_offset_of_titleRecords_2() { return static_cast<int32_t>(offsetof(SmartPosterRecord_t1640848801, ___titleRecords_2)); }
	inline List_1_t3785772365 * get_titleRecords_2() const { return ___titleRecords_2; }
	inline List_1_t3785772365 ** get_address_of_titleRecords_2() { return &___titleRecords_2; }
	inline void set_titleRecords_2(List_1_t3785772365 * value)
	{
		___titleRecords_2 = value;
		Il2CppCodeGenWriteBarrier((&___titleRecords_2), value);
	}

	inline static int32_t get_offset_of_iconRecords_3() { return static_cast<int32_t>(offsetof(SmartPosterRecord_t1640848801, ___iconRecords_3)); }
	inline List_1_t2208895230 * get_iconRecords_3() const { return ___iconRecords_3; }
	inline List_1_t2208895230 ** get_address_of_iconRecords_3() { return &___iconRecords_3; }
	inline void set_iconRecords_3(List_1_t2208895230 * value)
	{
		___iconRecords_3 = value;
		Il2CppCodeGenWriteBarrier((&___iconRecords_3), value);
	}

	inline static int32_t get_offset_of_extraRecords_4() { return static_cast<int32_t>(offsetof(SmartPosterRecord_t1640848801, ___extraRecords_4)); }
	inline List_1_t4162620298 * get_extraRecords_4() const { return ___extraRecords_4; }
	inline List_1_t4162620298 ** get_address_of_extraRecords_4() { return &___extraRecords_4; }
	inline void set_extraRecords_4(List_1_t4162620298 * value)
	{
		___extraRecords_4 = value;
		Il2CppCodeGenWriteBarrier((&___extraRecords_4), value);
	}

	inline static int32_t get_offset_of_action_5() { return static_cast<int32_t>(offsetof(SmartPosterRecord_t1640848801, ___action_5)); }
	inline int32_t get_action_5() const { return ___action_5; }
	inline int32_t* get_address_of_action_5() { return &___action_5; }
	inline void set_action_5(int32_t value)
	{
		___action_5 = value;
	}

	inline static int32_t get_offset_of_size_6() { return static_cast<int32_t>(offsetof(SmartPosterRecord_t1640848801, ___size_6)); }
	inline int32_t get_size_6() const { return ___size_6; }
	inline int32_t* get_address_of_size_6() { return &___size_6; }
	inline void set_size_6(int32_t value)
	{
		___size_6 = value;
	}

	inline static int32_t get_offset_of_mimeType_7() { return static_cast<int32_t>(offsetof(SmartPosterRecord_t1640848801, ___mimeType_7)); }
	inline String_t* get_mimeType_7() const { return ___mimeType_7; }
	inline String_t** get_address_of_mimeType_7() { return &___mimeType_7; }
	inline void set_mimeType_7(String_t* value)
	{
		___mimeType_7 = value;
		Il2CppCodeGenWriteBarrier((&___mimeType_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTPOSTERRECORD_T1640848801_H
#ifndef TEXTRECORD_T2313697623_H
#define TEXTRECORD_T2313697623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.TextRecord
struct  TextRecord_t2313697623  : public NDEFRecord_t2690545556
{
public:
	// System.String DigitsNFCToolkit.TextRecord::text
	String_t* ___text_1;
	// System.String DigitsNFCToolkit.TextRecord::languageCode
	String_t* ___languageCode_2;
	// DigitsNFCToolkit.TextRecord/TextEncoding DigitsNFCToolkit.TextRecord::textEncoding
	int32_t ___textEncoding_3;

public:
	inline static int32_t get_offset_of_text_1() { return static_cast<int32_t>(offsetof(TextRecord_t2313697623, ___text_1)); }
	inline String_t* get_text_1() const { return ___text_1; }
	inline String_t** get_address_of_text_1() { return &___text_1; }
	inline void set_text_1(String_t* value)
	{
		___text_1 = value;
		Il2CppCodeGenWriteBarrier((&___text_1), value);
	}

	inline static int32_t get_offset_of_languageCode_2() { return static_cast<int32_t>(offsetof(TextRecord_t2313697623, ___languageCode_2)); }
	inline String_t* get_languageCode_2() const { return ___languageCode_2; }
	inline String_t** get_address_of_languageCode_2() { return &___languageCode_2; }
	inline void set_languageCode_2(String_t* value)
	{
		___languageCode_2 = value;
		Il2CppCodeGenWriteBarrier((&___languageCode_2), value);
	}

	inline static int32_t get_offset_of_textEncoding_3() { return static_cast<int32_t>(offsetof(TextRecord_t2313697623, ___textEncoding_3)); }
	inline int32_t get_textEncoding_3() const { return ___textEncoding_3; }
	inline int32_t* get_address_of_textEncoding_3() { return &___textEncoding_3; }
	inline void set_textEncoding_3(int32_t value)
	{
		___textEncoding_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRECORD_T2313697623_H
#ifndef UNKNOWNRECORD_T3228240714_H
#define UNKNOWNRECORD_T3228240714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.UnknownRecord
struct  UnknownRecord_t3228240714  : public NDEFRecord_t2690545556
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNKNOWNRECORD_T3228240714_H
#ifndef URIRECORD_T2230063309_H
#define URIRECORD_T2230063309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.UriRecord
struct  UriRecord_t2230063309  : public NDEFRecord_t2690545556
{
public:
	// System.String DigitsNFCToolkit.UriRecord::fullUri
	String_t* ___fullUri_1;
	// System.String DigitsNFCToolkit.UriRecord::uri
	String_t* ___uri_2;
	// System.String DigitsNFCToolkit.UriRecord::protocol
	String_t* ___protocol_3;

public:
	inline static int32_t get_offset_of_fullUri_1() { return static_cast<int32_t>(offsetof(UriRecord_t2230063309, ___fullUri_1)); }
	inline String_t* get_fullUri_1() const { return ___fullUri_1; }
	inline String_t** get_address_of_fullUri_1() { return &___fullUri_1; }
	inline void set_fullUri_1(String_t* value)
	{
		___fullUri_1 = value;
		Il2CppCodeGenWriteBarrier((&___fullUri_1), value);
	}

	inline static int32_t get_offset_of_uri_2() { return static_cast<int32_t>(offsetof(UriRecord_t2230063309, ___uri_2)); }
	inline String_t* get_uri_2() const { return ___uri_2; }
	inline String_t** get_address_of_uri_2() { return &___uri_2; }
	inline void set_uri_2(String_t* value)
	{
		___uri_2 = value;
		Il2CppCodeGenWriteBarrier((&___uri_2), value);
	}

	inline static int32_t get_offset_of_protocol_3() { return static_cast<int32_t>(offsetof(UriRecord_t2230063309, ___protocol_3)); }
	inline String_t* get_protocol_3() const { return ___protocol_3; }
	inline String_t** get_address_of_protocol_3() { return &___protocol_3; }
	inline void set_protocol_3(String_t* value)
	{
		___protocol_3 = value;
		Il2CppCodeGenWriteBarrier((&___protocol_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIRECORD_T2230063309_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef TEXTURE2D_T3840446185_H
#define TEXTURE2D_T3840446185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3840446185  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3840446185_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef RECTTRANSFORM_T3704657025_H
#define RECTTRANSFORM_T3704657025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t3704657025  : public Transform_t3600365921
{
public:

public:
};

struct RectTransform_t3704657025_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1258266594 * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t1258266594 * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t1258266594 * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T3704657025_H
#ifndef NATIVENFC_T1941597496_H
#define NATIVENFC_T1941597496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NativeNFC
struct  NativeNFC_t1941597496  : public MonoBehaviour_t3962482529
{
public:
	// DigitsNFCToolkit.OnNFCTagDetected DigitsNFCToolkit.NativeNFC::onNFCTagDetected
	OnNFCTagDetected_t3189675727 * ___onNFCTagDetected_4;
	// DigitsNFCToolkit.OnNDEFReadFinished DigitsNFCToolkit.NativeNFC::onNDEFReadFinished
	OnNDEFReadFinished_t1327886840 * ___onNDEFReadFinished_5;
	// DigitsNFCToolkit.OnNDEFWriteFinished DigitsNFCToolkit.NativeNFC::onNDEFWriteFinished
	OnNDEFWriteFinished_t4102039599 * ___onNDEFWriteFinished_6;
	// DigitsNFCToolkit.OnNDEFPushFinished DigitsNFCToolkit.NativeNFC::onNDEFPushFinished
	OnNDEFPushFinished_t4279917764 * ___onNDEFPushFinished_7;
	// System.Boolean DigitsNFCToolkit.NativeNFC::resetOnTimeout
	bool ___resetOnTimeout_8;

public:
	inline static int32_t get_offset_of_onNFCTagDetected_4() { return static_cast<int32_t>(offsetof(NativeNFC_t1941597496, ___onNFCTagDetected_4)); }
	inline OnNFCTagDetected_t3189675727 * get_onNFCTagDetected_4() const { return ___onNFCTagDetected_4; }
	inline OnNFCTagDetected_t3189675727 ** get_address_of_onNFCTagDetected_4() { return &___onNFCTagDetected_4; }
	inline void set_onNFCTagDetected_4(OnNFCTagDetected_t3189675727 * value)
	{
		___onNFCTagDetected_4 = value;
		Il2CppCodeGenWriteBarrier((&___onNFCTagDetected_4), value);
	}

	inline static int32_t get_offset_of_onNDEFReadFinished_5() { return static_cast<int32_t>(offsetof(NativeNFC_t1941597496, ___onNDEFReadFinished_5)); }
	inline OnNDEFReadFinished_t1327886840 * get_onNDEFReadFinished_5() const { return ___onNDEFReadFinished_5; }
	inline OnNDEFReadFinished_t1327886840 ** get_address_of_onNDEFReadFinished_5() { return &___onNDEFReadFinished_5; }
	inline void set_onNDEFReadFinished_5(OnNDEFReadFinished_t1327886840 * value)
	{
		___onNDEFReadFinished_5 = value;
		Il2CppCodeGenWriteBarrier((&___onNDEFReadFinished_5), value);
	}

	inline static int32_t get_offset_of_onNDEFWriteFinished_6() { return static_cast<int32_t>(offsetof(NativeNFC_t1941597496, ___onNDEFWriteFinished_6)); }
	inline OnNDEFWriteFinished_t4102039599 * get_onNDEFWriteFinished_6() const { return ___onNDEFWriteFinished_6; }
	inline OnNDEFWriteFinished_t4102039599 ** get_address_of_onNDEFWriteFinished_6() { return &___onNDEFWriteFinished_6; }
	inline void set_onNDEFWriteFinished_6(OnNDEFWriteFinished_t4102039599 * value)
	{
		___onNDEFWriteFinished_6 = value;
		Il2CppCodeGenWriteBarrier((&___onNDEFWriteFinished_6), value);
	}

	inline static int32_t get_offset_of_onNDEFPushFinished_7() { return static_cast<int32_t>(offsetof(NativeNFC_t1941597496, ___onNDEFPushFinished_7)); }
	inline OnNDEFPushFinished_t4279917764 * get_onNDEFPushFinished_7() const { return ___onNDEFPushFinished_7; }
	inline OnNDEFPushFinished_t4279917764 ** get_address_of_onNDEFPushFinished_7() { return &___onNDEFPushFinished_7; }
	inline void set_onNDEFPushFinished_7(OnNDEFPushFinished_t4279917764 * value)
	{
		___onNDEFPushFinished_7 = value;
		Il2CppCodeGenWriteBarrier((&___onNDEFPushFinished_7), value);
	}

	inline static int32_t get_offset_of_resetOnTimeout_8() { return static_cast<int32_t>(offsetof(NativeNFC_t1941597496, ___resetOnTimeout_8)); }
	inline bool get_resetOnTimeout_8() const { return ___resetOnTimeout_8; }
	inline bool* get_address_of_resetOnTimeout_8() { return &___resetOnTimeout_8; }
	inline void set_resetOnTimeout_8(bool value)
	{
		___resetOnTimeout_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVENFC_T1941597496_H
#ifndef NATIVENFCMANAGER_T351225459_H
#define NATIVENFCMANAGER_T351225459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NativeNFCManager
struct  NativeNFCManager_t351225459  : public MonoBehaviour_t3962482529
{
public:
	// DigitsNFCToolkit.NativeNFC DigitsNFCToolkit.NativeNFCManager::nfc
	NativeNFC_t1941597496 * ___nfc_6;

public:
	inline static int32_t get_offset_of_nfc_6() { return static_cast<int32_t>(offsetof(NativeNFCManager_t351225459, ___nfc_6)); }
	inline NativeNFC_t1941597496 * get_nfc_6() const { return ___nfc_6; }
	inline NativeNFC_t1941597496 ** get_address_of_nfc_6() { return &___nfc_6; }
	inline void set_nfc_6(NativeNFC_t1941597496 * value)
	{
		___nfc_6 = value;
		Il2CppCodeGenWriteBarrier((&___nfc_6), value);
	}
};

struct NativeNFCManager_t351225459_StaticFields
{
public:
	// DigitsNFCToolkit.NativeNFCManager DigitsNFCToolkit.NativeNFCManager::instance
	NativeNFCManager_t351225459 * ___instance_5;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(NativeNFCManager_t351225459_StaticFields, ___instance_5)); }
	inline NativeNFCManager_t351225459 * get_instance_5() const { return ___instance_5; }
	inline NativeNFCManager_t351225459 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(NativeNFCManager_t351225459 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVENFCMANAGER_T351225459_H
#ifndef MESSAGESCREENVIEW_T146641597_H
#define MESSAGESCREENVIEW_T146641597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.MessageScreenView
struct  MessageScreenView_t146641597  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform DigitsNFCToolkit.Samples.MessageScreenView::writeMessageBox
	RectTransform_t3704657025 * ___writeMessageBox_4;
	// UnityEngine.UI.Text DigitsNFCToolkit.Samples.MessageScreenView::writeLabel
	Text_t1901882714 * ___writeLabel_5;
	// UnityEngine.UI.Button DigitsNFCToolkit.Samples.MessageScreenView::writeCancelButton
	Button_t4055032469 * ___writeCancelButton_6;
	// UnityEngine.UI.Button DigitsNFCToolkit.Samples.MessageScreenView::writeOKButton
	Button_t4055032469 * ___writeOKButton_7;
	// UnityEngine.RectTransform DigitsNFCToolkit.Samples.MessageScreenView::pushMessageBox
	RectTransform_t3704657025 * ___pushMessageBox_8;
	// UnityEngine.UI.Text DigitsNFCToolkit.Samples.MessageScreenView::pushLabel
	Text_t1901882714 * ___pushLabel_9;
	// UnityEngine.UI.Button DigitsNFCToolkit.Samples.MessageScreenView::pushCancelButton
	Button_t4055032469 * ___pushCancelButton_10;
	// UnityEngine.UI.Button DigitsNFCToolkit.Samples.MessageScreenView::pushOKButton
	Button_t4055032469 * ___pushOKButton_11;
	// System.Boolean DigitsNFCToolkit.Samples.MessageScreenView::initialized
	bool ___initialized_12;

public:
	inline static int32_t get_offset_of_writeMessageBox_4() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___writeMessageBox_4)); }
	inline RectTransform_t3704657025 * get_writeMessageBox_4() const { return ___writeMessageBox_4; }
	inline RectTransform_t3704657025 ** get_address_of_writeMessageBox_4() { return &___writeMessageBox_4; }
	inline void set_writeMessageBox_4(RectTransform_t3704657025 * value)
	{
		___writeMessageBox_4 = value;
		Il2CppCodeGenWriteBarrier((&___writeMessageBox_4), value);
	}

	inline static int32_t get_offset_of_writeLabel_5() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___writeLabel_5)); }
	inline Text_t1901882714 * get_writeLabel_5() const { return ___writeLabel_5; }
	inline Text_t1901882714 ** get_address_of_writeLabel_5() { return &___writeLabel_5; }
	inline void set_writeLabel_5(Text_t1901882714 * value)
	{
		___writeLabel_5 = value;
		Il2CppCodeGenWriteBarrier((&___writeLabel_5), value);
	}

	inline static int32_t get_offset_of_writeCancelButton_6() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___writeCancelButton_6)); }
	inline Button_t4055032469 * get_writeCancelButton_6() const { return ___writeCancelButton_6; }
	inline Button_t4055032469 ** get_address_of_writeCancelButton_6() { return &___writeCancelButton_6; }
	inline void set_writeCancelButton_6(Button_t4055032469 * value)
	{
		___writeCancelButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___writeCancelButton_6), value);
	}

	inline static int32_t get_offset_of_writeOKButton_7() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___writeOKButton_7)); }
	inline Button_t4055032469 * get_writeOKButton_7() const { return ___writeOKButton_7; }
	inline Button_t4055032469 ** get_address_of_writeOKButton_7() { return &___writeOKButton_7; }
	inline void set_writeOKButton_7(Button_t4055032469 * value)
	{
		___writeOKButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___writeOKButton_7), value);
	}

	inline static int32_t get_offset_of_pushMessageBox_8() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___pushMessageBox_8)); }
	inline RectTransform_t3704657025 * get_pushMessageBox_8() const { return ___pushMessageBox_8; }
	inline RectTransform_t3704657025 ** get_address_of_pushMessageBox_8() { return &___pushMessageBox_8; }
	inline void set_pushMessageBox_8(RectTransform_t3704657025 * value)
	{
		___pushMessageBox_8 = value;
		Il2CppCodeGenWriteBarrier((&___pushMessageBox_8), value);
	}

	inline static int32_t get_offset_of_pushLabel_9() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___pushLabel_9)); }
	inline Text_t1901882714 * get_pushLabel_9() const { return ___pushLabel_9; }
	inline Text_t1901882714 ** get_address_of_pushLabel_9() { return &___pushLabel_9; }
	inline void set_pushLabel_9(Text_t1901882714 * value)
	{
		___pushLabel_9 = value;
		Il2CppCodeGenWriteBarrier((&___pushLabel_9), value);
	}

	inline static int32_t get_offset_of_pushCancelButton_10() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___pushCancelButton_10)); }
	inline Button_t4055032469 * get_pushCancelButton_10() const { return ___pushCancelButton_10; }
	inline Button_t4055032469 ** get_address_of_pushCancelButton_10() { return &___pushCancelButton_10; }
	inline void set_pushCancelButton_10(Button_t4055032469 * value)
	{
		___pushCancelButton_10 = value;
		Il2CppCodeGenWriteBarrier((&___pushCancelButton_10), value);
	}

	inline static int32_t get_offset_of_pushOKButton_11() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___pushOKButton_11)); }
	inline Button_t4055032469 * get_pushOKButton_11() const { return ___pushOKButton_11; }
	inline Button_t4055032469 ** get_address_of_pushOKButton_11() { return &___pushOKButton_11; }
	inline void set_pushOKButton_11(Button_t4055032469 * value)
	{
		___pushOKButton_11 = value;
		Il2CppCodeGenWriteBarrier((&___pushOKButton_11), value);
	}

	inline static int32_t get_offset_of_initialized_12() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___initialized_12)); }
	inline bool get_initialized_12() const { return ___initialized_12; }
	inline bool* get_address_of_initialized_12() { return &___initialized_12; }
	inline void set_initialized_12(bool value)
	{
		___initialized_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGESCREENVIEW_T146641597_H
#ifndef NAVIGATIONMANAGER_T1939391727_H
#define NAVIGATIONMANAGER_T1939391727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.NavigationManager
struct  NavigationManager_t1939391727  : public MonoBehaviour_t3962482529
{
public:
	// DigitsNFCToolkit.Samples.ReadScreenControl DigitsNFCToolkit.Samples.NavigationManager::readScreenControl
	ReadScreenControl_t3483866810 * ___readScreenControl_4;
	// DigitsNFCToolkit.Samples.WriteScreenControl DigitsNFCToolkit.Samples.NavigationManager::writeScreenControl
	WriteScreenControl_t1506090515 * ___writeScreenControl_5;

public:
	inline static int32_t get_offset_of_readScreenControl_4() { return static_cast<int32_t>(offsetof(NavigationManager_t1939391727, ___readScreenControl_4)); }
	inline ReadScreenControl_t3483866810 * get_readScreenControl_4() const { return ___readScreenControl_4; }
	inline ReadScreenControl_t3483866810 ** get_address_of_readScreenControl_4() { return &___readScreenControl_4; }
	inline void set_readScreenControl_4(ReadScreenControl_t3483866810 * value)
	{
		___readScreenControl_4 = value;
		Il2CppCodeGenWriteBarrier((&___readScreenControl_4), value);
	}

	inline static int32_t get_offset_of_writeScreenControl_5() { return static_cast<int32_t>(offsetof(NavigationManager_t1939391727, ___writeScreenControl_5)); }
	inline WriteScreenControl_t1506090515 * get_writeScreenControl_5() const { return ___writeScreenControl_5; }
	inline WriteScreenControl_t1506090515 ** get_address_of_writeScreenControl_5() { return &___writeScreenControl_5; }
	inline void set_writeScreenControl_5(WriteScreenControl_t1506090515 * value)
	{
		___writeScreenControl_5 = value;
		Il2CppCodeGenWriteBarrier((&___writeScreenControl_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVIGATIONMANAGER_T1939391727_H
#ifndef READSCREENCONTROL_T3483866810_H
#define READSCREENCONTROL_T3483866810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.ReadScreenControl
struct  ReadScreenControl_t3483866810  : public MonoBehaviour_t3962482529
{
public:
	// DigitsNFCToolkit.Samples.ReadScreenView DigitsNFCToolkit.Samples.ReadScreenControl::view
	ReadScreenView_t239900869 * ___view_4;

public:
	inline static int32_t get_offset_of_view_4() { return static_cast<int32_t>(offsetof(ReadScreenControl_t3483866810, ___view_4)); }
	inline ReadScreenView_t239900869 * get_view_4() const { return ___view_4; }
	inline ReadScreenView_t239900869 ** get_address_of_view_4() { return &___view_4; }
	inline void set_view_4(ReadScreenView_t239900869 * value)
	{
		___view_4 = value;
		Il2CppCodeGenWriteBarrier((&___view_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSCREENCONTROL_T3483866810_H
#ifndef READSCREENVIEW_T239900869_H
#define READSCREENVIEW_T239900869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.ReadScreenView
struct  ReadScreenView_t239900869  : public MonoBehaviour_t3962482529
{
public:
	// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.ReadScreenView::textRecordItemPrefab
	RecordItem_t1075151419 * ___textRecordItemPrefab_11;
	// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.ReadScreenView::uriRecordItemPrefab
	RecordItem_t1075151419 * ___uriRecordItemPrefab_12;
	// DigitsNFCToolkit.Samples.ImageRecordItem DigitsNFCToolkit.Samples.ReadScreenView::mimeMediaRecordItemPrefab
	ImageRecordItem_t2104316047 * ___mimeMediaRecordItemPrefab_13;
	// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.ReadScreenView::externalTypeRecordItemPrefab
	RecordItem_t1075151419 * ___externalTypeRecordItemPrefab_14;
	// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.ReadScreenView::smartPosterRecordItemPrefab
	RecordItem_t1075151419 * ___smartPosterRecordItemPrefab_15;
	// UnityEngine.RectTransform DigitsNFCToolkit.Samples.ReadScreenView::tagInfoTransform
	RectTransform_t3704657025 * ___tagInfoTransform_16;
	// UnityEngine.RectTransform DigitsNFCToolkit.Samples.ReadScreenView::iOSReadTransform
	RectTransform_t3704657025 * ___iOSReadTransform_17;
	// UnityEngine.UI.Text DigitsNFCToolkit.Samples.ReadScreenView::tagInfoContentLabel
	Text_t1901882714 * ___tagInfoContentLabel_18;
	// UnityEngine.UI.ScrollRect DigitsNFCToolkit.Samples.ReadScreenView::ndefMessageScrollRect
	ScrollRect_t4137855814 * ___ndefMessageScrollRect_19;

public:
	inline static int32_t get_offset_of_textRecordItemPrefab_11() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___textRecordItemPrefab_11)); }
	inline RecordItem_t1075151419 * get_textRecordItemPrefab_11() const { return ___textRecordItemPrefab_11; }
	inline RecordItem_t1075151419 ** get_address_of_textRecordItemPrefab_11() { return &___textRecordItemPrefab_11; }
	inline void set_textRecordItemPrefab_11(RecordItem_t1075151419 * value)
	{
		___textRecordItemPrefab_11 = value;
		Il2CppCodeGenWriteBarrier((&___textRecordItemPrefab_11), value);
	}

	inline static int32_t get_offset_of_uriRecordItemPrefab_12() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___uriRecordItemPrefab_12)); }
	inline RecordItem_t1075151419 * get_uriRecordItemPrefab_12() const { return ___uriRecordItemPrefab_12; }
	inline RecordItem_t1075151419 ** get_address_of_uriRecordItemPrefab_12() { return &___uriRecordItemPrefab_12; }
	inline void set_uriRecordItemPrefab_12(RecordItem_t1075151419 * value)
	{
		___uriRecordItemPrefab_12 = value;
		Il2CppCodeGenWriteBarrier((&___uriRecordItemPrefab_12), value);
	}

	inline static int32_t get_offset_of_mimeMediaRecordItemPrefab_13() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___mimeMediaRecordItemPrefab_13)); }
	inline ImageRecordItem_t2104316047 * get_mimeMediaRecordItemPrefab_13() const { return ___mimeMediaRecordItemPrefab_13; }
	inline ImageRecordItem_t2104316047 ** get_address_of_mimeMediaRecordItemPrefab_13() { return &___mimeMediaRecordItemPrefab_13; }
	inline void set_mimeMediaRecordItemPrefab_13(ImageRecordItem_t2104316047 * value)
	{
		___mimeMediaRecordItemPrefab_13 = value;
		Il2CppCodeGenWriteBarrier((&___mimeMediaRecordItemPrefab_13), value);
	}

	inline static int32_t get_offset_of_externalTypeRecordItemPrefab_14() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___externalTypeRecordItemPrefab_14)); }
	inline RecordItem_t1075151419 * get_externalTypeRecordItemPrefab_14() const { return ___externalTypeRecordItemPrefab_14; }
	inline RecordItem_t1075151419 ** get_address_of_externalTypeRecordItemPrefab_14() { return &___externalTypeRecordItemPrefab_14; }
	inline void set_externalTypeRecordItemPrefab_14(RecordItem_t1075151419 * value)
	{
		___externalTypeRecordItemPrefab_14 = value;
		Il2CppCodeGenWriteBarrier((&___externalTypeRecordItemPrefab_14), value);
	}

	inline static int32_t get_offset_of_smartPosterRecordItemPrefab_15() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___smartPosterRecordItemPrefab_15)); }
	inline RecordItem_t1075151419 * get_smartPosterRecordItemPrefab_15() const { return ___smartPosterRecordItemPrefab_15; }
	inline RecordItem_t1075151419 ** get_address_of_smartPosterRecordItemPrefab_15() { return &___smartPosterRecordItemPrefab_15; }
	inline void set_smartPosterRecordItemPrefab_15(RecordItem_t1075151419 * value)
	{
		___smartPosterRecordItemPrefab_15 = value;
		Il2CppCodeGenWriteBarrier((&___smartPosterRecordItemPrefab_15), value);
	}

	inline static int32_t get_offset_of_tagInfoTransform_16() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___tagInfoTransform_16)); }
	inline RectTransform_t3704657025 * get_tagInfoTransform_16() const { return ___tagInfoTransform_16; }
	inline RectTransform_t3704657025 ** get_address_of_tagInfoTransform_16() { return &___tagInfoTransform_16; }
	inline void set_tagInfoTransform_16(RectTransform_t3704657025 * value)
	{
		___tagInfoTransform_16 = value;
		Il2CppCodeGenWriteBarrier((&___tagInfoTransform_16), value);
	}

	inline static int32_t get_offset_of_iOSReadTransform_17() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___iOSReadTransform_17)); }
	inline RectTransform_t3704657025 * get_iOSReadTransform_17() const { return ___iOSReadTransform_17; }
	inline RectTransform_t3704657025 ** get_address_of_iOSReadTransform_17() { return &___iOSReadTransform_17; }
	inline void set_iOSReadTransform_17(RectTransform_t3704657025 * value)
	{
		___iOSReadTransform_17 = value;
		Il2CppCodeGenWriteBarrier((&___iOSReadTransform_17), value);
	}

	inline static int32_t get_offset_of_tagInfoContentLabel_18() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___tagInfoContentLabel_18)); }
	inline Text_t1901882714 * get_tagInfoContentLabel_18() const { return ___tagInfoContentLabel_18; }
	inline Text_t1901882714 ** get_address_of_tagInfoContentLabel_18() { return &___tagInfoContentLabel_18; }
	inline void set_tagInfoContentLabel_18(Text_t1901882714 * value)
	{
		___tagInfoContentLabel_18 = value;
		Il2CppCodeGenWriteBarrier((&___tagInfoContentLabel_18), value);
	}

	inline static int32_t get_offset_of_ndefMessageScrollRect_19() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___ndefMessageScrollRect_19)); }
	inline ScrollRect_t4137855814 * get_ndefMessageScrollRect_19() const { return ___ndefMessageScrollRect_19; }
	inline ScrollRect_t4137855814 ** get_address_of_ndefMessageScrollRect_19() { return &___ndefMessageScrollRect_19; }
	inline void set_ndefMessageScrollRect_19(ScrollRect_t4137855814 * value)
	{
		___ndefMessageScrollRect_19 = value;
		Il2CppCodeGenWriteBarrier((&___ndefMessageScrollRect_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSCREENVIEW_T239900869_H
#ifndef RECORDITEM_T1075151419_H
#define RECORDITEM_T1075151419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.RecordItem
struct  RecordItem_t1075151419  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform DigitsNFCToolkit.Samples.RecordItem::<RectTransform>k__BackingField
	RectTransform_t3704657025 * ___U3CRectTransformU3Ek__BackingField_4;
	// UnityEngine.UI.Text DigitsNFCToolkit.Samples.RecordItem::label
	Text_t1901882714 * ___label_5;

public:
	inline static int32_t get_offset_of_U3CRectTransformU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RecordItem_t1075151419, ___U3CRectTransformU3Ek__BackingField_4)); }
	inline RectTransform_t3704657025 * get_U3CRectTransformU3Ek__BackingField_4() const { return ___U3CRectTransformU3Ek__BackingField_4; }
	inline RectTransform_t3704657025 ** get_address_of_U3CRectTransformU3Ek__BackingField_4() { return &___U3CRectTransformU3Ek__BackingField_4; }
	inline void set_U3CRectTransformU3Ek__BackingField_4(RectTransform_t3704657025 * value)
	{
		___U3CRectTransformU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRectTransformU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_label_5() { return static_cast<int32_t>(offsetof(RecordItem_t1075151419, ___label_5)); }
	inline Text_t1901882714 * get_label_5() const { return ___label_5; }
	inline Text_t1901882714 ** get_address_of_label_5() { return &___label_5; }
	inline void set_label_5(Text_t1901882714 * value)
	{
		___label_5 = value;
		Il2CppCodeGenWriteBarrier((&___label_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECORDITEM_T1075151419_H
#ifndef WRITESCREENCONTROL_T1506090515_H
#define WRITESCREENCONTROL_T1506090515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.WriteScreenControl
struct  WriteScreenControl_t1506090515  : public MonoBehaviour_t3962482529
{
public:
	// DigitsNFCToolkit.Samples.WriteScreenView DigitsNFCToolkit.Samples.WriteScreenControl::view
	WriteScreenView_t2350253495 * ___view_4;
	// DigitsNFCToolkit.Samples.MessageScreenView DigitsNFCToolkit.Samples.WriteScreenControl::messageScreenView
	MessageScreenView_t146641597 * ___messageScreenView_5;
	// UnityEngine.Sprite DigitsNFCToolkit.Samples.WriteScreenControl::musicNoteIcon
	Sprite_t280657092 * ___musicNoteIcon_6;
	// UnityEngine.Sprite DigitsNFCToolkit.Samples.WriteScreenControl::faceIcon
	Sprite_t280657092 * ___faceIcon_7;
	// UnityEngine.Sprite DigitsNFCToolkit.Samples.WriteScreenControl::arrowIcon
	Sprite_t280657092 * ___arrowIcon_8;
	// DigitsNFCToolkit.NDEFMessage DigitsNFCToolkit.Samples.WriteScreenControl::pendingMessage
	NDEFMessage_t279637043 * ___pendingMessage_9;

public:
	inline static int32_t get_offset_of_view_4() { return static_cast<int32_t>(offsetof(WriteScreenControl_t1506090515, ___view_4)); }
	inline WriteScreenView_t2350253495 * get_view_4() const { return ___view_4; }
	inline WriteScreenView_t2350253495 ** get_address_of_view_4() { return &___view_4; }
	inline void set_view_4(WriteScreenView_t2350253495 * value)
	{
		___view_4 = value;
		Il2CppCodeGenWriteBarrier((&___view_4), value);
	}

	inline static int32_t get_offset_of_messageScreenView_5() { return static_cast<int32_t>(offsetof(WriteScreenControl_t1506090515, ___messageScreenView_5)); }
	inline MessageScreenView_t146641597 * get_messageScreenView_5() const { return ___messageScreenView_5; }
	inline MessageScreenView_t146641597 ** get_address_of_messageScreenView_5() { return &___messageScreenView_5; }
	inline void set_messageScreenView_5(MessageScreenView_t146641597 * value)
	{
		___messageScreenView_5 = value;
		Il2CppCodeGenWriteBarrier((&___messageScreenView_5), value);
	}

	inline static int32_t get_offset_of_musicNoteIcon_6() { return static_cast<int32_t>(offsetof(WriteScreenControl_t1506090515, ___musicNoteIcon_6)); }
	inline Sprite_t280657092 * get_musicNoteIcon_6() const { return ___musicNoteIcon_6; }
	inline Sprite_t280657092 ** get_address_of_musicNoteIcon_6() { return &___musicNoteIcon_6; }
	inline void set_musicNoteIcon_6(Sprite_t280657092 * value)
	{
		___musicNoteIcon_6 = value;
		Il2CppCodeGenWriteBarrier((&___musicNoteIcon_6), value);
	}

	inline static int32_t get_offset_of_faceIcon_7() { return static_cast<int32_t>(offsetof(WriteScreenControl_t1506090515, ___faceIcon_7)); }
	inline Sprite_t280657092 * get_faceIcon_7() const { return ___faceIcon_7; }
	inline Sprite_t280657092 ** get_address_of_faceIcon_7() { return &___faceIcon_7; }
	inline void set_faceIcon_7(Sprite_t280657092 * value)
	{
		___faceIcon_7 = value;
		Il2CppCodeGenWriteBarrier((&___faceIcon_7), value);
	}

	inline static int32_t get_offset_of_arrowIcon_8() { return static_cast<int32_t>(offsetof(WriteScreenControl_t1506090515, ___arrowIcon_8)); }
	inline Sprite_t280657092 * get_arrowIcon_8() const { return ___arrowIcon_8; }
	inline Sprite_t280657092 ** get_address_of_arrowIcon_8() { return &___arrowIcon_8; }
	inline void set_arrowIcon_8(Sprite_t280657092 * value)
	{
		___arrowIcon_8 = value;
		Il2CppCodeGenWriteBarrier((&___arrowIcon_8), value);
	}

	inline static int32_t get_offset_of_pendingMessage_9() { return static_cast<int32_t>(offsetof(WriteScreenControl_t1506090515, ___pendingMessage_9)); }
	inline NDEFMessage_t279637043 * get_pendingMessage_9() const { return ___pendingMessage_9; }
	inline NDEFMessage_t279637043 ** get_address_of_pendingMessage_9() { return &___pendingMessage_9; }
	inline void set_pendingMessage_9(NDEFMessage_t279637043 * value)
	{
		___pendingMessage_9 = value;
		Il2CppCodeGenWriteBarrier((&___pendingMessage_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESCREENCONTROL_T1506090515_H
#ifndef WRITESCREENVIEW_T2350253495_H
#define WRITESCREENVIEW_T2350253495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.WriteScreenView
struct  WriteScreenView_t2350253495  : public MonoBehaviour_t3962482529
{
public:
	// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.WriteScreenView::textRecordItemPrefab
	RecordItem_t1075151419 * ___textRecordItemPrefab_10;
	// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.WriteScreenView::uriRecordItemPrefab
	RecordItem_t1075151419 * ___uriRecordItemPrefab_11;
	// DigitsNFCToolkit.Samples.ImageRecordItem DigitsNFCToolkit.Samples.WriteScreenView::mimeMediaRecordItemPrefab
	ImageRecordItem_t2104316047 * ___mimeMediaRecordItemPrefab_12;
	// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.WriteScreenView::externalTypeRecordItemPrefab
	RecordItem_t1075151419 * ___externalTypeRecordItemPrefab_13;
	// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.WriteScreenView::smartPosterRecordItemPrefab
	RecordItem_t1075151419 * ___smartPosterRecordItemPrefab_14;
	// UnityEngine.UI.ScrollRect DigitsNFCToolkit.Samples.WriteScreenView::ndefMessageScrollRect
	ScrollRect_t4137855814 * ___ndefMessageScrollRect_15;
	// UnityEngine.UI.Text DigitsNFCToolkit.Samples.WriteScreenView::titleLabel
	Text_t1901882714 * ___titleLabel_16;
	// UnityEngine.UI.Dropdown DigitsNFCToolkit.Samples.WriteScreenView::typeDropdown
	Dropdown_t2274391225 * ___typeDropdown_17;
	// UnityEngine.Transform DigitsNFCToolkit.Samples.WriteScreenView::textRecordTransform
	Transform_t3600365921 * ___textRecordTransform_18;
	// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::textInput
	InputField_t3762917431 * ___textInput_19;
	// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::languageCodeInput
	InputField_t3762917431 * ___languageCodeInput_20;
	// UnityEngine.UI.Dropdown DigitsNFCToolkit.Samples.WriteScreenView::textEncodingDropdown
	Dropdown_t2274391225 * ___textEncodingDropdown_21;
	// UnityEngine.UI.Button DigitsNFCToolkit.Samples.WriteScreenView::addRecordButton
	Button_t4055032469 * ___addRecordButton_22;
	// UnityEngine.Transform DigitsNFCToolkit.Samples.WriteScreenView::uriRecordTransform
	Transform_t3600365921 * ___uriRecordTransform_23;
	// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::uriInput
	InputField_t3762917431 * ___uriInput_24;
	// UnityEngine.Transform DigitsNFCToolkit.Samples.WriteScreenView::mimeMediaRecordTransform
	Transform_t3600365921 * ___mimeMediaRecordTransform_25;
	// UnityEngine.UI.Dropdown DigitsNFCToolkit.Samples.WriteScreenView::iconDropdown
	Dropdown_t2274391225 * ___iconDropdown_26;
	// UnityEngine.UI.Text DigitsNFCToolkit.Samples.WriteScreenView::mimeTypeLabel
	Text_t1901882714 * ___mimeTypeLabel_27;
	// UnityEngine.Transform DigitsNFCToolkit.Samples.WriteScreenView::externalTypeRecordTransform
	Transform_t3600365921 * ___externalTypeRecordTransform_28;
	// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::domainNameInput
	InputField_t3762917431 * ___domainNameInput_29;
	// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::domainTypeInput
	InputField_t3762917431 * ___domainTypeInput_30;
	// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::domainDataInput
	InputField_t3762917431 * ___domainDataInput_31;
	// UnityEngine.UI.Button DigitsNFCToolkit.Samples.WriteScreenView::clearButton
	Button_t4055032469 * ___clearButton_32;
	// UnityEngine.UI.Button DigitsNFCToolkit.Samples.WriteScreenView::writeButton
	Button_t4055032469 * ___writeButton_33;

public:
	inline static int32_t get_offset_of_textRecordItemPrefab_10() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___textRecordItemPrefab_10)); }
	inline RecordItem_t1075151419 * get_textRecordItemPrefab_10() const { return ___textRecordItemPrefab_10; }
	inline RecordItem_t1075151419 ** get_address_of_textRecordItemPrefab_10() { return &___textRecordItemPrefab_10; }
	inline void set_textRecordItemPrefab_10(RecordItem_t1075151419 * value)
	{
		___textRecordItemPrefab_10 = value;
		Il2CppCodeGenWriteBarrier((&___textRecordItemPrefab_10), value);
	}

	inline static int32_t get_offset_of_uriRecordItemPrefab_11() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___uriRecordItemPrefab_11)); }
	inline RecordItem_t1075151419 * get_uriRecordItemPrefab_11() const { return ___uriRecordItemPrefab_11; }
	inline RecordItem_t1075151419 ** get_address_of_uriRecordItemPrefab_11() { return &___uriRecordItemPrefab_11; }
	inline void set_uriRecordItemPrefab_11(RecordItem_t1075151419 * value)
	{
		___uriRecordItemPrefab_11 = value;
		Il2CppCodeGenWriteBarrier((&___uriRecordItemPrefab_11), value);
	}

	inline static int32_t get_offset_of_mimeMediaRecordItemPrefab_12() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___mimeMediaRecordItemPrefab_12)); }
	inline ImageRecordItem_t2104316047 * get_mimeMediaRecordItemPrefab_12() const { return ___mimeMediaRecordItemPrefab_12; }
	inline ImageRecordItem_t2104316047 ** get_address_of_mimeMediaRecordItemPrefab_12() { return &___mimeMediaRecordItemPrefab_12; }
	inline void set_mimeMediaRecordItemPrefab_12(ImageRecordItem_t2104316047 * value)
	{
		___mimeMediaRecordItemPrefab_12 = value;
		Il2CppCodeGenWriteBarrier((&___mimeMediaRecordItemPrefab_12), value);
	}

	inline static int32_t get_offset_of_externalTypeRecordItemPrefab_13() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___externalTypeRecordItemPrefab_13)); }
	inline RecordItem_t1075151419 * get_externalTypeRecordItemPrefab_13() const { return ___externalTypeRecordItemPrefab_13; }
	inline RecordItem_t1075151419 ** get_address_of_externalTypeRecordItemPrefab_13() { return &___externalTypeRecordItemPrefab_13; }
	inline void set_externalTypeRecordItemPrefab_13(RecordItem_t1075151419 * value)
	{
		___externalTypeRecordItemPrefab_13 = value;
		Il2CppCodeGenWriteBarrier((&___externalTypeRecordItemPrefab_13), value);
	}

	inline static int32_t get_offset_of_smartPosterRecordItemPrefab_14() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___smartPosterRecordItemPrefab_14)); }
	inline RecordItem_t1075151419 * get_smartPosterRecordItemPrefab_14() const { return ___smartPosterRecordItemPrefab_14; }
	inline RecordItem_t1075151419 ** get_address_of_smartPosterRecordItemPrefab_14() { return &___smartPosterRecordItemPrefab_14; }
	inline void set_smartPosterRecordItemPrefab_14(RecordItem_t1075151419 * value)
	{
		___smartPosterRecordItemPrefab_14 = value;
		Il2CppCodeGenWriteBarrier((&___smartPosterRecordItemPrefab_14), value);
	}

	inline static int32_t get_offset_of_ndefMessageScrollRect_15() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___ndefMessageScrollRect_15)); }
	inline ScrollRect_t4137855814 * get_ndefMessageScrollRect_15() const { return ___ndefMessageScrollRect_15; }
	inline ScrollRect_t4137855814 ** get_address_of_ndefMessageScrollRect_15() { return &___ndefMessageScrollRect_15; }
	inline void set_ndefMessageScrollRect_15(ScrollRect_t4137855814 * value)
	{
		___ndefMessageScrollRect_15 = value;
		Il2CppCodeGenWriteBarrier((&___ndefMessageScrollRect_15), value);
	}

	inline static int32_t get_offset_of_titleLabel_16() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___titleLabel_16)); }
	inline Text_t1901882714 * get_titleLabel_16() const { return ___titleLabel_16; }
	inline Text_t1901882714 ** get_address_of_titleLabel_16() { return &___titleLabel_16; }
	inline void set_titleLabel_16(Text_t1901882714 * value)
	{
		___titleLabel_16 = value;
		Il2CppCodeGenWriteBarrier((&___titleLabel_16), value);
	}

	inline static int32_t get_offset_of_typeDropdown_17() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___typeDropdown_17)); }
	inline Dropdown_t2274391225 * get_typeDropdown_17() const { return ___typeDropdown_17; }
	inline Dropdown_t2274391225 ** get_address_of_typeDropdown_17() { return &___typeDropdown_17; }
	inline void set_typeDropdown_17(Dropdown_t2274391225 * value)
	{
		___typeDropdown_17 = value;
		Il2CppCodeGenWriteBarrier((&___typeDropdown_17), value);
	}

	inline static int32_t get_offset_of_textRecordTransform_18() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___textRecordTransform_18)); }
	inline Transform_t3600365921 * get_textRecordTransform_18() const { return ___textRecordTransform_18; }
	inline Transform_t3600365921 ** get_address_of_textRecordTransform_18() { return &___textRecordTransform_18; }
	inline void set_textRecordTransform_18(Transform_t3600365921 * value)
	{
		___textRecordTransform_18 = value;
		Il2CppCodeGenWriteBarrier((&___textRecordTransform_18), value);
	}

	inline static int32_t get_offset_of_textInput_19() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___textInput_19)); }
	inline InputField_t3762917431 * get_textInput_19() const { return ___textInput_19; }
	inline InputField_t3762917431 ** get_address_of_textInput_19() { return &___textInput_19; }
	inline void set_textInput_19(InputField_t3762917431 * value)
	{
		___textInput_19 = value;
		Il2CppCodeGenWriteBarrier((&___textInput_19), value);
	}

	inline static int32_t get_offset_of_languageCodeInput_20() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___languageCodeInput_20)); }
	inline InputField_t3762917431 * get_languageCodeInput_20() const { return ___languageCodeInput_20; }
	inline InputField_t3762917431 ** get_address_of_languageCodeInput_20() { return &___languageCodeInput_20; }
	inline void set_languageCodeInput_20(InputField_t3762917431 * value)
	{
		___languageCodeInput_20 = value;
		Il2CppCodeGenWriteBarrier((&___languageCodeInput_20), value);
	}

	inline static int32_t get_offset_of_textEncodingDropdown_21() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___textEncodingDropdown_21)); }
	inline Dropdown_t2274391225 * get_textEncodingDropdown_21() const { return ___textEncodingDropdown_21; }
	inline Dropdown_t2274391225 ** get_address_of_textEncodingDropdown_21() { return &___textEncodingDropdown_21; }
	inline void set_textEncodingDropdown_21(Dropdown_t2274391225 * value)
	{
		___textEncodingDropdown_21 = value;
		Il2CppCodeGenWriteBarrier((&___textEncodingDropdown_21), value);
	}

	inline static int32_t get_offset_of_addRecordButton_22() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___addRecordButton_22)); }
	inline Button_t4055032469 * get_addRecordButton_22() const { return ___addRecordButton_22; }
	inline Button_t4055032469 ** get_address_of_addRecordButton_22() { return &___addRecordButton_22; }
	inline void set_addRecordButton_22(Button_t4055032469 * value)
	{
		___addRecordButton_22 = value;
		Il2CppCodeGenWriteBarrier((&___addRecordButton_22), value);
	}

	inline static int32_t get_offset_of_uriRecordTransform_23() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___uriRecordTransform_23)); }
	inline Transform_t3600365921 * get_uriRecordTransform_23() const { return ___uriRecordTransform_23; }
	inline Transform_t3600365921 ** get_address_of_uriRecordTransform_23() { return &___uriRecordTransform_23; }
	inline void set_uriRecordTransform_23(Transform_t3600365921 * value)
	{
		___uriRecordTransform_23 = value;
		Il2CppCodeGenWriteBarrier((&___uriRecordTransform_23), value);
	}

	inline static int32_t get_offset_of_uriInput_24() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___uriInput_24)); }
	inline InputField_t3762917431 * get_uriInput_24() const { return ___uriInput_24; }
	inline InputField_t3762917431 ** get_address_of_uriInput_24() { return &___uriInput_24; }
	inline void set_uriInput_24(InputField_t3762917431 * value)
	{
		___uriInput_24 = value;
		Il2CppCodeGenWriteBarrier((&___uriInput_24), value);
	}

	inline static int32_t get_offset_of_mimeMediaRecordTransform_25() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___mimeMediaRecordTransform_25)); }
	inline Transform_t3600365921 * get_mimeMediaRecordTransform_25() const { return ___mimeMediaRecordTransform_25; }
	inline Transform_t3600365921 ** get_address_of_mimeMediaRecordTransform_25() { return &___mimeMediaRecordTransform_25; }
	inline void set_mimeMediaRecordTransform_25(Transform_t3600365921 * value)
	{
		___mimeMediaRecordTransform_25 = value;
		Il2CppCodeGenWriteBarrier((&___mimeMediaRecordTransform_25), value);
	}

	inline static int32_t get_offset_of_iconDropdown_26() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___iconDropdown_26)); }
	inline Dropdown_t2274391225 * get_iconDropdown_26() const { return ___iconDropdown_26; }
	inline Dropdown_t2274391225 ** get_address_of_iconDropdown_26() { return &___iconDropdown_26; }
	inline void set_iconDropdown_26(Dropdown_t2274391225 * value)
	{
		___iconDropdown_26 = value;
		Il2CppCodeGenWriteBarrier((&___iconDropdown_26), value);
	}

	inline static int32_t get_offset_of_mimeTypeLabel_27() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___mimeTypeLabel_27)); }
	inline Text_t1901882714 * get_mimeTypeLabel_27() const { return ___mimeTypeLabel_27; }
	inline Text_t1901882714 ** get_address_of_mimeTypeLabel_27() { return &___mimeTypeLabel_27; }
	inline void set_mimeTypeLabel_27(Text_t1901882714 * value)
	{
		___mimeTypeLabel_27 = value;
		Il2CppCodeGenWriteBarrier((&___mimeTypeLabel_27), value);
	}

	inline static int32_t get_offset_of_externalTypeRecordTransform_28() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___externalTypeRecordTransform_28)); }
	inline Transform_t3600365921 * get_externalTypeRecordTransform_28() const { return ___externalTypeRecordTransform_28; }
	inline Transform_t3600365921 ** get_address_of_externalTypeRecordTransform_28() { return &___externalTypeRecordTransform_28; }
	inline void set_externalTypeRecordTransform_28(Transform_t3600365921 * value)
	{
		___externalTypeRecordTransform_28 = value;
		Il2CppCodeGenWriteBarrier((&___externalTypeRecordTransform_28), value);
	}

	inline static int32_t get_offset_of_domainNameInput_29() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___domainNameInput_29)); }
	inline InputField_t3762917431 * get_domainNameInput_29() const { return ___domainNameInput_29; }
	inline InputField_t3762917431 ** get_address_of_domainNameInput_29() { return &___domainNameInput_29; }
	inline void set_domainNameInput_29(InputField_t3762917431 * value)
	{
		___domainNameInput_29 = value;
		Il2CppCodeGenWriteBarrier((&___domainNameInput_29), value);
	}

	inline static int32_t get_offset_of_domainTypeInput_30() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___domainTypeInput_30)); }
	inline InputField_t3762917431 * get_domainTypeInput_30() const { return ___domainTypeInput_30; }
	inline InputField_t3762917431 ** get_address_of_domainTypeInput_30() { return &___domainTypeInput_30; }
	inline void set_domainTypeInput_30(InputField_t3762917431 * value)
	{
		___domainTypeInput_30 = value;
		Il2CppCodeGenWriteBarrier((&___domainTypeInput_30), value);
	}

	inline static int32_t get_offset_of_domainDataInput_31() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___domainDataInput_31)); }
	inline InputField_t3762917431 * get_domainDataInput_31() const { return ___domainDataInput_31; }
	inline InputField_t3762917431 ** get_address_of_domainDataInput_31() { return &___domainDataInput_31; }
	inline void set_domainDataInput_31(InputField_t3762917431 * value)
	{
		___domainDataInput_31 = value;
		Il2CppCodeGenWriteBarrier((&___domainDataInput_31), value);
	}

	inline static int32_t get_offset_of_clearButton_32() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___clearButton_32)); }
	inline Button_t4055032469 * get_clearButton_32() const { return ___clearButton_32; }
	inline Button_t4055032469 ** get_address_of_clearButton_32() { return &___clearButton_32; }
	inline void set_clearButton_32(Button_t4055032469 * value)
	{
		___clearButton_32 = value;
		Il2CppCodeGenWriteBarrier((&___clearButton_32), value);
	}

	inline static int32_t get_offset_of_writeButton_33() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___writeButton_33)); }
	inline Button_t4055032469 * get_writeButton_33() const { return ___writeButton_33; }
	inline Button_t4055032469 ** get_address_of_writeButton_33() { return &___writeButton_33; }
	inline void set_writeButton_33(Button_t4055032469 * value)
	{
		___writeButton_33 = value;
		Il2CppCodeGenWriteBarrier((&___writeButton_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESCREENVIEW_T2350253495_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef IOSNFC_T293874181_H
#define IOSNFC_T293874181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.IOSNFC
struct  IOSNFC_t293874181  : public NativeNFC_t1941597496
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNFC_T293874181_H
#ifndef IMAGERECORDITEM_T2104316047_H
#define IMAGERECORDITEM_T2104316047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.ImageRecordItem
struct  ImageRecordItem_t2104316047  : public RecordItem_t1075151419
{
public:
	// UnityEngine.UI.Image DigitsNFCToolkit.Samples.ImageRecordItem::imageRenderer
	Image_t2670269651 * ___imageRenderer_6;

public:
	inline static int32_t get_offset_of_imageRenderer_6() { return static_cast<int32_t>(offsetof(ImageRecordItem_t2104316047, ___imageRenderer_6)); }
	inline Image_t2670269651 * get_imageRenderer_6() const { return ___imageRenderer_6; }
	inline Image_t2670269651 ** get_address_of_imageRenderer_6() { return &___imageRenderer_6; }
	inline void set_imageRenderer_6(Image_t2670269651 * value)
	{
		___imageRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((&___imageRenderer_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGERECORDITEM_T2104316047_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_6)); }
	inline Material_t340375123 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t340375123 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t340375123 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_7)); }
	inline Color_t2555686324  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t2555686324 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t2555686324  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_9)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_11)); }
	inline Canvas_t3310196443 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t3310196443 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t340375123 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t340375123 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t3648964284 * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t3648964284 * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef SCROLLRECT_T4137855814_H
#define SCROLLRECT_T4137855814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect
struct  ScrollRect_t4137855814  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Content
	RectTransform_t3704657025 * ___m_Content_4;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Horizontal
	bool ___m_Horizontal_5;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Vertical
	bool ___m_Vertical_6;
	// UnityEngine.UI.ScrollRect/MovementType UnityEngine.UI.ScrollRect::m_MovementType
	int32_t ___m_MovementType_7;
	// System.Single UnityEngine.UI.ScrollRect::m_Elasticity
	float ___m_Elasticity_8;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Inertia
	bool ___m_Inertia_9;
	// System.Single UnityEngine.UI.ScrollRect::m_DecelerationRate
	float ___m_DecelerationRate_10;
	// System.Single UnityEngine.UI.ScrollRect::m_ScrollSensitivity
	float ___m_ScrollSensitivity_11;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Viewport
	RectTransform_t3704657025 * ___m_Viewport_12;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_HorizontalScrollbar
	Scrollbar_t1494447233 * ___m_HorizontalScrollbar_13;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_VerticalScrollbar
	Scrollbar_t1494447233 * ___m_VerticalScrollbar_14;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_HorizontalScrollbarVisibility
	int32_t ___m_HorizontalScrollbarVisibility_15;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_VerticalScrollbarVisibility
	int32_t ___m_VerticalScrollbarVisibility_16;
	// System.Single UnityEngine.UI.ScrollRect::m_HorizontalScrollbarSpacing
	float ___m_HorizontalScrollbarSpacing_17;
	// System.Single UnityEngine.UI.ScrollRect::m_VerticalScrollbarSpacing
	float ___m_VerticalScrollbarSpacing_18;
	// UnityEngine.UI.ScrollRect/ScrollRectEvent UnityEngine.UI.ScrollRect::m_OnValueChanged
	ScrollRectEvent_t343079324 * ___m_OnValueChanged_19;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PointerStartLocalCursor
	Vector2_t2156229523  ___m_PointerStartLocalCursor_20;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_ContentStartPosition
	Vector2_t2156229523  ___m_ContentStartPosition_21;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_ViewRect
	RectTransform_t3704657025 * ___m_ViewRect_22;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ContentBounds
	Bounds_t2266837910  ___m_ContentBounds_23;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ViewBounds
	Bounds_t2266837910  ___m_ViewBounds_24;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_Velocity
	Vector2_t2156229523  ___m_Velocity_25;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Dragging
	bool ___m_Dragging_26;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PrevPosition
	Vector2_t2156229523  ___m_PrevPosition_27;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevContentBounds
	Bounds_t2266837910  ___m_PrevContentBounds_28;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevViewBounds
	Bounds_t2266837910  ___m_PrevViewBounds_29;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HasRebuiltLayout
	bool ___m_HasRebuiltLayout_30;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HSliderExpand
	bool ___m_HSliderExpand_31;
	// System.Boolean UnityEngine.UI.ScrollRect::m_VSliderExpand
	bool ___m_VSliderExpand_32;
	// System.Single UnityEngine.UI.ScrollRect::m_HSliderHeight
	float ___m_HSliderHeight_33;
	// System.Single UnityEngine.UI.ScrollRect::m_VSliderWidth
	float ___m_VSliderWidth_34;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Rect
	RectTransform_t3704657025 * ___m_Rect_35;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_HorizontalScrollbarRect
	RectTransform_t3704657025 * ___m_HorizontalScrollbarRect_36;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_VerticalScrollbarRect
	RectTransform_t3704657025 * ___m_VerticalScrollbarRect_37;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ScrollRect::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_38;
	// UnityEngine.Vector3[] UnityEngine.UI.ScrollRect::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_39;

public:
	inline static int32_t get_offset_of_m_Content_4() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Content_4)); }
	inline RectTransform_t3704657025 * get_m_Content_4() const { return ___m_Content_4; }
	inline RectTransform_t3704657025 ** get_address_of_m_Content_4() { return &___m_Content_4; }
	inline void set_m_Content_4(RectTransform_t3704657025 * value)
	{
		___m_Content_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_4), value);
	}

	inline static int32_t get_offset_of_m_Horizontal_5() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Horizontal_5)); }
	inline bool get_m_Horizontal_5() const { return ___m_Horizontal_5; }
	inline bool* get_address_of_m_Horizontal_5() { return &___m_Horizontal_5; }
	inline void set_m_Horizontal_5(bool value)
	{
		___m_Horizontal_5 = value;
	}

	inline static int32_t get_offset_of_m_Vertical_6() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Vertical_6)); }
	inline bool get_m_Vertical_6() const { return ___m_Vertical_6; }
	inline bool* get_address_of_m_Vertical_6() { return &___m_Vertical_6; }
	inline void set_m_Vertical_6(bool value)
	{
		___m_Vertical_6 = value;
	}

	inline static int32_t get_offset_of_m_MovementType_7() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_MovementType_7)); }
	inline int32_t get_m_MovementType_7() const { return ___m_MovementType_7; }
	inline int32_t* get_address_of_m_MovementType_7() { return &___m_MovementType_7; }
	inline void set_m_MovementType_7(int32_t value)
	{
		___m_MovementType_7 = value;
	}

	inline static int32_t get_offset_of_m_Elasticity_8() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Elasticity_8)); }
	inline float get_m_Elasticity_8() const { return ___m_Elasticity_8; }
	inline float* get_address_of_m_Elasticity_8() { return &___m_Elasticity_8; }
	inline void set_m_Elasticity_8(float value)
	{
		___m_Elasticity_8 = value;
	}

	inline static int32_t get_offset_of_m_Inertia_9() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Inertia_9)); }
	inline bool get_m_Inertia_9() const { return ___m_Inertia_9; }
	inline bool* get_address_of_m_Inertia_9() { return &___m_Inertia_9; }
	inline void set_m_Inertia_9(bool value)
	{
		___m_Inertia_9 = value;
	}

	inline static int32_t get_offset_of_m_DecelerationRate_10() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_DecelerationRate_10)); }
	inline float get_m_DecelerationRate_10() const { return ___m_DecelerationRate_10; }
	inline float* get_address_of_m_DecelerationRate_10() { return &___m_DecelerationRate_10; }
	inline void set_m_DecelerationRate_10(float value)
	{
		___m_DecelerationRate_10 = value;
	}

	inline static int32_t get_offset_of_m_ScrollSensitivity_11() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ScrollSensitivity_11)); }
	inline float get_m_ScrollSensitivity_11() const { return ___m_ScrollSensitivity_11; }
	inline float* get_address_of_m_ScrollSensitivity_11() { return &___m_ScrollSensitivity_11; }
	inline void set_m_ScrollSensitivity_11(float value)
	{
		___m_ScrollSensitivity_11 = value;
	}

	inline static int32_t get_offset_of_m_Viewport_12() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Viewport_12)); }
	inline RectTransform_t3704657025 * get_m_Viewport_12() const { return ___m_Viewport_12; }
	inline RectTransform_t3704657025 ** get_address_of_m_Viewport_12() { return &___m_Viewport_12; }
	inline void set_m_Viewport_12(RectTransform_t3704657025 * value)
	{
		___m_Viewport_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Viewport_12), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbar_13() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HorizontalScrollbar_13)); }
	inline Scrollbar_t1494447233 * get_m_HorizontalScrollbar_13() const { return ___m_HorizontalScrollbar_13; }
	inline Scrollbar_t1494447233 ** get_address_of_m_HorizontalScrollbar_13() { return &___m_HorizontalScrollbar_13; }
	inline void set_m_HorizontalScrollbar_13(Scrollbar_t1494447233 * value)
	{
		___m_HorizontalScrollbar_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalScrollbar_13), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbar_14() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VerticalScrollbar_14)); }
	inline Scrollbar_t1494447233 * get_m_VerticalScrollbar_14() const { return ___m_VerticalScrollbar_14; }
	inline Scrollbar_t1494447233 ** get_address_of_m_VerticalScrollbar_14() { return &___m_VerticalScrollbar_14; }
	inline void set_m_VerticalScrollbar_14(Scrollbar_t1494447233 * value)
	{
		___m_VerticalScrollbar_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbar_14), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarVisibility_15() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HorizontalScrollbarVisibility_15)); }
	inline int32_t get_m_HorizontalScrollbarVisibility_15() const { return ___m_HorizontalScrollbarVisibility_15; }
	inline int32_t* get_address_of_m_HorizontalScrollbarVisibility_15() { return &___m_HorizontalScrollbarVisibility_15; }
	inline void set_m_HorizontalScrollbarVisibility_15(int32_t value)
	{
		___m_HorizontalScrollbarVisibility_15 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarVisibility_16() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VerticalScrollbarVisibility_16)); }
	inline int32_t get_m_VerticalScrollbarVisibility_16() const { return ___m_VerticalScrollbarVisibility_16; }
	inline int32_t* get_address_of_m_VerticalScrollbarVisibility_16() { return &___m_VerticalScrollbarVisibility_16; }
	inline void set_m_VerticalScrollbarVisibility_16(int32_t value)
	{
		___m_VerticalScrollbarVisibility_16 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarSpacing_17() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HorizontalScrollbarSpacing_17)); }
	inline float get_m_HorizontalScrollbarSpacing_17() const { return ___m_HorizontalScrollbarSpacing_17; }
	inline float* get_address_of_m_HorizontalScrollbarSpacing_17() { return &___m_HorizontalScrollbarSpacing_17; }
	inline void set_m_HorizontalScrollbarSpacing_17(float value)
	{
		___m_HorizontalScrollbarSpacing_17 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarSpacing_18() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VerticalScrollbarSpacing_18)); }
	inline float get_m_VerticalScrollbarSpacing_18() const { return ___m_VerticalScrollbarSpacing_18; }
	inline float* get_address_of_m_VerticalScrollbarSpacing_18() { return &___m_VerticalScrollbarSpacing_18; }
	inline void set_m_VerticalScrollbarSpacing_18(float value)
	{
		___m_VerticalScrollbarSpacing_18 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_19() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_OnValueChanged_19)); }
	inline ScrollRectEvent_t343079324 * get_m_OnValueChanged_19() const { return ___m_OnValueChanged_19; }
	inline ScrollRectEvent_t343079324 ** get_address_of_m_OnValueChanged_19() { return &___m_OnValueChanged_19; }
	inline void set_m_OnValueChanged_19(ScrollRectEvent_t343079324 * value)
	{
		___m_OnValueChanged_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_19), value);
	}

	inline static int32_t get_offset_of_m_PointerStartLocalCursor_20() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_PointerStartLocalCursor_20)); }
	inline Vector2_t2156229523  get_m_PointerStartLocalCursor_20() const { return ___m_PointerStartLocalCursor_20; }
	inline Vector2_t2156229523 * get_address_of_m_PointerStartLocalCursor_20() { return &___m_PointerStartLocalCursor_20; }
	inline void set_m_PointerStartLocalCursor_20(Vector2_t2156229523  value)
	{
		___m_PointerStartLocalCursor_20 = value;
	}

	inline static int32_t get_offset_of_m_ContentStartPosition_21() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ContentStartPosition_21)); }
	inline Vector2_t2156229523  get_m_ContentStartPosition_21() const { return ___m_ContentStartPosition_21; }
	inline Vector2_t2156229523 * get_address_of_m_ContentStartPosition_21() { return &___m_ContentStartPosition_21; }
	inline void set_m_ContentStartPosition_21(Vector2_t2156229523  value)
	{
		___m_ContentStartPosition_21 = value;
	}

	inline static int32_t get_offset_of_m_ViewRect_22() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ViewRect_22)); }
	inline RectTransform_t3704657025 * get_m_ViewRect_22() const { return ___m_ViewRect_22; }
	inline RectTransform_t3704657025 ** get_address_of_m_ViewRect_22() { return &___m_ViewRect_22; }
	inline void set_m_ViewRect_22(RectTransform_t3704657025 * value)
	{
		___m_ViewRect_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ViewRect_22), value);
	}

	inline static int32_t get_offset_of_m_ContentBounds_23() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ContentBounds_23)); }
	inline Bounds_t2266837910  get_m_ContentBounds_23() const { return ___m_ContentBounds_23; }
	inline Bounds_t2266837910 * get_address_of_m_ContentBounds_23() { return &___m_ContentBounds_23; }
	inline void set_m_ContentBounds_23(Bounds_t2266837910  value)
	{
		___m_ContentBounds_23 = value;
	}

	inline static int32_t get_offset_of_m_ViewBounds_24() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ViewBounds_24)); }
	inline Bounds_t2266837910  get_m_ViewBounds_24() const { return ___m_ViewBounds_24; }
	inline Bounds_t2266837910 * get_address_of_m_ViewBounds_24() { return &___m_ViewBounds_24; }
	inline void set_m_ViewBounds_24(Bounds_t2266837910  value)
	{
		___m_ViewBounds_24 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_25() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Velocity_25)); }
	inline Vector2_t2156229523  get_m_Velocity_25() const { return ___m_Velocity_25; }
	inline Vector2_t2156229523 * get_address_of_m_Velocity_25() { return &___m_Velocity_25; }
	inline void set_m_Velocity_25(Vector2_t2156229523  value)
	{
		___m_Velocity_25 = value;
	}

	inline static int32_t get_offset_of_m_Dragging_26() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Dragging_26)); }
	inline bool get_m_Dragging_26() const { return ___m_Dragging_26; }
	inline bool* get_address_of_m_Dragging_26() { return &___m_Dragging_26; }
	inline void set_m_Dragging_26(bool value)
	{
		___m_Dragging_26 = value;
	}

	inline static int32_t get_offset_of_m_PrevPosition_27() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_PrevPosition_27)); }
	inline Vector2_t2156229523  get_m_PrevPosition_27() const { return ___m_PrevPosition_27; }
	inline Vector2_t2156229523 * get_address_of_m_PrevPosition_27() { return &___m_PrevPosition_27; }
	inline void set_m_PrevPosition_27(Vector2_t2156229523  value)
	{
		___m_PrevPosition_27 = value;
	}

	inline static int32_t get_offset_of_m_PrevContentBounds_28() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_PrevContentBounds_28)); }
	inline Bounds_t2266837910  get_m_PrevContentBounds_28() const { return ___m_PrevContentBounds_28; }
	inline Bounds_t2266837910 * get_address_of_m_PrevContentBounds_28() { return &___m_PrevContentBounds_28; }
	inline void set_m_PrevContentBounds_28(Bounds_t2266837910  value)
	{
		___m_PrevContentBounds_28 = value;
	}

	inline static int32_t get_offset_of_m_PrevViewBounds_29() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_PrevViewBounds_29)); }
	inline Bounds_t2266837910  get_m_PrevViewBounds_29() const { return ___m_PrevViewBounds_29; }
	inline Bounds_t2266837910 * get_address_of_m_PrevViewBounds_29() { return &___m_PrevViewBounds_29; }
	inline void set_m_PrevViewBounds_29(Bounds_t2266837910  value)
	{
		___m_PrevViewBounds_29 = value;
	}

	inline static int32_t get_offset_of_m_HasRebuiltLayout_30() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HasRebuiltLayout_30)); }
	inline bool get_m_HasRebuiltLayout_30() const { return ___m_HasRebuiltLayout_30; }
	inline bool* get_address_of_m_HasRebuiltLayout_30() { return &___m_HasRebuiltLayout_30; }
	inline void set_m_HasRebuiltLayout_30(bool value)
	{
		___m_HasRebuiltLayout_30 = value;
	}

	inline static int32_t get_offset_of_m_HSliderExpand_31() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HSliderExpand_31)); }
	inline bool get_m_HSliderExpand_31() const { return ___m_HSliderExpand_31; }
	inline bool* get_address_of_m_HSliderExpand_31() { return &___m_HSliderExpand_31; }
	inline void set_m_HSliderExpand_31(bool value)
	{
		___m_HSliderExpand_31 = value;
	}

	inline static int32_t get_offset_of_m_VSliderExpand_32() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VSliderExpand_32)); }
	inline bool get_m_VSliderExpand_32() const { return ___m_VSliderExpand_32; }
	inline bool* get_address_of_m_VSliderExpand_32() { return &___m_VSliderExpand_32; }
	inline void set_m_VSliderExpand_32(bool value)
	{
		___m_VSliderExpand_32 = value;
	}

	inline static int32_t get_offset_of_m_HSliderHeight_33() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HSliderHeight_33)); }
	inline float get_m_HSliderHeight_33() const { return ___m_HSliderHeight_33; }
	inline float* get_address_of_m_HSliderHeight_33() { return &___m_HSliderHeight_33; }
	inline void set_m_HSliderHeight_33(float value)
	{
		___m_HSliderHeight_33 = value;
	}

	inline static int32_t get_offset_of_m_VSliderWidth_34() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VSliderWidth_34)); }
	inline float get_m_VSliderWidth_34() const { return ___m_VSliderWidth_34; }
	inline float* get_address_of_m_VSliderWidth_34() { return &___m_VSliderWidth_34; }
	inline void set_m_VSliderWidth_34(float value)
	{
		___m_VSliderWidth_34 = value;
	}

	inline static int32_t get_offset_of_m_Rect_35() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Rect_35)); }
	inline RectTransform_t3704657025 * get_m_Rect_35() const { return ___m_Rect_35; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_35() { return &___m_Rect_35; }
	inline void set_m_Rect_35(RectTransform_t3704657025 * value)
	{
		___m_Rect_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_35), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarRect_36() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HorizontalScrollbarRect_36)); }
	inline RectTransform_t3704657025 * get_m_HorizontalScrollbarRect_36() const { return ___m_HorizontalScrollbarRect_36; }
	inline RectTransform_t3704657025 ** get_address_of_m_HorizontalScrollbarRect_36() { return &___m_HorizontalScrollbarRect_36; }
	inline void set_m_HorizontalScrollbarRect_36(RectTransform_t3704657025 * value)
	{
		___m_HorizontalScrollbarRect_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalScrollbarRect_36), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarRect_37() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VerticalScrollbarRect_37)); }
	inline RectTransform_t3704657025 * get_m_VerticalScrollbarRect_37() const { return ___m_VerticalScrollbarRect_37; }
	inline RectTransform_t3704657025 ** get_address_of_m_VerticalScrollbarRect_37() { return &___m_VerticalScrollbarRect_37; }
	inline void set_m_VerticalScrollbarRect_37(RectTransform_t3704657025 * value)
	{
		___m_VerticalScrollbarRect_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbarRect_37), value);
	}

	inline static int32_t get_offset_of_m_Tracker_38() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Tracker_38)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_38() const { return ___m_Tracker_38; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_38() { return &___m_Tracker_38; }
	inline void set_m_Tracker_38(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_38 = value;
	}

	inline static int32_t get_offset_of_m_Corners_39() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Corners_39)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_39() const { return ___m_Corners_39; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_39() { return &___m_Corners_39; }
	inline void set_m_Corners_39(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLRECT_T4137855814_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_5;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_6;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_7;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_8;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_9;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_11;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_12;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_13;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_17;

public:
	inline static int32_t get_offset_of_m_Navigation_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_5)); }
	inline Navigation_t3049316579  get_m_Navigation_5() const { return ___m_Navigation_5; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_5() { return &___m_Navigation_5; }
	inline void set_m_Navigation_5(Navigation_t3049316579  value)
	{
		___m_Navigation_5 = value;
	}

	inline static int32_t get_offset_of_m_Transition_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_6)); }
	inline int32_t get_m_Transition_6() const { return ___m_Transition_6; }
	inline int32_t* get_address_of_m_Transition_6() { return &___m_Transition_6; }
	inline void set_m_Transition_6(int32_t value)
	{
		___m_Transition_6 = value;
	}

	inline static int32_t get_offset_of_m_Colors_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_7)); }
	inline ColorBlock_t2139031574  get_m_Colors_7() const { return ___m_Colors_7; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_7() { return &___m_Colors_7; }
	inline void set_m_Colors_7(ColorBlock_t2139031574  value)
	{
		___m_Colors_7 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_8)); }
	inline SpriteState_t1362986479  get_m_SpriteState_8() const { return ___m_SpriteState_8; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_8() { return &___m_SpriteState_8; }
	inline void set_m_SpriteState_8(SpriteState_t1362986479  value)
	{
		___m_SpriteState_8 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_9)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_9() const { return ___m_AnimationTriggers_9; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_9() { return &___m_AnimationTriggers_9; }
	inline void set_m_AnimationTriggers_9(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_9), value);
	}

	inline static int32_t get_offset_of_m_Interactable_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_10)); }
	inline bool get_m_Interactable_10() const { return ___m_Interactable_10; }
	inline bool* get_address_of_m_Interactable_10() { return &___m_Interactable_10; }
	inline void set_m_Interactable_10(bool value)
	{
		___m_Interactable_10 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_11)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_11() const { return ___m_TargetGraphic_11; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_11() { return &___m_TargetGraphic_11; }
	inline void set_m_TargetGraphic_11(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_11), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_12)); }
	inline bool get_m_GroupsAllowInteraction_12() const { return ___m_GroupsAllowInteraction_12; }
	inline bool* get_address_of_m_GroupsAllowInteraction_12() { return &___m_GroupsAllowInteraction_12; }
	inline void set_m_GroupsAllowInteraction_12(bool value)
	{
		___m_GroupsAllowInteraction_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_13)); }
	inline int32_t get_m_CurrentSelectionState_13() const { return ___m_CurrentSelectionState_13; }
	inline int32_t* get_address_of_m_CurrentSelectionState_13() { return &___m_CurrentSelectionState_13; }
	inline void set_m_CurrentSelectionState_13(int32_t value)
	{
		___m_CurrentSelectionState_13 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_14)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_14() const { return ___U3CisPointerInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_14() { return &___U3CisPointerInsideU3Ek__BackingField_14; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_14(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_15() const { return ___U3CisPointerDownU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_15() { return &___U3CisPointerDownU3Ek__BackingField_15; }
	inline void set_U3CisPointerDownU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_16)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_16() const { return ___U3ChasSelectionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_16() { return &___U3ChasSelectionU3Ek__BackingField_16; }
	inline void set_U3ChasSelectionU3Ek__BackingField_16(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_17() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_17)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_17() const { return ___m_CanvasGroupCache_17; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_17() { return &___m_CanvasGroupCache_17; }
	inline void set_m_CanvasGroupCache_17(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_17), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_4;

public:
	inline static int32_t get_offset_of_s_List_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_4)); }
	inline List_1_t427135887 * get_s_List_4() const { return ___s_List_4; }
	inline List_1_t427135887 ** get_address_of_s_List_4() { return &___s_List_4; }
	inline void set_s_List_4(List_1_t427135887 * value)
	{
		___s_List_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef BUTTON_T4055032469_H
#define BUTTON_T4055032469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Button
struct  Button_t4055032469  : public Selectable_t3250028441
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t48803504 * ___m_OnClick_18;

public:
	inline static int32_t get_offset_of_m_OnClick_18() { return static_cast<int32_t>(offsetof(Button_t4055032469, ___m_OnClick_18)); }
	inline ButtonClickedEvent_t48803504 * get_m_OnClick_18() const { return ___m_OnClick_18; }
	inline ButtonClickedEvent_t48803504 ** get_address_of_m_OnClick_18() { return &___m_OnClick_18; }
	inline void set_m_OnClick_18(ButtonClickedEvent_t48803504 * value)
	{
		___m_OnClick_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnClick_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T4055032469_H
#ifndef DROPDOWN_T2274391225_H
#define DROPDOWN_T2274391225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Dropdown
struct  Dropdown_t2274391225  : public Selectable_t3250028441
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Dropdown::m_Template
	RectTransform_t3704657025 * ___m_Template_18;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_CaptionText
	Text_t1901882714 * ___m_CaptionText_19;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_CaptionImage
	Image_t2670269651 * ___m_CaptionImage_20;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_ItemText
	Text_t1901882714 * ___m_ItemText_21;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_ItemImage
	Image_t2670269651 * ___m_ItemImage_22;
	// System.Int32 UnityEngine.UI.Dropdown::m_Value
	int32_t ___m_Value_23;
	// UnityEngine.UI.Dropdown/OptionDataList UnityEngine.UI.Dropdown::m_Options
	OptionDataList_t1438173104 * ___m_Options_24;
	// UnityEngine.UI.Dropdown/DropdownEvent UnityEngine.UI.Dropdown::m_OnValueChanged
	DropdownEvent_t4040729994 * ___m_OnValueChanged_25;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Dropdown
	GameObject_t1113636619 * ___m_Dropdown_26;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Blocker
	GameObject_t1113636619 * ___m_Blocker_27;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem> UnityEngine.UI.Dropdown::m_Items
	List_1_t2924027637 * ___m_Items_28;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween> UnityEngine.UI.Dropdown::m_AlphaTweenRunner
	TweenRunner_1_t3520241082 * ___m_AlphaTweenRunner_29;
	// System.Boolean UnityEngine.UI.Dropdown::validTemplate
	bool ___validTemplate_30;

public:
	inline static int32_t get_offset_of_m_Template_18() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Template_18)); }
	inline RectTransform_t3704657025 * get_m_Template_18() const { return ___m_Template_18; }
	inline RectTransform_t3704657025 ** get_address_of_m_Template_18() { return &___m_Template_18; }
	inline void set_m_Template_18(RectTransform_t3704657025 * value)
	{
		___m_Template_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Template_18), value);
	}

	inline static int32_t get_offset_of_m_CaptionText_19() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_CaptionText_19)); }
	inline Text_t1901882714 * get_m_CaptionText_19() const { return ___m_CaptionText_19; }
	inline Text_t1901882714 ** get_address_of_m_CaptionText_19() { return &___m_CaptionText_19; }
	inline void set_m_CaptionText_19(Text_t1901882714 * value)
	{
		___m_CaptionText_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_CaptionText_19), value);
	}

	inline static int32_t get_offset_of_m_CaptionImage_20() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_CaptionImage_20)); }
	inline Image_t2670269651 * get_m_CaptionImage_20() const { return ___m_CaptionImage_20; }
	inline Image_t2670269651 ** get_address_of_m_CaptionImage_20() { return &___m_CaptionImage_20; }
	inline void set_m_CaptionImage_20(Image_t2670269651 * value)
	{
		___m_CaptionImage_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_CaptionImage_20), value);
	}

	inline static int32_t get_offset_of_m_ItemText_21() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_ItemText_21)); }
	inline Text_t1901882714 * get_m_ItemText_21() const { return ___m_ItemText_21; }
	inline Text_t1901882714 ** get_address_of_m_ItemText_21() { return &___m_ItemText_21; }
	inline void set_m_ItemText_21(Text_t1901882714 * value)
	{
		___m_ItemText_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemText_21), value);
	}

	inline static int32_t get_offset_of_m_ItemImage_22() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_ItemImage_22)); }
	inline Image_t2670269651 * get_m_ItemImage_22() const { return ___m_ItemImage_22; }
	inline Image_t2670269651 ** get_address_of_m_ItemImage_22() { return &___m_ItemImage_22; }
	inline void set_m_ItemImage_22(Image_t2670269651 * value)
	{
		___m_ItemImage_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemImage_22), value);
	}

	inline static int32_t get_offset_of_m_Value_23() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Value_23)); }
	inline int32_t get_m_Value_23() const { return ___m_Value_23; }
	inline int32_t* get_address_of_m_Value_23() { return &___m_Value_23; }
	inline void set_m_Value_23(int32_t value)
	{
		___m_Value_23 = value;
	}

	inline static int32_t get_offset_of_m_Options_24() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Options_24)); }
	inline OptionDataList_t1438173104 * get_m_Options_24() const { return ___m_Options_24; }
	inline OptionDataList_t1438173104 ** get_address_of_m_Options_24() { return &___m_Options_24; }
	inline void set_m_Options_24(OptionDataList_t1438173104 * value)
	{
		___m_Options_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_Options_24), value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_25() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_OnValueChanged_25)); }
	inline DropdownEvent_t4040729994 * get_m_OnValueChanged_25() const { return ___m_OnValueChanged_25; }
	inline DropdownEvent_t4040729994 ** get_address_of_m_OnValueChanged_25() { return &___m_OnValueChanged_25; }
	inline void set_m_OnValueChanged_25(DropdownEvent_t4040729994 * value)
	{
		___m_OnValueChanged_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_25), value);
	}

	inline static int32_t get_offset_of_m_Dropdown_26() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Dropdown_26)); }
	inline GameObject_t1113636619 * get_m_Dropdown_26() const { return ___m_Dropdown_26; }
	inline GameObject_t1113636619 ** get_address_of_m_Dropdown_26() { return &___m_Dropdown_26; }
	inline void set_m_Dropdown_26(GameObject_t1113636619 * value)
	{
		___m_Dropdown_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dropdown_26), value);
	}

	inline static int32_t get_offset_of_m_Blocker_27() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Blocker_27)); }
	inline GameObject_t1113636619 * get_m_Blocker_27() const { return ___m_Blocker_27; }
	inline GameObject_t1113636619 ** get_address_of_m_Blocker_27() { return &___m_Blocker_27; }
	inline void set_m_Blocker_27(GameObject_t1113636619 * value)
	{
		___m_Blocker_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Blocker_27), value);
	}

	inline static int32_t get_offset_of_m_Items_28() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Items_28)); }
	inline List_1_t2924027637 * get_m_Items_28() const { return ___m_Items_28; }
	inline List_1_t2924027637 ** get_address_of_m_Items_28() { return &___m_Items_28; }
	inline void set_m_Items_28(List_1_t2924027637 * value)
	{
		___m_Items_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_Items_28), value);
	}

	inline static int32_t get_offset_of_m_AlphaTweenRunner_29() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_AlphaTweenRunner_29)); }
	inline TweenRunner_1_t3520241082 * get_m_AlphaTweenRunner_29() const { return ___m_AlphaTweenRunner_29; }
	inline TweenRunner_1_t3520241082 ** get_address_of_m_AlphaTweenRunner_29() { return &___m_AlphaTweenRunner_29; }
	inline void set_m_AlphaTweenRunner_29(TweenRunner_1_t3520241082 * value)
	{
		___m_AlphaTweenRunner_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlphaTweenRunner_29), value);
	}

	inline static int32_t get_offset_of_validTemplate_30() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___validTemplate_30)); }
	inline bool get_validTemplate_30() const { return ___validTemplate_30; }
	inline bool* get_address_of_validTemplate_30() { return &___validTemplate_30; }
	inline void set_validTemplate_30(bool value)
	{
		___validTemplate_30 = value;
	}
};

struct Dropdown_t2274391225_StaticFields
{
public:
	// UnityEngine.UI.Dropdown/OptionData UnityEngine.UI.Dropdown::s_NoOptionData
	OptionData_t3270282352 * ___s_NoOptionData_31;

public:
	inline static int32_t get_offset_of_s_NoOptionData_31() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225_StaticFields, ___s_NoOptionData_31)); }
	inline OptionData_t3270282352 * get_s_NoOptionData_31() const { return ___s_NoOptionData_31; }
	inline OptionData_t3270282352 ** get_address_of_s_NoOptionData_31() { return &___s_NoOptionData_31; }
	inline void set_s_NoOptionData_31(OptionData_t3270282352 * value)
	{
		___s_NoOptionData_31 = value;
		Il2CppCodeGenWriteBarrier((&___s_NoOptionData_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWN_T2274391225_H
#ifndef INPUTFIELD_T3762917431_H
#define INPUTFIELD_T3762917431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField
struct  InputField_t3762917431  : public Selectable_t3250028441
{
public:
	// UnityEngine.TouchScreenKeyboard UnityEngine.UI.InputField::m_Keyboard
	TouchScreenKeyboard_t731888065 * ___m_Keyboard_18;
	// UnityEngine.UI.Text UnityEngine.UI.InputField::m_TextComponent
	Text_t1901882714 * ___m_TextComponent_20;
	// UnityEngine.UI.Graphic UnityEngine.UI.InputField::m_Placeholder
	Graphic_t1660335611 * ___m_Placeholder_21;
	// UnityEngine.UI.InputField/ContentType UnityEngine.UI.InputField::m_ContentType
	int32_t ___m_ContentType_22;
	// UnityEngine.UI.InputField/InputType UnityEngine.UI.InputField::m_InputType
	int32_t ___m_InputType_23;
	// System.Char UnityEngine.UI.InputField::m_AsteriskChar
	Il2CppChar ___m_AsteriskChar_24;
	// UnityEngine.TouchScreenKeyboardType UnityEngine.UI.InputField::m_KeyboardType
	int32_t ___m_KeyboardType_25;
	// UnityEngine.UI.InputField/LineType UnityEngine.UI.InputField::m_LineType
	int32_t ___m_LineType_26;
	// System.Boolean UnityEngine.UI.InputField::m_HideMobileInput
	bool ___m_HideMobileInput_27;
	// UnityEngine.UI.InputField/CharacterValidation UnityEngine.UI.InputField::m_CharacterValidation
	int32_t ___m_CharacterValidation_28;
	// System.Int32 UnityEngine.UI.InputField::m_CharacterLimit
	int32_t ___m_CharacterLimit_29;
	// UnityEngine.UI.InputField/SubmitEvent UnityEngine.UI.InputField::m_OnEndEdit
	SubmitEvent_t648412432 * ___m_OnEndEdit_30;
	// UnityEngine.UI.InputField/OnChangeEvent UnityEngine.UI.InputField::m_OnValueChanged
	OnChangeEvent_t467195904 * ___m_OnValueChanged_31;
	// UnityEngine.UI.InputField/OnValidateInput UnityEngine.UI.InputField::m_OnValidateInput
	OnValidateInput_t2355412304 * ___m_OnValidateInput_32;
	// UnityEngine.Color UnityEngine.UI.InputField::m_CaretColor
	Color_t2555686324  ___m_CaretColor_33;
	// System.Boolean UnityEngine.UI.InputField::m_CustomCaretColor
	bool ___m_CustomCaretColor_34;
	// UnityEngine.Color UnityEngine.UI.InputField::m_SelectionColor
	Color_t2555686324  ___m_SelectionColor_35;
	// System.String UnityEngine.UI.InputField::m_Text
	String_t* ___m_Text_36;
	// System.Single UnityEngine.UI.InputField::m_CaretBlinkRate
	float ___m_CaretBlinkRate_37;
	// System.Int32 UnityEngine.UI.InputField::m_CaretWidth
	int32_t ___m_CaretWidth_38;
	// System.Boolean UnityEngine.UI.InputField::m_ReadOnly
	bool ___m_ReadOnly_39;
	// System.Int32 UnityEngine.UI.InputField::m_CaretPosition
	int32_t ___m_CaretPosition_40;
	// System.Int32 UnityEngine.UI.InputField::m_CaretSelectPosition
	int32_t ___m_CaretSelectPosition_41;
	// UnityEngine.RectTransform UnityEngine.UI.InputField::caretRectTrans
	RectTransform_t3704657025 * ___caretRectTrans_42;
	// UnityEngine.UIVertex[] UnityEngine.UI.InputField::m_CursorVerts
	UIVertexU5BU5D_t1981460040* ___m_CursorVerts_43;
	// UnityEngine.TextGenerator UnityEngine.UI.InputField::m_InputTextCache
	TextGenerator_t3211863866 * ___m_InputTextCache_44;
	// UnityEngine.CanvasRenderer UnityEngine.UI.InputField::m_CachedInputRenderer
	CanvasRenderer_t2598313366 * ___m_CachedInputRenderer_45;
	// System.Boolean UnityEngine.UI.InputField::m_PreventFontCallback
	bool ___m_PreventFontCallback_46;
	// UnityEngine.Mesh UnityEngine.UI.InputField::m_Mesh
	Mesh_t3648964284 * ___m_Mesh_47;
	// System.Boolean UnityEngine.UI.InputField::m_AllowInput
	bool ___m_AllowInput_48;
	// System.Boolean UnityEngine.UI.InputField::m_ShouldActivateNextUpdate
	bool ___m_ShouldActivateNextUpdate_49;
	// System.Boolean UnityEngine.UI.InputField::m_UpdateDrag
	bool ___m_UpdateDrag_50;
	// System.Boolean UnityEngine.UI.InputField::m_DragPositionOutOfBounds
	bool ___m_DragPositionOutOfBounds_51;
	// System.Boolean UnityEngine.UI.InputField::m_CaretVisible
	bool ___m_CaretVisible_54;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_BlinkCoroutine
	Coroutine_t3829159415 * ___m_BlinkCoroutine_55;
	// System.Single UnityEngine.UI.InputField::m_BlinkStartTime
	float ___m_BlinkStartTime_56;
	// System.Int32 UnityEngine.UI.InputField::m_DrawStart
	int32_t ___m_DrawStart_57;
	// System.Int32 UnityEngine.UI.InputField::m_DrawEnd
	int32_t ___m_DrawEnd_58;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_DragCoroutine
	Coroutine_t3829159415 * ___m_DragCoroutine_59;
	// System.String UnityEngine.UI.InputField::m_OriginalText
	String_t* ___m_OriginalText_60;
	// System.Boolean UnityEngine.UI.InputField::m_WasCanceled
	bool ___m_WasCanceled_61;
	// System.Boolean UnityEngine.UI.InputField::m_HasDoneFocusTransition
	bool ___m_HasDoneFocusTransition_62;
	// UnityEngine.Event UnityEngine.UI.InputField::m_ProcessingEvent
	Event_t2956885303 * ___m_ProcessingEvent_64;

public:
	inline static int32_t get_offset_of_m_Keyboard_18() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Keyboard_18)); }
	inline TouchScreenKeyboard_t731888065 * get_m_Keyboard_18() const { return ___m_Keyboard_18; }
	inline TouchScreenKeyboard_t731888065 ** get_address_of_m_Keyboard_18() { return &___m_Keyboard_18; }
	inline void set_m_Keyboard_18(TouchScreenKeyboard_t731888065 * value)
	{
		___m_Keyboard_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Keyboard_18), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_20() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_TextComponent_20)); }
	inline Text_t1901882714 * get_m_TextComponent_20() const { return ___m_TextComponent_20; }
	inline Text_t1901882714 ** get_address_of_m_TextComponent_20() { return &___m_TextComponent_20; }
	inline void set_m_TextComponent_20(Text_t1901882714 * value)
	{
		___m_TextComponent_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_20), value);
	}

	inline static int32_t get_offset_of_m_Placeholder_21() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Placeholder_21)); }
	inline Graphic_t1660335611 * get_m_Placeholder_21() const { return ___m_Placeholder_21; }
	inline Graphic_t1660335611 ** get_address_of_m_Placeholder_21() { return &___m_Placeholder_21; }
	inline void set_m_Placeholder_21(Graphic_t1660335611 * value)
	{
		___m_Placeholder_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Placeholder_21), value);
	}

	inline static int32_t get_offset_of_m_ContentType_22() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ContentType_22)); }
	inline int32_t get_m_ContentType_22() const { return ___m_ContentType_22; }
	inline int32_t* get_address_of_m_ContentType_22() { return &___m_ContentType_22; }
	inline void set_m_ContentType_22(int32_t value)
	{
		___m_ContentType_22 = value;
	}

	inline static int32_t get_offset_of_m_InputType_23() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_InputType_23)); }
	inline int32_t get_m_InputType_23() const { return ___m_InputType_23; }
	inline int32_t* get_address_of_m_InputType_23() { return &___m_InputType_23; }
	inline void set_m_InputType_23(int32_t value)
	{
		___m_InputType_23 = value;
	}

	inline static int32_t get_offset_of_m_AsteriskChar_24() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_AsteriskChar_24)); }
	inline Il2CppChar get_m_AsteriskChar_24() const { return ___m_AsteriskChar_24; }
	inline Il2CppChar* get_address_of_m_AsteriskChar_24() { return &___m_AsteriskChar_24; }
	inline void set_m_AsteriskChar_24(Il2CppChar value)
	{
		___m_AsteriskChar_24 = value;
	}

	inline static int32_t get_offset_of_m_KeyboardType_25() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_KeyboardType_25)); }
	inline int32_t get_m_KeyboardType_25() const { return ___m_KeyboardType_25; }
	inline int32_t* get_address_of_m_KeyboardType_25() { return &___m_KeyboardType_25; }
	inline void set_m_KeyboardType_25(int32_t value)
	{
		___m_KeyboardType_25 = value;
	}

	inline static int32_t get_offset_of_m_LineType_26() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_LineType_26)); }
	inline int32_t get_m_LineType_26() const { return ___m_LineType_26; }
	inline int32_t* get_address_of_m_LineType_26() { return &___m_LineType_26; }
	inline void set_m_LineType_26(int32_t value)
	{
		___m_LineType_26 = value;
	}

	inline static int32_t get_offset_of_m_HideMobileInput_27() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_HideMobileInput_27)); }
	inline bool get_m_HideMobileInput_27() const { return ___m_HideMobileInput_27; }
	inline bool* get_address_of_m_HideMobileInput_27() { return &___m_HideMobileInput_27; }
	inline void set_m_HideMobileInput_27(bool value)
	{
		___m_HideMobileInput_27 = value;
	}

	inline static int32_t get_offset_of_m_CharacterValidation_28() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CharacterValidation_28)); }
	inline int32_t get_m_CharacterValidation_28() const { return ___m_CharacterValidation_28; }
	inline int32_t* get_address_of_m_CharacterValidation_28() { return &___m_CharacterValidation_28; }
	inline void set_m_CharacterValidation_28(int32_t value)
	{
		___m_CharacterValidation_28 = value;
	}

	inline static int32_t get_offset_of_m_CharacterLimit_29() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CharacterLimit_29)); }
	inline int32_t get_m_CharacterLimit_29() const { return ___m_CharacterLimit_29; }
	inline int32_t* get_address_of_m_CharacterLimit_29() { return &___m_CharacterLimit_29; }
	inline void set_m_CharacterLimit_29(int32_t value)
	{
		___m_CharacterLimit_29 = value;
	}

	inline static int32_t get_offset_of_m_OnEndEdit_30() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OnEndEdit_30)); }
	inline SubmitEvent_t648412432 * get_m_OnEndEdit_30() const { return ___m_OnEndEdit_30; }
	inline SubmitEvent_t648412432 ** get_address_of_m_OnEndEdit_30() { return &___m_OnEndEdit_30; }
	inline void set_m_OnEndEdit_30(SubmitEvent_t648412432 * value)
	{
		___m_OnEndEdit_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnEndEdit_30), value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_31() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OnValueChanged_31)); }
	inline OnChangeEvent_t467195904 * get_m_OnValueChanged_31() const { return ___m_OnValueChanged_31; }
	inline OnChangeEvent_t467195904 ** get_address_of_m_OnValueChanged_31() { return &___m_OnValueChanged_31; }
	inline void set_m_OnValueChanged_31(OnChangeEvent_t467195904 * value)
	{
		___m_OnValueChanged_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_31), value);
	}

	inline static int32_t get_offset_of_m_OnValidateInput_32() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OnValidateInput_32)); }
	inline OnValidateInput_t2355412304 * get_m_OnValidateInput_32() const { return ___m_OnValidateInput_32; }
	inline OnValidateInput_t2355412304 ** get_address_of_m_OnValidateInput_32() { return &___m_OnValidateInput_32; }
	inline void set_m_OnValidateInput_32(OnValidateInput_t2355412304 * value)
	{
		___m_OnValidateInput_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValidateInput_32), value);
	}

	inline static int32_t get_offset_of_m_CaretColor_33() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretColor_33)); }
	inline Color_t2555686324  get_m_CaretColor_33() const { return ___m_CaretColor_33; }
	inline Color_t2555686324 * get_address_of_m_CaretColor_33() { return &___m_CaretColor_33; }
	inline void set_m_CaretColor_33(Color_t2555686324  value)
	{
		___m_CaretColor_33 = value;
	}

	inline static int32_t get_offset_of_m_CustomCaretColor_34() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CustomCaretColor_34)); }
	inline bool get_m_CustomCaretColor_34() const { return ___m_CustomCaretColor_34; }
	inline bool* get_address_of_m_CustomCaretColor_34() { return &___m_CustomCaretColor_34; }
	inline void set_m_CustomCaretColor_34(bool value)
	{
		___m_CustomCaretColor_34 = value;
	}

	inline static int32_t get_offset_of_m_SelectionColor_35() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_SelectionColor_35)); }
	inline Color_t2555686324  get_m_SelectionColor_35() const { return ___m_SelectionColor_35; }
	inline Color_t2555686324 * get_address_of_m_SelectionColor_35() { return &___m_SelectionColor_35; }
	inline void set_m_SelectionColor_35(Color_t2555686324  value)
	{
		___m_SelectionColor_35 = value;
	}

	inline static int32_t get_offset_of_m_Text_36() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Text_36)); }
	inline String_t* get_m_Text_36() const { return ___m_Text_36; }
	inline String_t** get_address_of_m_Text_36() { return &___m_Text_36; }
	inline void set_m_Text_36(String_t* value)
	{
		___m_Text_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_36), value);
	}

	inline static int32_t get_offset_of_m_CaretBlinkRate_37() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretBlinkRate_37)); }
	inline float get_m_CaretBlinkRate_37() const { return ___m_CaretBlinkRate_37; }
	inline float* get_address_of_m_CaretBlinkRate_37() { return &___m_CaretBlinkRate_37; }
	inline void set_m_CaretBlinkRate_37(float value)
	{
		___m_CaretBlinkRate_37 = value;
	}

	inline static int32_t get_offset_of_m_CaretWidth_38() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretWidth_38)); }
	inline int32_t get_m_CaretWidth_38() const { return ___m_CaretWidth_38; }
	inline int32_t* get_address_of_m_CaretWidth_38() { return &___m_CaretWidth_38; }
	inline void set_m_CaretWidth_38(int32_t value)
	{
		___m_CaretWidth_38 = value;
	}

	inline static int32_t get_offset_of_m_ReadOnly_39() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ReadOnly_39)); }
	inline bool get_m_ReadOnly_39() const { return ___m_ReadOnly_39; }
	inline bool* get_address_of_m_ReadOnly_39() { return &___m_ReadOnly_39; }
	inline void set_m_ReadOnly_39(bool value)
	{
		___m_ReadOnly_39 = value;
	}

	inline static int32_t get_offset_of_m_CaretPosition_40() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretPosition_40)); }
	inline int32_t get_m_CaretPosition_40() const { return ___m_CaretPosition_40; }
	inline int32_t* get_address_of_m_CaretPosition_40() { return &___m_CaretPosition_40; }
	inline void set_m_CaretPosition_40(int32_t value)
	{
		___m_CaretPosition_40 = value;
	}

	inline static int32_t get_offset_of_m_CaretSelectPosition_41() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretSelectPosition_41)); }
	inline int32_t get_m_CaretSelectPosition_41() const { return ___m_CaretSelectPosition_41; }
	inline int32_t* get_address_of_m_CaretSelectPosition_41() { return &___m_CaretSelectPosition_41; }
	inline void set_m_CaretSelectPosition_41(int32_t value)
	{
		___m_CaretSelectPosition_41 = value;
	}

	inline static int32_t get_offset_of_caretRectTrans_42() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___caretRectTrans_42)); }
	inline RectTransform_t3704657025 * get_caretRectTrans_42() const { return ___caretRectTrans_42; }
	inline RectTransform_t3704657025 ** get_address_of_caretRectTrans_42() { return &___caretRectTrans_42; }
	inline void set_caretRectTrans_42(RectTransform_t3704657025 * value)
	{
		___caretRectTrans_42 = value;
		Il2CppCodeGenWriteBarrier((&___caretRectTrans_42), value);
	}

	inline static int32_t get_offset_of_m_CursorVerts_43() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CursorVerts_43)); }
	inline UIVertexU5BU5D_t1981460040* get_m_CursorVerts_43() const { return ___m_CursorVerts_43; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_CursorVerts_43() { return &___m_CursorVerts_43; }
	inline void set_m_CursorVerts_43(UIVertexU5BU5D_t1981460040* value)
	{
		___m_CursorVerts_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_CursorVerts_43), value);
	}

	inline static int32_t get_offset_of_m_InputTextCache_44() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_InputTextCache_44)); }
	inline TextGenerator_t3211863866 * get_m_InputTextCache_44() const { return ___m_InputTextCache_44; }
	inline TextGenerator_t3211863866 ** get_address_of_m_InputTextCache_44() { return &___m_InputTextCache_44; }
	inline void set_m_InputTextCache_44(TextGenerator_t3211863866 * value)
	{
		___m_InputTextCache_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputTextCache_44), value);
	}

	inline static int32_t get_offset_of_m_CachedInputRenderer_45() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CachedInputRenderer_45)); }
	inline CanvasRenderer_t2598313366 * get_m_CachedInputRenderer_45() const { return ___m_CachedInputRenderer_45; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CachedInputRenderer_45() { return &___m_CachedInputRenderer_45; }
	inline void set_m_CachedInputRenderer_45(CanvasRenderer_t2598313366 * value)
	{
		___m_CachedInputRenderer_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedInputRenderer_45), value);
	}

	inline static int32_t get_offset_of_m_PreventFontCallback_46() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_PreventFontCallback_46)); }
	inline bool get_m_PreventFontCallback_46() const { return ___m_PreventFontCallback_46; }
	inline bool* get_address_of_m_PreventFontCallback_46() { return &___m_PreventFontCallback_46; }
	inline void set_m_PreventFontCallback_46(bool value)
	{
		___m_PreventFontCallback_46 = value;
	}

	inline static int32_t get_offset_of_m_Mesh_47() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Mesh_47)); }
	inline Mesh_t3648964284 * get_m_Mesh_47() const { return ___m_Mesh_47; }
	inline Mesh_t3648964284 ** get_address_of_m_Mesh_47() { return &___m_Mesh_47; }
	inline void set_m_Mesh_47(Mesh_t3648964284 * value)
	{
		___m_Mesh_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mesh_47), value);
	}

	inline static int32_t get_offset_of_m_AllowInput_48() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_AllowInput_48)); }
	inline bool get_m_AllowInput_48() const { return ___m_AllowInput_48; }
	inline bool* get_address_of_m_AllowInput_48() { return &___m_AllowInput_48; }
	inline void set_m_AllowInput_48(bool value)
	{
		___m_AllowInput_48 = value;
	}

	inline static int32_t get_offset_of_m_ShouldActivateNextUpdate_49() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ShouldActivateNextUpdate_49)); }
	inline bool get_m_ShouldActivateNextUpdate_49() const { return ___m_ShouldActivateNextUpdate_49; }
	inline bool* get_address_of_m_ShouldActivateNextUpdate_49() { return &___m_ShouldActivateNextUpdate_49; }
	inline void set_m_ShouldActivateNextUpdate_49(bool value)
	{
		___m_ShouldActivateNextUpdate_49 = value;
	}

	inline static int32_t get_offset_of_m_UpdateDrag_50() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_UpdateDrag_50)); }
	inline bool get_m_UpdateDrag_50() const { return ___m_UpdateDrag_50; }
	inline bool* get_address_of_m_UpdateDrag_50() { return &___m_UpdateDrag_50; }
	inline void set_m_UpdateDrag_50(bool value)
	{
		___m_UpdateDrag_50 = value;
	}

	inline static int32_t get_offset_of_m_DragPositionOutOfBounds_51() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DragPositionOutOfBounds_51)); }
	inline bool get_m_DragPositionOutOfBounds_51() const { return ___m_DragPositionOutOfBounds_51; }
	inline bool* get_address_of_m_DragPositionOutOfBounds_51() { return &___m_DragPositionOutOfBounds_51; }
	inline void set_m_DragPositionOutOfBounds_51(bool value)
	{
		___m_DragPositionOutOfBounds_51 = value;
	}

	inline static int32_t get_offset_of_m_CaretVisible_54() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretVisible_54)); }
	inline bool get_m_CaretVisible_54() const { return ___m_CaretVisible_54; }
	inline bool* get_address_of_m_CaretVisible_54() { return &___m_CaretVisible_54; }
	inline void set_m_CaretVisible_54(bool value)
	{
		___m_CaretVisible_54 = value;
	}

	inline static int32_t get_offset_of_m_BlinkCoroutine_55() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_BlinkCoroutine_55)); }
	inline Coroutine_t3829159415 * get_m_BlinkCoroutine_55() const { return ___m_BlinkCoroutine_55; }
	inline Coroutine_t3829159415 ** get_address_of_m_BlinkCoroutine_55() { return &___m_BlinkCoroutine_55; }
	inline void set_m_BlinkCoroutine_55(Coroutine_t3829159415 * value)
	{
		___m_BlinkCoroutine_55 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlinkCoroutine_55), value);
	}

	inline static int32_t get_offset_of_m_BlinkStartTime_56() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_BlinkStartTime_56)); }
	inline float get_m_BlinkStartTime_56() const { return ___m_BlinkStartTime_56; }
	inline float* get_address_of_m_BlinkStartTime_56() { return &___m_BlinkStartTime_56; }
	inline void set_m_BlinkStartTime_56(float value)
	{
		___m_BlinkStartTime_56 = value;
	}

	inline static int32_t get_offset_of_m_DrawStart_57() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DrawStart_57)); }
	inline int32_t get_m_DrawStart_57() const { return ___m_DrawStart_57; }
	inline int32_t* get_address_of_m_DrawStart_57() { return &___m_DrawStart_57; }
	inline void set_m_DrawStart_57(int32_t value)
	{
		___m_DrawStart_57 = value;
	}

	inline static int32_t get_offset_of_m_DrawEnd_58() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DrawEnd_58)); }
	inline int32_t get_m_DrawEnd_58() const { return ___m_DrawEnd_58; }
	inline int32_t* get_address_of_m_DrawEnd_58() { return &___m_DrawEnd_58; }
	inline void set_m_DrawEnd_58(int32_t value)
	{
		___m_DrawEnd_58 = value;
	}

	inline static int32_t get_offset_of_m_DragCoroutine_59() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DragCoroutine_59)); }
	inline Coroutine_t3829159415 * get_m_DragCoroutine_59() const { return ___m_DragCoroutine_59; }
	inline Coroutine_t3829159415 ** get_address_of_m_DragCoroutine_59() { return &___m_DragCoroutine_59; }
	inline void set_m_DragCoroutine_59(Coroutine_t3829159415 * value)
	{
		___m_DragCoroutine_59 = value;
		Il2CppCodeGenWriteBarrier((&___m_DragCoroutine_59), value);
	}

	inline static int32_t get_offset_of_m_OriginalText_60() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OriginalText_60)); }
	inline String_t* get_m_OriginalText_60() const { return ___m_OriginalText_60; }
	inline String_t** get_address_of_m_OriginalText_60() { return &___m_OriginalText_60; }
	inline void set_m_OriginalText_60(String_t* value)
	{
		___m_OriginalText_60 = value;
		Il2CppCodeGenWriteBarrier((&___m_OriginalText_60), value);
	}

	inline static int32_t get_offset_of_m_WasCanceled_61() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_WasCanceled_61)); }
	inline bool get_m_WasCanceled_61() const { return ___m_WasCanceled_61; }
	inline bool* get_address_of_m_WasCanceled_61() { return &___m_WasCanceled_61; }
	inline void set_m_WasCanceled_61(bool value)
	{
		___m_WasCanceled_61 = value;
	}

	inline static int32_t get_offset_of_m_HasDoneFocusTransition_62() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_HasDoneFocusTransition_62)); }
	inline bool get_m_HasDoneFocusTransition_62() const { return ___m_HasDoneFocusTransition_62; }
	inline bool* get_address_of_m_HasDoneFocusTransition_62() { return &___m_HasDoneFocusTransition_62; }
	inline void set_m_HasDoneFocusTransition_62(bool value)
	{
		___m_HasDoneFocusTransition_62 = value;
	}

	inline static int32_t get_offset_of_m_ProcessingEvent_64() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ProcessingEvent_64)); }
	inline Event_t2956885303 * get_m_ProcessingEvent_64() const { return ___m_ProcessingEvent_64; }
	inline Event_t2956885303 ** get_address_of_m_ProcessingEvent_64() { return &___m_ProcessingEvent_64; }
	inline void set_m_ProcessingEvent_64(Event_t2956885303 * value)
	{
		___m_ProcessingEvent_64 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProcessingEvent_64), value);
	}
};

struct InputField_t3762917431_StaticFields
{
public:
	// System.Char[] UnityEngine.UI.InputField::kSeparators
	CharU5BU5D_t3528271667* ___kSeparators_19;

public:
	inline static int32_t get_offset_of_kSeparators_19() { return static_cast<int32_t>(offsetof(InputField_t3762917431_StaticFields, ___kSeparators_19)); }
	inline CharU5BU5D_t3528271667* get_kSeparators_19() const { return ___kSeparators_19; }
	inline CharU5BU5D_t3528271667** get_address_of_kSeparators_19() { return &___kSeparators_19; }
	inline void set_kSeparators_19(CharU5BU5D_t3528271667* value)
	{
		___kSeparators_19 = value;
		Il2CppCodeGenWriteBarrier((&___kSeparators_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTFIELD_T3762917431_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_22)); }
	inline Material_t340375123 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_t340375123 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_23)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_29)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef IMAGE_T2670269651_H
#define IMAGE_T2670269651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2670269651  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t280657092 * ___m_Sprite_31;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t280657092 * ___m_OverrideSprite_32;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_33;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_34;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_35;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_36;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_37;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_38;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_39;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_40;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_41;

public:
	inline static int32_t get_offset_of_m_Sprite_31() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Sprite_31)); }
	inline Sprite_t280657092 * get_m_Sprite_31() const { return ___m_Sprite_31; }
	inline Sprite_t280657092 ** get_address_of_m_Sprite_31() { return &___m_Sprite_31; }
	inline void set_m_Sprite_31(Sprite_t280657092 * value)
	{
		___m_Sprite_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_31), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_32() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_OverrideSprite_32)); }
	inline Sprite_t280657092 * get_m_OverrideSprite_32() const { return ___m_OverrideSprite_32; }
	inline Sprite_t280657092 ** get_address_of_m_OverrideSprite_32() { return &___m_OverrideSprite_32; }
	inline void set_m_OverrideSprite_32(Sprite_t280657092 * value)
	{
		___m_OverrideSprite_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_32), value);
	}

	inline static int32_t get_offset_of_m_Type_33() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Type_33)); }
	inline int32_t get_m_Type_33() const { return ___m_Type_33; }
	inline int32_t* get_address_of_m_Type_33() { return &___m_Type_33; }
	inline void set_m_Type_33(int32_t value)
	{
		___m_Type_33 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_34() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_PreserveAspect_34)); }
	inline bool get_m_PreserveAspect_34() const { return ___m_PreserveAspect_34; }
	inline bool* get_address_of_m_PreserveAspect_34() { return &___m_PreserveAspect_34; }
	inline void set_m_PreserveAspect_34(bool value)
	{
		___m_PreserveAspect_34 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_35() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillCenter_35)); }
	inline bool get_m_FillCenter_35() const { return ___m_FillCenter_35; }
	inline bool* get_address_of_m_FillCenter_35() { return &___m_FillCenter_35; }
	inline void set_m_FillCenter_35(bool value)
	{
		___m_FillCenter_35 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_36() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillMethod_36)); }
	inline int32_t get_m_FillMethod_36() const { return ___m_FillMethod_36; }
	inline int32_t* get_address_of_m_FillMethod_36() { return &___m_FillMethod_36; }
	inline void set_m_FillMethod_36(int32_t value)
	{
		___m_FillMethod_36 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_37() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillAmount_37)); }
	inline float get_m_FillAmount_37() const { return ___m_FillAmount_37; }
	inline float* get_address_of_m_FillAmount_37() { return &___m_FillAmount_37; }
	inline void set_m_FillAmount_37(float value)
	{
		___m_FillAmount_37 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_38() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillClockwise_38)); }
	inline bool get_m_FillClockwise_38() const { return ___m_FillClockwise_38; }
	inline bool* get_address_of_m_FillClockwise_38() { return &___m_FillClockwise_38; }
	inline void set_m_FillClockwise_38(bool value)
	{
		___m_FillClockwise_38 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_39() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillOrigin_39)); }
	inline int32_t get_m_FillOrigin_39() const { return ___m_FillOrigin_39; }
	inline int32_t* get_address_of_m_FillOrigin_39() { return &___m_FillOrigin_39; }
	inline void set_m_FillOrigin_39(int32_t value)
	{
		___m_FillOrigin_39 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_40() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_AlphaHitTestMinimumThreshold_40)); }
	inline float get_m_AlphaHitTestMinimumThreshold_40() const { return ___m_AlphaHitTestMinimumThreshold_40; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_40() { return &___m_AlphaHitTestMinimumThreshold_40; }
	inline void set_m_AlphaHitTestMinimumThreshold_40(float value)
	{
		___m_AlphaHitTestMinimumThreshold_40 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_41() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Tracked_41)); }
	inline bool get_m_Tracked_41() const { return ___m_Tracked_41; }
	inline bool* get_address_of_m_Tracked_41() { return &___m_Tracked_41; }
	inline void set_m_Tracked_41(bool value)
	{
		___m_Tracked_41 = value;
	}
};

struct Image_t2670269651_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t340375123 * ___s_ETC1DefaultUI_30;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t1457185986* ___s_VertScratch_42;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t1457185986* ___s_UVScratch_43;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1718750761* ___s_Xy_44;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1718750761* ___s_Uv_45;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t4142344393 * ___m_TrackedTexturelessImages_46;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_47;
	// System.Action`1<UnityEngine.U2D.SpriteAtlas> UnityEngine.UI.Image::<>f__mg$cache0
	Action_1_t819399007 * ___U3CU3Ef__mgU24cache0_48;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_30() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_ETC1DefaultUI_30)); }
	inline Material_t340375123 * get_s_ETC1DefaultUI_30() const { return ___s_ETC1DefaultUI_30; }
	inline Material_t340375123 ** get_address_of_s_ETC1DefaultUI_30() { return &___s_ETC1DefaultUI_30; }
	inline void set_s_ETC1DefaultUI_30(Material_t340375123 * value)
	{
		___s_ETC1DefaultUI_30 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_30), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_42() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_VertScratch_42)); }
	inline Vector2U5BU5D_t1457185986* get_s_VertScratch_42() const { return ___s_VertScratch_42; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_VertScratch_42() { return &___s_VertScratch_42; }
	inline void set_s_VertScratch_42(Vector2U5BU5D_t1457185986* value)
	{
		___s_VertScratch_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_42), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_43() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_UVScratch_43)); }
	inline Vector2U5BU5D_t1457185986* get_s_UVScratch_43() const { return ___s_UVScratch_43; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_UVScratch_43() { return &___s_UVScratch_43; }
	inline void set_s_UVScratch_43(Vector2U5BU5D_t1457185986* value)
	{
		___s_UVScratch_43 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_43), value);
	}

	inline static int32_t get_offset_of_s_Xy_44() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Xy_44)); }
	inline Vector3U5BU5D_t1718750761* get_s_Xy_44() const { return ___s_Xy_44; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Xy_44() { return &___s_Xy_44; }
	inline void set_s_Xy_44(Vector3U5BU5D_t1718750761* value)
	{
		___s_Xy_44 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_44), value);
	}

	inline static int32_t get_offset_of_s_Uv_45() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Uv_45)); }
	inline Vector3U5BU5D_t1718750761* get_s_Uv_45() const { return ___s_Uv_45; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Uv_45() { return &___s_Uv_45; }
	inline void set_s_Uv_45(Vector3U5BU5D_t1718750761* value)
	{
		___s_Uv_45 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_45), value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_46() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___m_TrackedTexturelessImages_46)); }
	inline List_1_t4142344393 * get_m_TrackedTexturelessImages_46() const { return ___m_TrackedTexturelessImages_46; }
	inline List_1_t4142344393 ** get_address_of_m_TrackedTexturelessImages_46() { return &___m_TrackedTexturelessImages_46; }
	inline void set_m_TrackedTexturelessImages_46(List_1_t4142344393 * value)
	{
		___m_TrackedTexturelessImages_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackedTexturelessImages_46), value);
	}

	inline static int32_t get_offset_of_s_Initialized_47() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Initialized_47)); }
	inline bool get_s_Initialized_47() const { return ___s_Initialized_47; }
	inline bool* get_address_of_s_Initialized_47() { return &___s_Initialized_47; }
	inline void set_s_Initialized_47(bool value)
	{
		___s_Initialized_47 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_48() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___U3CU3Ef__mgU24cache0_48)); }
	inline Action_1_t819399007 * get_U3CU3Ef__mgU24cache0_48() const { return ___U3CU3Ef__mgU24cache0_48; }
	inline Action_1_t819399007 ** get_address_of_U3CU3Ef__mgU24cache0_48() { return &___U3CU3Ef__mgU24cache0_48; }
	inline void set_U3CU3Ef__mgU24cache0_48(Action_1_t819399007 * value)
	{
		___U3CU3Ef__mgU24cache0_48 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_48), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2670269651_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_30;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_31;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_32;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_33;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_35;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_36;

public:
	inline static int32_t get_offset_of_m_FontData_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_30)); }
	inline FontData_t746620069 * get_m_FontData_30() const { return ___m_FontData_30; }
	inline FontData_t746620069 ** get_address_of_m_FontData_30() { return &___m_FontData_30; }
	inline void set_m_FontData_30(FontData_t746620069 * value)
	{
		___m_FontData_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_30), value);
	}

	inline static int32_t get_offset_of_m_Text_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_31)); }
	inline String_t* get_m_Text_31() const { return ___m_Text_31; }
	inline String_t** get_address_of_m_Text_31() { return &___m_Text_31; }
	inline void set_m_Text_31(String_t* value)
	{
		___m_Text_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_31), value);
	}

	inline static int32_t get_offset_of_m_TextCache_32() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_32)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_32() const { return ___m_TextCache_32; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_32() { return &___m_TextCache_32; }
	inline void set_m_TextCache_32(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_32), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_33)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_33() const { return ___m_TextCacheForLayout_33; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_33() { return &___m_TextCacheForLayout_33; }
	inline void set_m_TextCacheForLayout_33(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_33), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_35() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_35)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_35() const { return ___m_DisableFontTextureRebuiltCallback_35; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_35() { return &___m_DisableFontTextureRebuiltCallback_35; }
	inline void set_m_DisableFontTextureRebuiltCallback_35(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_35 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_36() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_36)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_36() const { return ___m_TempVerts_36; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_36() { return &___m_TempVerts_36; }
	inline void set_m_TempVerts_36(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_36), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_34;

public:
	inline static int32_t get_offset_of_s_DefaultText_34() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_34)); }
	inline Material_t340375123 * get_s_DefaultText_34() const { return ___s_DefaultText_34; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_34() { return &___s_DefaultText_34; }
	inline void set_s_DefaultText_34(Material_t340375123 * value)
	{
		___s_DefaultText_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// DigitsNFCToolkit.NFCTechnology[]
struct NFCTechnologyU5BU5D_t328493574  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t1703627840  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t1188392813 * m_Items[1];

public:
	inline Delegate_t1188392813 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t1188392813 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t1188392813 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Delegate_t1188392813 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t1188392813 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t1188392813 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// DigitsNFCToolkit.TextRecord/TextEncoding[]
struct TextEncodingU5BU5D_t2855432831  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// DigitsNFCToolkit.Samples.WriteScreenControl/IconID[]
struct IconIDU5BU5D_t2629431601  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_t2146457487  List_1_GetEnumerator_m816315209_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m337713592_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_m1328026504_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_set_Item_m550276520_gshared (List_1_t257213610 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" IL2CPP_METHOD_ATTR void List_1_Clear_m3697625829_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void List_1_RemoveAt_m2730968292_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m518943619_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * KeyValuePair_2_get_Key_m4184817181_gshared (KeyValuePair_2_t2530217319 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * KeyValuePair_2_get_Value_m1132502692_gshared (KeyValuePair_2_t2530217319 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Stack_1__ctor_m3164958980_gshared (Stack_1_t3923495619 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<System.Object>::Pop()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Stack_1_Pop_m756553478_gshared (Stack_1_t3923495619 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(!0)
extern "C" IL2CPP_METHOD_ATTR void Stack_1_Push_m1669856732_gshared (Stack_1_t3923495619 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_m1542987838_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m2446893047_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponentInChildren_TisRuntimeObject_m1033527003_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<DigitsNFCToolkit.TextRecord/TextEncoding>(System.Collections.IEnumerable)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Cast_TisTextEncoding_t3210513626_m2305854455_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<DigitsNFCToolkit.TextRecord/TextEncoding>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" IL2CPP_METHOD_ATTR TextEncodingU5BU5D_t2855432831* Enumerable_ToArray_TisTextEncoding_t3210513626_m1218064777_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<DigitsNFCToolkit.Samples.WriteScreenControl/IconID>(System.Collections.IEnumerable)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Cast_TisIconID_t582738576_m3494484091_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<DigitsNFCToolkit.Samples.WriteScreenControl/IconID>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" IL2CPP_METHOD_ATTR IconIDU5BU5D_t2629431601* Enumerable_ToArray_TisIconID_t582738576_m2108801283_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);

// System.Void DigitsNFCToolkit.NDEFRecord::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NDEFRecord__ctor_m2075191861 (NDEFRecord_t2690545556 * __this, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NDEFRecord::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFRecord_ParseJSON_m1625700843 (NDEFRecord_t2690545556 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.JSON.JSONObject::TryGetString(System.String,System.String&)
extern "C" IL2CPP_METHOD_ATTR bool JSONObject_TryGetString_m766921647 (JSONObject_t321714843 * __this, String_t* ___key0, String_t** ___stringValue1, const RuntimeMethod* method);
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.NDEFRecord::ToJSON()
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * NDEFRecord_ToJSON_m164142245 (NDEFRecord_t2690545556 * __this, const RuntimeMethod* method);
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONValue::op_Implicit(System.String)
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONValue_op_Implicit_m357672470 (RuntimeObject * __this /* static, unused */, String_t* ___str0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONObject::Add(System.String,DigitsNFCToolkit.JSON.JSONValue)
extern "C" IL2CPP_METHOD_ATTR void JSONObject_Add_m2412535806 (JSONObject_t321714843 * __this, String_t* ___key0, JSONValue_t4275860644 * ___value1, const RuntimeMethod* method);
// System.Byte[] DigitsNFCToolkit.Util::DecodeBase64UrlSafe(System.String)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_t4116647657* Util_DecodeBase64UrlSafe_m4209975833 (RuntimeObject * __this /* static, unused */, String_t* ___base64String0, const RuntimeMethod* method);
// System.String DigitsNFCToolkit.Util::EncodeBase64UrlSafe(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR String_t* Util_EncodeBase64UrlSafe_m2760074679 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___bytes0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NativeNFC__ctor_m920500560 (NativeNFC_t1941597496 * __this, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.NativeNFC::get_ResetOnTimeout()
extern "C" IL2CPP_METHOD_ATTR bool NativeNFC_get_ResetOnTimeout_m1804879484 (NativeNFC_t1941597496 * __this, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::set_ResetOnTimeout(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_set_ResetOnTimeout_m1401711570 (NativeNFC_t1941597496 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.IOSNFC::_nativeNFC_setResetOnTimeout(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void IOSNFC__nativeNFC_setResetOnTimeout_m2191450753 (RuntimeObject * __this /* static, unused */, bool ___resetOnTimeout0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
extern "C" IL2CPP_METHOD_ATTR String_t* Object_get_name_m4211327027 (Object_t631007953 * __this, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.IOSNFC::_nativeNFC_initialize(System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSNFC__nativeNFC_initialize_m1907124886 (RuntimeObject * __this /* static, unused */, String_t* ___gameObjectName0, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.IOSNFC::_nativeNFC_isNFCTagInfoReadSupported()
extern "C" IL2CPP_METHOD_ATTR bool IOSNFC__nativeNFC_isNFCTagInfoReadSupported_m305629107 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.IOSNFC::_nativeNFC_isNDEFReadSupported()
extern "C" IL2CPP_METHOD_ATTR bool IOSNFC__nativeNFC_isNDEFReadSupported_m1124741525 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.IOSNFC::_nativeNFC_isNDEFWriteSupported()
extern "C" IL2CPP_METHOD_ATTR bool IOSNFC__nativeNFC_isNDEFWriteSupported_m1714142755 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.IOSNFC::_nativeNFC_isNFCEnabled()
extern "C" IL2CPP_METHOD_ATTR bool IOSNFC__nativeNFC_isNFCEnabled_m3040644964 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.IOSNFC::_nativeNFC_enable()
extern "C" IL2CPP_METHOD_ATTR void IOSNFC__nativeNFC_enable_m1112423440 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.IOSNFC::_nativeNFC_disable()
extern "C" IL2CPP_METHOD_ATTR void IOSNFC__nativeNFC_disable_m24928640 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.IOSNFC::_nativeNFC_requestNDEFWrite(System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSNFC__nativeNFC_requestNDEFWrite_m2029809237 (RuntimeObject * __this /* static, unused */, String_t* ___messageJSON0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.IOSNFC::_nativeNFC_cancelNDEFWriteRequest()
extern "C" IL2CPP_METHOD_ATTR void IOSNFC__nativeNFC_cancelNDEFWriteRequest_m3528688979 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<DigitsNFCToolkit.JSON.JSONValue>::.ctor()
inline void List_1__ctor_m452167397 (List_1_t1452968090 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1452968090 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<DigitsNFCToolkit.JSON.JSONValue>::GetEnumerator()
inline Enumerator_t3342211967  List_1_GetEnumerator_m3485391654 (List_1_t1452968090 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t3342211967  (*) (List_1_t1452968090 *, const RuntimeMethod*))List_1_GetEnumerator_m816315209_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<DigitsNFCToolkit.JSON.JSONValue>::get_Current()
inline JSONValue_t4275860644 * Enumerator_get_Current_m976429252 (Enumerator_t3342211967 * __this, const RuntimeMethod* method)
{
	return ((  JSONValue_t4275860644 * (*) (Enumerator_t3342211967 *, const RuntimeMethod*))Enumerator_get_Current_m337713592_gshared)(__this, method);
}
// System.Void DigitsNFCToolkit.JSON.JSONValue::.ctor(DigitsNFCToolkit.JSON.JSONValue)
extern "C" IL2CPP_METHOD_ATTR void JSONValue__ctor_m1222737883 (JSONValue_t4275860644 * __this, JSONValue_t4275860644 * ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<DigitsNFCToolkit.JSON.JSONValue>::Add(!0)
inline void List_1_Add_m119444007 (List_1_t1452968090 * __this, JSONValue_t4275860644 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1452968090 *, JSONValue_t4275860644 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<DigitsNFCToolkit.JSON.JSONValue>::MoveNext()
inline bool Enumerator_MoveNext_m4065387915 (Enumerator_t3342211967 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t3342211967 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<DigitsNFCToolkit.JSON.JSONValue>::Dispose()
inline void Enumerator_Dispose_m3644548256 (Enumerator_t3342211967 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t3342211967 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1<DigitsNFCToolkit.JSON.JSONValue>::get_Item(System.Int32)
inline JSONValue_t4275860644 * List_1_get_Item_m740911558 (List_1_t1452968090 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  JSONValue_t4275860644 * (*) (List_1_t1452968090 *, int32_t, const RuntimeMethod*))List_1_get_Item_m1328026504_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<DigitsNFCToolkit.JSON.JSONValue>::set_Item(System.Int32,!0)
inline void List_1_set_Item_m1423443710 (List_1_t1452968090 * __this, int32_t p0, JSONValue_t4275860644 * p1, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1452968090 *, int32_t, JSONValue_t4275860644 *, const RuntimeMethod*))List_1_set_Item_m550276520_gshared)(__this, p0, p1, method);
}
// System.Int32 System.Collections.Generic.List`1<DigitsNFCToolkit.JSON.JSONValue>::get_Count()
inline int32_t List_1_get_Count_m3731320739 (List_1_t1452968090 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t1452968090 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method);
}
// System.Void System.Text.StringBuilder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void StringBuilder__ctor_m3121283359 (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char)
extern "C" IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_m2383614642 (StringBuilder_t * __this, Il2CppChar p0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C" IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_m1965104174 (StringBuilder_t * __this, String_t* p0, const RuntimeMethod* method);
// System.Int32 System.Text.StringBuilder::get_Length()
extern "C" IL2CPP_METHOD_ATTR int32_t StringBuilder_get_Length_m3238060835 (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Remove(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Remove_m940064945 (StringBuilder_t * __this, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m1715369213 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.JSON.JSONObject::Parse(System.String)
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * JSONObject_Parse_m2486419426 (RuntimeObject * __this /* static, unused */, String_t* ___jsonString0, const RuntimeMethod* method);
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONObject::GetValue(System.String)
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONObject_GetValue_m193839943 (JSONObject_t321714843 * __this, String_t* ___key0, const RuntimeMethod* method);
// DigitsNFCToolkit.JSON.JSONArray DigitsNFCToolkit.JSON.JSONValue::get_Array()
extern "C" IL2CPP_METHOD_ATTR JSONArray_t4024675823 * JSONValue_get_Array_m3632598190 (JSONValue_t4275860644 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<DigitsNFCToolkit.JSON.JSONValue>::Clear()
inline void List_1_Clear_m613793141 (List_1_t1452968090 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1452968090 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<DigitsNFCToolkit.JSON.JSONValue>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_m2053294765 (List_1_t1452968090 * __this, int32_t p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1452968090 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m2730968292_gshared)(__this, p0, method);
}
// System.String System.String::Concat(System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m2971454694 (RuntimeObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogError_m2850623458 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONArray::.ctor(DigitsNFCToolkit.JSON.JSONArray)
extern "C" IL2CPP_METHOD_ATTR void JSONArray__ctor_m4099340829 (JSONArray_t4024675823 * __this, JSONArray_t4024675823 * ___array0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONArray::Add(DigitsNFCToolkit.JSON.JSONValue)
extern "C" IL2CPP_METHOD_ATTR void JSONArray_Add_m1346855851 (JSONArray_t4024675823 * __this, JSONValue_t4275860644 * ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::.ctor()
inline void Dictionary_2__ctor_m691717352 (Dictionary_2_t4061116943 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t4061116943 *, const RuntimeMethod*))Dictionary_2__ctor_m518943619_gshared)(__this, method);
}
// !0 System.Collections.Generic.KeyValuePair`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::get_Key()
inline String_t* KeyValuePair_2_get_Key_m1640885509 (KeyValuePair_2_t2163821814 * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (KeyValuePair_2_t2163821814 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m4184817181_gshared)(__this, method);
}
// !1 System.Collections.Generic.KeyValuePair`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::get_Value()
inline JSONValue_t4275860644 * KeyValuePair_2_get_Value_m1594442022 (KeyValuePair_2_t2163821814 * __this, const RuntimeMethod* method)
{
	return ((  JSONValue_t4275860644 * (*) (KeyValuePair_2_t2163821814 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1132502692_gshared)(__this, method);
}
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.String DigitsNFCToolkit.JSON.JSONValue::get_String()
extern "C" IL2CPP_METHOD_ATTR String_t* JSONValue_get_String_m3887450814 (JSONValue_t4275860644 * __this, const RuntimeMethod* method);
// System.Double DigitsNFCToolkit.JSON.JSONValue::get_Double()
extern "C" IL2CPP_METHOD_ATTR double JSONValue_get_Double_m3385163784 (JSONValue_t4275860644 * __this, const RuntimeMethod* method);
// System.Single DigitsNFCToolkit.JSON.JSONValue::get_Float()
extern "C" IL2CPP_METHOD_ATTR float JSONValue_get_Float_m2758114374 (JSONValue_t4275860644 * __this, const RuntimeMethod* method);
// System.Int32 DigitsNFCToolkit.JSON.JSONValue::get_Integer()
extern "C" IL2CPP_METHOD_ATTR int32_t JSONValue_get_Integer_m3463426723 (JSONValue_t4275860644 * __this, const RuntimeMethod* method);
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.JSON.JSONValue::get_Object()
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * JSONValue_get_Object_m3828143293 (JSONValue_t4275860644 * __this, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.JSON.JSONValue::get_Boolean()
extern "C" IL2CPP_METHOD_ATTR bool JSONValue_get_Boolean_m2734736792 (JSONValue_t4275860644 * __this, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m2969720369 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.String>::.ctor()
inline void Stack_1__ctor_m2770587905 (Stack_1_t2690840144 * __this, const RuntimeMethod* method)
{
	((  void (*) (Stack_1_t2690840144 *, const RuntimeMethod*))Stack_1__ctor_m3164958980_gshared)(__this, method);
}
// System.Int32 DigitsNFCToolkit.JSON.JSONObject::SkipWhitespace(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t JSONObject_SkipWhitespace_m1442147541 (RuntimeObject * __this /* static, unused */, String_t* ___str0, int32_t ___pos1, const RuntimeMethod* method);
// System.Char System.String::get_Chars(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_m2986988803 (String_t* __this, int32_t p0, const RuntimeMethod* method);
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.JSON.JSONObject::Fail(System.Char,System.Int32)
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * JSONObject_Fail_m1137956452 (RuntimeObject * __this /* static, unused */, Il2CppChar ___expected0, int32_t ___position1, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONObject::.ctor()
extern "C" IL2CPP_METHOD_ATTR void JSONObject__ctor_m3436253305 (JSONObject_t321714843 * __this, const RuntimeMethod* method);
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONValue::op_Implicit(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONValue_op_Implicit_m1136109141 (RuntimeObject * __this /* static, unused */, JSONObject_t321714843 * ___obj0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONValue::set_Parent(DigitsNFCToolkit.JSON.JSONValue)
extern "C" IL2CPP_METHOD_ATTR void JSONValue_set_Parent_m1935092005 (JSONValue_t4275860644 * __this, JSONValue_t4275860644 * ___value0, const RuntimeMethod* method);
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONValue::get_Parent()
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONValue_get_Parent_m2676443687 (JSONValue_t4275860644 * __this, const RuntimeMethod* method);
// DigitsNFCToolkit.JSON.JSONValueType DigitsNFCToolkit.JSON.JSONValue::get_Type()
extern "C" IL2CPP_METHOD_ATTR int32_t JSONValue_get_Type_m3765076585 (JSONValue_t4275860644 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<System.String>::Pop()
inline String_t* Stack_1_Pop_m513619580 (Stack_1_t2690840144 * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Stack_1_t2690840144 *, const RuntimeMethod*))Stack_1_Pop_m756553478_gshared)(__this, method);
}
// System.Void DigitsNFCToolkit.JSON.JSONValue::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void JSONValue__ctor_m3984902601 (JSONValue_t4275860644 * __this, JSONObject_t321714843 * ___obj0, const RuntimeMethod* method);
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.JSON.JSONObject::Fail(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * JSONObject_Fail_m3595512628 (RuntimeObject * __this /* static, unused */, String_t* ___expected0, int32_t ___position1, const RuntimeMethod* method);
// System.String DigitsNFCToolkit.JSON.JSONObject::ParseString(System.String,System.Int32&)
extern "C" IL2CPP_METHOD_ATTR String_t* JSONObject_ParseString_m1617168844 (RuntimeObject * __this /* static, unused */, String_t* ___str0, int32_t* ___startPosition1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.String>::Push(!0)
inline void Stack_1_Push_m43534404 (Stack_1_t2690840144 * __this, String_t* p0, const RuntimeMethod* method)
{
	((  void (*) (Stack_1_t2690840144 *, String_t*, const RuntimeMethod*))Stack_1_Push_m1669856732_gshared)(__this, p0, method);
}
// System.Boolean System.Char::IsDigit(System.Char)
extern "C" IL2CPP_METHOD_ATTR bool Char_IsDigit_m3646673943 (RuntimeObject * __this /* static, unused */, Il2CppChar p0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONValue::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void JSONValue__ctor_m2817256385 (JSONValue_t4275860644 * __this, String_t* ___str0, const RuntimeMethod* method);
// System.Double DigitsNFCToolkit.JSON.JSONObject::ParseNumber(System.String,System.Int32&)
extern "C" IL2CPP_METHOD_ATTR double JSONObject_ParseNumber_m2501283307 (RuntimeObject * __this /* static, unused */, String_t* ___str0, int32_t* ___startPosition1, const RuntimeMethod* method);
// System.Boolean System.Double::IsNaN(System.Double)
extern "C" IL2CPP_METHOD_ATTR bool Double_IsNaN_m649024406 (RuntimeObject * __this /* static, unused */, double p0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONValue::.ctor(System.Double)
extern "C" IL2CPP_METHOD_ATTR void JSONValue__ctor_m610419965 (JSONValue_t4275860644 * __this, double ___number0, const RuntimeMethod* method);
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONValue::op_Implicit(System.Double)
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONValue_op_Implicit_m3141527758 (RuntimeObject * __this /* static, unused */, double ___number0, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
extern "C" IL2CPP_METHOD_ATTR int32_t String_get_Length_m3847582255 (String_t* __this, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONValue::.ctor(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void JSONValue__ctor_m31333835 (JSONValue_t4275860644 * __this, bool ___boolean0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONArray::.ctor()
extern "C" IL2CPP_METHOD_ATTR void JSONArray__ctor_m3612236623 (JSONArray_t4024675823 * __this, const RuntimeMethod* method);
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONValue::op_Implicit(DigitsNFCToolkit.JSON.JSONArray)
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONValue_op_Implicit_m216610386 (RuntimeObject * __this /* static, unused */, JSONArray_t4024675823 * ___array0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONValue::.ctor(DigitsNFCToolkit.JSON.JSONArray)
extern "C" IL2CPP_METHOD_ATTR void JSONValue__ctor_m3750205962 (JSONValue_t4275860644 * __this, JSONArray_t4024675823 * ___array0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONValue::.ctor(DigitsNFCToolkit.JSON.JSONValueType)
extern "C" IL2CPP_METHOD_ATTR void JSONValue__ctor_m34194388 (JSONValue_t4275860644 * __this, int32_t ___type0, const RuntimeMethod* method);
// System.Boolean System.Char::IsWhiteSpace(System.Char)
extern "C" IL2CPP_METHOD_ATTR bool Char_IsWhiteSpace_m2148390798 (RuntimeObject * __this /* static, unused */, Il2CppChar p0, const RuntimeMethod* method);
// System.Int32 System.String::IndexOf(System.Char,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t String_IndexOf_m2466398549 (String_t* __this, Il2CppChar p0, int32_t p1, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Substring_m1610150815 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Regex::Match(System.String)
extern "C" IL2CPP_METHOD_ATTR Match_t3408321083 * Regex_Match_m2255756165 (Regex_t3657309853 * __this, String_t* p0, const RuntimeMethod* method);
// System.Boolean System.Text.RegularExpressions.Group::get_Success()
extern "C" IL2CPP_METHOD_ATTR bool Group_get_Success_m3823591889 (Group_t2468205786 * __this, const RuntimeMethod* method);
// System.Text.RegularExpressions.Group System.Text.RegularExpressions.GroupCollection::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Group_t2468205786 * GroupCollection_get_Item_m723682197 (GroupCollection_t69770484 * __this, int32_t p0, const RuntimeMethod* method);
// System.Text.RegularExpressions.CaptureCollection System.Text.RegularExpressions.Group::get_Captures()
extern "C" IL2CPP_METHOD_ATTR CaptureCollection_t1760593541 * Group_get_Captures_m1655762411 (Group_t2468205786 * __this, const RuntimeMethod* method);
// System.Text.RegularExpressions.Capture System.Text.RegularExpressions.CaptureCollection::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Capture_t2232016050 * CaptureCollection_get_Item_m3183527268 (CaptureCollection_t1760593541 * __this, int32_t p0, const RuntimeMethod* method);
// System.String System.Text.RegularExpressions.Capture::get_Value()
extern "C" IL2CPP_METHOD_ATTR String_t* Capture_get_Value_m3919646039 (Capture_t2232016050 * __this, const RuntimeMethod* method);
// System.Byte System.Byte::Parse(System.String,System.Globalization.NumberStyles)
extern "C" IL2CPP_METHOD_ATTR uint8_t Byte_Parse_m534370573 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.Text.Encoding System.Text.Encoding::get_Unicode()
extern "C" IL2CPP_METHOD_ATTR Encoding_t1523322056 * Encoding_get_Unicode_m811213576 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String System.String::Replace(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Replace_m1273907647 (String_t* __this, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.String DigitsNFCToolkit.JSON.JSONObject::SanitizeJSONString(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* JSONObject_SanitizeJSONString_m479242491 (RuntimeObject * __this /* static, unused */, String_t* ___jsonStringValue0, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void StringBuilder__ctor_m2989139009 (StringBuilder_t * __this, String_t* p0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Replace(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Replace_m1968561789 (StringBuilder_t * __this, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
extern "C" IL2CPP_METHOD_ATTR CultureInfo_t4157843068 * CultureInfo_get_InvariantCulture_m3532445182 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean System.Double::TryParse(System.String,System.Globalization.NumberStyles,System.IFormatProvider,System.Double&)
extern "C" IL2CPP_METHOD_ATTR bool Double_TryParse_m623190659 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, RuntimeObject* p2, double* p3, const RuntimeMethod* method);
// System.String System.String::CreateString(System.Char,System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* String_CreateString_m1262864254 (String_t* __this, Il2CppChar ___c0, int32_t ___count1, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m3755062657 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method);
// System.Void System.Text.RegularExpressions.Regex::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void Regex__ctor_m3948448025 (Regex_t3657309853 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONValue::set_Type(DigitsNFCToolkit.JSON.JSONValueType)
extern "C" IL2CPP_METHOD_ATTR void JSONValue_set_Type_m40491884 (JSONValue_t4275860644 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONValue::set_String(System.String)
extern "C" IL2CPP_METHOD_ATTR void JSONValue_set_String_m3777049347 (JSONValue_t4275860644 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONValue::set_Double(System.Double)
extern "C" IL2CPP_METHOD_ATTR void JSONValue_set_Double_m2090912025 (JSONValue_t4275860644 * __this, double ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONValue::set_Object(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void JSONValue_set_Object_m1350223645 (JSONValue_t4275860644 * __this, JSONObject_t321714843 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONValue::set_Array(DigitsNFCToolkit.JSON.JSONArray)
extern "C" IL2CPP_METHOD_ATTR void JSONValue_set_Array_m3526479683 (JSONValue_t4275860644 * __this, JSONArray_t4024675823 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONValue::set_Boolean(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void JSONValue_set_Boolean_m4041559619 (JSONValue_t4275860644 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.JSON.JSONObject::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void JSONObject__ctor_m2883999366 (JSONObject_t321714843 * __this, JSONObject_t321714843 * ___other0, const RuntimeMethod* method);
// System.String System.Double::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* Double_ToString_m1229922074 (double* __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<DigitsNFCToolkit.NDEFRecord>::.ctor()
inline void List_1__ctor_m1332560254 (List_1_t4162620298 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4162620298 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void DigitsNFCToolkit.NDEFMessage::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFMessage_ParseJSON_m112885689 (NDEFMessage_t279637043 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// DigitsNFCToolkit.NDEFMessageWriteState DigitsNFCToolkit.NDEFMessage::get_WriteState()
extern "C" IL2CPP_METHOD_ATTR int32_t NDEFMessage_get_WriteState_m1477143167 (NDEFMessage_t279637043 * __this, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.JSON.JSONObject::TryGetArray(System.String,DigitsNFCToolkit.JSON.JSONArray&)
extern "C" IL2CPP_METHOD_ATTR bool JSONObject_TryGetArray_m3928523712 (JSONObject_t321714843 * __this, String_t* ___key0, JSONArray_t4024675823 ** ___arrayValue1, const RuntimeMethod* method);
// System.Int32 DigitsNFCToolkit.JSON.JSONArray::get_Length()
extern "C" IL2CPP_METHOD_ATTR int32_t JSONArray_get_Length_m1921089944 (JSONArray_t4024675823 * __this, const RuntimeMethod* method);
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONArray::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONArray_get_Item_m1052458067 (JSONArray_t4024675823 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.JSON.JSONObject::TryGetInt(System.String,System.Int32&)
extern "C" IL2CPP_METHOD_ATTR bool JSONObject_TryGetInt_m2083265671 (JSONObject_t321714843 * __this, String_t* ___key0, int32_t* ___intValue1, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.AbsoluteUriRecord::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void AbsoluteUriRecord__ctor_m157112738 (AbsoluteUriRecord_t2525168784 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.EmptyRecord::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void EmptyRecord__ctor_m3438728046 (EmptyRecord_t1486430273 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.ExternalTypeRecord::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void ExternalTypeRecord__ctor_m3317940198 (ExternalTypeRecord_t4087466745 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.MimeMediaRecord::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void MimeMediaRecord__ctor_m1733123593 (MimeMediaRecord_t736820488 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.SmartPosterRecord::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void SmartPosterRecord__ctor_m3134893622 (SmartPosterRecord_t1640848801 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.TextRecord::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void TextRecord__ctor_m2059777621 (TextRecord_t2313697623 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.UnknownRecord::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void UnknownRecord__ctor_m1040786691 (UnknownRecord_t3228240714 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.UriRecord::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void UriRecord__ctor_m12655916 (UriRecord_t2230063309 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<DigitsNFCToolkit.NDEFRecord>::Add(!0)
inline void List_1_Add_m350525082 (List_1_t4162620298 * __this, NDEFRecord_t2690545556 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4162620298 *, NDEFRecord_t2690545556 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Int32 System.Collections.Generic.List`1<DigitsNFCToolkit.NDEFRecord>::get_Count()
inline int32_t List_1_get_Count_m2774962351 (List_1_t4162620298 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t4162620298 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1<DigitsNFCToolkit.NDEFRecord>::get_Item(System.Int32)
inline NDEFRecord_t2690545556 * List_1_get_Item_m1387264933 (List_1_t4162620298 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  NDEFRecord_t2690545556 * (*) (List_1_t4162620298 *, int32_t, const RuntimeMethod*))List_1_get_Item_m1328026504_gshared)(__this, p0, method);
}
// System.Void DigitsNFCToolkit.NDEFPushResult::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFPushResult_ParseJSON_m806318507 (NDEFPushResult_t3422827153 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.JSON.JSONObject::TryGetBoolean(System.String,System.Boolean&)
extern "C" IL2CPP_METHOD_ATTR bool JSONObject_TryGetBoolean_m3739980323 (JSONObject_t321714843 * __this, String_t* ___key0, bool* ___boolValue1, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.JSON.JSONObject::TryGetObject(System.String,DigitsNFCToolkit.JSON.JSONObject&)
extern "C" IL2CPP_METHOD_ATTR bool JSONObject_TryGetObject_m2604600723 (JSONObject_t321714843 * __this, String_t* ___key0, JSONObject_t321714843 ** ___objectValue1, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NDEFMessage::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFMessage__ctor_m1423095509 (NDEFMessage_t279637043 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NDEFMessage::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NDEFMessage__ctor_m2200006917 (NDEFMessage_t279637043 * __this, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NDEFReadResult::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFReadResult_ParseJSON_m4234342281 (NDEFReadResult_t3483243621 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NDEFWriteResult::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFWriteResult_ParseJSON_m4251555459 (NDEFWriteResult_t4210562629 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NFCTag::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NFCTag_ParseJSON_m4194543403 (NFCTag_t2820711232 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONValue::op_Implicit(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONValue_op_Implicit_m2321309250 (RuntimeObject * __this /* static, unused */, bool ___boolean0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C" IL2CPP_METHOD_ATTR Delegate_t1188392813 * Delegate_Combine_m1859655160 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C" IL2CPP_METHOD_ATTR Delegate_t1188392813 * Delegate_Remove_m334097152 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::add_onNFCTagDetected(DigitsNFCToolkit.OnNFCTagDetected)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_add_onNFCTagDetected_m1360338169 (NativeNFC_t1941597496 * __this, OnNFCTagDetected_t3189675727 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::remove_onNFCTagDetected(DigitsNFCToolkit.OnNFCTagDetected)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_remove_onNFCTagDetected_m4260315192 (NativeNFC_t1941597496 * __this, OnNFCTagDetected_t3189675727 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::add_onNDEFReadFinished(DigitsNFCToolkit.OnNDEFReadFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_add_onNDEFReadFinished_m1875928045 (NativeNFC_t1941597496 * __this, OnNDEFReadFinished_t1327886840 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::remove_onNDEFReadFinished(DigitsNFCToolkit.OnNDEFReadFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_remove_onNDEFReadFinished_m3344582477 (NativeNFC_t1941597496 * __this, OnNDEFReadFinished_t1327886840 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::add_onNDEFWriteFinished(DigitsNFCToolkit.OnNDEFWriteFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_add_onNDEFWriteFinished_m3934953293 (NativeNFC_t1941597496 * __this, OnNDEFWriteFinished_t4102039599 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::remove_onNDEFWriteFinished(DigitsNFCToolkit.OnNDEFWriteFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_remove_onNDEFWriteFinished_m1549713225 (NativeNFC_t1941597496 * __this, OnNDEFWriteFinished_t4102039599 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::add_onNDEFPushFinished(DigitsNFCToolkit.OnNDEFPushFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_add_onNDEFPushFinished_m4177872223 (NativeNFC_t1941597496 * __this, OnNDEFPushFinished_t4279917764 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::remove_onNDEFPushFinished(DigitsNFCToolkit.OnNDEFPushFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_remove_onNDEFPushFinished_m1864805945 (NativeNFC_t1941597496 * __this, OnNDEFPushFinished_t4279917764 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NFCTag::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NFCTag__ctor_m976544915 (NFCTag_t2820711232 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.OnNFCTagDetected::Invoke(DigitsNFCToolkit.NFCTag)
extern "C" IL2CPP_METHOD_ATTR void OnNFCTagDetected_Invoke_m4012074991 (OnNFCTagDetected_t3189675727 * __this, NFCTag_t2820711232 * ___tag0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NDEFReadResult::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFReadResult__ctor_m440765780 (NDEFReadResult_t3483243621 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.OnNDEFReadFinished::Invoke(DigitsNFCToolkit.NDEFReadResult)
extern "C" IL2CPP_METHOD_ATTR void OnNDEFReadFinished_Invoke_m990719778 (OnNDEFReadFinished_t1327886840 * __this, NDEFReadResult_t3483243621 * ___result0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NDEFWriteResult::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFWriteResult__ctor_m404001616 (NDEFWriteResult_t4210562629 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.OnNDEFWriteFinished::Invoke(DigitsNFCToolkit.NDEFWriteResult)
extern "C" IL2CPP_METHOD_ATTR void OnNDEFWriteFinished_Invoke_m3001009917 (OnNDEFWriteFinished_t4102039599 * __this, NDEFWriteResult_t4210562629 * ___result0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NDEFPushResult::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFPushResult__ctor_m770393463 (NDEFPushResult_t3422827153 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.OnNDEFPushFinished::Invoke(DigitsNFCToolkit.NDEFPushResult)
extern "C" IL2CPP_METHOD_ATTR void OnNDEFPushFinished_Invoke_m1414772192 (OnNDEFPushFinished_t4279917764 * __this, NDEFPushResult_t3422827153 * ___result0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<DigitsNFCToolkit.NativeNFCManager>()
inline NativeNFCManager_t351225459 * Object_FindObjectOfType_TisNativeNFCManager_t351225459_m2303328554 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	return ((  NativeNFCManager_t351225459 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m1542987838_gshared)(__this /* static, unused */, method);
}
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void GameObject__ctor_m2093116449 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<DigitsNFCToolkit.NativeNFCManager>()
inline NativeNFCManager_t351225459 * GameObject_AddComponent_TisNativeNFCManager_t351225459_m2397571266 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  NativeNFCManager_t351225459 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method);
}
// DigitsNFCToolkit.NativeNFCManager DigitsNFCToolkit.NativeNFCManager::get_Instance()
extern "C" IL2CPP_METHOD_ATTR NativeNFCManager_t351225459 * NativeNFCManager_get_Instance_m711561572 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// DigitsNFCToolkit.NativeNFC DigitsNFCToolkit.NativeNFCManager::get_NFC()
extern "C" IL2CPP_METHOD_ATTR NativeNFC_t1941597496 * NativeNFCManager_get_NFC_m290841912 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<DigitsNFCToolkit.IOSNFC>()
inline IOSNFC_t293874181 * GameObject_AddComponent_TisIOSNFC_t293874181_m566898834 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  IOSNFC_t293874181 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogWarning_m3752629331 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.NDEFMessage::ToJSON()
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * NDEFMessage_ToJSON_m895517323 (NDEFMessage_t279637043 * __this, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::add_NFCTagDetected(DigitsNFCToolkit.OnNFCTagDetected)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_add_NFCTagDetected_m2650456630 (NativeNFC_t1941597496 * __this, OnNFCTagDetected_t3189675727 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::remove_NFCTagDetected(DigitsNFCToolkit.OnNFCTagDetected)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_remove_NFCTagDetected_m2280126304 (NativeNFC_t1941597496 * __this, OnNFCTagDetected_t3189675727 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::add_NDEFReadFinished(DigitsNFCToolkit.OnNDEFReadFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_add_NDEFReadFinished_m2374080351 (NativeNFC_t1941597496 * __this, OnNDEFReadFinished_t1327886840 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::remove_NDEFReadFinished(DigitsNFCToolkit.OnNDEFReadFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_remove_NDEFReadFinished_m596925152 (NativeNFC_t1941597496 * __this, OnNDEFReadFinished_t1327886840 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::add_NDEFWriteFinished(DigitsNFCToolkit.OnNDEFWriteFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_add_NDEFWriteFinished_m3503282290 (NativeNFC_t1941597496 * __this, OnNDEFWriteFinished_t4102039599 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::remove_NDEFWriteFinished(DigitsNFCToolkit.OnNDEFWriteFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_remove_NDEFWriteFinished_m4241802593 (NativeNFC_t1941597496 * __this, OnNDEFWriteFinished_t4102039599 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::add_NDEFPushFinished(DigitsNFCToolkit.OnNDEFPushFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_add_NDEFPushFinished_m621482237 (NativeNFC_t1941597496 * __this, OnNDEFPushFinished_t4279917764 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFC::remove_NDEFPushFinished(DigitsNFCToolkit.OnNDEFPushFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_remove_NDEFPushFinished_m199227403 (NativeNFC_t1941597496 * __this, OnNDEFPushFinished_t4279917764 * ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.RecordItem::.ctor()
extern "C" IL2CPP_METHOD_ATTR void RecordItem__ctor_m1983231623 (RecordItem_t1075151419 * __this, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.RecordItem::Awake()
extern "C" IL2CPP_METHOD_ATTR void RecordItem_Awake_m673876687 (RecordItem_t1075151419 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Transform_Find_m1729760951 (Transform_t3600365921 * __this, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
inline Image_t2670269651 * Component_GetComponent_TisImage_t2670269651_m980647750 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  Image_t2670269651 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Texture2D__ctor_m2862217990 (Texture2D_t3840446185 * __this, int32_t p0, int32_t p1, int32_t p2, bool p3, const RuntimeMethod* method);
// System.Boolean UnityEngine.ImageConversion::LoadImage(UnityEngine.Texture2D,System.Byte[])
extern "C" IL2CPP_METHOD_ATTR bool ImageConversion_LoadImage_m2182108104 (RuntimeObject * __this /* static, unused */, Texture2D_t3840446185 * p0, ByteU5BU5D_t4116647657* p1, const RuntimeMethod* method);
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
extern "C" IL2CPP_METHOD_ATTR void Texture_set_filterMode_m3078068058 (Texture_t3661962703 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Rect__ctor_m2614021312 (Rect_t2360479859 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Sprite_t280657092 * Sprite_Create_m803022218 (RuntimeObject * __this /* static, unused */, Texture2D_t3840446185 * p0, Rect_t2360479859  p1, Vector2_t2156229523  p2, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
extern "C" IL2CPP_METHOD_ATTR void Image_set_sprite_m2369174689 (Image_t2670269651 * __this, Sprite_t280657092 * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
inline RectTransform_t3704657025 * Component_GetComponent_TisRectTransform_t3704657025_m3396022872 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  RectTransform_t3704657025 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
inline Text_t1901882714 * Component_GetComponent_TisText_t1901882714_m4196288697 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  Text_t1901882714 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Button>()
inline Button_t4055032469 * Component_GetComponent_TisButton_t4055032469_m1381873113 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  Button_t4055032469 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// System.Void DigitsNFCToolkit.Samples.MessageScreenView::Initialize()
extern "C" IL2CPP_METHOD_ATTR void MessageScreenView_Initialize_m784771305 (MessageScreenView_t146641597 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.NavigationManager::SwitchToReadScreen()
extern "C" IL2CPP_METHOD_ATTR void NavigationManager_SwitchToReadScreen_m2950668844 (NavigationManager_t1939391727 * __this, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.OnNFCTagDetected::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void OnNFCTagDetected__ctor_m625598112 (OnNFCTagDetected_t3189675727 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFCManager::AddNFCTagDetectedListener(DigitsNFCToolkit.OnNFCTagDetected)
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_AddNFCTagDetectedListener_m3624669884 (RuntimeObject * __this /* static, unused */, OnNFCTagDetected_t3189675727 * ___listener0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.OnNDEFReadFinished::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void OnNDEFReadFinished__ctor_m1837666951 (OnNDEFReadFinished_t1327886840 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFCManager::AddNDEFReadFinishedListener(DigitsNFCToolkit.OnNDEFReadFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_AddNDEFReadFinishedListener_m874873674 (RuntimeObject * __this /* static, unused */, OnNDEFReadFinished_t1327886840 * ___listener0, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.NativeNFCManager::IsNFCTagInfoReadSupported()
extern "C" IL2CPP_METHOD_ATTR bool NativeNFCManager_IsNFCTagInfoReadSupported_m3802949991 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.NativeNFCManager::IsNDEFReadSupported()
extern "C" IL2CPP_METHOD_ATTR bool NativeNFCManager_IsNDEFReadSupported_m4240159853 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.NativeNFCManager::IsNDEFWriteSupported()
extern "C" IL2CPP_METHOD_ATTR bool NativeNFCManager_IsNDEFWriteSupported_m1689756067 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.NativeNFCManager::IsNFCEnabled()
extern "C" IL2CPP_METHOD_ATTR bool NativeNFCManager_IsNFCEnabled_m3780324468 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.NativeNFCManager::IsNDEFPushEnabled()
extern "C" IL2CPP_METHOD_ATTR bool NativeNFCManager_IsNDEFPushEnabled_m743806102 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFCManager::set_ResetOnTimeout(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_set_ResetOnTimeout_m718426537 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFCManager::Enable()
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_Enable_m3630587251 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.ReadScreenView::UpdateTagInfo(DigitsNFCToolkit.NFCTag)
extern "C" IL2CPP_METHOD_ATTR void ReadScreenView_UpdateTagInfo_m2808310696 (ReadScreenView_t239900869 * __this, NFCTag_t2820711232 * ___tag0, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.NDEFReadResult::get_Success()
extern "C" IL2CPP_METHOD_ATTR bool NDEFReadResult_get_Success_m1093983849 (NDEFReadResult_t3483243621 * __this, const RuntimeMethod* method);
// System.String DigitsNFCToolkit.NDEFReadResult::get_TagID()
extern "C" IL2CPP_METHOD_ATTR String_t* NDEFReadResult_get_TagID_m4041318970 (NDEFReadResult_t3483243621 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m2844511972 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method);
// DigitsNFCToolkit.NDEFMessage DigitsNFCToolkit.NDEFReadResult::get_Message()
extern "C" IL2CPP_METHOD_ATTR NDEFMessage_t279637043 * NDEFReadResult_get_Message_m152108824 (NDEFReadResult_t3483243621 * __this, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.ReadScreenView::UpdateNDEFMessage(DigitsNFCToolkit.NDEFMessage)
extern "C" IL2CPP_METHOD_ATTR void ReadScreenView_UpdateNDEFMessage_m814396461 (ReadScreenView_t239900869 * __this, NDEFMessage_t279637043 * ___message0, const RuntimeMethod* method);
// DigitsNFCToolkit.NDEFReadError DigitsNFCToolkit.NDEFReadResult::get_Error()
extern "C" IL2CPP_METHOD_ATTR int32_t NDEFReadResult_get_Error_m3746343680 (NDEFReadResult_t3483243621 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m2556382932 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.ScrollRect>()
inline ScrollRect_t4137855814 * Component_GetComponent_TisScrollRect_t4137855814_m3636214290 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  ScrollRect_t4137855814 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// DigitsNFCToolkit.NFCTechnology[] DigitsNFCToolkit.NFCTag::get_Technologies()
extern "C" IL2CPP_METHOD_ATTR NFCTechnologyU5BU5D_t328493574* NFCTag_get_Technologies_m134455690 (NFCTag_t2820711232 * __this, const RuntimeMethod* method);
// System.Int32 DigitsNFCToolkit.NFCTag::get_MaxWriteSize()
extern "C" IL2CPP_METHOD_ATTR int32_t NFCTag_get_MaxWriteSize_m697043576 (NFCTag_t2820711232 * __this, const RuntimeMethod* method);
// System.String DigitsNFCToolkit.NFCTag::get_ID()
extern "C" IL2CPP_METHOD_ATTR String_t* NFCTag_get_ID_m2099905974 (NFCTag_t2820711232 * __this, const RuntimeMethod* method);
// System.String DigitsNFCToolkit.NFCTag::get_Manufacturer()
extern "C" IL2CPP_METHOD_ATTR String_t* NFCTag_get_Manufacturer_m3670434854 (NFCTag_t2820711232 * __this, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.NFCTag::get_Writable()
extern "C" IL2CPP_METHOD_ATTR bool NFCTag_get_Writable_m3150305762 (NFCTag_t2820711232 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m630303134 (RuntimeObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t2843939325* p1, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.ReadScreenView::CleanupRecordItems()
extern "C" IL2CPP_METHOD_ATTR void ReadScreenView_CleanupRecordItems_m1746468235 (ReadScreenView_t239900869 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<DigitsNFCToolkit.NDEFRecord> DigitsNFCToolkit.NDEFMessage::get_Records()
extern "C" IL2CPP_METHOD_ATTR List_1_t4162620298 * NDEFMessage_get_Records_m1930208688 (NDEFMessage_t279637043 * __this, const RuntimeMethod* method);
// DigitsNFCToolkit.NDEFRecordType DigitsNFCToolkit.NDEFRecord::get_Type()
extern "C" IL2CPP_METHOD_ATTR int32_t NDEFRecord_get_Type_m1191149495 (NDEFRecord_t2690545556 * __this, const RuntimeMethod* method);
// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.ReadScreenView::CreateRecordItem(DigitsNFCToolkit.Samples.RecordItem)
extern "C" IL2CPP_METHOD_ATTR RecordItem_t1075151419 * ReadScreenView_CreateRecordItem_m3952030190 (ReadScreenView_t239900869 * __this, RecordItem_t1075151419 * ___prefab0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.RecordItem::UpdateLabel(System.String)
extern "C" IL2CPP_METHOD_ATTR void RecordItem_UpdateLabel_m1853417565 (RecordItem_t1075151419 * __this, String_t* ___text0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.ImageRecordItem::LoadImage(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void ImageRecordItem_LoadImage_m2052998847 (ImageRecordItem_t2104316047 * __this, ByteU5BU5D_t4116647657* ___bytes0, const RuntimeMethod* method);
// System.Text.Encoding System.Text.Encoding::get_UTF8()
extern "C" IL2CPP_METHOD_ATTR Encoding_t1523322056 * Encoding_get_UTF8_m1008486739 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<DigitsNFCToolkit.TextRecord>::get_Count()
inline int32_t List_1_get_Count_m3842818989 (List_1_t3785772365 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t3785772365 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<DigitsNFCToolkit.MimeMediaRecord>::get_Count()
inline int32_t List_1_get_Count_m1758644702 (List_1_t2208895230 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t2208895230 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method);
}
// UnityEngine.RectTransform DigitsNFCToolkit.Samples.RecordItem::get_RectTransform()
extern "C" IL2CPP_METHOD_ATTR RectTransform_t3704657025 * RecordItem_get_RectTransform_m3728143926 (RecordItem_t1075151419 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void RectTransform_set_anchoredPosition_m4126691837 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method);
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C" IL2CPP_METHOD_ATTR Rect_t2360479859  RectTransform_get_rect_m574169965 (RectTransform_t3704657025 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_height()
extern "C" IL2CPP_METHOD_ATTR float Rect_get_height_m1358425599 (Rect_t2360479859 * __this, const RuntimeMethod* method);
// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::get_content()
extern "C" IL2CPP_METHOD_ATTR RectTransform_t3704657025 * ScrollRect_get_content_m2477524320 (ScrollRect_t4137855814 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void RectTransform_set_sizeDelta_m3462269772 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Transform_get_childCount_m3145433196 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Transform_GetChild_m1092972975 (Transform_t3600365921 * __this, int32_t p0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<DigitsNFCToolkit.Samples.RecordItem>(!!0)
inline RecordItem_t1075151419 * Object_Instantiate_TisRecordItem_t1075151419_m880386264 (RuntimeObject * __this /* static, unused */, RecordItem_t1075151419 * p0, const RuntimeMethod* method)
{
	return ((  RecordItem_t1075151419 * (*) (RuntimeObject * /* static, unused */, RecordItem_t1075151419 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m2446893047_gshared)(__this /* static, unused */, p0, method);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  RectTransform_get_sizeDelta_m2183112744 (RectTransform_t3704657025 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void Transform_SetParent_m381167889 (Transform_t3600365921 * __this, Transform_t3600365921 * p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_get_one_m738793577 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector2_op_Implicit_m1860157806 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localScale_m3053443106 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Quaternion_get_identity_m3722672781 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localRotation_m19445462 (Transform_t3600365921 * __this, Quaternion_t2301928331  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_zero_m1409827619 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localPosition_m4128471975 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.RecordItem::set_RectTransform(UnityEngine.RectTransform)
extern "C" IL2CPP_METHOD_ATTR void RecordItem_set_RectTransform_m3200480544 (RecordItem_t1075151419 * __this, RectTransform_t3704657025 * ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.UI.Text>()
inline Text_t1901882714 * Component_GetComponentInChildren_TisText_t1901882714_m396351542 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  Text_t1901882714 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_m1033527003_gshared)(__this, method);
}
// System.Void DigitsNFCToolkit.OnNDEFWriteFinished::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void OnNDEFWriteFinished__ctor_m1674872822 (OnNDEFWriteFinished_t4102039599 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFCManager::AddNDEFWriteFinishedListener(DigitsNFCToolkit.OnNDEFWriteFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_AddNDEFWriteFinishedListener_m2087486335 (RuntimeObject * __this /* static, unused */, OnNDEFWriteFinished_t4102039599 * ___listener0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.OnNDEFPushFinished::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void OnNDEFPushFinished__ctor_m1418827824 (OnNDEFPushFinished_t4279917764 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.NativeNFCManager::AddNDEFPushFinishedListener(DigitsNFCToolkit.OnNDEFPushFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_AddNDEFPushFinishedListener_m3015772070 (RuntimeObject * __this /* static, unused */, OnNDEFPushFinished_t4279917764 * ___listener0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::UpdateTypeDropdownOptions(System.String[])
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_UpdateTypeDropdownOptions_m2278996124 (WriteScreenView_t2350253495 * __this, StringU5BU5D_t1281789340* ___options0, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C" IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m1620074514 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const RuntimeMethod* method);
// System.Array System.Enum::GetValues(System.Type)
extern "C" IL2CPP_METHOD_ATTR RuntimeArray * Enum_GetValues_m4192343468 (RuntimeObject * __this /* static, unused */, Type_t * p0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<DigitsNFCToolkit.TextRecord/TextEncoding>(System.Collections.IEnumerable)
inline RuntimeObject* Enumerable_Cast_TisTextEncoding_t3210513626_m2305854455 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_Cast_TisTextEncoding_t3210513626_m2305854455_gshared)(__this /* static, unused */, p0, method);
}
// !!0[] System.Linq.Enumerable::ToArray<DigitsNFCToolkit.TextRecord/TextEncoding>(System.Collections.Generic.IEnumerable`1<!!0>)
inline TextEncodingU5BU5D_t2855432831* Enumerable_ToArray_TisTextEncoding_t3210513626_m1218064777 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  TextEncodingU5BU5D_t2855432831* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisTextEncoding_t3210513626_m1218064777_gshared)(__this /* static, unused */, p0, method);
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::UpdateTextEncodingDropdownOptions(System.String[])
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_UpdateTextEncodingDropdownOptions_m2236664622 (WriteScreenView_t2350253495 * __this, StringU5BU5D_t1281789340* ___options0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<DigitsNFCToolkit.Samples.WriteScreenControl/IconID>(System.Collections.IEnumerable)
inline RuntimeObject* Enumerable_Cast_TisIconID_t582738576_m3494484091 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_Cast_TisIconID_t582738576_m3494484091_gshared)(__this /* static, unused */, p0, method);
}
// !!0[] System.Linq.Enumerable::ToArray<DigitsNFCToolkit.Samples.WriteScreenControl/IconID>(System.Collections.Generic.IEnumerable`1<!!0>)
inline IconIDU5BU5D_t2629431601* Enumerable_ToArray_TisIconID_t582738576_m2108801283 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  IconIDU5BU5D_t2629431601* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisIconID_t582738576_m2108801283_gshared)(__this /* static, unused */, p0, method);
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::UpdateIconDropdownOptions(System.String[])
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_UpdateIconDropdownOptions_m304314056 (WriteScreenView_t2350253495 * __this, StringU5BU5D_t1281789340* ___options0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<DigitsNFCToolkit.TextRecord>::.ctor()
inline void List_1__ctor_m2409190732 (List_1_t3785772365 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3785772365 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void DigitsNFCToolkit.TextRecord::.ctor(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void TextRecord__ctor_m3042244564 (TextRecord_t2313697623 * __this, String_t* ___text0, String_t* ___languageCode1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<DigitsNFCToolkit.TextRecord>::Add(!0)
inline void List_1_Add_m2718096786 (List_1_t3785772365 * __this, TextRecord_t2313697623 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3785772365 *, TextRecord_t2313697623 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<DigitsNFCToolkit.MimeMediaRecord>::.ctor()
inline void List_1__ctor_m2367744050 (List_1_t2208895230 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2208895230 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void DigitsNFCToolkit.UriRecord::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void UriRecord__ctor_m714533555 (UriRecord_t2230063309 * __this, String_t* ___fullUri0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.SmartPosterRecord::.ctor(System.String,DigitsNFCToolkit.SmartPosterRecord/RecommendedAction,System.Int32,System.String,System.Collections.Generic.List`1<DigitsNFCToolkit.TextRecord>,System.Collections.Generic.List`1<DigitsNFCToolkit.MimeMediaRecord>,System.Collections.Generic.List`1<DigitsNFCToolkit.NDEFRecord>)
extern "C" IL2CPP_METHOD_ATTR void SmartPosterRecord__ctor_m1413753803 (SmartPosterRecord_t1640848801 * __this, String_t* ___uri0, int32_t ___action1, int32_t ___size2, String_t* ___mimeType3, List_1_t3785772365 * ___titleRecords4, List_1_t2208895230 * ___iconRecords5, List_1_t4162620298 * ___extraRecords6, const RuntimeMethod* method);
// UnityEngine.UI.Dropdown DigitsNFCToolkit.Samples.WriteScreenView::get_TypeDropdown()
extern "C" IL2CPP_METHOD_ATTR Dropdown_t2274391225 * WriteScreenView_get_TypeDropdown_m169477996 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData> UnityEngine.UI.Dropdown::get_options()
extern "C" IL2CPP_METHOD_ATTR List_1_t447389798 * Dropdown_get_options_m2762539965 (Dropdown_t2274391225 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::get_Item(System.Int32)
inline OptionData_t3270282352 * List_1_get_Item_m489380024 (List_1_t447389798 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  OptionData_t3270282352 * (*) (List_1_t447389798 *, int32_t, const RuntimeMethod*))List_1_get_Item_m1328026504_gshared)(__this, p0, method);
}
// System.String UnityEngine.UI.Dropdown/OptionData::get_text()
extern "C" IL2CPP_METHOD_ATTR String_t* OptionData_get_text_m2997376818 (OptionData_t3270282352 * __this, const RuntimeMethod* method);
// System.Object System.Enum::Parse(System.Type,System.String)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Enum_Parse_m1871331077 (RuntimeObject * __this /* static, unused */, Type_t * p0, String_t* p1, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::SwitchToTextRecordInput()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_SwitchToTextRecordInput_m801913819 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::SwitchToUriInput()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_SwitchToUriInput_m1218801993 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::SwitchToMimeMediaInput()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_SwitchToMimeMediaInput_m1134766923 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::SwitchToExternalTypeInput()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_SwitchToExternalTypeInput_m1529510719 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.UI.Dropdown::get_value()
extern "C" IL2CPP_METHOD_ATTR int32_t Dropdown_get_value_m1555353112 (Dropdown_t2274391225 * __this, const RuntimeMethod* method);
// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::get_TextInput()
extern "C" IL2CPP_METHOD_ATTR InputField_t3762917431 * WriteScreenView_get_TextInput_m1287173561 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method);
// System.String UnityEngine.UI.InputField::get_text()
extern "C" IL2CPP_METHOD_ATTR String_t* InputField_get_text_m3534748202 (InputField_t3762917431 * __this, const RuntimeMethod* method);
// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::get_LanguageCodeInput()
extern "C" IL2CPP_METHOD_ATTR InputField_t3762917431 * WriteScreenView_get_LanguageCodeInput_m2131884372 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method);
// UnityEngine.UI.Dropdown DigitsNFCToolkit.Samples.WriteScreenView::get_TextEncodingDropdown()
extern "C" IL2CPP_METHOD_ATTR Dropdown_t2274391225 * WriteScreenView_get_TextEncodingDropdown_m1795396977 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.TextRecord::.ctor(System.String,System.String,DigitsNFCToolkit.TextRecord/TextEncoding)
extern "C" IL2CPP_METHOD_ATTR void TextRecord__ctor_m2191892803 (TextRecord_t2313697623 * __this, String_t* ___text0, String_t* ___languageCode1, int32_t ___textEncoding2, const RuntimeMethod* method);
// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::get_UriInput()
extern "C" IL2CPP_METHOD_ATTR InputField_t3762917431 * WriteScreenView_get_UriInput_m822036843 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method);
// UnityEngine.UI.Dropdown DigitsNFCToolkit.Samples.WriteScreenView::get_IconDropdown()
extern "C" IL2CPP_METHOD_ATTR Dropdown_t2274391225 * WriteScreenView_get_IconDropdown_m124205079 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method);
// System.Byte[] DigitsNFCToolkit.Samples.WriteScreenControl::GetIconBytes(DigitsNFCToolkit.Samples.WriteScreenControl/IconID)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_t4116647657* WriteScreenControl_GetIconBytes_m2925827785 (WriteScreenControl_t1506090515 * __this, int32_t ___iconID0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.MimeMediaRecord::.ctor(System.String,System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void MimeMediaRecord__ctor_m353281183 (MimeMediaRecord_t736820488 * __this, String_t* ___mimeType0, ByteU5BU5D_t4116647657* ___mimeData1, const RuntimeMethod* method);
// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::get_DomainNameInput()
extern "C" IL2CPP_METHOD_ATTR InputField_t3762917431 * WriteScreenView_get_DomainNameInput_m3346781220 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method);
// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::get_DomainTypeInput()
extern "C" IL2CPP_METHOD_ATTR InputField_t3762917431 * WriteScreenView_get_DomainTypeInput_m3846803875 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method);
// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::get_DomainDataInput()
extern "C" IL2CPP_METHOD_ATTR InputField_t3762917431 * WriteScreenView_get_DomainDataInput_m3660011111 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.ExternalTypeRecord::.ctor(System.String,System.String,System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void ExternalTypeRecord__ctor_m222117595 (ExternalTypeRecord_t4087466745 * __this, String_t* ___domainName0, String_t* ___domainType1, ByteU5BU5D_t4116647657* ___domainData2, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::UpdateNDEFMessage(DigitsNFCToolkit.NDEFMessage)
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_UpdateNDEFMessage_m4183170585 (WriteScreenView_t2350253495 * __this, NDEFMessage_t279637043 * ___message0, const RuntimeMethod* method);
// UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
extern "C" IL2CPP_METHOD_ATTR Texture2D_t3840446185 * Sprite_get_texture_m3976398399 (Sprite_t280657092 * __this, const RuntimeMethod* method);
// System.Byte[] UnityEngine.ImageConversion::EncodeToPNG(UnityEngine.Texture2D)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_t4116647657* ImageConversion_EncodeToPNG_m2292631531 (RuntimeObject * __this /* static, unused */, Texture2D_t3840446185 * p0, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::CleanupRecordItems()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_CleanupRecordItems_m619684253 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.MessageScreenView::Hide()
extern "C" IL2CPP_METHOD_ATTR void MessageScreenView_Hide_m3938180501 (MessageScreenView_t146641597 * __this, const RuntimeMethod* method);
// DigitsNFCToolkit.NDEFMessage DigitsNFCToolkit.NDEFWriteResult::get_Message()
extern "C" IL2CPP_METHOD_ATTR NDEFMessage_t279637043 * NDEFWriteResult_get_Message_m1148114373 (NDEFWriteResult_t4210562629 * __this, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.NDEFWriteResult::get_Success()
extern "C" IL2CPP_METHOD_ATTR bool NDEFWriteResult_get_Success_m3422449045 (NDEFWriteResult_t4210562629 * __this, const RuntimeMethod* method);
// System.String DigitsNFCToolkit.NDEFWriteResult::get_TagID()
extern "C" IL2CPP_METHOD_ATTR String_t* NDEFWriteResult_get_TagID_m1976340487 (NDEFWriteResult_t4210562629 * __this, const RuntimeMethod* method);
// DigitsNFCToolkit.NDEFWriteError DigitsNFCToolkit.NDEFWriteResult::get_Error()
extern "C" IL2CPP_METHOD_ATTR int32_t NDEFWriteResult_get_Error_m2217684677 (NDEFWriteResult_t4210562629 * __this, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.MessageScreenView::SwitchToWriteResult(System.String)
extern "C" IL2CPP_METHOD_ATTR void MessageScreenView_SwitchToWriteResult_m1196524044 (MessageScreenView_t146641597 * __this, String_t* ___writeResult0, const RuntimeMethod* method);
// DigitsNFCToolkit.NDEFMessage DigitsNFCToolkit.NDEFPushResult::get_Message()
extern "C" IL2CPP_METHOD_ATTR NDEFMessage_t279637043 * NDEFPushResult_get_Message_m1817705072 (NDEFPushResult_t3422827153 * __this, const RuntimeMethod* method);
// System.Boolean DigitsNFCToolkit.NDEFPushResult::get_Success()
extern "C" IL2CPP_METHOD_ATTR bool NDEFPushResult_get_Success_m1923447426 (NDEFPushResult_t3422827153 * __this, const RuntimeMethod* method);
// System.Void DigitsNFCToolkit.Samples.MessageScreenView::SwitchToPushResult(System.String)
extern "C" IL2CPP_METHOD_ATTR void MessageScreenView_SwitchToPushResult_m1664855704 (MessageScreenView_t146641597 * __this, String_t* ___pushResult0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Dropdown>()
inline Dropdown_t2274391225 * Component_GetComponent_TisDropdown_t2274391225_m782479209 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  Dropdown_t2274391225 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.InputField>()
inline InputField_t3762917431 * Component_GetComponent_TisInputField_t3762917431_m3342128916 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  InputField_t3762917431 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Color__ctor_m286683560 (Color_t2555686324 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Selectable::set_interactable(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Selectable_set_interactable_m3105888815 (Selectable_t3250028441 * __this, bool p0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Dropdown::ClearOptions()
extern "C" IL2CPP_METHOD_ATTR void Dropdown_ClearOptions_m4085591601 (Dropdown_t2274391225 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::.ctor()
inline void List_1__ctor_m1438953653 (List_1_t447389798 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t447389798 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void UnityEngine.UI.Dropdown/OptionData::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void OptionData__ctor_m2696491456 (OptionData_t3270282352 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::Add(!0)
inline void List_1_Add_m3700962105 (List_1_t447389798 * __this, OptionData_t3270282352 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t447389798 *, OptionData_t3270282352 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Void UnityEngine.UI.Dropdown::AddOptions(System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>)
extern "C" IL2CPP_METHOD_ATTR void Dropdown_AddOptions_m3733885929 (Dropdown_t2274391225 * __this, List_1_t447389798 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.InputField::set_text(System.String)
extern "C" IL2CPP_METHOD_ATTR void InputField_set_text_m1877260015 (InputField_t3762917431 * __this, String_t* p0, const RuntimeMethod* method);
// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.WriteScreenView::CreateRecordItem(DigitsNFCToolkit.Samples.RecordItem)
extern "C" IL2CPP_METHOD_ATTR RecordItem_t1075151419 * WriteScreenView_CreateRecordItem_m4278698087 (WriteScreenView_t2350253495 * __this, RecordItem_t1075151419 * ___prefab0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<DigitsNFCToolkit.MimeMediaRecord>::Add(!0)
inline void List_1_Add_m3311994978 (List_1_t2208895230 * __this, MimeMediaRecord_t736820488 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2208895230 *, MimeMediaRecord_t736820488 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONObject::get_Item(System.String)
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONObject_get_Item_m3915825514 (JSONObject_t321714843 * __this, String_t* ___key0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<DigitsNFCToolkit.TextRecord>::get_Item(System.Int32)
inline TextRecord_t2313697623 * List_1_get_Item_m3864448256 (List_1_t3785772365 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  TextRecord_t2313697623 * (*) (List_1_t3785772365 *, int32_t, const RuntimeMethod*))List_1_get_Item_m1328026504_gshared)(__this, p0, method);
}
// !0 System.Collections.Generic.List`1<DigitsNFCToolkit.MimeMediaRecord>::get_Item(System.Int32)
inline MimeMediaRecord_t736820488 * List_1_get_Item_m988656695 (List_1_t2208895230 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  MimeMediaRecord_t736820488 * (*) (List_1_t2208895230 *, int32_t, const RuntimeMethod*))List_1_get_Item_m1328026504_gshared)(__this, p0, method);
}
// System.String System.Convert::ToBase64String(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR String_t* Convert_ToBase64String_m3839334935 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* p0, const RuntimeMethod* method);
// System.String System.String::PadRight(System.Int32,System.Char)
extern "C" IL2CPP_METHOD_ATTR String_t* String_PadRight_m50345030 (String_t* __this, int32_t p0, Il2CppChar p1, const RuntimeMethod* method);
// System.Byte[] System.Convert::FromBase64String(System.String)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_t4116647657* Convert_FromBase64String_m3685135396 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.AbsoluteUriRecord::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void AbsoluteUriRecord__ctor_m3415815829 (AbsoluteUriRecord_t2525168784 * __this, String_t* ___uri0, const RuntimeMethod* method)
{
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		((NDEFRecord_t2690545556 *)__this)->set_type_0(0);
		String_t* L_0 = ___uri0;
		__this->set_uri_1(L_0);
		return;
	}
}
// System.Void DigitsNFCToolkit.AbsoluteUriRecord::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void AbsoluteUriRecord__ctor_m157112738 (AbsoluteUriRecord_t2525168784 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		VirtActionInvoker1< JSONObject_t321714843 * >::Invoke(6 /* System.Void DigitsNFCToolkit.NDEFRecord::ParseJSON(DigitsNFCToolkit.JSON.JSONObject) */, __this, L_0);
		return;
	}
}
// System.Void DigitsNFCToolkit.AbsoluteUriRecord::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void AbsoluteUriRecord_ParseJSON_m530352929 (AbsoluteUriRecord_t2525168784 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AbsoluteUriRecord_ParseJSON_m530352929_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		NDEFRecord_ParseJSON_m1625700843(__this, L_0, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_1 = ___jsonObject0;
		String_t** L_2 = __this->get_address_of_uri_1();
		NullCheck(L_1);
		JSONObject_TryGetString_m766921647(L_1, _stringLiteral3313977880, (String_t**)L_2, /*hidden argument*/NULL);
		return;
	}
}
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.AbsoluteUriRecord::ToJSON()
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * AbsoluteUriRecord_ToJSON_m1384635261 (AbsoluteUriRecord_t2525168784 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AbsoluteUriRecord_ToJSON_m1384635261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t321714843 * V_0 = NULL;
	{
		JSONObject_t321714843 * L_0 = NDEFRecord_ToJSON_m164142245(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t321714843 * L_1 = V_0;
		String_t* L_2 = __this->get_uri_1();
		JSONValue_t4275860644 * L_3 = JSONValue_op_Implicit_m357672470(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		JSONObject_Add_m2412535806(L_1, _stringLiteral3313977880, L_3, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_4 = V_0;
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.EmptyRecord::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void EmptyRecord__ctor_m4226548438 (EmptyRecord_t1486430273 * __this, String_t* ___uri0, const RuntimeMethod* method)
{
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		((NDEFRecord_t2690545556 *)__this)->set_type_0(1);
		return;
	}
}
// System.Void DigitsNFCToolkit.EmptyRecord::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void EmptyRecord__ctor_m3438728046 (EmptyRecord_t1486430273 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		VirtActionInvoker1< JSONObject_t321714843 * >::Invoke(6 /* System.Void DigitsNFCToolkit.NDEFRecord::ParseJSON(DigitsNFCToolkit.JSON.JSONObject) */, __this, L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.ExternalTypeRecord::.ctor(System.String,System.String,System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void ExternalTypeRecord__ctor_m222117595 (ExternalTypeRecord_t4087466745 * __this, String_t* ___domainName0, String_t* ___domainType1, ByteU5BU5D_t4116647657* ___domainData2, const RuntimeMethod* method)
{
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		((NDEFRecord_t2690545556 *)__this)->set_type_0(2);
		String_t* L_0 = ___domainName0;
		__this->set_domainName_1(L_0);
		String_t* L_1 = ___domainType1;
		__this->set_domainType_2(L_1);
		ByteU5BU5D_t4116647657* L_2 = ___domainData2;
		__this->set_domainData_3(L_2);
		return;
	}
}
// System.Void DigitsNFCToolkit.ExternalTypeRecord::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void ExternalTypeRecord__ctor_m3317940198 (ExternalTypeRecord_t4087466745 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		VirtActionInvoker1< JSONObject_t321714843 * >::Invoke(6 /* System.Void DigitsNFCToolkit.NDEFRecord::ParseJSON(DigitsNFCToolkit.JSON.JSONObject) */, __this, L_0);
		return;
	}
}
// System.Void DigitsNFCToolkit.ExternalTypeRecord::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void ExternalTypeRecord_ParseJSON_m2193836732 (ExternalTypeRecord_t4087466745 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExternalTypeRecord_ParseJSON_m2193836732_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		NDEFRecord_ParseJSON_m1625700843(__this, L_0, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_1 = ___jsonObject0;
		String_t** L_2 = __this->get_address_of_domainName_1();
		NullCheck(L_1);
		JSONObject_TryGetString_m766921647(L_1, _stringLiteral3820123721, (String_t**)L_2, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_3 = ___jsonObject0;
		String_t** L_4 = __this->get_address_of_domainType_2();
		NullCheck(L_3);
		JSONObject_TryGetString_m766921647(L_3, _stringLiteral863860273, (String_t**)L_4, /*hidden argument*/NULL);
		V_0 = (String_t*)NULL;
		JSONObject_t321714843 * L_5 = ___jsonObject0;
		NullCheck(L_5);
		bool L_6 = JSONObject_TryGetString_m766921647(L_5, _stringLiteral690725853, (String_t**)(&V_0), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004b;
		}
	}
	{
		String_t* L_7 = V_0;
		ByteU5BU5D_t4116647657* L_8 = Util_DecodeBase64UrlSafe_m4209975833(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		__this->set_domainData_3(L_8);
	}

IL_004b:
	{
		return;
	}
}
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.ExternalTypeRecord::ToJSON()
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * ExternalTypeRecord_ToJSON_m2957991546 (ExternalTypeRecord_t4087466745 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExternalTypeRecord_ToJSON_m2957991546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t321714843 * V_0 = NULL;
	{
		JSONObject_t321714843 * L_0 = NDEFRecord_ToJSON_m164142245(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t321714843 * L_1 = V_0;
		String_t* L_2 = __this->get_domainName_1();
		JSONValue_t4275860644 * L_3 = JSONValue_op_Implicit_m357672470(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		JSONObject_Add_m2412535806(L_1, _stringLiteral3820123721, L_3, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_4 = V_0;
		String_t* L_5 = __this->get_domainType_2();
		JSONValue_t4275860644 * L_6 = JSONValue_op_Implicit_m357672470(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		JSONObject_Add_m2412535806(L_4, _stringLiteral863860273, L_6, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_7 = V_0;
		ByteU5BU5D_t4116647657* L_8 = __this->get_domainData_3();
		String_t* L_9 = Util_EncodeBase64UrlSafe_m2760074679(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		JSONValue_t4275860644 * L_10 = JSONValue_op_Implicit_m357672470(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		JSONObject_Add_m2412535806(L_7, _stringLiteral690725853, L_10, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_11 = V_0;
		return L_11;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.IOSNFC::.ctor()
extern "C" IL2CPP_METHOD_ATTR void IOSNFC__ctor_m952920223 (IOSNFC_t293874181 * __this, const RuntimeMethod* method)
{
	{
		NativeNFC__ctor_m920500560(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _nativeNFC_initialize(char*);
// System.Void DigitsNFCToolkit.IOSNFC::_nativeNFC_initialize(System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSNFC__nativeNFC_initialize_m1907124886 (RuntimeObject * __this /* static, unused */, String_t* ___gameObjectName0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___gameObjectName0' to native representation
	char* ____gameObjectName0_marshaled = NULL;
	____gameObjectName0_marshaled = il2cpp_codegen_marshal_string(___gameObjectName0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_nativeNFC_initialize)(____gameObjectName0_marshaled);

	// Marshaling cleanup of parameter '___gameObjectName0' native representation
	il2cpp_codegen_marshal_free(____gameObjectName0_marshaled);
	____gameObjectName0_marshaled = NULL;

}
extern "C" int32_t DEFAULT_CALL _nativeNFC_isNFCTagInfoReadSupported();
// System.Boolean DigitsNFCToolkit.IOSNFC::_nativeNFC_isNFCTagInfoReadSupported()
extern "C" IL2CPP_METHOD_ATTR bool IOSNFC__nativeNFC_isNFCTagInfoReadSupported_m305629107 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_nativeNFC_isNFCTagInfoReadSupported)();

	return static_cast<bool>(returnValue);
}
extern "C" int32_t DEFAULT_CALL _nativeNFC_isNDEFReadSupported();
// System.Boolean DigitsNFCToolkit.IOSNFC::_nativeNFC_isNDEFReadSupported()
extern "C" IL2CPP_METHOD_ATTR bool IOSNFC__nativeNFC_isNDEFReadSupported_m1124741525 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_nativeNFC_isNDEFReadSupported)();

	return static_cast<bool>(returnValue);
}
extern "C" int32_t DEFAULT_CALL _nativeNFC_isNDEFWriteSupported();
// System.Boolean DigitsNFCToolkit.IOSNFC::_nativeNFC_isNDEFWriteSupported()
extern "C" IL2CPP_METHOD_ATTR bool IOSNFC__nativeNFC_isNDEFWriteSupported_m1714142755 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_nativeNFC_isNDEFWriteSupported)();

	return static_cast<bool>(returnValue);
}
extern "C" int32_t DEFAULT_CALL _nativeNFC_isNFCEnabled();
// System.Boolean DigitsNFCToolkit.IOSNFC::_nativeNFC_isNFCEnabled()
extern "C" IL2CPP_METHOD_ATTR bool IOSNFC__nativeNFC_isNFCEnabled_m3040644964 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_nativeNFC_isNFCEnabled)();

	return static_cast<bool>(returnValue);
}
extern "C" void DEFAULT_CALL _nativeNFC_disable();
// System.Void DigitsNFCToolkit.IOSNFC::_nativeNFC_disable()
extern "C" IL2CPP_METHOD_ATTR void IOSNFC__nativeNFC_disable_m24928640 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_nativeNFC_disable)();

}
extern "C" void DEFAULT_CALL _nativeNFC_enable();
// System.Void DigitsNFCToolkit.IOSNFC::_nativeNFC_enable()
extern "C" IL2CPP_METHOD_ATTR void IOSNFC__nativeNFC_enable_m1112423440 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_nativeNFC_enable)();

}
extern "C" void DEFAULT_CALL _nativeNFC_setResetOnTimeout(int32_t);
// System.Void DigitsNFCToolkit.IOSNFC::_nativeNFC_setResetOnTimeout(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void IOSNFC__nativeNFC_setResetOnTimeout_m2191450753 (RuntimeObject * __this /* static, unused */, bool ___resetOnTimeout0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_nativeNFC_setResetOnTimeout)(static_cast<int32_t>(___resetOnTimeout0));

}
extern "C" void DEFAULT_CALL _nativeNFC_requestNDEFWrite(char*);
// System.Void DigitsNFCToolkit.IOSNFC::_nativeNFC_requestNDEFWrite(System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSNFC__nativeNFC_requestNDEFWrite_m2029809237 (RuntimeObject * __this /* static, unused */, String_t* ___messageJSON0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___messageJSON0' to native representation
	char* ____messageJSON0_marshaled = NULL;
	____messageJSON0_marshaled = il2cpp_codegen_marshal_string(___messageJSON0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_nativeNFC_requestNDEFWrite)(____messageJSON0_marshaled);

	// Marshaling cleanup of parameter '___messageJSON0' native representation
	il2cpp_codegen_marshal_free(____messageJSON0_marshaled);
	____messageJSON0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _nativeNFC_cancelNDEFWriteRequest();
// System.Void DigitsNFCToolkit.IOSNFC::_nativeNFC_cancelNDEFWriteRequest()
extern "C" IL2CPP_METHOD_ATTR void IOSNFC__nativeNFC_cancelNDEFWriteRequest_m3528688979 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_nativeNFC_cancelNDEFWriteRequest)();

}
// System.Boolean DigitsNFCToolkit.IOSNFC::get_ResetOnTimeout()
extern "C" IL2CPP_METHOD_ATTR bool IOSNFC_get_ResetOnTimeout_m3371504972 (IOSNFC_t293874181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = NativeNFC_get_ResetOnTimeout_m1804879484(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void DigitsNFCToolkit.IOSNFC::set_ResetOnTimeout(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void IOSNFC_set_ResetOnTimeout_m2510433323 (IOSNFC_t293874181 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		NativeNFC_set_ResetOnTimeout_m1401711570(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ((NativeNFC_t1941597496 *)__this)->get_resetOnTimeout_8();
		IOSNFC__nativeNFC_setResetOnTimeout_m2191450753(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.IOSNFC::Initialize()
extern "C" IL2CPP_METHOD_ATTR void IOSNFC_Initialize_m1876576823 (IOSNFC_t293874181 * __this, const RuntimeMethod* method)
{
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m4211327027(L_0, /*hidden argument*/NULL);
		IOSNFC__nativeNFC_initialize_m1907124886(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DigitsNFCToolkit.IOSNFC::IsNFCTagInfoReadSupported()
extern "C" IL2CPP_METHOD_ATTR bool IOSNFC_IsNFCTagInfoReadSupported_m4136616386 (IOSNFC_t293874181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = IOSNFC__nativeNFC_isNFCTagInfoReadSupported_m305629107(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean DigitsNFCToolkit.IOSNFC::IsNDEFReadSupported()
extern "C" IL2CPP_METHOD_ATTR bool IOSNFC_IsNDEFReadSupported_m233125081 (IOSNFC_t293874181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = IOSNFC__nativeNFC_isNDEFReadSupported_m1124741525(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean DigitsNFCToolkit.IOSNFC::IsNDEFWriteSupported()
extern "C" IL2CPP_METHOD_ATTR bool IOSNFC_IsNDEFWriteSupported_m271953153 (IOSNFC_t293874181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = IOSNFC__nativeNFC_isNDEFWriteSupported_m1714142755(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean DigitsNFCToolkit.IOSNFC::IsNFCEnabled()
extern "C" IL2CPP_METHOD_ATTR bool IOSNFC_IsNFCEnabled_m3300123586 (IOSNFC_t293874181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = IOSNFC__nativeNFC_isNFCEnabled_m3040644964(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean DigitsNFCToolkit.IOSNFC::IsNDEFPushEnabled()
extern "C" IL2CPP_METHOD_ATTR bool IOSNFC_IsNDEFPushEnabled_m2191275901 (IOSNFC_t293874181 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Void DigitsNFCToolkit.IOSNFC::Enable()
extern "C" IL2CPP_METHOD_ATTR void IOSNFC_Enable_m1077751544 (IOSNFC_t293874181 * __this, const RuntimeMethod* method)
{
	{
		IOSNFC__nativeNFC_enable_m1112423440(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.IOSNFC::Disable()
extern "C" IL2CPP_METHOD_ATTR void IOSNFC_Disable_m1643307866 (IOSNFC_t293874181 * __this, const RuntimeMethod* method)
{
	{
		IOSNFC__nativeNFC_disable_m24928640(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.IOSNFC::RequestNDEFWrite(System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSNFC_RequestNDEFWrite_m4235354767 (IOSNFC_t293874181 * __this, String_t* ___messageJSON0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___messageJSON0;
		IOSNFC__nativeNFC_requestNDEFWrite_m2029809237(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.IOSNFC::CancelNDEFWriteRequest()
extern "C" IL2CPP_METHOD_ATTR void IOSNFC_CancelNDEFWriteRequest_m132034116 (IOSNFC_t293874181 * __this, const RuntimeMethod* method)
{
	{
		IOSNFC__nativeNFC_cancelNDEFWriteRequest_m3528688979(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.IOSNFC::RequestNDEFPush(System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSNFC_RequestNDEFPush_m3130439772 (IOSNFC_t293874181 * __this, String_t* ___messageJSON0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.IOSNFC::CancelNDEFPushRequest()
extern "C" IL2CPP_METHOD_ATTR void IOSNFC_CancelNDEFPushRequest_m1945873742 (IOSNFC_t293874181 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.JSON.JSONArray::.ctor()
extern "C" IL2CPP_METHOD_ATTR void JSONArray__ctor_m3612236623 (JSONArray_t4024675823 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONArray__ctor_m3612236623_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1452968090 * L_0 = (List_1_t1452968090 *)il2cpp_codegen_object_new(List_1_t1452968090_il2cpp_TypeInfo_var);
		List_1__ctor_m452167397(L_0, /*hidden argument*/List_1__ctor_m452167397_RuntimeMethod_var);
		__this->set_values_0(L_0);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONArray::.ctor(DigitsNFCToolkit.JSON.JSONArray)
extern "C" IL2CPP_METHOD_ATTR void JSONArray__ctor_m4099340829 (JSONArray_t4024675823 * __this, JSONArray_t4024675823 * ___array0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONArray__ctor_m4099340829_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONValue_t4275860644 * V_0 = NULL;
	Enumerator_t3342211967  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1452968090 * L_0 = (List_1_t1452968090 *)il2cpp_codegen_object_new(List_1_t1452968090_il2cpp_TypeInfo_var);
		List_1__ctor_m452167397(L_0, /*hidden argument*/List_1__ctor_m452167397_RuntimeMethod_var);
		__this->set_values_0(L_0);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		List_1_t1452968090 * L_1 = (List_1_t1452968090 *)il2cpp_codegen_object_new(List_1_t1452968090_il2cpp_TypeInfo_var);
		List_1__ctor_m452167397(L_1, /*hidden argument*/List_1__ctor_m452167397_RuntimeMethod_var);
		__this->set_values_0(L_1);
		JSONArray_t4024675823 * L_2 = ___array0;
		NullCheck(L_2);
		List_1_t1452968090 * L_3 = L_2->get_values_0();
		NullCheck(L_3);
		Enumerator_t3342211967  L_4 = List_1_GetEnumerator_m3485391654(L_3, /*hidden argument*/List_1_GetEnumerator_m3485391654_RuntimeMethod_var);
		V_1 = L_4;
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0046;
		}

IL_002d:
		{
			JSONValue_t4275860644 * L_5 = Enumerator_get_Current_m976429252((Enumerator_t3342211967 *)(&V_1), /*hidden argument*/Enumerator_get_Current_m976429252_RuntimeMethod_var);
			V_0 = L_5;
			List_1_t1452968090 * L_6 = __this->get_values_0();
			JSONValue_t4275860644 * L_7 = V_0;
			JSONValue_t4275860644 * L_8 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
			JSONValue__ctor_m1222737883(L_8, L_7, /*hidden argument*/NULL);
			NullCheck(L_6);
			List_1_Add_m119444007(L_6, L_8, /*hidden argument*/List_1_Add_m119444007_RuntimeMethod_var);
		}

IL_0046:
		{
			bool L_9 = Enumerator_MoveNext_m4065387915((Enumerator_t3342211967 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m4065387915_RuntimeMethod_var);
			if (L_9)
			{
				goto IL_002d;
			}
		}

IL_0052:
		{
			IL2CPP_LEAVE(0x65, FINALLY_0057);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0057;
	}

FINALLY_0057:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3644548256((Enumerator_t3342211967 *)(&V_1), /*hidden argument*/Enumerator_Dispose_m3644548256_RuntimeMethod_var);
		IL2CPP_END_FINALLY(87)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(87)
	{
		IL2CPP_JUMP_TBL(0x65, IL_0065)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0065:
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONArray::Add(DigitsNFCToolkit.JSON.JSONValue)
extern "C" IL2CPP_METHOD_ATTR void JSONArray_Add_m1346855851 (JSONArray_t4024675823 * __this, JSONValue_t4275860644 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONArray_Add_m1346855851_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1452968090 * L_0 = __this->get_values_0();
		JSONValue_t4275860644 * L_1 = ___value0;
		NullCheck(L_0);
		List_1_Add_m119444007(L_0, L_1, /*hidden argument*/List_1_Add_m119444007_RuntimeMethod_var);
		return;
	}
}
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONArray::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONArray_get_Item_m1052458067 (JSONArray_t4024675823 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONArray_get_Item_m1052458067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1452968090 * L_0 = __this->get_values_0();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		JSONValue_t4275860644 * L_2 = List_1_get_Item_m740911558(L_0, L_1, /*hidden argument*/List_1_get_Item_m740911558_RuntimeMethod_var);
		return L_2;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONArray::set_Item(System.Int32,DigitsNFCToolkit.JSON.JSONValue)
extern "C" IL2CPP_METHOD_ATTR void JSONArray_set_Item_m1103846076 (JSONArray_t4024675823 * __this, int32_t ___index0, JSONValue_t4275860644 * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONArray_set_Item_m1103846076_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1452968090 * L_0 = __this->get_values_0();
		int32_t L_1 = ___index0;
		JSONValue_t4275860644 * L_2 = ___value1;
		NullCheck(L_0);
		List_1_set_Item_m1423443710(L_0, L_1, L_2, /*hidden argument*/List_1_set_Item_m1423443710_RuntimeMethod_var);
		return;
	}
}
// System.Int32 DigitsNFCToolkit.JSON.JSONArray::get_Length()
extern "C" IL2CPP_METHOD_ATTR int32_t JSONArray_get_Length_m1921089944 (JSONArray_t4024675823 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONArray_get_Length_m1921089944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1452968090 * L_0 = __this->get_values_0();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m3731320739(L_0, /*hidden argument*/List_1_get_Count_m3731320739_RuntimeMethod_var);
		return L_1;
	}
}
// System.String DigitsNFCToolkit.JSON.JSONArray::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* JSONArray_ToString_m3591961336 (JSONArray_t4024675823 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONArray_ToString_m3591961336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	JSONValue_t4275860644 * V_1 = NULL;
	Enumerator_t3342211967  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t * L_0 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_Append_m2383614642(L_1, ((int32_t)91), /*hidden argument*/NULL);
		List_1_t1452968090 * L_2 = __this->get_values_0();
		NullCheck(L_2);
		Enumerator_t3342211967  L_3 = List_1_GetEnumerator_m3485391654(L_2, /*hidden argument*/List_1_GetEnumerator_m3485391654_RuntimeMethod_var);
		V_2 = L_3;
	}

IL_001b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003e;
		}

IL_0020:
		{
			JSONValue_t4275860644 * L_4 = Enumerator_get_Current_m976429252((Enumerator_t3342211967 *)(&V_2), /*hidden argument*/Enumerator_get_Current_m976429252_RuntimeMethod_var);
			V_1 = L_4;
			StringBuilder_t * L_5 = V_0;
			JSONValue_t4275860644 * L_6 = V_1;
			NullCheck(L_6);
			String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
			NullCheck(L_5);
			StringBuilder_Append_m1965104174(L_5, L_7, /*hidden argument*/NULL);
			StringBuilder_t * L_8 = V_0;
			NullCheck(L_8);
			StringBuilder_Append_m2383614642(L_8, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_003e:
		{
			bool L_9 = Enumerator_MoveNext_m4065387915((Enumerator_t3342211967 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m4065387915_RuntimeMethod_var);
			if (L_9)
			{
				goto IL_0020;
			}
		}

IL_004a:
		{
			IL2CPP_LEAVE(0x5D, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3644548256((Enumerator_t3342211967 *)(&V_2), /*hidden argument*/Enumerator_Dispose_m3644548256_RuntimeMethod_var);
		IL2CPP_END_FINALLY(79)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x5D, IL_005d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_005d:
	{
		List_1_t1452968090 * L_10 = __this->get_values_0();
		NullCheck(L_10);
		int32_t L_11 = List_1_get_Count_m3731320739(L_10, /*hidden argument*/List_1_get_Count_m3731320739_RuntimeMethod_var);
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		StringBuilder_t * L_12 = V_0;
		StringBuilder_t * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = StringBuilder_get_Length_m3238060835(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		StringBuilder_Remove_m940064945(L_12, ((int32_t)il2cpp_codegen_subtract((int32_t)L_14, (int32_t)1)), 1, /*hidden argument*/NULL);
	}

IL_007e:
	{
		StringBuilder_t * L_15 = V_0;
		NullCheck(L_15);
		StringBuilder_Append_m2383614642(L_15, ((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		return L_17;
	}
}
// System.Collections.Generic.IEnumerator`1<DigitsNFCToolkit.JSON.JSONValue> DigitsNFCToolkit.JSON.JSONArray::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* JSONArray_GetEnumerator_m2591971435 (JSONArray_t4024675823 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONArray_GetEnumerator_m2591971435_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1452968090 * L_0 = __this->get_values_0();
		NullCheck(L_0);
		Enumerator_t3342211967  L_1 = List_1_GetEnumerator_m3485391654(L_0, /*hidden argument*/List_1_GetEnumerator_m3485391654_RuntimeMethod_var);
		Enumerator_t3342211967  L_2 = L_1;
		RuntimeObject * L_3 = Box(Enumerator_t3342211967_il2cpp_TypeInfo_var, &L_2);
		return (RuntimeObject*)L_3;
	}
}
// System.Collections.IEnumerator DigitsNFCToolkit.JSON.JSONArray::System.Collections.IEnumerable.GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* JSONArray_System_Collections_IEnumerable_GetEnumerator_m3127926955 (JSONArray_t4024675823 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONArray_System_Collections_IEnumerable_GetEnumerator_m3127926955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1452968090 * L_0 = __this->get_values_0();
		NullCheck(L_0);
		Enumerator_t3342211967  L_1 = List_1_GetEnumerator_m3485391654(L_0, /*hidden argument*/List_1_GetEnumerator_m3485391654_RuntimeMethod_var);
		Enumerator_t3342211967  L_2 = L_1;
		RuntimeObject * L_3 = Box(Enumerator_t3342211967_il2cpp_TypeInfo_var, &L_2);
		return (RuntimeObject*)L_3;
	}
}
// DigitsNFCToolkit.JSON.JSONArray DigitsNFCToolkit.JSON.JSONArray::Parse(System.String)
extern "C" IL2CPP_METHOD_ATTR JSONArray_t4024675823 * JSONArray_Parse_m572560914 (RuntimeObject * __this /* static, unused */, String_t* ___jsonString0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONArray_Parse_m572560914_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t321714843 * V_0 = NULL;
	JSONArray_t4024675823 * G_B3_0 = NULL;
	{
		String_t* L_0 = ___jsonString0;
		Il2CppChar L_1 = ((Il2CppChar)((int32_t)125));
		RuntimeObject * L_2 = Box(Char_t3634460470_il2cpp_TypeInfo_var, &L_1);
		String_t* L_3 = String_Concat_m1715369213(NULL /*static, unused*/, _stringLiteral3685564639, L_0, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_4 = JSONObject_Parse_m2486419426(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		JSONObject_t321714843 * L_5 = V_0;
		if (L_5)
		{
			goto IL_0024;
		}
	}
	{
		G_B3_0 = ((JSONArray_t4024675823 *)(NULL));
		goto IL_0034;
	}

IL_0024:
	{
		JSONObject_t321714843 * L_6 = V_0;
		NullCheck(L_6);
		JSONValue_t4275860644 * L_7 = JSONObject_GetValue_m193839943(L_6, _stringLiteral4007973390, /*hidden argument*/NULL);
		NullCheck(L_7);
		JSONArray_t4024675823 * L_8 = JSONValue_get_Array_m3632598190(L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_0034:
	{
		return G_B3_0;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONArray::Clear()
extern "C" IL2CPP_METHOD_ATTR void JSONArray_Clear_m3405071474 (JSONArray_t4024675823 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONArray_Clear_m3405071474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1452968090 * L_0 = __this->get_values_0();
		NullCheck(L_0);
		List_1_Clear_m613793141(L_0, /*hidden argument*/List_1_Clear_m613793141_RuntimeMethod_var);
		return;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONArray::Remove(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void JSONArray_Remove_m1539378238 (JSONArray_t4024675823 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONArray_Remove_m1539378238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_1 = ___index0;
		List_1_t1452968090 * L_2 = __this->get_values_0();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m3731320739(L_2, /*hidden argument*/List_1_get_Count_m3731320739_RuntimeMethod_var);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0029;
		}
	}
	{
		List_1_t1452968090 * L_4 = __this->get_values_0();
		int32_t L_5 = ___index0;
		NullCheck(L_4);
		List_1_RemoveAt_m2053294765(L_4, L_5, /*hidden argument*/List_1_RemoveAt_m2053294765_RuntimeMethod_var);
		goto IL_006d;
	}

IL_0029:
	{
		ObjectU5BU5D_t2843939325* L_6 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t2843939325* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral3707480139);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral3707480139);
		ObjectU5BU5D_t2843939325* L_8 = L_7;
		int32_t L_9 = ___index0;
		int32_t L_10 = L_9;
		RuntimeObject * L_11 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_11);
		ObjectU5BU5D_t2843939325* L_12 = L_8;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral685789873);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral685789873);
		ObjectU5BU5D_t2843939325* L_13 = L_12;
		List_1_t1452968090 * L_14 = __this->get_values_0();
		NullCheck(L_14);
		int32_t L_15 = List_1_get_Count_m3731320739(L_14, /*hidden argument*/List_1_get_Count_m3731320739_RuntimeMethod_var);
		int32_t L_16 = L_15;
		RuntimeObject * L_17 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_17);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_17);
		ObjectU5BU5D_t2843939325* L_18 = L_13;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral3452614535);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral3452614535);
		String_t* L_19 = String_Concat_m2971454694(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
	}

IL_006d:
	{
		return;
	}
}
// DigitsNFCToolkit.JSON.JSONArray DigitsNFCToolkit.JSON.JSONArray::op_Addition(DigitsNFCToolkit.JSON.JSONArray,DigitsNFCToolkit.JSON.JSONArray)
extern "C" IL2CPP_METHOD_ATTR JSONArray_t4024675823 * JSONArray_op_Addition_m1422077166 (RuntimeObject * __this /* static, unused */, JSONArray_t4024675823 * ___lhs0, JSONArray_t4024675823 * ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONArray_op_Addition_m1422077166_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONArray_t4024675823 * V_0 = NULL;
	JSONValue_t4275860644 * V_1 = NULL;
	Enumerator_t3342211967  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		JSONArray_t4024675823 * L_0 = ___lhs0;
		JSONArray_t4024675823 * L_1 = (JSONArray_t4024675823 *)il2cpp_codegen_object_new(JSONArray_t4024675823_il2cpp_TypeInfo_var);
		JSONArray__ctor_m4099340829(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONArray_t4024675823 * L_2 = ___rhs1;
		NullCheck(L_2);
		List_1_t1452968090 * L_3 = L_2->get_values_0();
		NullCheck(L_3);
		Enumerator_t3342211967  L_4 = List_1_GetEnumerator_m3485391654(L_3, /*hidden argument*/List_1_GetEnumerator_m3485391654_RuntimeMethod_var);
		V_2 = L_4;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0027;
		}

IL_0018:
		{
			JSONValue_t4275860644 * L_5 = Enumerator_get_Current_m976429252((Enumerator_t3342211967 *)(&V_2), /*hidden argument*/Enumerator_get_Current_m976429252_RuntimeMethod_var);
			V_1 = L_5;
			JSONArray_t4024675823 * L_6 = V_0;
			JSONValue_t4275860644 * L_7 = V_1;
			NullCheck(L_6);
			JSONArray_Add_m1346855851(L_6, L_7, /*hidden argument*/NULL);
		}

IL_0027:
		{
			bool L_8 = Enumerator_MoveNext_m4065387915((Enumerator_t3342211967 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m4065387915_RuntimeMethod_var);
			if (L_8)
			{
				goto IL_0018;
			}
		}

IL_0033:
		{
			IL2CPP_LEAVE(0x46, FINALLY_0038);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0038;
	}

FINALLY_0038:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3644548256((Enumerator_t3342211967 *)(&V_2), /*hidden argument*/Enumerator_Dispose_m3644548256_RuntimeMethod_var);
		IL2CPP_END_FINALLY(56)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(56)
	{
		IL2CPP_JUMP_TBL(0x46, IL_0046)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0046:
	{
		JSONArray_t4024675823 * L_9 = V_0;
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.JSON.JSONObject::.ctor()
extern "C" IL2CPP_METHOD_ATTR void JSONObject__ctor_m3436253305 (JSONObject_t321714843 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__ctor_m3436253305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t4061116943 * L_0 = (Dictionary_2_t4061116943 *)il2cpp_codegen_object_new(Dictionary_2_t4061116943_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m691717352(L_0, /*hidden argument*/Dictionary_2__ctor_m691717352_RuntimeMethod_var);
		__this->set_values_0(L_0);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONObject::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void JSONObject__ctor_m2883999366 (JSONObject_t321714843 * __this, JSONObject_t321714843 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__ctor_m2883999366_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2163821814  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RuntimeObject* V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t4061116943 * L_0 = (Dictionary_2_t4061116943 *)il2cpp_codegen_object_new(Dictionary_2_t4061116943_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m691717352(L_0, /*hidden argument*/Dictionary_2__ctor_m691717352_RuntimeMethod_var);
		__this->set_values_0(L_0);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		Dictionary_2_t4061116943 * L_1 = (Dictionary_2_t4061116943 *)il2cpp_codegen_object_new(Dictionary_2_t4061116943_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m691717352(L_1, /*hidden argument*/Dictionary_2__ctor_m691717352_RuntimeMethod_var);
		__this->set_values_0(L_1);
		JSONObject_t321714843 * L_2 = ___other0;
		if (!L_2)
		{
			goto IL_0075;
		}
	}
	{
		JSONObject_t321714843 * L_3 = ___other0;
		NullCheck(L_3);
		RuntimeObject* L_4 = L_3->get_values_0();
		NullCheck(L_4);
		RuntimeObject* L_5 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,DigitsNFCToolkit.JSON.JSONValue>>::GetEnumerator() */, IEnumerable_1_t1143674703_il2cpp_TypeInfo_var, L_4);
		V_1 = L_5;
	}

IL_002e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0058;
		}

IL_0033:
		{
			RuntimeObject* L_6 = V_1;
			NullCheck(L_6);
			KeyValuePair_2_t2163821814  L_7 = InterfaceFuncInvoker0< KeyValuePair_2_t2163821814  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,DigitsNFCToolkit.JSON.JSONValue>>::get_Current() */, IEnumerator_1_t2596392282_il2cpp_TypeInfo_var, L_6);
			V_0 = L_7;
			RuntimeObject* L_8 = __this->get_values_0();
			String_t* L_9 = KeyValuePair_2_get_Key_m1640885509((KeyValuePair_2_t2163821814 *)(&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m1640885509_RuntimeMethod_var);
			JSONValue_t4275860644 * L_10 = KeyValuePair_2_get_Value_m1594442022((KeyValuePair_2_t2163821814 *)(&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1594442022_RuntimeMethod_var);
			JSONValue_t4275860644 * L_11 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
			JSONValue__ctor_m1222737883(L_11, L_10, /*hidden argument*/NULL);
			NullCheck(L_8);
			InterfaceActionInvoker2< String_t*, JSONValue_t4275860644 * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t2524968334_il2cpp_TypeInfo_var, L_8, L_9, L_11);
		}

IL_0058:
		{
			RuntimeObject* L_12 = V_1;
			NullCheck(L_12);
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_12);
			if (L_13)
			{
				goto IL_0033;
			}
		}

IL_0063:
		{
			IL2CPP_LEAVE(0x75, FINALLY_0068);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0068;
	}

FINALLY_0068:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_14 = V_1;
			if (!L_14)
			{
				goto IL_0074;
			}
		}

IL_006e:
		{
			RuntimeObject* L_15 = V_1;
			NullCheck(L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_15);
		}

IL_0074:
		{
			IL2CPP_END_FINALLY(104)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(104)
	{
		IL2CPP_JUMP_TBL(0x75, IL_0075)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0075:
	{
		return;
	}
}
// System.Boolean DigitsNFCToolkit.JSON.JSONObject::ContainsKey(System.String)
extern "C" IL2CPP_METHOD_ATTR bool JSONObject_ContainsKey_m2085965128 (JSONObject_t321714843 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_ContainsKey_m2085965128_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_values_0();
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::ContainsKey(!0) */, IDictionary_2_t2524968334_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONObject::GetValue(System.String)
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONObject_GetValue_m193839943 (JSONObject_t321714843 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetValue_m193839943_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONValue_t4275860644 * V_0 = NULL;
	{
		RuntimeObject* L_0 = __this->get_values_0();
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		InterfaceFuncInvoker2< bool, String_t*, JSONValue_t4275860644 ** >::Invoke(6 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::TryGetValue(!0,!1&) */, IDictionary_2_t2524968334_il2cpp_TypeInfo_var, L_0, L_1, (JSONValue_t4275860644 **)(&V_0));
		JSONValue_t4275860644 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.Generic.ICollection`1<System.String> DigitsNFCToolkit.JSON.JSONObject::GetKeys()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* JSONObject_GetKeys_m698843428 (JSONObject_t321714843 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetKeys_m698843428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_values_0();
		NullCheck(L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* System.Collections.Generic.ICollection`1<!0> System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::get_Keys() */, IDictionary_2_t2524968334_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.String DigitsNFCToolkit.JSON.JSONObject::GetString(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* JSONObject_GetString_m3932807855 (JSONObject_t321714843 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetString_m3932807855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONValue_t4275860644 * V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		JSONValue_t4275860644 * L_1 = JSONObject_GetValue_m193839943(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONValue_t4275860644 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		String_t* L_3 = ___key0;
		String_t* L_4 = String_Concat_m3937257545(NULL /*static, unused*/, L_3, _stringLiteral2477980183, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		return L_5;
	}

IL_0024:
	{
		JSONValue_t4275860644 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = JSONValue_get_String_m3887450814(L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Boolean DigitsNFCToolkit.JSON.JSONObject::TryGetString(System.String,System.String&)
extern "C" IL2CPP_METHOD_ATTR bool JSONObject_TryGetString_m766921647 (JSONObject_t321714843 * __this, String_t* ___key0, String_t** ___stringValue1, const RuntimeMethod* method)
{
	JSONValue_t4275860644 * V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		JSONValue_t4275860644 * L_1 = JSONObject_GetValue_m193839943(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONValue_t4275860644 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		String_t** L_3 = ___stringValue1;
		JSONValue_t4275860644 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = JSONValue_get_String_m3887450814(L_4, /*hidden argument*/NULL);
		*((RuntimeObject **)L_3) = (RuntimeObject *)L_5;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_3, (RuntimeObject *)L_5);
		return (bool)1;
	}

IL_0018:
	{
		String_t** L_6 = ___stringValue1;
		*((RuntimeObject **)L_6) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_6, (RuntimeObject *)NULL);
		return (bool)0;
	}
}
// System.Double DigitsNFCToolkit.JSON.JSONObject::GetNumber(System.String)
extern "C" IL2CPP_METHOD_ATTR double JSONObject_GetNumber_m2560286787 (JSONObject_t321714843 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetNumber_m2560286787_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONValue_t4275860644 * V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		JSONValue_t4275860644 * L_1 = JSONObject_GetValue_m193839943(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONValue_t4275860644 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_3 = ___key0;
		String_t* L_4 = String_Concat_m3937257545(NULL /*static, unused*/, L_3, _stringLiteral4182101087, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return (std::numeric_limits<double>::quiet_NaN());
	}

IL_0028:
	{
		JSONValue_t4275860644 * L_5 = V_0;
		NullCheck(L_5);
		double L_6 = JSONValue_get_Double_m3385163784(L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean DigitsNFCToolkit.JSON.JSONObject::TryGetDouble(System.String,System.Double&)
extern "C" IL2CPP_METHOD_ATTR bool JSONObject_TryGetDouble_m486415091 (JSONObject_t321714843 * __this, String_t* ___key0, double* ___doubleValue1, const RuntimeMethod* method)
{
	JSONValue_t4275860644 * V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		JSONValue_t4275860644 * L_1 = JSONObject_GetValue_m193839943(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONValue_t4275860644 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		double* L_3 = ___doubleValue1;
		JSONValue_t4275860644 * L_4 = V_0;
		NullCheck(L_4);
		double L_5 = JSONValue_get_Double_m3385163784(L_4, /*hidden argument*/NULL);
		*((double*)L_3) = (double)L_5;
		return (bool)1;
	}

IL_0018:
	{
		double* L_6 = ___doubleValue1;
		*((double*)L_6) = (double)(0.0);
		return (bool)0;
	}
}
// System.Boolean DigitsNFCToolkit.JSON.JSONObject::TryGetFloat(System.String,System.Single&)
extern "C" IL2CPP_METHOD_ATTR bool JSONObject_TryGetFloat_m401017672 (JSONObject_t321714843 * __this, String_t* ___key0, float* ___floatValue1, const RuntimeMethod* method)
{
	JSONValue_t4275860644 * V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		JSONValue_t4275860644 * L_1 = JSONObject_GetValue_m193839943(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONValue_t4275860644 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		float* L_3 = ___floatValue1;
		JSONValue_t4275860644 * L_4 = V_0;
		NullCheck(L_4);
		float L_5 = JSONValue_get_Float_m2758114374(L_4, /*hidden argument*/NULL);
		*((float*)L_3) = (float)L_5;
		return (bool)1;
	}

IL_0018:
	{
		float* L_6 = ___floatValue1;
		*((float*)L_6) = (float)(0.0f);
		return (bool)0;
	}
}
// System.Boolean DigitsNFCToolkit.JSON.JSONObject::TryGetInt(System.String,System.Int32&)
extern "C" IL2CPP_METHOD_ATTR bool JSONObject_TryGetInt_m2083265671 (JSONObject_t321714843 * __this, String_t* ___key0, int32_t* ___intValue1, const RuntimeMethod* method)
{
	JSONValue_t4275860644 * V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		JSONValue_t4275860644 * L_1 = JSONObject_GetValue_m193839943(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONValue_t4275860644 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		int32_t* L_3 = ___intValue1;
		JSONValue_t4275860644 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = JSONValue_get_Integer_m3463426723(L_4, /*hidden argument*/NULL);
		*((int32_t*)L_3) = (int32_t)L_5;
		return (bool)1;
	}

IL_0018:
	{
		int32_t* L_6 = ___intValue1;
		*((int32_t*)L_6) = (int32_t)0;
		return (bool)0;
	}
}
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.JSON.JSONObject::GetObject(System.String)
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * JSONObject_GetObject_m1756406420 (JSONObject_t321714843 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetObject_m1756406420_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONValue_t4275860644 * V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		JSONValue_t4275860644 * L_1 = JSONObject_GetValue_m193839943(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONValue_t4275860644 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_3 = ___key0;
		String_t* L_4 = String_Concat_m3937257545(NULL /*static, unused*/, L_3, _stringLiteral4182101087, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return (JSONObject_t321714843 *)NULL;
	}

IL_0020:
	{
		JSONValue_t4275860644 * L_5 = V_0;
		NullCheck(L_5);
		JSONObject_t321714843 * L_6 = JSONValue_get_Object_m3828143293(L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean DigitsNFCToolkit.JSON.JSONObject::TryGetObject(System.String,DigitsNFCToolkit.JSON.JSONObject&)
extern "C" IL2CPP_METHOD_ATTR bool JSONObject_TryGetObject_m2604600723 (JSONObject_t321714843 * __this, String_t* ___key0, JSONObject_t321714843 ** ___objectValue1, const RuntimeMethod* method)
{
	JSONValue_t4275860644 * V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		JSONValue_t4275860644 * L_1 = JSONObject_GetValue_m193839943(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONValue_t4275860644 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		JSONObject_t321714843 ** L_3 = ___objectValue1;
		JSONValue_t4275860644 * L_4 = V_0;
		NullCheck(L_4);
		JSONObject_t321714843 * L_5 = JSONValue_get_Object_m3828143293(L_4, /*hidden argument*/NULL);
		*((RuntimeObject **)L_3) = (RuntimeObject *)L_5;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_3, (RuntimeObject *)L_5);
		return (bool)1;
	}

IL_0018:
	{
		JSONObject_t321714843 ** L_6 = ___objectValue1;
		*((RuntimeObject **)L_6) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_6, (RuntimeObject *)NULL);
		return (bool)0;
	}
}
// System.Boolean DigitsNFCToolkit.JSON.JSONObject::GetBoolean(System.String)
extern "C" IL2CPP_METHOD_ATTR bool JSONObject_GetBoolean_m3047886088 (JSONObject_t321714843 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetBoolean_m3047886088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONValue_t4275860644 * V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		JSONValue_t4275860644 * L_1 = JSONObject_GetValue_m193839943(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONValue_t4275860644 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_3 = ___key0;
		String_t* L_4 = String_Concat_m3937257545(NULL /*static, unused*/, L_3, _stringLiteral4182101087, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0020:
	{
		JSONValue_t4275860644 * L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = JSONValue_get_Boolean_m2734736792(L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean DigitsNFCToolkit.JSON.JSONObject::TryGetBoolean(System.String,System.Boolean&)
extern "C" IL2CPP_METHOD_ATTR bool JSONObject_TryGetBoolean_m3739980323 (JSONObject_t321714843 * __this, String_t* ___key0, bool* ___boolValue1, const RuntimeMethod* method)
{
	JSONValue_t4275860644 * V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		JSONValue_t4275860644 * L_1 = JSONObject_GetValue_m193839943(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONValue_t4275860644 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		bool* L_3 = ___boolValue1;
		JSONValue_t4275860644 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = JSONValue_get_Boolean_m2734736792(L_4, /*hidden argument*/NULL);
		*((int8_t*)L_3) = (int8_t)L_5;
		return (bool)1;
	}

IL_0018:
	{
		bool* L_6 = ___boolValue1;
		*((int8_t*)L_6) = (int8_t)0;
		return (bool)0;
	}
}
// DigitsNFCToolkit.JSON.JSONArray DigitsNFCToolkit.JSON.JSONObject::GetArray(System.String)
extern "C" IL2CPP_METHOD_ATTR JSONArray_t4024675823 * JSONObject_GetArray_m3732897099 (JSONObject_t321714843 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetArray_m3732897099_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONValue_t4275860644 * V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		JSONValue_t4275860644 * L_1 = JSONObject_GetValue_m193839943(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONValue_t4275860644 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_3 = ___key0;
		String_t* L_4 = String_Concat_m3937257545(NULL /*static, unused*/, L_3, _stringLiteral4182101087, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return (JSONArray_t4024675823 *)NULL;
	}

IL_0020:
	{
		JSONValue_t4275860644 * L_5 = V_0;
		NullCheck(L_5);
		JSONArray_t4024675823 * L_6 = JSONValue_get_Array_m3632598190(L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean DigitsNFCToolkit.JSON.JSONObject::TryGetArray(System.String,DigitsNFCToolkit.JSON.JSONArray&)
extern "C" IL2CPP_METHOD_ATTR bool JSONObject_TryGetArray_m3928523712 (JSONObject_t321714843 * __this, String_t* ___key0, JSONArray_t4024675823 ** ___arrayValue1, const RuntimeMethod* method)
{
	JSONValue_t4275860644 * V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		JSONValue_t4275860644 * L_1 = JSONObject_GetValue_m193839943(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONValue_t4275860644 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		JSONArray_t4024675823 ** L_3 = ___arrayValue1;
		JSONValue_t4275860644 * L_4 = V_0;
		NullCheck(L_4);
		JSONArray_t4024675823 * L_5 = JSONValue_get_Array_m3632598190(L_4, /*hidden argument*/NULL);
		*((RuntimeObject **)L_3) = (RuntimeObject *)L_5;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_3, (RuntimeObject *)L_5);
		return (bool)1;
	}

IL_0018:
	{
		JSONArray_t4024675823 ** L_6 = ___arrayValue1;
		*((RuntimeObject **)L_6) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_6, (RuntimeObject *)NULL);
		return (bool)0;
	}
}
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONObject::get_Item(System.String)
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONObject_get_Item_m3915825514 (JSONObject_t321714843 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___key0;
		JSONValue_t4275860644 * L_1 = JSONObject_GetValue_m193839943(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONObject::set_Item(System.String,DigitsNFCToolkit.JSON.JSONValue)
extern "C" IL2CPP_METHOD_ATTR void JSONObject_set_Item_m3960241132 (JSONObject_t321714843 * __this, String_t* ___key0, JSONValue_t4275860644 * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_set_Item_m3960241132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_values_0();
		String_t* L_1 = ___key0;
		JSONValue_t4275860644 * L_2 = ___value1;
		NullCheck(L_0);
		InterfaceActionInvoker2< String_t*, JSONValue_t4275860644 * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t2524968334_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONObject::Add(System.String,DigitsNFCToolkit.JSON.JSONValue)
extern "C" IL2CPP_METHOD_ATTR void JSONObject_Add_m2412535806 (JSONObject_t321714843 * __this, String_t* ___key0, JSONValue_t4275860644 * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m2412535806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_values_0();
		String_t* L_1 = ___key0;
		JSONValue_t4275860644 * L_2 = ___value1;
		NullCheck(L_0);
		InterfaceActionInvoker2< String_t*, JSONValue_t4275860644 * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t2524968334_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONObject::Add(System.Collections.Generic.KeyValuePair`2<System.String,DigitsNFCToolkit.JSON.JSONValue>)
extern "C" IL2CPP_METHOD_ATTR void JSONObject_Add_m177659171 (JSONObject_t321714843 * __this, KeyValuePair_2_t2163821814  ___pair0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m177659171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_values_0();
		String_t* L_1 = KeyValuePair_2_get_Key_m1640885509((KeyValuePair_2_t2163821814 *)(&___pair0), /*hidden argument*/KeyValuePair_2_get_Key_m1640885509_RuntimeMethod_var);
		JSONValue_t4275860644 * L_2 = KeyValuePair_2_get_Value_m1594442022((KeyValuePair_2_t2163821814 *)(&___pair0), /*hidden argument*/KeyValuePair_2_get_Value_m1594442022_RuntimeMethod_var);
		NullCheck(L_0);
		InterfaceActionInvoker2< String_t*, JSONValue_t4275860644 * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t2524968334_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.JSON.JSONObject::Parse(System.String)
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * JSONObject_Parse_m2486419426 (RuntimeObject * __this /* static, unused */, String_t* ___jsonString0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Parse_m2486419426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONValue_t4275860644 * V_0 = NULL;
	Stack_1_t2690840144 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	JSONValue_t4275860644 * V_4 = NULL;
	int32_t V_5 = 0;
	String_t* V_6 = NULL;
	Il2CppChar V_7 = 0x0;
	Il2CppChar V_8 = 0x0;
	String_t* V_9 = NULL;
	int32_t V_10 = 0;
	double V_11 = 0.0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	JSONValue_t4275860644 * V_15 = NULL;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t G_B36_0 = 0;
	{
		String_t* L_0 = ___jsonString0;
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (JSONObject_t321714843 *)NULL;
	}

IL_000d:
	{
		V_0 = (JSONValue_t4275860644 *)NULL;
		Stack_1_t2690840144 * L_2 = (Stack_1_t2690840144 *)il2cpp_codegen_object_new(Stack_1_t2690840144_il2cpp_TypeInfo_var);
		Stack_1__ctor_m2770587905(L_2, /*hidden argument*/Stack_1__ctor_m2770587905_RuntimeMethod_var);
		V_1 = L_2;
		V_2 = 0;
		V_3 = 0;
		goto IL_072d;
	}

IL_001e:
	{
		String_t* L_3 = ___jsonString0;
		int32_t L_4 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		int32_t L_5 = JSONObject_SkipWhitespace_m1442147541(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		int32_t L_6 = V_2;
		switch (L_6)
		{
			case 0:
			{
				goto IL_0061;
			}
			case 1:
			{
				goto IL_057b;
			}
			case 2:
			{
				goto IL_009c;
			}
			case 3:
			{
				goto IL_05b6;
			}
			case 4:
			{
				goto IL_014c;
			}
			case 5:
			{
				goto IL_0218;
			}
			case 6:
			{
				goto IL_0191;
			}
			case 7:
			{
				goto IL_01af;
			}
			case 8:
			{
				goto IL_02e4;
			}
			case 9:
			{
				goto IL_036a;
			}
			case 10:
			{
				goto IL_03f5;
			}
			case 11:
			{
				goto IL_0666;
			}
		}
	}
	{
		goto IL_0729;
	}

IL_0061:
	{
		String_t* L_7 = ___jsonString0;
		int32_t L_8 = V_3;
		NullCheck(L_7);
		Il2CppChar L_9 = String_get_Chars_m2986988803(L_7, L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)((int32_t)123))))
		{
			goto IL_0078;
		}
	}
	{
		int32_t L_10 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_11 = JSONObject_Fail_m1137956452(NULL /*static, unused*/, ((int32_t)123), L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0078:
	{
		JSONObject_t321714843 * L_12 = (JSONObject_t321714843 *)il2cpp_codegen_object_new(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject__ctor_m3436253305(L_12, /*hidden argument*/NULL);
		JSONValue_t4275860644 * L_13 = JSONValue_op_Implicit_m1136109141(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		V_4 = L_13;
		JSONValue_t4275860644 * L_14 = V_0;
		if (!L_14)
		{
			goto IL_0092;
		}
	}
	{
		JSONValue_t4275860644 * L_15 = V_4;
		JSONValue_t4275860644 * L_16 = V_0;
		NullCheck(L_15);
		JSONValue_set_Parent_m1935092005(L_15, L_16, /*hidden argument*/NULL);
	}

IL_0092:
	{
		JSONValue_t4275860644 * L_17 = V_4;
		V_0 = L_17;
		V_2 = 4;
		goto IL_0729;
	}

IL_009c:
	{
		String_t* L_18 = ___jsonString0;
		int32_t L_19 = V_3;
		NullCheck(L_18);
		Il2CppChar L_20 = String_get_Chars_m2986988803(L_18, L_19, /*hidden argument*/NULL);
		if ((((int32_t)L_20) == ((int32_t)((int32_t)125))))
		{
			goto IL_00b3;
		}
	}
	{
		int32_t L_21 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_22 = JSONObject_Fail_m1137956452(NULL /*static, unused*/, ((int32_t)125), L_21, /*hidden argument*/NULL);
		return L_22;
	}

IL_00b3:
	{
		JSONValue_t4275860644 * L_23 = V_0;
		NullCheck(L_23);
		JSONValue_t4275860644 * L_24 = JSONValue_get_Parent_m2676443687(L_23, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_00c5;
		}
	}
	{
		JSONValue_t4275860644 * L_25 = V_0;
		NullCheck(L_25);
		JSONObject_t321714843 * L_26 = JSONValue_get_Object_m3828143293(L_25, /*hidden argument*/NULL);
		return L_26;
	}

IL_00c5:
	{
		JSONValue_t4275860644 * L_27 = V_0;
		NullCheck(L_27);
		JSONValue_t4275860644 * L_28 = JSONValue_get_Parent_m2676443687(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		int32_t L_29 = JSONValue_get_Type_m3765076585(L_28, /*hidden argument*/NULL);
		V_5 = L_29;
		int32_t L_30 = V_5;
		if ((((int32_t)L_30) == ((int32_t)2)))
		{
			goto IL_00e7;
		}
	}
	{
		int32_t L_31 = V_5;
		if ((((int32_t)L_31) == ((int32_t)3)))
		{
			goto IL_0112;
		}
	}
	{
		goto IL_0132;
	}

IL_00e7:
	{
		JSONValue_t4275860644 * L_32 = V_0;
		NullCheck(L_32);
		JSONValue_t4275860644 * L_33 = JSONValue_get_Parent_m2676443687(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		JSONObject_t321714843 * L_34 = JSONValue_get_Object_m3828143293(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		RuntimeObject* L_35 = L_34->get_values_0();
		Stack_1_t2690840144 * L_36 = V_1;
		NullCheck(L_36);
		String_t* L_37 = Stack_1_Pop_m513619580(L_36, /*hidden argument*/Stack_1_Pop_m513619580_RuntimeMethod_var);
		JSONValue_t4275860644 * L_38 = V_0;
		NullCheck(L_38);
		JSONObject_t321714843 * L_39 = JSONValue_get_Object_m3828143293(L_38, /*hidden argument*/NULL);
		JSONValue_t4275860644 * L_40 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
		JSONValue__ctor_m3984902601(L_40, L_39, /*hidden argument*/NULL);
		NullCheck(L_35);
		InterfaceActionInvoker2< String_t*, JSONValue_t4275860644 * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t2524968334_il2cpp_TypeInfo_var, L_35, L_37, L_40);
		goto IL_013e;
	}

IL_0112:
	{
		JSONValue_t4275860644 * L_41 = V_0;
		NullCheck(L_41);
		JSONValue_t4275860644 * L_42 = JSONValue_get_Parent_m2676443687(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		JSONArray_t4024675823 * L_43 = JSONValue_get_Array_m3632598190(L_42, /*hidden argument*/NULL);
		JSONValue_t4275860644 * L_44 = V_0;
		NullCheck(L_44);
		JSONObject_t321714843 * L_45 = JSONValue_get_Object_m3828143293(L_44, /*hidden argument*/NULL);
		JSONValue_t4275860644 * L_46 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
		JSONValue__ctor_m3984902601(L_46, L_45, /*hidden argument*/NULL);
		NullCheck(L_43);
		JSONArray_Add_m1346855851(L_43, L_46, /*hidden argument*/NULL);
		goto IL_013e;
	}

IL_0132:
	{
		int32_t L_47 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_48 = JSONObject_Fail_m3595512628(NULL /*static, unused*/, _stringLiteral4012561394, L_47, /*hidden argument*/NULL);
		return L_48;
	}

IL_013e:
	{
		JSONValue_t4275860644 * L_49 = V_0;
		NullCheck(L_49);
		JSONValue_t4275860644 * L_50 = JSONValue_get_Parent_m2676443687(L_49, /*hidden argument*/NULL);
		V_0 = L_50;
		V_2 = 7;
		goto IL_0729;
	}

IL_014c:
	{
		String_t* L_51 = ___jsonString0;
		int32_t L_52 = V_3;
		NullCheck(L_51);
		Il2CppChar L_53 = String_get_Chars_m2986988803(L_51, L_52, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_53) == ((uint32_t)((int32_t)125)))))
		{
			goto IL_0165;
		}
	}
	{
		int32_t L_54 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_54, (int32_t)1));
		V_2 = 2;
		goto IL_0729;
	}

IL_0165:
	{
		String_t* L_55 = ___jsonString0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		String_t* L_56 = JSONObject_ParseString_m1617168844(NULL /*static, unused*/, L_55, (int32_t*)(&V_3), /*hidden argument*/NULL);
		V_6 = L_56;
		String_t* L_57 = V_6;
		if (L_57)
		{
			goto IL_0182;
		}
	}
	{
		int32_t L_58 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_59 = JSONObject_Fail_m3595512628(NULL /*static, unused*/, _stringLiteral4087621358, L_58, /*hidden argument*/NULL);
		return L_59;
	}

IL_0182:
	{
		Stack_1_t2690840144 * L_60 = V_1;
		String_t* L_61 = V_6;
		NullCheck(L_60);
		Stack_1_Push_m43534404(L_60, L_61, /*hidden argument*/Stack_1_Push_m43534404_RuntimeMethod_var);
		V_2 = 6;
		goto IL_0729;
	}

IL_0191:
	{
		String_t* L_62 = ___jsonString0;
		int32_t L_63 = V_3;
		NullCheck(L_62);
		Il2CppChar L_64 = String_get_Chars_m2986988803(L_62, L_63, /*hidden argument*/NULL);
		if ((((int32_t)L_64) == ((int32_t)((int32_t)58))))
		{
			goto IL_01a8;
		}
	}
	{
		int32_t L_65 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_66 = JSONObject_Fail_m1137956452(NULL /*static, unused*/, ((int32_t)58), L_65, /*hidden argument*/NULL);
		return L_66;
	}

IL_01a8:
	{
		V_2 = 5;
		goto IL_0729;
	}

IL_01af:
	{
		String_t* L_67 = ___jsonString0;
		int32_t L_68 = V_3;
		NullCheck(L_67);
		Il2CppChar L_69 = String_get_Chars_m2986988803(L_67, L_68, /*hidden argument*/NULL);
		V_7 = L_69;
		Il2CppChar L_70 = V_7;
		if ((((int32_t)L_70) == ((int32_t)((int32_t)44))))
		{
			goto IL_01d8;
		}
	}
	{
		Il2CppChar L_71 = V_7;
		if ((((int32_t)L_71) == ((int32_t)((int32_t)93))))
		{
			goto IL_01fc;
		}
	}
	{
		Il2CppChar L_72 = V_7;
		if ((((int32_t)L_72) == ((int32_t)((int32_t)125))))
		{
			goto IL_01f1;
		}
	}
	{
		goto IL_0207;
	}

IL_01d8:
	{
		JSONValue_t4275860644 * L_73 = V_0;
		NullCheck(L_73);
		int32_t L_74 = JSONValue_get_Type_m3765076585(L_73, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_74) == ((uint32_t)2))))
		{
			goto IL_01ea;
		}
	}
	{
		G_B36_0 = 4;
		goto IL_01eb;
	}

IL_01ea:
	{
		G_B36_0 = 5;
	}

IL_01eb:
	{
		V_2 = G_B36_0;
		goto IL_0213;
	}

IL_01f1:
	{
		V_2 = 2;
		int32_t L_75 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_75, (int32_t)1));
		goto IL_0213;
	}

IL_01fc:
	{
		V_2 = 3;
		int32_t L_76 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_76, (int32_t)1));
		goto IL_0213;
	}

IL_0207:
	{
		int32_t L_77 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_78 = JSONObject_Fail_m3595512628(NULL /*static, unused*/, _stringLiteral1773568958, L_77, /*hidden argument*/NULL);
		return L_78;
	}

IL_0213:
	{
		goto IL_0729;
	}

IL_0218:
	{
		String_t* L_79 = ___jsonString0;
		int32_t L_80 = V_3;
		NullCheck(L_79);
		Il2CppChar L_81 = String_get_Chars_m2986988803(L_79, L_80, /*hidden argument*/NULL);
		V_8 = L_81;
		Il2CppChar L_82 = V_8;
		if ((!(((uint32_t)L_82) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0231;
		}
	}
	{
		V_2 = 8;
		goto IL_02db;
	}

IL_0231:
	{
		Il2CppChar L_83 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_84 = Char_IsDigit_m3646673943(NULL /*static, unused*/, L_83, /*hidden argument*/NULL);
		if (L_84)
		{
			goto IL_0246;
		}
	}
	{
		Il2CppChar L_85 = V_8;
		if ((!(((uint32_t)L_85) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_024e;
		}
	}

IL_0246:
	{
		V_2 = ((int32_t)9);
		goto IL_02db;
	}

IL_024e:
	{
		Il2CppChar L_86 = V_8;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_86, (int32_t)((int32_t)91))))
		{
			case 0:
			{
				goto IL_0294;
			}
			case 1:
			{
				goto IL_0264;
			}
			case 2:
			{
				goto IL_029b;
			}
		}
	}

IL_0264:
	{
		Il2CppChar L_87 = V_8;
		if ((((int32_t)L_87) == ((int32_t)((int32_t)102))))
		{
			goto IL_02bf;
		}
	}
	{
		Il2CppChar L_88 = V_8;
		if ((((int32_t)L_88) == ((int32_t)((int32_t)110))))
		{
			goto IL_02c7;
		}
	}
	{
		Il2CppChar L_89 = V_8;
		if ((((int32_t)L_89) == ((int32_t)((int32_t)116))))
		{
			goto IL_02bf;
		}
	}
	{
		Il2CppChar L_90 = V_8;
		if ((((int32_t)L_90) == ((int32_t)((int32_t)123))))
		{
			goto IL_028d;
		}
	}
	{
		goto IL_02cf;
	}

IL_028d:
	{
		V_2 = 0;
		goto IL_02db;
	}

IL_0294:
	{
		V_2 = 1;
		goto IL_02db;
	}

IL_029b:
	{
		JSONValue_t4275860644 * L_91 = V_0;
		NullCheck(L_91);
		int32_t L_92 = JSONValue_get_Type_m3765076585(L_91, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_92) == ((uint32_t)3))))
		{
			goto IL_02ae;
		}
	}
	{
		V_2 = 3;
		goto IL_02ba;
	}

IL_02ae:
	{
		int32_t L_93 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_94 = JSONObject_Fail_m3595512628(NULL /*static, unused*/, _stringLiteral1780417286, L_93, /*hidden argument*/NULL);
		return L_94;
	}

IL_02ba:
	{
		goto IL_02db;
	}

IL_02bf:
	{
		V_2 = ((int32_t)10);
		goto IL_02db;
	}

IL_02c7:
	{
		V_2 = ((int32_t)11);
		goto IL_02db;
	}

IL_02cf:
	{
		int32_t L_95 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_96 = JSONObject_Fail_m3595512628(NULL /*static, unused*/, _stringLiteral3779069074, L_95, /*hidden argument*/NULL);
		return L_96;
	}

IL_02db:
	{
		int32_t L_97 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_97, (int32_t)1));
		goto IL_0729;
	}

IL_02e4:
	{
		String_t* L_98 = ___jsonString0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		String_t* L_99 = JSONObject_ParseString_m1617168844(NULL /*static, unused*/, L_98, (int32_t*)(&V_3), /*hidden argument*/NULL);
		V_9 = L_99;
		String_t* L_100 = V_9;
		if (L_100)
		{
			goto IL_0301;
		}
	}
	{
		int32_t L_101 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_102 = JSONObject_Fail_m3595512628(NULL /*static, unused*/, _stringLiteral2075513268, L_101, /*hidden argument*/NULL);
		return L_102;
	}

IL_0301:
	{
		JSONValue_t4275860644 * L_103 = V_0;
		NullCheck(L_103);
		int32_t L_104 = JSONValue_get_Type_m3765076585(L_103, /*hidden argument*/NULL);
		V_10 = L_104;
		int32_t L_105 = V_10;
		if ((((int32_t)L_105) == ((int32_t)2)))
		{
			goto IL_031e;
		}
	}
	{
		int32_t L_106 = V_10;
		if ((((int32_t)L_106) == ((int32_t)3)))
		{
			goto IL_0340;
		}
	}
	{
		goto IL_0357;
	}

IL_031e:
	{
		JSONValue_t4275860644 * L_107 = V_0;
		NullCheck(L_107);
		JSONObject_t321714843 * L_108 = JSONValue_get_Object_m3828143293(L_107, /*hidden argument*/NULL);
		NullCheck(L_108);
		RuntimeObject* L_109 = L_108->get_values_0();
		Stack_1_t2690840144 * L_110 = V_1;
		NullCheck(L_110);
		String_t* L_111 = Stack_1_Pop_m513619580(L_110, /*hidden argument*/Stack_1_Pop_m513619580_RuntimeMethod_var);
		String_t* L_112 = V_9;
		JSONValue_t4275860644 * L_113 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
		JSONValue__ctor_m2817256385(L_113, L_112, /*hidden argument*/NULL);
		NullCheck(L_109);
		InterfaceActionInvoker2< String_t*, JSONValue_t4275860644 * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t2524968334_il2cpp_TypeInfo_var, L_109, L_111, L_113);
		goto IL_0363;
	}

IL_0340:
	{
		JSONValue_t4275860644 * L_114 = V_0;
		NullCheck(L_114);
		JSONArray_t4024675823 * L_115 = JSONValue_get_Array_m3632598190(L_114, /*hidden argument*/NULL);
		String_t* L_116 = V_9;
		JSONValue_t4275860644 * L_117 = JSONValue_op_Implicit_m357672470(NULL /*static, unused*/, L_116, /*hidden argument*/NULL);
		NullCheck(L_115);
		JSONArray_Add_m1346855851(L_115, L_117, /*hidden argument*/NULL);
		goto IL_0363;
	}

IL_0357:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral2793114053, /*hidden argument*/NULL);
		return (JSONObject_t321714843 *)NULL;
	}

IL_0363:
	{
		V_2 = 7;
		goto IL_0729;
	}

IL_036a:
	{
		String_t* L_118 = ___jsonString0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		double L_119 = JSONObject_ParseNumber_m2501283307(NULL /*static, unused*/, L_118, (int32_t*)(&V_3), /*hidden argument*/NULL);
		V_11 = L_119;
		double L_120 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(Double_t594665363_il2cpp_TypeInfo_var);
		bool L_121 = Double_IsNaN_m649024406(NULL /*static, unused*/, L_120, /*hidden argument*/NULL);
		if (!L_121)
		{
			goto IL_038c;
		}
	}
	{
		int32_t L_122 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_123 = JSONObject_Fail_m3595512628(NULL /*static, unused*/, _stringLiteral2535715514, L_122, /*hidden argument*/NULL);
		return L_123;
	}

IL_038c:
	{
		JSONValue_t4275860644 * L_124 = V_0;
		NullCheck(L_124);
		int32_t L_125 = JSONValue_get_Type_m3765076585(L_124, /*hidden argument*/NULL);
		V_12 = L_125;
		int32_t L_126 = V_12;
		if ((((int32_t)L_126) == ((int32_t)2)))
		{
			goto IL_03a9;
		}
	}
	{
		int32_t L_127 = V_12;
		if ((((int32_t)L_127) == ((int32_t)3)))
		{
			goto IL_03cb;
		}
	}
	{
		goto IL_03e2;
	}

IL_03a9:
	{
		JSONValue_t4275860644 * L_128 = V_0;
		NullCheck(L_128);
		JSONObject_t321714843 * L_129 = JSONValue_get_Object_m3828143293(L_128, /*hidden argument*/NULL);
		NullCheck(L_129);
		RuntimeObject* L_130 = L_129->get_values_0();
		Stack_1_t2690840144 * L_131 = V_1;
		NullCheck(L_131);
		String_t* L_132 = Stack_1_Pop_m513619580(L_131, /*hidden argument*/Stack_1_Pop_m513619580_RuntimeMethod_var);
		double L_133 = V_11;
		JSONValue_t4275860644 * L_134 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
		JSONValue__ctor_m610419965(L_134, L_133, /*hidden argument*/NULL);
		NullCheck(L_130);
		InterfaceActionInvoker2< String_t*, JSONValue_t4275860644 * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t2524968334_il2cpp_TypeInfo_var, L_130, L_132, L_134);
		goto IL_03ee;
	}

IL_03cb:
	{
		JSONValue_t4275860644 * L_135 = V_0;
		NullCheck(L_135);
		JSONArray_t4024675823 * L_136 = JSONValue_get_Array_m3632598190(L_135, /*hidden argument*/NULL);
		double L_137 = V_11;
		JSONValue_t4275860644 * L_138 = JSONValue_op_Implicit_m3141527758(NULL /*static, unused*/, L_137, /*hidden argument*/NULL);
		NullCheck(L_136);
		JSONArray_Add_m1346855851(L_136, L_138, /*hidden argument*/NULL);
		goto IL_03ee;
	}

IL_03e2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral2793114053, /*hidden argument*/NULL);
		return (JSONObject_t321714843 *)NULL;
	}

IL_03ee:
	{
		V_2 = 7;
		goto IL_0729;
	}

IL_03f5:
	{
		String_t* L_139 = ___jsonString0;
		int32_t L_140 = V_3;
		NullCheck(L_139);
		Il2CppChar L_141 = String_get_Chars_m2986988803(L_139, L_140, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_141) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_04b6;
		}
	}
	{
		String_t* L_142 = ___jsonString0;
		NullCheck(L_142);
		int32_t L_143 = String_get_Length_m3847582255(L_142, /*hidden argument*/NULL);
		int32_t L_144 = V_3;
		if ((((int32_t)L_143) < ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_144, (int32_t)4)))))
		{
			goto IL_0441;
		}
	}
	{
		String_t* L_145 = ___jsonString0;
		int32_t L_146 = V_3;
		NullCheck(L_145);
		Il2CppChar L_147 = String_get_Chars_m2986988803(L_145, ((int32_t)il2cpp_codegen_add((int32_t)L_146, (int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_147) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_0441;
		}
	}
	{
		String_t* L_148 = ___jsonString0;
		int32_t L_149 = V_3;
		NullCheck(L_148);
		Il2CppChar L_150 = String_get_Chars_m2986988803(L_148, ((int32_t)il2cpp_codegen_add((int32_t)L_149, (int32_t)2)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_150) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_0441;
		}
	}
	{
		String_t* L_151 = ___jsonString0;
		int32_t L_152 = V_3;
		NullCheck(L_151);
		Il2CppChar L_153 = String_get_Chars_m2986988803(L_151, ((int32_t)il2cpp_codegen_add((int32_t)L_152, (int32_t)3)), /*hidden argument*/NULL);
		if ((((int32_t)L_153) == ((int32_t)((int32_t)101))))
		{
			goto IL_044d;
		}
	}

IL_0441:
	{
		int32_t L_154 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_155 = JSONObject_Fail_m3595512628(NULL /*static, unused*/, _stringLiteral4002445229, L_154, /*hidden argument*/NULL);
		return L_155;
	}

IL_044d:
	{
		JSONValue_t4275860644 * L_156 = V_0;
		NullCheck(L_156);
		int32_t L_157 = JSONValue_get_Type_m3765076585(L_156, /*hidden argument*/NULL);
		V_13 = L_157;
		int32_t L_158 = V_13;
		if ((((int32_t)L_158) == ((int32_t)2)))
		{
			goto IL_046a;
		}
	}
	{
		int32_t L_159 = V_13;
		if ((((int32_t)L_159) == ((int32_t)3)))
		{
			goto IL_048b;
		}
	}
	{
		goto IL_04a1;
	}

IL_046a:
	{
		JSONValue_t4275860644 * L_160 = V_0;
		NullCheck(L_160);
		JSONObject_t321714843 * L_161 = JSONValue_get_Object_m3828143293(L_160, /*hidden argument*/NULL);
		NullCheck(L_161);
		RuntimeObject* L_162 = L_161->get_values_0();
		Stack_1_t2690840144 * L_163 = V_1;
		NullCheck(L_163);
		String_t* L_164 = Stack_1_Pop_m513619580(L_163, /*hidden argument*/Stack_1_Pop_m513619580_RuntimeMethod_var);
		JSONValue_t4275860644 * L_165 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
		JSONValue__ctor_m31333835(L_165, (bool)1, /*hidden argument*/NULL);
		NullCheck(L_162);
		InterfaceActionInvoker2< String_t*, JSONValue_t4275860644 * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t2524968334_il2cpp_TypeInfo_var, L_162, L_164, L_165);
		goto IL_04ad;
	}

IL_048b:
	{
		JSONValue_t4275860644 * L_166 = V_0;
		NullCheck(L_166);
		JSONArray_t4024675823 * L_167 = JSONValue_get_Array_m3632598190(L_166, /*hidden argument*/NULL);
		JSONValue_t4275860644 * L_168 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
		JSONValue__ctor_m31333835(L_168, (bool)1, /*hidden argument*/NULL);
		NullCheck(L_167);
		JSONArray_Add_m1346855851(L_167, L_168, /*hidden argument*/NULL);
		goto IL_04ad;
	}

IL_04a1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral2793114053, /*hidden argument*/NULL);
		return (JSONObject_t321714843 *)NULL;
	}

IL_04ad:
	{
		int32_t L_169 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_169, (int32_t)3));
		goto IL_0574;
	}

IL_04b6:
	{
		String_t* L_170 = ___jsonString0;
		NullCheck(L_170);
		int32_t L_171 = String_get_Length_m3847582255(L_170, /*hidden argument*/NULL);
		int32_t L_172 = V_3;
		if ((((int32_t)L_171) < ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_172, (int32_t)5)))))
		{
			goto IL_0504;
		}
	}
	{
		String_t* L_173 = ___jsonString0;
		int32_t L_174 = V_3;
		NullCheck(L_173);
		Il2CppChar L_175 = String_get_Chars_m2986988803(L_173, ((int32_t)il2cpp_codegen_add((int32_t)L_174, (int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_175) == ((uint32_t)((int32_t)97)))))
		{
			goto IL_0504;
		}
	}
	{
		String_t* L_176 = ___jsonString0;
		int32_t L_177 = V_3;
		NullCheck(L_176);
		Il2CppChar L_178 = String_get_Chars_m2986988803(L_176, ((int32_t)il2cpp_codegen_add((int32_t)L_177, (int32_t)2)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_178) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0504;
		}
	}
	{
		String_t* L_179 = ___jsonString0;
		int32_t L_180 = V_3;
		NullCheck(L_179);
		Il2CppChar L_181 = String_get_Chars_m2986988803(L_179, ((int32_t)il2cpp_codegen_add((int32_t)L_180, (int32_t)3)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_181) == ((uint32_t)((int32_t)115)))))
		{
			goto IL_0504;
		}
	}
	{
		String_t* L_182 = ___jsonString0;
		int32_t L_183 = V_3;
		NullCheck(L_182);
		Il2CppChar L_184 = String_get_Chars_m2986988803(L_182, ((int32_t)il2cpp_codegen_add((int32_t)L_183, (int32_t)4)), /*hidden argument*/NULL);
		if ((((int32_t)L_184) == ((int32_t)((int32_t)101))))
		{
			goto IL_0510;
		}
	}

IL_0504:
	{
		int32_t L_185 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_186 = JSONObject_Fail_m3595512628(NULL /*static, unused*/, _stringLiteral3875954633, L_185, /*hidden argument*/NULL);
		return L_186;
	}

IL_0510:
	{
		JSONValue_t4275860644 * L_187 = V_0;
		NullCheck(L_187);
		int32_t L_188 = JSONValue_get_Type_m3765076585(L_187, /*hidden argument*/NULL);
		V_14 = L_188;
		int32_t L_189 = V_14;
		if ((((int32_t)L_189) == ((int32_t)2)))
		{
			goto IL_052d;
		}
	}
	{
		int32_t L_190 = V_14;
		if ((((int32_t)L_190) == ((int32_t)3)))
		{
			goto IL_054e;
		}
	}
	{
		goto IL_0564;
	}

IL_052d:
	{
		JSONValue_t4275860644 * L_191 = V_0;
		NullCheck(L_191);
		JSONObject_t321714843 * L_192 = JSONValue_get_Object_m3828143293(L_191, /*hidden argument*/NULL);
		NullCheck(L_192);
		RuntimeObject* L_193 = L_192->get_values_0();
		Stack_1_t2690840144 * L_194 = V_1;
		NullCheck(L_194);
		String_t* L_195 = Stack_1_Pop_m513619580(L_194, /*hidden argument*/Stack_1_Pop_m513619580_RuntimeMethod_var);
		JSONValue_t4275860644 * L_196 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
		JSONValue__ctor_m31333835(L_196, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_193);
		InterfaceActionInvoker2< String_t*, JSONValue_t4275860644 * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t2524968334_il2cpp_TypeInfo_var, L_193, L_195, L_196);
		goto IL_0570;
	}

IL_054e:
	{
		JSONValue_t4275860644 * L_197 = V_0;
		NullCheck(L_197);
		JSONArray_t4024675823 * L_198 = JSONValue_get_Array_m3632598190(L_197, /*hidden argument*/NULL);
		JSONValue_t4275860644 * L_199 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
		JSONValue__ctor_m31333835(L_199, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_198);
		JSONArray_Add_m1346855851(L_198, L_199, /*hidden argument*/NULL);
		goto IL_0570;
	}

IL_0564:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral2793114053, /*hidden argument*/NULL);
		return (JSONObject_t321714843 *)NULL;
	}

IL_0570:
	{
		int32_t L_200 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_200, (int32_t)4));
	}

IL_0574:
	{
		V_2 = 7;
		goto IL_0729;
	}

IL_057b:
	{
		String_t* L_201 = ___jsonString0;
		int32_t L_202 = V_3;
		NullCheck(L_201);
		Il2CppChar L_203 = String_get_Chars_m2986988803(L_201, L_202, /*hidden argument*/NULL);
		if ((((int32_t)L_203) == ((int32_t)((int32_t)91))))
		{
			goto IL_0592;
		}
	}
	{
		int32_t L_204 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_205 = JSONObject_Fail_m1137956452(NULL /*static, unused*/, ((int32_t)91), L_204, /*hidden argument*/NULL);
		return L_205;
	}

IL_0592:
	{
		JSONArray_t4024675823 * L_206 = (JSONArray_t4024675823 *)il2cpp_codegen_object_new(JSONArray_t4024675823_il2cpp_TypeInfo_var);
		JSONArray__ctor_m3612236623(L_206, /*hidden argument*/NULL);
		JSONValue_t4275860644 * L_207 = JSONValue_op_Implicit_m216610386(NULL /*static, unused*/, L_206, /*hidden argument*/NULL);
		V_15 = L_207;
		JSONValue_t4275860644 * L_208 = V_0;
		if (!L_208)
		{
			goto IL_05ac;
		}
	}
	{
		JSONValue_t4275860644 * L_209 = V_15;
		JSONValue_t4275860644 * L_210 = V_0;
		NullCheck(L_209);
		JSONValue_set_Parent_m1935092005(L_209, L_210, /*hidden argument*/NULL);
	}

IL_05ac:
	{
		JSONValue_t4275860644 * L_211 = V_15;
		V_0 = L_211;
		V_2 = 5;
		goto IL_0729;
	}

IL_05b6:
	{
		String_t* L_212 = ___jsonString0;
		int32_t L_213 = V_3;
		NullCheck(L_212);
		Il2CppChar L_214 = String_get_Chars_m2986988803(L_212, L_213, /*hidden argument*/NULL);
		if ((((int32_t)L_214) == ((int32_t)((int32_t)93))))
		{
			goto IL_05cd;
		}
	}
	{
		int32_t L_215 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_216 = JSONObject_Fail_m1137956452(NULL /*static, unused*/, ((int32_t)93), L_215, /*hidden argument*/NULL);
		return L_216;
	}

IL_05cd:
	{
		JSONValue_t4275860644 * L_217 = V_0;
		NullCheck(L_217);
		JSONValue_t4275860644 * L_218 = JSONValue_get_Parent_m2676443687(L_217, /*hidden argument*/NULL);
		if (L_218)
		{
			goto IL_05df;
		}
	}
	{
		JSONValue_t4275860644 * L_219 = V_0;
		NullCheck(L_219);
		JSONObject_t321714843 * L_220 = JSONValue_get_Object_m3828143293(L_219, /*hidden argument*/NULL);
		return L_220;
	}

IL_05df:
	{
		JSONValue_t4275860644 * L_221 = V_0;
		NullCheck(L_221);
		JSONValue_t4275860644 * L_222 = JSONValue_get_Parent_m2676443687(L_221, /*hidden argument*/NULL);
		NullCheck(L_222);
		int32_t L_223 = JSONValue_get_Type_m3765076585(L_222, /*hidden argument*/NULL);
		V_16 = L_223;
		int32_t L_224 = V_16;
		if ((((int32_t)L_224) == ((int32_t)2)))
		{
			goto IL_0601;
		}
	}
	{
		int32_t L_225 = V_16;
		if ((((int32_t)L_225) == ((int32_t)3)))
		{
			goto IL_062c;
		}
	}
	{
		goto IL_064c;
	}

IL_0601:
	{
		JSONValue_t4275860644 * L_226 = V_0;
		NullCheck(L_226);
		JSONValue_t4275860644 * L_227 = JSONValue_get_Parent_m2676443687(L_226, /*hidden argument*/NULL);
		NullCheck(L_227);
		JSONObject_t321714843 * L_228 = JSONValue_get_Object_m3828143293(L_227, /*hidden argument*/NULL);
		NullCheck(L_228);
		RuntimeObject* L_229 = L_228->get_values_0();
		Stack_1_t2690840144 * L_230 = V_1;
		NullCheck(L_230);
		String_t* L_231 = Stack_1_Pop_m513619580(L_230, /*hidden argument*/Stack_1_Pop_m513619580_RuntimeMethod_var);
		JSONValue_t4275860644 * L_232 = V_0;
		NullCheck(L_232);
		JSONArray_t4024675823 * L_233 = JSONValue_get_Array_m3632598190(L_232, /*hidden argument*/NULL);
		JSONValue_t4275860644 * L_234 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
		JSONValue__ctor_m3750205962(L_234, L_233, /*hidden argument*/NULL);
		NullCheck(L_229);
		InterfaceActionInvoker2< String_t*, JSONValue_t4275860644 * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t2524968334_il2cpp_TypeInfo_var, L_229, L_231, L_234);
		goto IL_0658;
	}

IL_062c:
	{
		JSONValue_t4275860644 * L_235 = V_0;
		NullCheck(L_235);
		JSONValue_t4275860644 * L_236 = JSONValue_get_Parent_m2676443687(L_235, /*hidden argument*/NULL);
		NullCheck(L_236);
		JSONArray_t4024675823 * L_237 = JSONValue_get_Array_m3632598190(L_236, /*hidden argument*/NULL);
		JSONValue_t4275860644 * L_238 = V_0;
		NullCheck(L_238);
		JSONArray_t4024675823 * L_239 = JSONValue_get_Array_m3632598190(L_238, /*hidden argument*/NULL);
		JSONValue_t4275860644 * L_240 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
		JSONValue__ctor_m3750205962(L_240, L_239, /*hidden argument*/NULL);
		NullCheck(L_237);
		JSONArray_Add_m1346855851(L_237, L_240, /*hidden argument*/NULL);
		goto IL_0658;
	}

IL_064c:
	{
		int32_t L_241 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_242 = JSONObject_Fail_m3595512628(NULL /*static, unused*/, _stringLiteral4012561394, L_241, /*hidden argument*/NULL);
		return L_242;
	}

IL_0658:
	{
		JSONValue_t4275860644 * L_243 = V_0;
		NullCheck(L_243);
		JSONValue_t4275860644 * L_244 = JSONValue_get_Parent_m2676443687(L_243, /*hidden argument*/NULL);
		V_0 = L_244;
		V_2 = 7;
		goto IL_0729;
	}

IL_0666:
	{
		String_t* L_245 = ___jsonString0;
		int32_t L_246 = V_3;
		NullCheck(L_245);
		Il2CppChar L_247 = String_get_Chars_m2986988803(L_245, L_246, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_247) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_0722;
		}
	}
	{
		String_t* L_248 = ___jsonString0;
		NullCheck(L_248);
		int32_t L_249 = String_get_Length_m3847582255(L_248, /*hidden argument*/NULL);
		int32_t L_250 = V_3;
		if ((((int32_t)L_249) < ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_250, (int32_t)4)))))
		{
			goto IL_06b2;
		}
	}
	{
		String_t* L_251 = ___jsonString0;
		int32_t L_252 = V_3;
		NullCheck(L_251);
		Il2CppChar L_253 = String_get_Chars_m2986988803(L_251, ((int32_t)il2cpp_codegen_add((int32_t)L_252, (int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_253) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_06b2;
		}
	}
	{
		String_t* L_254 = ___jsonString0;
		int32_t L_255 = V_3;
		NullCheck(L_254);
		Il2CppChar L_256 = String_get_Chars_m2986988803(L_254, ((int32_t)il2cpp_codegen_add((int32_t)L_255, (int32_t)2)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_256) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_06b2;
		}
	}
	{
		String_t* L_257 = ___jsonString0;
		int32_t L_258 = V_3;
		NullCheck(L_257);
		Il2CppChar L_259 = String_get_Chars_m2986988803(L_257, ((int32_t)il2cpp_codegen_add((int32_t)L_258, (int32_t)3)), /*hidden argument*/NULL);
		if ((((int32_t)L_259) == ((int32_t)((int32_t)108))))
		{
			goto IL_06be;
		}
	}

IL_06b2:
	{
		int32_t L_260 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_261 = JSONObject_Fail_m3595512628(NULL /*static, unused*/, _stringLiteral1202628576, L_260, /*hidden argument*/NULL);
		return L_261;
	}

IL_06be:
	{
		JSONValue_t4275860644 * L_262 = V_0;
		NullCheck(L_262);
		int32_t L_263 = JSONValue_get_Type_m3765076585(L_262, /*hidden argument*/NULL);
		V_17 = L_263;
		int32_t L_264 = V_17;
		if ((((int32_t)L_264) == ((int32_t)2)))
		{
			goto IL_06db;
		}
	}
	{
		int32_t L_265 = V_17;
		if ((((int32_t)L_265) == ((int32_t)3)))
		{
			goto IL_06fc;
		}
	}
	{
		goto IL_0712;
	}

IL_06db:
	{
		JSONValue_t4275860644 * L_266 = V_0;
		NullCheck(L_266);
		JSONObject_t321714843 * L_267 = JSONValue_get_Object_m3828143293(L_266, /*hidden argument*/NULL);
		NullCheck(L_267);
		RuntimeObject* L_268 = L_267->get_values_0();
		Stack_1_t2690840144 * L_269 = V_1;
		NullCheck(L_269);
		String_t* L_270 = Stack_1_Pop_m513619580(L_269, /*hidden argument*/Stack_1_Pop_m513619580_RuntimeMethod_var);
		JSONValue_t4275860644 * L_271 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
		JSONValue__ctor_m34194388(L_271, 5, /*hidden argument*/NULL);
		NullCheck(L_268);
		InterfaceActionInvoker2< String_t*, JSONValue_t4275860644 * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t2524968334_il2cpp_TypeInfo_var, L_268, L_270, L_271);
		goto IL_071e;
	}

IL_06fc:
	{
		JSONValue_t4275860644 * L_272 = V_0;
		NullCheck(L_272);
		JSONArray_t4024675823 * L_273 = JSONValue_get_Array_m3632598190(L_272, /*hidden argument*/NULL);
		JSONValue_t4275860644 * L_274 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
		JSONValue__ctor_m34194388(L_274, 5, /*hidden argument*/NULL);
		NullCheck(L_273);
		JSONArray_Add_m1346855851(L_273, L_274, /*hidden argument*/NULL);
		goto IL_071e;
	}

IL_0712:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral2793114053, /*hidden argument*/NULL);
		return (JSONObject_t321714843 *)NULL;
	}

IL_071e:
	{
		int32_t L_275 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_275, (int32_t)3));
	}

IL_0722:
	{
		V_2 = 7;
		goto IL_0729;
	}

IL_0729:
	{
		int32_t L_276 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_276, (int32_t)1));
	}

IL_072d:
	{
		int32_t L_277 = V_3;
		String_t* L_278 = ___jsonString0;
		NullCheck(L_278);
		int32_t L_279 = String_get_Length_m3847582255(L_278, /*hidden argument*/NULL);
		if ((((int32_t)L_277) < ((int32_t)L_279)))
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral1239137644, /*hidden argument*/NULL);
		return (JSONObject_t321714843 *)NULL;
	}
}
// System.Int32 DigitsNFCToolkit.JSON.JSONObject::SkipWhitespace(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t JSONObject_SkipWhitespace_m1442147541 (RuntimeObject * __this /* static, unused */, String_t* ___str0, int32_t ___pos1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_SkipWhitespace_m1442147541_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		goto IL_000a;
	}

IL_0005:
	{
		int32_t L_0 = ___pos1;
		___pos1 = ((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1));
	}

IL_000a:
	{
		int32_t L_1 = ___pos1;
		String_t* L_2 = ___str0;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m3847582255(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_4 = ___str0;
		int32_t L_5 = ___pos1;
		NullCheck(L_4);
		Il2CppChar L_6 = String_get_Chars_m2986988803(L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_7 = Char_IsWhiteSpace_m2148390798(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0005;
		}
	}

IL_0027:
	{
		int32_t L_8 = ___pos1;
		return L_8;
	}
}
// System.String DigitsNFCToolkit.JSON.JSONObject::ParseString(System.String,System.Int32&)
extern "C" IL2CPP_METHOD_ATTR String_t* JSONObject_ParseString_m1617168844 (RuntimeObject * __this /* static, unused */, String_t* ___str0, int32_t* ___startPosition1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_ParseString_m1617168844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	Match_t3408321083 * V_2 = NULL;
	String_t* V_3 = NULL;
	{
		String_t* L_0 = ___str0;
		int32_t* L_1 = ___startPosition1;
		int32_t L_2 = *((int32_t*)L_1);
		NullCheck(L_0);
		Il2CppChar L_3 = String_get_Chars_m2986988803(L_0, L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_001e;
		}
	}
	{
		int32_t* L_4 = ___startPosition1;
		int32_t L_5 = *((int32_t*)L_4);
		String_t* L_6 = ___str0;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m3847582255(L_6, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1))) < ((int32_t)L_7)))
		{
			goto IL_002a;
		}
	}

IL_001e:
	{
		int32_t* L_8 = ___startPosition1;
		int32_t L_9 = *((int32_t*)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_Fail_m1137956452(NULL /*static, unused*/, ((int32_t)34), L_9, /*hidden argument*/NULL);
		return (String_t*)NULL;
	}

IL_002a:
	{
		String_t* L_10 = ___str0;
		int32_t* L_11 = ___startPosition1;
		int32_t L_12 = *((int32_t*)L_11);
		NullCheck(L_10);
		int32_t L_13 = String_IndexOf_m2466398549(L_10, ((int32_t)34), ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = V_0;
		int32_t* L_15 = ___startPosition1;
		int32_t L_16 = *((int32_t*)L_15);
		if ((((int32_t)L_14) > ((int32_t)L_16)))
		{
			goto IL_004d;
		}
	}
	{
		int32_t* L_17 = ___startPosition1;
		int32_t L_18 = *((int32_t*)L_17);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_Fail_m1137956452(NULL /*static, unused*/, ((int32_t)34), ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1)), /*hidden argument*/NULL);
		return (String_t*)NULL;
	}

IL_004d:
	{
		goto IL_0074;
	}

IL_0052:
	{
		String_t* L_19 = ___str0;
		int32_t L_20 = V_0;
		NullCheck(L_19);
		int32_t L_21 = String_IndexOf_m2466398549(L_19, ((int32_t)34), ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_21;
		int32_t L_22 = V_0;
		int32_t* L_23 = ___startPosition1;
		int32_t L_24 = *((int32_t*)L_23);
		if ((((int32_t)L_22) > ((int32_t)L_24)))
		{
			goto IL_0074;
		}
	}
	{
		int32_t* L_25 = ___startPosition1;
		int32_t L_26 = *((int32_t*)L_25);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_Fail_m1137956452(NULL /*static, unused*/, ((int32_t)34), ((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)1)), /*hidden argument*/NULL);
		return (String_t*)NULL;
	}

IL_0074:
	{
		String_t* L_27 = ___str0;
		int32_t L_28 = V_0;
		NullCheck(L_27);
		Il2CppChar L_29 = String_get_Chars_m2986988803(L_27, ((int32_t)il2cpp_codegen_subtract((int32_t)L_28, (int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_29) == ((int32_t)((int32_t)92))))
		{
			goto IL_0052;
		}
	}
	{
		String_t* L_30 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		V_1 = L_30;
		int32_t L_31 = V_0;
		int32_t* L_32 = ___startPosition1;
		int32_t L_33 = *((int32_t*)L_32);
		if ((((int32_t)L_31) <= ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1)))))
		{
			goto IL_00a5;
		}
	}
	{
		String_t* L_34 = ___str0;
		int32_t* L_35 = ___startPosition1;
		int32_t L_36 = *((int32_t*)L_35);
		int32_t L_37 = V_0;
		int32_t* L_38 = ___startPosition1;
		int32_t L_39 = *((int32_t*)L_38);
		NullCheck(L_34);
		String_t* L_40 = String_Substring_m1610150815(L_34, ((int32_t)il2cpp_codegen_add((int32_t)L_36, (int32_t)1)), ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_37, (int32_t)L_39)), (int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_40;
	}

IL_00a5:
	{
		int32_t* L_41 = ___startPosition1;
		int32_t L_42 = V_0;
		*((int32_t*)L_41) = (int32_t)L_42;
	}

IL_00a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		Regex_t3657309853 * L_43 = ((JSONObject_t321714843_StaticFields*)il2cpp_codegen_static_fields_for(JSONObject_t321714843_il2cpp_TypeInfo_var))->get_unicodeRegex_1();
		String_t* L_44 = V_1;
		NullCheck(L_43);
		Match_t3408321083 * L_45 = Regex_Match_m2255756165(L_43, L_44, /*hidden argument*/NULL);
		V_2 = L_45;
		Match_t3408321083 * L_46 = V_2;
		NullCheck(L_46);
		bool L_47 = Group_get_Success_m3823591889(L_46, /*hidden argument*/NULL);
		if (L_47)
		{
			goto IL_00c4;
		}
	}
	{
		goto IL_013e;
	}

IL_00c4:
	{
		Match_t3408321083 * L_48 = V_2;
		NullCheck(L_48);
		GroupCollection_t69770484 * L_49 = VirtFuncInvoker0< GroupCollection_t69770484 * >::Invoke(5 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_48);
		NullCheck(L_49);
		Group_t2468205786 * L_50 = GroupCollection_get_Item_m723682197(L_49, 1, /*hidden argument*/NULL);
		NullCheck(L_50);
		CaptureCollection_t1760593541 * L_51 = Group_get_Captures_m1655762411(L_50, /*hidden argument*/NULL);
		NullCheck(L_51);
		Capture_t2232016050 * L_52 = CaptureCollection_get_Item_m3183527268(L_51, 0, /*hidden argument*/NULL);
		NullCheck(L_52);
		String_t* L_53 = Capture_get_Value_m3919646039(L_52, /*hidden argument*/NULL);
		V_3 = L_53;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		ByteU5BU5D_t4116647657* L_54 = ((JSONObject_t321714843_StaticFields*)il2cpp_codegen_static_fields_for(JSONObject_t321714843_il2cpp_TypeInfo_var))->get_unicodeBytes_2();
		String_t* L_55 = V_3;
		NullCheck(L_55);
		String_t* L_56 = String_Substring_m1610150815(L_55, 0, 2, /*hidden argument*/NULL);
		uint8_t L_57 = Byte_Parse_m534370573(NULL /*static, unused*/, L_56, ((int32_t)515), /*hidden argument*/NULL);
		NullCheck(L_54);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)L_57);
		ByteU5BU5D_t4116647657* L_58 = ((JSONObject_t321714843_StaticFields*)il2cpp_codegen_static_fields_for(JSONObject_t321714843_il2cpp_TypeInfo_var))->get_unicodeBytes_2();
		String_t* L_59 = V_3;
		NullCheck(L_59);
		String_t* L_60 = String_Substring_m1610150815(L_59, 2, 2, /*hidden argument*/NULL);
		uint8_t L_61 = Byte_Parse_m534370573(NULL /*static, unused*/, L_60, ((int32_t)515), /*hidden argument*/NULL);
		NullCheck(L_58);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)L_61);
		Encoding_t1523322056 * L_62 = Encoding_get_Unicode_m811213576(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_63 = ((JSONObject_t321714843_StaticFields*)il2cpp_codegen_static_fields_for(JSONObject_t321714843_il2cpp_TypeInfo_var))->get_unicodeBytes_2();
		ByteU5BU5D_t4116647657* L_64 = ((JSONObject_t321714843_StaticFields*)il2cpp_codegen_static_fields_for(JSONObject_t321714843_il2cpp_TypeInfo_var))->get_unicodeBytes_2();
		NullCheck(L_64);
		NullCheck(L_62);
		String_t* L_65 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t4116647657*, int32_t, int32_t >::Invoke(33 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_62, L_63, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_64)->max_length)))));
		V_3 = L_65;
		String_t* L_66 = V_1;
		Match_t3408321083 * L_67 = V_2;
		NullCheck(L_67);
		String_t* L_68 = Capture_get_Value_m3919646039(L_67, /*hidden argument*/NULL);
		String_t* L_69 = V_3;
		NullCheck(L_66);
		String_t* L_70 = String_Replace_m1273907647(L_66, L_68, L_69, /*hidden argument*/NULL);
		V_1 = L_70;
		goto IL_00a8;
	}

IL_013e:
	{
		String_t* L_71 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		String_t* L_72 = JSONObject_SanitizeJSONString_m479242491(NULL /*static, unused*/, L_71, /*hidden argument*/NULL);
		V_1 = L_72;
		String_t* L_73 = V_1;
		return L_73;
	}
}
// System.String DigitsNFCToolkit.JSON.JSONObject::SanitizeJSONString(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* JSONObject_SanitizeJSONString_m479242491 (RuntimeObject * __this /* static, unused */, String_t* ___jsonStringValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_SanitizeJSONString_m479242491_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	{
		String_t* L_0 = ___jsonStringValue0;
		StringBuilder_t * L_1 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m2989139009(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		StringBuilder_t * L_2 = V_0;
		NullCheck(L_2);
		StringBuilder_Replace_m1968561789(L_2, _stringLiteral3450386420, _stringLiteral3452614526, /*hidden argument*/NULL);
		StringBuilder_t * L_3 = V_0;
		NullCheck(L_3);
		StringBuilder_Replace_m1968561789(L_3, _stringLiteral3458119668, _stringLiteral3452614644, /*hidden argument*/NULL);
		StringBuilder_t * L_4 = V_0;
		NullCheck(L_4);
		StringBuilder_Replace_m1968561789(L_4, _stringLiteral3450583028, _stringLiteral3452614529, /*hidden argument*/NULL);
		StringBuilder_t * L_5 = V_0;
		NullCheck(L_5);
		StringBuilder_Replace_m1968561789(L_5, _stringLiteral3453138932, _stringLiteral3452614568, /*hidden argument*/NULL);
		StringBuilder_t * L_6 = V_0;
		NullCheck(L_6);
		StringBuilder_Replace_m1968561789(L_6, _stringLiteral3452876788, _stringLiteral3452614564, /*hidden argument*/NULL);
		StringBuilder_t * L_7 = V_0;
		NullCheck(L_7);
		StringBuilder_Replace_m1968561789(L_7, _stringLiteral3453007860, _stringLiteral3452614566, /*hidden argument*/NULL);
		StringBuilder_t * L_8 = V_0;
		NullCheck(L_8);
		StringBuilder_Replace_m1968561789(L_8, _stringLiteral3452811252, _stringLiteral3452614563, /*hidden argument*/NULL);
		StringBuilder_t * L_9 = V_0;
		NullCheck(L_9);
		StringBuilder_Replace_m1968561789(L_9, _stringLiteral3453073396, _stringLiteral3452614567, /*hidden argument*/NULL);
		StringBuilder_t * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		return L_11;
	}
}
// System.Double DigitsNFCToolkit.JSON.JSONObject::ParseNumber(System.String,System.Int32&)
extern "C" IL2CPP_METHOD_ATTR double JSONObject_ParseNumber_m2501283307 (RuntimeObject * __this /* static, unused */, String_t* ___str0, int32_t* ___startPosition1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_ParseNumber_m2501283307_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	double V_1 = 0.0;
	{
		int32_t* L_0 = ___startPosition1;
		int32_t L_1 = *((int32_t*)L_0);
		String_t* L_2 = ___str0;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m3847582255(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_002e;
		}
	}
	{
		String_t* L_4 = ___str0;
		int32_t* L_5 = ___startPosition1;
		int32_t L_6 = *((int32_t*)L_5);
		NullCheck(L_4);
		Il2CppChar L_7 = String_get_Chars_m2986988803(L_4, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_8 = Char_IsDigit_m3646673943(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_9 = ___str0;
		int32_t* L_10 = ___startPosition1;
		int32_t L_11 = *((int32_t*)L_10);
		NullCheck(L_9);
		Il2CppChar L_12 = String_get_Chars_m2986988803(L_9, L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_12) == ((int32_t)((int32_t)45))))
		{
			goto IL_0038;
		}
	}

IL_002e:
	{
		return (std::numeric_limits<double>::quiet_NaN());
	}

IL_0038:
	{
		int32_t* L_13 = ___startPosition1;
		int32_t L_14 = *((int32_t*)L_13);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
		goto IL_0046;
	}

IL_0042:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0046:
	{
		int32_t L_16 = V_0;
		String_t* L_17 = ___str0;
		NullCheck(L_17);
		int32_t L_18 = String_get_Length_m3847582255(L_17, /*hidden argument*/NULL);
		if ((((int32_t)L_16) >= ((int32_t)L_18)))
		{
			goto IL_007c;
		}
	}
	{
		String_t* L_19 = ___str0;
		int32_t L_20 = V_0;
		NullCheck(L_19);
		Il2CppChar L_21 = String_get_Chars_m2986988803(L_19, L_20, /*hidden argument*/NULL);
		if ((((int32_t)L_21) == ((int32_t)((int32_t)44))))
		{
			goto IL_007c;
		}
	}
	{
		String_t* L_22 = ___str0;
		int32_t L_23 = V_0;
		NullCheck(L_22);
		Il2CppChar L_24 = String_get_Chars_m2986988803(L_22, L_23, /*hidden argument*/NULL);
		if ((((int32_t)L_24) == ((int32_t)((int32_t)93))))
		{
			goto IL_007c;
		}
	}
	{
		String_t* L_25 = ___str0;
		int32_t L_26 = V_0;
		NullCheck(L_25);
		Il2CppChar L_27 = String_get_Chars_m2986988803(L_25, L_26, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_27) == ((uint32_t)((int32_t)125)))))
		{
			goto IL_0042;
		}
	}

IL_007c:
	{
		String_t* L_28 = ___str0;
		int32_t* L_29 = ___startPosition1;
		int32_t L_30 = *((int32_t*)L_29);
		int32_t L_31 = V_0;
		int32_t* L_32 = ___startPosition1;
		int32_t L_33 = *((int32_t*)L_32);
		NullCheck(L_28);
		String_t* L_34 = String_Substring_m1610150815(L_28, L_30, ((int32_t)il2cpp_codegen_subtract((int32_t)L_31, (int32_t)L_33)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t4157843068_il2cpp_TypeInfo_var);
		CultureInfo_t4157843068 * L_35 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Double_t594665363_il2cpp_TypeInfo_var);
		bool L_36 = Double_TryParse_m623190659(NULL /*static, unused*/, L_34, ((int32_t)167), L_35, (double*)(&V_1), /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_00a8;
		}
	}
	{
		return (std::numeric_limits<double>::quiet_NaN());
	}

IL_00a8:
	{
		int32_t* L_37 = ___startPosition1;
		int32_t L_38 = V_0;
		*((int32_t*)L_37) = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_38, (int32_t)1));
		double L_39 = V_1;
		return L_39;
	}
}
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.JSON.JSONObject::Fail(System.Char,System.Int32)
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * JSONObject_Fail_m1137956452 (RuntimeObject * __this /* static, unused */, Il2CppChar ___expected0, int32_t ___position1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Fail_m1137956452_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppChar L_0 = ___expected0;
		String_t* L_1 = String_CreateString_m1262864254(NULL, L_0, 1, /*hidden argument*/NULL);
		int32_t L_2 = ___position1;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_3 = JSONObject_Fail_m3595512628(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.JSON.JSONObject::Fail(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * JSONObject_Fail_m3595512628 (RuntimeObject * __this /* static, unused */, String_t* ___expected0, int32_t ___position1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Fail_m3595512628_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t2843939325* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral1783094721);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral1783094721);
		ObjectU5BU5D_t2843939325* L_2 = L_1;
		String_t* L_3 = ___expected0;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = L_2;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral731054790);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral731054790);
		ObjectU5BU5D_t2843939325* L_5 = L_4;
		int32_t L_6 = ___position1;
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_8);
		String_t* L_9 = String_Concat_m2971454694(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return (JSONObject_t321714843 *)NULL;
	}
}
// System.String DigitsNFCToolkit.JSON.JSONObject::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* JSONObject_ToString_m2895087238 (JSONObject_t321714843 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_ToString_m2895087238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	KeyValuePair_2_t2163821814  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RuntimeObject* V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t * L_0 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_Append_m2383614642(L_1, ((int32_t)123), /*hidden argument*/NULL);
		RuntimeObject* L_2 = __this->get_values_0();
		NullCheck(L_2);
		RuntimeObject* L_3 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,DigitsNFCToolkit.JSON.JSONValue>>::GetEnumerator() */, IEnumerable_1_t1143674703_il2cpp_TypeInfo_var, L_2);
		V_2 = L_3;
	}

IL_001b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0069;
		}

IL_0020:
		{
			RuntimeObject* L_4 = V_2;
			NullCheck(L_4);
			KeyValuePair_2_t2163821814  L_5 = InterfaceFuncInvoker0< KeyValuePair_2_t2163821814  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,DigitsNFCToolkit.JSON.JSONValue>>::get_Current() */, IEnumerator_1_t2596392282_il2cpp_TypeInfo_var, L_4);
			V_1 = L_5;
			StringBuilder_t * L_6 = V_0;
			String_t* L_7 = KeyValuePair_2_get_Key_m1640885509((KeyValuePair_2_t2163821814 *)(&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m1640885509_RuntimeMethod_var);
			String_t* L_8 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral3452614526, L_7, _stringLiteral3452614526, /*hidden argument*/NULL);
			NullCheck(L_6);
			StringBuilder_Append_m1965104174(L_6, L_8, /*hidden argument*/NULL);
			StringBuilder_t * L_9 = V_0;
			NullCheck(L_9);
			StringBuilder_Append_m2383614642(L_9, ((int32_t)58), /*hidden argument*/NULL);
			StringBuilder_t * L_10 = V_0;
			JSONValue_t4275860644 * L_11 = KeyValuePair_2_get_Value_m1594442022((KeyValuePair_2_t2163821814 *)(&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m1594442022_RuntimeMethod_var);
			NullCheck(L_11);
			String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_11);
			NullCheck(L_10);
			StringBuilder_Append_m1965104174(L_10, L_12, /*hidden argument*/NULL);
			StringBuilder_t * L_13 = V_0;
			NullCheck(L_13);
			StringBuilder_Append_m2383614642(L_13, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_0069:
		{
			RuntimeObject* L_14 = V_2;
			NullCheck(L_14);
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_0020;
			}
		}

IL_0074:
		{
			IL2CPP_LEAVE(0x86, FINALLY_0079);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0079;
	}

FINALLY_0079:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_16 = V_2;
			if (!L_16)
			{
				goto IL_0085;
			}
		}

IL_007f:
		{
			RuntimeObject* L_17 = V_2;
			NullCheck(L_17);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_17);
		}

IL_0085:
		{
			IL2CPP_END_FINALLY(121)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(121)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0086:
	{
		RuntimeObject* L_18 = __this->get_values_0();
		NullCheck(L_18);
		int32_t L_19 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,DigitsNFCToolkit.JSON.JSONValue>>::get_Count() */, ICollection_1_t697006752_il2cpp_TypeInfo_var, L_18);
		if ((((int32_t)L_19) <= ((int32_t)0)))
		{
			goto IL_00a7;
		}
	}
	{
		StringBuilder_t * L_20 = V_0;
		StringBuilder_t * L_21 = V_0;
		NullCheck(L_21);
		int32_t L_22 = StringBuilder_get_Length_m3238060835(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		StringBuilder_Remove_m940064945(L_20, ((int32_t)il2cpp_codegen_subtract((int32_t)L_22, (int32_t)1)), 1, /*hidden argument*/NULL);
	}

IL_00a7:
	{
		StringBuilder_t * L_23 = V_0;
		NullCheck(L_23);
		StringBuilder_Append_m2383614642(L_23, ((int32_t)125), /*hidden argument*/NULL);
		StringBuilder_t * L_24 = V_0;
		NullCheck(L_24);
		String_t* L_25 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_24);
		return L_25;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,DigitsNFCToolkit.JSON.JSONValue>> DigitsNFCToolkit.JSON.JSONObject::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* JSONObject_GetEnumerator_m1190645326 (JSONObject_t321714843 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetEnumerator_m1190645326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_values_0();
		NullCheck(L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,DigitsNFCToolkit.JSON.JSONValue>>::GetEnumerator() */, IEnumerable_1_t1143674703_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Collections.IEnumerator DigitsNFCToolkit.JSON.JSONObject::System.Collections.IEnumerable.GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* JSONObject_System_Collections_IEnumerable_GetEnumerator_m1937787778 (JSONObject_t321714843 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_System_Collections_IEnumerable_GetEnumerator_m1937787778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_values_0();
		NullCheck(L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,DigitsNFCToolkit.JSON.JSONValue>>::GetEnumerator() */, IEnumerable_1_t1143674703_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONObject::Clear()
extern "C" IL2CPP_METHOD_ATTR void JSONObject_Clear_m476800019 (JSONObject_t321714843 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Clear_m476800019_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_values_0();
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,DigitsNFCToolkit.JSON.JSONValue>>::Clear() */, ICollection_1_t697006752_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONObject::Remove(System.String)
extern "C" IL2CPP_METHOD_ATTR void JSONObject_Remove_m1786693770 (JSONObject_t321714843 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Remove_m1786693770_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_values_0();
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::ContainsKey(!0) */, IDictionary_2_t2524968334_il2cpp_TypeInfo_var, L_0, L_1);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		RuntimeObject* L_3 = __this->get_values_0();
		String_t* L_4 = ___key0;
		NullCheck(L_3);
		InterfaceFuncInvoker1< bool, String_t* >::Invoke(5 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>::Remove(!0) */, IDictionary_2_t2524968334_il2cpp_TypeInfo_var, L_3, L_4);
	}

IL_001e:
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONObject::.cctor()
extern "C" IL2CPP_METHOD_ATTR void JSONObject__cctor_m723892856 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__cctor_m723892856_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Regex_t3657309853 * L_0 = (Regex_t3657309853 *)il2cpp_codegen_object_new(Regex_t3657309853_il2cpp_TypeInfo_var);
		Regex__ctor_m3948448025(L_0, _stringLiteral259035310, /*hidden argument*/NULL);
		((JSONObject_t321714843_StaticFields*)il2cpp_codegen_static_fields_for(JSONObject_t321714843_il2cpp_TypeInfo_var))->set_unicodeRegex_1(L_0);
		ByteU5BU5D_t4116647657* L_1 = (ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)2);
		((JSONObject_t321714843_StaticFields*)il2cpp_codegen_static_fields_for(JSONObject_t321714843_il2cpp_TypeInfo_var))->set_unicodeBytes_2(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.JSON.JSONValue::.ctor(DigitsNFCToolkit.JSON.JSONValueType)
extern "C" IL2CPP_METHOD_ATTR void JSONValue__ctor_m34194388 (JSONValue_t4275860644 * __this, int32_t ___type0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___type0;
		JSONValue_set_Type_m40491884(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONValue::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void JSONValue__ctor_m2817256385 (JSONValue_t4275860644 * __this, String_t* ___str0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		JSONValue_set_Type_m40491884(__this, 0, /*hidden argument*/NULL);
		String_t* L_0 = ___str0;
		JSONValue_set_String_m3777049347(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONValue::.ctor(System.Double)
extern "C" IL2CPP_METHOD_ATTR void JSONValue__ctor_m610419965 (JSONValue_t4275860644 * __this, double ___number0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		JSONValue_set_Type_m40491884(__this, 1, /*hidden argument*/NULL);
		double L_0 = ___number0;
		JSONValue_set_Double_m2090912025(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONValue::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void JSONValue__ctor_m3984902601 (JSONValue_t4275860644 * __this, JSONObject_t321714843 * ___obj0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		JSONValue_set_Type_m40491884(__this, 5, /*hidden argument*/NULL);
		goto IL_0026;
	}

IL_0018:
	{
		JSONValue_set_Type_m40491884(__this, 2, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_1 = ___obj0;
		JSONValue_set_Object_m1350223645(__this, L_1, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONValue::.ctor(DigitsNFCToolkit.JSON.JSONArray)
extern "C" IL2CPP_METHOD_ATTR void JSONValue__ctor_m3750205962 (JSONValue_t4275860644 * __this, JSONArray_t4024675823 * ___array0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		JSONValue_set_Type_m40491884(__this, 3, /*hidden argument*/NULL);
		JSONArray_t4024675823 * L_0 = ___array0;
		JSONValue_set_Array_m3526479683(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONValue::.ctor(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void JSONValue__ctor_m31333835 (JSONValue_t4275860644 * __this, bool ___boolean0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		JSONValue_set_Type_m40491884(__this, 4, /*hidden argument*/NULL);
		bool L_0 = ___boolean0;
		JSONValue_set_Boolean_m4041559619(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONValue::.ctor(DigitsNFCToolkit.JSON.JSONValue)
extern "C" IL2CPP_METHOD_ATTR void JSONValue__ctor_m1222737883 (JSONValue_t4275860644 * __this, JSONValue_t4275860644 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONValue__ctor_m1222737883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		JSONValue_t4275860644 * L_0 = ___value0;
		NullCheck(L_0);
		int32_t L_1 = JSONValue_get_Type_m3765076585(L_0, /*hidden argument*/NULL);
		JSONValue_set_Type_m40491884(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = JSONValue_get_Type_m3765076585(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		switch (L_3)
		{
			case 0:
			{
				goto IL_0038;
			}
			case 1:
			{
				goto IL_005a;
			}
			case 2:
			{
				goto IL_006b;
			}
			case 3:
			{
				goto IL_008c;
			}
			case 4:
			{
				goto IL_0049;
			}
		}
	}
	{
		goto IL_00a2;
	}

IL_0038:
	{
		JSONValue_t4275860644 * L_4 = ___value0;
		NullCheck(L_4);
		String_t* L_5 = JSONValue_get_String_m3887450814(L_4, /*hidden argument*/NULL);
		JSONValue_set_String_m3777049347(__this, L_5, /*hidden argument*/NULL);
		goto IL_00a2;
	}

IL_0049:
	{
		JSONValue_t4275860644 * L_6 = ___value0;
		NullCheck(L_6);
		bool L_7 = JSONValue_get_Boolean_m2734736792(L_6, /*hidden argument*/NULL);
		JSONValue_set_Boolean_m4041559619(__this, L_7, /*hidden argument*/NULL);
		goto IL_00a2;
	}

IL_005a:
	{
		JSONValue_t4275860644 * L_8 = ___value0;
		NullCheck(L_8);
		double L_9 = JSONValue_get_Double_m3385163784(L_8, /*hidden argument*/NULL);
		JSONValue_set_Double_m2090912025(__this, L_9, /*hidden argument*/NULL);
		goto IL_00a2;
	}

IL_006b:
	{
		JSONValue_t4275860644 * L_10 = ___value0;
		NullCheck(L_10);
		JSONObject_t321714843 * L_11 = JSONValue_get_Object_m3828143293(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0087;
		}
	}
	{
		JSONValue_t4275860644 * L_12 = ___value0;
		NullCheck(L_12);
		JSONObject_t321714843 * L_13 = JSONValue_get_Object_m3828143293(L_12, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_14 = (JSONObject_t321714843 *)il2cpp_codegen_object_new(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject__ctor_m2883999366(L_14, L_13, /*hidden argument*/NULL);
		JSONValue_set_Object_m1350223645(__this, L_14, /*hidden argument*/NULL);
	}

IL_0087:
	{
		goto IL_00a2;
	}

IL_008c:
	{
		JSONValue_t4275860644 * L_15 = ___value0;
		NullCheck(L_15);
		JSONArray_t4024675823 * L_16 = JSONValue_get_Array_m3632598190(L_15, /*hidden argument*/NULL);
		JSONArray_t4024675823 * L_17 = (JSONArray_t4024675823 *)il2cpp_codegen_object_new(JSONArray_t4024675823_il2cpp_TypeInfo_var);
		JSONArray__ctor_m4099340829(L_17, L_16, /*hidden argument*/NULL);
		JSONValue_set_Array_m3526479683(__this, L_17, /*hidden argument*/NULL);
		goto IL_00a2;
	}

IL_00a2:
	{
		return;
	}
}
// DigitsNFCToolkit.JSON.JSONValueType DigitsNFCToolkit.JSON.JSONValue::get_Type()
extern "C" IL2CPP_METHOD_ATTR int32_t JSONValue_get_Type_m3765076585 (JSONValue_t4275860644 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CTypeU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONValue::set_Type(DigitsNFCToolkit.JSON.JSONValueType)
extern "C" IL2CPP_METHOD_ATTR void JSONValue_set_Type_m40491884 (JSONValue_t4275860644 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String DigitsNFCToolkit.JSON.JSONValue::get_String()
extern "C" IL2CPP_METHOD_ATTR String_t* JSONValue_get_String_m3887450814 (JSONValue_t4275860644 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CStringU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONValue::set_String(System.String)
extern "C" IL2CPP_METHOD_ATTR void JSONValue_set_String_m3777049347 (JSONValue_t4275860644 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStringU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Double DigitsNFCToolkit.JSON.JSONValue::get_Double()
extern "C" IL2CPP_METHOD_ATTR double JSONValue_get_Double_m3385163784 (JSONValue_t4275860644 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CDoubleU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONValue::set_Double(System.Double)
extern "C" IL2CPP_METHOD_ATTR void JSONValue_set_Double_m2090912025 (JSONValue_t4275860644 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CDoubleU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Single DigitsNFCToolkit.JSON.JSONValue::get_Float()
extern "C" IL2CPP_METHOD_ATTR float JSONValue_get_Float_m2758114374 (JSONValue_t4275860644 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = JSONValue_get_Double_m3385163784(__this, /*hidden argument*/NULL);
		return (((float)((float)L_0)));
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONValue::set_Float(System.Single)
extern "C" IL2CPP_METHOD_ATTR void JSONValue_set_Float_m908577611 (JSONValue_t4275860644 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		JSONValue_set_Double_m2090912025(__this, (((double)((double)L_0))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 DigitsNFCToolkit.JSON.JSONValue::get_Integer()
extern "C" IL2CPP_METHOD_ATTR int32_t JSONValue_get_Integer_m3463426723 (JSONValue_t4275860644 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = JSONValue_get_Double_m3385163784(__this, /*hidden argument*/NULL);
		return (((int32_t)((int32_t)L_0)));
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONValue::set_Integer(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void JSONValue_set_Integer_m2673453816 (JSONValue_t4275860644 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		JSONValue_set_Double_m2090912025(__this, (((double)((double)L_0))), /*hidden argument*/NULL);
		return;
	}
}
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.JSON.JSONValue::get_Object()
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * JSONValue_get_Object_m3828143293 (JSONValue_t4275860644 * __this, const RuntimeMethod* method)
{
	{
		JSONObject_t321714843 * L_0 = __this->get_U3CObjectU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONValue::set_Object(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void JSONValue_set_Object_m1350223645 (JSONValue_t4275860644 * __this, JSONObject_t321714843 * ___value0, const RuntimeMethod* method)
{
	{
		JSONObject_t321714843 * L_0 = ___value0;
		__this->set_U3CObjectU3Ek__BackingField_3(L_0);
		return;
	}
}
// DigitsNFCToolkit.JSON.JSONArray DigitsNFCToolkit.JSON.JSONValue::get_Array()
extern "C" IL2CPP_METHOD_ATTR JSONArray_t4024675823 * JSONValue_get_Array_m3632598190 (JSONValue_t4275860644 * __this, const RuntimeMethod* method)
{
	{
		JSONArray_t4024675823 * L_0 = __this->get_U3CArrayU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONValue::set_Array(DigitsNFCToolkit.JSON.JSONArray)
extern "C" IL2CPP_METHOD_ATTR void JSONValue_set_Array_m3526479683 (JSONValue_t4275860644 * __this, JSONArray_t4024675823 * ___value0, const RuntimeMethod* method)
{
	{
		JSONArray_t4024675823 * L_0 = ___value0;
		__this->set_U3CArrayU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Boolean DigitsNFCToolkit.JSON.JSONValue::get_Boolean()
extern "C" IL2CPP_METHOD_ATTR bool JSONValue_get_Boolean_m2734736792 (JSONValue_t4275860644 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CBooleanU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONValue::set_Boolean(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void JSONValue_set_Boolean_m4041559619 (JSONValue_t4275860644 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CBooleanU3Ek__BackingField_5(L_0);
		return;
	}
}
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONValue::get_Parent()
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONValue_get_Parent_m2676443687 (JSONValue_t4275860644 * __this, const RuntimeMethod* method)
{
	{
		JSONValue_t4275860644 * L_0 = __this->get_U3CParentU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void DigitsNFCToolkit.JSON.JSONValue::set_Parent(DigitsNFCToolkit.JSON.JSONValue)
extern "C" IL2CPP_METHOD_ATTR void JSONValue_set_Parent_m1935092005 (JSONValue_t4275860644 * __this, JSONValue_t4275860644 * ___value0, const RuntimeMethod* method)
{
	{
		JSONValue_t4275860644 * L_0 = ___value0;
		__this->set_U3CParentU3Ek__BackingField_6(L_0);
		return;
	}
}
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONValue::op_Implicit(System.String)
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONValue_op_Implicit_m357672470 (RuntimeObject * __this /* static, unused */, String_t* ___str0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONValue_op_Implicit_m357672470_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		JSONValue_t4275860644 * L_1 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
		JSONValue__ctor_m2817256385(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONValue::op_Implicit(System.Double)
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONValue_op_Implicit_m3141527758 (RuntimeObject * __this /* static, unused */, double ___number0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONValue_op_Implicit_m3141527758_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		double L_0 = ___number0;
		JSONValue_t4275860644 * L_1 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
		JSONValue__ctor_m610419965(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONValue::op_Implicit(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONValue_op_Implicit_m1136109141 (RuntimeObject * __this /* static, unused */, JSONObject_t321714843 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONValue_op_Implicit_m1136109141_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JSONObject_t321714843 * L_0 = ___obj0;
		JSONValue_t4275860644 * L_1 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
		JSONValue__ctor_m3984902601(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONValue::op_Implicit(DigitsNFCToolkit.JSON.JSONArray)
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONValue_op_Implicit_m216610386 (RuntimeObject * __this /* static, unused */, JSONArray_t4024675823 * ___array0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONValue_op_Implicit_m216610386_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JSONArray_t4024675823 * L_0 = ___array0;
		JSONValue_t4275860644 * L_1 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
		JSONValue__ctor_m3750205962(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONValue::op_Implicit(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR JSONValue_t4275860644 * JSONValue_op_Implicit_m2321309250 (RuntimeObject * __this /* static, unused */, bool ___boolean0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONValue_op_Implicit_m2321309250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___boolean0;
		JSONValue_t4275860644 * L_1 = (JSONValue_t4275860644 *)il2cpp_codegen_object_new(JSONValue_t4275860644_il2cpp_TypeInfo_var);
		JSONValue__ctor_m31333835(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String DigitsNFCToolkit.JSON.JSONValue::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* JSONValue_ToString_m620812479 (JSONValue_t4275860644 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JSONValue_ToString_m620812479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	double V_1 = 0.0;
	String_t* G_B7_0 = NULL;
	{
		int32_t L_0 = JSONValue_get_Type_m3765076585(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0072;
			}
			case 1:
			{
				goto IL_005d;
			}
			case 2:
			{
				goto IL_002a;
			}
			case 3:
			{
				goto IL_0036;
			}
			case 4:
			{
				goto IL_0042;
			}
			case 5:
			{
				goto IL_0088;
			}
		}
	}
	{
		goto IL_008e;
	}

IL_002a:
	{
		JSONObject_t321714843 * L_2 = JSONValue_get_Object_m3828143293(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		return L_3;
	}

IL_0036:
	{
		JSONArray_t4024675823 * L_4 = JSONValue_get_Array_m3632598190(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		return L_5;
	}

IL_0042:
	{
		bool L_6 = JSONValue_get_Boolean_m2734736792(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0057;
		}
	}
	{
		G_B7_0 = _stringLiteral4002445229;
		goto IL_005c;
	}

IL_0057:
	{
		G_B7_0 = _stringLiteral3875954633;
	}

IL_005c:
	{
		return G_B7_0;
	}

IL_005d:
	{
		double L_7 = JSONValue_get_Double_m3385163784(__this, /*hidden argument*/NULL);
		V_1 = L_7;
		String_t* L_8 = Double_ToString_m1229922074((double*)(&V_1), /*hidden argument*/NULL);
		return L_8;
	}

IL_0072:
	{
		String_t* L_9 = JSONValue_get_String_m3887450814(__this, /*hidden argument*/NULL);
		String_t* L_10 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral3452614526, L_9, _stringLiteral3452614526, /*hidden argument*/NULL);
		return L_10;
	}

IL_0088:
	{
		return _stringLiteral1202628576;
	}

IL_008e:
	{
		return _stringLiteral1202628576;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.MimeMediaRecord::.ctor(System.String,System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void MimeMediaRecord__ctor_m353281183 (MimeMediaRecord_t736820488 * __this, String_t* ___mimeType0, ByteU5BU5D_t4116647657* ___mimeData1, const RuntimeMethod* method)
{
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		((NDEFRecord_t2690545556 *)__this)->set_type_0(3);
		String_t* L_0 = ___mimeType0;
		__this->set_mimeType_1(L_0);
		ByteU5BU5D_t4116647657* L_1 = ___mimeData1;
		__this->set_mimeData_2(L_1);
		return;
	}
}
// System.Void DigitsNFCToolkit.MimeMediaRecord::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void MimeMediaRecord__ctor_m1733123593 (MimeMediaRecord_t736820488 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		VirtActionInvoker1< JSONObject_t321714843 * >::Invoke(6 /* System.Void DigitsNFCToolkit.NDEFRecord::ParseJSON(DigitsNFCToolkit.JSON.JSONObject) */, __this, L_0);
		return;
	}
}
// System.Void DigitsNFCToolkit.MimeMediaRecord::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void MimeMediaRecord_ParseJSON_m4043513565 (MimeMediaRecord_t736820488 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MimeMediaRecord_ParseJSON_m4043513565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		NDEFRecord_ParseJSON_m1625700843(__this, L_0, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_1 = ___jsonObject0;
		String_t** L_2 = __this->get_address_of_mimeType_1();
		NullCheck(L_1);
		JSONObject_TryGetString_m766921647(L_1, _stringLiteral2142701798, (String_t**)L_2, /*hidden argument*/NULL);
		V_0 = (String_t*)NULL;
		JSONObject_t321714843 * L_3 = ___jsonObject0;
		NullCheck(L_3);
		bool L_4 = JSONObject_TryGetString_m766921647(L_3, _stringLiteral3941463922, (String_t**)(&V_0), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0039;
		}
	}
	{
		String_t* L_5 = V_0;
		ByteU5BU5D_t4116647657* L_6 = Util_DecodeBase64UrlSafe_m4209975833(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_mimeData_2(L_6);
	}

IL_0039:
	{
		return;
	}
}
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.MimeMediaRecord::ToJSON()
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * MimeMediaRecord_ToJSON_m500662920 (MimeMediaRecord_t736820488 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MimeMediaRecord_ToJSON_m500662920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t321714843 * V_0 = NULL;
	{
		JSONObject_t321714843 * L_0 = NDEFRecord_ToJSON_m164142245(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t321714843 * L_1 = V_0;
		String_t* L_2 = __this->get_mimeType_1();
		JSONValue_t4275860644 * L_3 = JSONValue_op_Implicit_m357672470(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		JSONObject_Add_m2412535806(L_1, _stringLiteral2142701798, L_3, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_4 = V_0;
		ByteU5BU5D_t4116647657* L_5 = __this->get_mimeData_2();
		String_t* L_6 = Util_EncodeBase64UrlSafe_m2760074679(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		JSONValue_t4275860644 * L_7 = JSONValue_op_Implicit_m357672470(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		JSONObject_Add_m2412535806(L_4, _stringLiteral3941463922, L_7, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_8 = V_0;
		return L_8;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.NDEFMessage::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NDEFMessage__ctor_m2200006917 (NDEFMessage_t279637043 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NDEFMessage__ctor_m2200006917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		List_1_t4162620298 * L_0 = (List_1_t4162620298 *)il2cpp_codegen_object_new(List_1_t4162620298_il2cpp_TypeInfo_var);
		List_1__ctor_m1332560254(L_0, /*hidden argument*/List_1__ctor_m1332560254_RuntimeMethod_var);
		__this->set_records_0(L_0);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_tagID_1(L_1);
		__this->set_writeState_2(0);
		__this->set_writeError_3(0);
		return;
	}
}
// System.Void DigitsNFCToolkit.NDEFMessage::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFMessage__ctor_m1423095509 (NDEFMessage_t279637043 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		NDEFMessage_ParseJSON_m112885689(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<DigitsNFCToolkit.NDEFRecord> DigitsNFCToolkit.NDEFMessage::get_Records()
extern "C" IL2CPP_METHOD_ATTR List_1_t4162620298 * NDEFMessage_get_Records_m1930208688 (NDEFMessage_t279637043 * __this, const RuntimeMethod* method)
{
	{
		List_1_t4162620298 * L_0 = __this->get_records_0();
		return L_0;
	}
}
// System.String DigitsNFCToolkit.NDEFMessage::get_TagID()
extern "C" IL2CPP_METHOD_ATTR String_t* NDEFMessage_get_TagID_m2458113489 (NDEFMessage_t279637043 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_tagID_1();
		return L_0;
	}
}
// DigitsNFCToolkit.NDEFMessageWriteState DigitsNFCToolkit.NDEFMessage::get_WriteState()
extern "C" IL2CPP_METHOD_ATTR int32_t NDEFMessage_get_WriteState_m1477143167 (NDEFMessage_t279637043 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_writeState_2();
		return L_0;
	}
}
// DigitsNFCToolkit.NDEFMessageWriteError DigitsNFCToolkit.NDEFMessage::get_WriteError()
extern "C" IL2CPP_METHOD_ATTR int32_t NDEFMessage_get_WriteError_m410788832 (NDEFMessage_t279637043 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_writeError_3();
		return L_0;
	}
}
// System.Boolean DigitsNFCToolkit.NDEFMessage::get_WriteSuccess()
extern "C" IL2CPP_METHOD_ATTR bool NDEFMessage_get_WriteSuccess_m3040163407 (NDEFMessage_t279637043 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = NDEFMessage_get_WriteState_m1477143167(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// System.Void DigitsNFCToolkit.NDEFMessage::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFMessage_ParseJSON_m112885689 (NDEFMessage_t279637043 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NDEFMessage_ParseJSON_m112885689_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONArray_t4024675823 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	JSONObject_t321714843 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	NDEFRecord_t2690545556 * V_6 = NULL;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		NullCheck(L_0);
		bool L_1 = JSONObject_TryGetArray_m3928523712(L_0, _stringLiteral3952973286, (JSONArray_t4024675823 **)(&V_0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00fe;
		}
	}
	{
		JSONArray_t4024675823 * L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = JSONArray_get_Length_m1921089944(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		List_1_t4162620298 * L_4 = (List_1_t4162620298 *)il2cpp_codegen_object_new(List_1_t4162620298_il2cpp_TypeInfo_var);
		List_1__ctor_m1332560254(L_4, /*hidden argument*/List_1__ctor_m1332560254_RuntimeMethod_var);
		__this->set_records_0(L_4);
		V_2 = 0;
		goto IL_00f2;
	}

IL_002b:
	{
		JSONArray_t4024675823 * L_5 = V_0;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		JSONValue_t4275860644 * L_7 = JSONArray_get_Item_m1052458067(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		JSONObject_t321714843 * L_8 = JSONValue_get_Object_m3828143293(L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		JSONObject_t321714843 * L_9 = V_3;
		NullCheck(L_9);
		JSONObject_TryGetInt_m2083265671(L_9, _stringLiteral3243520166, (int32_t*)(&V_4), /*hidden argument*/NULL);
		int32_t L_10 = V_4;
		V_5 = L_10;
		V_6 = (NDEFRecord_t2690545556 *)NULL;
		int32_t L_11 = V_5;
		switch (L_11)
		{
			case 0:
			{
				goto IL_0079;
			}
			case 1:
			{
				goto IL_0086;
			}
			case 2:
			{
				goto IL_0093;
			}
			case 3:
			{
				goto IL_00a0;
			}
			case 4:
			{
				goto IL_00ad;
			}
			case 5:
			{
				goto IL_00ba;
			}
			case 6:
			{
				goto IL_00c7;
			}
			case 7:
			{
				goto IL_00d4;
			}
		}
	}
	{
		goto IL_00e1;
	}

IL_0079:
	{
		JSONObject_t321714843 * L_12 = V_3;
		AbsoluteUriRecord_t2525168784 * L_13 = (AbsoluteUriRecord_t2525168784 *)il2cpp_codegen_object_new(AbsoluteUriRecord_t2525168784_il2cpp_TypeInfo_var);
		AbsoluteUriRecord__ctor_m157112738(L_13, L_12, /*hidden argument*/NULL);
		V_6 = L_13;
		goto IL_00e1;
	}

IL_0086:
	{
		JSONObject_t321714843 * L_14 = V_3;
		EmptyRecord_t1486430273 * L_15 = (EmptyRecord_t1486430273 *)il2cpp_codegen_object_new(EmptyRecord_t1486430273_il2cpp_TypeInfo_var);
		EmptyRecord__ctor_m3438728046(L_15, L_14, /*hidden argument*/NULL);
		V_6 = L_15;
		goto IL_00e1;
	}

IL_0093:
	{
		JSONObject_t321714843 * L_16 = V_3;
		ExternalTypeRecord_t4087466745 * L_17 = (ExternalTypeRecord_t4087466745 *)il2cpp_codegen_object_new(ExternalTypeRecord_t4087466745_il2cpp_TypeInfo_var);
		ExternalTypeRecord__ctor_m3317940198(L_17, L_16, /*hidden argument*/NULL);
		V_6 = L_17;
		goto IL_00e1;
	}

IL_00a0:
	{
		JSONObject_t321714843 * L_18 = V_3;
		MimeMediaRecord_t736820488 * L_19 = (MimeMediaRecord_t736820488 *)il2cpp_codegen_object_new(MimeMediaRecord_t736820488_il2cpp_TypeInfo_var);
		MimeMediaRecord__ctor_m1733123593(L_19, L_18, /*hidden argument*/NULL);
		V_6 = L_19;
		goto IL_00e1;
	}

IL_00ad:
	{
		JSONObject_t321714843 * L_20 = V_3;
		SmartPosterRecord_t1640848801 * L_21 = (SmartPosterRecord_t1640848801 *)il2cpp_codegen_object_new(SmartPosterRecord_t1640848801_il2cpp_TypeInfo_var);
		SmartPosterRecord__ctor_m3134893622(L_21, L_20, /*hidden argument*/NULL);
		V_6 = L_21;
		goto IL_00e1;
	}

IL_00ba:
	{
		JSONObject_t321714843 * L_22 = V_3;
		TextRecord_t2313697623 * L_23 = (TextRecord_t2313697623 *)il2cpp_codegen_object_new(TextRecord_t2313697623_il2cpp_TypeInfo_var);
		TextRecord__ctor_m2059777621(L_23, L_22, /*hidden argument*/NULL);
		V_6 = L_23;
		goto IL_00e1;
	}

IL_00c7:
	{
		JSONObject_t321714843 * L_24 = V_3;
		UnknownRecord_t3228240714 * L_25 = (UnknownRecord_t3228240714 *)il2cpp_codegen_object_new(UnknownRecord_t3228240714_il2cpp_TypeInfo_var);
		UnknownRecord__ctor_m1040786691(L_25, L_24, /*hidden argument*/NULL);
		V_6 = L_25;
		goto IL_00e1;
	}

IL_00d4:
	{
		JSONObject_t321714843 * L_26 = V_3;
		UriRecord_t2230063309 * L_27 = (UriRecord_t2230063309 *)il2cpp_codegen_object_new(UriRecord_t2230063309_il2cpp_TypeInfo_var);
		UriRecord__ctor_m12655916(L_27, L_26, /*hidden argument*/NULL);
		V_6 = L_27;
		goto IL_00e1;
	}

IL_00e1:
	{
		List_1_t4162620298 * L_28 = __this->get_records_0();
		NDEFRecord_t2690545556 * L_29 = V_6;
		NullCheck(L_28);
		List_1_Add_m350525082(L_28, L_29, /*hidden argument*/List_1_Add_m350525082_RuntimeMethod_var);
		int32_t L_30 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
	}

IL_00f2:
	{
		int32_t L_31 = V_2;
		int32_t L_32 = V_1;
		if ((((int32_t)L_31) < ((int32_t)L_32)))
		{
			goto IL_002b;
		}
	}
	{
		goto IL_0109;
	}

IL_00fe:
	{
		List_1_t4162620298 * L_33 = (List_1_t4162620298 *)il2cpp_codegen_object_new(List_1_t4162620298_il2cpp_TypeInfo_var);
		List_1__ctor_m1332560254(L_33, /*hidden argument*/List_1__ctor_m1332560254_RuntimeMethod_var);
		__this->set_records_0(L_33);
	}

IL_0109:
	{
		JSONObject_t321714843 * L_34 = ___jsonObject0;
		String_t** L_35 = __this->get_address_of_tagID_1();
		NullCheck(L_34);
		JSONObject_TryGetString_m766921647(L_34, _stringLiteral2127062272, (String_t**)L_35, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_36 = ___jsonObject0;
		NullCheck(L_36);
		JSONObject_TryGetInt_m2083265671(L_36, _stringLiteral605846768, (int32_t*)(&V_7), /*hidden argument*/NULL);
		int32_t L_37 = V_7;
		__this->set_writeState_2(L_37);
		JSONObject_t321714843 * L_38 = ___jsonObject0;
		NullCheck(L_38);
		JSONObject_TryGetInt_m2083265671(L_38, _stringLiteral2329350324, (int32_t*)(&V_8), /*hidden argument*/NULL);
		int32_t L_39 = V_8;
		__this->set_writeError_3(L_39);
		return;
	}
}
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.NDEFMessage::ToJSON()
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * NDEFMessage_ToJSON_m895517323 (NDEFMessage_t279637043 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NDEFMessage_ToJSON_m895517323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t321714843 * V_0 = NULL;
	JSONArray_t4024675823 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		JSONObject_t321714843 * L_0 = (JSONObject_t321714843 *)il2cpp_codegen_object_new(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject__ctor_m3436253305(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONArray_t4024675823 * L_1 = (JSONArray_t4024675823 *)il2cpp_codegen_object_new(JSONArray_t4024675823_il2cpp_TypeInfo_var);
		JSONArray__ctor_m3612236623(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		List_1_t4162620298 * L_2 = __this->get_records_0();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m2774962351(L_2, /*hidden argument*/List_1_get_Count_m2774962351_RuntimeMethod_var);
		V_2 = L_3;
		V_3 = 0;
		goto IL_003f;
	}

IL_001f:
	{
		JSONArray_t4024675823 * L_4 = V_1;
		List_1_t4162620298 * L_5 = __this->get_records_0();
		int32_t L_6 = V_3;
		NullCheck(L_5);
		NDEFRecord_t2690545556 * L_7 = List_1_get_Item_m1387264933(L_5, L_6, /*hidden argument*/List_1_get_Item_m1387264933_RuntimeMethod_var);
		NullCheck(L_7);
		JSONObject_t321714843 * L_8 = VirtFuncInvoker0< JSONObject_t321714843 * >::Invoke(7 /* DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.NDEFRecord::ToJSON() */, L_7);
		JSONValue_t4275860644 * L_9 = JSONValue_op_Implicit_m1136109141(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_4);
		JSONArray_Add_m1346855851(L_4, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_003f:
	{
		int32_t L_11 = V_3;
		int32_t L_12 = V_2;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_001f;
		}
	}
	{
		JSONObject_t321714843 * L_13 = V_0;
		JSONArray_t4024675823 * L_14 = V_1;
		JSONValue_t4275860644 * L_15 = JSONValue_op_Implicit_m216610386(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		JSONObject_Add_m2412535806(L_13, _stringLiteral3952973286, L_15, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_16 = V_0;
		String_t* L_17 = __this->get_tagID_1();
		JSONValue_t4275860644 * L_18 = JSONValue_op_Implicit_m357672470(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		JSONObject_Add_m2412535806(L_16, _stringLiteral2127062272, L_18, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_19 = V_0;
		int32_t L_20 = __this->get_writeState_2();
		JSONValue_t4275860644 * L_21 = JSONValue_op_Implicit_m3141527758(NULL /*static, unused*/, (((double)((double)L_20))), /*hidden argument*/NULL);
		NullCheck(L_19);
		JSONObject_Add_m2412535806(L_19, _stringLiteral605846768, L_21, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_22 = V_0;
		int32_t L_23 = __this->get_writeError_3();
		JSONValue_t4275860644 * L_24 = JSONValue_op_Implicit_m3141527758(NULL /*static, unused*/, (((double)((double)L_23))), /*hidden argument*/NULL);
		NullCheck(L_22);
		JSONObject_Add_m2412535806(L_22, _stringLiteral3619508179, L_24, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_25 = V_0;
		return L_25;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.NDEFPushResult::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFPushResult__ctor_m770393463 (NDEFPushResult_t3422827153 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		NDEFPushResult_ParseJSON_m806318507(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DigitsNFCToolkit.NDEFPushResult::get_Success()
extern "C" IL2CPP_METHOD_ATTR bool NDEFPushResult_get_Success_m1923447426 (NDEFPushResult_t3422827153 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_success_0();
		return L_0;
	}
}
// DigitsNFCToolkit.NDEFMessage DigitsNFCToolkit.NDEFPushResult::get_Message()
extern "C" IL2CPP_METHOD_ATTR NDEFMessage_t279637043 * NDEFPushResult_get_Message_m1817705072 (NDEFPushResult_t3422827153 * __this, const RuntimeMethod* method)
{
	{
		NDEFMessage_t279637043 * L_0 = __this->get_message_1();
		return L_0;
	}
}
// System.Void DigitsNFCToolkit.NDEFPushResult::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFPushResult_ParseJSON_m806318507 (NDEFPushResult_t3422827153 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NDEFPushResult_ParseJSON_m806318507_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t321714843 * V_0 = NULL;
	{
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		bool* L_1 = __this->get_address_of_success_0();
		NullCheck(L_0);
		JSONObject_TryGetBoolean_m3739980323(L_0, _stringLiteral224826876, (bool*)L_1, /*hidden argument*/NULL);
		V_0 = (JSONObject_t321714843 *)NULL;
		JSONObject_t321714843 * L_2 = ___jsonObject0;
		NullCheck(L_2);
		bool L_3 = JSONObject_TryGetObject_m2604600723(L_2, _stringLiteral3253941996, (JSONObject_t321714843 **)(&V_0), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0037;
		}
	}
	{
		JSONObject_t321714843 * L_4 = V_0;
		NDEFMessage_t279637043 * L_5 = (NDEFMessage_t279637043 *)il2cpp_codegen_object_new(NDEFMessage_t279637043_il2cpp_TypeInfo_var);
		NDEFMessage__ctor_m1423095509(L_5, L_4, /*hidden argument*/NULL);
		__this->set_message_1(L_5);
		goto IL_0042;
	}

IL_0037:
	{
		NDEFMessage_t279637043 * L_6 = (NDEFMessage_t279637043 *)il2cpp_codegen_object_new(NDEFMessage_t279637043_il2cpp_TypeInfo_var);
		NDEFMessage__ctor_m2200006917(L_6, /*hidden argument*/NULL);
		__this->set_message_1(L_6);
	}

IL_0042:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.NDEFReadResult::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFReadResult__ctor_m440765780 (NDEFReadResult_t3483243621 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		NDEFReadResult_ParseJSON_m4234342281(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DigitsNFCToolkit.NDEFReadResult::get_Success()
extern "C" IL2CPP_METHOD_ATTR bool NDEFReadResult_get_Success_m1093983849 (NDEFReadResult_t3483243621 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_success_0();
		return L_0;
	}
}
// DigitsNFCToolkit.NDEFReadError DigitsNFCToolkit.NDEFReadResult::get_Error()
extern "C" IL2CPP_METHOD_ATTR int32_t NDEFReadResult_get_Error_m3746343680 (NDEFReadResult_t3483243621 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_error_1();
		return L_0;
	}
}
// DigitsNFCToolkit.NDEFMessage DigitsNFCToolkit.NDEFReadResult::get_Message()
extern "C" IL2CPP_METHOD_ATTR NDEFMessage_t279637043 * NDEFReadResult_get_Message_m152108824 (NDEFReadResult_t3483243621 * __this, const RuntimeMethod* method)
{
	{
		NDEFMessage_t279637043 * L_0 = __this->get_message_2();
		return L_0;
	}
}
// System.String DigitsNFCToolkit.NDEFReadResult::get_TagID()
extern "C" IL2CPP_METHOD_ATTR String_t* NDEFReadResult_get_TagID_m4041318970 (NDEFReadResult_t3483243621 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_tagID_3();
		return L_0;
	}
}
// System.Void DigitsNFCToolkit.NDEFReadResult::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFReadResult_ParseJSON_m4234342281 (NDEFReadResult_t3483243621 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NDEFReadResult_ParseJSON_m4234342281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	JSONObject_t321714843 * V_1 = NULL;
	{
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		bool* L_1 = __this->get_address_of_success_0();
		NullCheck(L_0);
		JSONObject_TryGetBoolean_m3739980323(L_0, _stringLiteral224826876, (bool*)L_1, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_2 = ___jsonObject0;
		NullCheck(L_2);
		JSONObject_TryGetInt_m2083265671(L_2, _stringLiteral95342995, (int32_t*)(&V_0), /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		__this->set_error_1(L_3);
		V_1 = (JSONObject_t321714843 *)NULL;
		JSONObject_t321714843 * L_4 = ___jsonObject0;
		NullCheck(L_4);
		bool L_5 = JSONObject_TryGetObject_m2604600723(L_4, _stringLiteral3253941996, (JSONObject_t321714843 **)(&V_1), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t321714843 * L_6 = V_1;
		NDEFMessage_t279637043 * L_7 = (NDEFMessage_t279637043 *)il2cpp_codegen_object_new(NDEFMessage_t279637043_il2cpp_TypeInfo_var);
		NDEFMessage__ctor_m1423095509(L_7, L_6, /*hidden argument*/NULL);
		__this->set_message_2(L_7);
		goto IL_0057;
	}

IL_004c:
	{
		NDEFMessage_t279637043 * L_8 = (NDEFMessage_t279637043 *)il2cpp_codegen_object_new(NDEFMessage_t279637043_il2cpp_TypeInfo_var);
		NDEFMessage__ctor_m2200006917(L_8, /*hidden argument*/NULL);
		__this->set_message_2(L_8);
	}

IL_0057:
	{
		JSONObject_t321714843 * L_9 = ___jsonObject0;
		String_t** L_10 = __this->get_address_of_tagID_3();
		NullCheck(L_9);
		JSONObject_TryGetString_m766921647(L_9, _stringLiteral2127062272, (String_t**)L_10, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.NDEFRecord::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NDEFRecord__ctor_m2075191861 (NDEFRecord_t2690545556 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// DigitsNFCToolkit.NDEFRecordType DigitsNFCToolkit.NDEFRecord::get_Type()
extern "C" IL2CPP_METHOD_ATTR int32_t NDEFRecord_get_Type_m1191149495 (NDEFRecord_t2690545556 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_type_0();
		return L_0;
	}
}
// System.Void DigitsNFCToolkit.NDEFRecord::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFRecord_ParseJSON_m1625700843 (NDEFRecord_t2690545556 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NDEFRecord_ParseJSON_m1625700843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		NullCheck(L_0);
		JSONObject_TryGetInt_m2083265671(L_0, _stringLiteral3243520166, (int32_t*)(&V_0), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		__this->set_type_0(L_1);
		return;
	}
}
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.NDEFRecord::ToJSON()
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * NDEFRecord_ToJSON_m164142245 (NDEFRecord_t2690545556 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NDEFRecord_ToJSON_m164142245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t321714843 * V_0 = NULL;
	{
		JSONObject_t321714843 * L_0 = (JSONObject_t321714843 *)il2cpp_codegen_object_new(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject__ctor_m3436253305(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t321714843 * L_1 = V_0;
		int32_t L_2 = __this->get_type_0();
		JSONValue_t4275860644 * L_3 = JSONValue_op_Implicit_m3141527758(NULL /*static, unused*/, (((double)((double)L_2))), /*hidden argument*/NULL);
		NullCheck(L_1);
		JSONObject_Add_m2412535806(L_1, _stringLiteral3243520166, L_3, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_4 = V_0;
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.NDEFWriteResult::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFWriteResult__ctor_m404001616 (NDEFWriteResult_t4210562629 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		NDEFWriteResult_ParseJSON_m4251555459(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DigitsNFCToolkit.NDEFWriteResult::get_Success()
extern "C" IL2CPP_METHOD_ATTR bool NDEFWriteResult_get_Success_m3422449045 (NDEFWriteResult_t4210562629 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_success_0();
		return L_0;
	}
}
// DigitsNFCToolkit.NDEFWriteError DigitsNFCToolkit.NDEFWriteResult::get_Error()
extern "C" IL2CPP_METHOD_ATTR int32_t NDEFWriteResult_get_Error_m2217684677 (NDEFWriteResult_t4210562629 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_error_1();
		return L_0;
	}
}
// DigitsNFCToolkit.NDEFMessage DigitsNFCToolkit.NDEFWriteResult::get_Message()
extern "C" IL2CPP_METHOD_ATTR NDEFMessage_t279637043 * NDEFWriteResult_get_Message_m1148114373 (NDEFWriteResult_t4210562629 * __this, const RuntimeMethod* method)
{
	{
		NDEFMessage_t279637043 * L_0 = __this->get_message_2();
		return L_0;
	}
}
// System.String DigitsNFCToolkit.NDEFWriteResult::get_TagID()
extern "C" IL2CPP_METHOD_ATTR String_t* NDEFWriteResult_get_TagID_m1976340487 (NDEFWriteResult_t4210562629 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_tagID_3();
		return L_0;
	}
}
// System.Void DigitsNFCToolkit.NDEFWriteResult::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NDEFWriteResult_ParseJSON_m4251555459 (NDEFWriteResult_t4210562629 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NDEFWriteResult_ParseJSON_m4251555459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	JSONObject_t321714843 * V_1 = NULL;
	{
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		bool* L_1 = __this->get_address_of_success_0();
		NullCheck(L_0);
		JSONObject_TryGetBoolean_m3739980323(L_0, _stringLiteral224826876, (bool*)L_1, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_2 = ___jsonObject0;
		NullCheck(L_2);
		JSONObject_TryGetInt_m2083265671(L_2, _stringLiteral95342995, (int32_t*)(&V_0), /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		__this->set_error_1(L_3);
		V_1 = (JSONObject_t321714843 *)NULL;
		JSONObject_t321714843 * L_4 = ___jsonObject0;
		NullCheck(L_4);
		bool L_5 = JSONObject_TryGetObject_m2604600723(L_4, _stringLiteral3253941996, (JSONObject_t321714843 **)(&V_1), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004c;
		}
	}
	{
		JSONObject_t321714843 * L_6 = V_1;
		NDEFMessage_t279637043 * L_7 = (NDEFMessage_t279637043 *)il2cpp_codegen_object_new(NDEFMessage_t279637043_il2cpp_TypeInfo_var);
		NDEFMessage__ctor_m1423095509(L_7, L_6, /*hidden argument*/NULL);
		__this->set_message_2(L_7);
		goto IL_0057;
	}

IL_004c:
	{
		NDEFMessage_t279637043 * L_8 = (NDEFMessage_t279637043 *)il2cpp_codegen_object_new(NDEFMessage_t279637043_il2cpp_TypeInfo_var);
		NDEFMessage__ctor_m2200006917(L_8, /*hidden argument*/NULL);
		__this->set_message_2(L_8);
	}

IL_0057:
	{
		JSONObject_t321714843 * L_9 = ___jsonObject0;
		String_t** L_10 = __this->get_address_of_tagID_3();
		NullCheck(L_9);
		JSONObject_TryGetString_m766921647(L_9, _stringLiteral2127062272, (String_t**)L_10, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.NFCTag::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NFCTag__ctor_m976544915 (NFCTag_t2820711232 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		NFCTag_ParseJSON_m4194543403(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String DigitsNFCToolkit.NFCTag::get_ID()
extern "C" IL2CPP_METHOD_ATTR String_t* NFCTag_get_ID_m2099905974 (NFCTag_t2820711232 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_id_0();
		return L_0;
	}
}
// DigitsNFCToolkit.NFCTechnology[] DigitsNFCToolkit.NFCTag::get_Technologies()
extern "C" IL2CPP_METHOD_ATTR NFCTechnologyU5BU5D_t328493574* NFCTag_get_Technologies_m134455690 (NFCTag_t2820711232 * __this, const RuntimeMethod* method)
{
	{
		NFCTechnologyU5BU5D_t328493574* L_0 = __this->get_technologies_1();
		return L_0;
	}
}
// System.String DigitsNFCToolkit.NFCTag::get_Manufacturer()
extern "C" IL2CPP_METHOD_ATTR String_t* NFCTag_get_Manufacturer_m3670434854 (NFCTag_t2820711232 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_manufacturer_2();
		return L_0;
	}
}
// System.Boolean DigitsNFCToolkit.NFCTag::get_Writable()
extern "C" IL2CPP_METHOD_ATTR bool NFCTag_get_Writable_m3150305762 (NFCTag_t2820711232 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_writable_3();
		return L_0;
	}
}
// System.Int32 DigitsNFCToolkit.NFCTag::get_MaxWriteSize()
extern "C" IL2CPP_METHOD_ATTR int32_t NFCTag_get_MaxWriteSize_m697043576 (NFCTag_t2820711232 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_maxWriteSize_4();
		return L_0;
	}
}
// System.Void DigitsNFCToolkit.NFCTag::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void NFCTag_ParseJSON_m4194543403 (NFCTag_t2820711232 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NFCTag_ParseJSON_m4194543403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONArray_t4024675823 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		String_t** L_1 = __this->get_address_of_id_0();
		NullCheck(L_0);
		JSONObject_TryGetString_m766921647(L_0, _stringLiteral3454449607, (String_t**)L_1, /*hidden argument*/NULL);
		JSONArray_t4024675823 * L_2 = (JSONArray_t4024675823 *)il2cpp_codegen_object_new(JSONArray_t4024675823_il2cpp_TypeInfo_var);
		JSONArray__ctor_m3612236623(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		JSONObject_t321714843 * L_3 = ___jsonObject0;
		NullCheck(L_3);
		bool L_4 = JSONObject_TryGetArray_m3928523712(L_3, _stringLiteral992023537, (JSONArray_t4024675823 **)(&V_0), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006a;
		}
	}
	{
		JSONArray_t4024675823 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = JSONArray_get_Length_m1921089944(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7 = V_1;
		NFCTechnologyU5BU5D_t328493574* L_8 = (NFCTechnologyU5BU5D_t328493574*)SZArrayNew(NFCTechnologyU5BU5D_t328493574_il2cpp_TypeInfo_var, (uint32_t)L_7);
		__this->set_technologies_1(L_8);
		V_2 = 0;
		goto IL_005e;
	}

IL_0044:
	{
		JSONArray_t4024675823 * L_9 = V_0;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		JSONValue_t4275860644 * L_11 = JSONArray_get_Item_m1052458067(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = JSONValue_get_Integer_m3463426723(L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		NFCTechnologyU5BU5D_t328493574* L_13 = __this->get_technologies_1();
		int32_t L_14 = V_2;
		int32_t L_15 = V_3;
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (int32_t)L_15);
		int32_t L_16 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
	}

IL_005e:
	{
		int32_t L_17 = V_2;
		int32_t L_18 = V_1;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0044;
		}
	}
	{
		goto IL_0076;
	}

IL_006a:
	{
		NFCTechnologyU5BU5D_t328493574* L_19 = (NFCTechnologyU5BU5D_t328493574*)SZArrayNew(NFCTechnologyU5BU5D_t328493574_il2cpp_TypeInfo_var, (uint32_t)0);
		__this->set_technologies_1(L_19);
	}

IL_0076:
	{
		JSONObject_t321714843 * L_20 = ___jsonObject0;
		String_t** L_21 = __this->get_address_of_manufacturer_2();
		NullCheck(L_20);
		JSONObject_TryGetString_m766921647(L_20, _stringLiteral1997836753, (String_t**)L_21, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_22 = ___jsonObject0;
		bool* L_23 = __this->get_address_of_writable_3();
		NullCheck(L_22);
		JSONObject_TryGetBoolean_m3739980323(L_22, _stringLiteral891850287, (bool*)L_23, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_24 = ___jsonObject0;
		int32_t* L_25 = __this->get_address_of_maxWriteSize_4();
		NullCheck(L_24);
		JSONObject_TryGetInt_m2083265671(L_24, _stringLiteral3070491693, (int32_t*)L_25, /*hidden argument*/NULL);
		return;
	}
}
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.NFCTag::ToJSON()
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * NFCTag_ToJSON_m1354994276 (NFCTag_t2820711232 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NFCTag_ToJSON_m1354994276_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t321714843 * V_0 = NULL;
	JSONArray_t4024675823 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		JSONObject_t321714843 * L_0 = (JSONObject_t321714843 *)il2cpp_codegen_object_new(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject__ctor_m3436253305(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t321714843 * L_1 = V_0;
		String_t* L_2 = __this->get_id_0();
		JSONValue_t4275860644 * L_3 = JSONValue_op_Implicit_m357672470(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		JSONObject_Add_m2412535806(L_1, _stringLiteral3454449607, L_3, /*hidden argument*/NULL);
		JSONArray_t4024675823 * L_4 = (JSONArray_t4024675823 *)il2cpp_codegen_object_new(JSONArray_t4024675823_il2cpp_TypeInfo_var);
		JSONArray__ctor_m3612236623(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		NFCTechnologyU5BU5D_t328493574* L_5 = __this->get_technologies_1();
		NullCheck(L_5);
		V_2 = (((int32_t)((int32_t)(((RuntimeArray *)L_5)->max_length))));
		V_3 = 0;
		goto IL_004a;
	}

IL_0032:
	{
		JSONArray_t4024675823 * L_6 = V_1;
		NFCTechnologyU5BU5D_t328493574* L_7 = __this->get_technologies_1();
		int32_t L_8 = V_3;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		int32_t L_10 = (int32_t)(L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		JSONValue_t4275860644 * L_11 = JSONValue_op_Implicit_m3141527758(NULL /*static, unused*/, (((double)((double)L_10))), /*hidden argument*/NULL);
		NullCheck(L_6);
		JSONArray_Add_m1346855851(L_6, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_004a:
	{
		int32_t L_13 = V_3;
		int32_t L_14 = V_2;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0032;
		}
	}
	{
		JSONObject_t321714843 * L_15 = V_0;
		JSONArray_t4024675823 * L_16 = V_1;
		JSONValue_t4275860644 * L_17 = JSONValue_op_Implicit_m216610386(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		JSONObject_Add_m2412535806(L_15, _stringLiteral992023537, L_17, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_18 = V_0;
		String_t* L_19 = __this->get_manufacturer_2();
		JSONValue_t4275860644 * L_20 = JSONValue_op_Implicit_m357672470(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		JSONObject_Add_m2412535806(L_18, _stringLiteral1997836753, L_20, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_21 = V_0;
		bool L_22 = __this->get_writable_3();
		JSONValue_t4275860644 * L_23 = JSONValue_op_Implicit_m2321309250(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		JSONObject_Add_m2412535806(L_21, _stringLiteral891850287, L_23, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_24 = V_0;
		int32_t L_25 = __this->get_maxWriteSize_4();
		JSONValue_t4275860644 * L_26 = JSONValue_op_Implicit_m3141527758(NULL /*static, unused*/, (((double)((double)L_25))), /*hidden argument*/NULL);
		NullCheck(L_24);
		JSONObject_Add_m2412535806(L_24, _stringLiteral3070491693, L_26, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_27 = V_0;
		return L_27;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.NativeNFC::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NativeNFC__ctor_m920500560 (NativeNFC_t1941597496 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::add_onNFCTagDetected(DigitsNFCToolkit.OnNFCTagDetected)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_add_onNFCTagDetected_m1360338169 (NativeNFC_t1941597496 * __this, OnNFCTagDetected_t3189675727 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeNFC_add_onNFCTagDetected_m1360338169_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OnNFCTagDetected_t3189675727 * V_0 = NULL;
	OnNFCTagDetected_t3189675727 * V_1 = NULL;
	{
		OnNFCTagDetected_t3189675727 * L_0 = __this->get_onNFCTagDetected_4();
		V_0 = L_0;
	}

IL_0007:
	{
		OnNFCTagDetected_t3189675727 * L_1 = V_0;
		V_1 = L_1;
		OnNFCTagDetected_t3189675727 ** L_2 = __this->get_address_of_onNFCTagDetected_4();
		OnNFCTagDetected_t3189675727 * L_3 = V_1;
		OnNFCTagDetected_t3189675727 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		OnNFCTagDetected_t3189675727 * L_6 = V_0;
		OnNFCTagDetected_t3189675727 * L_7 = InterlockedCompareExchangeImpl<OnNFCTagDetected_t3189675727 *>((OnNFCTagDetected_t3189675727 **)L_2, ((OnNFCTagDetected_t3189675727 *)CastclassSealed((RuntimeObject*)L_5, OnNFCTagDetected_t3189675727_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		OnNFCTagDetected_t3189675727 * L_8 = V_0;
		OnNFCTagDetected_t3189675727 * L_9 = V_1;
		if ((!(((RuntimeObject*)(OnNFCTagDetected_t3189675727 *)L_8) == ((RuntimeObject*)(OnNFCTagDetected_t3189675727 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::remove_onNFCTagDetected(DigitsNFCToolkit.OnNFCTagDetected)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_remove_onNFCTagDetected_m4260315192 (NativeNFC_t1941597496 * __this, OnNFCTagDetected_t3189675727 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeNFC_remove_onNFCTagDetected_m4260315192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OnNFCTagDetected_t3189675727 * V_0 = NULL;
	OnNFCTagDetected_t3189675727 * V_1 = NULL;
	{
		OnNFCTagDetected_t3189675727 * L_0 = __this->get_onNFCTagDetected_4();
		V_0 = L_0;
	}

IL_0007:
	{
		OnNFCTagDetected_t3189675727 * L_1 = V_0;
		V_1 = L_1;
		OnNFCTagDetected_t3189675727 ** L_2 = __this->get_address_of_onNFCTagDetected_4();
		OnNFCTagDetected_t3189675727 * L_3 = V_1;
		OnNFCTagDetected_t3189675727 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		OnNFCTagDetected_t3189675727 * L_6 = V_0;
		OnNFCTagDetected_t3189675727 * L_7 = InterlockedCompareExchangeImpl<OnNFCTagDetected_t3189675727 *>((OnNFCTagDetected_t3189675727 **)L_2, ((OnNFCTagDetected_t3189675727 *)CastclassSealed((RuntimeObject*)L_5, OnNFCTagDetected_t3189675727_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		OnNFCTagDetected_t3189675727 * L_8 = V_0;
		OnNFCTagDetected_t3189675727 * L_9 = V_1;
		if ((!(((RuntimeObject*)(OnNFCTagDetected_t3189675727 *)L_8) == ((RuntimeObject*)(OnNFCTagDetected_t3189675727 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::add_onNDEFReadFinished(DigitsNFCToolkit.OnNDEFReadFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_add_onNDEFReadFinished_m1875928045 (NativeNFC_t1941597496 * __this, OnNDEFReadFinished_t1327886840 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeNFC_add_onNDEFReadFinished_m1875928045_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OnNDEFReadFinished_t1327886840 * V_0 = NULL;
	OnNDEFReadFinished_t1327886840 * V_1 = NULL;
	{
		OnNDEFReadFinished_t1327886840 * L_0 = __this->get_onNDEFReadFinished_5();
		V_0 = L_0;
	}

IL_0007:
	{
		OnNDEFReadFinished_t1327886840 * L_1 = V_0;
		V_1 = L_1;
		OnNDEFReadFinished_t1327886840 ** L_2 = __this->get_address_of_onNDEFReadFinished_5();
		OnNDEFReadFinished_t1327886840 * L_3 = V_1;
		OnNDEFReadFinished_t1327886840 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		OnNDEFReadFinished_t1327886840 * L_6 = V_0;
		OnNDEFReadFinished_t1327886840 * L_7 = InterlockedCompareExchangeImpl<OnNDEFReadFinished_t1327886840 *>((OnNDEFReadFinished_t1327886840 **)L_2, ((OnNDEFReadFinished_t1327886840 *)CastclassSealed((RuntimeObject*)L_5, OnNDEFReadFinished_t1327886840_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		OnNDEFReadFinished_t1327886840 * L_8 = V_0;
		OnNDEFReadFinished_t1327886840 * L_9 = V_1;
		if ((!(((RuntimeObject*)(OnNDEFReadFinished_t1327886840 *)L_8) == ((RuntimeObject*)(OnNDEFReadFinished_t1327886840 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::remove_onNDEFReadFinished(DigitsNFCToolkit.OnNDEFReadFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_remove_onNDEFReadFinished_m3344582477 (NativeNFC_t1941597496 * __this, OnNDEFReadFinished_t1327886840 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeNFC_remove_onNDEFReadFinished_m3344582477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OnNDEFReadFinished_t1327886840 * V_0 = NULL;
	OnNDEFReadFinished_t1327886840 * V_1 = NULL;
	{
		OnNDEFReadFinished_t1327886840 * L_0 = __this->get_onNDEFReadFinished_5();
		V_0 = L_0;
	}

IL_0007:
	{
		OnNDEFReadFinished_t1327886840 * L_1 = V_0;
		V_1 = L_1;
		OnNDEFReadFinished_t1327886840 ** L_2 = __this->get_address_of_onNDEFReadFinished_5();
		OnNDEFReadFinished_t1327886840 * L_3 = V_1;
		OnNDEFReadFinished_t1327886840 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		OnNDEFReadFinished_t1327886840 * L_6 = V_0;
		OnNDEFReadFinished_t1327886840 * L_7 = InterlockedCompareExchangeImpl<OnNDEFReadFinished_t1327886840 *>((OnNDEFReadFinished_t1327886840 **)L_2, ((OnNDEFReadFinished_t1327886840 *)CastclassSealed((RuntimeObject*)L_5, OnNDEFReadFinished_t1327886840_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		OnNDEFReadFinished_t1327886840 * L_8 = V_0;
		OnNDEFReadFinished_t1327886840 * L_9 = V_1;
		if ((!(((RuntimeObject*)(OnNDEFReadFinished_t1327886840 *)L_8) == ((RuntimeObject*)(OnNDEFReadFinished_t1327886840 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::add_onNDEFWriteFinished(DigitsNFCToolkit.OnNDEFWriteFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_add_onNDEFWriteFinished_m3934953293 (NativeNFC_t1941597496 * __this, OnNDEFWriteFinished_t4102039599 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeNFC_add_onNDEFWriteFinished_m3934953293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OnNDEFWriteFinished_t4102039599 * V_0 = NULL;
	OnNDEFWriteFinished_t4102039599 * V_1 = NULL;
	{
		OnNDEFWriteFinished_t4102039599 * L_0 = __this->get_onNDEFWriteFinished_6();
		V_0 = L_0;
	}

IL_0007:
	{
		OnNDEFWriteFinished_t4102039599 * L_1 = V_0;
		V_1 = L_1;
		OnNDEFWriteFinished_t4102039599 ** L_2 = __this->get_address_of_onNDEFWriteFinished_6();
		OnNDEFWriteFinished_t4102039599 * L_3 = V_1;
		OnNDEFWriteFinished_t4102039599 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		OnNDEFWriteFinished_t4102039599 * L_6 = V_0;
		OnNDEFWriteFinished_t4102039599 * L_7 = InterlockedCompareExchangeImpl<OnNDEFWriteFinished_t4102039599 *>((OnNDEFWriteFinished_t4102039599 **)L_2, ((OnNDEFWriteFinished_t4102039599 *)CastclassSealed((RuntimeObject*)L_5, OnNDEFWriteFinished_t4102039599_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		OnNDEFWriteFinished_t4102039599 * L_8 = V_0;
		OnNDEFWriteFinished_t4102039599 * L_9 = V_1;
		if ((!(((RuntimeObject*)(OnNDEFWriteFinished_t4102039599 *)L_8) == ((RuntimeObject*)(OnNDEFWriteFinished_t4102039599 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::remove_onNDEFWriteFinished(DigitsNFCToolkit.OnNDEFWriteFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_remove_onNDEFWriteFinished_m1549713225 (NativeNFC_t1941597496 * __this, OnNDEFWriteFinished_t4102039599 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeNFC_remove_onNDEFWriteFinished_m1549713225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OnNDEFWriteFinished_t4102039599 * V_0 = NULL;
	OnNDEFWriteFinished_t4102039599 * V_1 = NULL;
	{
		OnNDEFWriteFinished_t4102039599 * L_0 = __this->get_onNDEFWriteFinished_6();
		V_0 = L_0;
	}

IL_0007:
	{
		OnNDEFWriteFinished_t4102039599 * L_1 = V_0;
		V_1 = L_1;
		OnNDEFWriteFinished_t4102039599 ** L_2 = __this->get_address_of_onNDEFWriteFinished_6();
		OnNDEFWriteFinished_t4102039599 * L_3 = V_1;
		OnNDEFWriteFinished_t4102039599 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		OnNDEFWriteFinished_t4102039599 * L_6 = V_0;
		OnNDEFWriteFinished_t4102039599 * L_7 = InterlockedCompareExchangeImpl<OnNDEFWriteFinished_t4102039599 *>((OnNDEFWriteFinished_t4102039599 **)L_2, ((OnNDEFWriteFinished_t4102039599 *)CastclassSealed((RuntimeObject*)L_5, OnNDEFWriteFinished_t4102039599_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		OnNDEFWriteFinished_t4102039599 * L_8 = V_0;
		OnNDEFWriteFinished_t4102039599 * L_9 = V_1;
		if ((!(((RuntimeObject*)(OnNDEFWriteFinished_t4102039599 *)L_8) == ((RuntimeObject*)(OnNDEFWriteFinished_t4102039599 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::add_onNDEFPushFinished(DigitsNFCToolkit.OnNDEFPushFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_add_onNDEFPushFinished_m4177872223 (NativeNFC_t1941597496 * __this, OnNDEFPushFinished_t4279917764 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeNFC_add_onNDEFPushFinished_m4177872223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OnNDEFPushFinished_t4279917764 * V_0 = NULL;
	OnNDEFPushFinished_t4279917764 * V_1 = NULL;
	{
		OnNDEFPushFinished_t4279917764 * L_0 = __this->get_onNDEFPushFinished_7();
		V_0 = L_0;
	}

IL_0007:
	{
		OnNDEFPushFinished_t4279917764 * L_1 = V_0;
		V_1 = L_1;
		OnNDEFPushFinished_t4279917764 ** L_2 = __this->get_address_of_onNDEFPushFinished_7();
		OnNDEFPushFinished_t4279917764 * L_3 = V_1;
		OnNDEFPushFinished_t4279917764 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		OnNDEFPushFinished_t4279917764 * L_6 = V_0;
		OnNDEFPushFinished_t4279917764 * L_7 = InterlockedCompareExchangeImpl<OnNDEFPushFinished_t4279917764 *>((OnNDEFPushFinished_t4279917764 **)L_2, ((OnNDEFPushFinished_t4279917764 *)CastclassSealed((RuntimeObject*)L_5, OnNDEFPushFinished_t4279917764_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		OnNDEFPushFinished_t4279917764 * L_8 = V_0;
		OnNDEFPushFinished_t4279917764 * L_9 = V_1;
		if ((!(((RuntimeObject*)(OnNDEFPushFinished_t4279917764 *)L_8) == ((RuntimeObject*)(OnNDEFPushFinished_t4279917764 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::remove_onNDEFPushFinished(DigitsNFCToolkit.OnNDEFPushFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_remove_onNDEFPushFinished_m1864805945 (NativeNFC_t1941597496 * __this, OnNDEFPushFinished_t4279917764 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeNFC_remove_onNDEFPushFinished_m1864805945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OnNDEFPushFinished_t4279917764 * V_0 = NULL;
	OnNDEFPushFinished_t4279917764 * V_1 = NULL;
	{
		OnNDEFPushFinished_t4279917764 * L_0 = __this->get_onNDEFPushFinished_7();
		V_0 = L_0;
	}

IL_0007:
	{
		OnNDEFPushFinished_t4279917764 * L_1 = V_0;
		V_1 = L_1;
		OnNDEFPushFinished_t4279917764 ** L_2 = __this->get_address_of_onNDEFPushFinished_7();
		OnNDEFPushFinished_t4279917764 * L_3 = V_1;
		OnNDEFPushFinished_t4279917764 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		OnNDEFPushFinished_t4279917764 * L_6 = V_0;
		OnNDEFPushFinished_t4279917764 * L_7 = InterlockedCompareExchangeImpl<OnNDEFPushFinished_t4279917764 *>((OnNDEFPushFinished_t4279917764 **)L_2, ((OnNDEFPushFinished_t4279917764 *)CastclassSealed((RuntimeObject*)L_5, OnNDEFPushFinished_t4279917764_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		OnNDEFPushFinished_t4279917764 * L_8 = V_0;
		OnNDEFPushFinished_t4279917764 * L_9 = V_1;
		if ((!(((RuntimeObject*)(OnNDEFPushFinished_t4279917764 *)L_8) == ((RuntimeObject*)(OnNDEFPushFinished_t4279917764 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::add_NFCTagDetected(DigitsNFCToolkit.OnNFCTagDetected)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_add_NFCTagDetected_m2650456630 (NativeNFC_t1941597496 * __this, OnNFCTagDetected_t3189675727 * ___value0, const RuntimeMethod* method)
{
	{
		OnNFCTagDetected_t3189675727 * L_0 = ___value0;
		NativeNFC_add_onNFCTagDetected_m1360338169(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::remove_NFCTagDetected(DigitsNFCToolkit.OnNFCTagDetected)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_remove_NFCTagDetected_m2280126304 (NativeNFC_t1941597496 * __this, OnNFCTagDetected_t3189675727 * ___value0, const RuntimeMethod* method)
{
	{
		OnNFCTagDetected_t3189675727 * L_0 = ___value0;
		NativeNFC_remove_onNFCTagDetected_m4260315192(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::add_NDEFReadFinished(DigitsNFCToolkit.OnNDEFReadFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_add_NDEFReadFinished_m2374080351 (NativeNFC_t1941597496 * __this, OnNDEFReadFinished_t1327886840 * ___value0, const RuntimeMethod* method)
{
	{
		OnNDEFReadFinished_t1327886840 * L_0 = ___value0;
		NativeNFC_add_onNDEFReadFinished_m1875928045(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::remove_NDEFReadFinished(DigitsNFCToolkit.OnNDEFReadFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_remove_NDEFReadFinished_m596925152 (NativeNFC_t1941597496 * __this, OnNDEFReadFinished_t1327886840 * ___value0, const RuntimeMethod* method)
{
	{
		OnNDEFReadFinished_t1327886840 * L_0 = ___value0;
		NativeNFC_remove_onNDEFReadFinished_m3344582477(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::add_NDEFWriteFinished(DigitsNFCToolkit.OnNDEFWriteFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_add_NDEFWriteFinished_m3503282290 (NativeNFC_t1941597496 * __this, OnNDEFWriteFinished_t4102039599 * ___value0, const RuntimeMethod* method)
{
	{
		OnNDEFWriteFinished_t4102039599 * L_0 = ___value0;
		NativeNFC_add_onNDEFWriteFinished_m3934953293(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::remove_NDEFWriteFinished(DigitsNFCToolkit.OnNDEFWriteFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_remove_NDEFWriteFinished_m4241802593 (NativeNFC_t1941597496 * __this, OnNDEFWriteFinished_t4102039599 * ___value0, const RuntimeMethod* method)
{
	{
		OnNDEFWriteFinished_t4102039599 * L_0 = ___value0;
		NativeNFC_remove_onNDEFWriteFinished_m1549713225(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::add_NDEFPushFinished(DigitsNFCToolkit.OnNDEFPushFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_add_NDEFPushFinished_m621482237 (NativeNFC_t1941597496 * __this, OnNDEFPushFinished_t4279917764 * ___value0, const RuntimeMethod* method)
{
	{
		OnNDEFPushFinished_t4279917764 * L_0 = ___value0;
		NativeNFC_add_onNDEFPushFinished_m4177872223(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::remove_NDEFPushFinished(DigitsNFCToolkit.OnNDEFPushFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_remove_NDEFPushFinished_m199227403 (NativeNFC_t1941597496 * __this, OnNDEFPushFinished_t4279917764 * ___value0, const RuntimeMethod* method)
{
	{
		OnNDEFPushFinished_t4279917764 * L_0 = ___value0;
		NativeNFC_remove_onNDEFPushFinished_m1864805945(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DigitsNFCToolkit.NativeNFC::get_ResetOnTimeout()
extern "C" IL2CPP_METHOD_ATTR bool NativeNFC_get_ResetOnTimeout_m1804879484 (NativeNFC_t1941597496 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_resetOnTimeout_8();
		return L_0;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::set_ResetOnTimeout(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_set_ResetOnTimeout_m1401711570 (NativeNFC_t1941597496 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_resetOnTimeout_8(L_0);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::OnNFCTagDetected(System.String)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_OnNFCTagDetected_m3702881045 (NativeNFC_t1941597496 * __this, String_t* ___tagJSON0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeNFC_OnNFCTagDetected_m3702881045_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t321714843 * V_0 = NULL;
	NFCTag_t2820711232 * V_1 = NULL;
	{
		String_t* L_0 = ___tagJSON0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_1 = JSONObject_Parse_m2486419426(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONObject_t321714843 * L_2 = V_0;
		NFCTag_t2820711232 * L_3 = (NFCTag_t2820711232 *)il2cpp_codegen_object_new(NFCTag_t2820711232_il2cpp_TypeInfo_var);
		NFCTag__ctor_m976544915(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		OnNFCTagDetected_t3189675727 * L_4 = __this->get_onNFCTagDetected_4();
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		OnNFCTagDetected_t3189675727 * L_5 = __this->get_onNFCTagDetected_4();
		NFCTag_t2820711232 * L_6 = V_1;
		NullCheck(L_5);
		OnNFCTagDetected_Invoke_m4012074991(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::OnNDEFReadFinished(System.String)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_OnNDEFReadFinished_m4172086545 (NativeNFC_t1941597496 * __this, String_t* ___resultJSON0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeNFC_OnNDEFReadFinished_m4172086545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t321714843 * V_0 = NULL;
	NDEFReadResult_t3483243621 * V_1 = NULL;
	{
		String_t* L_0 = ___resultJSON0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_1 = JSONObject_Parse_m2486419426(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONObject_t321714843 * L_2 = V_0;
		NDEFReadResult_t3483243621 * L_3 = (NDEFReadResult_t3483243621 *)il2cpp_codegen_object_new(NDEFReadResult_t3483243621_il2cpp_TypeInfo_var);
		NDEFReadResult__ctor_m440765780(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		OnNDEFReadFinished_t1327886840 * L_4 = __this->get_onNDEFReadFinished_5();
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		OnNDEFReadFinished_t1327886840 * L_5 = __this->get_onNDEFReadFinished_5();
		NDEFReadResult_t3483243621 * L_6 = V_1;
		NullCheck(L_5);
		OnNDEFReadFinished_Invoke_m990719778(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::OnNDEFWriteFinished(System.String)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_OnNDEFWriteFinished_m400958761 (NativeNFC_t1941597496 * __this, String_t* ___resultJSON0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeNFC_OnNDEFWriteFinished_m400958761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t321714843 * V_0 = NULL;
	NDEFWriteResult_t4210562629 * V_1 = NULL;
	{
		String_t* L_0 = ___resultJSON0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_1 = JSONObject_Parse_m2486419426(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONObject_t321714843 * L_2 = V_0;
		NDEFWriteResult_t4210562629 * L_3 = (NDEFWriteResult_t4210562629 *)il2cpp_codegen_object_new(NDEFWriteResult_t4210562629_il2cpp_TypeInfo_var);
		NDEFWriteResult__ctor_m404001616(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		OnNDEFWriteFinished_t4102039599 * L_4 = __this->get_onNDEFWriteFinished_6();
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		OnNDEFWriteFinished_t4102039599 * L_5 = __this->get_onNDEFWriteFinished_6();
		NDEFWriteResult_t4210562629 * L_6 = V_1;
		NullCheck(L_5);
		OnNDEFWriteFinished_Invoke_m3001009917(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFC::OnNDEFPushFinished(System.String)
extern "C" IL2CPP_METHOD_ATTR void NativeNFC_OnNDEFPushFinished_m516808229 (NativeNFC_t1941597496 * __this, String_t* ___resultJSON0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeNFC_OnNDEFPushFinished_m516808229_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t321714843 * V_0 = NULL;
	NDEFPushResult_t3422827153 * V_1 = NULL;
	{
		String_t* L_0 = ___resultJSON0;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t321714843_il2cpp_TypeInfo_var);
		JSONObject_t321714843 * L_1 = JSONObject_Parse_m2486419426(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONObject_t321714843 * L_2 = V_0;
		NDEFPushResult_t3422827153 * L_3 = (NDEFPushResult_t3422827153 *)il2cpp_codegen_object_new(NDEFPushResult_t3422827153_il2cpp_TypeInfo_var);
		NDEFPushResult__ctor_m770393463(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		OnNDEFPushFinished_t4279917764 * L_4 = __this->get_onNDEFPushFinished_7();
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		OnNDEFPushFinished_t4279917764 * L_5 = __this->get_onNDEFPushFinished_7();
		NDEFPushResult_t3422827153 * L_6 = V_1;
		NullCheck(L_5);
		OnNDEFPushFinished_Invoke_m1414772192(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.NativeNFCManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager__ctor_m3362296162 (NativeNFCManager_t351225459 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// DigitsNFCToolkit.NativeNFCManager DigitsNFCToolkit.NativeNFCManager::get_Instance()
extern "C" IL2CPP_METHOD_ATTR NativeNFCManager_t351225459 * NativeNFCManager_get_Instance_m711561572 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeNFCManager_get_Instance_m711561572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = ((NativeNFCManager_t351225459_StaticFields*)il2cpp_codegen_static_fields_for(NativeNFCManager_t351225459_il2cpp_TypeInfo_var))->get_instance_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		NativeNFCManager_t351225459 * L_2 = Object_FindObjectOfType_TisNativeNFCManager_t351225459_m2303328554(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisNativeNFCManager_t351225459_m2303328554_RuntimeMethod_var);
		((NativeNFCManager_t351225459_StaticFields*)il2cpp_codegen_static_fields_for(NativeNFCManager_t351225459_il2cpp_TypeInfo_var))->set_instance_5(L_2);
		NativeNFCManager_t351225459 * L_3 = ((NativeNFCManager_t351225459_StaticFields*)il2cpp_codegen_static_fields_for(NativeNFCManager_t351225459_il2cpp_TypeInfo_var))->get_instance_5();
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		GameObject_t1113636619 * L_5 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_5, _stringLiteral1410634818, /*hidden argument*/NULL);
		V_0 = L_5;
		GameObject_t1113636619 * L_6 = V_0;
		NullCheck(L_6);
		NativeNFCManager_t351225459 * L_7 = GameObject_AddComponent_TisNativeNFCManager_t351225459_m2397571266(L_6, /*hidden argument*/GameObject_AddComponent_TisNativeNFCManager_t351225459_m2397571266_RuntimeMethod_var);
		((NativeNFCManager_t351225459_StaticFields*)il2cpp_codegen_static_fields_for(NativeNFCManager_t351225459_il2cpp_TypeInfo_var))->set_instance_5(L_7);
	}

IL_0040:
	{
		NativeNFCManager_t351225459 * L_8 = ((NativeNFCManager_t351225459_StaticFields*)il2cpp_codegen_static_fields_for(NativeNFCManager_t351225459_il2cpp_TypeInfo_var))->get_instance_5();
		return L_8;
	}
}
// DigitsNFCToolkit.NativeNFC DigitsNFCToolkit.NativeNFCManager::get_NFC()
extern "C" IL2CPP_METHOD_ATTR NativeNFC_t1941597496 * NativeNFCManager_get_NFC_m290841912 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		return L_1;
	}
}
// System.Boolean DigitsNFCToolkit.NativeNFCManager::get_ResetOnTimeout()
extern "C" IL2CPP_METHOD_ATTR bool NativeNFCManager_get_ResetOnTimeout_m2947000604 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		NativeNFC_t1941597496 * L_0 = NativeNFCManager_get_NFC_m290841912(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean DigitsNFCToolkit.NativeNFC::get_ResetOnTimeout() */, L_0);
		return L_1;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::set_ResetOnTimeout(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_set_ResetOnTimeout_m718426537 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	{
		NativeNFC_t1941597496 * L_0 = NativeNFCManager_get_NFC_m290841912(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = ___value0;
		NullCheck(L_0);
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void DigitsNFCToolkit.NativeNFC::set_ResetOnTimeout(System.Boolean) */, L_0, L_1);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::Awake()
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_Awake_m2031265650 (NativeNFCManager_t351225459 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeNFCManager_Awake_m2031265650_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IOSNFC_t293874181 * L_1 = GameObject_AddComponent_TisIOSNFC_t293874181_m566898834(L_0, /*hidden argument*/GameObject_AddComponent_TisIOSNFC_t293874181_m566898834_RuntimeMethod_var);
		__this->set_nfc_6(L_1);
		NativeNFC_t1941597496 * L_2 = __this->get_nfc_6();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(6 /* System.Void DigitsNFCToolkit.NativeNFC::Initialize() */, L_2);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_OnDestroy_m1812899065 (NativeNFCManager_t351225459 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeNFCManager_OnDestroy_m1812899065_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((NativeNFCManager_t351225459_StaticFields*)il2cpp_codegen_static_fields_for(NativeNFCManager_t351225459_il2cpp_TypeInfo_var))->set_instance_5((NativeNFCManager_t351225459 *)NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::TryDestroy()
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_TryDestroy_m3564277066 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeNFCManager_TryDestroy_m3564277066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NativeNFCManager_t351225459 * L_0 = ((NativeNFCManager_t351225459_StaticFields*)il2cpp_codegen_static_fields_for(NativeNFCManager_t351225459_il2cpp_TypeInfo_var))->get_instance_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		NativeNFCManager_t351225459 * L_2 = ((NativeNFCManager_t351225459_StaticFields*)il2cpp_codegen_static_fields_for(NativeNFCManager_t351225459_il2cpp_TypeInfo_var))->get_instance_5();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		NativeNFCManager_t351225459 * L_5 = ((NativeNFCManager_t351225459_StaticFields*)il2cpp_codegen_static_fields_for(NativeNFCManager_t351225459_il2cpp_TypeInfo_var))->get_instance_5();
		NullCheck(L_5);
		GameObject_t1113636619 * L_6 = Component_get_gameObject_m442555142(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Boolean DigitsNFCToolkit.NativeNFCManager::IsNFCTagInfoReadSupported()
extern "C" IL2CPP_METHOD_ATTR bool NativeNFCManager_IsNFCTagInfoReadSupported_m3802949991 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean DigitsNFCToolkit.NativeNFC::IsNFCTagInfoReadSupported() */, L_2);
		return L_3;
	}
}
// System.Boolean DigitsNFCToolkit.NativeNFCManager::IsNDEFReadSupported()
extern "C" IL2CPP_METHOD_ATTR bool NativeNFCManager_IsNDEFReadSupported_m4240159853 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean DigitsNFCToolkit.NativeNFC::IsNDEFReadSupported() */, L_2);
		return L_3;
	}
}
// System.Boolean DigitsNFCToolkit.NativeNFCManager::IsNDEFWriteSupported()
extern "C" IL2CPP_METHOD_ATTR bool NativeNFCManager_IsNDEFWriteSupported_m1689756067 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean DigitsNFCToolkit.NativeNFC::IsNDEFWriteSupported() */, L_2);
		return L_3;
	}
}
// System.Boolean DigitsNFCToolkit.NativeNFCManager::IsNFCEnabled()
extern "C" IL2CPP_METHOD_ATTR bool NativeNFCManager_IsNFCEnabled_m3780324468 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean DigitsNFCToolkit.NativeNFC::IsNFCEnabled() */, L_2);
		return L_3;
	}
}
// System.Boolean DigitsNFCToolkit.NativeNFCManager::IsNDEFPushEnabled()
extern "C" IL2CPP_METHOD_ATTR bool NativeNFCManager_IsNDEFPushEnabled_m743806102 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean DigitsNFCToolkit.NativeNFC::IsNDEFPushEnabled() */, L_2);
		return L_3;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::Enable()
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_Enable_m3630587251 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeNFCManager_Enable_m3630587251_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean DigitsNFCToolkit.NativeNFC::IsNFCEnabled() */, L_2);
		if (L_3)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, _stringLiteral453214562, /*hidden argument*/NULL);
	}

IL_0020:
	{
		NativeNFC_t1941597496 * L_4 = V_0;
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(12 /* System.Void DigitsNFCToolkit.NativeNFC::Enable() */, L_4);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::Disable()
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_Disable_m599991495 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(13 /* System.Void DigitsNFCToolkit.NativeNFC::Disable() */, L_2);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::RequestNDEFWrite(DigitsNFCToolkit.NDEFMessage)
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_RequestNDEFWrite_m306153961 (RuntimeObject * __this /* static, unused */, NDEFMessage_t279637043 * ___message0, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		NDEFMessage_t279637043 * L_3 = ___message0;
		NullCheck(L_3);
		JSONObject_t321714843 * L_4 = NDEFMessage_ToJSON_m895517323(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(14 /* System.Void DigitsNFCToolkit.NativeNFC::RequestNDEFWrite(System.String) */, L_2, L_5);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::CancelNDEFWriteRequest()
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_CancelNDEFWriteRequest_m546161934 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(15 /* System.Void DigitsNFCToolkit.NativeNFC::CancelNDEFWriteRequest() */, L_2);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::RequestNDEFPush(DigitsNFCToolkit.NDEFMessage)
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_RequestNDEFPush_m4123021546 (RuntimeObject * __this /* static, unused */, NDEFMessage_t279637043 * ___message0, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		NDEFMessage_t279637043 * L_3 = ___message0;
		NullCheck(L_3);
		JSONObject_t321714843 * L_4 = NDEFMessage_ToJSON_m895517323(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void DigitsNFCToolkit.NativeNFC::RequestNDEFPush(System.String) */, L_2, L_5);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::CancelNDEFPushRequest()
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_CancelNDEFPushRequest_m2961389426 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(17 /* System.Void DigitsNFCToolkit.NativeNFC::CancelNDEFPushRequest() */, L_2);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::AddNFCTagDetectedListener(DigitsNFCToolkit.OnNFCTagDetected)
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_AddNFCTagDetectedListener_m3624669884 (RuntimeObject * __this /* static, unused */, OnNFCTagDetected_t3189675727 * ___listener0, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		OnNFCTagDetected_t3189675727 * L_3 = ___listener0;
		NullCheck(L_2);
		NativeNFC_add_NFCTagDetected_m2650456630(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::RemoveNFCTagDetectedListener(DigitsNFCToolkit.OnNFCTagDetected)
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_RemoveNFCTagDetectedListener_m1171272344 (RuntimeObject * __this /* static, unused */, OnNFCTagDetected_t3189675727 * ___listener0, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		OnNFCTagDetected_t3189675727 * L_3 = ___listener0;
		NullCheck(L_2);
		NativeNFC_remove_NFCTagDetected_m2280126304(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::AddNDEFReadFinishedListener(DigitsNFCToolkit.OnNDEFReadFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_AddNDEFReadFinishedListener_m874873674 (RuntimeObject * __this /* static, unused */, OnNDEFReadFinished_t1327886840 * ___listener0, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		OnNDEFReadFinished_t1327886840 * L_3 = ___listener0;
		NullCheck(L_2);
		NativeNFC_add_NDEFReadFinished_m2374080351(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::RemoveNDEFReadFinishedListener(DigitsNFCToolkit.OnNDEFReadFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_RemoveNDEFReadFinishedListener_m1389420016 (RuntimeObject * __this /* static, unused */, OnNDEFReadFinished_t1327886840 * ___listener0, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		OnNDEFReadFinished_t1327886840 * L_3 = ___listener0;
		NullCheck(L_2);
		NativeNFC_remove_NDEFReadFinished_m596925152(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::AddNDEFWriteFinishedListener(DigitsNFCToolkit.OnNDEFWriteFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_AddNDEFWriteFinishedListener_m2087486335 (RuntimeObject * __this /* static, unused */, OnNDEFWriteFinished_t4102039599 * ___listener0, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		OnNDEFWriteFinished_t4102039599 * L_3 = ___listener0;
		NullCheck(L_2);
		NativeNFC_add_NDEFWriteFinished_m3503282290(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::RemoveNDEFWriteFinishedListener(DigitsNFCToolkit.OnNDEFWriteFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_RemoveNDEFWriteFinishedListener_m2668620198 (RuntimeObject * __this /* static, unused */, OnNDEFWriteFinished_t4102039599 * ___listener0, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		OnNDEFWriteFinished_t4102039599 * L_3 = ___listener0;
		NullCheck(L_2);
		NativeNFC_remove_NDEFWriteFinished_m4241802593(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::AddNDEFPushFinishedListener(DigitsNFCToolkit.OnNDEFPushFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_AddNDEFPushFinishedListener_m3015772070 (RuntimeObject * __this /* static, unused */, OnNDEFPushFinished_t4279917764 * ___listener0, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		OnNDEFPushFinished_t4279917764 * L_3 = ___listener0;
		NullCheck(L_2);
		NativeNFC_add_NDEFPushFinished_m621482237(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.NativeNFCManager::RemoveNDEFPushFinishedListener(DigitsNFCToolkit.OnNDEFPushFinished)
extern "C" IL2CPP_METHOD_ATTR void NativeNFCManager_RemoveNDEFPushFinishedListener_m3877643317 (RuntimeObject * __this /* static, unused */, OnNDEFPushFinished_t4279917764 * ___listener0, const RuntimeMethod* method)
{
	NativeNFC_t1941597496 * V_0 = NULL;
	{
		NativeNFCManager_t351225459 * L_0 = NativeNFCManager_get_Instance_m711561572(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		NativeNFC_t1941597496 * L_1 = L_0->get_nfc_6();
		V_0 = L_1;
		NativeNFC_t1941597496 * L_2 = V_0;
		OnNDEFPushFinished_t4279917764 * L_3 = ___listener0;
		NullCheck(L_2);
		NativeNFC_remove_NDEFPushFinished_m199227403(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.OnNDEFPushFinished::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void OnNDEFPushFinished__ctor_m1418827824 (OnNDEFPushFinished_t4279917764 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DigitsNFCToolkit.OnNDEFPushFinished::Invoke(DigitsNFCToolkit.NDEFPushResult)
extern "C" IL2CPP_METHOD_ATTR void OnNDEFPushFinished_Invoke_m1414772192 (OnNDEFPushFinished_t4279917764 * __this, NDEFPushResult_t3422827153 * ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	DelegateU5BU5D_t1703627840* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t1188392813* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			if (___methodIsStatic)
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
				{
					// open
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, NDEFPushResult_t3422827153 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, ___result0, targetMethod);
					}
				}
				else
				{
					// closed
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, void*, NDEFPushResult_t3422827153 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___result0, targetMethod);
					}
				}
			}
			else
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
				{
					// closed
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker1< NDEFPushResult_t3422827153 * >::Invoke(targetMethod, targetThis, ___result0);
							else
								GenericVirtActionInvoker1< NDEFPushResult_t3422827153 * >::Invoke(targetMethod, targetThis, ___result0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker1< NDEFPushResult_t3422827153 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___result0);
							else
								VirtActionInvoker1< NDEFPushResult_t3422827153 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___result0);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (void*, NDEFPushResult_t3422827153 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(targetThis, ___result0, targetMethod);
					}
				}
				else
				{
					// open
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker0::Invoke(targetMethod, ___result0);
							else
								GenericVirtActionInvoker0::Invoke(targetMethod, ___result0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___result0);
							else
								VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___result0);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (NDEFPushResult_t3422827153 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___result0, targetMethod);
					}
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		if (___methodIsStatic)
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
			{
				// open
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, NDEFPushResult_t3422827153 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, ___result0, targetMethod);
				}
			}
			else
			{
				// closed
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, void*, NDEFPushResult_t3422827153 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___result0, targetMethod);
				}
			}
		}
		else
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker1< NDEFPushResult_t3422827153 * >::Invoke(targetMethod, targetThis, ___result0);
						else
							GenericVirtActionInvoker1< NDEFPushResult_t3422827153 * >::Invoke(targetMethod, targetThis, ___result0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker1< NDEFPushResult_t3422827153 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___result0);
						else
							VirtActionInvoker1< NDEFPushResult_t3422827153 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___result0);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, NDEFPushResult_t3422827153 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___result0, targetMethod);
				}
			}
			else
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker0::Invoke(targetMethod, ___result0);
						else
							GenericVirtActionInvoker0::Invoke(targetMethod, ___result0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___result0);
						else
							VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___result0);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (NDEFPushResult_t3422827153 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___result0, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult DigitsNFCToolkit.OnNDEFPushFinished::BeginInvoke(DigitsNFCToolkit.NDEFPushResult,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* OnNDEFPushFinished_BeginInvoke_m3593998124 (OnNDEFPushFinished_t4279917764 * __this, NDEFPushResult_t3422827153 * ___result0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___result0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void DigitsNFCToolkit.OnNDEFPushFinished::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void OnNDEFPushFinished_EndInvoke_m2647289836 (OnNDEFPushFinished_t4279917764 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.OnNDEFReadFinished::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void OnNDEFReadFinished__ctor_m1837666951 (OnNDEFReadFinished_t1327886840 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DigitsNFCToolkit.OnNDEFReadFinished::Invoke(DigitsNFCToolkit.NDEFReadResult)
extern "C" IL2CPP_METHOD_ATTR void OnNDEFReadFinished_Invoke_m990719778 (OnNDEFReadFinished_t1327886840 * __this, NDEFReadResult_t3483243621 * ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	DelegateU5BU5D_t1703627840* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t1188392813* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			if (___methodIsStatic)
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
				{
					// open
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, NDEFReadResult_t3483243621 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, ___result0, targetMethod);
					}
				}
				else
				{
					// closed
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, void*, NDEFReadResult_t3483243621 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___result0, targetMethod);
					}
				}
			}
			else
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
				{
					// closed
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker1< NDEFReadResult_t3483243621 * >::Invoke(targetMethod, targetThis, ___result0);
							else
								GenericVirtActionInvoker1< NDEFReadResult_t3483243621 * >::Invoke(targetMethod, targetThis, ___result0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker1< NDEFReadResult_t3483243621 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___result0);
							else
								VirtActionInvoker1< NDEFReadResult_t3483243621 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___result0);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (void*, NDEFReadResult_t3483243621 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(targetThis, ___result0, targetMethod);
					}
				}
				else
				{
					// open
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker0::Invoke(targetMethod, ___result0);
							else
								GenericVirtActionInvoker0::Invoke(targetMethod, ___result0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___result0);
							else
								VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___result0);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (NDEFReadResult_t3483243621 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___result0, targetMethod);
					}
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		if (___methodIsStatic)
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
			{
				// open
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, NDEFReadResult_t3483243621 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, ___result0, targetMethod);
				}
			}
			else
			{
				// closed
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, void*, NDEFReadResult_t3483243621 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___result0, targetMethod);
				}
			}
		}
		else
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker1< NDEFReadResult_t3483243621 * >::Invoke(targetMethod, targetThis, ___result0);
						else
							GenericVirtActionInvoker1< NDEFReadResult_t3483243621 * >::Invoke(targetMethod, targetThis, ___result0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker1< NDEFReadResult_t3483243621 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___result0);
						else
							VirtActionInvoker1< NDEFReadResult_t3483243621 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___result0);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, NDEFReadResult_t3483243621 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___result0, targetMethod);
				}
			}
			else
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker0::Invoke(targetMethod, ___result0);
						else
							GenericVirtActionInvoker0::Invoke(targetMethod, ___result0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___result0);
						else
							VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___result0);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (NDEFReadResult_t3483243621 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___result0, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult DigitsNFCToolkit.OnNDEFReadFinished::BeginInvoke(DigitsNFCToolkit.NDEFReadResult,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* OnNDEFReadFinished_BeginInvoke_m4087690972 (OnNDEFReadFinished_t1327886840 * __this, NDEFReadResult_t3483243621 * ___result0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___result0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void DigitsNFCToolkit.OnNDEFReadFinished::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void OnNDEFReadFinished_EndInvoke_m1139288163 (OnNDEFReadFinished_t1327886840 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.OnNDEFWriteFinished::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void OnNDEFWriteFinished__ctor_m1674872822 (OnNDEFWriteFinished_t4102039599 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DigitsNFCToolkit.OnNDEFWriteFinished::Invoke(DigitsNFCToolkit.NDEFWriteResult)
extern "C" IL2CPP_METHOD_ATTR void OnNDEFWriteFinished_Invoke_m3001009917 (OnNDEFWriteFinished_t4102039599 * __this, NDEFWriteResult_t4210562629 * ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	DelegateU5BU5D_t1703627840* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t1188392813* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			if (___methodIsStatic)
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
				{
					// open
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, NDEFWriteResult_t4210562629 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, ___result0, targetMethod);
					}
				}
				else
				{
					// closed
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, void*, NDEFWriteResult_t4210562629 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___result0, targetMethod);
					}
				}
			}
			else
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
				{
					// closed
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker1< NDEFWriteResult_t4210562629 * >::Invoke(targetMethod, targetThis, ___result0);
							else
								GenericVirtActionInvoker1< NDEFWriteResult_t4210562629 * >::Invoke(targetMethod, targetThis, ___result0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker1< NDEFWriteResult_t4210562629 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___result0);
							else
								VirtActionInvoker1< NDEFWriteResult_t4210562629 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___result0);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (void*, NDEFWriteResult_t4210562629 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(targetThis, ___result0, targetMethod);
					}
				}
				else
				{
					// open
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker0::Invoke(targetMethod, ___result0);
							else
								GenericVirtActionInvoker0::Invoke(targetMethod, ___result0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___result0);
							else
								VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___result0);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (NDEFWriteResult_t4210562629 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___result0, targetMethod);
					}
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		if (___methodIsStatic)
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
			{
				// open
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, NDEFWriteResult_t4210562629 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, ___result0, targetMethod);
				}
			}
			else
			{
				// closed
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, void*, NDEFWriteResult_t4210562629 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___result0, targetMethod);
				}
			}
		}
		else
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker1< NDEFWriteResult_t4210562629 * >::Invoke(targetMethod, targetThis, ___result0);
						else
							GenericVirtActionInvoker1< NDEFWriteResult_t4210562629 * >::Invoke(targetMethod, targetThis, ___result0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker1< NDEFWriteResult_t4210562629 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___result0);
						else
							VirtActionInvoker1< NDEFWriteResult_t4210562629 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___result0);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, NDEFWriteResult_t4210562629 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___result0, targetMethod);
				}
			}
			else
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker0::Invoke(targetMethod, ___result0);
						else
							GenericVirtActionInvoker0::Invoke(targetMethod, ___result0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___result0);
						else
							VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___result0);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (NDEFWriteResult_t4210562629 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___result0, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult DigitsNFCToolkit.OnNDEFWriteFinished::BeginInvoke(DigitsNFCToolkit.NDEFWriteResult,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* OnNDEFWriteFinished_BeginInvoke_m861060932 (OnNDEFWriteFinished_t4102039599 * __this, NDEFWriteResult_t4210562629 * ___result0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___result0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void DigitsNFCToolkit.OnNDEFWriteFinished::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void OnNDEFWriteFinished_EndInvoke_m1667872680 (OnNDEFWriteFinished_t4102039599 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.OnNFCTagDetected::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void OnNFCTagDetected__ctor_m625598112 (OnNFCTagDetected_t3189675727 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DigitsNFCToolkit.OnNFCTagDetected::Invoke(DigitsNFCToolkit.NFCTag)
extern "C" IL2CPP_METHOD_ATTR void OnNFCTagDetected_Invoke_m4012074991 (OnNFCTagDetected_t3189675727 * __this, NFCTag_t2820711232 * ___tag0, const RuntimeMethod* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	DelegateU5BU5D_t1703627840* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t1188392813* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			if (___methodIsStatic)
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
				{
					// open
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, NFCTag_t2820711232 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, ___tag0, targetMethod);
					}
				}
				else
				{
					// closed
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, void*, NFCTag_t2820711232 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___tag0, targetMethod);
					}
				}
			}
			else
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
				{
					// closed
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker1< NFCTag_t2820711232 * >::Invoke(targetMethod, targetThis, ___tag0);
							else
								GenericVirtActionInvoker1< NFCTag_t2820711232 * >::Invoke(targetMethod, targetThis, ___tag0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker1< NFCTag_t2820711232 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___tag0);
							else
								VirtActionInvoker1< NFCTag_t2820711232 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___tag0);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (void*, NFCTag_t2820711232 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(targetThis, ___tag0, targetMethod);
					}
				}
				else
				{
					// open
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker0::Invoke(targetMethod, ___tag0);
							else
								GenericVirtActionInvoker0::Invoke(targetMethod, ___tag0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___tag0);
							else
								VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___tag0);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (NFCTag_t2820711232 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___tag0, targetMethod);
					}
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		if (___methodIsStatic)
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
			{
				// open
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, NFCTag_t2820711232 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, ___tag0, targetMethod);
				}
			}
			else
			{
				// closed
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, void*, NFCTag_t2820711232 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___tag0, targetMethod);
				}
			}
		}
		else
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker1< NFCTag_t2820711232 * >::Invoke(targetMethod, targetThis, ___tag0);
						else
							GenericVirtActionInvoker1< NFCTag_t2820711232 * >::Invoke(targetMethod, targetThis, ___tag0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker1< NFCTag_t2820711232 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___tag0);
						else
							VirtActionInvoker1< NFCTag_t2820711232 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___tag0);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, NFCTag_t2820711232 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___tag0, targetMethod);
				}
			}
			else
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker0::Invoke(targetMethod, ___tag0);
						else
							GenericVirtActionInvoker0::Invoke(targetMethod, ___tag0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___tag0);
						else
							VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___tag0);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (NFCTag_t2820711232 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___tag0, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult DigitsNFCToolkit.OnNFCTagDetected::BeginInvoke(DigitsNFCToolkit.NFCTag,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* OnNFCTagDetected_BeginInvoke_m1300494257 (OnNFCTagDetected_t3189675727 * __this, NFCTag_t2820711232 * ___tag0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___tag0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void DigitsNFCToolkit.OnNFCTagDetected::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void OnNFCTagDetected_EndInvoke_m751307675 (OnNFCTagDetected_t3189675727 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.Samples.ImageRecordItem::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ImageRecordItem__ctor_m333262205 (ImageRecordItem_t2104316047 * __this, const RuntimeMethod* method)
{
	{
		RecordItem__ctor_m1983231623(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.ImageRecordItem::Awake()
extern "C" IL2CPP_METHOD_ATTR void ImageRecordItem_Awake_m810633560 (ImageRecordItem_t2104316047 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImageRecordItem_Awake_m810633560_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RecordItem_Awake_m673876687(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = Transform_Find_m1729760951(L_0, _stringLiteral820751583, /*hidden argument*/NULL);
		NullCheck(L_1);
		Image_t2670269651 * L_2 = Component_GetComponent_TisImage_t2670269651_m980647750(L_1, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		__this->set_imageRenderer_6(L_2);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.ImageRecordItem::LoadImage(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void ImageRecordItem_LoadImage_m2052998847 (ImageRecordItem_t2104316047 * __this, ByteU5BU5D_t4116647657* ___bytes0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImageRecordItem_LoadImage_m2052998847_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Texture2D_t3840446185 * V_0 = NULL;
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t2360479859  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Sprite_t280657092 * V_3 = NULL;
	{
		Texture2D_t3840446185 * L_0 = (Texture2D_t3840446185 *)il2cpp_codegen_object_new(Texture2D_t3840446185_il2cpp_TypeInfo_var);
		Texture2D__ctor_m2862217990(L_0, 1, 1, 3, (bool)0, /*hidden argument*/NULL);
		V_0 = L_0;
		Texture2D_t3840446185 * L_1 = V_0;
		ByteU5BU5D_t4116647657* L_2 = ___bytes0;
		ImageConversion_LoadImage_m2182108104(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_3 = V_0;
		NullCheck(L_3);
		Texture_set_filterMode_m3078068058(L_3, 0, /*hidden argument*/NULL);
		Vector2__ctor_m3970636864((Vector2_t2156229523 *)(&V_1), (0.5f), (0.5f), /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_4);
		Texture2D_t3840446185 * L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_6);
		Rect__ctor_m2614021312((Rect_t2360479859 *)(&V_2), (0.0f), (0.0f), (((float)((float)L_5))), (((float)((float)L_7))), /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_8 = V_0;
		Rect_t2360479859  L_9 = V_2;
		Vector2_t2156229523  L_10 = V_1;
		Sprite_t280657092 * L_11 = Sprite_Create_m803022218(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		Image_t2670269651 * L_12 = __this->get_imageRenderer_6();
		Sprite_t280657092 * L_13 = V_3;
		NullCheck(L_12);
		Image_set_sprite_m2369174689(L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.Samples.MessageScreenView::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MessageScreenView__ctor_m2720021321 (MessageScreenView_t146641597 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.MessageScreenView::Initialize()
extern "C" IL2CPP_METHOD_ATTR void MessageScreenView_Initialize_m784771305 (MessageScreenView_t146641597 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageScreenView_Initialize_m784771305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = Transform_Find_m1729760951(L_0, _stringLiteral731656311, /*hidden argument*/NULL);
		NullCheck(L_1);
		RectTransform_t3704657025 * L_2 = Component_GetComponent_TisRectTransform_t3704657025_m3396022872(L_1, /*hidden argument*/Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var);
		__this->set_writeMessageBox_4(L_2);
		RectTransform_t3704657025 * L_3 = __this->get_writeMessageBox_4();
		NullCheck(L_3);
		Transform_t3600365921 * L_4 = Transform_Find_m1729760951(L_3, _stringLiteral3923321062, /*hidden argument*/NULL);
		NullCheck(L_4);
		Text_t1901882714 * L_5 = Component_GetComponent_TisText_t1901882714_m4196288697(L_4, /*hidden argument*/Component_GetComponent_TisText_t1901882714_m4196288697_RuntimeMethod_var);
		__this->set_writeLabel_5(L_5);
		RectTransform_t3704657025 * L_6 = __this->get_writeMessageBox_4();
		NullCheck(L_6);
		Transform_t3600365921 * L_7 = Transform_Find_m1729760951(L_6, _stringLiteral1036118513, /*hidden argument*/NULL);
		NullCheck(L_7);
		Button_t4055032469 * L_8 = Component_GetComponent_TisButton_t4055032469_m1381873113(L_7, /*hidden argument*/Component_GetComponent_TisButton_t4055032469_m1381873113_RuntimeMethod_var);
		__this->set_writeCancelButton_6(L_8);
		RectTransform_t3704657025 * L_9 = __this->get_writeMessageBox_4();
		NullCheck(L_9);
		Transform_t3600365921 * L_10 = Transform_Find_m1729760951(L_9, _stringLiteral100159630, /*hidden argument*/NULL);
		NullCheck(L_10);
		Button_t4055032469 * L_11 = Component_GetComponent_TisButton_t4055032469_m1381873113(L_10, /*hidden argument*/Component_GetComponent_TisButton_t4055032469_m1381873113_RuntimeMethod_var);
		__this->set_writeOKButton_7(L_11);
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t3600365921 * L_13 = Transform_Find_m1729760951(L_12, _stringLiteral673134560, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_t3704657025 * L_14 = Component_GetComponent_TisRectTransform_t3704657025_m3396022872(L_13, /*hidden argument*/Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var);
		__this->set_pushMessageBox_8(L_14);
		RectTransform_t3704657025 * L_15 = __this->get_pushMessageBox_8();
		NullCheck(L_15);
		Transform_t3600365921 * L_16 = Transform_Find_m1729760951(L_15, _stringLiteral3923321062, /*hidden argument*/NULL);
		NullCheck(L_16);
		Text_t1901882714 * L_17 = Component_GetComponent_TisText_t1901882714_m4196288697(L_16, /*hidden argument*/Component_GetComponent_TisText_t1901882714_m4196288697_RuntimeMethod_var);
		__this->set_pushLabel_9(L_17);
		RectTransform_t3704657025 * L_18 = __this->get_pushMessageBox_8();
		NullCheck(L_18);
		Transform_t3600365921 * L_19 = Transform_Find_m1729760951(L_18, _stringLiteral1036118513, /*hidden argument*/NULL);
		NullCheck(L_19);
		Button_t4055032469 * L_20 = Component_GetComponent_TisButton_t4055032469_m1381873113(L_19, /*hidden argument*/Component_GetComponent_TisButton_t4055032469_m1381873113_RuntimeMethod_var);
		__this->set_pushCancelButton_10(L_20);
		RectTransform_t3704657025 * L_21 = __this->get_pushMessageBox_8();
		NullCheck(L_21);
		Transform_t3600365921 * L_22 = Transform_Find_m1729760951(L_21, _stringLiteral100159630, /*hidden argument*/NULL);
		NullCheck(L_22);
		Button_t4055032469 * L_23 = Component_GetComponent_TisButton_t4055032469_m1381873113(L_22, /*hidden argument*/Component_GetComponent_TisButton_t4055032469_m1381873113_RuntimeMethod_var);
		__this->set_pushOKButton_11(L_23);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.MessageScreenView::Awake()
extern "C" IL2CPP_METHOD_ATTR void MessageScreenView_Awake_m22382092 (MessageScreenView_t146641597 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_initialized_12();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		MessageScreenView_Initialize_m784771305(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.MessageScreenView::Show()
extern "C" IL2CPP_METHOD_ATTR void MessageScreenView_Show_m1026536713 (MessageScreenView_t146641597 * __this, const RuntimeMethod* method)
{
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.MessageScreenView::Hide()
extern "C" IL2CPP_METHOD_ATTR void MessageScreenView_Hide_m3938180501 (MessageScreenView_t146641597 * __this, const RuntimeMethod* method)
{
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.MessageScreenView::SwitchToPendingWrite()
extern "C" IL2CPP_METHOD_ATTR void MessageScreenView_SwitchToPendingWrite_m2230352620 (MessageScreenView_t146641597 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageScreenView_SwitchToPendingWrite_m2230352620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_initialized_12();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		MessageScreenView_Initialize_m784771305(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		RectTransform_t3704657025 * L_1 = __this->get_pushMessageBox_8();
		NullCheck(L_1);
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_3 = __this->get_writeMessageBox_4();
		NullCheck(L_3);
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)1, /*hidden argument*/NULL);
		Text_t1901882714 * L_5 = __this->get_writeLabel_5();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteral3165397527);
		Button_t4055032469 * L_6 = __this->get_writeCancelButton_6();
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_SetActive_m796801857(L_7, (bool)1, /*hidden argument*/NULL);
		Button_t4055032469 * L_8 = __this->get_writeOKButton_7();
		NullCheck(L_8);
		GameObject_t1113636619 * L_9 = Component_get_gameObject_m442555142(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_SetActive_m796801857(L_9, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.MessageScreenView::SwitchToWriteResult(System.String)
extern "C" IL2CPP_METHOD_ATTR void MessageScreenView_SwitchToWriteResult_m1196524044 (MessageScreenView_t146641597 * __this, String_t* ___writeResult0, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_initialized_12();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		MessageScreenView_Initialize_m784771305(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		RectTransform_t3704657025 * L_1 = __this->get_pushMessageBox_8();
		NullCheck(L_1);
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_3 = __this->get_writeMessageBox_4();
		NullCheck(L_3);
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)1, /*hidden argument*/NULL);
		Text_t1901882714 * L_5 = __this->get_writeLabel_5();
		String_t* L_6 = ___writeResult0;
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_6);
		Button_t4055032469 * L_7 = __this->get_writeCancelButton_6();
		NullCheck(L_7);
		GameObject_t1113636619 * L_8 = Component_get_gameObject_m442555142(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_SetActive_m796801857(L_8, (bool)0, /*hidden argument*/NULL);
		Button_t4055032469 * L_9 = __this->get_writeOKButton_7();
		NullCheck(L_9);
		GameObject_t1113636619 * L_10 = Component_get_gameObject_m442555142(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_SetActive_m796801857(L_10, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.MessageScreenView::SwitchToPendingPush()
extern "C" IL2CPP_METHOD_ATTR void MessageScreenView_SwitchToPendingPush_m1103692928 (MessageScreenView_t146641597 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageScreenView_SwitchToPendingPush_m1103692928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_initialized_12();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		MessageScreenView_Initialize_m784771305(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		RectTransform_t3704657025 * L_1 = __this->get_writeMessageBox_4();
		NullCheck(L_1);
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_3 = __this->get_pushMessageBox_8();
		NullCheck(L_3);
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)1, /*hidden argument*/NULL);
		Text_t1901882714 * L_5 = __this->get_pushLabel_9();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteral2983490506);
		Button_t4055032469 * L_6 = __this->get_pushCancelButton_10();
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_SetActive_m796801857(L_7, (bool)1, /*hidden argument*/NULL);
		Button_t4055032469 * L_8 = __this->get_pushOKButton_11();
		NullCheck(L_8);
		GameObject_t1113636619 * L_9 = Component_get_gameObject_m442555142(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_SetActive_m796801857(L_9, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.MessageScreenView::SwitchToPushResult(System.String)
extern "C" IL2CPP_METHOD_ATTR void MessageScreenView_SwitchToPushResult_m1664855704 (MessageScreenView_t146641597 * __this, String_t* ___pushResult0, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_initialized_12();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		MessageScreenView_Initialize_m784771305(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		RectTransform_t3704657025 * L_1 = __this->get_writeMessageBox_4();
		NullCheck(L_1);
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_3 = __this->get_pushMessageBox_8();
		NullCheck(L_3);
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)1, /*hidden argument*/NULL);
		Text_t1901882714 * L_5 = __this->get_pushLabel_9();
		String_t* L_6 = ___pushResult0;
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_6);
		Button_t4055032469 * L_7 = __this->get_pushCancelButton_10();
		NullCheck(L_7);
		GameObject_t1113636619 * L_8 = Component_get_gameObject_m442555142(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_SetActive_m796801857(L_8, (bool)0, /*hidden argument*/NULL);
		Button_t4055032469 * L_9 = __this->get_pushOKButton_11();
		NullCheck(L_9);
		GameObject_t1113636619 * L_10 = Component_get_gameObject_m442555142(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_SetActive_m796801857(L_10, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.Samples.NavigationManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NavigationManager__ctor_m869205361 (NavigationManager_t1939391727 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.NavigationManager::Start()
extern "C" IL2CPP_METHOD_ATTR void NavigationManager_Start_m4032256153 (NavigationManager_t1939391727 * __this, const RuntimeMethod* method)
{
	{
		NavigationManager_SwitchToReadScreen_m2950668844(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.NavigationManager::SwitchToReadScreen()
extern "C" IL2CPP_METHOD_ATTR void NavigationManager_SwitchToReadScreen_m2950668844 (NavigationManager_t1939391727 * __this, const RuntimeMethod* method)
{
	{
		WriteScreenControl_t1506090515 * L_0 = __this->get_writeScreenControl_5();
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		ReadScreenControl_t3483866810 * L_2 = __this->get_readScreenControl_4();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.NavigationManager::SwitchToWriteScreen()
extern "C" IL2CPP_METHOD_ATTR void NavigationManager_SwitchToWriteScreen_m4258474829 (NavigationManager_t1939391727 * __this, const RuntimeMethod* method)
{
	{
		ReadScreenControl_t3483866810 * L_0 = __this->get_readScreenControl_4();
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		WriteScreenControl_t1506090515 * L_2 = __this->get_writeScreenControl_5();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.Samples.ReadScreenControl::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ReadScreenControl__ctor_m762647204 (ReadScreenControl_t3483866810 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.ReadScreenControl::Start()
extern "C" IL2CPP_METHOD_ATTR void ReadScreenControl_Start_m1589085894 (ReadScreenControl_t3483866810 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadScreenControl_Start_m1589085894_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = (intptr_t)ReadScreenControl_OnNFCTagDetected_m4030544015_RuntimeMethod_var;
		OnNFCTagDetected_t3189675727 * L_1 = (OnNFCTagDetected_t3189675727 *)il2cpp_codegen_object_new(OnNFCTagDetected_t3189675727_il2cpp_TypeInfo_var);
		OnNFCTagDetected__ctor_m625598112(L_1, __this, (intptr_t)L_0, /*hidden argument*/NULL);
		NativeNFCManager_AddNFCTagDetectedListener_m3624669884(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		intptr_t L_2 = (intptr_t)ReadScreenControl_OnNDEFReadFinished_m3925533757_RuntimeMethod_var;
		OnNDEFReadFinished_t1327886840 * L_3 = (OnNDEFReadFinished_t1327886840 *)il2cpp_codegen_object_new(OnNDEFReadFinished_t1327886840_il2cpp_TypeInfo_var);
		OnNDEFReadFinished__ctor_m1837666951(L_3, __this, (intptr_t)L_2, /*hidden argument*/NULL);
		NativeNFCManager_AddNDEFReadFinishedListener_m874873674(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		bool L_4 = NativeNFCManager_IsNFCTagInfoReadSupported_m3802949991(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = L_4;
		RuntimeObject * L_6 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_5);
		String_t* L_7 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral1526924809, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		bool L_8 = NativeNFCManager_IsNDEFReadSupported_m4240159853(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_9 = L_8;
		RuntimeObject * L_10 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_9);
		String_t* L_11 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral4081923444, L_10, /*hidden argument*/NULL);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		bool L_12 = NativeNFCManager_IsNDEFWriteSupported_m1689756067(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_13 = L_12;
		RuntimeObject * L_14 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_13);
		String_t* L_15 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral1091168510, L_14, /*hidden argument*/NULL);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		bool L_16 = NativeNFCManager_IsNFCEnabled_m3780324468(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_17 = L_16;
		RuntimeObject * L_18 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_17);
		String_t* L_19 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral2455323115, L_18, /*hidden argument*/NULL);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		bool L_20 = NativeNFCManager_IsNDEFPushEnabled_m743806102(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_21 = L_20;
		RuntimeObject * L_22 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_21);
		String_t* L_23 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral881278981, L_22, /*hidden argument*/NULL);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.ReadScreenControl::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void ReadScreenControl_OnEnable_m1221940168 (ReadScreenControl_t3483866810 * __this, const RuntimeMethod* method)
{
	{
		ReadScreenView_t239900869 * L_0 = __this->get_view_4();
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.ReadScreenControl::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void ReadScreenControl_OnDisable_m1933725734 (ReadScreenControl_t3483866810 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadScreenControl_OnDisable_m1933725734_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReadScreenView_t239900869 * L_0 = __this->get_view_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		ReadScreenView_t239900869 * L_2 = __this->get_view_4();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.ReadScreenControl::OnStartNFCReadClick()
extern "C" IL2CPP_METHOD_ATTR void ReadScreenControl_OnStartNFCReadClick_m1246611128 (ReadScreenControl_t3483866810 * __this, const RuntimeMethod* method)
{
	{
		NativeNFCManager_set_ResetOnTimeout_m718426537(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		NativeNFCManager_Enable_m3630587251(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.ReadScreenControl::OnNFCTagDetected(DigitsNFCToolkit.NFCTag)
extern "C" IL2CPP_METHOD_ATTR void ReadScreenControl_OnNFCTagDetected_m4030544015 (ReadScreenControl_t3483866810 * __this, NFCTag_t2820711232 * ___tag0, const RuntimeMethod* method)
{
	{
		ReadScreenView_t239900869 * L_0 = __this->get_view_4();
		NFCTag_t2820711232 * L_1 = ___tag0;
		NullCheck(L_0);
		ReadScreenView_UpdateTagInfo_m2808310696(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.ReadScreenControl::OnNDEFReadFinished(DigitsNFCToolkit.NDEFReadResult)
extern "C" IL2CPP_METHOD_ATTR void ReadScreenControl_OnNDEFReadFinished_m3925533757 (ReadScreenControl_t3483866810 * __this, NDEFReadResult_t3483243621 * ___result0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadScreenControl_OnNDEFReadFinished_m3925533757_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		V_0 = L_0;
		NDEFReadResult_t3483243621 * L_1 = ___result0;
		NullCheck(L_1);
		bool L_2 = NDEFReadResult_get_Success_m1093983849(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0038;
		}
	}
	{
		NDEFReadResult_t3483243621 * L_3 = ___result0;
		NullCheck(L_3);
		String_t* L_4 = NDEFReadResult_get_TagID_m4041318970(L_3, /*hidden argument*/NULL);
		String_t* L_5 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral3594963313, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		ReadScreenView_t239900869 * L_6 = __this->get_view_4();
		NDEFReadResult_t3483243621 * L_7 = ___result0;
		NullCheck(L_7);
		NDEFMessage_t279637043 * L_8 = NDEFReadResult_get_Message_m152108824(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		ReadScreenView_UpdateNDEFMessage_m814396461(L_6, L_8, /*hidden argument*/NULL);
		goto IL_0054;
	}

IL_0038:
	{
		NDEFReadResult_t3483243621 * L_9 = ___result0;
		NullCheck(L_9);
		String_t* L_10 = NDEFReadResult_get_TagID_m4041318970(L_9, /*hidden argument*/NULL);
		NDEFReadResult_t3483243621 * L_11 = ___result0;
		NullCheck(L_11);
		int32_t L_12 = NDEFReadResult_get_Error_m3746343680(L_11, /*hidden argument*/NULL);
		int32_t L_13 = L_12;
		RuntimeObject * L_14 = Box(NDEFReadError_t886852181_il2cpp_TypeInfo_var, &L_13);
		String_t* L_15 = String_Format_m2556382932(NULL /*static, unused*/, _stringLiteral3160991853, L_10, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
	}

IL_0054:
	{
		String_t* L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.Samples.ReadScreenView::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ReadScreenView__ctor_m1326582427 (ReadScreenView_t239900869 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.ReadScreenView::Awake()
extern "C" IL2CPP_METHOD_ATTR void ReadScreenView_Awake_m1822767483 (ReadScreenView_t239900869 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadScreenView_Awake_m1822767483_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = Transform_Find_m1729760951(L_0, _stringLiteral3860635076, /*hidden argument*/NULL);
		NullCheck(L_1);
		RectTransform_t3704657025 * L_2 = Component_GetComponent_TisRectTransform_t3704657025_m3396022872(L_1, /*hidden argument*/Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var);
		__this->set_tagInfoTransform_16(L_2);
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3600365921 * L_4 = Transform_Find_m1729760951(L_3, _stringLiteral410768231, /*hidden argument*/NULL);
		NullCheck(L_4);
		RectTransform_t3704657025 * L_5 = Component_GetComponent_TisRectTransform_t3704657025_m3396022872(L_4, /*hidden argument*/Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var);
		__this->set_iOSReadTransform_17(L_5);
		RectTransform_t3704657025 * L_6 = __this->get_tagInfoTransform_16();
		NullCheck(L_6);
		Transform_t3600365921 * L_7 = Transform_Find_m1729760951(L_6, _stringLiteral3434468235, /*hidden argument*/NULL);
		NullCheck(L_7);
		Text_t1901882714 * L_8 = Component_GetComponent_TisText_t1901882714_m4196288697(L_7, /*hidden argument*/Component_GetComponent_TisText_t1901882714_m4196288697_RuntimeMethod_var);
		__this->set_tagInfoContentLabel_18(L_8);
		Transform_t3600365921 * L_9 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t3600365921 * L_10 = Transform_Find_m1729760951(L_9, _stringLiteral110252601, /*hidden argument*/NULL);
		NullCheck(L_10);
		ScrollRect_t4137855814 * L_11 = Component_GetComponent_TisScrollRect_t4137855814_m3636214290(L_10, /*hidden argument*/Component_GetComponent_TisScrollRect_t4137855814_m3636214290_RuntimeMethod_var);
		__this->set_ndefMessageScrollRect_19(L_11);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.ReadScreenView::Start()
extern "C" IL2CPP_METHOD_ATTR void ReadScreenView_Start_m3630219313 (ReadScreenView_t239900869 * __this, const RuntimeMethod* method)
{
	{
		RectTransform_t3704657025 * L_0 = __this->get_tagInfoTransform_16();
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_2 = __this->get_iOSReadTransform_17();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.ReadScreenView::UpdateTagInfo(DigitsNFCToolkit.NFCTag)
extern "C" IL2CPP_METHOD_ATTR void ReadScreenView_UpdateTagInfo_m2808310696 (ReadScreenView_t239900869 * __this, NFCTag_t2820711232 * ___tag0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadScreenView_UpdateTagInfo_m2808310696_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	NFCTechnologyU5BU5D_t328493574* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	{
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		V_0 = L_0;
		NFCTag_t2820711232 * L_1 = ___tag0;
		NullCheck(L_1);
		NFCTechnologyU5BU5D_t328493574* L_2 = NFCTag_get_Technologies_m134455690(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		NFCTechnologyU5BU5D_t328493574* L_3 = V_1;
		NullCheck(L_3);
		V_2 = (((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))));
		V_3 = 0;
		goto IL_0048;
	}

IL_0018:
	{
		int32_t L_4 = V_3;
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_5 = V_0;
		String_t* L_6 = String_Concat_m3937257545(NULL /*static, unused*/, L_5, _stringLiteral3450517380, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_002b:
	{
		String_t* L_7 = V_0;
		NFCTechnologyU5BU5D_t328493574* L_8 = V_1;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		RuntimeObject * L_10 = Box(NFCTechnology_t1376062623_il2cpp_TypeInfo_var, ((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))));
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		*((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))) = *(int32_t*)UnBox(L_10);
		String_t* L_12 = String_Concat_m3937257545(NULL /*static, unused*/, L_7, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		int32_t L_13 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0048:
	{
		int32_t L_14 = V_3;
		int32_t L_15 = V_2;
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_16 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		V_4 = L_16;
		NFCTag_t2820711232 * L_17 = ___tag0;
		NullCheck(L_17);
		int32_t L_18 = NFCTag_get_MaxWriteSize_m697043576(L_17, /*hidden argument*/NULL);
		if ((((int32_t)L_18) <= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		NFCTag_t2820711232 * L_19 = ___tag0;
		NullCheck(L_19);
		int32_t L_20 = NFCTag_get_MaxWriteSize_m697043576(L_19, /*hidden argument*/NULL);
		int32_t L_21 = L_20;
		RuntimeObject * L_22 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_21);
		String_t* L_23 = String_Concat_m904156431(NULL /*static, unused*/, L_22, _stringLiteral1218818254, /*hidden argument*/NULL);
		V_4 = L_23;
		goto IL_0085;
	}

IL_007e:
	{
		V_4 = _stringLiteral2854237347;
	}

IL_0085:
	{
		ObjectU5BU5D_t2843939325* L_24 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t2843939325* L_25 = L_24;
		NFCTag_t2820711232 * L_26 = ___tag0;
		NullCheck(L_26);
		String_t* L_27 = NFCTag_get_ID_m2099905974(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_27);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_27);
		ObjectU5BU5D_t2843939325* L_28 = L_25;
		String_t* L_29 = V_0;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_29);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_29);
		ObjectU5BU5D_t2843939325* L_30 = L_28;
		NFCTag_t2820711232 * L_31 = ___tag0;
		NullCheck(L_31);
		String_t* L_32 = NFCTag_get_Manufacturer_m3670434854(L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, L_32);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_32);
		ObjectU5BU5D_t2843939325* L_33 = L_30;
		NFCTag_t2820711232 * L_34 = ___tag0;
		NullCheck(L_34);
		bool L_35 = NFCTag_get_Writable_m3150305762(L_34, /*hidden argument*/NULL);
		bool L_36 = L_35;
		RuntimeObject * L_37 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_37);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_37);
		ObjectU5BU5D_t2843939325* L_38 = L_33;
		String_t* L_39 = V_4;
		NullCheck(L_38);
		ArrayElementTypeCheck (L_38, L_39);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_39);
		String_t* L_40 = String_Format_m630303134(NULL /*static, unused*/, _stringLiteral1993607875, L_38, /*hidden argument*/NULL);
		V_5 = L_40;
		Text_t1901882714 * L_41 = __this->get_tagInfoContentLabel_18();
		String_t* L_42 = V_5;
		NullCheck(L_41);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_41, L_42);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.ReadScreenView::UpdateNDEFMessage(DigitsNFCToolkit.NDEFMessage)
extern "C" IL2CPP_METHOD_ATTR void ReadScreenView_UpdateNDEFMessage_m814396461 (ReadScreenView_t239900869 * __this, NDEFMessage_t279637043 * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadScreenView_UpdateNDEFMessage_m814396461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	List_1_t4162620298 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	NDEFRecord_t2690545556 * V_4 = NULL;
	RecordItem_t1075151419 * V_5 = NULL;
	int32_t V_6 = 0;
	TextRecord_t2313697623 * V_7 = NULL;
	UriRecord_t2230063309 * V_8 = NULL;
	MimeMediaRecord_t736820488 * V_9 = NULL;
	ExternalTypeRecord_t4087466745 * V_10 = NULL;
	int32_t V_11 = 0;
	String_t* V_12 = NULL;
	SmartPosterRecord_t1640848801 * V_13 = NULL;
	int32_t V_14 = 0;
	Rect_t2360479859  V_15;
	memset(&V_15, 0, sizeof(V_15));
	{
		ReadScreenView_CleanupRecordItems_m1746468235(__this, /*hidden argument*/NULL);
		V_0 = (0.0f);
		NDEFMessage_t279637043 * L_0 = ___message0;
		NullCheck(L_0);
		List_1_t4162620298 * L_1 = NDEFMessage_get_Records_m1930208688(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		List_1_t4162620298 * L_2 = V_1;
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m2774962351(L_2, /*hidden argument*/List_1_get_Count_m2774962351_RuntimeMethod_var);
		V_2 = L_3;
		V_3 = 0;
		goto IL_02bf;
	}

IL_0021:
	{
		List_1_t4162620298 * L_4 = V_1;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		NDEFRecord_t2690545556 * L_6 = List_1_get_Item_m1387264933(L_4, L_5, /*hidden argument*/List_1_get_Item_m1387264933_RuntimeMethod_var);
		V_4 = L_6;
		V_5 = (RecordItem_t1075151419 *)NULL;
		NDEFRecord_t2690545556 * L_7 = V_4;
		NullCheck(L_7);
		int32_t L_8 = NDEFRecord_get_Type_m1191149495(L_7, /*hidden argument*/NULL);
		V_6 = L_8;
		int32_t L_9 = V_6;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)2)))
		{
			case 0:
			{
				goto IL_0162;
			}
			case 1:
			{
				goto IL_0115;
			}
			case 2:
			{
				goto IL_01df;
			}
			case 3:
			{
				goto IL_005c;
			}
			case 4:
			{
				goto IL_0284;
			}
			case 5:
			{
				goto IL_00bb;
			}
		}
	}
	{
		goto IL_0284;
	}

IL_005c:
	{
		RecordItem_t1075151419 * L_10 = __this->get_textRecordItemPrefab_11();
		RecordItem_t1075151419 * L_11 = ReadScreenView_CreateRecordItem_m3952030190(__this, L_10, /*hidden argument*/NULL);
		V_5 = L_11;
		NDEFRecord_t2690545556 * L_12 = V_4;
		V_7 = ((TextRecord_t2313697623 *)CastclassClass((RuntimeObject*)L_12, TextRecord_t2313697623_il2cpp_TypeInfo_var));
		RecordItem_t1075151419 * L_13 = V_5;
		ObjectU5BU5D_t2843939325* L_14 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t2843939325* L_15 = L_14;
		int32_t L_16 = ((int32_t)5);
		RuntimeObject * L_17 = Box(NDEFRecordType_t4294622310_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_17);
		ObjectU5BU5D_t2843939325* L_18 = L_15;
		TextRecord_t2313697623 * L_19 = V_7;
		NullCheck(L_19);
		String_t* L_20 = L_19->get_text_1();
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_20);
		ObjectU5BU5D_t2843939325* L_21 = L_18;
		TextRecord_t2313697623 * L_22 = V_7;
		NullCheck(L_22);
		String_t* L_23 = L_22->get_languageCode_2();
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_23);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_23);
		ObjectU5BU5D_t2843939325* L_24 = L_21;
		TextRecord_t2313697623 * L_25 = V_7;
		NullCheck(L_25);
		int32_t L_26 = L_25->get_textEncoding_3();
		int32_t L_27 = L_26;
		RuntimeObject * L_28 = Box(TextEncoding_t3210513626_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_28);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_28);
		String_t* L_29 = String_Format_m630303134(NULL /*static, unused*/, _stringLiteral3037589779, L_24, /*hidden argument*/NULL);
		NullCheck(L_13);
		RecordItem_UpdateLabel_m1853417565(L_13, L_29, /*hidden argument*/NULL);
		goto IL_0284;
	}

IL_00bb:
	{
		RecordItem_t1075151419 * L_30 = __this->get_uriRecordItemPrefab_12();
		RecordItem_t1075151419 * L_31 = ReadScreenView_CreateRecordItem_m3952030190(__this, L_30, /*hidden argument*/NULL);
		V_5 = L_31;
		NDEFRecord_t2690545556 * L_32 = V_4;
		V_8 = ((UriRecord_t2230063309 *)CastclassClass((RuntimeObject*)L_32, UriRecord_t2230063309_il2cpp_TypeInfo_var));
		RecordItem_t1075151419 * L_33 = V_5;
		ObjectU5BU5D_t2843939325* L_34 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t2843939325* L_35 = L_34;
		int32_t L_36 = ((int32_t)7);
		RuntimeObject * L_37 = Box(NDEFRecordType_t4294622310_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_37);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_37);
		ObjectU5BU5D_t2843939325* L_38 = L_35;
		UriRecord_t2230063309 * L_39 = V_8;
		NullCheck(L_39);
		String_t* L_40 = L_39->get_fullUri_1();
		NullCheck(L_38);
		ArrayElementTypeCheck (L_38, L_40);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_40);
		ObjectU5BU5D_t2843939325* L_41 = L_38;
		UriRecord_t2230063309 * L_42 = V_8;
		NullCheck(L_42);
		String_t* L_43 = L_42->get_uri_2();
		NullCheck(L_41);
		ArrayElementTypeCheck (L_41, L_43);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_43);
		ObjectU5BU5D_t2843939325* L_44 = L_41;
		UriRecord_t2230063309 * L_45 = V_8;
		NullCheck(L_45);
		String_t* L_46 = L_45->get_protocol_3();
		NullCheck(L_44);
		ArrayElementTypeCheck (L_44, L_46);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_46);
		String_t* L_47 = String_Format_m630303134(NULL /*static, unused*/, _stringLiteral2378602798, L_44, /*hidden argument*/NULL);
		NullCheck(L_33);
		RecordItem_UpdateLabel_m1853417565(L_33, L_47, /*hidden argument*/NULL);
		goto IL_0284;
	}

IL_0115:
	{
		ImageRecordItem_t2104316047 * L_48 = __this->get_mimeMediaRecordItemPrefab_13();
		RecordItem_t1075151419 * L_49 = ReadScreenView_CreateRecordItem_m3952030190(__this, L_48, /*hidden argument*/NULL);
		V_5 = L_49;
		NDEFRecord_t2690545556 * L_50 = V_4;
		V_9 = ((MimeMediaRecord_t736820488 *)CastclassClass((RuntimeObject*)L_50, MimeMediaRecord_t736820488_il2cpp_TypeInfo_var));
		RecordItem_t1075151419 * L_51 = V_5;
		int32_t L_52 = ((int32_t)3);
		RuntimeObject * L_53 = Box(NDEFRecordType_t4294622310_il2cpp_TypeInfo_var, &L_52);
		MimeMediaRecord_t736820488 * L_54 = V_9;
		NullCheck(L_54);
		String_t* L_55 = L_54->get_mimeType_1();
		String_t* L_56 = String_Format_m2556382932(NULL /*static, unused*/, _stringLiteral1274537113, L_53, L_55, /*hidden argument*/NULL);
		NullCheck(L_51);
		RecordItem_UpdateLabel_m1853417565(L_51, L_56, /*hidden argument*/NULL);
		RecordItem_t1075151419 * L_57 = V_5;
		MimeMediaRecord_t736820488 * L_58 = V_9;
		NullCheck(L_58);
		ByteU5BU5D_t4116647657* L_59 = L_58->get_mimeData_2();
		NullCheck(((ImageRecordItem_t2104316047 *)CastclassClass((RuntimeObject*)L_57, ImageRecordItem_t2104316047_il2cpp_TypeInfo_var)));
		ImageRecordItem_LoadImage_m2052998847(((ImageRecordItem_t2104316047 *)CastclassClass((RuntimeObject*)L_57, ImageRecordItem_t2104316047_il2cpp_TypeInfo_var)), L_59, /*hidden argument*/NULL);
		goto IL_0284;
	}

IL_0162:
	{
		RecordItem_t1075151419 * L_60 = __this->get_externalTypeRecordItemPrefab_14();
		RecordItem_t1075151419 * L_61 = ReadScreenView_CreateRecordItem_m3952030190(__this, L_60, /*hidden argument*/NULL);
		V_5 = L_61;
		NDEFRecord_t2690545556 * L_62 = V_4;
		V_10 = ((ExternalTypeRecord_t4087466745 *)CastclassClass((RuntimeObject*)L_62, ExternalTypeRecord_t4087466745_il2cpp_TypeInfo_var));
		ExternalTypeRecord_t4087466745 * L_63 = V_10;
		NullCheck(L_63);
		ByteU5BU5D_t4116647657* L_64 = L_63->get_domainData_3();
		NullCheck(L_64);
		V_11 = (((int32_t)((int32_t)(((RuntimeArray *)L_64)->max_length))));
		Encoding_t1523322056 * L_65 = Encoding_get_UTF8_m1008486739(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExternalTypeRecord_t4087466745 * L_66 = V_10;
		NullCheck(L_66);
		ByteU5BU5D_t4116647657* L_67 = L_66->get_domainData_3();
		NullCheck(L_65);
		String_t* L_68 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t4116647657* >::Invoke(32 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_65, L_67);
		V_12 = L_68;
		RecordItem_t1075151419 * L_69 = V_5;
		ObjectU5BU5D_t2843939325* L_70 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t2843939325* L_71 = L_70;
		int32_t L_72 = ((int32_t)2);
		RuntimeObject * L_73 = Box(NDEFRecordType_t4294622310_il2cpp_TypeInfo_var, &L_72);
		NullCheck(L_71);
		ArrayElementTypeCheck (L_71, L_73);
		(L_71)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_73);
		ObjectU5BU5D_t2843939325* L_74 = L_71;
		ExternalTypeRecord_t4087466745 * L_75 = V_10;
		NullCheck(L_75);
		String_t* L_76 = L_75->get_domainName_1();
		NullCheck(L_74);
		ArrayElementTypeCheck (L_74, L_76);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_76);
		ObjectU5BU5D_t2843939325* L_77 = L_74;
		ExternalTypeRecord_t4087466745 * L_78 = V_10;
		NullCheck(L_78);
		String_t* L_79 = L_78->get_domainType_2();
		NullCheck(L_77);
		ArrayElementTypeCheck (L_77, L_79);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_79);
		ObjectU5BU5D_t2843939325* L_80 = L_77;
		int32_t L_81 = V_11;
		int32_t L_82 = L_81;
		RuntimeObject * L_83 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_82);
		NullCheck(L_80);
		ArrayElementTypeCheck (L_80, L_83);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_83);
		ObjectU5BU5D_t2843939325* L_84 = L_80;
		String_t* L_85 = V_12;
		NullCheck(L_84);
		ArrayElementTypeCheck (L_84, L_85);
		(L_84)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_85);
		String_t* L_86 = String_Format_m630303134(NULL /*static, unused*/, _stringLiteral3680104728, L_84, /*hidden argument*/NULL);
		NullCheck(L_69);
		RecordItem_UpdateLabel_m1853417565(L_69, L_86, /*hidden argument*/NULL);
		goto IL_0284;
	}

IL_01df:
	{
		RecordItem_t1075151419 * L_87 = __this->get_smartPosterRecordItemPrefab_15();
		RecordItem_t1075151419 * L_88 = ReadScreenView_CreateRecordItem_m3952030190(__this, L_87, /*hidden argument*/NULL);
		V_5 = L_88;
		NDEFRecord_t2690545556 * L_89 = V_4;
		V_13 = ((SmartPosterRecord_t1640848801 *)CastclassClass((RuntimeObject*)L_89, SmartPosterRecord_t1640848801_il2cpp_TypeInfo_var));
		SmartPosterRecord_t1640848801 * L_90 = V_13;
		NullCheck(L_90);
		List_1_t3785772365 * L_91 = L_90->get_titleRecords_2();
		NullCheck(L_91);
		int32_t L_92 = List_1_get_Count_m3842818989(L_91, /*hidden argument*/List_1_get_Count_m3842818989_RuntimeMethod_var);
		SmartPosterRecord_t1640848801 * L_93 = V_13;
		NullCheck(L_93);
		List_1_t2208895230 * L_94 = L_93->get_iconRecords_3();
		NullCheck(L_94);
		int32_t L_95 = List_1_get_Count_m1758644702(L_94, /*hidden argument*/List_1_get_Count_m1758644702_RuntimeMethod_var);
		SmartPosterRecord_t1640848801 * L_96 = V_13;
		NullCheck(L_96);
		List_1_t4162620298 * L_97 = L_96->get_extraRecords_4();
		NullCheck(L_97);
		int32_t L_98 = List_1_get_Count_m2774962351(L_97, /*hidden argument*/List_1_get_Count_m2774962351_RuntimeMethod_var);
		V_14 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_92, (int32_t)L_95)), (int32_t)L_98));
		RecordItem_t1075151419 * L_99 = V_5;
		ObjectU5BU5D_t2843939325* L_100 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)6);
		ObjectU5BU5D_t2843939325* L_101 = L_100;
		int32_t L_102 = ((int32_t)4);
		RuntimeObject * L_103 = Box(NDEFRecordType_t4294622310_il2cpp_TypeInfo_var, &L_102);
		NullCheck(L_101);
		ArrayElementTypeCheck (L_101, L_103);
		(L_101)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_103);
		ObjectU5BU5D_t2843939325* L_104 = L_101;
		SmartPosterRecord_t1640848801 * L_105 = V_13;
		NullCheck(L_105);
		UriRecord_t2230063309 * L_106 = L_105->get_uriRecord_1();
		NullCheck(L_106);
		String_t* L_107 = L_106->get_fullUri_1();
		NullCheck(L_104);
		ArrayElementTypeCheck (L_104, L_107);
		(L_104)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_107);
		ObjectU5BU5D_t2843939325* L_108 = L_104;
		SmartPosterRecord_t1640848801 * L_109 = V_13;
		NullCheck(L_109);
		int32_t L_110 = L_109->get_action_5();
		int32_t L_111 = L_110;
		RuntimeObject * L_112 = Box(RecommendedAction_t1182550772_il2cpp_TypeInfo_var, &L_111);
		NullCheck(L_108);
		ArrayElementTypeCheck (L_108, L_112);
		(L_108)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_112);
		ObjectU5BU5D_t2843939325* L_113 = L_108;
		SmartPosterRecord_t1640848801 * L_114 = V_13;
		NullCheck(L_114);
		int32_t L_115 = L_114->get_size_6();
		int32_t L_116 = L_115;
		RuntimeObject * L_117 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_116);
		NullCheck(L_113);
		ArrayElementTypeCheck (L_113, L_117);
		(L_113)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_117);
		ObjectU5BU5D_t2843939325* L_118 = L_113;
		SmartPosterRecord_t1640848801 * L_119 = V_13;
		NullCheck(L_119);
		String_t* L_120 = L_119->get_mimeType_7();
		NullCheck(L_118);
		ArrayElementTypeCheck (L_118, L_120);
		(L_118)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_120);
		ObjectU5BU5D_t2843939325* L_121 = L_118;
		int32_t L_122 = V_14;
		int32_t L_123 = L_122;
		RuntimeObject * L_124 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_123);
		NullCheck(L_121);
		ArrayElementTypeCheck (L_121, L_124);
		(L_121)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_124);
		String_t* L_125 = String_Format_m630303134(NULL /*static, unused*/, _stringLiteral3439933068, L_121, /*hidden argument*/NULL);
		NullCheck(L_99);
		RecordItem_UpdateLabel_m1853417565(L_99, L_125, /*hidden argument*/NULL);
		goto IL_0284;
	}

IL_0284:
	{
		RecordItem_t1075151419 * L_126 = V_5;
		NullCheck(L_126);
		RectTransform_t3704657025 * L_127 = RecordItem_get_RectTransform_m3728143926(L_126, /*hidden argument*/NULL);
		float L_128 = V_0;
		Vector2_t2156229523  L_129;
		memset(&L_129, 0, sizeof(L_129));
		Vector2__ctor_m3970636864((&L_129), (0.0f), L_128, /*hidden argument*/NULL);
		NullCheck(L_127);
		RectTransform_set_anchoredPosition_m4126691837(L_127, L_129, /*hidden argument*/NULL);
		float L_130 = V_0;
		RecordItem_t1075151419 * L_131 = V_5;
		NullCheck(L_131);
		RectTransform_t3704657025 * L_132 = RecordItem_get_RectTransform_m3728143926(L_131, /*hidden argument*/NULL);
		NullCheck(L_132);
		Rect_t2360479859  L_133 = RectTransform_get_rect_m574169965(L_132, /*hidden argument*/NULL);
		V_15 = L_133;
		float L_134 = Rect_get_height_m1358425599((Rect_t2360479859 *)(&V_15), /*hidden argument*/NULL);
		V_0 = ((float)il2cpp_codegen_subtract((float)L_130, (float)L_134));
		float L_135 = V_0;
		V_0 = ((float)il2cpp_codegen_subtract((float)L_135, (float)(8.0f)));
		int32_t L_136 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_136, (int32_t)1));
	}

IL_02bf:
	{
		int32_t L_137 = V_3;
		int32_t L_138 = V_2;
		if ((((int32_t)L_137) < ((int32_t)L_138)))
		{
			goto IL_0021;
		}
	}
	{
		ScrollRect_t4137855814 * L_139 = __this->get_ndefMessageScrollRect_19();
		NullCheck(L_139);
		RectTransform_t3704657025 * L_140 = ScrollRect_get_content_m2477524320(L_139, /*hidden argument*/NULL);
		float L_141 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_142 = fabsf(L_141);
		Vector2_t2156229523  L_143;
		memset(&L_143, 0, sizeof(L_143));
		Vector2__ctor_m3970636864((&L_143), (0.0f), L_142, /*hidden argument*/NULL);
		NullCheck(L_140);
		RectTransform_set_sizeDelta_m3462269772(L_140, L_143, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.ReadScreenView::CleanupRecordItems()
extern "C" IL2CPP_METHOD_ATTR void ReadScreenView_CleanupRecordItems_m1746468235 (ReadScreenView_t239900869 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadScreenView_CleanupRecordItems_m1746468235_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3704657025 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		ScrollRect_t4137855814 * L_0 = __this->get_ndefMessageScrollRect_19();
		NullCheck(L_0);
		RectTransform_t3704657025 * L_1 = ScrollRect_get_content_m2477524320(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RectTransform_t3704657025 * L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = Transform_get_childCount_m3145433196(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		V_2 = 0;
		goto IL_002f;
	}

IL_001a:
	{
		RectTransform_t3704657025 * L_4 = V_0;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		Transform_t3600365921 * L_6 = Transform_GetChild_m1092972975(L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_002f:
	{
		int32_t L_9 = V_2;
		int32_t L_10 = V_1;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_001a;
		}
	}
	{
		return;
	}
}
// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.ReadScreenView::CreateRecordItem(DigitsNFCToolkit.Samples.RecordItem)
extern "C" IL2CPP_METHOD_ATTR RecordItem_t1075151419 * ReadScreenView_CreateRecordItem_m3952030190 (ReadScreenView_t239900869 * __this, RecordItem_t1075151419 * ___prefab0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadScreenView_CreateRecordItem_m3952030190_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RecordItem_t1075151419 * V_0 = NULL;
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RecordItem_t1075151419 * L_0 = ___prefab0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		RecordItem_t1075151419 * L_1 = Object_Instantiate_TisRecordItem_t1075151419_m880386264(NULL /*static, unused*/, L_0, /*hidden argument*/Object_Instantiate_TisRecordItem_t1075151419_m880386264_RuntimeMethod_var);
		V_0 = L_1;
		RecordItem_t1075151419 * L_2 = V_0;
		NullCheck(L_2);
		RectTransform_t3704657025 * L_3 = RecordItem_get_RectTransform_m3728143926(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector2_t2156229523  L_4 = RectTransform_get_sizeDelta_m2183112744(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		RecordItem_t1075151419 * L_5 = V_0;
		NullCheck(L_5);
		RectTransform_t3704657025 * L_6 = RecordItem_get_RectTransform_m3728143926(L_5, /*hidden argument*/NULL);
		ScrollRect_t4137855814 * L_7 = __this->get_ndefMessageScrollRect_19();
		NullCheck(L_7);
		RectTransform_t3704657025 * L_8 = ScrollRect_get_content_m2477524320(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_SetParent_m381167889(L_6, L_8, /*hidden argument*/NULL);
		RecordItem_t1075151419 * L_9 = V_0;
		NullCheck(L_9);
		RectTransform_t3704657025 * L_10 = RecordItem_get_RectTransform_m3728143926(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_11 = Vector2_get_one_m738793577(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_localScale_m3053443106(L_10, L_12, /*hidden argument*/NULL);
		RecordItem_t1075151419 * L_13 = V_0;
		NullCheck(L_13);
		RectTransform_t3704657025 * L_14 = RecordItem_get_RectTransform_m3728143926(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_15 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_localRotation_m19445462(L_14, L_15, /*hidden argument*/NULL);
		RecordItem_t1075151419 * L_16 = V_0;
		NullCheck(L_16);
		RectTransform_t3704657025 * L_17 = RecordItem_get_RectTransform_m3728143926(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_18 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_localPosition_m4128471975(L_17, L_18, /*hidden argument*/NULL);
		RecordItem_t1075151419 * L_19 = V_0;
		NullCheck(L_19);
		RectTransform_t3704657025 * L_20 = RecordItem_get_RectTransform_m3728143926(L_19, /*hidden argument*/NULL);
		Vector2_t2156229523  L_21 = V_1;
		NullCheck(L_20);
		RectTransform_set_sizeDelta_m3462269772(L_20, L_21, /*hidden argument*/NULL);
		RecordItem_t1075151419 * L_22 = V_0;
		return L_22;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.Samples.RecordItem::.ctor()
extern "C" IL2CPP_METHOD_ATTR void RecordItem__ctor_m1983231623 (RecordItem_t1075151419 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RectTransform DigitsNFCToolkit.Samples.RecordItem::get_RectTransform()
extern "C" IL2CPP_METHOD_ATTR RectTransform_t3704657025 * RecordItem_get_RectTransform_m3728143926 (RecordItem_t1075151419 * __this, const RuntimeMethod* method)
{
	{
		RectTransform_t3704657025 * L_0 = __this->get_U3CRectTransformU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void DigitsNFCToolkit.Samples.RecordItem::set_RectTransform(UnityEngine.RectTransform)
extern "C" IL2CPP_METHOD_ATTR void RecordItem_set_RectTransform_m3200480544 (RecordItem_t1075151419 * __this, RectTransform_t3704657025 * ___value0, const RuntimeMethod* method)
{
	{
		RectTransform_t3704657025 * L_0 = ___value0;
		__this->set_U3CRectTransformU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.RecordItem::Awake()
extern "C" IL2CPP_METHOD_ATTR void RecordItem_Awake_m673876687 (RecordItem_t1075151419 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RecordItem_Awake_m673876687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t3704657025 * L_0 = Component_GetComponent_TisRectTransform_t3704657025_m3396022872(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var);
		RecordItem_set_RectTransform_m3200480544(__this, L_0, /*hidden argument*/NULL);
		Text_t1901882714 * L_1 = Component_GetComponentInChildren_TisText_t1901882714_m396351542(__this, /*hidden argument*/Component_GetComponentInChildren_TisText_t1901882714_m396351542_RuntimeMethod_var);
		__this->set_label_5(L_1);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.RecordItem::UpdateLabel(System.String)
extern "C" IL2CPP_METHOD_ATTR void RecordItem_UpdateLabel_m1853417565 (RecordItem_t1075151419 * __this, String_t* ___text0, const RuntimeMethod* method)
{
	{
		Text_t1901882714 * L_0 = __this->get_label_5();
		String_t* L_1 = ___text0;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.Samples.WriteScreenControl::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenControl__ctor_m862936518 (WriteScreenControl_t1506090515 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenControl::Start()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenControl_Start_m777714958 (WriteScreenControl_t1506090515 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenControl_Start_m777714958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1281789340* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	TextEncodingU5BU5D_t2855432831* V_5 = NULL;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	IconIDU5BU5D_t2629431601* V_8 = NULL;
	int32_t V_9 = 0;
	{
		intptr_t L_0 = (intptr_t)WriteScreenControl_OnNDEFWriteFinished_m3757117548_RuntimeMethod_var;
		OnNDEFWriteFinished_t4102039599 * L_1 = (OnNDEFWriteFinished_t4102039599 *)il2cpp_codegen_object_new(OnNDEFWriteFinished_t4102039599_il2cpp_TypeInfo_var);
		OnNDEFWriteFinished__ctor_m1674872822(L_1, __this, (intptr_t)L_0, /*hidden argument*/NULL);
		NativeNFCManager_AddNDEFWriteFinishedListener_m2087486335(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		intptr_t L_2 = (intptr_t)WriteScreenControl_OnNDEFPushFinished_m3165364698_RuntimeMethod_var;
		OnNDEFPushFinished_t4279917764 * L_3 = (OnNDEFPushFinished_t4279917764 *)il2cpp_codegen_object_new(OnNDEFPushFinished_t4279917764_il2cpp_TypeInfo_var);
		OnNDEFPushFinished__ctor_m1418827824(L_3, __this, (intptr_t)L_2, /*hidden argument*/NULL);
		NativeNFCManager_AddNDEFPushFinishedListener_m3015772070(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		StringU5BU5D_t1281789340* L_4 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_t1281789340* L_5 = L_4;
		V_1 = 5;
		RuntimeObject * L_6 = Box(NDEFRecordType_t4294622310_il2cpp_TypeInfo_var, (&V_1));
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		V_1 = *(int32_t*)UnBox(L_6);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_7);
		StringU5BU5D_t1281789340* L_8 = L_5;
		V_2 = 7;
		RuntimeObject * L_9 = Box(NDEFRecordType_t4294622310_il2cpp_TypeInfo_var, (&V_2));
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		V_2 = *(int32_t*)UnBox(L_9);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_10);
		StringU5BU5D_t1281789340* L_11 = L_8;
		V_3 = 3;
		RuntimeObject * L_12 = Box(NDEFRecordType_t4294622310_il2cpp_TypeInfo_var, (&V_3));
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_12);
		V_3 = *(int32_t*)UnBox(L_12);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_13);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_13);
		StringU5BU5D_t1281789340* L_14 = L_11;
		V_4 = 2;
		RuntimeObject * L_15 = Box(NDEFRecordType_t4294622310_il2cpp_TypeInfo_var, (&V_4));
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_4 = *(int32_t*)UnBox(L_15);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_16);
		V_0 = L_14;
		WriteScreenView_t2350253495 * L_17 = __this->get_view_4();
		StringU5BU5D_t1281789340* L_18 = V_0;
		NullCheck(L_17);
		WriteScreenView_UpdateTypeDropdownOptions_m2278996124(L_17, L_18, /*hidden argument*/NULL);
		RuntimeTypeHandle_t3027515415  L_19 = { reinterpret_cast<intptr_t> (TextEncoding_t3210513626_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t4135868527_il2cpp_TypeInfo_var);
		RuntimeArray * L_21 = Enum_GetValues_m4192343468(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		RuntimeObject* L_22 = Enumerable_Cast_TisTextEncoding_t3210513626_m2305854455(NULL /*static, unused*/, L_21, /*hidden argument*/Enumerable_Cast_TisTextEncoding_t3210513626_m2305854455_RuntimeMethod_var);
		TextEncodingU5BU5D_t2855432831* L_23 = Enumerable_ToArray_TisTextEncoding_t3210513626_m1218064777(NULL /*static, unused*/, L_22, /*hidden argument*/Enumerable_ToArray_TisTextEncoding_t3210513626_m1218064777_RuntimeMethod_var);
		V_5 = L_23;
		TextEncodingU5BU5D_t2855432831* L_24 = V_5;
		NullCheck(L_24);
		V_6 = (((int32_t)((int32_t)(((RuntimeArray *)L_24)->max_length))));
		int32_t L_25 = V_6;
		StringU5BU5D_t1281789340* L_26 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)L_25);
		V_0 = L_26;
		V_7 = 0;
		goto IL_00cd;
	}

IL_00af:
	{
		StringU5BU5D_t1281789340* L_27 = V_0;
		int32_t L_28 = V_7;
		TextEncodingU5BU5D_t2855432831* L_29 = V_5;
		int32_t L_30 = V_7;
		NullCheck(L_29);
		RuntimeObject * L_31 = Box(TextEncoding_t3210513626_il2cpp_TypeInfo_var, ((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_30))));
		NullCheck(L_31);
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_31);
		*((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_30))) = *(int32_t*)UnBox(L_31);
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, L_32);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(L_28), (String_t*)L_32);
		int32_t L_33 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1));
	}

IL_00cd:
	{
		int32_t L_34 = V_7;
		int32_t L_35 = V_6;
		if ((((int32_t)L_34) < ((int32_t)L_35)))
		{
			goto IL_00af;
		}
	}
	{
		WriteScreenView_t2350253495 * L_36 = __this->get_view_4();
		StringU5BU5D_t1281789340* L_37 = V_0;
		NullCheck(L_36);
		WriteScreenView_UpdateTextEncodingDropdownOptions_m2236664622(L_36, L_37, /*hidden argument*/NULL);
		RuntimeTypeHandle_t3027515415  L_38 = { reinterpret_cast<intptr_t> (IconID_t582738576_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t4135868527_il2cpp_TypeInfo_var);
		RuntimeArray * L_40 = Enum_GetValues_m4192343468(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		RuntimeObject* L_41 = Enumerable_Cast_TisIconID_t582738576_m3494484091(NULL /*static, unused*/, L_40, /*hidden argument*/Enumerable_Cast_TisIconID_t582738576_m3494484091_RuntimeMethod_var);
		IconIDU5BU5D_t2629431601* L_42 = Enumerable_ToArray_TisIconID_t582738576_m2108801283(NULL /*static, unused*/, L_41, /*hidden argument*/Enumerable_ToArray_TisIconID_t582738576_m2108801283_RuntimeMethod_var);
		V_8 = L_42;
		IconIDU5BU5D_t2629431601* L_43 = V_8;
		NullCheck(L_43);
		V_6 = (((int32_t)((int32_t)(((RuntimeArray *)L_43)->max_length))));
		int32_t L_44 = V_6;
		StringU5BU5D_t1281789340* L_45 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)L_44);
		V_0 = L_45;
		V_9 = 0;
		goto IL_0131;
	}

IL_0113:
	{
		StringU5BU5D_t1281789340* L_46 = V_0;
		int32_t L_47 = V_9;
		IconIDU5BU5D_t2629431601* L_48 = V_8;
		int32_t L_49 = V_9;
		NullCheck(L_48);
		RuntimeObject * L_50 = Box(IconID_t582738576_il2cpp_TypeInfo_var, ((L_48)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_49))));
		NullCheck(L_50);
		String_t* L_51 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_50);
		*((L_48)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_49))) = *(int32_t*)UnBox(L_50);
		NullCheck(L_46);
		ArrayElementTypeCheck (L_46, L_51);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(L_47), (String_t*)L_51);
		int32_t L_52 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_52, (int32_t)1));
	}

IL_0131:
	{
		int32_t L_53 = V_9;
		int32_t L_54 = V_6;
		if ((((int32_t)L_53) < ((int32_t)L_54)))
		{
			goto IL_0113;
		}
	}
	{
		WriteScreenView_t2350253495 * L_55 = __this->get_view_4();
		StringU5BU5D_t1281789340* L_56 = V_0;
		NullCheck(L_55);
		WriteScreenView_UpdateIconDropdownOptions_m304314056(L_55, L_56, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenControl::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenControl_OnEnable_m361695074 (WriteScreenControl_t1506090515 * __this, const RuntimeMethod* method)
{
	{
		WriteScreenView_t2350253495 * L_0 = __this->get_view_4();
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenControl::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenControl_OnDisable_m2989907286 (WriteScreenControl_t1506090515 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenControl_OnDisable_m2989907286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WriteScreenView_t2350253495 * L_0 = __this->get_view_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		WriteScreenView_t2350253495 * L_2 = __this->get_view_4();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// DigitsNFCToolkit.SmartPosterRecord DigitsNFCToolkit.Samples.WriteScreenControl::CreateTestSmartPosterRecord()
extern "C" IL2CPP_METHOD_ATTR SmartPosterRecord_t1640848801 * WriteScreenControl_CreateTestSmartPosterRecord_m2515146452 (WriteScreenControl_t1506090515 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenControl_CreateTestSmartPosterRecord_m2515146452_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3785772365 * V_0 = NULL;
	List_1_t2208895230 * V_1 = NULL;
	List_1_t4162620298 * V_2 = NULL;
	{
		List_1_t3785772365 * L_0 = (List_1_t3785772365 *)il2cpp_codegen_object_new(List_1_t3785772365_il2cpp_TypeInfo_var);
		List_1__ctor_m2409190732(L_0, /*hidden argument*/List_1__ctor_m2409190732_RuntimeMethod_var);
		V_0 = L_0;
		List_1_t3785772365 * L_1 = V_0;
		TextRecord_t2313697623 * L_2 = (TextRecord_t2313697623 *)il2cpp_codegen_object_new(TextRecord_t2313697623_il2cpp_TypeInfo_var);
		TextRecord__ctor_m3042244564(L_2, _stringLiteral3774377648, _stringLiteral3454973890, /*hidden argument*/NULL);
		NullCheck(L_1);
		List_1_Add_m2718096786(L_1, L_2, /*hidden argument*/List_1_Add_m2718096786_RuntimeMethod_var);
		List_1_t3785772365 * L_3 = V_0;
		TextRecord_t2313697623 * L_4 = (TextRecord_t2313697623 *)il2cpp_codegen_object_new(TextRecord_t2313697623_il2cpp_TypeInfo_var);
		TextRecord__ctor_m3042244564(L_4, _stringLiteral2634224520, _stringLiteral3454842811, /*hidden argument*/NULL);
		NullCheck(L_3);
		List_1_Add_m2718096786(L_3, L_4, /*hidden argument*/List_1_Add_m2718096786_RuntimeMethod_var);
		List_1_t2208895230 * L_5 = (List_1_t2208895230 *)il2cpp_codegen_object_new(List_1_t2208895230_il2cpp_TypeInfo_var);
		List_1__ctor_m2367744050(L_5, /*hidden argument*/List_1__ctor_m2367744050_RuntimeMethod_var);
		V_1 = L_5;
		List_1_t4162620298 * L_6 = (List_1_t4162620298 *)il2cpp_codegen_object_new(List_1_t4162620298_il2cpp_TypeInfo_var);
		List_1__ctor_m1332560254(L_6, /*hidden argument*/List_1__ctor_m1332560254_RuntimeMethod_var);
		V_2 = L_6;
		List_1_t4162620298 * L_7 = V_2;
		TextRecord_t2313697623 * L_8 = (TextRecord_t2313697623 *)il2cpp_codegen_object_new(TextRecord_t2313697623_il2cpp_TypeInfo_var);
		TextRecord__ctor_m3042244564(L_8, _stringLiteral1504015411, _stringLiteral3454973890, /*hidden argument*/NULL);
		NullCheck(L_7);
		List_1_Add_m350525082(L_7, L_8, /*hidden argument*/List_1_Add_m350525082_RuntimeMethod_var);
		List_1_t4162620298 * L_9 = V_2;
		TextRecord_t2313697623 * L_10 = (TextRecord_t2313697623 *)il2cpp_codegen_object_new(TextRecord_t2313697623_il2cpp_TypeInfo_var);
		TextRecord__ctor_m3042244564(L_10, _stringLiteral2361935695, _stringLiteral3454842811, /*hidden argument*/NULL);
		NullCheck(L_9);
		List_1_Add_m350525082(L_9, L_10, /*hidden argument*/List_1_Add_m350525082_RuntimeMethod_var);
		List_1_t4162620298 * L_11 = V_2;
		UriRecord_t2230063309 * L_12 = (UriRecord_t2230063309 *)il2cpp_codegen_object_new(UriRecord_t2230063309_il2cpp_TypeInfo_var);
		UriRecord__ctor_m714533555(L_12, _stringLiteral363185568, /*hidden argument*/NULL);
		NullCheck(L_11);
		List_1_Add_m350525082(L_11, L_12, /*hidden argument*/List_1_Add_m350525082_RuntimeMethod_var);
		List_1_t3785772365 * L_13 = V_0;
		List_1_t2208895230 * L_14 = V_1;
		List_1_t4162620298 * L_15 = V_2;
		SmartPosterRecord_t1640848801 * L_16 = (SmartPosterRecord_t1640848801 *)il2cpp_codegen_object_new(SmartPosterRecord_t1640848801_il2cpp_TypeInfo_var);
		SmartPosterRecord__ctor_m1413753803(L_16, _stringLiteral3008852884, 1, ((int32_t)1534690), _stringLiteral1377815833, L_13, L_14, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenControl::OnRecordTypeChanged(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void WriteScreenControl_OnRecordTypeChanged_m3725801358 (WriteScreenControl_t1506090515 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenControl_OnRecordTypeChanged_m3725801358_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		WriteScreenView_t2350253495 * L_0 = __this->get_view_4();
		NullCheck(L_0);
		Dropdown_t2274391225 * L_1 = WriteScreenView_get_TypeDropdown_m169477996(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		List_1_t447389798 * L_2 = Dropdown_get_options_m2762539965(L_1, /*hidden argument*/NULL);
		int32_t L_3 = ___index0;
		NullCheck(L_2);
		OptionData_t3270282352 * L_4 = List_1_get_Item_m489380024(L_2, L_3, /*hidden argument*/List_1_get_Item_m489380024_RuntimeMethod_var);
		NullCheck(L_4);
		String_t* L_5 = OptionData_get_text_m2997376818(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RuntimeTypeHandle_t3027515415  L_6 = { reinterpret_cast<intptr_t> (NDEFRecordType_t4294622310_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		String_t* L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t4135868527_il2cpp_TypeInfo_var);
		RuntimeObject * L_9 = Enum_Parse_m1871331077(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_1 = ((*(int32_t*)((int32_t*)UnBox(L_9, NDEFRecordType_t4294622310_il2cpp_TypeInfo_var))));
		int32_t L_10 = V_1;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_10, (int32_t)2)))
		{
			case 0:
			{
				goto IL_0087;
			}
			case 1:
			{
				goto IL_0077;
			}
			case 2:
			{
				goto IL_0097;
			}
			case 3:
			{
				goto IL_0057;
			}
			case 4:
			{
				goto IL_0097;
			}
			case 5:
			{
				goto IL_0067;
			}
		}
	}
	{
		goto IL_0097;
	}

IL_0057:
	{
		WriteScreenView_t2350253495 * L_11 = __this->get_view_4();
		NullCheck(L_11);
		WriteScreenView_SwitchToTextRecordInput_m801913819(L_11, /*hidden argument*/NULL);
		goto IL_0097;
	}

IL_0067:
	{
		WriteScreenView_t2350253495 * L_12 = __this->get_view_4();
		NullCheck(L_12);
		WriteScreenView_SwitchToUriInput_m1218801993(L_12, /*hidden argument*/NULL);
		goto IL_0097;
	}

IL_0077:
	{
		WriteScreenView_t2350253495 * L_13 = __this->get_view_4();
		NullCheck(L_13);
		WriteScreenView_SwitchToMimeMediaInput_m1134766923(L_13, /*hidden argument*/NULL);
		goto IL_0097;
	}

IL_0087:
	{
		WriteScreenView_t2350253495 * L_14 = __this->get_view_4();
		NullCheck(L_14);
		WriteScreenView_SwitchToExternalTypeInput_m1529510719(L_14, /*hidden argument*/NULL);
		goto IL_0097;
	}

IL_0097:
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenControl::OnAddRecordClick()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenControl_OnAddRecordClick_m2006621214 (WriteScreenControl_t1506090515 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenControl_OnAddRecordClick_m2006621214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NDEFRecord_t2690545556 * V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	int32_t V_5 = 0;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	int32_t V_8 = 0;
	ByteU5BU5D_t4116647657* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	ByteU5BU5D_t4116647657* V_13 = NULL;
	{
		NDEFMessage_t279637043 * L_0 = __this->get_pendingMessage_9();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		NDEFMessage_t279637043 * L_1 = (NDEFMessage_t279637043 *)il2cpp_codegen_object_new(NDEFMessage_t279637043_il2cpp_TypeInfo_var);
		NDEFMessage__ctor_m2200006917(L_1, /*hidden argument*/NULL);
		__this->set_pendingMessage_9(L_1);
	}

IL_0016:
	{
		V_0 = (NDEFRecord_t2690545556 *)NULL;
		WriteScreenView_t2350253495 * L_2 = __this->get_view_4();
		NullCheck(L_2);
		Dropdown_t2274391225 * L_3 = WriteScreenView_get_TypeDropdown_m169477996(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		List_1_t447389798 * L_4 = Dropdown_get_options_m2762539965(L_3, /*hidden argument*/NULL);
		WriteScreenView_t2350253495 * L_5 = __this->get_view_4();
		NullCheck(L_5);
		Dropdown_t2274391225 * L_6 = WriteScreenView_get_TypeDropdown_m169477996(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = Dropdown_get_value_m1555353112(L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		OptionData_t3270282352 * L_8 = List_1_get_Item_m489380024(L_4, L_7, /*hidden argument*/List_1_get_Item_m489380024_RuntimeMethod_var);
		NullCheck(L_8);
		String_t* L_9 = OptionData_get_text_m2997376818(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		RuntimeTypeHandle_t3027515415  L_10 = { reinterpret_cast<intptr_t> (NDEFRecordType_t4294622310_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		String_t* L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t4135868527_il2cpp_TypeInfo_var);
		RuntimeObject * L_13 = Enum_Parse_m1871331077(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_2 = ((*(int32_t*)((int32_t*)UnBox(L_13, NDEFRecordType_t4294622310_il2cpp_TypeInfo_var))));
		int32_t L_14 = V_2;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_14, (int32_t)2)))
		{
			case 0:
			{
				goto IL_0128;
			}
			case 1:
			{
				goto IL_00f6;
			}
			case 2:
			{
				goto IL_017d;
			}
			case 3:
			{
				goto IL_007e;
			}
			case 4:
			{
				goto IL_017d;
			}
			case 5:
			{
				goto IL_00d7;
			}
		}
	}
	{
		goto IL_017d;
	}

IL_007e:
	{
		WriteScreenView_t2350253495 * L_15 = __this->get_view_4();
		NullCheck(L_15);
		InputField_t3762917431 * L_16 = WriteScreenView_get_TextInput_m1287173561(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		String_t* L_17 = InputField_get_text_m3534748202(L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		WriteScreenView_t2350253495 * L_18 = __this->get_view_4();
		NullCheck(L_18);
		InputField_t3762917431 * L_19 = WriteScreenView_get_LanguageCodeInput_m2131884372(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		String_t* L_20 = InputField_get_text_m3534748202(L_19, /*hidden argument*/NULL);
		V_4 = L_20;
		String_t* L_21 = V_4;
		NullCheck(L_21);
		int32_t L_22 = String_get_Length_m3847582255(L_21, /*hidden argument*/NULL);
		if ((((int32_t)L_22) == ((int32_t)2)))
		{
			goto IL_00b5;
		}
	}
	{
		V_4 = _stringLiteral3454842811;
	}

IL_00b5:
	{
		WriteScreenView_t2350253495 * L_23 = __this->get_view_4();
		NullCheck(L_23);
		Dropdown_t2274391225 * L_24 = WriteScreenView_get_TextEncodingDropdown_m1795396977(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		int32_t L_25 = Dropdown_get_value_m1555353112(L_24, /*hidden argument*/NULL);
		V_5 = L_25;
		String_t* L_26 = V_3;
		String_t* L_27 = V_4;
		int32_t L_28 = V_5;
		TextRecord_t2313697623 * L_29 = (TextRecord_t2313697623 *)il2cpp_codegen_object_new(TextRecord_t2313697623_il2cpp_TypeInfo_var);
		TextRecord__ctor_m2191892803(L_29, L_26, L_27, L_28, /*hidden argument*/NULL);
		V_0 = L_29;
		goto IL_017d;
	}

IL_00d7:
	{
		WriteScreenView_t2350253495 * L_30 = __this->get_view_4();
		NullCheck(L_30);
		InputField_t3762917431 * L_31 = WriteScreenView_get_UriInput_m822036843(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		String_t* L_32 = InputField_get_text_m3534748202(L_31, /*hidden argument*/NULL);
		V_6 = L_32;
		String_t* L_33 = V_6;
		UriRecord_t2230063309 * L_34 = (UriRecord_t2230063309 *)il2cpp_codegen_object_new(UriRecord_t2230063309_il2cpp_TypeInfo_var);
		UriRecord__ctor_m714533555(L_34, L_33, /*hidden argument*/NULL);
		V_0 = L_34;
		goto IL_017d;
	}

IL_00f6:
	{
		V_7 = _stringLiteral2045074213;
		WriteScreenView_t2350253495 * L_35 = __this->get_view_4();
		NullCheck(L_35);
		Dropdown_t2274391225 * L_36 = WriteScreenView_get_IconDropdown_m124205079(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		int32_t L_37 = Dropdown_get_value_m1555353112(L_36, /*hidden argument*/NULL);
		V_8 = L_37;
		int32_t L_38 = V_8;
		ByteU5BU5D_t4116647657* L_39 = WriteScreenControl_GetIconBytes_m2925827785(__this, L_38, /*hidden argument*/NULL);
		V_9 = L_39;
		String_t* L_40 = V_7;
		ByteU5BU5D_t4116647657* L_41 = V_9;
		MimeMediaRecord_t736820488 * L_42 = (MimeMediaRecord_t736820488 *)il2cpp_codegen_object_new(MimeMediaRecord_t736820488_il2cpp_TypeInfo_var);
		MimeMediaRecord__ctor_m353281183(L_42, L_40, L_41, /*hidden argument*/NULL);
		V_0 = L_42;
		goto IL_017d;
	}

IL_0128:
	{
		WriteScreenView_t2350253495 * L_43 = __this->get_view_4();
		NullCheck(L_43);
		InputField_t3762917431 * L_44 = WriteScreenView_get_DomainNameInput_m3346781220(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		String_t* L_45 = InputField_get_text_m3534748202(L_44, /*hidden argument*/NULL);
		V_10 = L_45;
		WriteScreenView_t2350253495 * L_46 = __this->get_view_4();
		NullCheck(L_46);
		InputField_t3762917431 * L_47 = WriteScreenView_get_DomainTypeInput_m3846803875(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		String_t* L_48 = InputField_get_text_m3534748202(L_47, /*hidden argument*/NULL);
		V_11 = L_48;
		WriteScreenView_t2350253495 * L_49 = __this->get_view_4();
		NullCheck(L_49);
		InputField_t3762917431 * L_50 = WriteScreenView_get_DomainDataInput_m3660011111(L_49, /*hidden argument*/NULL);
		NullCheck(L_50);
		String_t* L_51 = InputField_get_text_m3534748202(L_50, /*hidden argument*/NULL);
		V_12 = L_51;
		Encoding_t1523322056 * L_52 = Encoding_get_UTF8_m1008486739(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_53 = V_12;
		NullCheck(L_52);
		ByteU5BU5D_t4116647657* L_54 = VirtFuncInvoker1< ByteU5BU5D_t4116647657*, String_t* >::Invoke(16 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_52, L_53);
		V_13 = L_54;
		String_t* L_55 = V_10;
		String_t* L_56 = V_11;
		ByteU5BU5D_t4116647657* L_57 = V_13;
		ExternalTypeRecord_t4087466745 * L_58 = (ExternalTypeRecord_t4087466745 *)il2cpp_codegen_object_new(ExternalTypeRecord_t4087466745_il2cpp_TypeInfo_var);
		ExternalTypeRecord__ctor_m222117595(L_58, L_55, L_56, L_57, /*hidden argument*/NULL);
		V_0 = L_58;
		goto IL_017d;
	}

IL_017d:
	{
		NDEFMessage_t279637043 * L_59 = __this->get_pendingMessage_9();
		NullCheck(L_59);
		List_1_t4162620298 * L_60 = NDEFMessage_get_Records_m1930208688(L_59, /*hidden argument*/NULL);
		NDEFRecord_t2690545556 * L_61 = V_0;
		NullCheck(L_60);
		List_1_Add_m350525082(L_60, L_61, /*hidden argument*/List_1_Add_m350525082_RuntimeMethod_var);
		WriteScreenView_t2350253495 * L_62 = __this->get_view_4();
		NDEFMessage_t279637043 * L_63 = __this->get_pendingMessage_9();
		NullCheck(L_62);
		WriteScreenView_UpdateNDEFMessage_m4183170585(L_62, L_63, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] DigitsNFCToolkit.Samples.WriteScreenControl::GetIconBytes(DigitsNFCToolkit.Samples.WriteScreenControl/IconID)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_t4116647657* WriteScreenControl_GetIconBytes_m2925827785 (WriteScreenControl_t1506090515 * __this, int32_t ___iconID0, const RuntimeMethod* method)
{
	ByteU5BU5D_t4116647657* V_0 = NULL;
	{
		V_0 = (ByteU5BU5D_t4116647657*)NULL;
		int32_t L_0 = ___iconID0;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_1 = ___iconID0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_2 = ___iconID0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0047;
		}
	}
	{
		goto IL_005d;
	}

IL_001b:
	{
		Sprite_t280657092 * L_3 = __this->get_musicNoteIcon_6();
		NullCheck(L_3);
		Texture2D_t3840446185 * L_4 = Sprite_get_texture_m3976398399(L_3, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_5 = ImageConversion_EncodeToPNG_m2292631531(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_005d;
	}

IL_0031:
	{
		Sprite_t280657092 * L_6 = __this->get_faceIcon_7();
		NullCheck(L_6);
		Texture2D_t3840446185 * L_7 = Sprite_get_texture_m3976398399(L_6, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_8 = ImageConversion_EncodeToPNG_m2292631531(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_005d;
	}

IL_0047:
	{
		Sprite_t280657092 * L_9 = __this->get_arrowIcon_8();
		NullCheck(L_9);
		Texture2D_t3840446185 * L_10 = Sprite_get_texture_m3976398399(L_9, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_11 = ImageConversion_EncodeToPNG_m2292631531(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_005d;
	}

IL_005d:
	{
		ByteU5BU5D_t4116647657* L_12 = V_0;
		return L_12;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenControl::OnClearMessageClick()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenControl_OnClearMessageClick_m1257383075 (WriteScreenControl_t1506090515 * __this, const RuntimeMethod* method)
{
	{
		__this->set_pendingMessage_9((NDEFMessage_t279637043 *)NULL);
		WriteScreenView_t2350253495 * L_0 = __this->get_view_4();
		NullCheck(L_0);
		WriteScreenView_CleanupRecordItems_m619684253(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenControl::OnWriteMessageClick()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenControl_OnWriteMessageClick_m1467433300 (WriteScreenControl_t1506090515 * __this, const RuntimeMethod* method)
{
	{
		NDEFMessage_t279637043 * L_0 = __this->get_pendingMessage_9();
		if (!L_0)
		{
			goto IL_000b;
		}
	}

IL_000b:
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenControl::OnWriteOKClick()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenControl_OnWriteOKClick_m1567200970 (WriteScreenControl_t1506090515 * __this, const RuntimeMethod* method)
{
	{
		MessageScreenView_t146641597 * L_0 = __this->get_messageScreenView_5();
		NullCheck(L_0);
		MessageScreenView_Hide_m3938180501(L_0, /*hidden argument*/NULL);
		__this->set_pendingMessage_9((NDEFMessage_t279637043 *)NULL);
		WriteScreenView_t2350253495 * L_1 = __this->get_view_4();
		NullCheck(L_1);
		WriteScreenView_CleanupRecordItems_m619684253(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenControl::OnWriteCancelClick()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenControl_OnWriteCancelClick_m2618116806 (WriteScreenControl_t1506090515 * __this, const RuntimeMethod* method)
{
	{
		MessageScreenView_t146641597 * L_0 = __this->get_messageScreenView_5();
		NullCheck(L_0);
		MessageScreenView_Hide_m3938180501(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenControl::OnPushMessageClick()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenControl_OnPushMessageClick_m2167233052 (WriteScreenControl_t1506090515 * __this, const RuntimeMethod* method)
{
	{
		NDEFMessage_t279637043 * L_0 = __this->get_pendingMessage_9();
		if (!L_0)
		{
			goto IL_000b;
		}
	}

IL_000b:
	{
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenControl::OnPushOKClick()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenControl_OnPushOKClick_m233519614 (WriteScreenControl_t1506090515 * __this, const RuntimeMethod* method)
{
	{
		MessageScreenView_t146641597 * L_0 = __this->get_messageScreenView_5();
		NullCheck(L_0);
		MessageScreenView_Hide_m3938180501(L_0, /*hidden argument*/NULL);
		__this->set_pendingMessage_9((NDEFMessage_t279637043 *)NULL);
		WriteScreenView_t2350253495 * L_1 = __this->get_view_4();
		NullCheck(L_1);
		WriteScreenView_CleanupRecordItems_m619684253(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenControl::OnPushCancelClick()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenControl_OnPushCancelClick_m3228935159 (WriteScreenControl_t1506090515 * __this, const RuntimeMethod* method)
{
	{
		MessageScreenView_t146641597 * L_0 = __this->get_messageScreenView_5();
		NullCheck(L_0);
		MessageScreenView_Hide_m3938180501(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenControl::OnNDEFWriteFinished(DigitsNFCToolkit.NDEFWriteResult)
extern "C" IL2CPP_METHOD_ATTR void WriteScreenControl_OnNDEFWriteFinished_m3757117548 (WriteScreenControl_t1506090515 * __this, NDEFWriteResult_t4210562629 * ___result0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenControl_OnNDEFWriteFinished_m3757117548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		WriteScreenView_t2350253495 * L_0 = __this->get_view_4();
		NDEFWriteResult_t4210562629 * L_1 = ___result0;
		NullCheck(L_1);
		NDEFMessage_t279637043 * L_2 = NDEFWriteResult_get_Message_m1148114373(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		WriteScreenView_UpdateNDEFMessage_m4183170585(L_0, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		V_0 = L_3;
		NDEFWriteResult_t4210562629 * L_4 = ___result0;
		NullCheck(L_4);
		bool L_5 = NDEFWriteResult_get_Success_m3422449045(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0038;
		}
	}
	{
		NDEFWriteResult_t4210562629 * L_6 = ___result0;
		NullCheck(L_6);
		String_t* L_7 = NDEFWriteResult_get_TagID_m1976340487(L_6, /*hidden argument*/NULL);
		String_t* L_8 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral363395119, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0054;
	}

IL_0038:
	{
		NDEFWriteResult_t4210562629 * L_9 = ___result0;
		NullCheck(L_9);
		String_t* L_10 = NDEFWriteResult_get_TagID_m1976340487(L_9, /*hidden argument*/NULL);
		NDEFWriteResult_t4210562629 * L_11 = ___result0;
		NullCheck(L_11);
		int32_t L_12 = NDEFWriteResult_get_Error_m2217684677(L_11, /*hidden argument*/NULL);
		int32_t L_13 = L_12;
		RuntimeObject * L_14 = Box(NDEFWriteError_t890528595_il2cpp_TypeInfo_var, &L_13);
		String_t* L_15 = String_Format_m2556382932(NULL /*static, unused*/, _stringLiteral3530183375, L_10, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
	}

IL_0054:
	{
		String_t* L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		MessageScreenView_t146641597 * L_17 = __this->get_messageScreenView_5();
		String_t* L_18 = V_0;
		NullCheck(L_17);
		MessageScreenView_SwitchToWriteResult_m1196524044(L_17, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenControl::OnNDEFPushFinished(DigitsNFCToolkit.NDEFPushResult)
extern "C" IL2CPP_METHOD_ATTR void WriteScreenControl_OnNDEFPushFinished_m3165364698 (WriteScreenControl_t1506090515 * __this, NDEFPushResult_t3422827153 * ___result0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenControl_OnNDEFPushFinished_m3165364698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		WriteScreenView_t2350253495 * L_0 = __this->get_view_4();
		NDEFPushResult_t3422827153 * L_1 = ___result0;
		NullCheck(L_1);
		NDEFMessage_t279637043 * L_2 = NDEFPushResult_get_Message_m1817705072(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		WriteScreenView_UpdateNDEFMessage_m4183170585(L_0, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		V_0 = L_3;
		NDEFPushResult_t3422827153 * L_4 = ___result0;
		NullCheck(L_4);
		bool L_5 = NDEFPushResult_get_Success_m1923447426(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002d;
		}
	}
	{
		V_0 = _stringLiteral150590343;
		goto IL_0033;
	}

IL_002d:
	{
		V_0 = _stringLiteral4041715024;
	}

IL_0033:
	{
		String_t* L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		MessageScreenView_t146641597 * L_7 = __this->get_messageScreenView_5();
		String_t* L_8 = V_0;
		NullCheck(L_7);
		MessageScreenView_SwitchToPushResult_m1664855704(L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView__ctor_m3713638999 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.Dropdown DigitsNFCToolkit.Samples.WriteScreenView::get_TypeDropdown()
extern "C" IL2CPP_METHOD_ATTR Dropdown_t2274391225 * WriteScreenView_get_TypeDropdown_m169477996 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	{
		Dropdown_t2274391225 * L_0 = __this->get_typeDropdown_17();
		return L_0;
	}
}
// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::get_TextInput()
extern "C" IL2CPP_METHOD_ATTR InputField_t3762917431 * WriteScreenView_get_TextInput_m1287173561 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	{
		InputField_t3762917431 * L_0 = __this->get_textInput_19();
		return L_0;
	}
}
// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::get_LanguageCodeInput()
extern "C" IL2CPP_METHOD_ATTR InputField_t3762917431 * WriteScreenView_get_LanguageCodeInput_m2131884372 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	{
		InputField_t3762917431 * L_0 = __this->get_languageCodeInput_20();
		return L_0;
	}
}
// UnityEngine.UI.Dropdown DigitsNFCToolkit.Samples.WriteScreenView::get_TextEncodingDropdown()
extern "C" IL2CPP_METHOD_ATTR Dropdown_t2274391225 * WriteScreenView_get_TextEncodingDropdown_m1795396977 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	{
		Dropdown_t2274391225 * L_0 = __this->get_textEncodingDropdown_21();
		return L_0;
	}
}
// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::get_UriInput()
extern "C" IL2CPP_METHOD_ATTR InputField_t3762917431 * WriteScreenView_get_UriInput_m822036843 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	{
		InputField_t3762917431 * L_0 = __this->get_uriInput_24();
		return L_0;
	}
}
// UnityEngine.UI.Dropdown DigitsNFCToolkit.Samples.WriteScreenView::get_IconDropdown()
extern "C" IL2CPP_METHOD_ATTR Dropdown_t2274391225 * WriteScreenView_get_IconDropdown_m124205079 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	{
		Dropdown_t2274391225 * L_0 = __this->get_iconDropdown_26();
		return L_0;
	}
}
// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::get_DomainNameInput()
extern "C" IL2CPP_METHOD_ATTR InputField_t3762917431 * WriteScreenView_get_DomainNameInput_m3346781220 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	{
		InputField_t3762917431 * L_0 = __this->get_domainNameInput_29();
		return L_0;
	}
}
// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::get_DomainTypeInput()
extern "C" IL2CPP_METHOD_ATTR InputField_t3762917431 * WriteScreenView_get_DomainTypeInput_m3846803875 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	{
		InputField_t3762917431 * L_0 = __this->get_domainTypeInput_30();
		return L_0;
	}
}
// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::get_DomainDataInput()
extern "C" IL2CPP_METHOD_ATTR InputField_t3762917431 * WriteScreenView_get_DomainDataInput_m3660011111 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	{
		InputField_t3762917431 * L_0 = __this->get_domainDataInput_31();
		return L_0;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::Awake()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_Awake_m1065143798 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenView_Awake_m1065143798_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = Transform_Find_m1729760951(L_0, _stringLiteral110252601, /*hidden argument*/NULL);
		NullCheck(L_1);
		ScrollRect_t4137855814 * L_2 = Component_GetComponent_TisScrollRect_t4137855814_m3636214290(L_1, /*hidden argument*/Component_GetComponent_TisScrollRect_t4137855814_m3636214290_RuntimeMethod_var);
		__this->set_ndefMessageScrollRect_15(L_2);
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3600365921 * L_4 = Transform_Find_m1729760951(L_3, _stringLiteral2734644804, /*hidden argument*/NULL);
		NullCheck(L_4);
		Text_t1901882714 * L_5 = Component_GetComponent_TisText_t1901882714_m4196288697(L_4, /*hidden argument*/Component_GetComponent_TisText_t1901882714_m4196288697_RuntimeMethod_var);
		__this->set_titleLabel_16(L_5);
		Transform_t3600365921 * L_6 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t3600365921 * L_7 = Transform_Find_m1729760951(L_6, _stringLiteral721440197, /*hidden argument*/NULL);
		NullCheck(L_7);
		Dropdown_t2274391225 * L_8 = Component_GetComponent_TisDropdown_t2274391225_m782479209(L_7, /*hidden argument*/Component_GetComponent_TisDropdown_t2274391225_m782479209_RuntimeMethod_var);
		__this->set_typeDropdown_17(L_8);
		Transform_t3600365921 * L_9 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t3600365921 * L_10 = Transform_Find_m1729760951(L_9, _stringLiteral672945067, /*hidden argument*/NULL);
		NullCheck(L_10);
		Button_t4055032469 * L_11 = Component_GetComponent_TisButton_t4055032469_m1381873113(L_10, /*hidden argument*/Component_GetComponent_TisButton_t4055032469_m1381873113_RuntimeMethod_var);
		__this->set_addRecordButton_22(L_11);
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t3600365921 * L_13 = Transform_Find_m1729760951(L_12, _stringLiteral1346367280, /*hidden argument*/NULL);
		__this->set_textRecordTransform_18(L_13);
		Transform_t3600365921 * L_14 = __this->get_textRecordTransform_18();
		NullCheck(L_14);
		Transform_t3600365921 * L_15 = Transform_Find_m1729760951(L_14, _stringLiteral1281245125, /*hidden argument*/NULL);
		NullCheck(L_15);
		InputField_t3762917431 * L_16 = Component_GetComponent_TisInputField_t3762917431_m3342128916(L_15, /*hidden argument*/Component_GetComponent_TisInputField_t3762917431_m3342128916_RuntimeMethod_var);
		__this->set_textInput_19(L_16);
		Transform_t3600365921 * L_17 = __this->get_textRecordTransform_18();
		NullCheck(L_17);
		Transform_t3600365921 * L_18 = Transform_Find_m1729760951(L_17, _stringLiteral2308089002, /*hidden argument*/NULL);
		NullCheck(L_18);
		InputField_t3762917431 * L_19 = Component_GetComponent_TisInputField_t3762917431_m3342128916(L_18, /*hidden argument*/Component_GetComponent_TisInputField_t3762917431_m3342128916_RuntimeMethod_var);
		__this->set_languageCodeInput_20(L_19);
		Transform_t3600365921 * L_20 = __this->get_textRecordTransform_18();
		NullCheck(L_20);
		Transform_t3600365921 * L_21 = Transform_Find_m1729760951(L_20, _stringLiteral3723456887, /*hidden argument*/NULL);
		NullCheck(L_21);
		Dropdown_t2274391225 * L_22 = Component_GetComponent_TisDropdown_t2274391225_m782479209(L_21, /*hidden argument*/Component_GetComponent_TisDropdown_t2274391225_m782479209_RuntimeMethod_var);
		__this->set_textEncodingDropdown_21(L_22);
		Transform_t3600365921 * L_23 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_t3600365921 * L_24 = Transform_Find_m1729760951(L_23, _stringLiteral1553682333, /*hidden argument*/NULL);
		__this->set_uriRecordTransform_23(L_24);
		Transform_t3600365921 * L_25 = __this->get_uriRecordTransform_23();
		NullCheck(L_25);
		Transform_t3600365921 * L_26 = Transform_Find_m1729760951(L_25, _stringLiteral3042993421, /*hidden argument*/NULL);
		NullCheck(L_26);
		InputField_t3762917431 * L_27 = Component_GetComponent_TisInputField_t3762917431_m3342128916(L_26, /*hidden argument*/Component_GetComponent_TisInputField_t3762917431_m3342128916_RuntimeMethod_var);
		__this->set_uriInput_24(L_27);
		Transform_t3600365921 * L_28 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_t3600365921 * L_29 = Transform_Find_m1729760951(L_28, _stringLiteral2803871764, /*hidden argument*/NULL);
		__this->set_mimeMediaRecordTransform_25(L_29);
		Transform_t3600365921 * L_30 = __this->get_mimeMediaRecordTransform_25();
		NullCheck(L_30);
		Transform_t3600365921 * L_31 = Transform_Find_m1729760951(L_30, _stringLiteral2803516718, /*hidden argument*/NULL);
		NullCheck(L_31);
		Dropdown_t2274391225 * L_32 = Component_GetComponent_TisDropdown_t2274391225_m782479209(L_31, /*hidden argument*/Component_GetComponent_TisDropdown_t2274391225_m782479209_RuntimeMethod_var);
		__this->set_iconDropdown_26(L_32);
		Transform_t3600365921 * L_33 = __this->get_mimeMediaRecordTransform_25();
		NullCheck(L_33);
		Transform_t3600365921 * L_34 = Transform_Find_m1729760951(L_33, _stringLiteral3089059158, /*hidden argument*/NULL);
		NullCheck(L_34);
		Text_t1901882714 * L_35 = Component_GetComponent_TisText_t1901882714_m4196288697(L_34, /*hidden argument*/Component_GetComponent_TisText_t1901882714_m4196288697_RuntimeMethod_var);
		__this->set_mimeTypeLabel_27(L_35);
		Transform_t3600365921 * L_36 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_t3600365921 * L_37 = Transform_Find_m1729760951(L_36, _stringLiteral771524720, /*hidden argument*/NULL);
		__this->set_externalTypeRecordTransform_28(L_37);
		Transform_t3600365921 * L_38 = __this->get_externalTypeRecordTransform_28();
		NullCheck(L_38);
		Transform_t3600365921 * L_39 = Transform_Find_m1729760951(L_38, _stringLiteral2350814669, /*hidden argument*/NULL);
		NullCheck(L_39);
		InputField_t3762917431 * L_40 = Component_GetComponent_TisInputField_t3762917431_m3342128916(L_39, /*hidden argument*/Component_GetComponent_TisInputField_t3762917431_m3342128916_RuntimeMethod_var);
		__this->set_domainNameInput_29(L_40);
		Transform_t3600365921 * L_41 = __this->get_externalTypeRecordTransform_28();
		NullCheck(L_41);
		Transform_t3600365921 * L_42 = Transform_Find_m1729760951(L_41, _stringLiteral2129789834, /*hidden argument*/NULL);
		NullCheck(L_42);
		InputField_t3762917431 * L_43 = Component_GetComponent_TisInputField_t3762917431_m3342128916(L_42, /*hidden argument*/Component_GetComponent_TisInputField_t3762917431_m3342128916_RuntimeMethod_var);
		__this->set_domainTypeInput_30(L_43);
		Transform_t3600365921 * L_44 = __this->get_externalTypeRecordTransform_28();
		NullCheck(L_44);
		Transform_t3600365921 * L_45 = Transform_Find_m1729760951(L_44, _stringLiteral2059862158, /*hidden argument*/NULL);
		NullCheck(L_45);
		InputField_t3762917431 * L_46 = Component_GetComponent_TisInputField_t3762917431_m3342128916(L_45, /*hidden argument*/Component_GetComponent_TisInputField_t3762917431_m3342128916_RuntimeMethod_var);
		__this->set_domainDataInput_31(L_46);
		Transform_t3600365921 * L_47 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		Transform_t3600365921 * L_48 = Transform_Find_m1729760951(L_47, _stringLiteral1005157632, /*hidden argument*/NULL);
		NullCheck(L_48);
		Button_t4055032469 * L_49 = Component_GetComponent_TisButton_t4055032469_m1381873113(L_48, /*hidden argument*/Component_GetComponent_TisButton_t4055032469_m1381873113_RuntimeMethod_var);
		__this->set_clearButton_32(L_49);
		Transform_t3600365921 * L_50 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_50);
		Transform_t3600365921 * L_51 = Transform_Find_m1729760951(L_50, _stringLiteral2863031003, /*hidden argument*/NULL);
		NullCheck(L_51);
		Button_t4055032469 * L_52 = Component_GetComponent_TisButton_t4055032469_m1381873113(L_51, /*hidden argument*/Component_GetComponent_TisButton_t4055032469_m1381873113_RuntimeMethod_var);
		__this->set_writeButton_33(L_52);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::Start()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_Start_m109890395 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenView_Start_m109890395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t1901882714 * L_0 = __this->get_titleLabel_16();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, _stringLiteral570303280);
		Text_t1901882714 * L_1 = __this->get_titleLabel_16();
		Color_t2555686324  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Color__ctor_m286683560((&L_2), (0.8f), (0.1f), (0.2f), /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1, L_2);
		Button_t4055032469 * L_3 = __this->get_addRecordButton_22();
		NullCheck(L_3);
		Selectable_set_interactable_m3105888815(L_3, (bool)0, /*hidden argument*/NULL);
		Button_t4055032469 * L_4 = __this->get_clearButton_32();
		NullCheck(L_4);
		Selectable_set_interactable_m3105888815(L_4, (bool)0, /*hidden argument*/NULL);
		Button_t4055032469 * L_5 = __this->get_writeButton_33();
		NullCheck(L_5);
		Selectable_set_interactable_m3105888815(L_5, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::UpdateTypeDropdownOptions(System.String[])
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_UpdateTypeDropdownOptions_m2278996124 (WriteScreenView_t2350253495 * __this, StringU5BU5D_t1281789340* ___options0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenView_UpdateTypeDropdownOptions_m2278996124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t447389798 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		Dropdown_t2274391225 * L_0 = __this->get_typeDropdown_17();
		NullCheck(L_0);
		Dropdown_ClearOptions_m4085591601(L_0, /*hidden argument*/NULL);
		List_1_t447389798 * L_1 = (List_1_t447389798 *)il2cpp_codegen_object_new(List_1_t447389798_il2cpp_TypeInfo_var);
		List_1__ctor_m1438953653(L_1, /*hidden argument*/List_1__ctor_m1438953653_RuntimeMethod_var);
		V_0 = L_1;
		StringU5BU5D_t1281789340* L_2 = ___options0;
		NullCheck(L_2);
		V_1 = (((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length))));
		V_2 = 0;
		goto IL_002e;
	}

IL_001c:
	{
		List_1_t447389798 * L_3 = V_0;
		StringU5BU5D_t1281789340* L_4 = ___options0;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		String_t* L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		OptionData_t3270282352 * L_8 = (OptionData_t3270282352 *)il2cpp_codegen_object_new(OptionData_t3270282352_il2cpp_TypeInfo_var);
		OptionData__ctor_m2696491456(L_8, L_7, /*hidden argument*/NULL);
		NullCheck(L_3);
		List_1_Add_m3700962105(L_3, L_8, /*hidden argument*/List_1_Add_m3700962105_RuntimeMethod_var);
		int32_t L_9 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_2;
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_001c;
		}
	}
	{
		Dropdown_t2274391225 * L_12 = __this->get_typeDropdown_17();
		List_1_t447389798 * L_13 = V_0;
		NullCheck(L_12);
		Dropdown_AddOptions_m3733885929(L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::UpdateTextEncodingDropdownOptions(System.String[])
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_UpdateTextEncodingDropdownOptions_m2236664622 (WriteScreenView_t2350253495 * __this, StringU5BU5D_t1281789340* ___options0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenView_UpdateTextEncodingDropdownOptions_m2236664622_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t447389798 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		Dropdown_t2274391225 * L_0 = __this->get_textEncodingDropdown_21();
		NullCheck(L_0);
		Dropdown_ClearOptions_m4085591601(L_0, /*hidden argument*/NULL);
		List_1_t447389798 * L_1 = (List_1_t447389798 *)il2cpp_codegen_object_new(List_1_t447389798_il2cpp_TypeInfo_var);
		List_1__ctor_m1438953653(L_1, /*hidden argument*/List_1__ctor_m1438953653_RuntimeMethod_var);
		V_0 = L_1;
		StringU5BU5D_t1281789340* L_2 = ___options0;
		NullCheck(L_2);
		V_1 = (((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length))));
		V_2 = 0;
		goto IL_002e;
	}

IL_001c:
	{
		List_1_t447389798 * L_3 = V_0;
		StringU5BU5D_t1281789340* L_4 = ___options0;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		String_t* L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		OptionData_t3270282352 * L_8 = (OptionData_t3270282352 *)il2cpp_codegen_object_new(OptionData_t3270282352_il2cpp_TypeInfo_var);
		OptionData__ctor_m2696491456(L_8, L_7, /*hidden argument*/NULL);
		NullCheck(L_3);
		List_1_Add_m3700962105(L_3, L_8, /*hidden argument*/List_1_Add_m3700962105_RuntimeMethod_var);
		int32_t L_9 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_2;
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_001c;
		}
	}
	{
		Dropdown_t2274391225 * L_12 = __this->get_textEncodingDropdown_21();
		List_1_t447389798 * L_13 = V_0;
		NullCheck(L_12);
		Dropdown_AddOptions_m3733885929(L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::UpdateIconDropdownOptions(System.String[])
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_UpdateIconDropdownOptions_m304314056 (WriteScreenView_t2350253495 * __this, StringU5BU5D_t1281789340* ___options0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenView_UpdateIconDropdownOptions_m304314056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t447389798 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		Dropdown_t2274391225 * L_0 = __this->get_iconDropdown_26();
		NullCheck(L_0);
		Dropdown_ClearOptions_m4085591601(L_0, /*hidden argument*/NULL);
		List_1_t447389798 * L_1 = (List_1_t447389798 *)il2cpp_codegen_object_new(List_1_t447389798_il2cpp_TypeInfo_var);
		List_1__ctor_m1438953653(L_1, /*hidden argument*/List_1__ctor_m1438953653_RuntimeMethod_var);
		V_0 = L_1;
		StringU5BU5D_t1281789340* L_2 = ___options0;
		NullCheck(L_2);
		V_1 = (((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length))));
		V_2 = 0;
		goto IL_002e;
	}

IL_001c:
	{
		List_1_t447389798 * L_3 = V_0;
		StringU5BU5D_t1281789340* L_4 = ___options0;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		String_t* L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		OptionData_t3270282352 * L_8 = (OptionData_t3270282352 *)il2cpp_codegen_object_new(OptionData_t3270282352_il2cpp_TypeInfo_var);
		OptionData__ctor_m2696491456(L_8, L_7, /*hidden argument*/NULL);
		NullCheck(L_3);
		List_1_Add_m3700962105(L_3, L_8, /*hidden argument*/List_1_Add_m3700962105_RuntimeMethod_var);
		int32_t L_9 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_2;
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_001c;
		}
	}
	{
		Dropdown_t2274391225 * L_12 = __this->get_iconDropdown_26();
		List_1_t447389798 * L_13 = V_0;
		NullCheck(L_12);
		Dropdown_AddOptions_m3733885929(L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::SwitchToTextRecordInput()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_SwitchToTextRecordInput_m801913819 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = __this->get_textRecordTransform_18();
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)1, /*hidden argument*/NULL);
		Transform_t3600365921 * L_2 = __this->get_uriRecordTransform_23();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_4 = __this->get_mimeMediaRecordTransform_25();
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_m796801857(L_5, (bool)0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_6 = __this->get_externalTypeRecordTransform_28();
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_SetActive_m796801857(L_7, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::SwitchToUriInput()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_SwitchToUriInput_m1218801993 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = __this->get_textRecordTransform_18();
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_2 = __this->get_uriRecordTransform_23();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)1, /*hidden argument*/NULL);
		Transform_t3600365921 * L_4 = __this->get_mimeMediaRecordTransform_25();
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_m796801857(L_5, (bool)0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_6 = __this->get_externalTypeRecordTransform_28();
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_SetActive_m796801857(L_7, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::SwitchToMimeMediaInput()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_SwitchToMimeMediaInput_m1134766923 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = __this->get_textRecordTransform_18();
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_2 = __this->get_uriRecordTransform_23();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_4 = __this->get_mimeMediaRecordTransform_25();
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_m796801857(L_5, (bool)1, /*hidden argument*/NULL);
		Transform_t3600365921 * L_6 = __this->get_externalTypeRecordTransform_28();
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_SetActive_m796801857(L_7, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::SwitchToExternalTypeInput()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_SwitchToExternalTypeInput_m1529510719 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = __this->get_textRecordTransform_18();
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_2 = __this->get_uriRecordTransform_23();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_4 = __this->get_mimeMediaRecordTransform_25();
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_m796801857(L_5, (bool)0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_6 = __this->get_externalTypeRecordTransform_28();
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_SetActive_m796801857(L_7, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::ClearTextInput()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_ClearTextInput_m1742933371 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenView_ClearTextInput_m1742933371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InputField_t3762917431 * L_0 = __this->get_textInput_19();
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		NullCheck(L_0);
		InputField_set_text_m1877260015(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::ResetLanguageCodeInput()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_ResetLanguageCodeInput_m1307137580 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenView_ResetLanguageCodeInput_m1307137580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InputField_t3762917431 * L_0 = __this->get_textInput_19();
		NullCheck(L_0);
		InputField_set_text_m1877260015(L_0, _stringLiteral3454842811, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::ClearUriInput()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_ClearUriInput_m2839071453 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenView_ClearUriInput_m2839071453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InputField_t3762917431 * L_0 = __this->get_uriInput_24();
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		NullCheck(L_0);
		InputField_set_text_m1877260015(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::UpdateNDEFMessage(DigitsNFCToolkit.NDEFMessage)
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_UpdateNDEFMessage_m4183170585 (WriteScreenView_t2350253495 * __this, NDEFMessage_t279637043 * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenView_UpdateNDEFMessage_m4183170585_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	List_1_t4162620298 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	NDEFRecord_t2690545556 * V_4 = NULL;
	RecordItem_t1075151419 * V_5 = NULL;
	int32_t V_6 = 0;
	TextRecord_t2313697623 * V_7 = NULL;
	UriRecord_t2230063309 * V_8 = NULL;
	MimeMediaRecord_t736820488 * V_9 = NULL;
	ExternalTypeRecord_t4087466745 * V_10 = NULL;
	int32_t V_11 = 0;
	String_t* V_12 = NULL;
	SmartPosterRecord_t1640848801 * V_13 = NULL;
	int32_t V_14 = 0;
	Rect_t2360479859  V_15;
	memset(&V_15, 0, sizeof(V_15));
	{
		WriteScreenView_CleanupRecordItems_m619684253(__this, /*hidden argument*/NULL);
		V_0 = (0.0f);
		NDEFMessage_t279637043 * L_0 = ___message0;
		NullCheck(L_0);
		List_1_t4162620298 * L_1 = NDEFMessage_get_Records_m1930208688(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		List_1_t4162620298 * L_2 = V_1;
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m2774962351(L_2, /*hidden argument*/List_1_get_Count_m2774962351_RuntimeMethod_var);
		V_2 = L_3;
		V_3 = 0;
		goto IL_02bf;
	}

IL_0021:
	{
		List_1_t4162620298 * L_4 = V_1;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		NDEFRecord_t2690545556 * L_6 = List_1_get_Item_m1387264933(L_4, L_5, /*hidden argument*/List_1_get_Item_m1387264933_RuntimeMethod_var);
		V_4 = L_6;
		V_5 = (RecordItem_t1075151419 *)NULL;
		NDEFRecord_t2690545556 * L_7 = V_4;
		NullCheck(L_7);
		int32_t L_8 = NDEFRecord_get_Type_m1191149495(L_7, /*hidden argument*/NULL);
		V_6 = L_8;
		int32_t L_9 = V_6;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)2)))
		{
			case 0:
			{
				goto IL_0162;
			}
			case 1:
			{
				goto IL_0115;
			}
			case 2:
			{
				goto IL_01df;
			}
			case 3:
			{
				goto IL_005c;
			}
			case 4:
			{
				goto IL_0284;
			}
			case 5:
			{
				goto IL_00bb;
			}
		}
	}
	{
		goto IL_0284;
	}

IL_005c:
	{
		RecordItem_t1075151419 * L_10 = __this->get_textRecordItemPrefab_10();
		RecordItem_t1075151419 * L_11 = WriteScreenView_CreateRecordItem_m4278698087(__this, L_10, /*hidden argument*/NULL);
		V_5 = L_11;
		NDEFRecord_t2690545556 * L_12 = V_4;
		V_7 = ((TextRecord_t2313697623 *)CastclassClass((RuntimeObject*)L_12, TextRecord_t2313697623_il2cpp_TypeInfo_var));
		RecordItem_t1075151419 * L_13 = V_5;
		ObjectU5BU5D_t2843939325* L_14 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t2843939325* L_15 = L_14;
		int32_t L_16 = ((int32_t)5);
		RuntimeObject * L_17 = Box(NDEFRecordType_t4294622310_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_17);
		ObjectU5BU5D_t2843939325* L_18 = L_15;
		TextRecord_t2313697623 * L_19 = V_7;
		NullCheck(L_19);
		String_t* L_20 = L_19->get_text_1();
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_20);
		ObjectU5BU5D_t2843939325* L_21 = L_18;
		TextRecord_t2313697623 * L_22 = V_7;
		NullCheck(L_22);
		String_t* L_23 = L_22->get_languageCode_2();
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_23);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_23);
		ObjectU5BU5D_t2843939325* L_24 = L_21;
		TextRecord_t2313697623 * L_25 = V_7;
		NullCheck(L_25);
		int32_t L_26 = L_25->get_textEncoding_3();
		int32_t L_27 = L_26;
		RuntimeObject * L_28 = Box(TextEncoding_t3210513626_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_28);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_28);
		String_t* L_29 = String_Format_m630303134(NULL /*static, unused*/, _stringLiteral3037589779, L_24, /*hidden argument*/NULL);
		NullCheck(L_13);
		RecordItem_UpdateLabel_m1853417565(L_13, L_29, /*hidden argument*/NULL);
		goto IL_0284;
	}

IL_00bb:
	{
		RecordItem_t1075151419 * L_30 = __this->get_uriRecordItemPrefab_11();
		RecordItem_t1075151419 * L_31 = WriteScreenView_CreateRecordItem_m4278698087(__this, L_30, /*hidden argument*/NULL);
		V_5 = L_31;
		NDEFRecord_t2690545556 * L_32 = V_4;
		V_8 = ((UriRecord_t2230063309 *)CastclassClass((RuntimeObject*)L_32, UriRecord_t2230063309_il2cpp_TypeInfo_var));
		RecordItem_t1075151419 * L_33 = V_5;
		ObjectU5BU5D_t2843939325* L_34 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t2843939325* L_35 = L_34;
		int32_t L_36 = ((int32_t)7);
		RuntimeObject * L_37 = Box(NDEFRecordType_t4294622310_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_37);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_37);
		ObjectU5BU5D_t2843939325* L_38 = L_35;
		UriRecord_t2230063309 * L_39 = V_8;
		NullCheck(L_39);
		String_t* L_40 = L_39->get_fullUri_1();
		NullCheck(L_38);
		ArrayElementTypeCheck (L_38, L_40);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_40);
		ObjectU5BU5D_t2843939325* L_41 = L_38;
		UriRecord_t2230063309 * L_42 = V_8;
		NullCheck(L_42);
		String_t* L_43 = L_42->get_uri_2();
		NullCheck(L_41);
		ArrayElementTypeCheck (L_41, L_43);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_43);
		ObjectU5BU5D_t2843939325* L_44 = L_41;
		UriRecord_t2230063309 * L_45 = V_8;
		NullCheck(L_45);
		String_t* L_46 = L_45->get_protocol_3();
		NullCheck(L_44);
		ArrayElementTypeCheck (L_44, L_46);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_46);
		String_t* L_47 = String_Format_m630303134(NULL /*static, unused*/, _stringLiteral2378602798, L_44, /*hidden argument*/NULL);
		NullCheck(L_33);
		RecordItem_UpdateLabel_m1853417565(L_33, L_47, /*hidden argument*/NULL);
		goto IL_0284;
	}

IL_0115:
	{
		ImageRecordItem_t2104316047 * L_48 = __this->get_mimeMediaRecordItemPrefab_12();
		RecordItem_t1075151419 * L_49 = WriteScreenView_CreateRecordItem_m4278698087(__this, L_48, /*hidden argument*/NULL);
		V_5 = L_49;
		NDEFRecord_t2690545556 * L_50 = V_4;
		V_9 = ((MimeMediaRecord_t736820488 *)CastclassClass((RuntimeObject*)L_50, MimeMediaRecord_t736820488_il2cpp_TypeInfo_var));
		RecordItem_t1075151419 * L_51 = V_5;
		int32_t L_52 = ((int32_t)3);
		RuntimeObject * L_53 = Box(NDEFRecordType_t4294622310_il2cpp_TypeInfo_var, &L_52);
		MimeMediaRecord_t736820488 * L_54 = V_9;
		NullCheck(L_54);
		String_t* L_55 = L_54->get_mimeType_1();
		String_t* L_56 = String_Format_m2556382932(NULL /*static, unused*/, _stringLiteral1274537113, L_53, L_55, /*hidden argument*/NULL);
		NullCheck(L_51);
		RecordItem_UpdateLabel_m1853417565(L_51, L_56, /*hidden argument*/NULL);
		RecordItem_t1075151419 * L_57 = V_5;
		MimeMediaRecord_t736820488 * L_58 = V_9;
		NullCheck(L_58);
		ByteU5BU5D_t4116647657* L_59 = L_58->get_mimeData_2();
		NullCheck(((ImageRecordItem_t2104316047 *)CastclassClass((RuntimeObject*)L_57, ImageRecordItem_t2104316047_il2cpp_TypeInfo_var)));
		ImageRecordItem_LoadImage_m2052998847(((ImageRecordItem_t2104316047 *)CastclassClass((RuntimeObject*)L_57, ImageRecordItem_t2104316047_il2cpp_TypeInfo_var)), L_59, /*hidden argument*/NULL);
		goto IL_0284;
	}

IL_0162:
	{
		RecordItem_t1075151419 * L_60 = __this->get_externalTypeRecordItemPrefab_13();
		RecordItem_t1075151419 * L_61 = WriteScreenView_CreateRecordItem_m4278698087(__this, L_60, /*hidden argument*/NULL);
		V_5 = L_61;
		NDEFRecord_t2690545556 * L_62 = V_4;
		V_10 = ((ExternalTypeRecord_t4087466745 *)CastclassClass((RuntimeObject*)L_62, ExternalTypeRecord_t4087466745_il2cpp_TypeInfo_var));
		ExternalTypeRecord_t4087466745 * L_63 = V_10;
		NullCheck(L_63);
		ByteU5BU5D_t4116647657* L_64 = L_63->get_domainData_3();
		NullCheck(L_64);
		V_11 = (((int32_t)((int32_t)(((RuntimeArray *)L_64)->max_length))));
		Encoding_t1523322056 * L_65 = Encoding_get_UTF8_m1008486739(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExternalTypeRecord_t4087466745 * L_66 = V_10;
		NullCheck(L_66);
		ByteU5BU5D_t4116647657* L_67 = L_66->get_domainData_3();
		NullCheck(L_65);
		String_t* L_68 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t4116647657* >::Invoke(32 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_65, L_67);
		V_12 = L_68;
		RecordItem_t1075151419 * L_69 = V_5;
		ObjectU5BU5D_t2843939325* L_70 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t2843939325* L_71 = L_70;
		int32_t L_72 = ((int32_t)2);
		RuntimeObject * L_73 = Box(NDEFRecordType_t4294622310_il2cpp_TypeInfo_var, &L_72);
		NullCheck(L_71);
		ArrayElementTypeCheck (L_71, L_73);
		(L_71)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_73);
		ObjectU5BU5D_t2843939325* L_74 = L_71;
		ExternalTypeRecord_t4087466745 * L_75 = V_10;
		NullCheck(L_75);
		String_t* L_76 = L_75->get_domainName_1();
		NullCheck(L_74);
		ArrayElementTypeCheck (L_74, L_76);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_76);
		ObjectU5BU5D_t2843939325* L_77 = L_74;
		ExternalTypeRecord_t4087466745 * L_78 = V_10;
		NullCheck(L_78);
		String_t* L_79 = L_78->get_domainType_2();
		NullCheck(L_77);
		ArrayElementTypeCheck (L_77, L_79);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_79);
		ObjectU5BU5D_t2843939325* L_80 = L_77;
		int32_t L_81 = V_11;
		int32_t L_82 = L_81;
		RuntimeObject * L_83 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_82);
		NullCheck(L_80);
		ArrayElementTypeCheck (L_80, L_83);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_83);
		ObjectU5BU5D_t2843939325* L_84 = L_80;
		String_t* L_85 = V_12;
		NullCheck(L_84);
		ArrayElementTypeCheck (L_84, L_85);
		(L_84)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_85);
		String_t* L_86 = String_Format_m630303134(NULL /*static, unused*/, _stringLiteral3680104728, L_84, /*hidden argument*/NULL);
		NullCheck(L_69);
		RecordItem_UpdateLabel_m1853417565(L_69, L_86, /*hidden argument*/NULL);
		goto IL_0284;
	}

IL_01df:
	{
		RecordItem_t1075151419 * L_87 = __this->get_smartPosterRecordItemPrefab_14();
		RecordItem_t1075151419 * L_88 = WriteScreenView_CreateRecordItem_m4278698087(__this, L_87, /*hidden argument*/NULL);
		V_5 = L_88;
		NDEFRecord_t2690545556 * L_89 = V_4;
		V_13 = ((SmartPosterRecord_t1640848801 *)CastclassClass((RuntimeObject*)L_89, SmartPosterRecord_t1640848801_il2cpp_TypeInfo_var));
		SmartPosterRecord_t1640848801 * L_90 = V_13;
		NullCheck(L_90);
		List_1_t3785772365 * L_91 = L_90->get_titleRecords_2();
		NullCheck(L_91);
		int32_t L_92 = List_1_get_Count_m3842818989(L_91, /*hidden argument*/List_1_get_Count_m3842818989_RuntimeMethod_var);
		SmartPosterRecord_t1640848801 * L_93 = V_13;
		NullCheck(L_93);
		List_1_t2208895230 * L_94 = L_93->get_iconRecords_3();
		NullCheck(L_94);
		int32_t L_95 = List_1_get_Count_m1758644702(L_94, /*hidden argument*/List_1_get_Count_m1758644702_RuntimeMethod_var);
		SmartPosterRecord_t1640848801 * L_96 = V_13;
		NullCheck(L_96);
		List_1_t4162620298 * L_97 = L_96->get_extraRecords_4();
		NullCheck(L_97);
		int32_t L_98 = List_1_get_Count_m2774962351(L_97, /*hidden argument*/List_1_get_Count_m2774962351_RuntimeMethod_var);
		V_14 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_92, (int32_t)L_95)), (int32_t)L_98));
		RecordItem_t1075151419 * L_99 = V_5;
		ObjectU5BU5D_t2843939325* L_100 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)6);
		ObjectU5BU5D_t2843939325* L_101 = L_100;
		int32_t L_102 = ((int32_t)4);
		RuntimeObject * L_103 = Box(NDEFRecordType_t4294622310_il2cpp_TypeInfo_var, &L_102);
		NullCheck(L_101);
		ArrayElementTypeCheck (L_101, L_103);
		(L_101)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_103);
		ObjectU5BU5D_t2843939325* L_104 = L_101;
		SmartPosterRecord_t1640848801 * L_105 = V_13;
		NullCheck(L_105);
		UriRecord_t2230063309 * L_106 = L_105->get_uriRecord_1();
		NullCheck(L_106);
		String_t* L_107 = L_106->get_fullUri_1();
		NullCheck(L_104);
		ArrayElementTypeCheck (L_104, L_107);
		(L_104)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_107);
		ObjectU5BU5D_t2843939325* L_108 = L_104;
		SmartPosterRecord_t1640848801 * L_109 = V_13;
		NullCheck(L_109);
		int32_t L_110 = L_109->get_action_5();
		int32_t L_111 = L_110;
		RuntimeObject * L_112 = Box(RecommendedAction_t1182550772_il2cpp_TypeInfo_var, &L_111);
		NullCheck(L_108);
		ArrayElementTypeCheck (L_108, L_112);
		(L_108)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_112);
		ObjectU5BU5D_t2843939325* L_113 = L_108;
		SmartPosterRecord_t1640848801 * L_114 = V_13;
		NullCheck(L_114);
		int32_t L_115 = L_114->get_size_6();
		int32_t L_116 = L_115;
		RuntimeObject * L_117 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_116);
		NullCheck(L_113);
		ArrayElementTypeCheck (L_113, L_117);
		(L_113)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_117);
		ObjectU5BU5D_t2843939325* L_118 = L_113;
		SmartPosterRecord_t1640848801 * L_119 = V_13;
		NullCheck(L_119);
		String_t* L_120 = L_119->get_mimeType_7();
		NullCheck(L_118);
		ArrayElementTypeCheck (L_118, L_120);
		(L_118)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_120);
		ObjectU5BU5D_t2843939325* L_121 = L_118;
		int32_t L_122 = V_14;
		int32_t L_123 = L_122;
		RuntimeObject * L_124 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_123);
		NullCheck(L_121);
		ArrayElementTypeCheck (L_121, L_124);
		(L_121)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_124);
		String_t* L_125 = String_Format_m630303134(NULL /*static, unused*/, _stringLiteral3439933068, L_121, /*hidden argument*/NULL);
		NullCheck(L_99);
		RecordItem_UpdateLabel_m1853417565(L_99, L_125, /*hidden argument*/NULL);
		goto IL_0284;
	}

IL_0284:
	{
		RecordItem_t1075151419 * L_126 = V_5;
		NullCheck(L_126);
		RectTransform_t3704657025 * L_127 = RecordItem_get_RectTransform_m3728143926(L_126, /*hidden argument*/NULL);
		float L_128 = V_0;
		Vector2_t2156229523  L_129;
		memset(&L_129, 0, sizeof(L_129));
		Vector2__ctor_m3970636864((&L_129), (0.0f), L_128, /*hidden argument*/NULL);
		NullCheck(L_127);
		RectTransform_set_anchoredPosition_m4126691837(L_127, L_129, /*hidden argument*/NULL);
		float L_130 = V_0;
		RecordItem_t1075151419 * L_131 = V_5;
		NullCheck(L_131);
		RectTransform_t3704657025 * L_132 = RecordItem_get_RectTransform_m3728143926(L_131, /*hidden argument*/NULL);
		NullCheck(L_132);
		Rect_t2360479859  L_133 = RectTransform_get_rect_m574169965(L_132, /*hidden argument*/NULL);
		V_15 = L_133;
		float L_134 = Rect_get_height_m1358425599((Rect_t2360479859 *)(&V_15), /*hidden argument*/NULL);
		V_0 = ((float)il2cpp_codegen_subtract((float)L_130, (float)L_134));
		float L_135 = V_0;
		V_0 = ((float)il2cpp_codegen_subtract((float)L_135, (float)(8.0f)));
		int32_t L_136 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_136, (int32_t)1));
	}

IL_02bf:
	{
		int32_t L_137 = V_3;
		int32_t L_138 = V_2;
		if ((((int32_t)L_137) < ((int32_t)L_138)))
		{
			goto IL_0021;
		}
	}
	{
		ScrollRect_t4137855814 * L_139 = __this->get_ndefMessageScrollRect_15();
		NullCheck(L_139);
		RectTransform_t3704657025 * L_140 = ScrollRect_get_content_m2477524320(L_139, /*hidden argument*/NULL);
		float L_141 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_142 = fabsf(L_141);
		Vector2_t2156229523  L_143;
		memset(&L_143, 0, sizeof(L_143));
		Vector2__ctor_m3970636864((&L_143), (0.0f), L_142, /*hidden argument*/NULL);
		NullCheck(L_140);
		RectTransform_set_sizeDelta_m3462269772(L_140, L_143, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DigitsNFCToolkit.Samples.WriteScreenView::CleanupRecordItems()
extern "C" IL2CPP_METHOD_ATTR void WriteScreenView_CleanupRecordItems_m619684253 (WriteScreenView_t2350253495 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenView_CleanupRecordItems_m619684253_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3704657025 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		ScrollRect_t4137855814 * L_0 = __this->get_ndefMessageScrollRect_15();
		NullCheck(L_0);
		RectTransform_t3704657025 * L_1 = ScrollRect_get_content_m2477524320(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RectTransform_t3704657025 * L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = Transform_get_childCount_m3145433196(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		V_2 = 0;
		goto IL_002f;
	}

IL_001a:
	{
		RectTransform_t3704657025 * L_4 = V_0;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		Transform_t3600365921 * L_6 = Transform_GetChild_m1092972975(L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_002f:
	{
		int32_t L_9 = V_2;
		int32_t L_10 = V_1;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_001a;
		}
	}
	{
		return;
	}
}
// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.WriteScreenView::CreateRecordItem(DigitsNFCToolkit.Samples.RecordItem)
extern "C" IL2CPP_METHOD_ATTR RecordItem_t1075151419 * WriteScreenView_CreateRecordItem_m4278698087 (WriteScreenView_t2350253495 * __this, RecordItem_t1075151419 * ___prefab0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteScreenView_CreateRecordItem_m4278698087_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RecordItem_t1075151419 * V_0 = NULL;
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RecordItem_t1075151419 * L_0 = ___prefab0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		RecordItem_t1075151419 * L_1 = Object_Instantiate_TisRecordItem_t1075151419_m880386264(NULL /*static, unused*/, L_0, /*hidden argument*/Object_Instantiate_TisRecordItem_t1075151419_m880386264_RuntimeMethod_var);
		V_0 = L_1;
		RecordItem_t1075151419 * L_2 = V_0;
		NullCheck(L_2);
		RectTransform_t3704657025 * L_3 = RecordItem_get_RectTransform_m3728143926(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector2_t2156229523  L_4 = RectTransform_get_sizeDelta_m2183112744(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		RecordItem_t1075151419 * L_5 = V_0;
		NullCheck(L_5);
		RectTransform_t3704657025 * L_6 = RecordItem_get_RectTransform_m3728143926(L_5, /*hidden argument*/NULL);
		ScrollRect_t4137855814 * L_7 = __this->get_ndefMessageScrollRect_15();
		NullCheck(L_7);
		RectTransform_t3704657025 * L_8 = ScrollRect_get_content_m2477524320(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_SetParent_m381167889(L_6, L_8, /*hidden argument*/NULL);
		RecordItem_t1075151419 * L_9 = V_0;
		NullCheck(L_9);
		RectTransform_t3704657025 * L_10 = RecordItem_get_RectTransform_m3728143926(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_11 = Vector2_get_one_m738793577(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_localScale_m3053443106(L_10, L_12, /*hidden argument*/NULL);
		RecordItem_t1075151419 * L_13 = V_0;
		NullCheck(L_13);
		RectTransform_t3704657025 * L_14 = RecordItem_get_RectTransform_m3728143926(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_15 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_localRotation_m19445462(L_14, L_15, /*hidden argument*/NULL);
		RecordItem_t1075151419 * L_16 = V_0;
		NullCheck(L_16);
		RectTransform_t3704657025 * L_17 = RecordItem_get_RectTransform_m3728143926(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_18 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_localPosition_m4128471975(L_17, L_18, /*hidden argument*/NULL);
		RecordItem_t1075151419 * L_19 = V_0;
		NullCheck(L_19);
		RectTransform_t3704657025 * L_20 = RecordItem_get_RectTransform_m3728143926(L_19, /*hidden argument*/NULL);
		Vector2_t2156229523  L_21 = V_1;
		NullCheck(L_20);
		RectTransform_set_sizeDelta_m3462269772(L_20, L_21, /*hidden argument*/NULL);
		RecordItem_t1075151419 * L_22 = V_0;
		return L_22;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.SmartPosterRecord::.ctor(System.String,DigitsNFCToolkit.SmartPosterRecord/RecommendedAction,System.Int32,System.String)
extern "C" IL2CPP_METHOD_ATTR void SmartPosterRecord__ctor_m608988587 (SmartPosterRecord_t1640848801 * __this, String_t* ___uri0, int32_t ___action1, int32_t ___size2, String_t* ___mimeType3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmartPosterRecord__ctor_m608988587_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		((NDEFRecord_t2690545556 *)__this)->set_type_0(4);
		String_t* L_0 = ___uri0;
		UriRecord_t2230063309 * L_1 = (UriRecord_t2230063309 *)il2cpp_codegen_object_new(UriRecord_t2230063309_il2cpp_TypeInfo_var);
		UriRecord__ctor_m714533555(L_1, L_0, /*hidden argument*/NULL);
		__this->set_uriRecord_1(L_1);
		int32_t L_2 = ___action1;
		__this->set_action_5(L_2);
		int32_t L_3 = ___size2;
		__this->set_size_6(L_3);
		String_t* L_4 = ___mimeType3;
		__this->set_mimeType_7(L_4);
		List_1_t3785772365 * L_5 = (List_1_t3785772365 *)il2cpp_codegen_object_new(List_1_t3785772365_il2cpp_TypeInfo_var);
		List_1__ctor_m2409190732(L_5, /*hidden argument*/List_1__ctor_m2409190732_RuntimeMethod_var);
		__this->set_titleRecords_2(L_5);
		List_1_t2208895230 * L_6 = (List_1_t2208895230 *)il2cpp_codegen_object_new(List_1_t2208895230_il2cpp_TypeInfo_var);
		List_1__ctor_m2367744050(L_6, /*hidden argument*/List_1__ctor_m2367744050_RuntimeMethod_var);
		__this->set_iconRecords_3(L_6);
		List_1_t4162620298 * L_7 = (List_1_t4162620298 *)il2cpp_codegen_object_new(List_1_t4162620298_il2cpp_TypeInfo_var);
		List_1__ctor_m1332560254(L_7, /*hidden argument*/List_1__ctor_m1332560254_RuntimeMethod_var);
		__this->set_extraRecords_4(L_7);
		return;
	}
}
// System.Void DigitsNFCToolkit.SmartPosterRecord::.ctor(System.String,DigitsNFCToolkit.SmartPosterRecord/RecommendedAction,System.Int32,System.String,System.Collections.Generic.List`1<DigitsNFCToolkit.TextRecord>)
extern "C" IL2CPP_METHOD_ATTR void SmartPosterRecord__ctor_m3562119446 (SmartPosterRecord_t1640848801 * __this, String_t* ___uri0, int32_t ___action1, int32_t ___size2, String_t* ___mimeType3, List_1_t3785772365 * ___titleRecords4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmartPosterRecord__ctor_m3562119446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		((NDEFRecord_t2690545556 *)__this)->set_type_0(4);
		String_t* L_0 = ___uri0;
		UriRecord_t2230063309 * L_1 = (UriRecord_t2230063309 *)il2cpp_codegen_object_new(UriRecord_t2230063309_il2cpp_TypeInfo_var);
		UriRecord__ctor_m714533555(L_1, L_0, /*hidden argument*/NULL);
		__this->set_uriRecord_1(L_1);
		int32_t L_2 = ___action1;
		__this->set_action_5(L_2);
		int32_t L_3 = ___size2;
		__this->set_size_6(L_3);
		String_t* L_4 = ___mimeType3;
		__this->set_mimeType_7(L_4);
		List_1_t3785772365 * L_5 = ___titleRecords4;
		__this->set_titleRecords_2(L_5);
		List_1_t2208895230 * L_6 = (List_1_t2208895230 *)il2cpp_codegen_object_new(List_1_t2208895230_il2cpp_TypeInfo_var);
		List_1__ctor_m2367744050(L_6, /*hidden argument*/List_1__ctor_m2367744050_RuntimeMethod_var);
		__this->set_iconRecords_3(L_6);
		List_1_t4162620298 * L_7 = (List_1_t4162620298 *)il2cpp_codegen_object_new(List_1_t4162620298_il2cpp_TypeInfo_var);
		List_1__ctor_m1332560254(L_7, /*hidden argument*/List_1__ctor_m1332560254_RuntimeMethod_var);
		__this->set_extraRecords_4(L_7);
		return;
	}
}
// System.Void DigitsNFCToolkit.SmartPosterRecord::.ctor(System.String,DigitsNFCToolkit.SmartPosterRecord/RecommendedAction,System.Int32,System.String,System.Collections.Generic.List`1<DigitsNFCToolkit.TextRecord>,System.Collections.Generic.List`1<DigitsNFCToolkit.MimeMediaRecord>)
extern "C" IL2CPP_METHOD_ATTR void SmartPosterRecord__ctor_m3727071704 (SmartPosterRecord_t1640848801 * __this, String_t* ___uri0, int32_t ___action1, int32_t ___size2, String_t* ___mimeType3, List_1_t3785772365 * ___titleRecords4, List_1_t2208895230 * ___iconRecords5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmartPosterRecord__ctor_m3727071704_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		((NDEFRecord_t2690545556 *)__this)->set_type_0(4);
		String_t* L_0 = ___uri0;
		UriRecord_t2230063309 * L_1 = (UriRecord_t2230063309 *)il2cpp_codegen_object_new(UriRecord_t2230063309_il2cpp_TypeInfo_var);
		UriRecord__ctor_m714533555(L_1, L_0, /*hidden argument*/NULL);
		__this->set_uriRecord_1(L_1);
		int32_t L_2 = ___action1;
		__this->set_action_5(L_2);
		int32_t L_3 = ___size2;
		__this->set_size_6(L_3);
		String_t* L_4 = ___mimeType3;
		__this->set_mimeType_7(L_4);
		List_1_t3785772365 * L_5 = ___titleRecords4;
		__this->set_titleRecords_2(L_5);
		List_1_t2208895230 * L_6 = ___iconRecords5;
		__this->set_iconRecords_3(L_6);
		List_1_t4162620298 * L_7 = (List_1_t4162620298 *)il2cpp_codegen_object_new(List_1_t4162620298_il2cpp_TypeInfo_var);
		List_1__ctor_m1332560254(L_7, /*hidden argument*/List_1__ctor_m1332560254_RuntimeMethod_var);
		__this->set_extraRecords_4(L_7);
		return;
	}
}
// System.Void DigitsNFCToolkit.SmartPosterRecord::.ctor(System.String,DigitsNFCToolkit.SmartPosterRecord/RecommendedAction,System.Int32,System.String,System.Collections.Generic.List`1<DigitsNFCToolkit.TextRecord>,System.Collections.Generic.List`1<DigitsNFCToolkit.MimeMediaRecord>,System.Collections.Generic.List`1<DigitsNFCToolkit.NDEFRecord>)
extern "C" IL2CPP_METHOD_ATTR void SmartPosterRecord__ctor_m1413753803 (SmartPosterRecord_t1640848801 * __this, String_t* ___uri0, int32_t ___action1, int32_t ___size2, String_t* ___mimeType3, List_1_t3785772365 * ___titleRecords4, List_1_t2208895230 * ___iconRecords5, List_1_t4162620298 * ___extraRecords6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmartPosterRecord__ctor_m1413753803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		((NDEFRecord_t2690545556 *)__this)->set_type_0(4);
		String_t* L_0 = ___uri0;
		UriRecord_t2230063309 * L_1 = (UriRecord_t2230063309 *)il2cpp_codegen_object_new(UriRecord_t2230063309_il2cpp_TypeInfo_var);
		UriRecord__ctor_m714533555(L_1, L_0, /*hidden argument*/NULL);
		__this->set_uriRecord_1(L_1);
		List_1_t3785772365 * L_2 = ___titleRecords4;
		__this->set_titleRecords_2(L_2);
		List_1_t2208895230 * L_3 = ___iconRecords5;
		__this->set_iconRecords_3(L_3);
		List_1_t4162620298 * L_4 = ___extraRecords6;
		__this->set_extraRecords_4(L_4);
		int32_t L_5 = ___action1;
		__this->set_action_5(L_5);
		int32_t L_6 = ___size2;
		__this->set_size_6(L_6);
		String_t* L_7 = ___mimeType3;
		__this->set_mimeType_7(L_7);
		return;
	}
}
// System.Void DigitsNFCToolkit.SmartPosterRecord::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void SmartPosterRecord__ctor_m3134893622 (SmartPosterRecord_t1640848801 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		VirtActionInvoker1< JSONObject_t321714843 * >::Invoke(6 /* System.Void DigitsNFCToolkit.NDEFRecord::ParseJSON(DigitsNFCToolkit.JSON.JSONObject) */, __this, L_0);
		return;
	}
}
// System.Void DigitsNFCToolkit.SmartPosterRecord::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void SmartPosterRecord_ParseJSON_m2944093550 (SmartPosterRecord_t1640848801 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmartPosterRecord_ParseJSON_m2944093550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t321714843 * V_0 = NULL;
	JSONArray_t4024675823 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	JSONArray_t4024675823 * V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	JSONArray_t4024675823 * V_7 = NULL;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	JSONObject_t321714843 * V_10 = NULL;
	NDEFRecord_t2690545556 * V_11 = NULL;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	{
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		NDEFRecord_ParseJSON_m1625700843(__this, L_0, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_1 = ___jsonObject0;
		NullCheck(L_1);
		bool L_2 = JSONObject_TryGetObject_m2604600723(L_1, _stringLiteral2628391645, (JSONObject_t321714843 **)(&V_0), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		JSONObject_t321714843 * L_3 = V_0;
		UriRecord_t2230063309 * L_4 = (UriRecord_t2230063309 *)il2cpp_codegen_object_new(UriRecord_t2230063309_il2cpp_TypeInfo_var);
		UriRecord__ctor_m12655916(L_4, L_3, /*hidden argument*/NULL);
		__this->set_uriRecord_1(L_4);
	}

IL_0025:
	{
		List_1_t3785772365 * L_5 = (List_1_t3785772365 *)il2cpp_codegen_object_new(List_1_t3785772365_il2cpp_TypeInfo_var);
		List_1__ctor_m2409190732(L_5, /*hidden argument*/List_1__ctor_m2409190732_RuntimeMethod_var);
		__this->set_titleRecords_2(L_5);
		JSONObject_t321714843 * L_6 = ___jsonObject0;
		NullCheck(L_6);
		bool L_7 = JSONObject_TryGetArray_m3928523712(L_6, _stringLiteral1559920598, (JSONArray_t4024675823 **)(&V_1), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0077;
		}
	}
	{
		JSONArray_t4024675823 * L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = JSONArray_get_Length_m1921089944(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		V_3 = 0;
		goto IL_0070;
	}

IL_0050:
	{
		List_1_t3785772365 * L_10 = __this->get_titleRecords_2();
		JSONArray_t4024675823 * L_11 = V_1;
		int32_t L_12 = V_3;
		NullCheck(L_11);
		JSONValue_t4275860644 * L_13 = JSONArray_get_Item_m1052458067(L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		JSONObject_t321714843 * L_14 = JSONValue_get_Object_m3828143293(L_13, /*hidden argument*/NULL);
		TextRecord_t2313697623 * L_15 = (TextRecord_t2313697623 *)il2cpp_codegen_object_new(TextRecord_t2313697623_il2cpp_TypeInfo_var);
		TextRecord__ctor_m2059777621(L_15, L_14, /*hidden argument*/NULL);
		NullCheck(L_10);
		List_1_Add_m2718096786(L_10, L_15, /*hidden argument*/List_1_Add_m2718096786_RuntimeMethod_var);
		int32_t L_16 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
	}

IL_0070:
	{
		int32_t L_17 = V_3;
		int32_t L_18 = V_2;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0050;
		}
	}

IL_0077:
	{
		List_1_t2208895230 * L_19 = (List_1_t2208895230 *)il2cpp_codegen_object_new(List_1_t2208895230_il2cpp_TypeInfo_var);
		List_1__ctor_m2367744050(L_19, /*hidden argument*/List_1__ctor_m2367744050_RuntimeMethod_var);
		__this->set_iconRecords_3(L_19);
		JSONObject_t321714843 * L_20 = ___jsonObject0;
		NullCheck(L_20);
		bool L_21 = JSONObject_TryGetArray_m3928523712(L_20, _stringLiteral4086469251, (JSONArray_t4024675823 **)(&V_4), /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00d2;
		}
	}
	{
		JSONArray_t4024675823 * L_22 = V_4;
		NullCheck(L_22);
		int32_t L_23 = JSONArray_get_Length_m1921089944(L_22, /*hidden argument*/NULL);
		V_5 = L_23;
		V_6 = 0;
		goto IL_00c9;
	}

IL_00a5:
	{
		List_1_t2208895230 * L_24 = __this->get_iconRecords_3();
		JSONArray_t4024675823 * L_25 = V_4;
		int32_t L_26 = V_6;
		NullCheck(L_25);
		JSONValue_t4275860644 * L_27 = JSONArray_get_Item_m1052458067(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		JSONObject_t321714843 * L_28 = JSONValue_get_Object_m3828143293(L_27, /*hidden argument*/NULL);
		MimeMediaRecord_t736820488 * L_29 = (MimeMediaRecord_t736820488 *)il2cpp_codegen_object_new(MimeMediaRecord_t736820488_il2cpp_TypeInfo_var);
		MimeMediaRecord__ctor_m1733123593(L_29, L_28, /*hidden argument*/NULL);
		NullCheck(L_24);
		List_1_Add_m3311994978(L_24, L_29, /*hidden argument*/List_1_Add_m3311994978_RuntimeMethod_var);
		int32_t L_30 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
	}

IL_00c9:
	{
		int32_t L_31 = V_6;
		int32_t L_32 = V_5;
		if ((((int32_t)L_31) < ((int32_t)L_32)))
		{
			goto IL_00a5;
		}
	}

IL_00d2:
	{
		List_1_t4162620298 * L_33 = (List_1_t4162620298 *)il2cpp_codegen_object_new(List_1_t4162620298_il2cpp_TypeInfo_var);
		List_1__ctor_m1332560254(L_33, /*hidden argument*/List_1__ctor_m1332560254_RuntimeMethod_var);
		__this->set_extraRecords_4(L_33);
		JSONObject_t321714843 * L_34 = ___jsonObject0;
		NullCheck(L_34);
		bool L_35 = JSONObject_TryGetArray_m3928523712(L_34, _stringLiteral1558351666, (JSONArray_t4024675823 **)(&V_7), /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_01ec;
		}
	}
	{
		JSONArray_t4024675823 * L_36 = V_7;
		NullCheck(L_36);
		int32_t L_37 = JSONArray_get_Length_m1921089944(L_36, /*hidden argument*/NULL);
		V_8 = L_37;
		V_9 = 0;
		goto IL_01e3;
	}

IL_0100:
	{
		JSONArray_t4024675823 * L_38 = V_7;
		int32_t L_39 = V_9;
		NullCheck(L_38);
		JSONValue_t4275860644 * L_40 = JSONArray_get_Item_m1052458067(L_38, L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		JSONObject_t321714843 * L_41 = JSONValue_get_Object_m3828143293(L_40, /*hidden argument*/NULL);
		V_10 = L_41;
		V_11 = (NDEFRecord_t2690545556 *)NULL;
		JSONObject_t321714843 * L_42 = V_10;
		NullCheck(L_42);
		JSONValue_t4275860644 * L_43 = JSONObject_get_Item_m3915825514(L_42, _stringLiteral3243520166, /*hidden argument*/NULL);
		NullCheck(L_43);
		int32_t L_44 = JSONValue_get_Integer_m3463426723(L_43, /*hidden argument*/NULL);
		V_12 = L_44;
		int32_t L_45 = V_12;
		switch (L_45)
		{
			case 0:
			{
				goto IL_0152;
			}
			case 1:
			{
				goto IL_0160;
			}
			case 2:
			{
				goto IL_016e;
			}
			case 3:
			{
				goto IL_017c;
			}
			case 4:
			{
				goto IL_018a;
			}
			case 5:
			{
				goto IL_0198;
			}
			case 6:
			{
				goto IL_01a6;
			}
			case 7:
			{
				goto IL_01b4;
			}
		}
	}
	{
		goto IL_01c2;
	}

IL_0152:
	{
		JSONObject_t321714843 * L_46 = V_10;
		AbsoluteUriRecord_t2525168784 * L_47 = (AbsoluteUriRecord_t2525168784 *)il2cpp_codegen_object_new(AbsoluteUriRecord_t2525168784_il2cpp_TypeInfo_var);
		AbsoluteUriRecord__ctor_m157112738(L_47, L_46, /*hidden argument*/NULL);
		V_11 = L_47;
		goto IL_01d0;
	}

IL_0160:
	{
		JSONObject_t321714843 * L_48 = V_10;
		EmptyRecord_t1486430273 * L_49 = (EmptyRecord_t1486430273 *)il2cpp_codegen_object_new(EmptyRecord_t1486430273_il2cpp_TypeInfo_var);
		EmptyRecord__ctor_m3438728046(L_49, L_48, /*hidden argument*/NULL);
		V_11 = L_49;
		goto IL_01d0;
	}

IL_016e:
	{
		JSONObject_t321714843 * L_50 = V_10;
		ExternalTypeRecord_t4087466745 * L_51 = (ExternalTypeRecord_t4087466745 *)il2cpp_codegen_object_new(ExternalTypeRecord_t4087466745_il2cpp_TypeInfo_var);
		ExternalTypeRecord__ctor_m3317940198(L_51, L_50, /*hidden argument*/NULL);
		V_11 = L_51;
		goto IL_01d0;
	}

IL_017c:
	{
		JSONObject_t321714843 * L_52 = V_10;
		MimeMediaRecord_t736820488 * L_53 = (MimeMediaRecord_t736820488 *)il2cpp_codegen_object_new(MimeMediaRecord_t736820488_il2cpp_TypeInfo_var);
		MimeMediaRecord__ctor_m1733123593(L_53, L_52, /*hidden argument*/NULL);
		V_11 = L_53;
		goto IL_01d0;
	}

IL_018a:
	{
		JSONObject_t321714843 * L_54 = V_10;
		SmartPosterRecord_t1640848801 * L_55 = (SmartPosterRecord_t1640848801 *)il2cpp_codegen_object_new(SmartPosterRecord_t1640848801_il2cpp_TypeInfo_var);
		SmartPosterRecord__ctor_m3134893622(L_55, L_54, /*hidden argument*/NULL);
		V_11 = L_55;
		goto IL_01d0;
	}

IL_0198:
	{
		JSONObject_t321714843 * L_56 = V_10;
		TextRecord_t2313697623 * L_57 = (TextRecord_t2313697623 *)il2cpp_codegen_object_new(TextRecord_t2313697623_il2cpp_TypeInfo_var);
		TextRecord__ctor_m2059777621(L_57, L_56, /*hidden argument*/NULL);
		V_11 = L_57;
		goto IL_01d0;
	}

IL_01a6:
	{
		JSONObject_t321714843 * L_58 = V_10;
		UnknownRecord_t3228240714 * L_59 = (UnknownRecord_t3228240714 *)il2cpp_codegen_object_new(UnknownRecord_t3228240714_il2cpp_TypeInfo_var);
		UnknownRecord__ctor_m1040786691(L_59, L_58, /*hidden argument*/NULL);
		V_11 = L_59;
		goto IL_01d0;
	}

IL_01b4:
	{
		JSONObject_t321714843 * L_60 = V_10;
		UriRecord_t2230063309 * L_61 = (UriRecord_t2230063309 *)il2cpp_codegen_object_new(UriRecord_t2230063309_il2cpp_TypeInfo_var);
		UriRecord__ctor_m12655916(L_61, L_60, /*hidden argument*/NULL);
		V_11 = L_61;
		goto IL_01d0;
	}

IL_01c2:
	{
		JSONObject_t321714843 * L_62 = V_10;
		UnknownRecord_t3228240714 * L_63 = (UnknownRecord_t3228240714 *)il2cpp_codegen_object_new(UnknownRecord_t3228240714_il2cpp_TypeInfo_var);
		UnknownRecord__ctor_m1040786691(L_63, L_62, /*hidden argument*/NULL);
		V_11 = L_63;
		goto IL_01d0;
	}

IL_01d0:
	{
		List_1_t4162620298 * L_64 = __this->get_extraRecords_4();
		NDEFRecord_t2690545556 * L_65 = V_11;
		NullCheck(L_64);
		List_1_Add_m350525082(L_64, L_65, /*hidden argument*/List_1_Add_m350525082_RuntimeMethod_var);
		int32_t L_66 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_66, (int32_t)1));
	}

IL_01e3:
	{
		int32_t L_67 = V_9;
		int32_t L_68 = V_8;
		if ((((int32_t)L_67) < ((int32_t)L_68)))
		{
			goto IL_0100;
		}
	}

IL_01ec:
	{
		JSONObject_t321714843 * L_69 = ___jsonObject0;
		NullCheck(L_69);
		JSONObject_TryGetInt_m2083265671(L_69, _stringLiteral2365897554, (int32_t*)(&V_13), /*hidden argument*/NULL);
		int32_t L_70 = V_13;
		__this->set_action_5(L_70);
		JSONObject_t321714843 * L_71 = ___jsonObject0;
		int32_t* L_72 = __this->get_address_of_size_6();
		NullCheck(L_71);
		JSONObject_TryGetInt_m2083265671(L_71, _stringLiteral4049040645, (int32_t*)L_72, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_73 = ___jsonObject0;
		String_t** L_74 = __this->get_address_of_mimeType_7();
		NullCheck(L_73);
		JSONObject_TryGetString_m766921647(L_73, _stringLiteral2142701798, (String_t**)L_74, /*hidden argument*/NULL);
		return;
	}
}
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.SmartPosterRecord::ToJSON()
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * SmartPosterRecord_ToJSON_m1372760268 (SmartPosterRecord_t1640848801 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmartPosterRecord_ToJSON_m1372760268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t321714843 * V_0 = NULL;
	JSONArray_t4024675823 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	JSONArray_t4024675823 * V_4 = NULL;
	int32_t V_5 = 0;
	JSONArray_t4024675823 * V_6 = NULL;
	int32_t V_7 = 0;
	{
		JSONObject_t321714843 * L_0 = NDEFRecord_ToJSON_m164142245(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t321714843 * L_1 = V_0;
		UriRecord_t2230063309 * L_2 = __this->get_uriRecord_1();
		NullCheck(L_2);
		JSONObject_t321714843 * L_3 = VirtFuncInvoker0< JSONObject_t321714843 * >::Invoke(7 /* DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.NDEFRecord::ToJSON() */, L_2);
		JSONValue_t4275860644 * L_4 = JSONValue_op_Implicit_m1136109141(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		JSONObject_Add_m2412535806(L_1, _stringLiteral2628391645, L_4, /*hidden argument*/NULL);
		JSONArray_t4024675823 * L_5 = (JSONArray_t4024675823 *)il2cpp_codegen_object_new(JSONArray_t4024675823_il2cpp_TypeInfo_var);
		JSONArray__ctor_m3612236623(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		List_1_t3785772365 * L_6 = __this->get_titleRecords_2();
		NullCheck(L_6);
		int32_t L_7 = List_1_get_Count_m3842818989(L_6, /*hidden argument*/List_1_get_Count_m3842818989_RuntimeMethod_var);
		V_2 = L_7;
		V_3 = 0;
		goto IL_005b;
	}

IL_003b:
	{
		JSONArray_t4024675823 * L_8 = V_1;
		List_1_t3785772365 * L_9 = __this->get_titleRecords_2();
		int32_t L_10 = V_3;
		NullCheck(L_9);
		TextRecord_t2313697623 * L_11 = List_1_get_Item_m3864448256(L_9, L_10, /*hidden argument*/List_1_get_Item_m3864448256_RuntimeMethod_var);
		NullCheck(L_11);
		JSONObject_t321714843 * L_12 = VirtFuncInvoker0< JSONObject_t321714843 * >::Invoke(7 /* DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.NDEFRecord::ToJSON() */, L_11);
		JSONValue_t4275860644 * L_13 = JSONValue_op_Implicit_m1136109141(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NullCheck(L_8);
		JSONArray_Add_m1346855851(L_8, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_005b:
	{
		int32_t L_15 = V_3;
		int32_t L_16 = V_2;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_003b;
		}
	}
	{
		JSONObject_t321714843 * L_17 = V_0;
		JSONArray_t4024675823 * L_18 = V_1;
		JSONValue_t4275860644 * L_19 = JSONValue_op_Implicit_m216610386(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		JSONObject_Add_m2412535806(L_17, _stringLiteral1559920598, L_19, /*hidden argument*/NULL);
		JSONArray_t4024675823 * L_20 = (JSONArray_t4024675823 *)il2cpp_codegen_object_new(JSONArray_t4024675823_il2cpp_TypeInfo_var);
		JSONArray__ctor_m3612236623(L_20, /*hidden argument*/NULL);
		V_4 = L_20;
		List_1_t2208895230 * L_21 = __this->get_iconRecords_3();
		NullCheck(L_21);
		int32_t L_22 = List_1_get_Count_m1758644702(L_21, /*hidden argument*/List_1_get_Count_m1758644702_RuntimeMethod_var);
		V_2 = L_22;
		V_5 = 0;
		goto IL_00b2;
	}

IL_008e:
	{
		JSONArray_t4024675823 * L_23 = V_4;
		List_1_t2208895230 * L_24 = __this->get_iconRecords_3();
		int32_t L_25 = V_5;
		NullCheck(L_24);
		MimeMediaRecord_t736820488 * L_26 = List_1_get_Item_m988656695(L_24, L_25, /*hidden argument*/List_1_get_Item_m988656695_RuntimeMethod_var);
		NullCheck(L_26);
		JSONObject_t321714843 * L_27 = VirtFuncInvoker0< JSONObject_t321714843 * >::Invoke(7 /* DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.NDEFRecord::ToJSON() */, L_26);
		JSONValue_t4275860644 * L_28 = JSONValue_op_Implicit_m1136109141(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		NullCheck(L_23);
		JSONArray_Add_m1346855851(L_23, L_28, /*hidden argument*/NULL);
		int32_t L_29 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
	}

IL_00b2:
	{
		int32_t L_30 = V_5;
		int32_t L_31 = V_2;
		if ((((int32_t)L_30) < ((int32_t)L_31)))
		{
			goto IL_008e;
		}
	}
	{
		JSONObject_t321714843 * L_32 = V_0;
		JSONArray_t4024675823 * L_33 = V_4;
		JSONValue_t4275860644 * L_34 = JSONValue_op_Implicit_m216610386(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		JSONObject_Add_m2412535806(L_32, _stringLiteral4086469251, L_34, /*hidden argument*/NULL);
		JSONArray_t4024675823 * L_35 = (JSONArray_t4024675823 *)il2cpp_codegen_object_new(JSONArray_t4024675823_il2cpp_TypeInfo_var);
		JSONArray__ctor_m3612236623(L_35, /*hidden argument*/NULL);
		V_6 = L_35;
		List_1_t4162620298 * L_36 = __this->get_extraRecords_4();
		NullCheck(L_36);
		int32_t L_37 = List_1_get_Count_m2774962351(L_36, /*hidden argument*/List_1_get_Count_m2774962351_RuntimeMethod_var);
		V_2 = L_37;
		V_7 = 0;
		goto IL_010b;
	}

IL_00e7:
	{
		JSONArray_t4024675823 * L_38 = V_6;
		List_1_t4162620298 * L_39 = __this->get_extraRecords_4();
		int32_t L_40 = V_7;
		NullCheck(L_39);
		NDEFRecord_t2690545556 * L_41 = List_1_get_Item_m1387264933(L_39, L_40, /*hidden argument*/List_1_get_Item_m1387264933_RuntimeMethod_var);
		NullCheck(L_41);
		JSONObject_t321714843 * L_42 = VirtFuncInvoker0< JSONObject_t321714843 * >::Invoke(7 /* DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.NDEFRecord::ToJSON() */, L_41);
		JSONValue_t4275860644 * L_43 = JSONValue_op_Implicit_m1136109141(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		NullCheck(L_38);
		JSONArray_Add_m1346855851(L_38, L_43, /*hidden argument*/NULL);
		int32_t L_44 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_44, (int32_t)1));
	}

IL_010b:
	{
		int32_t L_45 = V_7;
		int32_t L_46 = V_2;
		if ((((int32_t)L_45) < ((int32_t)L_46)))
		{
			goto IL_00e7;
		}
	}
	{
		JSONObject_t321714843 * L_47 = V_0;
		JSONArray_t4024675823 * L_48 = V_6;
		JSONValue_t4275860644 * L_49 = JSONValue_op_Implicit_m216610386(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		NullCheck(L_47);
		JSONObject_Add_m2412535806(L_47, _stringLiteral1558351666, L_49, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_50 = V_0;
		int32_t L_51 = __this->get_action_5();
		JSONValue_t4275860644 * L_52 = JSONValue_op_Implicit_m3141527758(NULL /*static, unused*/, (((double)((double)L_51))), /*hidden argument*/NULL);
		NullCheck(L_50);
		JSONObject_Add_m2412535806(L_50, _stringLiteral2365897554, L_52, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_53 = V_0;
		int32_t L_54 = __this->get_size_6();
		JSONValue_t4275860644 * L_55 = JSONValue_op_Implicit_m3141527758(NULL /*static, unused*/, (((double)((double)L_54))), /*hidden argument*/NULL);
		NullCheck(L_53);
		JSONObject_Add_m2412535806(L_53, _stringLiteral4049040645, L_55, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_56 = V_0;
		String_t* L_57 = __this->get_mimeType_7();
		JSONValue_t4275860644 * L_58 = JSONValue_op_Implicit_m357672470(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		NullCheck(L_56);
		JSONObject_Add_m2412535806(L_56, _stringLiteral2142701798, L_58, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_59 = V_0;
		return L_59;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.TextRecord::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void TextRecord__ctor_m2466027249 (TextRecord_t2313697623 * __this, String_t* ___text0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextRecord__ctor_m2466027249_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		((NDEFRecord_t2690545556 *)__this)->set_type_0(5);
		String_t* L_0 = ___text0;
		__this->set_text_1(L_0);
		__this->set_languageCode_2(_stringLiteral3454842811);
		__this->set_textEncoding_3(0);
		return;
	}
}
// System.Void DigitsNFCToolkit.TextRecord::.ctor(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void TextRecord__ctor_m3042244564 (TextRecord_t2313697623 * __this, String_t* ___text0, String_t* ___languageCode1, const RuntimeMethod* method)
{
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		((NDEFRecord_t2690545556 *)__this)->set_type_0(5);
		String_t* L_0 = ___text0;
		__this->set_text_1(L_0);
		String_t* L_1 = ___languageCode1;
		__this->set_languageCode_2(L_1);
		__this->set_textEncoding_3(0);
		return;
	}
}
// System.Void DigitsNFCToolkit.TextRecord::.ctor(System.String,System.String,DigitsNFCToolkit.TextRecord/TextEncoding)
extern "C" IL2CPP_METHOD_ATTR void TextRecord__ctor_m2191892803 (TextRecord_t2313697623 * __this, String_t* ___text0, String_t* ___languageCode1, int32_t ___textEncoding2, const RuntimeMethod* method)
{
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		((NDEFRecord_t2690545556 *)__this)->set_type_0(5);
		String_t* L_0 = ___text0;
		__this->set_text_1(L_0);
		String_t* L_1 = ___languageCode1;
		__this->set_languageCode_2(L_1);
		int32_t L_2 = ___textEncoding2;
		__this->set_textEncoding_3(L_2);
		return;
	}
}
// System.Void DigitsNFCToolkit.TextRecord::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void TextRecord__ctor_m2059777621 (TextRecord_t2313697623 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		VirtActionInvoker1< JSONObject_t321714843 * >::Invoke(6 /* System.Void DigitsNFCToolkit.NDEFRecord::ParseJSON(DigitsNFCToolkit.JSON.JSONObject) */, __this, L_0);
		return;
	}
}
// System.Void DigitsNFCToolkit.TextRecord::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void TextRecord_ParseJSON_m1315260603 (TextRecord_t2313697623 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextRecord_ParseJSON_m1315260603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		NDEFRecord_ParseJSON_m1625700843(__this, L_0, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_1 = ___jsonObject0;
		String_t** L_2 = __this->get_address_of_text_1();
		NullCheck(L_1);
		JSONObject_TryGetString_m766921647(L_1, _stringLiteral3987835854, (String_t**)L_2, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_3 = ___jsonObject0;
		String_t** L_4 = __this->get_address_of_languageCode_2();
		NullCheck(L_3);
		JSONObject_TryGetString_m766921647(L_3, _stringLiteral2917620724, (String_t**)L_4, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_5 = ___jsonObject0;
		NullCheck(L_5);
		JSONObject_TryGetInt_m2083265671(L_5, _stringLiteral3221230196, (int32_t*)(&V_0), /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		__this->set_textEncoding_3(L_6);
		return;
	}
}
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.TextRecord::ToJSON()
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * TextRecord_ToJSON_m2400708536 (TextRecord_t2313697623 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextRecord_ToJSON_m2400708536_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t321714843 * V_0 = NULL;
	{
		JSONObject_t321714843 * L_0 = NDEFRecord_ToJSON_m164142245(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t321714843 * L_1 = V_0;
		String_t* L_2 = __this->get_text_1();
		JSONValue_t4275860644 * L_3 = JSONValue_op_Implicit_m357672470(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		JSONObject_Add_m2412535806(L_1, _stringLiteral3987835854, L_3, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_4 = V_0;
		String_t* L_5 = __this->get_languageCode_2();
		JSONValue_t4275860644 * L_6 = JSONValue_op_Implicit_m357672470(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		JSONObject_Add_m2412535806(L_4, _stringLiteral2917620724, L_6, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_7 = V_0;
		int32_t L_8 = __this->get_textEncoding_3();
		JSONValue_t4275860644 * L_9 = JSONValue_op_Implicit_m3141527758(NULL /*static, unused*/, (((double)((double)L_8))), /*hidden argument*/NULL);
		NullCheck(L_7);
		JSONObject_Add_m2412535806(L_7, _stringLiteral3221230196, L_9, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_10 = V_0;
		return L_10;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.UnknownRecord::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void UnknownRecord__ctor_m4285644215 (UnknownRecord_t3228240714 * __this, String_t* ___uri0, const RuntimeMethod* method)
{
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		((NDEFRecord_t2690545556 *)__this)->set_type_0(6);
		return;
	}
}
// System.Void DigitsNFCToolkit.UnknownRecord::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void UnknownRecord__ctor_m1040786691 (UnknownRecord_t3228240714 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		VirtActionInvoker1< JSONObject_t321714843 * >::Invoke(6 /* System.Void DigitsNFCToolkit.NDEFRecord::ParseJSON(DigitsNFCToolkit.JSON.JSONObject) */, __this, L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.UriRecord::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void UriRecord__ctor_m714533555 (UriRecord_t2230063309 * __this, String_t* ___fullUri0, const RuntimeMethod* method)
{
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		((NDEFRecord_t2690545556 *)__this)->set_type_0(7);
		String_t* L_0 = ___fullUri0;
		__this->set_fullUri_1(L_0);
		return;
	}
}
// System.Void DigitsNFCToolkit.UriRecord::.ctor(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void UriRecord__ctor_m12655916 (UriRecord_t2230063309 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	{
		NDEFRecord__ctor_m2075191861(__this, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		VirtActionInvoker1< JSONObject_t321714843 * >::Invoke(6 /* System.Void DigitsNFCToolkit.NDEFRecord::ParseJSON(DigitsNFCToolkit.JSON.JSONObject) */, __this, L_0);
		return;
	}
}
// System.Void DigitsNFCToolkit.UriRecord::ParseJSON(DigitsNFCToolkit.JSON.JSONObject)
extern "C" IL2CPP_METHOD_ATTR void UriRecord_ParseJSON_m3469570412 (UriRecord_t2230063309 * __this, JSONObject_t321714843 * ___jsonObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UriRecord_ParseJSON_m3469570412_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JSONObject_t321714843 * L_0 = ___jsonObject0;
		NDEFRecord_ParseJSON_m1625700843(__this, L_0, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_1 = ___jsonObject0;
		String_t** L_2 = __this->get_address_of_fullUri_1();
		NullCheck(L_1);
		JSONObject_TryGetString_m766921647(L_1, _stringLiteral1299802695, (String_t**)L_2, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_3 = ___jsonObject0;
		String_t** L_4 = __this->get_address_of_uri_2();
		NullCheck(L_3);
		JSONObject_TryGetString_m766921647(L_3, _stringLiteral3313977880, (String_t**)L_4, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_5 = ___jsonObject0;
		String_t** L_6 = __this->get_address_of_protocol_3();
		NullCheck(L_5);
		JSONObject_TryGetString_m766921647(L_5, _stringLiteral2468127799, (String_t**)L_6, /*hidden argument*/NULL);
		return;
	}
}
// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.UriRecord::ToJSON()
extern "C" IL2CPP_METHOD_ATTR JSONObject_t321714843 * UriRecord_ToJSON_m1049998470 (UriRecord_t2230063309 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UriRecord_ToJSON_m1049998470_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JSONObject_t321714843 * V_0 = NULL;
	{
		JSONObject_t321714843 * L_0 = NDEFRecord_ToJSON_m164142245(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t321714843 * L_1 = V_0;
		String_t* L_2 = __this->get_fullUri_1();
		JSONValue_t4275860644 * L_3 = JSONValue_op_Implicit_m357672470(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		JSONObject_Add_m2412535806(L_1, _stringLiteral1299802695, L_3, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_4 = V_0;
		String_t* L_5 = __this->get_uri_2();
		JSONValue_t4275860644 * L_6 = JSONValue_op_Implicit_m357672470(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		JSONObject_Add_m2412535806(L_4, _stringLiteral3313977880, L_6, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_7 = V_0;
		String_t* L_8 = __this->get_protocol_3();
		JSONValue_t4275860644 * L_9 = JSONValue_op_Implicit_m357672470(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		JSONObject_Add_m2412535806(L_7, _stringLiteral2468127799, L_9, /*hidden argument*/NULL);
		JSONObject_t321714843 * L_10 = V_0;
		return L_10;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DigitsNFCToolkit.Util::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Util__ctor_m374164062 (Util_t4025012431 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String DigitsNFCToolkit.Util::EncodeBase64UrlSafe(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR String_t* Util_EncodeBase64UrlSafe_m2760074679 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___bytes0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Util_EncodeBase64UrlSafe_m2760074679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ByteU5BU5D_t4116647657* L_0 = ___bytes0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		String_t* L_1 = Convert_ToBase64String_m3839334935(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		NullCheck(L_2);
		String_t* L_4 = String_Replace_m1273907647(L_2, _stringLiteral3452614547, L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = String_Replace_m1273907647(L_4, _stringLiteral3452614529, _stringLiteral3452614641, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = String_Replace_m1273907647(L_5, _stringLiteral3452614533, _stringLiteral3452614531, /*hidden argument*/NULL);
		V_0 = L_6;
		String_t* L_7 = V_0;
		return L_7;
	}
}
// System.Byte[] DigitsNFCToolkit.Util::DecodeBase64UrlSafe(System.String)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_t4116647657* Util_DecodeBase64UrlSafe_m4209975833 (RuntimeObject * __this /* static, unused */, String_t* ___base64String0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Util_DecodeBase64UrlSafe_m4209975833_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___base64String0;
		String_t* L_1 = ___base64String0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m3847582255(L_1, /*hidden argument*/NULL);
		String_t* L_3 = ___base64String0;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m3847582255(L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_5 = String_PadRight_m50345030(L_0, ((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)4, (int32_t)((int32_t)((int32_t)L_4%(int32_t)4))))%(int32_t)4)))), ((int32_t)61), /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = String_Replace_m1273907647(L_5, _stringLiteral3452614641, _stringLiteral3452614529, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = String_Replace_m1273907647(L_6, _stringLiteral3452614531, _stringLiteral3452614533, /*hidden argument*/NULL);
		___base64String0 = L_7;
		String_t* L_8 = ___base64String0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		ByteU5BU5D_t4116647657* L_9 = Convert_FromBase64String_m3685135396(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
