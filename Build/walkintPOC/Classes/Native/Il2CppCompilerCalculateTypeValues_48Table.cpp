﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// DigitsNFCToolkit.JSON.JSONArray
struct JSONArray_t4024675823;
// DigitsNFCToolkit.JSON.JSONObject
struct JSONObject_t321714843;
// DigitsNFCToolkit.NDEFMessage
struct NDEFMessage_t279637043;
// DigitsNFCToolkit.NDEFPushResult
struct NDEFPushResult_t3422827153;
// DigitsNFCToolkit.NDEFReadResult
struct NDEFReadResult_t3483243621;
// DigitsNFCToolkit.NDEFWriteResult
struct NDEFWriteResult_t4210562629;
// DigitsNFCToolkit.NFCTag
struct NFCTag_t2820711232;
// DigitsNFCToolkit.NFCTechnology[]
struct NFCTechnologyU5BU5D_t328493574;
// DigitsNFCToolkit.NativeNFC
struct NativeNFC_t1941597496;
// DigitsNFCToolkit.OnNDEFPushFinished
struct OnNDEFPushFinished_t4279917764;
// DigitsNFCToolkit.OnNDEFReadFinished
struct OnNDEFReadFinished_t1327886840;
// DigitsNFCToolkit.OnNDEFWriteFinished
struct OnNDEFWriteFinished_t4102039599;
// DigitsNFCToolkit.OnNFCTagDetected
struct OnNFCTagDetected_t3189675727;
// DigitsNFCToolkit.Samples.ImageRecordItem
struct ImageRecordItem_t2104316047;
// DigitsNFCToolkit.Samples.MessageScreenView
struct MessageScreenView_t146641597;
// DigitsNFCToolkit.Samples.ReadScreenControl
struct ReadScreenControl_t3483866810;
// DigitsNFCToolkit.Samples.ReadScreenView
struct ReadScreenView_t239900869;
// DigitsNFCToolkit.Samples.RecordItem
struct RecordItem_t1075151419;
// DigitsNFCToolkit.Samples.WriteScreenControl
struct WriteScreenControl_t1506090515;
// DigitsNFCToolkit.Samples.WriteScreenView
struct WriteScreenView_t2350253495;
// DigitsNFCToolkit.UriRecord
struct UriRecord_t2230063309;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue>
struct IDictionary_2_t2524968334;
// System.Collections.Generic.List`1<DigitsNFCToolkit.JSON.JSONValue>
struct List_1_t1452968090;
// System.Collections.Generic.List`1<DigitsNFCToolkit.MimeMediaRecord>
struct List_1_t2208895230;
// System.Collections.Generic.List`1<DigitsNFCToolkit.NDEFRecord>
struct List_1_t4162620298;
// System.Collections.Generic.List`1<DigitsNFCToolkit.TextRecord>
struct List_1_t3785772365;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;
// System.Void
struct Void_t1185182177;
// UniWebView
struct UniWebView_t941983939;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.UI.Text
struct Text_t1901882714;




#ifndef U3CMODULEU3E_T692745566_H
#define U3CMODULEU3E_T692745566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745566 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745566_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef JSONARRAY_T4024675823_H
#define JSONARRAY_T4024675823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.JSON.JSONArray
struct  JSONArray_t4024675823  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<DigitsNFCToolkit.JSON.JSONValue> DigitsNFCToolkit.JSON.JSONArray::values
	List_1_t1452968090 * ___values_0;

public:
	inline static int32_t get_offset_of_values_0() { return static_cast<int32_t>(offsetof(JSONArray_t4024675823, ___values_0)); }
	inline List_1_t1452968090 * get_values_0() const { return ___values_0; }
	inline List_1_t1452968090 ** get_address_of_values_0() { return &___values_0; }
	inline void set_values_0(List_1_t1452968090 * value)
	{
		___values_0 = value;
		Il2CppCodeGenWriteBarrier((&___values_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONARRAY_T4024675823_H
#ifndef JSONOBJECT_T321714843_H
#define JSONOBJECT_T321714843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.JSON.JSONObject
struct  JSONObject_t321714843  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,DigitsNFCToolkit.JSON.JSONValue> DigitsNFCToolkit.JSON.JSONObject::values
	RuntimeObject* ___values_0;

public:
	inline static int32_t get_offset_of_values_0() { return static_cast<int32_t>(offsetof(JSONObject_t321714843, ___values_0)); }
	inline RuntimeObject* get_values_0() const { return ___values_0; }
	inline RuntimeObject** get_address_of_values_0() { return &___values_0; }
	inline void set_values_0(RuntimeObject* value)
	{
		___values_0 = value;
		Il2CppCodeGenWriteBarrier((&___values_0), value);
	}
};

struct JSONObject_t321714843_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex DigitsNFCToolkit.JSON.JSONObject::unicodeRegex
	Regex_t3657309853 * ___unicodeRegex_1;
	// System.Byte[] DigitsNFCToolkit.JSON.JSONObject::unicodeBytes
	ByteU5BU5D_t4116647657* ___unicodeBytes_2;

public:
	inline static int32_t get_offset_of_unicodeRegex_1() { return static_cast<int32_t>(offsetof(JSONObject_t321714843_StaticFields, ___unicodeRegex_1)); }
	inline Regex_t3657309853 * get_unicodeRegex_1() const { return ___unicodeRegex_1; }
	inline Regex_t3657309853 ** get_address_of_unicodeRegex_1() { return &___unicodeRegex_1; }
	inline void set_unicodeRegex_1(Regex_t3657309853 * value)
	{
		___unicodeRegex_1 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeRegex_1), value);
	}

	inline static int32_t get_offset_of_unicodeBytes_2() { return static_cast<int32_t>(offsetof(JSONObject_t321714843_StaticFields, ___unicodeBytes_2)); }
	inline ByteU5BU5D_t4116647657* get_unicodeBytes_2() const { return ___unicodeBytes_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_unicodeBytes_2() { return &___unicodeBytes_2; }
	inline void set_unicodeBytes_2(ByteU5BU5D_t4116647657* value)
	{
		___unicodeBytes_2 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeBytes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECT_T321714843_H
#ifndef NDEFPUSHRESULT_T3422827153_H
#define NDEFPUSHRESULT_T3422827153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFPushResult
struct  NDEFPushResult_t3422827153  : public RuntimeObject
{
public:
	// System.Boolean DigitsNFCToolkit.NDEFPushResult::success
	bool ___success_0;
	// DigitsNFCToolkit.NDEFMessage DigitsNFCToolkit.NDEFPushResult::message
	NDEFMessage_t279637043 * ___message_1;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(NDEFPushResult_t3422827153, ___success_0)); }
	inline bool get_success_0() const { return ___success_0; }
	inline bool* get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(bool value)
	{
		___success_0 = value;
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(NDEFPushResult_t3422827153, ___message_1)); }
	inline NDEFMessage_t279637043 * get_message_1() const { return ___message_1; }
	inline NDEFMessage_t279637043 ** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(NDEFMessage_t279637043 * value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFPUSHRESULT_T3422827153_H
#ifndef NFCTAG_T2820711232_H
#define NFCTAG_T2820711232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NFCTag
struct  NFCTag_t2820711232  : public RuntimeObject
{
public:
	// System.String DigitsNFCToolkit.NFCTag::id
	String_t* ___id_0;
	// DigitsNFCToolkit.NFCTechnology[] DigitsNFCToolkit.NFCTag::technologies
	NFCTechnologyU5BU5D_t328493574* ___technologies_1;
	// System.String DigitsNFCToolkit.NFCTag::manufacturer
	String_t* ___manufacturer_2;
	// System.Boolean DigitsNFCToolkit.NFCTag::writable
	bool ___writable_3;
	// System.Int32 DigitsNFCToolkit.NFCTag::maxWriteSize
	int32_t ___maxWriteSize_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(NFCTag_t2820711232, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_technologies_1() { return static_cast<int32_t>(offsetof(NFCTag_t2820711232, ___technologies_1)); }
	inline NFCTechnologyU5BU5D_t328493574* get_technologies_1() const { return ___technologies_1; }
	inline NFCTechnologyU5BU5D_t328493574** get_address_of_technologies_1() { return &___technologies_1; }
	inline void set_technologies_1(NFCTechnologyU5BU5D_t328493574* value)
	{
		___technologies_1 = value;
		Il2CppCodeGenWriteBarrier((&___technologies_1), value);
	}

	inline static int32_t get_offset_of_manufacturer_2() { return static_cast<int32_t>(offsetof(NFCTag_t2820711232, ___manufacturer_2)); }
	inline String_t* get_manufacturer_2() const { return ___manufacturer_2; }
	inline String_t** get_address_of_manufacturer_2() { return &___manufacturer_2; }
	inline void set_manufacturer_2(String_t* value)
	{
		___manufacturer_2 = value;
		Il2CppCodeGenWriteBarrier((&___manufacturer_2), value);
	}

	inline static int32_t get_offset_of_writable_3() { return static_cast<int32_t>(offsetof(NFCTag_t2820711232, ___writable_3)); }
	inline bool get_writable_3() const { return ___writable_3; }
	inline bool* get_address_of_writable_3() { return &___writable_3; }
	inline void set_writable_3(bool value)
	{
		___writable_3 = value;
	}

	inline static int32_t get_offset_of_maxWriteSize_4() { return static_cast<int32_t>(offsetof(NFCTag_t2820711232, ___maxWriteSize_4)); }
	inline int32_t get_maxWriteSize_4() const { return ___maxWriteSize_4; }
	inline int32_t* get_address_of_maxWriteSize_4() { return &___maxWriteSize_4; }
	inline void set_maxWriteSize_4(int32_t value)
	{
		___maxWriteSize_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NFCTAG_T2820711232_H
#ifndef UTIL_T4025012431_H
#define UTIL_T4025012431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Util
struct  Util_t4025012431  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTIL_T4025012431_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CGETHTMLCONTENTU3EC__ANONSTOREY0_T803715261_H
#define U3CGETHTMLCONTENTU3EC__ANONSTOREY0_T803715261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/<GetHTMLContent>c__AnonStorey0
struct  U3CGetHTMLContentU3Ec__AnonStorey0_t803715261  : public RuntimeObject
{
public:
	// System.Action`1<System.String> UniWebView/<GetHTMLContent>c__AnonStorey0::handler
	Action_1_t2019918284 * ___handler_0;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(U3CGetHTMLContentU3Ec__AnonStorey0_t803715261, ___handler_0)); }
	inline Action_1_t2019918284 * get_handler_0() const { return ___handler_0; }
	inline Action_1_t2019918284 ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(Action_1_t2019918284 * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___handler_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETHTMLCONTENTU3EC__ANONSTOREY0_T803715261_H
#ifndef UNIWEBVIEWHELPER_T1459904593_H
#define UNIWEBVIEWHELPER_T1459904593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewHelper
struct  UniWebViewHelper_t1459904593  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWHELPER_T1459904593_H
#ifndef UNIWEBVIEWNATIVERESULTPAYLOAD_T4072739617_H
#define UNIWEBVIEWNATIVERESULTPAYLOAD_T4072739617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewNativeResultPayload
struct  UniWebViewNativeResultPayload_t4072739617  : public RuntimeObject
{
public:
	// System.String UniWebViewNativeResultPayload::identifier
	String_t* ___identifier_0;
	// System.String UniWebViewNativeResultPayload::resultCode
	String_t* ___resultCode_1;
	// System.String UniWebViewNativeResultPayload::data
	String_t* ___data_2;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(UniWebViewNativeResultPayload_t4072739617, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_resultCode_1() { return static_cast<int32_t>(offsetof(UniWebViewNativeResultPayload_t4072739617, ___resultCode_1)); }
	inline String_t* get_resultCode_1() const { return ___resultCode_1; }
	inline String_t** get_address_of_resultCode_1() { return &___resultCode_1; }
	inline void set_resultCode_1(String_t* value)
	{
		___resultCode_1 = value;
		Il2CppCodeGenWriteBarrier((&___resultCode_1), value);
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(UniWebViewNativeResultPayload_t4072739617, ___data_2)); }
	inline String_t* get_data_2() const { return ___data_2; }
	inline String_t** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(String_t* value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWNATIVERESULTPAYLOAD_T4072739617_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef UNIWEBVIEWMESSAGE_T2441068380_H
#define UNIWEBVIEWMESSAGE_T2441068380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewMessage
struct  UniWebViewMessage_t2441068380 
{
public:
	// System.String UniWebViewMessage::<RawMessage>k__BackingField
	String_t* ___U3CRawMessageU3Ek__BackingField_0;
	// System.String UniWebViewMessage::<Scheme>k__BackingField
	String_t* ___U3CSchemeU3Ek__BackingField_1;
	// System.String UniWebViewMessage::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniWebViewMessage::<Args>k__BackingField
	Dictionary_2_t1632706988 * ___U3CArgsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CRawMessageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UniWebViewMessage_t2441068380, ___U3CRawMessageU3Ek__BackingField_0)); }
	inline String_t* get_U3CRawMessageU3Ek__BackingField_0() const { return ___U3CRawMessageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CRawMessageU3Ek__BackingField_0() { return &___U3CRawMessageU3Ek__BackingField_0; }
	inline void set_U3CRawMessageU3Ek__BackingField_0(String_t* value)
	{
		___U3CRawMessageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawMessageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CSchemeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UniWebViewMessage_t2441068380, ___U3CSchemeU3Ek__BackingField_1)); }
	inline String_t* get_U3CSchemeU3Ek__BackingField_1() const { return ___U3CSchemeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CSchemeU3Ek__BackingField_1() { return &___U3CSchemeU3Ek__BackingField_1; }
	inline void set_U3CSchemeU3Ek__BackingField_1(String_t* value)
	{
		___U3CSchemeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSchemeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UniWebViewMessage_t2441068380, ___U3CPathU3Ek__BackingField_2)); }
	inline String_t* get_U3CPathU3Ek__BackingField_2() const { return ___U3CPathU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_2() { return &___U3CPathU3Ek__BackingField_2; }
	inline void set_U3CPathU3Ek__BackingField_2(String_t* value)
	{
		___U3CPathU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CArgsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UniWebViewMessage_t2441068380, ___U3CArgsU3Ek__BackingField_3)); }
	inline Dictionary_2_t1632706988 * get_U3CArgsU3Ek__BackingField_3() const { return ___U3CArgsU3Ek__BackingField_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_U3CArgsU3Ek__BackingField_3() { return &___U3CArgsU3Ek__BackingField_3; }
	inline void set_U3CArgsU3Ek__BackingField_3(Dictionary_2_t1632706988 * value)
	{
		___U3CArgsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArgsU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UniWebViewMessage
struct UniWebViewMessage_t2441068380_marshaled_pinvoke
{
	char* ___U3CRawMessageU3Ek__BackingField_0;
	char* ___U3CSchemeU3Ek__BackingField_1;
	char* ___U3CPathU3Ek__BackingField_2;
	Dictionary_2_t1632706988 * ___U3CArgsU3Ek__BackingField_3;
};
// Native definition for COM marshalling of UniWebViewMessage
struct UniWebViewMessage_t2441068380_marshaled_com
{
	Il2CppChar* ___U3CRawMessageU3Ek__BackingField_0;
	Il2CppChar* ___U3CSchemeU3Ek__BackingField_1;
	Il2CppChar* ___U3CPathU3Ek__BackingField_2;
	Dictionary_2_t1632706988 * ___U3CArgsU3Ek__BackingField_3;
};
#endif // UNIWEBVIEWMESSAGE_T2441068380_H
#ifndef JSONPARSINGSTATE_T2722137651_H
#define JSONPARSINGSTATE_T2722137651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.JSON.JSONObject/JSONParsingState
struct  JSONParsingState_t2722137651 
{
public:
	// System.Int32 DigitsNFCToolkit.JSON.JSONObject/JSONParsingState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JSONParsingState_t2722137651, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPARSINGSTATE_T2722137651_H
#ifndef JSONVALUETYPE_T3019243058_H
#define JSONVALUETYPE_T3019243058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.JSON.JSONValueType
struct  JSONValueType_t3019243058 
{
public:
	// System.Int32 DigitsNFCToolkit.JSON.JSONValueType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JSONValueType_t3019243058, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONVALUETYPE_T3019243058_H
#ifndef NDEFMESSAGEWRITEERROR_T164821916_H
#define NDEFMESSAGEWRITEERROR_T164821916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFMessageWriteError
struct  NDEFMessageWriteError_t164821916 
{
public:
	// System.Int32 DigitsNFCToolkit.NDEFMessageWriteError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NDEFMessageWriteError_t164821916, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFMESSAGEWRITEERROR_T164821916_H
#ifndef NDEFMESSAGEWRITESTATE_T2549748319_H
#define NDEFMESSAGEWRITESTATE_T2549748319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFMessageWriteState
struct  NDEFMessageWriteState_t2549748319 
{
public:
	// System.Int32 DigitsNFCToolkit.NDEFMessageWriteState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NDEFMessageWriteState_t2549748319, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFMESSAGEWRITESTATE_T2549748319_H
#ifndef NDEFREADERROR_T886852181_H
#define NDEFREADERROR_T886852181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFReadError
struct  NDEFReadError_t886852181 
{
public:
	// System.Int32 DigitsNFCToolkit.NDEFReadError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NDEFReadError_t886852181, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFREADERROR_T886852181_H
#ifndef NDEFRECORDTYPE_T4294622310_H
#define NDEFRECORDTYPE_T4294622310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFRecordType
struct  NDEFRecordType_t4294622310 
{
public:
	// System.Int32 DigitsNFCToolkit.NDEFRecordType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NDEFRecordType_t4294622310, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFRECORDTYPE_T4294622310_H
#ifndef NDEFWRITEERROR_T890528595_H
#define NDEFWRITEERROR_T890528595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFWriteError
struct  NDEFWriteError_t890528595 
{
public:
	// System.Int32 DigitsNFCToolkit.NDEFWriteError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NDEFWriteError_t890528595, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFWRITEERROR_T890528595_H
#ifndef NFCTECHNOLOGY_T1376062623_H
#define NFCTECHNOLOGY_T1376062623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NFCTechnology
struct  NFCTechnology_t1376062623 
{
public:
	// System.Int32 DigitsNFCToolkit.NFCTechnology::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NFCTechnology_t1376062623, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NFCTECHNOLOGY_T1376062623_H
#ifndef ICONID_T582738576_H
#define ICONID_T582738576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.WriteScreenControl/IconID
struct  IconID_t582738576 
{
public:
	// System.Int32 DigitsNFCToolkit.Samples.WriteScreenControl/IconID::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IconID_t582738576, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICONID_T582738576_H
#ifndef RECOMMENDEDACTION_T1182550772_H
#define RECOMMENDEDACTION_T1182550772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.SmartPosterRecord/RecommendedAction
struct  RecommendedAction_t1182550772 
{
public:
	// System.Int32 DigitsNFCToolkit.SmartPosterRecord/RecommendedAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RecommendedAction_t1182550772, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECOMMENDEDACTION_T1182550772_H
#ifndef TEXTENCODING_T3210513626_H
#define TEXTENCODING_T3210513626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.TextRecord/TextEncoding
struct  TextEncoding_t3210513626 
{
public:
	// System.Int32 DigitsNFCToolkit.TextRecord/TextEncoding::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextEncoding_t3210513626, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTENCODING_T3210513626_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef LEVEL_T4211844589_H
#define LEVEL_T4211844589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewLogger/Level
struct  Level_t4211844589 
{
public:
	// System.Int32 UniWebViewLogger/Level::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Level_t4211844589, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVEL_T4211844589_H
#ifndef UNIWEBVIEWTOOLBARPOSITION_T2230581447_H
#define UNIWEBVIEWTOOLBARPOSITION_T2230581447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewToolbarPosition
struct  UniWebViewToolbarPosition_t2230581447 
{
public:
	// System.Int32 UniWebViewToolbarPosition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UniWebViewToolbarPosition_t2230581447, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWTOOLBARPOSITION_T2230581447_H
#ifndef UNIWEBVIEWTRANSITIONEDGE_T2354894166_H
#define UNIWEBVIEWTRANSITIONEDGE_T2354894166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewTransitionEdge
struct  UniWebViewTransitionEdge_t2354894166 
{
public:
	// System.Int32 UniWebViewTransitionEdge::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UniWebViewTransitionEdge_t2354894166, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWTRANSITIONEDGE_T2354894166_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef SCREENORIENTATION_T1705519499_H
#define SCREENORIENTATION_T1705519499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t1705519499 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScreenOrientation_t1705519499, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T1705519499_H
#ifndef JSONVALUE_T4275860644_H
#define JSONVALUE_T4275860644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.JSON.JSONValue
struct  JSONValue_t4275860644  : public RuntimeObject
{
public:
	// DigitsNFCToolkit.JSON.JSONValueType DigitsNFCToolkit.JSON.JSONValue::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_0;
	// System.String DigitsNFCToolkit.JSON.JSONValue::<String>k__BackingField
	String_t* ___U3CStringU3Ek__BackingField_1;
	// System.Double DigitsNFCToolkit.JSON.JSONValue::<Double>k__BackingField
	double ___U3CDoubleU3Ek__BackingField_2;
	// DigitsNFCToolkit.JSON.JSONObject DigitsNFCToolkit.JSON.JSONValue::<Object>k__BackingField
	JSONObject_t321714843 * ___U3CObjectU3Ek__BackingField_3;
	// DigitsNFCToolkit.JSON.JSONArray DigitsNFCToolkit.JSON.JSONValue::<Array>k__BackingField
	JSONArray_t4024675823 * ___U3CArrayU3Ek__BackingField_4;
	// System.Boolean DigitsNFCToolkit.JSON.JSONValue::<Boolean>k__BackingField
	bool ___U3CBooleanU3Ek__BackingField_5;
	// DigitsNFCToolkit.JSON.JSONValue DigitsNFCToolkit.JSON.JSONValue::<Parent>k__BackingField
	JSONValue_t4275860644 * ___U3CParentU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JSONValue_t4275860644, ___U3CTypeU3Ek__BackingField_0)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_0() const { return ___U3CTypeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_0() { return &___U3CTypeU3Ek__BackingField_0; }
	inline void set_U3CTypeU3Ek__BackingField_0(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStringU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JSONValue_t4275860644, ___U3CStringU3Ek__BackingField_1)); }
	inline String_t* get_U3CStringU3Ek__BackingField_1() const { return ___U3CStringU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CStringU3Ek__BackingField_1() { return &___U3CStringU3Ek__BackingField_1; }
	inline void set_U3CStringU3Ek__BackingField_1(String_t* value)
	{
		___U3CStringU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStringU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDoubleU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(JSONValue_t4275860644, ___U3CDoubleU3Ek__BackingField_2)); }
	inline double get_U3CDoubleU3Ek__BackingField_2() const { return ___U3CDoubleU3Ek__BackingField_2; }
	inline double* get_address_of_U3CDoubleU3Ek__BackingField_2() { return &___U3CDoubleU3Ek__BackingField_2; }
	inline void set_U3CDoubleU3Ek__BackingField_2(double value)
	{
		___U3CDoubleU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CObjectU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(JSONValue_t4275860644, ___U3CObjectU3Ek__BackingField_3)); }
	inline JSONObject_t321714843 * get_U3CObjectU3Ek__BackingField_3() const { return ___U3CObjectU3Ek__BackingField_3; }
	inline JSONObject_t321714843 ** get_address_of_U3CObjectU3Ek__BackingField_3() { return &___U3CObjectU3Ek__BackingField_3; }
	inline void set_U3CObjectU3Ek__BackingField_3(JSONObject_t321714843 * value)
	{
		___U3CObjectU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjectU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CArrayU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(JSONValue_t4275860644, ___U3CArrayU3Ek__BackingField_4)); }
	inline JSONArray_t4024675823 * get_U3CArrayU3Ek__BackingField_4() const { return ___U3CArrayU3Ek__BackingField_4; }
	inline JSONArray_t4024675823 ** get_address_of_U3CArrayU3Ek__BackingField_4() { return &___U3CArrayU3Ek__BackingField_4; }
	inline void set_U3CArrayU3Ek__BackingField_4(JSONArray_t4024675823 * value)
	{
		___U3CArrayU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArrayU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CBooleanU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(JSONValue_t4275860644, ___U3CBooleanU3Ek__BackingField_5)); }
	inline bool get_U3CBooleanU3Ek__BackingField_5() const { return ___U3CBooleanU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CBooleanU3Ek__BackingField_5() { return &___U3CBooleanU3Ek__BackingField_5; }
	inline void set_U3CBooleanU3Ek__BackingField_5(bool value)
	{
		___U3CBooleanU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CParentU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JSONValue_t4275860644, ___U3CParentU3Ek__BackingField_6)); }
	inline JSONValue_t4275860644 * get_U3CParentU3Ek__BackingField_6() const { return ___U3CParentU3Ek__BackingField_6; }
	inline JSONValue_t4275860644 ** get_address_of_U3CParentU3Ek__BackingField_6() { return &___U3CParentU3Ek__BackingField_6; }
	inline void set_U3CParentU3Ek__BackingField_6(JSONValue_t4275860644 * value)
	{
		___U3CParentU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONVALUE_T4275860644_H
#ifndef NDEFMESSAGE_T279637043_H
#define NDEFMESSAGE_T279637043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFMessage
struct  NDEFMessage_t279637043  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<DigitsNFCToolkit.NDEFRecord> DigitsNFCToolkit.NDEFMessage::records
	List_1_t4162620298 * ___records_0;
	// System.String DigitsNFCToolkit.NDEFMessage::tagID
	String_t* ___tagID_1;
	// DigitsNFCToolkit.NDEFMessageWriteState DigitsNFCToolkit.NDEFMessage::writeState
	int32_t ___writeState_2;
	// DigitsNFCToolkit.NDEFMessageWriteError DigitsNFCToolkit.NDEFMessage::writeError
	int32_t ___writeError_3;

public:
	inline static int32_t get_offset_of_records_0() { return static_cast<int32_t>(offsetof(NDEFMessage_t279637043, ___records_0)); }
	inline List_1_t4162620298 * get_records_0() const { return ___records_0; }
	inline List_1_t4162620298 ** get_address_of_records_0() { return &___records_0; }
	inline void set_records_0(List_1_t4162620298 * value)
	{
		___records_0 = value;
		Il2CppCodeGenWriteBarrier((&___records_0), value);
	}

	inline static int32_t get_offset_of_tagID_1() { return static_cast<int32_t>(offsetof(NDEFMessage_t279637043, ___tagID_1)); }
	inline String_t* get_tagID_1() const { return ___tagID_1; }
	inline String_t** get_address_of_tagID_1() { return &___tagID_1; }
	inline void set_tagID_1(String_t* value)
	{
		___tagID_1 = value;
		Il2CppCodeGenWriteBarrier((&___tagID_1), value);
	}

	inline static int32_t get_offset_of_writeState_2() { return static_cast<int32_t>(offsetof(NDEFMessage_t279637043, ___writeState_2)); }
	inline int32_t get_writeState_2() const { return ___writeState_2; }
	inline int32_t* get_address_of_writeState_2() { return &___writeState_2; }
	inline void set_writeState_2(int32_t value)
	{
		___writeState_2 = value;
	}

	inline static int32_t get_offset_of_writeError_3() { return static_cast<int32_t>(offsetof(NDEFMessage_t279637043, ___writeError_3)); }
	inline int32_t get_writeError_3() const { return ___writeError_3; }
	inline int32_t* get_address_of_writeError_3() { return &___writeError_3; }
	inline void set_writeError_3(int32_t value)
	{
		___writeError_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFMESSAGE_T279637043_H
#ifndef NDEFREADRESULT_T3483243621_H
#define NDEFREADRESULT_T3483243621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFReadResult
struct  NDEFReadResult_t3483243621  : public RuntimeObject
{
public:
	// System.Boolean DigitsNFCToolkit.NDEFReadResult::success
	bool ___success_0;
	// DigitsNFCToolkit.NDEFReadError DigitsNFCToolkit.NDEFReadResult::error
	int32_t ___error_1;
	// DigitsNFCToolkit.NDEFMessage DigitsNFCToolkit.NDEFReadResult::message
	NDEFMessage_t279637043 * ___message_2;
	// System.String DigitsNFCToolkit.NDEFReadResult::tagID
	String_t* ___tagID_3;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(NDEFReadResult_t3483243621, ___success_0)); }
	inline bool get_success_0() const { return ___success_0; }
	inline bool* get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(bool value)
	{
		___success_0 = value;
	}

	inline static int32_t get_offset_of_error_1() { return static_cast<int32_t>(offsetof(NDEFReadResult_t3483243621, ___error_1)); }
	inline int32_t get_error_1() const { return ___error_1; }
	inline int32_t* get_address_of_error_1() { return &___error_1; }
	inline void set_error_1(int32_t value)
	{
		___error_1 = value;
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(NDEFReadResult_t3483243621, ___message_2)); }
	inline NDEFMessage_t279637043 * get_message_2() const { return ___message_2; }
	inline NDEFMessage_t279637043 ** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(NDEFMessage_t279637043 * value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_tagID_3() { return static_cast<int32_t>(offsetof(NDEFReadResult_t3483243621, ___tagID_3)); }
	inline String_t* get_tagID_3() const { return ___tagID_3; }
	inline String_t** get_address_of_tagID_3() { return &___tagID_3; }
	inline void set_tagID_3(String_t* value)
	{
		___tagID_3 = value;
		Il2CppCodeGenWriteBarrier((&___tagID_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFREADRESULT_T3483243621_H
#ifndef NDEFRECORD_T2690545556_H
#define NDEFRECORD_T2690545556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFRecord
struct  NDEFRecord_t2690545556  : public RuntimeObject
{
public:
	// DigitsNFCToolkit.NDEFRecordType DigitsNFCToolkit.NDEFRecord::type
	int32_t ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(NDEFRecord_t2690545556, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFRECORD_T2690545556_H
#ifndef NDEFWRITERESULT_T4210562629_H
#define NDEFWRITERESULT_T4210562629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NDEFWriteResult
struct  NDEFWriteResult_t4210562629  : public RuntimeObject
{
public:
	// System.Boolean DigitsNFCToolkit.NDEFWriteResult::success
	bool ___success_0;
	// DigitsNFCToolkit.NDEFWriteError DigitsNFCToolkit.NDEFWriteResult::error
	int32_t ___error_1;
	// DigitsNFCToolkit.NDEFMessage DigitsNFCToolkit.NDEFWriteResult::message
	NDEFMessage_t279637043 * ___message_2;
	// System.String DigitsNFCToolkit.NDEFWriteResult::tagID
	String_t* ___tagID_3;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(NDEFWriteResult_t4210562629, ___success_0)); }
	inline bool get_success_0() const { return ___success_0; }
	inline bool* get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(bool value)
	{
		___success_0 = value;
	}

	inline static int32_t get_offset_of_error_1() { return static_cast<int32_t>(offsetof(NDEFWriteResult_t4210562629, ___error_1)); }
	inline int32_t get_error_1() const { return ___error_1; }
	inline int32_t* get_address_of_error_1() { return &___error_1; }
	inline void set_error_1(int32_t value)
	{
		___error_1 = value;
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(NDEFWriteResult_t4210562629, ___message_2)); }
	inline NDEFMessage_t279637043 * get_message_2() const { return ___message_2; }
	inline NDEFMessage_t279637043 ** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(NDEFMessage_t279637043 * value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_tagID_3() { return static_cast<int32_t>(offsetof(NDEFWriteResult_t4210562629, ___tagID_3)); }
	inline String_t* get_tagID_3() const { return ___tagID_3; }
	inline String_t** get_address_of_tagID_3() { return &___tagID_3; }
	inline void set_tagID_3(String_t* value)
	{
		___tagID_3 = value;
		Il2CppCodeGenWriteBarrier((&___tagID_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDEFWRITERESULT_T4210562629_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef UNIWEBVIEWLOGGER_T952558916_H
#define UNIWEBVIEWLOGGER_T952558916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewLogger
struct  UniWebViewLogger_t952558916  : public RuntimeObject
{
public:
	// UniWebViewLogger/Level UniWebViewLogger::level
	int32_t ___level_1;

public:
	inline static int32_t get_offset_of_level_1() { return static_cast<int32_t>(offsetof(UniWebViewLogger_t952558916, ___level_1)); }
	inline int32_t get_level_1() const { return ___level_1; }
	inline int32_t* get_address_of_level_1() { return &___level_1; }
	inline void set_level_1(int32_t value)
	{
		___level_1 = value;
	}
};

struct UniWebViewLogger_t952558916_StaticFields
{
public:
	// UniWebViewLogger UniWebViewLogger::instance
	UniWebViewLogger_t952558916 * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(UniWebViewLogger_t952558916_StaticFields, ___instance_0)); }
	inline UniWebViewLogger_t952558916 * get_instance_0() const { return ___instance_0; }
	inline UniWebViewLogger_t952558916 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(UniWebViewLogger_t952558916 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWLOGGER_T952558916_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ABSOLUTEURIRECORD_T2525168784_H
#define ABSOLUTEURIRECORD_T2525168784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.AbsoluteUriRecord
struct  AbsoluteUriRecord_t2525168784  : public NDEFRecord_t2690545556
{
public:
	// System.String DigitsNFCToolkit.AbsoluteUriRecord::uri
	String_t* ___uri_1;

public:
	inline static int32_t get_offset_of_uri_1() { return static_cast<int32_t>(offsetof(AbsoluteUriRecord_t2525168784, ___uri_1)); }
	inline String_t* get_uri_1() const { return ___uri_1; }
	inline String_t** get_address_of_uri_1() { return &___uri_1; }
	inline void set_uri_1(String_t* value)
	{
		___uri_1 = value;
		Il2CppCodeGenWriteBarrier((&___uri_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSOLUTEURIRECORD_T2525168784_H
#ifndef EMPTYRECORD_T1486430273_H
#define EMPTYRECORD_T1486430273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.EmptyRecord
struct  EmptyRecord_t1486430273  : public NDEFRecord_t2690545556
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYRECORD_T1486430273_H
#ifndef EXTERNALTYPERECORD_T4087466745_H
#define EXTERNALTYPERECORD_T4087466745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.ExternalTypeRecord
struct  ExternalTypeRecord_t4087466745  : public NDEFRecord_t2690545556
{
public:
	// System.String DigitsNFCToolkit.ExternalTypeRecord::domainName
	String_t* ___domainName_1;
	// System.String DigitsNFCToolkit.ExternalTypeRecord::domainType
	String_t* ___domainType_2;
	// System.Byte[] DigitsNFCToolkit.ExternalTypeRecord::domainData
	ByteU5BU5D_t4116647657* ___domainData_3;

public:
	inline static int32_t get_offset_of_domainName_1() { return static_cast<int32_t>(offsetof(ExternalTypeRecord_t4087466745, ___domainName_1)); }
	inline String_t* get_domainName_1() const { return ___domainName_1; }
	inline String_t** get_address_of_domainName_1() { return &___domainName_1; }
	inline void set_domainName_1(String_t* value)
	{
		___domainName_1 = value;
		Il2CppCodeGenWriteBarrier((&___domainName_1), value);
	}

	inline static int32_t get_offset_of_domainType_2() { return static_cast<int32_t>(offsetof(ExternalTypeRecord_t4087466745, ___domainType_2)); }
	inline String_t* get_domainType_2() const { return ___domainType_2; }
	inline String_t** get_address_of_domainType_2() { return &___domainType_2; }
	inline void set_domainType_2(String_t* value)
	{
		___domainType_2 = value;
		Il2CppCodeGenWriteBarrier((&___domainType_2), value);
	}

	inline static int32_t get_offset_of_domainData_3() { return static_cast<int32_t>(offsetof(ExternalTypeRecord_t4087466745, ___domainData_3)); }
	inline ByteU5BU5D_t4116647657* get_domainData_3() const { return ___domainData_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_domainData_3() { return &___domainData_3; }
	inline void set_domainData_3(ByteU5BU5D_t4116647657* value)
	{
		___domainData_3 = value;
		Il2CppCodeGenWriteBarrier((&___domainData_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNALTYPERECORD_T4087466745_H
#ifndef MIMEMEDIARECORD_T736820488_H
#define MIMEMEDIARECORD_T736820488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.MimeMediaRecord
struct  MimeMediaRecord_t736820488  : public NDEFRecord_t2690545556
{
public:
	// System.String DigitsNFCToolkit.MimeMediaRecord::mimeType
	String_t* ___mimeType_1;
	// System.Byte[] DigitsNFCToolkit.MimeMediaRecord::mimeData
	ByteU5BU5D_t4116647657* ___mimeData_2;

public:
	inline static int32_t get_offset_of_mimeType_1() { return static_cast<int32_t>(offsetof(MimeMediaRecord_t736820488, ___mimeType_1)); }
	inline String_t* get_mimeType_1() const { return ___mimeType_1; }
	inline String_t** get_address_of_mimeType_1() { return &___mimeType_1; }
	inline void set_mimeType_1(String_t* value)
	{
		___mimeType_1 = value;
		Il2CppCodeGenWriteBarrier((&___mimeType_1), value);
	}

	inline static int32_t get_offset_of_mimeData_2() { return static_cast<int32_t>(offsetof(MimeMediaRecord_t736820488, ___mimeData_2)); }
	inline ByteU5BU5D_t4116647657* get_mimeData_2() const { return ___mimeData_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_mimeData_2() { return &___mimeData_2; }
	inline void set_mimeData_2(ByteU5BU5D_t4116647657* value)
	{
		___mimeData_2 = value;
		Il2CppCodeGenWriteBarrier((&___mimeData_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIMEMEDIARECORD_T736820488_H
#ifndef ONNDEFPUSHFINISHED_T4279917764_H
#define ONNDEFPUSHFINISHED_T4279917764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.OnNDEFPushFinished
struct  OnNDEFPushFinished_t4279917764  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONNDEFPUSHFINISHED_T4279917764_H
#ifndef ONNDEFREADFINISHED_T1327886840_H
#define ONNDEFREADFINISHED_T1327886840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.OnNDEFReadFinished
struct  OnNDEFReadFinished_t1327886840  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONNDEFREADFINISHED_T1327886840_H
#ifndef ONNDEFWRITEFINISHED_T4102039599_H
#define ONNDEFWRITEFINISHED_T4102039599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.OnNDEFWriteFinished
struct  OnNDEFWriteFinished_t4102039599  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONNDEFWRITEFINISHED_T4102039599_H
#ifndef ONNFCTAGDETECTED_T3189675727_H
#define ONNFCTAGDETECTED_T3189675727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.OnNFCTagDetected
struct  OnNFCTagDetected_t3189675727  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONNFCTAGDETECTED_T3189675727_H
#ifndef SMARTPOSTERRECORD_T1640848801_H
#define SMARTPOSTERRECORD_T1640848801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.SmartPosterRecord
struct  SmartPosterRecord_t1640848801  : public NDEFRecord_t2690545556
{
public:
	// DigitsNFCToolkit.UriRecord DigitsNFCToolkit.SmartPosterRecord::uriRecord
	UriRecord_t2230063309 * ___uriRecord_1;
	// System.Collections.Generic.List`1<DigitsNFCToolkit.TextRecord> DigitsNFCToolkit.SmartPosterRecord::titleRecords
	List_1_t3785772365 * ___titleRecords_2;
	// System.Collections.Generic.List`1<DigitsNFCToolkit.MimeMediaRecord> DigitsNFCToolkit.SmartPosterRecord::iconRecords
	List_1_t2208895230 * ___iconRecords_3;
	// System.Collections.Generic.List`1<DigitsNFCToolkit.NDEFRecord> DigitsNFCToolkit.SmartPosterRecord::extraRecords
	List_1_t4162620298 * ___extraRecords_4;
	// DigitsNFCToolkit.SmartPosterRecord/RecommendedAction DigitsNFCToolkit.SmartPosterRecord::action
	int32_t ___action_5;
	// System.Int32 DigitsNFCToolkit.SmartPosterRecord::size
	int32_t ___size_6;
	// System.String DigitsNFCToolkit.SmartPosterRecord::mimeType
	String_t* ___mimeType_7;

public:
	inline static int32_t get_offset_of_uriRecord_1() { return static_cast<int32_t>(offsetof(SmartPosterRecord_t1640848801, ___uriRecord_1)); }
	inline UriRecord_t2230063309 * get_uriRecord_1() const { return ___uriRecord_1; }
	inline UriRecord_t2230063309 ** get_address_of_uriRecord_1() { return &___uriRecord_1; }
	inline void set_uriRecord_1(UriRecord_t2230063309 * value)
	{
		___uriRecord_1 = value;
		Il2CppCodeGenWriteBarrier((&___uriRecord_1), value);
	}

	inline static int32_t get_offset_of_titleRecords_2() { return static_cast<int32_t>(offsetof(SmartPosterRecord_t1640848801, ___titleRecords_2)); }
	inline List_1_t3785772365 * get_titleRecords_2() const { return ___titleRecords_2; }
	inline List_1_t3785772365 ** get_address_of_titleRecords_2() { return &___titleRecords_2; }
	inline void set_titleRecords_2(List_1_t3785772365 * value)
	{
		___titleRecords_2 = value;
		Il2CppCodeGenWriteBarrier((&___titleRecords_2), value);
	}

	inline static int32_t get_offset_of_iconRecords_3() { return static_cast<int32_t>(offsetof(SmartPosterRecord_t1640848801, ___iconRecords_3)); }
	inline List_1_t2208895230 * get_iconRecords_3() const { return ___iconRecords_3; }
	inline List_1_t2208895230 ** get_address_of_iconRecords_3() { return &___iconRecords_3; }
	inline void set_iconRecords_3(List_1_t2208895230 * value)
	{
		___iconRecords_3 = value;
		Il2CppCodeGenWriteBarrier((&___iconRecords_3), value);
	}

	inline static int32_t get_offset_of_extraRecords_4() { return static_cast<int32_t>(offsetof(SmartPosterRecord_t1640848801, ___extraRecords_4)); }
	inline List_1_t4162620298 * get_extraRecords_4() const { return ___extraRecords_4; }
	inline List_1_t4162620298 ** get_address_of_extraRecords_4() { return &___extraRecords_4; }
	inline void set_extraRecords_4(List_1_t4162620298 * value)
	{
		___extraRecords_4 = value;
		Il2CppCodeGenWriteBarrier((&___extraRecords_4), value);
	}

	inline static int32_t get_offset_of_action_5() { return static_cast<int32_t>(offsetof(SmartPosterRecord_t1640848801, ___action_5)); }
	inline int32_t get_action_5() const { return ___action_5; }
	inline int32_t* get_address_of_action_5() { return &___action_5; }
	inline void set_action_5(int32_t value)
	{
		___action_5 = value;
	}

	inline static int32_t get_offset_of_size_6() { return static_cast<int32_t>(offsetof(SmartPosterRecord_t1640848801, ___size_6)); }
	inline int32_t get_size_6() const { return ___size_6; }
	inline int32_t* get_address_of_size_6() { return &___size_6; }
	inline void set_size_6(int32_t value)
	{
		___size_6 = value;
	}

	inline static int32_t get_offset_of_mimeType_7() { return static_cast<int32_t>(offsetof(SmartPosterRecord_t1640848801, ___mimeType_7)); }
	inline String_t* get_mimeType_7() const { return ___mimeType_7; }
	inline String_t** get_address_of_mimeType_7() { return &___mimeType_7; }
	inline void set_mimeType_7(String_t* value)
	{
		___mimeType_7 = value;
		Il2CppCodeGenWriteBarrier((&___mimeType_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTPOSTERRECORD_T1640848801_H
#ifndef TEXTRECORD_T2313697623_H
#define TEXTRECORD_T2313697623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.TextRecord
struct  TextRecord_t2313697623  : public NDEFRecord_t2690545556
{
public:
	// System.String DigitsNFCToolkit.TextRecord::text
	String_t* ___text_1;
	// System.String DigitsNFCToolkit.TextRecord::languageCode
	String_t* ___languageCode_2;
	// DigitsNFCToolkit.TextRecord/TextEncoding DigitsNFCToolkit.TextRecord::textEncoding
	int32_t ___textEncoding_3;

public:
	inline static int32_t get_offset_of_text_1() { return static_cast<int32_t>(offsetof(TextRecord_t2313697623, ___text_1)); }
	inline String_t* get_text_1() const { return ___text_1; }
	inline String_t** get_address_of_text_1() { return &___text_1; }
	inline void set_text_1(String_t* value)
	{
		___text_1 = value;
		Il2CppCodeGenWriteBarrier((&___text_1), value);
	}

	inline static int32_t get_offset_of_languageCode_2() { return static_cast<int32_t>(offsetof(TextRecord_t2313697623, ___languageCode_2)); }
	inline String_t* get_languageCode_2() const { return ___languageCode_2; }
	inline String_t** get_address_of_languageCode_2() { return &___languageCode_2; }
	inline void set_languageCode_2(String_t* value)
	{
		___languageCode_2 = value;
		Il2CppCodeGenWriteBarrier((&___languageCode_2), value);
	}

	inline static int32_t get_offset_of_textEncoding_3() { return static_cast<int32_t>(offsetof(TextRecord_t2313697623, ___textEncoding_3)); }
	inline int32_t get_textEncoding_3() const { return ___textEncoding_3; }
	inline int32_t* get_address_of_textEncoding_3() { return &___textEncoding_3; }
	inline void set_textEncoding_3(int32_t value)
	{
		___textEncoding_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRECORD_T2313697623_H
#ifndef UNKNOWNRECORD_T3228240714_H
#define UNKNOWNRECORD_T3228240714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.UnknownRecord
struct  UnknownRecord_t3228240714  : public NDEFRecord_t2690545556
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNKNOWNRECORD_T3228240714_H
#ifndef URIRECORD_T2230063309_H
#define URIRECORD_T2230063309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.UriRecord
struct  UriRecord_t2230063309  : public NDEFRecord_t2690545556
{
public:
	// System.String DigitsNFCToolkit.UriRecord::fullUri
	String_t* ___fullUri_1;
	// System.String DigitsNFCToolkit.UriRecord::uri
	String_t* ___uri_2;
	// System.String DigitsNFCToolkit.UriRecord::protocol
	String_t* ___protocol_3;

public:
	inline static int32_t get_offset_of_fullUri_1() { return static_cast<int32_t>(offsetof(UriRecord_t2230063309, ___fullUri_1)); }
	inline String_t* get_fullUri_1() const { return ___fullUri_1; }
	inline String_t** get_address_of_fullUri_1() { return &___fullUri_1; }
	inline void set_fullUri_1(String_t* value)
	{
		___fullUri_1 = value;
		Il2CppCodeGenWriteBarrier((&___fullUri_1), value);
	}

	inline static int32_t get_offset_of_uri_2() { return static_cast<int32_t>(offsetof(UriRecord_t2230063309, ___uri_2)); }
	inline String_t* get_uri_2() const { return ___uri_2; }
	inline String_t** get_address_of_uri_2() { return &___uri_2; }
	inline void set_uri_2(String_t* value)
	{
		___uri_2 = value;
		Il2CppCodeGenWriteBarrier((&___uri_2), value);
	}

	inline static int32_t get_offset_of_protocol_3() { return static_cast<int32_t>(offsetof(UriRecord_t2230063309, ___protocol_3)); }
	inline String_t* get_protocol_3() const { return ___protocol_3; }
	inline String_t** get_address_of_protocol_3() { return &___protocol_3; }
	inline void set_protocol_3(String_t* value)
	{
		___protocol_3 = value;
		Il2CppCodeGenWriteBarrier((&___protocol_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIRECORD_T2230063309_H
#ifndef KEYCODERECEIVEDDELEGATE_T3839084677_H
#define KEYCODERECEIVEDDELEGATE_T3839084677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/KeyCodeReceivedDelegate
struct  KeyCodeReceivedDelegate_t3839084677  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODERECEIVEDDELEGATE_T3839084677_H
#ifndef MESSAGERECEIVEDDELEGATE_T2288957136_H
#define MESSAGERECEIVEDDELEGATE_T2288957136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/MessageReceivedDelegate
struct  MessageReceivedDelegate_t2288957136  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGERECEIVEDDELEGATE_T2288957136_H
#ifndef OREINTATIONCHANGEDDELEGATE_T1877368362_H
#define OREINTATIONCHANGEDDELEGATE_T1877368362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/OreintationChangedDelegate
struct  OreintationChangedDelegate_t1877368362  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OREINTATIONCHANGEDDELEGATE_T1877368362_H
#ifndef PAGEERRORRECEIVEDDELEGATE_T1724664023_H
#define PAGEERRORRECEIVEDDELEGATE_T1724664023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/PageErrorReceivedDelegate
struct  PageErrorReceivedDelegate_t1724664023  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGEERRORRECEIVEDDELEGATE_T1724664023_H
#ifndef PAGEFINISHEDDELEGATE_T2717015276_H
#define PAGEFINISHEDDELEGATE_T2717015276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/PageFinishedDelegate
struct  PageFinishedDelegate_t2717015276  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGEFINISHEDDELEGATE_T2717015276_H
#ifndef PAGESTARTEDDELEGATE_T2830724344_H
#define PAGESTARTEDDELEGATE_T2830724344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/PageStartedDelegate
struct  PageStartedDelegate_t2830724344  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGESTARTEDDELEGATE_T2830724344_H
#ifndef SHOULDCLOSEDELEGATE_T766319959_H
#define SHOULDCLOSEDELEGATE_T766319959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/ShouldCloseDelegate
struct  ShouldCloseDelegate_t766319959  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOULDCLOSEDELEGATE_T766319959_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef NATIVENFC_T1941597496_H
#define NATIVENFC_T1941597496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NativeNFC
struct  NativeNFC_t1941597496  : public MonoBehaviour_t3962482529
{
public:
	// DigitsNFCToolkit.OnNFCTagDetected DigitsNFCToolkit.NativeNFC::onNFCTagDetected
	OnNFCTagDetected_t3189675727 * ___onNFCTagDetected_4;
	// DigitsNFCToolkit.OnNDEFReadFinished DigitsNFCToolkit.NativeNFC::onNDEFReadFinished
	OnNDEFReadFinished_t1327886840 * ___onNDEFReadFinished_5;
	// DigitsNFCToolkit.OnNDEFWriteFinished DigitsNFCToolkit.NativeNFC::onNDEFWriteFinished
	OnNDEFWriteFinished_t4102039599 * ___onNDEFWriteFinished_6;
	// DigitsNFCToolkit.OnNDEFPushFinished DigitsNFCToolkit.NativeNFC::onNDEFPushFinished
	OnNDEFPushFinished_t4279917764 * ___onNDEFPushFinished_7;
	// System.Boolean DigitsNFCToolkit.NativeNFC::resetOnTimeout
	bool ___resetOnTimeout_8;

public:
	inline static int32_t get_offset_of_onNFCTagDetected_4() { return static_cast<int32_t>(offsetof(NativeNFC_t1941597496, ___onNFCTagDetected_4)); }
	inline OnNFCTagDetected_t3189675727 * get_onNFCTagDetected_4() const { return ___onNFCTagDetected_4; }
	inline OnNFCTagDetected_t3189675727 ** get_address_of_onNFCTagDetected_4() { return &___onNFCTagDetected_4; }
	inline void set_onNFCTagDetected_4(OnNFCTagDetected_t3189675727 * value)
	{
		___onNFCTagDetected_4 = value;
		Il2CppCodeGenWriteBarrier((&___onNFCTagDetected_4), value);
	}

	inline static int32_t get_offset_of_onNDEFReadFinished_5() { return static_cast<int32_t>(offsetof(NativeNFC_t1941597496, ___onNDEFReadFinished_5)); }
	inline OnNDEFReadFinished_t1327886840 * get_onNDEFReadFinished_5() const { return ___onNDEFReadFinished_5; }
	inline OnNDEFReadFinished_t1327886840 ** get_address_of_onNDEFReadFinished_5() { return &___onNDEFReadFinished_5; }
	inline void set_onNDEFReadFinished_5(OnNDEFReadFinished_t1327886840 * value)
	{
		___onNDEFReadFinished_5 = value;
		Il2CppCodeGenWriteBarrier((&___onNDEFReadFinished_5), value);
	}

	inline static int32_t get_offset_of_onNDEFWriteFinished_6() { return static_cast<int32_t>(offsetof(NativeNFC_t1941597496, ___onNDEFWriteFinished_6)); }
	inline OnNDEFWriteFinished_t4102039599 * get_onNDEFWriteFinished_6() const { return ___onNDEFWriteFinished_6; }
	inline OnNDEFWriteFinished_t4102039599 ** get_address_of_onNDEFWriteFinished_6() { return &___onNDEFWriteFinished_6; }
	inline void set_onNDEFWriteFinished_6(OnNDEFWriteFinished_t4102039599 * value)
	{
		___onNDEFWriteFinished_6 = value;
		Il2CppCodeGenWriteBarrier((&___onNDEFWriteFinished_6), value);
	}

	inline static int32_t get_offset_of_onNDEFPushFinished_7() { return static_cast<int32_t>(offsetof(NativeNFC_t1941597496, ___onNDEFPushFinished_7)); }
	inline OnNDEFPushFinished_t4279917764 * get_onNDEFPushFinished_7() const { return ___onNDEFPushFinished_7; }
	inline OnNDEFPushFinished_t4279917764 ** get_address_of_onNDEFPushFinished_7() { return &___onNDEFPushFinished_7; }
	inline void set_onNDEFPushFinished_7(OnNDEFPushFinished_t4279917764 * value)
	{
		___onNDEFPushFinished_7 = value;
		Il2CppCodeGenWriteBarrier((&___onNDEFPushFinished_7), value);
	}

	inline static int32_t get_offset_of_resetOnTimeout_8() { return static_cast<int32_t>(offsetof(NativeNFC_t1941597496, ___resetOnTimeout_8)); }
	inline bool get_resetOnTimeout_8() const { return ___resetOnTimeout_8; }
	inline bool* get_address_of_resetOnTimeout_8() { return &___resetOnTimeout_8; }
	inline void set_resetOnTimeout_8(bool value)
	{
		___resetOnTimeout_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVENFC_T1941597496_H
#ifndef NATIVENFCMANAGER_T351225459_H
#define NATIVENFCMANAGER_T351225459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.NativeNFCManager
struct  NativeNFCManager_t351225459  : public MonoBehaviour_t3962482529
{
public:
	// DigitsNFCToolkit.NativeNFC DigitsNFCToolkit.NativeNFCManager::nfc
	NativeNFC_t1941597496 * ___nfc_6;

public:
	inline static int32_t get_offset_of_nfc_6() { return static_cast<int32_t>(offsetof(NativeNFCManager_t351225459, ___nfc_6)); }
	inline NativeNFC_t1941597496 * get_nfc_6() const { return ___nfc_6; }
	inline NativeNFC_t1941597496 ** get_address_of_nfc_6() { return &___nfc_6; }
	inline void set_nfc_6(NativeNFC_t1941597496 * value)
	{
		___nfc_6 = value;
		Il2CppCodeGenWriteBarrier((&___nfc_6), value);
	}
};

struct NativeNFCManager_t351225459_StaticFields
{
public:
	// DigitsNFCToolkit.NativeNFCManager DigitsNFCToolkit.NativeNFCManager::instance
	NativeNFCManager_t351225459 * ___instance_5;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(NativeNFCManager_t351225459_StaticFields, ___instance_5)); }
	inline NativeNFCManager_t351225459 * get_instance_5() const { return ___instance_5; }
	inline NativeNFCManager_t351225459 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(NativeNFCManager_t351225459 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVENFCMANAGER_T351225459_H
#ifndef MESSAGESCREENVIEW_T146641597_H
#define MESSAGESCREENVIEW_T146641597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.MessageScreenView
struct  MessageScreenView_t146641597  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform DigitsNFCToolkit.Samples.MessageScreenView::writeMessageBox
	RectTransform_t3704657025 * ___writeMessageBox_4;
	// UnityEngine.UI.Text DigitsNFCToolkit.Samples.MessageScreenView::writeLabel
	Text_t1901882714 * ___writeLabel_5;
	// UnityEngine.UI.Button DigitsNFCToolkit.Samples.MessageScreenView::writeCancelButton
	Button_t4055032469 * ___writeCancelButton_6;
	// UnityEngine.UI.Button DigitsNFCToolkit.Samples.MessageScreenView::writeOKButton
	Button_t4055032469 * ___writeOKButton_7;
	// UnityEngine.RectTransform DigitsNFCToolkit.Samples.MessageScreenView::pushMessageBox
	RectTransform_t3704657025 * ___pushMessageBox_8;
	// UnityEngine.UI.Text DigitsNFCToolkit.Samples.MessageScreenView::pushLabel
	Text_t1901882714 * ___pushLabel_9;
	// UnityEngine.UI.Button DigitsNFCToolkit.Samples.MessageScreenView::pushCancelButton
	Button_t4055032469 * ___pushCancelButton_10;
	// UnityEngine.UI.Button DigitsNFCToolkit.Samples.MessageScreenView::pushOKButton
	Button_t4055032469 * ___pushOKButton_11;
	// System.Boolean DigitsNFCToolkit.Samples.MessageScreenView::initialized
	bool ___initialized_12;

public:
	inline static int32_t get_offset_of_writeMessageBox_4() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___writeMessageBox_4)); }
	inline RectTransform_t3704657025 * get_writeMessageBox_4() const { return ___writeMessageBox_4; }
	inline RectTransform_t3704657025 ** get_address_of_writeMessageBox_4() { return &___writeMessageBox_4; }
	inline void set_writeMessageBox_4(RectTransform_t3704657025 * value)
	{
		___writeMessageBox_4 = value;
		Il2CppCodeGenWriteBarrier((&___writeMessageBox_4), value);
	}

	inline static int32_t get_offset_of_writeLabel_5() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___writeLabel_5)); }
	inline Text_t1901882714 * get_writeLabel_5() const { return ___writeLabel_5; }
	inline Text_t1901882714 ** get_address_of_writeLabel_5() { return &___writeLabel_5; }
	inline void set_writeLabel_5(Text_t1901882714 * value)
	{
		___writeLabel_5 = value;
		Il2CppCodeGenWriteBarrier((&___writeLabel_5), value);
	}

	inline static int32_t get_offset_of_writeCancelButton_6() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___writeCancelButton_6)); }
	inline Button_t4055032469 * get_writeCancelButton_6() const { return ___writeCancelButton_6; }
	inline Button_t4055032469 ** get_address_of_writeCancelButton_6() { return &___writeCancelButton_6; }
	inline void set_writeCancelButton_6(Button_t4055032469 * value)
	{
		___writeCancelButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___writeCancelButton_6), value);
	}

	inline static int32_t get_offset_of_writeOKButton_7() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___writeOKButton_7)); }
	inline Button_t4055032469 * get_writeOKButton_7() const { return ___writeOKButton_7; }
	inline Button_t4055032469 ** get_address_of_writeOKButton_7() { return &___writeOKButton_7; }
	inline void set_writeOKButton_7(Button_t4055032469 * value)
	{
		___writeOKButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___writeOKButton_7), value);
	}

	inline static int32_t get_offset_of_pushMessageBox_8() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___pushMessageBox_8)); }
	inline RectTransform_t3704657025 * get_pushMessageBox_8() const { return ___pushMessageBox_8; }
	inline RectTransform_t3704657025 ** get_address_of_pushMessageBox_8() { return &___pushMessageBox_8; }
	inline void set_pushMessageBox_8(RectTransform_t3704657025 * value)
	{
		___pushMessageBox_8 = value;
		Il2CppCodeGenWriteBarrier((&___pushMessageBox_8), value);
	}

	inline static int32_t get_offset_of_pushLabel_9() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___pushLabel_9)); }
	inline Text_t1901882714 * get_pushLabel_9() const { return ___pushLabel_9; }
	inline Text_t1901882714 ** get_address_of_pushLabel_9() { return &___pushLabel_9; }
	inline void set_pushLabel_9(Text_t1901882714 * value)
	{
		___pushLabel_9 = value;
		Il2CppCodeGenWriteBarrier((&___pushLabel_9), value);
	}

	inline static int32_t get_offset_of_pushCancelButton_10() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___pushCancelButton_10)); }
	inline Button_t4055032469 * get_pushCancelButton_10() const { return ___pushCancelButton_10; }
	inline Button_t4055032469 ** get_address_of_pushCancelButton_10() { return &___pushCancelButton_10; }
	inline void set_pushCancelButton_10(Button_t4055032469 * value)
	{
		___pushCancelButton_10 = value;
		Il2CppCodeGenWriteBarrier((&___pushCancelButton_10), value);
	}

	inline static int32_t get_offset_of_pushOKButton_11() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___pushOKButton_11)); }
	inline Button_t4055032469 * get_pushOKButton_11() const { return ___pushOKButton_11; }
	inline Button_t4055032469 ** get_address_of_pushOKButton_11() { return &___pushOKButton_11; }
	inline void set_pushOKButton_11(Button_t4055032469 * value)
	{
		___pushOKButton_11 = value;
		Il2CppCodeGenWriteBarrier((&___pushOKButton_11), value);
	}

	inline static int32_t get_offset_of_initialized_12() { return static_cast<int32_t>(offsetof(MessageScreenView_t146641597, ___initialized_12)); }
	inline bool get_initialized_12() const { return ___initialized_12; }
	inline bool* get_address_of_initialized_12() { return &___initialized_12; }
	inline void set_initialized_12(bool value)
	{
		___initialized_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGESCREENVIEW_T146641597_H
#ifndef NAVIGATIONMANAGER_T1939391727_H
#define NAVIGATIONMANAGER_T1939391727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.NavigationManager
struct  NavigationManager_t1939391727  : public MonoBehaviour_t3962482529
{
public:
	// DigitsNFCToolkit.Samples.ReadScreenControl DigitsNFCToolkit.Samples.NavigationManager::readScreenControl
	ReadScreenControl_t3483866810 * ___readScreenControl_4;
	// DigitsNFCToolkit.Samples.WriteScreenControl DigitsNFCToolkit.Samples.NavigationManager::writeScreenControl
	WriteScreenControl_t1506090515 * ___writeScreenControl_5;

public:
	inline static int32_t get_offset_of_readScreenControl_4() { return static_cast<int32_t>(offsetof(NavigationManager_t1939391727, ___readScreenControl_4)); }
	inline ReadScreenControl_t3483866810 * get_readScreenControl_4() const { return ___readScreenControl_4; }
	inline ReadScreenControl_t3483866810 ** get_address_of_readScreenControl_4() { return &___readScreenControl_4; }
	inline void set_readScreenControl_4(ReadScreenControl_t3483866810 * value)
	{
		___readScreenControl_4 = value;
		Il2CppCodeGenWriteBarrier((&___readScreenControl_4), value);
	}

	inline static int32_t get_offset_of_writeScreenControl_5() { return static_cast<int32_t>(offsetof(NavigationManager_t1939391727, ___writeScreenControl_5)); }
	inline WriteScreenControl_t1506090515 * get_writeScreenControl_5() const { return ___writeScreenControl_5; }
	inline WriteScreenControl_t1506090515 ** get_address_of_writeScreenControl_5() { return &___writeScreenControl_5; }
	inline void set_writeScreenControl_5(WriteScreenControl_t1506090515 * value)
	{
		___writeScreenControl_5 = value;
		Il2CppCodeGenWriteBarrier((&___writeScreenControl_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVIGATIONMANAGER_T1939391727_H
#ifndef READSCREENCONTROL_T3483866810_H
#define READSCREENCONTROL_T3483866810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.ReadScreenControl
struct  ReadScreenControl_t3483866810  : public MonoBehaviour_t3962482529
{
public:
	// DigitsNFCToolkit.Samples.ReadScreenView DigitsNFCToolkit.Samples.ReadScreenControl::view
	ReadScreenView_t239900869 * ___view_4;

public:
	inline static int32_t get_offset_of_view_4() { return static_cast<int32_t>(offsetof(ReadScreenControl_t3483866810, ___view_4)); }
	inline ReadScreenView_t239900869 * get_view_4() const { return ___view_4; }
	inline ReadScreenView_t239900869 ** get_address_of_view_4() { return &___view_4; }
	inline void set_view_4(ReadScreenView_t239900869 * value)
	{
		___view_4 = value;
		Il2CppCodeGenWriteBarrier((&___view_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSCREENCONTROL_T3483866810_H
#ifndef READSCREENVIEW_T239900869_H
#define READSCREENVIEW_T239900869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.ReadScreenView
struct  ReadScreenView_t239900869  : public MonoBehaviour_t3962482529
{
public:
	// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.ReadScreenView::textRecordItemPrefab
	RecordItem_t1075151419 * ___textRecordItemPrefab_11;
	// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.ReadScreenView::uriRecordItemPrefab
	RecordItem_t1075151419 * ___uriRecordItemPrefab_12;
	// DigitsNFCToolkit.Samples.ImageRecordItem DigitsNFCToolkit.Samples.ReadScreenView::mimeMediaRecordItemPrefab
	ImageRecordItem_t2104316047 * ___mimeMediaRecordItemPrefab_13;
	// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.ReadScreenView::externalTypeRecordItemPrefab
	RecordItem_t1075151419 * ___externalTypeRecordItemPrefab_14;
	// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.ReadScreenView::smartPosterRecordItemPrefab
	RecordItem_t1075151419 * ___smartPosterRecordItemPrefab_15;
	// UnityEngine.RectTransform DigitsNFCToolkit.Samples.ReadScreenView::tagInfoTransform
	RectTransform_t3704657025 * ___tagInfoTransform_16;
	// UnityEngine.RectTransform DigitsNFCToolkit.Samples.ReadScreenView::iOSReadTransform
	RectTransform_t3704657025 * ___iOSReadTransform_17;
	// UnityEngine.UI.Text DigitsNFCToolkit.Samples.ReadScreenView::tagInfoContentLabel
	Text_t1901882714 * ___tagInfoContentLabel_18;
	// UnityEngine.UI.ScrollRect DigitsNFCToolkit.Samples.ReadScreenView::ndefMessageScrollRect
	ScrollRect_t4137855814 * ___ndefMessageScrollRect_19;

public:
	inline static int32_t get_offset_of_textRecordItemPrefab_11() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___textRecordItemPrefab_11)); }
	inline RecordItem_t1075151419 * get_textRecordItemPrefab_11() const { return ___textRecordItemPrefab_11; }
	inline RecordItem_t1075151419 ** get_address_of_textRecordItemPrefab_11() { return &___textRecordItemPrefab_11; }
	inline void set_textRecordItemPrefab_11(RecordItem_t1075151419 * value)
	{
		___textRecordItemPrefab_11 = value;
		Il2CppCodeGenWriteBarrier((&___textRecordItemPrefab_11), value);
	}

	inline static int32_t get_offset_of_uriRecordItemPrefab_12() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___uriRecordItemPrefab_12)); }
	inline RecordItem_t1075151419 * get_uriRecordItemPrefab_12() const { return ___uriRecordItemPrefab_12; }
	inline RecordItem_t1075151419 ** get_address_of_uriRecordItemPrefab_12() { return &___uriRecordItemPrefab_12; }
	inline void set_uriRecordItemPrefab_12(RecordItem_t1075151419 * value)
	{
		___uriRecordItemPrefab_12 = value;
		Il2CppCodeGenWriteBarrier((&___uriRecordItemPrefab_12), value);
	}

	inline static int32_t get_offset_of_mimeMediaRecordItemPrefab_13() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___mimeMediaRecordItemPrefab_13)); }
	inline ImageRecordItem_t2104316047 * get_mimeMediaRecordItemPrefab_13() const { return ___mimeMediaRecordItemPrefab_13; }
	inline ImageRecordItem_t2104316047 ** get_address_of_mimeMediaRecordItemPrefab_13() { return &___mimeMediaRecordItemPrefab_13; }
	inline void set_mimeMediaRecordItemPrefab_13(ImageRecordItem_t2104316047 * value)
	{
		___mimeMediaRecordItemPrefab_13 = value;
		Il2CppCodeGenWriteBarrier((&___mimeMediaRecordItemPrefab_13), value);
	}

	inline static int32_t get_offset_of_externalTypeRecordItemPrefab_14() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___externalTypeRecordItemPrefab_14)); }
	inline RecordItem_t1075151419 * get_externalTypeRecordItemPrefab_14() const { return ___externalTypeRecordItemPrefab_14; }
	inline RecordItem_t1075151419 ** get_address_of_externalTypeRecordItemPrefab_14() { return &___externalTypeRecordItemPrefab_14; }
	inline void set_externalTypeRecordItemPrefab_14(RecordItem_t1075151419 * value)
	{
		___externalTypeRecordItemPrefab_14 = value;
		Il2CppCodeGenWriteBarrier((&___externalTypeRecordItemPrefab_14), value);
	}

	inline static int32_t get_offset_of_smartPosterRecordItemPrefab_15() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___smartPosterRecordItemPrefab_15)); }
	inline RecordItem_t1075151419 * get_smartPosterRecordItemPrefab_15() const { return ___smartPosterRecordItemPrefab_15; }
	inline RecordItem_t1075151419 ** get_address_of_smartPosterRecordItemPrefab_15() { return &___smartPosterRecordItemPrefab_15; }
	inline void set_smartPosterRecordItemPrefab_15(RecordItem_t1075151419 * value)
	{
		___smartPosterRecordItemPrefab_15 = value;
		Il2CppCodeGenWriteBarrier((&___smartPosterRecordItemPrefab_15), value);
	}

	inline static int32_t get_offset_of_tagInfoTransform_16() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___tagInfoTransform_16)); }
	inline RectTransform_t3704657025 * get_tagInfoTransform_16() const { return ___tagInfoTransform_16; }
	inline RectTransform_t3704657025 ** get_address_of_tagInfoTransform_16() { return &___tagInfoTransform_16; }
	inline void set_tagInfoTransform_16(RectTransform_t3704657025 * value)
	{
		___tagInfoTransform_16 = value;
		Il2CppCodeGenWriteBarrier((&___tagInfoTransform_16), value);
	}

	inline static int32_t get_offset_of_iOSReadTransform_17() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___iOSReadTransform_17)); }
	inline RectTransform_t3704657025 * get_iOSReadTransform_17() const { return ___iOSReadTransform_17; }
	inline RectTransform_t3704657025 ** get_address_of_iOSReadTransform_17() { return &___iOSReadTransform_17; }
	inline void set_iOSReadTransform_17(RectTransform_t3704657025 * value)
	{
		___iOSReadTransform_17 = value;
		Il2CppCodeGenWriteBarrier((&___iOSReadTransform_17), value);
	}

	inline static int32_t get_offset_of_tagInfoContentLabel_18() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___tagInfoContentLabel_18)); }
	inline Text_t1901882714 * get_tagInfoContentLabel_18() const { return ___tagInfoContentLabel_18; }
	inline Text_t1901882714 ** get_address_of_tagInfoContentLabel_18() { return &___tagInfoContentLabel_18; }
	inline void set_tagInfoContentLabel_18(Text_t1901882714 * value)
	{
		___tagInfoContentLabel_18 = value;
		Il2CppCodeGenWriteBarrier((&___tagInfoContentLabel_18), value);
	}

	inline static int32_t get_offset_of_ndefMessageScrollRect_19() { return static_cast<int32_t>(offsetof(ReadScreenView_t239900869, ___ndefMessageScrollRect_19)); }
	inline ScrollRect_t4137855814 * get_ndefMessageScrollRect_19() const { return ___ndefMessageScrollRect_19; }
	inline ScrollRect_t4137855814 ** get_address_of_ndefMessageScrollRect_19() { return &___ndefMessageScrollRect_19; }
	inline void set_ndefMessageScrollRect_19(ScrollRect_t4137855814 * value)
	{
		___ndefMessageScrollRect_19 = value;
		Il2CppCodeGenWriteBarrier((&___ndefMessageScrollRect_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSCREENVIEW_T239900869_H
#ifndef RECORDITEM_T1075151419_H
#define RECORDITEM_T1075151419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.RecordItem
struct  RecordItem_t1075151419  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform DigitsNFCToolkit.Samples.RecordItem::<RectTransform>k__BackingField
	RectTransform_t3704657025 * ___U3CRectTransformU3Ek__BackingField_4;
	// UnityEngine.UI.Text DigitsNFCToolkit.Samples.RecordItem::label
	Text_t1901882714 * ___label_5;

public:
	inline static int32_t get_offset_of_U3CRectTransformU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RecordItem_t1075151419, ___U3CRectTransformU3Ek__BackingField_4)); }
	inline RectTransform_t3704657025 * get_U3CRectTransformU3Ek__BackingField_4() const { return ___U3CRectTransformU3Ek__BackingField_4; }
	inline RectTransform_t3704657025 ** get_address_of_U3CRectTransformU3Ek__BackingField_4() { return &___U3CRectTransformU3Ek__BackingField_4; }
	inline void set_U3CRectTransformU3Ek__BackingField_4(RectTransform_t3704657025 * value)
	{
		___U3CRectTransformU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRectTransformU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_label_5() { return static_cast<int32_t>(offsetof(RecordItem_t1075151419, ___label_5)); }
	inline Text_t1901882714 * get_label_5() const { return ___label_5; }
	inline Text_t1901882714 ** get_address_of_label_5() { return &___label_5; }
	inline void set_label_5(Text_t1901882714 * value)
	{
		___label_5 = value;
		Il2CppCodeGenWriteBarrier((&___label_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECORDITEM_T1075151419_H
#ifndef WRITESCREENCONTROL_T1506090515_H
#define WRITESCREENCONTROL_T1506090515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.WriteScreenControl
struct  WriteScreenControl_t1506090515  : public MonoBehaviour_t3962482529
{
public:
	// DigitsNFCToolkit.Samples.WriteScreenView DigitsNFCToolkit.Samples.WriteScreenControl::view
	WriteScreenView_t2350253495 * ___view_4;
	// DigitsNFCToolkit.Samples.MessageScreenView DigitsNFCToolkit.Samples.WriteScreenControl::messageScreenView
	MessageScreenView_t146641597 * ___messageScreenView_5;
	// UnityEngine.Sprite DigitsNFCToolkit.Samples.WriteScreenControl::musicNoteIcon
	Sprite_t280657092 * ___musicNoteIcon_6;
	// UnityEngine.Sprite DigitsNFCToolkit.Samples.WriteScreenControl::faceIcon
	Sprite_t280657092 * ___faceIcon_7;
	// UnityEngine.Sprite DigitsNFCToolkit.Samples.WriteScreenControl::arrowIcon
	Sprite_t280657092 * ___arrowIcon_8;
	// DigitsNFCToolkit.NDEFMessage DigitsNFCToolkit.Samples.WriteScreenControl::pendingMessage
	NDEFMessage_t279637043 * ___pendingMessage_9;

public:
	inline static int32_t get_offset_of_view_4() { return static_cast<int32_t>(offsetof(WriteScreenControl_t1506090515, ___view_4)); }
	inline WriteScreenView_t2350253495 * get_view_4() const { return ___view_4; }
	inline WriteScreenView_t2350253495 ** get_address_of_view_4() { return &___view_4; }
	inline void set_view_4(WriteScreenView_t2350253495 * value)
	{
		___view_4 = value;
		Il2CppCodeGenWriteBarrier((&___view_4), value);
	}

	inline static int32_t get_offset_of_messageScreenView_5() { return static_cast<int32_t>(offsetof(WriteScreenControl_t1506090515, ___messageScreenView_5)); }
	inline MessageScreenView_t146641597 * get_messageScreenView_5() const { return ___messageScreenView_5; }
	inline MessageScreenView_t146641597 ** get_address_of_messageScreenView_5() { return &___messageScreenView_5; }
	inline void set_messageScreenView_5(MessageScreenView_t146641597 * value)
	{
		___messageScreenView_5 = value;
		Il2CppCodeGenWriteBarrier((&___messageScreenView_5), value);
	}

	inline static int32_t get_offset_of_musicNoteIcon_6() { return static_cast<int32_t>(offsetof(WriteScreenControl_t1506090515, ___musicNoteIcon_6)); }
	inline Sprite_t280657092 * get_musicNoteIcon_6() const { return ___musicNoteIcon_6; }
	inline Sprite_t280657092 ** get_address_of_musicNoteIcon_6() { return &___musicNoteIcon_6; }
	inline void set_musicNoteIcon_6(Sprite_t280657092 * value)
	{
		___musicNoteIcon_6 = value;
		Il2CppCodeGenWriteBarrier((&___musicNoteIcon_6), value);
	}

	inline static int32_t get_offset_of_faceIcon_7() { return static_cast<int32_t>(offsetof(WriteScreenControl_t1506090515, ___faceIcon_7)); }
	inline Sprite_t280657092 * get_faceIcon_7() const { return ___faceIcon_7; }
	inline Sprite_t280657092 ** get_address_of_faceIcon_7() { return &___faceIcon_7; }
	inline void set_faceIcon_7(Sprite_t280657092 * value)
	{
		___faceIcon_7 = value;
		Il2CppCodeGenWriteBarrier((&___faceIcon_7), value);
	}

	inline static int32_t get_offset_of_arrowIcon_8() { return static_cast<int32_t>(offsetof(WriteScreenControl_t1506090515, ___arrowIcon_8)); }
	inline Sprite_t280657092 * get_arrowIcon_8() const { return ___arrowIcon_8; }
	inline Sprite_t280657092 ** get_address_of_arrowIcon_8() { return &___arrowIcon_8; }
	inline void set_arrowIcon_8(Sprite_t280657092 * value)
	{
		___arrowIcon_8 = value;
		Il2CppCodeGenWriteBarrier((&___arrowIcon_8), value);
	}

	inline static int32_t get_offset_of_pendingMessage_9() { return static_cast<int32_t>(offsetof(WriteScreenControl_t1506090515, ___pendingMessage_9)); }
	inline NDEFMessage_t279637043 * get_pendingMessage_9() const { return ___pendingMessage_9; }
	inline NDEFMessage_t279637043 ** get_address_of_pendingMessage_9() { return &___pendingMessage_9; }
	inline void set_pendingMessage_9(NDEFMessage_t279637043 * value)
	{
		___pendingMessage_9 = value;
		Il2CppCodeGenWriteBarrier((&___pendingMessage_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESCREENCONTROL_T1506090515_H
#ifndef WRITESCREENVIEW_T2350253495_H
#define WRITESCREENVIEW_T2350253495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.WriteScreenView
struct  WriteScreenView_t2350253495  : public MonoBehaviour_t3962482529
{
public:
	// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.WriteScreenView::textRecordItemPrefab
	RecordItem_t1075151419 * ___textRecordItemPrefab_10;
	// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.WriteScreenView::uriRecordItemPrefab
	RecordItem_t1075151419 * ___uriRecordItemPrefab_11;
	// DigitsNFCToolkit.Samples.ImageRecordItem DigitsNFCToolkit.Samples.WriteScreenView::mimeMediaRecordItemPrefab
	ImageRecordItem_t2104316047 * ___mimeMediaRecordItemPrefab_12;
	// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.WriteScreenView::externalTypeRecordItemPrefab
	RecordItem_t1075151419 * ___externalTypeRecordItemPrefab_13;
	// DigitsNFCToolkit.Samples.RecordItem DigitsNFCToolkit.Samples.WriteScreenView::smartPosterRecordItemPrefab
	RecordItem_t1075151419 * ___smartPosterRecordItemPrefab_14;
	// UnityEngine.UI.ScrollRect DigitsNFCToolkit.Samples.WriteScreenView::ndefMessageScrollRect
	ScrollRect_t4137855814 * ___ndefMessageScrollRect_15;
	// UnityEngine.UI.Text DigitsNFCToolkit.Samples.WriteScreenView::titleLabel
	Text_t1901882714 * ___titleLabel_16;
	// UnityEngine.UI.Dropdown DigitsNFCToolkit.Samples.WriteScreenView::typeDropdown
	Dropdown_t2274391225 * ___typeDropdown_17;
	// UnityEngine.Transform DigitsNFCToolkit.Samples.WriteScreenView::textRecordTransform
	Transform_t3600365921 * ___textRecordTransform_18;
	// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::textInput
	InputField_t3762917431 * ___textInput_19;
	// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::languageCodeInput
	InputField_t3762917431 * ___languageCodeInput_20;
	// UnityEngine.UI.Dropdown DigitsNFCToolkit.Samples.WriteScreenView::textEncodingDropdown
	Dropdown_t2274391225 * ___textEncodingDropdown_21;
	// UnityEngine.UI.Button DigitsNFCToolkit.Samples.WriteScreenView::addRecordButton
	Button_t4055032469 * ___addRecordButton_22;
	// UnityEngine.Transform DigitsNFCToolkit.Samples.WriteScreenView::uriRecordTransform
	Transform_t3600365921 * ___uriRecordTransform_23;
	// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::uriInput
	InputField_t3762917431 * ___uriInput_24;
	// UnityEngine.Transform DigitsNFCToolkit.Samples.WriteScreenView::mimeMediaRecordTransform
	Transform_t3600365921 * ___mimeMediaRecordTransform_25;
	// UnityEngine.UI.Dropdown DigitsNFCToolkit.Samples.WriteScreenView::iconDropdown
	Dropdown_t2274391225 * ___iconDropdown_26;
	// UnityEngine.UI.Text DigitsNFCToolkit.Samples.WriteScreenView::mimeTypeLabel
	Text_t1901882714 * ___mimeTypeLabel_27;
	// UnityEngine.Transform DigitsNFCToolkit.Samples.WriteScreenView::externalTypeRecordTransform
	Transform_t3600365921 * ___externalTypeRecordTransform_28;
	// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::domainNameInput
	InputField_t3762917431 * ___domainNameInput_29;
	// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::domainTypeInput
	InputField_t3762917431 * ___domainTypeInput_30;
	// UnityEngine.UI.InputField DigitsNFCToolkit.Samples.WriteScreenView::domainDataInput
	InputField_t3762917431 * ___domainDataInput_31;
	// UnityEngine.UI.Button DigitsNFCToolkit.Samples.WriteScreenView::clearButton
	Button_t4055032469 * ___clearButton_32;
	// UnityEngine.UI.Button DigitsNFCToolkit.Samples.WriteScreenView::writeButton
	Button_t4055032469 * ___writeButton_33;

public:
	inline static int32_t get_offset_of_textRecordItemPrefab_10() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___textRecordItemPrefab_10)); }
	inline RecordItem_t1075151419 * get_textRecordItemPrefab_10() const { return ___textRecordItemPrefab_10; }
	inline RecordItem_t1075151419 ** get_address_of_textRecordItemPrefab_10() { return &___textRecordItemPrefab_10; }
	inline void set_textRecordItemPrefab_10(RecordItem_t1075151419 * value)
	{
		___textRecordItemPrefab_10 = value;
		Il2CppCodeGenWriteBarrier((&___textRecordItemPrefab_10), value);
	}

	inline static int32_t get_offset_of_uriRecordItemPrefab_11() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___uriRecordItemPrefab_11)); }
	inline RecordItem_t1075151419 * get_uriRecordItemPrefab_11() const { return ___uriRecordItemPrefab_11; }
	inline RecordItem_t1075151419 ** get_address_of_uriRecordItemPrefab_11() { return &___uriRecordItemPrefab_11; }
	inline void set_uriRecordItemPrefab_11(RecordItem_t1075151419 * value)
	{
		___uriRecordItemPrefab_11 = value;
		Il2CppCodeGenWriteBarrier((&___uriRecordItemPrefab_11), value);
	}

	inline static int32_t get_offset_of_mimeMediaRecordItemPrefab_12() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___mimeMediaRecordItemPrefab_12)); }
	inline ImageRecordItem_t2104316047 * get_mimeMediaRecordItemPrefab_12() const { return ___mimeMediaRecordItemPrefab_12; }
	inline ImageRecordItem_t2104316047 ** get_address_of_mimeMediaRecordItemPrefab_12() { return &___mimeMediaRecordItemPrefab_12; }
	inline void set_mimeMediaRecordItemPrefab_12(ImageRecordItem_t2104316047 * value)
	{
		___mimeMediaRecordItemPrefab_12 = value;
		Il2CppCodeGenWriteBarrier((&___mimeMediaRecordItemPrefab_12), value);
	}

	inline static int32_t get_offset_of_externalTypeRecordItemPrefab_13() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___externalTypeRecordItemPrefab_13)); }
	inline RecordItem_t1075151419 * get_externalTypeRecordItemPrefab_13() const { return ___externalTypeRecordItemPrefab_13; }
	inline RecordItem_t1075151419 ** get_address_of_externalTypeRecordItemPrefab_13() { return &___externalTypeRecordItemPrefab_13; }
	inline void set_externalTypeRecordItemPrefab_13(RecordItem_t1075151419 * value)
	{
		___externalTypeRecordItemPrefab_13 = value;
		Il2CppCodeGenWriteBarrier((&___externalTypeRecordItemPrefab_13), value);
	}

	inline static int32_t get_offset_of_smartPosterRecordItemPrefab_14() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___smartPosterRecordItemPrefab_14)); }
	inline RecordItem_t1075151419 * get_smartPosterRecordItemPrefab_14() const { return ___smartPosterRecordItemPrefab_14; }
	inline RecordItem_t1075151419 ** get_address_of_smartPosterRecordItemPrefab_14() { return &___smartPosterRecordItemPrefab_14; }
	inline void set_smartPosterRecordItemPrefab_14(RecordItem_t1075151419 * value)
	{
		___smartPosterRecordItemPrefab_14 = value;
		Il2CppCodeGenWriteBarrier((&___smartPosterRecordItemPrefab_14), value);
	}

	inline static int32_t get_offset_of_ndefMessageScrollRect_15() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___ndefMessageScrollRect_15)); }
	inline ScrollRect_t4137855814 * get_ndefMessageScrollRect_15() const { return ___ndefMessageScrollRect_15; }
	inline ScrollRect_t4137855814 ** get_address_of_ndefMessageScrollRect_15() { return &___ndefMessageScrollRect_15; }
	inline void set_ndefMessageScrollRect_15(ScrollRect_t4137855814 * value)
	{
		___ndefMessageScrollRect_15 = value;
		Il2CppCodeGenWriteBarrier((&___ndefMessageScrollRect_15), value);
	}

	inline static int32_t get_offset_of_titleLabel_16() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___titleLabel_16)); }
	inline Text_t1901882714 * get_titleLabel_16() const { return ___titleLabel_16; }
	inline Text_t1901882714 ** get_address_of_titleLabel_16() { return &___titleLabel_16; }
	inline void set_titleLabel_16(Text_t1901882714 * value)
	{
		___titleLabel_16 = value;
		Il2CppCodeGenWriteBarrier((&___titleLabel_16), value);
	}

	inline static int32_t get_offset_of_typeDropdown_17() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___typeDropdown_17)); }
	inline Dropdown_t2274391225 * get_typeDropdown_17() const { return ___typeDropdown_17; }
	inline Dropdown_t2274391225 ** get_address_of_typeDropdown_17() { return &___typeDropdown_17; }
	inline void set_typeDropdown_17(Dropdown_t2274391225 * value)
	{
		___typeDropdown_17 = value;
		Il2CppCodeGenWriteBarrier((&___typeDropdown_17), value);
	}

	inline static int32_t get_offset_of_textRecordTransform_18() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___textRecordTransform_18)); }
	inline Transform_t3600365921 * get_textRecordTransform_18() const { return ___textRecordTransform_18; }
	inline Transform_t3600365921 ** get_address_of_textRecordTransform_18() { return &___textRecordTransform_18; }
	inline void set_textRecordTransform_18(Transform_t3600365921 * value)
	{
		___textRecordTransform_18 = value;
		Il2CppCodeGenWriteBarrier((&___textRecordTransform_18), value);
	}

	inline static int32_t get_offset_of_textInput_19() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___textInput_19)); }
	inline InputField_t3762917431 * get_textInput_19() const { return ___textInput_19; }
	inline InputField_t3762917431 ** get_address_of_textInput_19() { return &___textInput_19; }
	inline void set_textInput_19(InputField_t3762917431 * value)
	{
		___textInput_19 = value;
		Il2CppCodeGenWriteBarrier((&___textInput_19), value);
	}

	inline static int32_t get_offset_of_languageCodeInput_20() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___languageCodeInput_20)); }
	inline InputField_t3762917431 * get_languageCodeInput_20() const { return ___languageCodeInput_20; }
	inline InputField_t3762917431 ** get_address_of_languageCodeInput_20() { return &___languageCodeInput_20; }
	inline void set_languageCodeInput_20(InputField_t3762917431 * value)
	{
		___languageCodeInput_20 = value;
		Il2CppCodeGenWriteBarrier((&___languageCodeInput_20), value);
	}

	inline static int32_t get_offset_of_textEncodingDropdown_21() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___textEncodingDropdown_21)); }
	inline Dropdown_t2274391225 * get_textEncodingDropdown_21() const { return ___textEncodingDropdown_21; }
	inline Dropdown_t2274391225 ** get_address_of_textEncodingDropdown_21() { return &___textEncodingDropdown_21; }
	inline void set_textEncodingDropdown_21(Dropdown_t2274391225 * value)
	{
		___textEncodingDropdown_21 = value;
		Il2CppCodeGenWriteBarrier((&___textEncodingDropdown_21), value);
	}

	inline static int32_t get_offset_of_addRecordButton_22() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___addRecordButton_22)); }
	inline Button_t4055032469 * get_addRecordButton_22() const { return ___addRecordButton_22; }
	inline Button_t4055032469 ** get_address_of_addRecordButton_22() { return &___addRecordButton_22; }
	inline void set_addRecordButton_22(Button_t4055032469 * value)
	{
		___addRecordButton_22 = value;
		Il2CppCodeGenWriteBarrier((&___addRecordButton_22), value);
	}

	inline static int32_t get_offset_of_uriRecordTransform_23() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___uriRecordTransform_23)); }
	inline Transform_t3600365921 * get_uriRecordTransform_23() const { return ___uriRecordTransform_23; }
	inline Transform_t3600365921 ** get_address_of_uriRecordTransform_23() { return &___uriRecordTransform_23; }
	inline void set_uriRecordTransform_23(Transform_t3600365921 * value)
	{
		___uriRecordTransform_23 = value;
		Il2CppCodeGenWriteBarrier((&___uriRecordTransform_23), value);
	}

	inline static int32_t get_offset_of_uriInput_24() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___uriInput_24)); }
	inline InputField_t3762917431 * get_uriInput_24() const { return ___uriInput_24; }
	inline InputField_t3762917431 ** get_address_of_uriInput_24() { return &___uriInput_24; }
	inline void set_uriInput_24(InputField_t3762917431 * value)
	{
		___uriInput_24 = value;
		Il2CppCodeGenWriteBarrier((&___uriInput_24), value);
	}

	inline static int32_t get_offset_of_mimeMediaRecordTransform_25() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___mimeMediaRecordTransform_25)); }
	inline Transform_t3600365921 * get_mimeMediaRecordTransform_25() const { return ___mimeMediaRecordTransform_25; }
	inline Transform_t3600365921 ** get_address_of_mimeMediaRecordTransform_25() { return &___mimeMediaRecordTransform_25; }
	inline void set_mimeMediaRecordTransform_25(Transform_t3600365921 * value)
	{
		___mimeMediaRecordTransform_25 = value;
		Il2CppCodeGenWriteBarrier((&___mimeMediaRecordTransform_25), value);
	}

	inline static int32_t get_offset_of_iconDropdown_26() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___iconDropdown_26)); }
	inline Dropdown_t2274391225 * get_iconDropdown_26() const { return ___iconDropdown_26; }
	inline Dropdown_t2274391225 ** get_address_of_iconDropdown_26() { return &___iconDropdown_26; }
	inline void set_iconDropdown_26(Dropdown_t2274391225 * value)
	{
		___iconDropdown_26 = value;
		Il2CppCodeGenWriteBarrier((&___iconDropdown_26), value);
	}

	inline static int32_t get_offset_of_mimeTypeLabel_27() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___mimeTypeLabel_27)); }
	inline Text_t1901882714 * get_mimeTypeLabel_27() const { return ___mimeTypeLabel_27; }
	inline Text_t1901882714 ** get_address_of_mimeTypeLabel_27() { return &___mimeTypeLabel_27; }
	inline void set_mimeTypeLabel_27(Text_t1901882714 * value)
	{
		___mimeTypeLabel_27 = value;
		Il2CppCodeGenWriteBarrier((&___mimeTypeLabel_27), value);
	}

	inline static int32_t get_offset_of_externalTypeRecordTransform_28() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___externalTypeRecordTransform_28)); }
	inline Transform_t3600365921 * get_externalTypeRecordTransform_28() const { return ___externalTypeRecordTransform_28; }
	inline Transform_t3600365921 ** get_address_of_externalTypeRecordTransform_28() { return &___externalTypeRecordTransform_28; }
	inline void set_externalTypeRecordTransform_28(Transform_t3600365921 * value)
	{
		___externalTypeRecordTransform_28 = value;
		Il2CppCodeGenWriteBarrier((&___externalTypeRecordTransform_28), value);
	}

	inline static int32_t get_offset_of_domainNameInput_29() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___domainNameInput_29)); }
	inline InputField_t3762917431 * get_domainNameInput_29() const { return ___domainNameInput_29; }
	inline InputField_t3762917431 ** get_address_of_domainNameInput_29() { return &___domainNameInput_29; }
	inline void set_domainNameInput_29(InputField_t3762917431 * value)
	{
		___domainNameInput_29 = value;
		Il2CppCodeGenWriteBarrier((&___domainNameInput_29), value);
	}

	inline static int32_t get_offset_of_domainTypeInput_30() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___domainTypeInput_30)); }
	inline InputField_t3762917431 * get_domainTypeInput_30() const { return ___domainTypeInput_30; }
	inline InputField_t3762917431 ** get_address_of_domainTypeInput_30() { return &___domainTypeInput_30; }
	inline void set_domainTypeInput_30(InputField_t3762917431 * value)
	{
		___domainTypeInput_30 = value;
		Il2CppCodeGenWriteBarrier((&___domainTypeInput_30), value);
	}

	inline static int32_t get_offset_of_domainDataInput_31() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___domainDataInput_31)); }
	inline InputField_t3762917431 * get_domainDataInput_31() const { return ___domainDataInput_31; }
	inline InputField_t3762917431 ** get_address_of_domainDataInput_31() { return &___domainDataInput_31; }
	inline void set_domainDataInput_31(InputField_t3762917431 * value)
	{
		___domainDataInput_31 = value;
		Il2CppCodeGenWriteBarrier((&___domainDataInput_31), value);
	}

	inline static int32_t get_offset_of_clearButton_32() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___clearButton_32)); }
	inline Button_t4055032469 * get_clearButton_32() const { return ___clearButton_32; }
	inline Button_t4055032469 ** get_address_of_clearButton_32() { return &___clearButton_32; }
	inline void set_clearButton_32(Button_t4055032469 * value)
	{
		___clearButton_32 = value;
		Il2CppCodeGenWriteBarrier((&___clearButton_32), value);
	}

	inline static int32_t get_offset_of_writeButton_33() { return static_cast<int32_t>(offsetof(WriteScreenView_t2350253495, ___writeButton_33)); }
	inline Button_t4055032469 * get_writeButton_33() const { return ___writeButton_33; }
	inline Button_t4055032469 ** get_address_of_writeButton_33() { return &___writeButton_33; }
	inline void set_writeButton_33(Button_t4055032469 * value)
	{
		___writeButton_33 = value;
		Il2CppCodeGenWriteBarrier((&___writeButton_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESCREENVIEW_T2350253495_H
#ifndef UNIWEBVIEWNATIVELISTENER_T1145263134_H
#define UNIWEBVIEWNATIVELISTENER_T1145263134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewNativeListener
struct  UniWebViewNativeListener_t1145263134  : public MonoBehaviour_t3962482529
{
public:
	// UniWebView UniWebViewNativeListener::webView
	UniWebView_t941983939 * ___webView_4;

public:
	inline static int32_t get_offset_of_webView_4() { return static_cast<int32_t>(offsetof(UniWebViewNativeListener_t1145263134, ___webView_4)); }
	inline UniWebView_t941983939 * get_webView_4() const { return ___webView_4; }
	inline UniWebView_t941983939 ** get_address_of_webView_4() { return &___webView_4; }
	inline void set_webView_4(UniWebView_t941983939 * value)
	{
		___webView_4 = value;
		Il2CppCodeGenWriteBarrier((&___webView_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWNATIVELISTENER_T1145263134_H
#ifndef IOSNFC_T293874181_H
#define IOSNFC_T293874181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.IOSNFC
struct  IOSNFC_t293874181  : public NativeNFC_t1941597496
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNFC_T293874181_H
#ifndef IMAGERECORDITEM_T2104316047_H
#define IMAGERECORDITEM_T2104316047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitsNFCToolkit.Samples.ImageRecordItem
struct  ImageRecordItem_t2104316047  : public RecordItem_t1075151419
{
public:
	// UnityEngine.UI.Image DigitsNFCToolkit.Samples.ImageRecordItem::imageRenderer
	Image_t2670269651 * ___imageRenderer_6;

public:
	inline static int32_t get_offset_of_imageRenderer_6() { return static_cast<int32_t>(offsetof(ImageRecordItem_t2104316047, ___imageRenderer_6)); }
	inline Image_t2670269651 * get_imageRenderer_6() const { return ___imageRenderer_6; }
	inline Image_t2670269651 ** get_address_of_imageRenderer_6() { return &___imageRenderer_6; }
	inline void set_imageRenderer_6(Image_t2670269651 * value)
	{
		___imageRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((&___imageRenderer_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGERECORDITEM_T2104316047_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4800 = { sizeof (PageStartedDelegate_t2830724344), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4801 = { sizeof (PageFinishedDelegate_t2717015276), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4802 = { sizeof (PageErrorReceivedDelegate_t1724664023), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4803 = { sizeof (MessageReceivedDelegate_t2288957136), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4804 = { sizeof (ShouldCloseDelegate_t766319959), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4805 = { sizeof (KeyCodeReceivedDelegate_t3839084677), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4806 = { sizeof (OreintationChangedDelegate_t1877368362), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4807 = { sizeof (U3CGetHTMLContentU3Ec__AnonStorey0_t803715261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4807[1] = 
{
	U3CGetHTMLContentU3Ec__AnonStorey0_t803715261::get_offset_of_handler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4808 = { sizeof (UniWebViewHelper_t1459904593), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4809 = { sizeof (UniWebViewLogger_t952558916), -1, sizeof(UniWebViewLogger_t952558916_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4809[2] = 
{
	UniWebViewLogger_t952558916_StaticFields::get_offset_of_instance_0(),
	UniWebViewLogger_t952558916::get_offset_of_level_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4810 = { sizeof (Level_t4211844589)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4810[6] = 
{
	Level_t4211844589::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4811 = { sizeof (UniWebViewMessage_t2441068380)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4811[4] = 
{
	UniWebViewMessage_t2441068380::get_offset_of_U3CRawMessageU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UniWebViewMessage_t2441068380::get_offset_of_U3CSchemeU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UniWebViewMessage_t2441068380::get_offset_of_U3CPathU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UniWebViewMessage_t2441068380::get_offset_of_U3CArgsU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4812 = { sizeof (UniWebViewNativeListener_t1145263134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4812[1] = 
{
	UniWebViewNativeListener_t1145263134::get_offset_of_webView_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4813 = { sizeof (UniWebViewNativeResultPayload_t4072739617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4813[3] = 
{
	UniWebViewNativeResultPayload_t4072739617::get_offset_of_identifier_0(),
	UniWebViewNativeResultPayload_t4072739617::get_offset_of_resultCode_1(),
	UniWebViewNativeResultPayload_t4072739617::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4814 = { sizeof (UniWebViewToolbarPosition_t2230581447)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4814[3] = 
{
	UniWebViewToolbarPosition_t2230581447::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4815 = { sizeof (UniWebViewTransitionEdge_t2354894166)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4815[6] = 
{
	UniWebViewTransitionEdge_t2354894166::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4816 = { sizeof (U3CModuleU3E_t692745566), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4817 = { sizeof (ImageRecordItem_t2104316047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4817[1] = 
{
	ImageRecordItem_t2104316047::get_offset_of_imageRenderer_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4818 = { sizeof (MessageScreenView_t146641597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4818[9] = 
{
	MessageScreenView_t146641597::get_offset_of_writeMessageBox_4(),
	MessageScreenView_t146641597::get_offset_of_writeLabel_5(),
	MessageScreenView_t146641597::get_offset_of_writeCancelButton_6(),
	MessageScreenView_t146641597::get_offset_of_writeOKButton_7(),
	MessageScreenView_t146641597::get_offset_of_pushMessageBox_8(),
	MessageScreenView_t146641597::get_offset_of_pushLabel_9(),
	MessageScreenView_t146641597::get_offset_of_pushCancelButton_10(),
	MessageScreenView_t146641597::get_offset_of_pushOKButton_11(),
	MessageScreenView_t146641597::get_offset_of_initialized_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4819 = { sizeof (NavigationManager_t1939391727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4819[2] = 
{
	NavigationManager_t1939391727::get_offset_of_readScreenControl_4(),
	NavigationManager_t1939391727::get_offset_of_writeScreenControl_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4820 = { sizeof (ReadScreenControl_t3483866810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4820[1] = 
{
	ReadScreenControl_t3483866810::get_offset_of_view_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4821 = { sizeof (ReadScreenView_t239900869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4821[16] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ReadScreenView_t239900869::get_offset_of_textRecordItemPrefab_11(),
	ReadScreenView_t239900869::get_offset_of_uriRecordItemPrefab_12(),
	ReadScreenView_t239900869::get_offset_of_mimeMediaRecordItemPrefab_13(),
	ReadScreenView_t239900869::get_offset_of_externalTypeRecordItemPrefab_14(),
	ReadScreenView_t239900869::get_offset_of_smartPosterRecordItemPrefab_15(),
	ReadScreenView_t239900869::get_offset_of_tagInfoTransform_16(),
	ReadScreenView_t239900869::get_offset_of_iOSReadTransform_17(),
	ReadScreenView_t239900869::get_offset_of_tagInfoContentLabel_18(),
	ReadScreenView_t239900869::get_offset_of_ndefMessageScrollRect_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4822 = { sizeof (RecordItem_t1075151419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4822[2] = 
{
	RecordItem_t1075151419::get_offset_of_U3CRectTransformU3Ek__BackingField_4(),
	RecordItem_t1075151419::get_offset_of_label_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4823 = { sizeof (WriteScreenControl_t1506090515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4823[6] = 
{
	WriteScreenControl_t1506090515::get_offset_of_view_4(),
	WriteScreenControl_t1506090515::get_offset_of_messageScreenView_5(),
	WriteScreenControl_t1506090515::get_offset_of_musicNoteIcon_6(),
	WriteScreenControl_t1506090515::get_offset_of_faceIcon_7(),
	WriteScreenControl_t1506090515::get_offset_of_arrowIcon_8(),
	WriteScreenControl_t1506090515::get_offset_of_pendingMessage_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4824 = { sizeof (IconID_t582738576)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4824[4] = 
{
	IconID_t582738576::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4825 = { sizeof (WriteScreenView_t2350253495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4825[30] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	WriteScreenView_t2350253495::get_offset_of_textRecordItemPrefab_10(),
	WriteScreenView_t2350253495::get_offset_of_uriRecordItemPrefab_11(),
	WriteScreenView_t2350253495::get_offset_of_mimeMediaRecordItemPrefab_12(),
	WriteScreenView_t2350253495::get_offset_of_externalTypeRecordItemPrefab_13(),
	WriteScreenView_t2350253495::get_offset_of_smartPosterRecordItemPrefab_14(),
	WriteScreenView_t2350253495::get_offset_of_ndefMessageScrollRect_15(),
	WriteScreenView_t2350253495::get_offset_of_titleLabel_16(),
	WriteScreenView_t2350253495::get_offset_of_typeDropdown_17(),
	WriteScreenView_t2350253495::get_offset_of_textRecordTransform_18(),
	WriteScreenView_t2350253495::get_offset_of_textInput_19(),
	WriteScreenView_t2350253495::get_offset_of_languageCodeInput_20(),
	WriteScreenView_t2350253495::get_offset_of_textEncodingDropdown_21(),
	WriteScreenView_t2350253495::get_offset_of_addRecordButton_22(),
	WriteScreenView_t2350253495::get_offset_of_uriRecordTransform_23(),
	WriteScreenView_t2350253495::get_offset_of_uriInput_24(),
	WriteScreenView_t2350253495::get_offset_of_mimeMediaRecordTransform_25(),
	WriteScreenView_t2350253495::get_offset_of_iconDropdown_26(),
	WriteScreenView_t2350253495::get_offset_of_mimeTypeLabel_27(),
	WriteScreenView_t2350253495::get_offset_of_externalTypeRecordTransform_28(),
	WriteScreenView_t2350253495::get_offset_of_domainNameInput_29(),
	WriteScreenView_t2350253495::get_offset_of_domainTypeInput_30(),
	WriteScreenView_t2350253495::get_offset_of_domainDataInput_31(),
	WriteScreenView_t2350253495::get_offset_of_clearButton_32(),
	WriteScreenView_t2350253495::get_offset_of_writeButton_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4826 = { sizeof (IOSNFC_t293874181), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4827 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4828 = { sizeof (JSONArray_t4024675823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4828[1] = 
{
	JSONArray_t4024675823::get_offset_of_values_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4829 = { sizeof (JSONObject_t321714843), -1, sizeof(JSONObject_t321714843_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4829[3] = 
{
	JSONObject_t321714843::get_offset_of_values_0(),
	JSONObject_t321714843_StaticFields::get_offset_of_unicodeRegex_1(),
	JSONObject_t321714843_StaticFields::get_offset_of_unicodeBytes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4830 = { sizeof (JSONParsingState_t2722137651)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4830[13] = 
{
	JSONParsingState_t2722137651::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4831 = { sizeof (JSONValueType_t3019243058)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4831[7] = 
{
	JSONValueType_t3019243058::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4832 = { sizeof (JSONValue_t4275860644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4832[7] = 
{
	JSONValue_t4275860644::get_offset_of_U3CTypeU3Ek__BackingField_0(),
	JSONValue_t4275860644::get_offset_of_U3CStringU3Ek__BackingField_1(),
	JSONValue_t4275860644::get_offset_of_U3CDoubleU3Ek__BackingField_2(),
	JSONValue_t4275860644::get_offset_of_U3CObjectU3Ek__BackingField_3(),
	JSONValue_t4275860644::get_offset_of_U3CArrayU3Ek__BackingField_4(),
	JSONValue_t4275860644::get_offset_of_U3CBooleanU3Ek__BackingField_5(),
	JSONValue_t4275860644::get_offset_of_U3CParentU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4833 = { sizeof (AbsoluteUriRecord_t2525168784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4833[1] = 
{
	AbsoluteUriRecord_t2525168784::get_offset_of_uri_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4834 = { sizeof (EmptyRecord_t1486430273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4835 = { sizeof (ExternalTypeRecord_t4087466745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4835[3] = 
{
	ExternalTypeRecord_t4087466745::get_offset_of_domainName_1(),
	ExternalTypeRecord_t4087466745::get_offset_of_domainType_2(),
	ExternalTypeRecord_t4087466745::get_offset_of_domainData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4836 = { sizeof (MimeMediaRecord_t736820488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4836[2] = 
{
	MimeMediaRecord_t736820488::get_offset_of_mimeType_1(),
	MimeMediaRecord_t736820488::get_offset_of_mimeData_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4837 = { sizeof (NDEFMessageWriteState_t2549748319)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4837[5] = 
{
	NDEFMessageWriteState_t2549748319::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4838 = { sizeof (NDEFMessageWriteError_t164821916)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4838[5] = 
{
	NDEFMessageWriteError_t164821916::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4839 = { sizeof (NDEFMessage_t279637043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4839[4] = 
{
	NDEFMessage_t279637043::get_offset_of_records_0(),
	NDEFMessage_t279637043::get_offset_of_tagID_1(),
	NDEFMessage_t279637043::get_offset_of_writeState_2(),
	NDEFMessage_t279637043::get_offset_of_writeError_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4840 = { sizeof (NDEFPushResult_t3422827153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4840[2] = 
{
	NDEFPushResult_t3422827153::get_offset_of_success_0(),
	NDEFPushResult_t3422827153::get_offset_of_message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4841 = { sizeof (NDEFReadError_t886852181)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4841[6] = 
{
	NDEFReadError_t886852181::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4842 = { sizeof (NDEFReadResult_t3483243621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4842[4] = 
{
	NDEFReadResult_t3483243621::get_offset_of_success_0(),
	NDEFReadResult_t3483243621::get_offset_of_error_1(),
	NDEFReadResult_t3483243621::get_offset_of_message_2(),
	NDEFReadResult_t3483243621::get_offset_of_tagID_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4843 = { sizeof (NDEFRecordType_t4294622310)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4843[9] = 
{
	NDEFRecordType_t4294622310::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4844 = { sizeof (NDEFRecord_t2690545556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4844[1] = 
{
	NDEFRecord_t2690545556::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4845 = { sizeof (NDEFWriteError_t890528595)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4845[5] = 
{
	NDEFWriteError_t890528595::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4846 = { sizeof (NDEFWriteResult_t4210562629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4846[4] = 
{
	NDEFWriteResult_t4210562629::get_offset_of_success_0(),
	NDEFWriteResult_t4210562629::get_offset_of_error_1(),
	NDEFWriteResult_t4210562629::get_offset_of_message_2(),
	NDEFWriteResult_t4210562629::get_offset_of_tagID_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4847 = { sizeof (NFCTechnology_t1376062623)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4847[12] = 
{
	NFCTechnology_t1376062623::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4848 = { sizeof (NFCTag_t2820711232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4848[5] = 
{
	NFCTag_t2820711232::get_offset_of_id_0(),
	NFCTag_t2820711232::get_offset_of_technologies_1(),
	NFCTag_t2820711232::get_offset_of_manufacturer_2(),
	NFCTag_t2820711232::get_offset_of_writable_3(),
	NFCTag_t2820711232::get_offset_of_maxWriteSize_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4849 = { sizeof (SmartPosterRecord_t1640848801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4849[7] = 
{
	SmartPosterRecord_t1640848801::get_offset_of_uriRecord_1(),
	SmartPosterRecord_t1640848801::get_offset_of_titleRecords_2(),
	SmartPosterRecord_t1640848801::get_offset_of_iconRecords_3(),
	SmartPosterRecord_t1640848801::get_offset_of_extraRecords_4(),
	SmartPosterRecord_t1640848801::get_offset_of_action_5(),
	SmartPosterRecord_t1640848801::get_offset_of_size_6(),
	SmartPosterRecord_t1640848801::get_offset_of_mimeType_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4850 = { sizeof (RecommendedAction_t1182550772)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4850[5] = 
{
	RecommendedAction_t1182550772::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4851 = { sizeof (TextRecord_t2313697623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4851[3] = 
{
	TextRecord_t2313697623::get_offset_of_text_1(),
	TextRecord_t2313697623::get_offset_of_languageCode_2(),
	TextRecord_t2313697623::get_offset_of_textEncoding_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4852 = { sizeof (TextEncoding_t3210513626)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4852[3] = 
{
	TextEncoding_t3210513626::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4853 = { sizeof (UnknownRecord_t3228240714), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4854 = { sizeof (UriRecord_t2230063309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4854[3] = 
{
	UriRecord_t2230063309::get_offset_of_fullUri_1(),
	UriRecord_t2230063309::get_offset_of_uri_2(),
	UriRecord_t2230063309::get_offset_of_protocol_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4855 = { sizeof (OnNFCTagDetected_t3189675727), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4856 = { sizeof (OnNDEFReadFinished_t1327886840), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4857 = { sizeof (OnNDEFWriteFinished_t4102039599), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4858 = { sizeof (OnNDEFPushFinished_t4279917764), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4859 = { sizeof (NativeNFC_t1941597496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4859[5] = 
{
	NativeNFC_t1941597496::get_offset_of_onNFCTagDetected_4(),
	NativeNFC_t1941597496::get_offset_of_onNDEFReadFinished_5(),
	NativeNFC_t1941597496::get_offset_of_onNDEFWriteFinished_6(),
	NativeNFC_t1941597496::get_offset_of_onNDEFPushFinished_7(),
	NativeNFC_t1941597496::get_offset_of_resetOnTimeout_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4860 = { sizeof (NativeNFCManager_t351225459), -1, sizeof(NativeNFCManager_t351225459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4860[3] = 
{
	0,
	NativeNFCManager_t351225459_StaticFields::get_offset_of_instance_5(),
	NativeNFCManager_t351225459::get_offset_of_nfc_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4861 = { sizeof (Util_t4025012431), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
