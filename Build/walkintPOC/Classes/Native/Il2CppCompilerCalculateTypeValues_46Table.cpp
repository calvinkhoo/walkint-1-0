﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ColorChangedEvent
struct ColorChangedEvent_t3019780707;
// HSVChangedEvent
struct HSVChangedEvent_t911780251;
// System.Action
struct Action_t1264377477;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t2696614423;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t898892918;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.Generic.List`1<UnityEngine.Rect>
struct List_1_t3832554601;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t881764471;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.DropDownListButton>
struct List_1_t418871995;
// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.DropDownListItem>
struct List_1_t1809319012;
// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.ReorderableListElement>
struct List_1_t3356400495;
// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.TextPic/HrefInfo>
struct List_1_t337968791;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t4142344393;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1234605051;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Predicate`1<UnityEngine.UI.Image>
struct Predicate_1_t3495563775;
// System.String
struct String_t;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t2331243652;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3903027533;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t3630163547;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// UnityEngine.EventSystems.PointerInputModule/MouseState
struct MouseState_t384203932;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Gradient
struct Gradient_t3067099924;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t2007329276;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t3069227754;
// UnityEngine.ParticleSystemRenderer
struct ParticleSystemRenderer_t2065813411;
// UnityEngine.RectOffset
struct RectOffset_t1369453676;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.Extensions.AutoCompleteComboBox
struct AutoCompleteComboBox_t2765567798;
// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionChangedEvent
struct SelectionChangedEvent_t1822043360;
// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionTextChangedEvent
struct SelectionTextChangedEvent_t4051177638;
// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionValidityChangedEvent
struct SelectionValidityChangedEvent_t954817928;
// UnityEngine.UI.Extensions.BoxSlider
struct BoxSlider_t3694973841;
// UnityEngine.UI.Extensions.CUIBezierCurve[]
struct CUIBezierCurveU5BU5D_t3815322299;
// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl
struct ColorPickerControl_t2793111723;
// UnityEngine.UI.Extensions.ComboBox
struct ComboBox_t4216213764;
// UnityEngine.UI.Extensions.ComboBox/SelectionChangedEvent
struct SelectionChangedEvent_t2252533886;
// UnityEngine.UI.Extensions.CooldownButton/CooldownButtonEvent
struct CooldownButtonEvent_t856711112;
// UnityEngine.UI.Extensions.DropDownList
struct DropDownList_t4179439446;
// UnityEngine.UI.Extensions.DropDownList/SelectionChangedEvent
struct SelectionChangedEvent_t1418459309;
// UnityEngine.UI.Extensions.DropDownListButton
struct DropDownListButton_t3241764549;
// UnityEngine.UI.Extensions.DropDownListItem
struct DropDownListItem_t337244270;
// UnityEngine.UI.Extensions.IBoxSelectable
struct IBoxSelectable_t463735904;
// UnityEngine.UI.Extensions.IBoxSelectable[]
struct IBoxSelectableU5BU5D_t183195169;
// UnityEngine.UI.Extensions.KnobFloatValueEvent
struct KnobFloatValueEvent_t1285673625;
// UnityEngine.UI.Extensions.RadialSlider/RadialSliderTextValueChangedEvent
struct RadialSliderTextValueChangedEvent_t1078616506;
// UnityEngine.UI.Extensions.RadialSlider/RadialSliderValueChangedEvent
struct RadialSliderValueChangedEvent_t1025479356;
// UnityEngine.UI.Extensions.ReorderableList
struct ReorderableList_t1822109201;
// UnityEngine.UI.Extensions.ReorderableList/ReorderableListHandler
struct ReorderableListHandler_t1290756480;
// UnityEngine.UI.Extensions.ReorderableListContent
struct ReorderableListContent_t2633001117;
// UnityEngine.UI.Extensions.ReorderableListElement
struct ReorderableListElement_t1884325753;
// UnityEngine.UI.Extensions.SegmentedControl/SegmentSelectedEvent
struct SegmentSelectedEvent_t878161132;
// UnityEngine.UI.Extensions.SelectionBox/SelectionEvent
struct SelectionEvent_t3355704588;
// UnityEngine.UI.Extensions.ShineEffect
struct ShineEffect_t3679628888;
// UnityEngine.UI.Extensions.Stepper/StepperValueChangedEvent
struct StepperValueChangedEvent_t1994331895;
// UnityEngine.UI.Extensions.TextPic/HrefClickEvent
struct HrefClickEvent_t324372001;
// UnityEngine.UI.Extensions.TextPic/IconName[]
struct IconNameU5BU5D_t3003742235;
// UnityEngine.UI.Extensions.Vector3_Array2D[]
struct Vector3_Array2DU5BU5D_t2158155091;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t1785403678;
// UnityEngine.UI.LayoutGroup
struct LayoutGroup_t2436138090;
// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t3839221559;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.UI.ScrollRect/ScrollRectEvent
struct ScrollRectEvent_t343079324;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_t774044132;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;

struct Vector3_t3722313464 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef U3CPRUNEITEMSLINQU3EC__ANONSTOREY1_T1526519783_H
#define U3CPRUNEITEMSLINQU3EC__ANONSTOREY1_T1526519783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox/<PruneItemsLinq>c__AnonStorey1
struct  U3CPruneItemsLinqU3Ec__AnonStorey1_t1526519783  : public RuntimeObject
{
public:
	// System.String UnityEngine.UI.Extensions.AutoCompleteComboBox/<PruneItemsLinq>c__AnonStorey1::currText
	String_t* ___currText_0;

public:
	inline static int32_t get_offset_of_currText_0() { return static_cast<int32_t>(offsetof(U3CPruneItemsLinqU3Ec__AnonStorey1_t1526519783, ___currText_0)); }
	inline String_t* get_currText_0() const { return ___currText_0; }
	inline String_t** get_address_of_currText_0() { return &___currText_0; }
	inline void set_currText_0(String_t* value)
	{
		___currText_0 = value;
		Il2CppCodeGenWriteBarrier((&___currText_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRUNEITEMSLINQU3EC__ANONSTOREY1_T1526519783_H
#ifndef U3CREBUILDPANELU3EC__ANONSTOREY0_T1110430399_H
#define U3CREBUILDPANELU3EC__ANONSTOREY0_T1110430399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox/<RebuildPanel>c__AnonStorey0
struct  U3CRebuildPanelU3Ec__AnonStorey0_t1110430399  : public RuntimeObject
{
public:
	// System.String UnityEngine.UI.Extensions.AutoCompleteComboBox/<RebuildPanel>c__AnonStorey0::textOfItem
	String_t* ___textOfItem_0;
	// UnityEngine.UI.Extensions.AutoCompleteComboBox UnityEngine.UI.Extensions.AutoCompleteComboBox/<RebuildPanel>c__AnonStorey0::$this
	AutoCompleteComboBox_t2765567798 * ___U24this_1;

public:
	inline static int32_t get_offset_of_textOfItem_0() { return static_cast<int32_t>(offsetof(U3CRebuildPanelU3Ec__AnonStorey0_t1110430399, ___textOfItem_0)); }
	inline String_t* get_textOfItem_0() const { return ___textOfItem_0; }
	inline String_t** get_address_of_textOfItem_0() { return &___textOfItem_0; }
	inline void set_textOfItem_0(String_t* value)
	{
		___textOfItem_0 = value;
		Il2CppCodeGenWriteBarrier((&___textOfItem_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRebuildPanelU3Ec__AnonStorey0_t1110430399, ___U24this_1)); }
	inline AutoCompleteComboBox_t2765567798 * get_U24this_1() const { return ___U24this_1; }
	inline AutoCompleteComboBox_t2765567798 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AutoCompleteComboBox_t2765567798 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREBUILDPANELU3EC__ANONSTOREY0_T1110430399_H
#ifndef HSVUTIL_T2354927206_H
#define HSVUTIL_T2354927206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.HSVUtil
struct  HSVUtil_t2354927206  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVUTIL_T2354927206_H
#ifndef U3CREBUILDPANELU3EC__ANONSTOREY0_T2402133322_H
#define U3CREBUILDPANELU3EC__ANONSTOREY0_T2402133322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ComboBox/<RebuildPanel>c__AnonStorey0
struct  U3CRebuildPanelU3Ec__AnonStorey0_t2402133322  : public RuntimeObject
{
public:
	// System.String UnityEngine.UI.Extensions.ComboBox/<RebuildPanel>c__AnonStorey0::textOfItem
	String_t* ___textOfItem_0;
	// UnityEngine.UI.Extensions.ComboBox UnityEngine.UI.Extensions.ComboBox/<RebuildPanel>c__AnonStorey0::$this
	ComboBox_t4216213764 * ___U24this_1;

public:
	inline static int32_t get_offset_of_textOfItem_0() { return static_cast<int32_t>(offsetof(U3CRebuildPanelU3Ec__AnonStorey0_t2402133322, ___textOfItem_0)); }
	inline String_t* get_textOfItem_0() const { return ___textOfItem_0; }
	inline String_t** get_address_of_textOfItem_0() { return &___textOfItem_0; }
	inline void set_textOfItem_0(String_t* value)
	{
		___textOfItem_0 = value;
		Il2CppCodeGenWriteBarrier((&___textOfItem_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRebuildPanelU3Ec__AnonStorey0_t2402133322, ___U24this_1)); }
	inline ComboBox_t4216213764 * get_U24this_1() const { return ___U24this_1; }
	inline ComboBox_t4216213764 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ComboBox_t4216213764 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREBUILDPANELU3EC__ANONSTOREY0_T2402133322_H
#ifndef U3CREBUILDPANELU3EC__ANONSTOREY0_T4108379701_H
#define U3CREBUILDPANELU3EC__ANONSTOREY0_T4108379701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.DropDownList/<RebuildPanel>c__AnonStorey0
struct  U3CRebuildPanelU3Ec__AnonStorey0_t4108379701  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.UI.Extensions.DropDownList/<RebuildPanel>c__AnonStorey0::ii
	int32_t ___ii_0;
	// UnityEngine.UI.Extensions.DropDownListItem UnityEngine.UI.Extensions.DropDownList/<RebuildPanel>c__AnonStorey0::item
	DropDownListItem_t337244270 * ___item_1;
	// UnityEngine.UI.Extensions.DropDownList UnityEngine.UI.Extensions.DropDownList/<RebuildPanel>c__AnonStorey0::$this
	DropDownList_t4179439446 * ___U24this_2;

public:
	inline static int32_t get_offset_of_ii_0() { return static_cast<int32_t>(offsetof(U3CRebuildPanelU3Ec__AnonStorey0_t4108379701, ___ii_0)); }
	inline int32_t get_ii_0() const { return ___ii_0; }
	inline int32_t* get_address_of_ii_0() { return &___ii_0; }
	inline void set_ii_0(int32_t value)
	{
		___ii_0 = value;
	}

	inline static int32_t get_offset_of_item_1() { return static_cast<int32_t>(offsetof(U3CRebuildPanelU3Ec__AnonStorey0_t4108379701, ___item_1)); }
	inline DropDownListItem_t337244270 * get_item_1() const { return ___item_1; }
	inline DropDownListItem_t337244270 ** get_address_of_item_1() { return &___item_1; }
	inline void set_item_1(DropDownListItem_t337244270 * value)
	{
		___item_1 = value;
		Il2CppCodeGenWriteBarrier((&___item_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CRebuildPanelU3Ec__AnonStorey0_t4108379701, ___U24this_2)); }
	inline DropDownList_t4179439446 * get_U24this_2() const { return ___U24this_2; }
	inline DropDownList_t4179439446 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(DropDownList_t4179439446 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREBUILDPANELU3EC__ANONSTOREY0_T4108379701_H
#ifndef DROPDOWNLISTBUTTON_T3241764549_H
#define DROPDOWNLISTBUTTON_T3241764549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.DropDownListButton
struct  DropDownListButton_t3241764549  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.DropDownListButton::rectTransform
	RectTransform_t3704657025 * ___rectTransform_0;
	// UnityEngine.UI.Button UnityEngine.UI.Extensions.DropDownListButton::btn
	Button_t4055032469 * ___btn_1;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.DropDownListButton::txt
	Text_t1901882714 * ___txt_2;
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.DropDownListButton::btnImg
	Image_t2670269651 * ___btnImg_3;
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.DropDownListButton::img
	Image_t2670269651 * ___img_4;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.DropDownListButton::gameobject
	GameObject_t1113636619 * ___gameobject_5;

public:
	inline static int32_t get_offset_of_rectTransform_0() { return static_cast<int32_t>(offsetof(DropDownListButton_t3241764549, ___rectTransform_0)); }
	inline RectTransform_t3704657025 * get_rectTransform_0() const { return ___rectTransform_0; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_0() { return &___rectTransform_0; }
	inline void set_rectTransform_0(RectTransform_t3704657025 * value)
	{
		___rectTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_0), value);
	}

	inline static int32_t get_offset_of_btn_1() { return static_cast<int32_t>(offsetof(DropDownListButton_t3241764549, ___btn_1)); }
	inline Button_t4055032469 * get_btn_1() const { return ___btn_1; }
	inline Button_t4055032469 ** get_address_of_btn_1() { return &___btn_1; }
	inline void set_btn_1(Button_t4055032469 * value)
	{
		___btn_1 = value;
		Il2CppCodeGenWriteBarrier((&___btn_1), value);
	}

	inline static int32_t get_offset_of_txt_2() { return static_cast<int32_t>(offsetof(DropDownListButton_t3241764549, ___txt_2)); }
	inline Text_t1901882714 * get_txt_2() const { return ___txt_2; }
	inline Text_t1901882714 ** get_address_of_txt_2() { return &___txt_2; }
	inline void set_txt_2(Text_t1901882714 * value)
	{
		___txt_2 = value;
		Il2CppCodeGenWriteBarrier((&___txt_2), value);
	}

	inline static int32_t get_offset_of_btnImg_3() { return static_cast<int32_t>(offsetof(DropDownListButton_t3241764549, ___btnImg_3)); }
	inline Image_t2670269651 * get_btnImg_3() const { return ___btnImg_3; }
	inline Image_t2670269651 ** get_address_of_btnImg_3() { return &___btnImg_3; }
	inline void set_btnImg_3(Image_t2670269651 * value)
	{
		___btnImg_3 = value;
		Il2CppCodeGenWriteBarrier((&___btnImg_3), value);
	}

	inline static int32_t get_offset_of_img_4() { return static_cast<int32_t>(offsetof(DropDownListButton_t3241764549, ___img_4)); }
	inline Image_t2670269651 * get_img_4() const { return ___img_4; }
	inline Image_t2670269651 ** get_address_of_img_4() { return &___img_4; }
	inline void set_img_4(Image_t2670269651 * value)
	{
		___img_4 = value;
		Il2CppCodeGenWriteBarrier((&___img_4), value);
	}

	inline static int32_t get_offset_of_gameobject_5() { return static_cast<int32_t>(offsetof(DropDownListButton_t3241764549, ___gameobject_5)); }
	inline GameObject_t1113636619 * get_gameobject_5() const { return ___gameobject_5; }
	inline GameObject_t1113636619 ** get_address_of_gameobject_5() { return &___gameobject_5; }
	inline void set_gameobject_5(GameObject_t1113636619 * value)
	{
		___gameobject_5 = value;
		Il2CppCodeGenWriteBarrier((&___gameobject_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWNLISTBUTTON_T3241764549_H
#ifndef DROPDOWNLISTITEM_T337244270_H
#define DROPDOWNLISTITEM_T337244270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.DropDownListItem
struct  DropDownListItem_t337244270  : public RuntimeObject
{
public:
	// System.String UnityEngine.UI.Extensions.DropDownListItem::_caption
	String_t* ____caption_0;
	// UnityEngine.Sprite UnityEngine.UI.Extensions.DropDownListItem::_image
	Sprite_t280657092 * ____image_1;
	// System.Boolean UnityEngine.UI.Extensions.DropDownListItem::_isDisabled
	bool ____isDisabled_2;
	// System.String UnityEngine.UI.Extensions.DropDownListItem::_id
	String_t* ____id_3;
	// System.Action UnityEngine.UI.Extensions.DropDownListItem::OnSelect
	Action_t1264377477 * ___OnSelect_4;
	// System.Action UnityEngine.UI.Extensions.DropDownListItem::OnUpdate
	Action_t1264377477 * ___OnUpdate_5;

public:
	inline static int32_t get_offset_of__caption_0() { return static_cast<int32_t>(offsetof(DropDownListItem_t337244270, ____caption_0)); }
	inline String_t* get__caption_0() const { return ____caption_0; }
	inline String_t** get_address_of__caption_0() { return &____caption_0; }
	inline void set__caption_0(String_t* value)
	{
		____caption_0 = value;
		Il2CppCodeGenWriteBarrier((&____caption_0), value);
	}

	inline static int32_t get_offset_of__image_1() { return static_cast<int32_t>(offsetof(DropDownListItem_t337244270, ____image_1)); }
	inline Sprite_t280657092 * get__image_1() const { return ____image_1; }
	inline Sprite_t280657092 ** get_address_of__image_1() { return &____image_1; }
	inline void set__image_1(Sprite_t280657092 * value)
	{
		____image_1 = value;
		Il2CppCodeGenWriteBarrier((&____image_1), value);
	}

	inline static int32_t get_offset_of__isDisabled_2() { return static_cast<int32_t>(offsetof(DropDownListItem_t337244270, ____isDisabled_2)); }
	inline bool get__isDisabled_2() const { return ____isDisabled_2; }
	inline bool* get_address_of__isDisabled_2() { return &____isDisabled_2; }
	inline void set__isDisabled_2(bool value)
	{
		____isDisabled_2 = value;
	}

	inline static int32_t get_offset_of__id_3() { return static_cast<int32_t>(offsetof(DropDownListItem_t337244270, ____id_3)); }
	inline String_t* get__id_3() const { return ____id_3; }
	inline String_t** get_address_of__id_3() { return &____id_3; }
	inline void set__id_3(String_t* value)
	{
		____id_3 = value;
		Il2CppCodeGenWriteBarrier((&____id_3), value);
	}

	inline static int32_t get_offset_of_OnSelect_4() { return static_cast<int32_t>(offsetof(DropDownListItem_t337244270, ___OnSelect_4)); }
	inline Action_t1264377477 * get_OnSelect_4() const { return ___OnSelect_4; }
	inline Action_t1264377477 ** get_address_of_OnSelect_4() { return &___OnSelect_4; }
	inline void set_OnSelect_4(Action_t1264377477 * value)
	{
		___OnSelect_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelect_4), value);
	}

	inline static int32_t get_offset_of_OnUpdate_5() { return static_cast<int32_t>(offsetof(DropDownListItem_t337244270, ___OnUpdate_5)); }
	inline Action_t1264377477 * get_OnUpdate_5() const { return ___OnUpdate_5; }
	inline Action_t1264377477 ** get_address_of_OnUpdate_5() { return &___OnUpdate_5; }
	inline void set_OnUpdate_5(Action_t1264377477 * value)
	{
		___OnUpdate_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnUpdate_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWNLISTITEM_T337244270_H
#ifndef FANCYSCROLLVIEWNULLCONTEXT_T3783020080_H
#define FANCYSCROLLVIEWNULLCONTEXT_T3783020080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.FancyScrollViewNullContext
struct  FancyScrollViewNullContext_t3783020080  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FANCYSCROLLVIEWNULLCONTEXT_T3783020080_H
#ifndef U3CREFRESHCHILDRENU3EC__ITERATOR0_T2101808732_H
#define U3CREFRESHCHILDRENU3EC__ITERATOR0_T2101808732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ReorderableListContent/<RefreshChildren>c__Iterator0
struct  U3CRefreshChildrenU3Ec__Iterator0_t2101808732  : public RuntimeObject
{
public:
	// UnityEngine.UI.Extensions.ReorderableListContent UnityEngine.UI.Extensions.ReorderableListContent/<RefreshChildren>c__Iterator0::$this
	ReorderableListContent_t2633001117 * ___U24this_0;
	// System.Object UnityEngine.UI.Extensions.ReorderableListContent/<RefreshChildren>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableListContent/<RefreshChildren>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.UI.Extensions.ReorderableListContent/<RefreshChildren>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRefreshChildrenU3Ec__Iterator0_t2101808732, ___U24this_0)); }
	inline ReorderableListContent_t2633001117 * get_U24this_0() const { return ___U24this_0; }
	inline ReorderableListContent_t2633001117 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ReorderableListContent_t2633001117 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRefreshChildrenU3Ec__Iterator0_t2101808732, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRefreshChildrenU3Ec__Iterator0_t2101808732, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRefreshChildrenU3Ec__Iterator0_t2101808732, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREFRESHCHILDRENU3EC__ITERATOR0_T2101808732_H
#ifndef HREFINFO_T3160861345_H
#define HREFINFO_T3160861345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.TextPic/HrefInfo
struct  HrefInfo_t3160861345  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.UI.Extensions.TextPic/HrefInfo::startIndex
	int32_t ___startIndex_0;
	// System.Int32 UnityEngine.UI.Extensions.TextPic/HrefInfo::endIndex
	int32_t ___endIndex_1;
	// System.String UnityEngine.UI.Extensions.TextPic/HrefInfo::name
	String_t* ___name_2;
	// System.Collections.Generic.List`1<UnityEngine.Rect> UnityEngine.UI.Extensions.TextPic/HrefInfo::boxes
	List_1_t3832554601 * ___boxes_3;

public:
	inline static int32_t get_offset_of_startIndex_0() { return static_cast<int32_t>(offsetof(HrefInfo_t3160861345, ___startIndex_0)); }
	inline int32_t get_startIndex_0() const { return ___startIndex_0; }
	inline int32_t* get_address_of_startIndex_0() { return &___startIndex_0; }
	inline void set_startIndex_0(int32_t value)
	{
		___startIndex_0 = value;
	}

	inline static int32_t get_offset_of_endIndex_1() { return static_cast<int32_t>(offsetof(HrefInfo_t3160861345, ___endIndex_1)); }
	inline int32_t get_endIndex_1() const { return ___endIndex_1; }
	inline int32_t* get_address_of_endIndex_1() { return &___endIndex_1; }
	inline void set_endIndex_1(int32_t value)
	{
		___endIndex_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(HrefInfo_t3160861345, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_boxes_3() { return static_cast<int32_t>(offsetof(HrefInfo_t3160861345, ___boxes_3)); }
	inline List_1_t3832554601 * get_boxes_3() const { return ___boxes_3; }
	inline List_1_t3832554601 ** get_address_of_boxes_3() { return &___boxes_3; }
	inline void set_boxes_3(List_1_t3832554601 * value)
	{
		___boxes_3 = value;
		Il2CppCodeGenWriteBarrier((&___boxes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HREFINFO_T3160861345_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_t2562230146__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef UNITYEVENT_1_T978947469_H
#define UNITYEVENT_1_T978947469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t978947469  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t978947469, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T978947469_H
#ifndef UNITYEVENT_1_T3832605257_H
#define UNITYEVENT_1_T3832605257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t3832605257  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3832605257, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3832605257_H
#ifndef UNITYEVENT_1_T2278926278_H
#define UNITYEVENT_1_T2278926278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t2278926278  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2278926278, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2278926278_H
#ifndef UNITYEVENT_1_T2729110193_H
#define UNITYEVENT_1_T2729110193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_t2729110193  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2729110193, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2729110193_H
#ifndef UNITYEVENT_1_T3437345828_H
#define UNITYEVENT_1_T3437345828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct  UnityEvent_1_t3437345828  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3437345828, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3437345828_H
#ifndef UNITYEVENT_1_T290703556_H
#define UNITYEVENT_1_T290703556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.PointerEventData/InputButton>
struct  UnityEvent_1_t290703556  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t290703556, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T290703556_H
#ifndef UNITYEVENT_1_T1064854673_H
#define UNITYEVENT_1_T1064854673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.UI.Extensions.IBoxSelectable[]>
struct  UnityEvent_1_t1064854673  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1064854673, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1064854673_H
#ifndef UNITYEVENT_1_T2644075916_H
#define UNITYEVENT_1_T2644075916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct>
struct  UnityEvent_1_t2644075916  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2644075916, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2644075916_H
#ifndef UNITYEVENT_2_T364267509_H
#define UNITYEVENT_2_T364267509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.String,System.Boolean>
struct  UnityEvent_2_t364267509  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t364267509, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T364267509_H
#ifndef UNITYEVENT_3_T1697774568_H
#define UNITYEVENT_3_T1697774568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.Single,System.Single,System.Single>
struct  UnityEvent_3_t1697774568  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t1697774568, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T1697774568_H
#ifndef MAINMODULE_T2320046318_H
#define MAINMODULE_T2320046318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MainModule
struct  MainModule_t2320046318 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/MainModule::m_ParticleSystem
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(MainModule_t2320046318, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t1800779281 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t1800779281 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t1800779281 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t2320046318_marshaled_pinvoke
{
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t2320046318_marshaled_com
{
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;
};
#endif // MAINMODULE_T2320046318_H
#ifndef TEXTURESHEETANIMATIONMODULE_T738696839_H
#define TEXTURESHEETANIMATIONMODULE_T738696839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/TextureSheetAnimationModule
struct  TextureSheetAnimationModule_t738696839 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/TextureSheetAnimationModule::m_ParticleSystem
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(TextureSheetAnimationModule_t738696839, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t1800779281 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t1800779281 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t1800779281 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/TextureSheetAnimationModule
struct TextureSheetAnimationModule_t738696839_marshaled_pinvoke
{
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/TextureSheetAnimationModule
struct TextureSheetAnimationModule_t738696839_marshaled_com
{
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;
};
#endif // TEXTURESHEETANIMATIONMODULE_T738696839_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef HSVCOLOR_T3316572990_H
#define HSVCOLOR_T3316572990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.HsvColor
struct  HsvColor_t3316572990 
{
public:
	// System.Double UnityEngine.UI.Extensions.ColorPicker.HsvColor::H
	double ___H_0;
	// System.Double UnityEngine.UI.Extensions.ColorPicker.HsvColor::S
	double ___S_1;
	// System.Double UnityEngine.UI.Extensions.ColorPicker.HsvColor::V
	double ___V_2;

public:
	inline static int32_t get_offset_of_H_0() { return static_cast<int32_t>(offsetof(HsvColor_t3316572990, ___H_0)); }
	inline double get_H_0() const { return ___H_0; }
	inline double* get_address_of_H_0() { return &___H_0; }
	inline void set_H_0(double value)
	{
		___H_0 = value;
	}

	inline static int32_t get_offset_of_S_1() { return static_cast<int32_t>(offsetof(HsvColor_t3316572990, ___S_1)); }
	inline double get_S_1() const { return ___S_1; }
	inline double* get_address_of_S_1() { return &___S_1; }
	inline void set_S_1(double value)
	{
		___S_1 = value;
	}

	inline static int32_t get_offset_of_V_2() { return static_cast<int32_t>(offsetof(HsvColor_t3316572990, ___V_2)); }
	inline double get_V_2() const { return ___V_2; }
	inline double* get_address_of_V_2() { return &___V_2; }
	inline void set_V_2(double value)
	{
		___V_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVCOLOR_T3316572990_H
#ifndef REORDERABLELISTEVENTSTRUCT_T1762416412_H
#define REORDERABLELISTEVENTSTRUCT_T1762416412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct
struct  ReorderableListEventStruct_t1762416412 
{
public:
	// UnityEngine.GameObject UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct::DroppedObject
	GameObject_t1113636619 * ___DroppedObject_0;
	// System.Int32 UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct::FromIndex
	int32_t ___FromIndex_1;
	// UnityEngine.UI.Extensions.ReorderableList UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct::FromList
	ReorderableList_t1822109201 * ___FromList_2;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct::IsAClone
	bool ___IsAClone_3;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct::SourceObject
	GameObject_t1113636619 * ___SourceObject_4;
	// System.Int32 UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct::ToIndex
	int32_t ___ToIndex_5;
	// UnityEngine.UI.Extensions.ReorderableList UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct::ToList
	ReorderableList_t1822109201 * ___ToList_6;

public:
	inline static int32_t get_offset_of_DroppedObject_0() { return static_cast<int32_t>(offsetof(ReorderableListEventStruct_t1762416412, ___DroppedObject_0)); }
	inline GameObject_t1113636619 * get_DroppedObject_0() const { return ___DroppedObject_0; }
	inline GameObject_t1113636619 ** get_address_of_DroppedObject_0() { return &___DroppedObject_0; }
	inline void set_DroppedObject_0(GameObject_t1113636619 * value)
	{
		___DroppedObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___DroppedObject_0), value);
	}

	inline static int32_t get_offset_of_FromIndex_1() { return static_cast<int32_t>(offsetof(ReorderableListEventStruct_t1762416412, ___FromIndex_1)); }
	inline int32_t get_FromIndex_1() const { return ___FromIndex_1; }
	inline int32_t* get_address_of_FromIndex_1() { return &___FromIndex_1; }
	inline void set_FromIndex_1(int32_t value)
	{
		___FromIndex_1 = value;
	}

	inline static int32_t get_offset_of_FromList_2() { return static_cast<int32_t>(offsetof(ReorderableListEventStruct_t1762416412, ___FromList_2)); }
	inline ReorderableList_t1822109201 * get_FromList_2() const { return ___FromList_2; }
	inline ReorderableList_t1822109201 ** get_address_of_FromList_2() { return &___FromList_2; }
	inline void set_FromList_2(ReorderableList_t1822109201 * value)
	{
		___FromList_2 = value;
		Il2CppCodeGenWriteBarrier((&___FromList_2), value);
	}

	inline static int32_t get_offset_of_IsAClone_3() { return static_cast<int32_t>(offsetof(ReorderableListEventStruct_t1762416412, ___IsAClone_3)); }
	inline bool get_IsAClone_3() const { return ___IsAClone_3; }
	inline bool* get_address_of_IsAClone_3() { return &___IsAClone_3; }
	inline void set_IsAClone_3(bool value)
	{
		___IsAClone_3 = value;
	}

	inline static int32_t get_offset_of_SourceObject_4() { return static_cast<int32_t>(offsetof(ReorderableListEventStruct_t1762416412, ___SourceObject_4)); }
	inline GameObject_t1113636619 * get_SourceObject_4() const { return ___SourceObject_4; }
	inline GameObject_t1113636619 ** get_address_of_SourceObject_4() { return &___SourceObject_4; }
	inline void set_SourceObject_4(GameObject_t1113636619 * value)
	{
		___SourceObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___SourceObject_4), value);
	}

	inline static int32_t get_offset_of_ToIndex_5() { return static_cast<int32_t>(offsetof(ReorderableListEventStruct_t1762416412, ___ToIndex_5)); }
	inline int32_t get_ToIndex_5() const { return ___ToIndex_5; }
	inline int32_t* get_address_of_ToIndex_5() { return &___ToIndex_5; }
	inline void set_ToIndex_5(int32_t value)
	{
		___ToIndex_5 = value;
	}

	inline static int32_t get_offset_of_ToList_6() { return static_cast<int32_t>(offsetof(ReorderableListEventStruct_t1762416412, ___ToList_6)); }
	inline ReorderableList_t1822109201 * get_ToList_6() const { return ___ToList_6; }
	inline ReorderableList_t1822109201 ** get_address_of_ToList_6() { return &___ToList_6; }
	inline void set_ToList_6(ReorderableList_t1822109201 * value)
	{
		___ToList_6 = value;
		Il2CppCodeGenWriteBarrier((&___ToList_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct
struct ReorderableListEventStruct_t1762416412_marshaled_pinvoke
{
	GameObject_t1113636619 * ___DroppedObject_0;
	int32_t ___FromIndex_1;
	ReorderableList_t1822109201 * ___FromList_2;
	int32_t ___IsAClone_3;
	GameObject_t1113636619 * ___SourceObject_4;
	int32_t ___ToIndex_5;
	ReorderableList_t1822109201 * ___ToList_6;
};
// Native definition for COM marshalling of UnityEngine.UI.Extensions.ReorderableList/ReorderableListEventStruct
struct ReorderableListEventStruct_t1762416412_marshaled_com
{
	GameObject_t1113636619 * ___DroppedObject_0;
	int32_t ___FromIndex_1;
	ReorderableList_t1822109201 * ___FromList_2;
	int32_t ___IsAClone_3;
	GameObject_t1113636619 * ___SourceObject_4;
	int32_t ___ToIndex_5;
	ReorderableList_t1822109201 * ___ToList_6;
};
#endif // REORDERABLELISTEVENTSTRUCT_T1762416412_H
#ifndef VECTOR3_ARRAY2D_T2295860118_H
#define VECTOR3_ARRAY2D_T2295860118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Vector3_Array2D
struct  Vector3_Array2D_t2295860118 
{
public:
	// UnityEngine.Vector3[] UnityEngine.UI.Extensions.Vector3_Array2D::array
	Vector3U5BU5D_t1718750761* ___array_0;

public:
	inline static int32_t get_offset_of_array_0() { return static_cast<int32_t>(offsetof(Vector3_Array2D_t2295860118, ___array_0)); }
	inline Vector3U5BU5D_t1718750761* get_array_0() const { return ___array_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_array_0() { return &___array_0; }
	inline void set_array_0(Vector3U5BU5D_t1718750761* value)
	{
		___array_0 = value;
		Il2CppCodeGenWriteBarrier((&___array_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Extensions.Vector3_Array2D
struct Vector3_Array2D_t2295860118_marshaled_pinvoke
{
	Vector3_t3722313464 * ___array_0;
};
// Native definition for COM marshalling of UnityEngine.UI.Extensions.Vector3_Array2D
struct Vector3_Array2D_t2295860118_marshaled_com
{
	Vector3_t3722313464 * ___array_0;
};
#endif // VECTOR3_ARRAY2D_T2295860118_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef COLORCHANGEDEVENT_T3019780707_H
#define COLORCHANGEDEVENT_T3019780707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorChangedEvent
struct  ColorChangedEvent_t3019780707  : public UnityEvent_1_t3437345828
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCHANGEDEVENT_T3019780707_H
#ifndef HSVCHANGEDEVENT_T911780251_H
#define HSVCHANGEDEVENT_T911780251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVChangedEvent
struct  HSVChangedEvent_t911780251  : public UnityEvent_3_t1697774568
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVCHANGEDEVENT_T911780251_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TEXTANCHOR_T2035777396_H
#define TEXTANCHOR_T2035777396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t2035777396 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAnchor_t2035777396, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T2035777396_H
#ifndef SELECTIONCHANGEDEVENT_T1822043360_H
#define SELECTIONCHANGEDEVENT_T1822043360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionChangedEvent
struct  SelectionChangedEvent_t1822043360  : public UnityEvent_2_t364267509
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONCHANGEDEVENT_T1822043360_H
#ifndef SELECTIONTEXTCHANGEDEVENT_T4051177638_H
#define SELECTIONTEXTCHANGEDEVENT_T4051177638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionTextChangedEvent
struct  SelectionTextChangedEvent_t4051177638  : public UnityEvent_1_t2729110193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONTEXTCHANGEDEVENT_T4051177638_H
#ifndef SELECTIONVALIDITYCHANGEDEVENT_T954817928_H
#define SELECTIONVALIDITYCHANGEDEVENT_T954817928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionValidityChangedEvent
struct  SelectionValidityChangedEvent_t954817928  : public UnityEvent_1_t978947469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONVALIDITYCHANGEDEVENT_T954817928_H
#ifndef AUTOCOMPLETESEARCHTYPE_T2628651075_H
#define AUTOCOMPLETESEARCHTYPE_T2628651075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteSearchType
struct  AutoCompleteSearchType_t2628651075 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.AutoCompleteSearchType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AutoCompleteSearchType_t2628651075, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOCOMPLETESEARCHTYPE_T2628651075_H
#ifndef COLORVALUES_T950091080_H
#define COLORVALUES_T950091080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorValues
struct  ColorValues_t950091080 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.ColorPicker.ColorValues::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorValues_t950091080, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORVALUES_T950091080_H
#ifndef SELECTIONCHANGEDEVENT_T2252533886_H
#define SELECTIONCHANGEDEVENT_T2252533886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ComboBox/SelectionChangedEvent
struct  SelectionChangedEvent_t2252533886  : public UnityEvent_1_t2729110193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONCHANGEDEVENT_T2252533886_H
#ifndef COOLDOWNBUTTONEVENT_T856711112_H
#define COOLDOWNBUTTONEVENT_T856711112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CooldownButton/CooldownButtonEvent
struct  CooldownButtonEvent_t856711112  : public UnityEvent_1_t290703556
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOLDOWNBUTTONEVENT_T856711112_H
#ifndef SELECTIONCHANGEDEVENT_T1418459309_H
#define SELECTIONCHANGEDEVENT_T1418459309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.DropDownList/SelectionChangedEvent
struct  SelectionChangedEvent_t1418459309  : public UnityEvent_1_t3832605257
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONCHANGEDEVENT_T1418459309_H
#ifndef BLEND_T976317323_H
#define BLEND_T976317323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Gradient2/Blend
struct  Blend_t976317323 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.Gradient2/Blend::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Blend_t976317323, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLEND_T976317323_H
#ifndef TYPE_T3681360936_H
#define TYPE_T3681360936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Gradient2/Type
struct  Type_t3681360936 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.Gradient2/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t3681360936, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T3681360936_H
#ifndef GRADIENTDIR_T1285337419_H
#define GRADIENTDIR_T1285337419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.GradientDir
struct  GradientDir_t1285337419 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.GradientDir::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GradientDir_t1285337419, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENTDIR_T1285337419_H
#ifndef GRADIENTMODE_T3981626032_H
#define GRADIENTMODE_T3981626032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.GradientMode
struct  GradientMode_t3981626032 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.GradientMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GradientMode_t3981626032, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENTMODE_T3981626032_H
#ifndef KNOBFLOATVALUEEVENT_T1285673625_H
#define KNOBFLOATVALUEEVENT_T1285673625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.KnobFloatValueEvent
struct  KnobFloatValueEvent_t1285673625  : public UnityEvent_1_t2278926278
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KNOBFLOATVALUEEVENT_T1285673625_H
#ifndef RADIALSLIDERTEXTVALUECHANGEDEVENT_T1078616506_H
#define RADIALSLIDERTEXTVALUECHANGEDEVENT_T1078616506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.RadialSlider/RadialSliderTextValueChangedEvent
struct  RadialSliderTextValueChangedEvent_t1078616506  : public UnityEvent_1_t2729110193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RADIALSLIDERTEXTVALUECHANGEDEVENT_T1078616506_H
#ifndef RADIALSLIDERVALUECHANGEDEVENT_T1025479356_H
#define RADIALSLIDERVALUECHANGEDEVENT_T1025479356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.RadialSlider/RadialSliderValueChangedEvent
struct  RadialSliderValueChangedEvent_t1025479356  : public UnityEvent_1_t3832605257
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RADIALSLIDERVALUECHANGEDEVENT_T1025479356_H
#ifndef REORDERABLELISTHANDLER_T1290756480_H
#define REORDERABLELISTHANDLER_T1290756480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ReorderableList/ReorderableListHandler
struct  ReorderableListHandler_t1290756480  : public UnityEvent_1_t2644075916
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REORDERABLELISTHANDLER_T1290756480_H
#ifndef SEGMENTSELECTEDEVENT_T878161132_H
#define SEGMENTSELECTEDEVENT_T878161132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SegmentedControl/SegmentSelectedEvent
struct  SegmentSelectedEvent_t878161132  : public UnityEvent_1_t3832605257
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEGMENTSELECTEDEVENT_T878161132_H
#ifndef SELECTIONEVENT_T3355704588_H
#define SELECTIONEVENT_T3355704588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SelectionBox/SelectionEvent
struct  SelectionEvent_t3355704588  : public UnityEvent_1_t1064854673
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONEVENT_T3355704588_H
#ifndef STEPPERVALUECHANGEDEVENT_T1994331895_H
#define STEPPERVALUECHANGEDEVENT_T1994331895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Stepper/StepperValueChangedEvent
struct  StepperValueChangedEvent_t1994331895  : public UnityEvent_1_t3832605257
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEPPERVALUECHANGEDEVENT_T1994331895_H
#ifndef HREFCLICKEVENT_T324372001_H
#define HREFCLICKEVENT_T324372001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.TextPic/HrefClickEvent
struct  HrefClickEvent_t324372001  : public UnityEvent_1_t2729110193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HREFCLICKEVENT_T324372001_H
#ifndef ICONNAME_T399235694_H
#define ICONNAME_T399235694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.TextPic/IconName
struct  IconName_t399235694 
{
public:
	// System.String UnityEngine.UI.Extensions.TextPic/IconName::name
	String_t* ___name_0;
	// UnityEngine.Sprite UnityEngine.UI.Extensions.TextPic/IconName::sprite
	Sprite_t280657092 * ___sprite_1;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.TextPic/IconName::offset
	Vector2_t2156229523  ___offset_2;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.TextPic/IconName::scale
	Vector2_t2156229523  ___scale_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(IconName_t399235694, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_sprite_1() { return static_cast<int32_t>(offsetof(IconName_t399235694, ___sprite_1)); }
	inline Sprite_t280657092 * get_sprite_1() const { return ___sprite_1; }
	inline Sprite_t280657092 ** get_address_of_sprite_1() { return &___sprite_1; }
	inline void set_sprite_1(Sprite_t280657092 * value)
	{
		___sprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_1), value);
	}

	inline static int32_t get_offset_of_offset_2() { return static_cast<int32_t>(offsetof(IconName_t399235694, ___offset_2)); }
	inline Vector2_t2156229523  get_offset_2() const { return ___offset_2; }
	inline Vector2_t2156229523 * get_address_of_offset_2() { return &___offset_2; }
	inline void set_offset_2(Vector2_t2156229523  value)
	{
		___offset_2 = value;
	}

	inline static int32_t get_offset_of_scale_3() { return static_cast<int32_t>(offsetof(IconName_t399235694, ___scale_3)); }
	inline Vector2_t2156229523  get_scale_3() const { return ___scale_3; }
	inline Vector2_t2156229523 * get_address_of_scale_3() { return &___scale_3; }
	inline void set_scale_3(Vector2_t2156229523  value)
	{
		___scale_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Extensions.TextPic/IconName
struct IconName_t399235694_marshaled_pinvoke
{
	char* ___name_0;
	Sprite_t280657092 * ___sprite_1;
	Vector2_t2156229523  ___offset_2;
	Vector2_t2156229523  ___scale_3;
};
// Native definition for COM marshalling of UnityEngine.UI.Extensions.TextPic/IconName
struct IconName_t399235694_marshaled_com
{
	Il2CppChar* ___name_0;
	Sprite_t280657092 * ___sprite_1;
	Vector2_t2156229523  ___offset_2;
	Vector2_t2156229523  ___scale_3;
};
#endif // ICONNAME_T399235694_H
#ifndef DIRECTION_T2018151358_H
#define DIRECTION_T2018151358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UI_Knob/Direction
struct  Direction_t2018151358 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.UI_Knob/Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_t2018151358, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T2018151358_H
#ifndef MOVEMENTTYPE_T4072922106_H
#define MOVEMENTTYPE_T4072922106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect/MovementType
struct  MovementType_t4072922106 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/MovementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MovementType_t4072922106, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTTYPE_T4072922106_H
#ifndef SCROLLBARVISIBILITY_T705693775_H
#define SCROLLBARVISIBILITY_T705693775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect/ScrollbarVisibility
struct  ScrollbarVisibility_t705693775 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/ScrollbarVisibility::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScrollbarVisibility_t705693775, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLBARVISIBILITY_T705693775_H
#ifndef DIRECTION_T337909235_H
#define DIRECTION_T337909235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/Direction
struct  Direction_t337909235 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_t337909235, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T337909235_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef AUTOCOMPLETECOMBOBOX_T2765567798_H
#define AUTOCOMPLETECOMBOBOX_T2765567798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox
struct  AutoCompleteComboBox_t2765567798  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color UnityEngine.UI.Extensions.AutoCompleteComboBox::disabledTextColor
	Color_t2555686324  ___disabledTextColor_4;
	// UnityEngine.UI.Extensions.DropDownListItem UnityEngine.UI.Extensions.AutoCompleteComboBox::<SelectedItem>k__BackingField
	DropDownListItem_t337244270 * ___U3CSelectedItemU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<System.String> UnityEngine.UI.Extensions.AutoCompleteComboBox::AvailableOptions
	List_1_t3319525431 * ___AvailableOptions_6;
	// System.Boolean UnityEngine.UI.Extensions.AutoCompleteComboBox::_isPanelActive
	bool ____isPanelActive_7;
	// System.Boolean UnityEngine.UI.Extensions.AutoCompleteComboBox::_hasDrawnOnce
	bool ____hasDrawnOnce_8;
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.AutoCompleteComboBox::_mainInput
	InputField_t3762917431 * ____mainInput_9;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_inputRT
	RectTransform_t3704657025 * ____inputRT_10;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_rectTransform
	RectTransform_t3704657025 * ____rectTransform_11;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_overlayRT
	RectTransform_t3704657025 * ____overlayRT_12;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_scrollPanelRT
	RectTransform_t3704657025 * ____scrollPanelRT_13;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_scrollBarRT
	RectTransform_t3704657025 * ____scrollBarRT_14;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_slidingAreaRT
	RectTransform_t3704657025 * ____slidingAreaRT_15;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_itemsPanelRT
	RectTransform_t3704657025 * ____itemsPanelRT_16;
	// UnityEngine.Canvas UnityEngine.UI.Extensions.AutoCompleteComboBox::_canvas
	Canvas_t3310196443 * ____canvas_17;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_canvasRT
	RectTransform_t3704657025 * ____canvasRT_18;
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.AutoCompleteComboBox::_scrollRect
	ScrollRect_t4137855814 * ____scrollRect_19;
	// System.Collections.Generic.List`1<System.String> UnityEngine.UI.Extensions.AutoCompleteComboBox::_panelItems
	List_1_t3319525431 * ____panelItems_20;
	// System.Collections.Generic.List`1<System.String> UnityEngine.UI.Extensions.AutoCompleteComboBox::_prunedPanelItems
	List_1_t3319525431 * ____prunedPanelItems_21;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> UnityEngine.UI.Extensions.AutoCompleteComboBox::panelObjects
	Dictionary_2_t898892918 * ___panelObjects_22;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.AutoCompleteComboBox::itemTemplate
	GameObject_t1113636619 * ___itemTemplate_23;
	// System.String UnityEngine.UI.Extensions.AutoCompleteComboBox::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_24;
	// System.Single UnityEngine.UI.Extensions.AutoCompleteComboBox::_scrollBarWidth
	float ____scrollBarWidth_25;
	// System.Int32 UnityEngine.UI.Extensions.AutoCompleteComboBox::_itemsToDisplay
	int32_t ____itemsToDisplay_26;
	// System.Boolean UnityEngine.UI.Extensions.AutoCompleteComboBox::SelectFirstItemOnStart
	bool ___SelectFirstItemOnStart_27;
	// System.Boolean UnityEngine.UI.Extensions.AutoCompleteComboBox::_ChangeInputTextColorBasedOnMatchingItems
	bool ____ChangeInputTextColorBasedOnMatchingItems_28;
	// UnityEngine.Color UnityEngine.UI.Extensions.AutoCompleteComboBox::ValidSelectionTextColor
	Color_t2555686324  ___ValidSelectionTextColor_29;
	// UnityEngine.Color UnityEngine.UI.Extensions.AutoCompleteComboBox::MatchingItemsRemainingTextColor
	Color_t2555686324  ___MatchingItemsRemainingTextColor_30;
	// UnityEngine.Color UnityEngine.UI.Extensions.AutoCompleteComboBox::NoItemsRemainingTextColor
	Color_t2555686324  ___NoItemsRemainingTextColor_31;
	// UnityEngine.UI.Extensions.AutoCompleteSearchType UnityEngine.UI.Extensions.AutoCompleteComboBox::autocompleteSearchType
	int32_t ___autocompleteSearchType_32;
	// System.Boolean UnityEngine.UI.Extensions.AutoCompleteComboBox::_selectionIsValid
	bool ____selectionIsValid_33;
	// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionTextChangedEvent UnityEngine.UI.Extensions.AutoCompleteComboBox::OnSelectionTextChanged
	SelectionTextChangedEvent_t4051177638 * ___OnSelectionTextChanged_34;
	// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionValidityChangedEvent UnityEngine.UI.Extensions.AutoCompleteComboBox::OnSelectionValidityChanged
	SelectionValidityChangedEvent_t954817928 * ___OnSelectionValidityChanged_35;
	// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionChangedEvent UnityEngine.UI.Extensions.AutoCompleteComboBox::OnSelectionChanged
	SelectionChangedEvent_t1822043360 * ___OnSelectionChanged_36;

public:
	inline static int32_t get_offset_of_disabledTextColor_4() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___disabledTextColor_4)); }
	inline Color_t2555686324  get_disabledTextColor_4() const { return ___disabledTextColor_4; }
	inline Color_t2555686324 * get_address_of_disabledTextColor_4() { return &___disabledTextColor_4; }
	inline void set_disabledTextColor_4(Color_t2555686324  value)
	{
		___disabledTextColor_4 = value;
	}

	inline static int32_t get_offset_of_U3CSelectedItemU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___U3CSelectedItemU3Ek__BackingField_5)); }
	inline DropDownListItem_t337244270 * get_U3CSelectedItemU3Ek__BackingField_5() const { return ___U3CSelectedItemU3Ek__BackingField_5; }
	inline DropDownListItem_t337244270 ** get_address_of_U3CSelectedItemU3Ek__BackingField_5() { return &___U3CSelectedItemU3Ek__BackingField_5; }
	inline void set_U3CSelectedItemU3Ek__BackingField_5(DropDownListItem_t337244270 * value)
	{
		___U3CSelectedItemU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSelectedItemU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_AvailableOptions_6() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___AvailableOptions_6)); }
	inline List_1_t3319525431 * get_AvailableOptions_6() const { return ___AvailableOptions_6; }
	inline List_1_t3319525431 ** get_address_of_AvailableOptions_6() { return &___AvailableOptions_6; }
	inline void set_AvailableOptions_6(List_1_t3319525431 * value)
	{
		___AvailableOptions_6 = value;
		Il2CppCodeGenWriteBarrier((&___AvailableOptions_6), value);
	}

	inline static int32_t get_offset_of__isPanelActive_7() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____isPanelActive_7)); }
	inline bool get__isPanelActive_7() const { return ____isPanelActive_7; }
	inline bool* get_address_of__isPanelActive_7() { return &____isPanelActive_7; }
	inline void set__isPanelActive_7(bool value)
	{
		____isPanelActive_7 = value;
	}

	inline static int32_t get_offset_of__hasDrawnOnce_8() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____hasDrawnOnce_8)); }
	inline bool get__hasDrawnOnce_8() const { return ____hasDrawnOnce_8; }
	inline bool* get_address_of__hasDrawnOnce_8() { return &____hasDrawnOnce_8; }
	inline void set__hasDrawnOnce_8(bool value)
	{
		____hasDrawnOnce_8 = value;
	}

	inline static int32_t get_offset_of__mainInput_9() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____mainInput_9)); }
	inline InputField_t3762917431 * get__mainInput_9() const { return ____mainInput_9; }
	inline InputField_t3762917431 ** get_address_of__mainInput_9() { return &____mainInput_9; }
	inline void set__mainInput_9(InputField_t3762917431 * value)
	{
		____mainInput_9 = value;
		Il2CppCodeGenWriteBarrier((&____mainInput_9), value);
	}

	inline static int32_t get_offset_of__inputRT_10() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____inputRT_10)); }
	inline RectTransform_t3704657025 * get__inputRT_10() const { return ____inputRT_10; }
	inline RectTransform_t3704657025 ** get_address_of__inputRT_10() { return &____inputRT_10; }
	inline void set__inputRT_10(RectTransform_t3704657025 * value)
	{
		____inputRT_10 = value;
		Il2CppCodeGenWriteBarrier((&____inputRT_10), value);
	}

	inline static int32_t get_offset_of__rectTransform_11() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____rectTransform_11)); }
	inline RectTransform_t3704657025 * get__rectTransform_11() const { return ____rectTransform_11; }
	inline RectTransform_t3704657025 ** get_address_of__rectTransform_11() { return &____rectTransform_11; }
	inline void set__rectTransform_11(RectTransform_t3704657025 * value)
	{
		____rectTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&____rectTransform_11), value);
	}

	inline static int32_t get_offset_of__overlayRT_12() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____overlayRT_12)); }
	inline RectTransform_t3704657025 * get__overlayRT_12() const { return ____overlayRT_12; }
	inline RectTransform_t3704657025 ** get_address_of__overlayRT_12() { return &____overlayRT_12; }
	inline void set__overlayRT_12(RectTransform_t3704657025 * value)
	{
		____overlayRT_12 = value;
		Il2CppCodeGenWriteBarrier((&____overlayRT_12), value);
	}

	inline static int32_t get_offset_of__scrollPanelRT_13() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____scrollPanelRT_13)); }
	inline RectTransform_t3704657025 * get__scrollPanelRT_13() const { return ____scrollPanelRT_13; }
	inline RectTransform_t3704657025 ** get_address_of__scrollPanelRT_13() { return &____scrollPanelRT_13; }
	inline void set__scrollPanelRT_13(RectTransform_t3704657025 * value)
	{
		____scrollPanelRT_13 = value;
		Il2CppCodeGenWriteBarrier((&____scrollPanelRT_13), value);
	}

	inline static int32_t get_offset_of__scrollBarRT_14() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____scrollBarRT_14)); }
	inline RectTransform_t3704657025 * get__scrollBarRT_14() const { return ____scrollBarRT_14; }
	inline RectTransform_t3704657025 ** get_address_of__scrollBarRT_14() { return &____scrollBarRT_14; }
	inline void set__scrollBarRT_14(RectTransform_t3704657025 * value)
	{
		____scrollBarRT_14 = value;
		Il2CppCodeGenWriteBarrier((&____scrollBarRT_14), value);
	}

	inline static int32_t get_offset_of__slidingAreaRT_15() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____slidingAreaRT_15)); }
	inline RectTransform_t3704657025 * get__slidingAreaRT_15() const { return ____slidingAreaRT_15; }
	inline RectTransform_t3704657025 ** get_address_of__slidingAreaRT_15() { return &____slidingAreaRT_15; }
	inline void set__slidingAreaRT_15(RectTransform_t3704657025 * value)
	{
		____slidingAreaRT_15 = value;
		Il2CppCodeGenWriteBarrier((&____slidingAreaRT_15), value);
	}

	inline static int32_t get_offset_of__itemsPanelRT_16() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____itemsPanelRT_16)); }
	inline RectTransform_t3704657025 * get__itemsPanelRT_16() const { return ____itemsPanelRT_16; }
	inline RectTransform_t3704657025 ** get_address_of__itemsPanelRT_16() { return &____itemsPanelRT_16; }
	inline void set__itemsPanelRT_16(RectTransform_t3704657025 * value)
	{
		____itemsPanelRT_16 = value;
		Il2CppCodeGenWriteBarrier((&____itemsPanelRT_16), value);
	}

	inline static int32_t get_offset_of__canvas_17() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____canvas_17)); }
	inline Canvas_t3310196443 * get__canvas_17() const { return ____canvas_17; }
	inline Canvas_t3310196443 ** get_address_of__canvas_17() { return &____canvas_17; }
	inline void set__canvas_17(Canvas_t3310196443 * value)
	{
		____canvas_17 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_17), value);
	}

	inline static int32_t get_offset_of__canvasRT_18() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____canvasRT_18)); }
	inline RectTransform_t3704657025 * get__canvasRT_18() const { return ____canvasRT_18; }
	inline RectTransform_t3704657025 ** get_address_of__canvasRT_18() { return &____canvasRT_18; }
	inline void set__canvasRT_18(RectTransform_t3704657025 * value)
	{
		____canvasRT_18 = value;
		Il2CppCodeGenWriteBarrier((&____canvasRT_18), value);
	}

	inline static int32_t get_offset_of__scrollRect_19() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____scrollRect_19)); }
	inline ScrollRect_t4137855814 * get__scrollRect_19() const { return ____scrollRect_19; }
	inline ScrollRect_t4137855814 ** get_address_of__scrollRect_19() { return &____scrollRect_19; }
	inline void set__scrollRect_19(ScrollRect_t4137855814 * value)
	{
		____scrollRect_19 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_19), value);
	}

	inline static int32_t get_offset_of__panelItems_20() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____panelItems_20)); }
	inline List_1_t3319525431 * get__panelItems_20() const { return ____panelItems_20; }
	inline List_1_t3319525431 ** get_address_of__panelItems_20() { return &____panelItems_20; }
	inline void set__panelItems_20(List_1_t3319525431 * value)
	{
		____panelItems_20 = value;
		Il2CppCodeGenWriteBarrier((&____panelItems_20), value);
	}

	inline static int32_t get_offset_of__prunedPanelItems_21() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____prunedPanelItems_21)); }
	inline List_1_t3319525431 * get__prunedPanelItems_21() const { return ____prunedPanelItems_21; }
	inline List_1_t3319525431 ** get_address_of__prunedPanelItems_21() { return &____prunedPanelItems_21; }
	inline void set__prunedPanelItems_21(List_1_t3319525431 * value)
	{
		____prunedPanelItems_21 = value;
		Il2CppCodeGenWriteBarrier((&____prunedPanelItems_21), value);
	}

	inline static int32_t get_offset_of_panelObjects_22() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___panelObjects_22)); }
	inline Dictionary_2_t898892918 * get_panelObjects_22() const { return ___panelObjects_22; }
	inline Dictionary_2_t898892918 ** get_address_of_panelObjects_22() { return &___panelObjects_22; }
	inline void set_panelObjects_22(Dictionary_2_t898892918 * value)
	{
		___panelObjects_22 = value;
		Il2CppCodeGenWriteBarrier((&___panelObjects_22), value);
	}

	inline static int32_t get_offset_of_itemTemplate_23() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___itemTemplate_23)); }
	inline GameObject_t1113636619 * get_itemTemplate_23() const { return ___itemTemplate_23; }
	inline GameObject_t1113636619 ** get_address_of_itemTemplate_23() { return &___itemTemplate_23; }
	inline void set_itemTemplate_23(GameObject_t1113636619 * value)
	{
		___itemTemplate_23 = value;
		Il2CppCodeGenWriteBarrier((&___itemTemplate_23), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___U3CTextU3Ek__BackingField_24)); }
	inline String_t* get_U3CTextU3Ek__BackingField_24() const { return ___U3CTextU3Ek__BackingField_24; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_24() { return &___U3CTextU3Ek__BackingField_24; }
	inline void set_U3CTextU3Ek__BackingField_24(String_t* value)
	{
		___U3CTextU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_24), value);
	}

	inline static int32_t get_offset_of__scrollBarWidth_25() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____scrollBarWidth_25)); }
	inline float get__scrollBarWidth_25() const { return ____scrollBarWidth_25; }
	inline float* get_address_of__scrollBarWidth_25() { return &____scrollBarWidth_25; }
	inline void set__scrollBarWidth_25(float value)
	{
		____scrollBarWidth_25 = value;
	}

	inline static int32_t get_offset_of__itemsToDisplay_26() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____itemsToDisplay_26)); }
	inline int32_t get__itemsToDisplay_26() const { return ____itemsToDisplay_26; }
	inline int32_t* get_address_of__itemsToDisplay_26() { return &____itemsToDisplay_26; }
	inline void set__itemsToDisplay_26(int32_t value)
	{
		____itemsToDisplay_26 = value;
	}

	inline static int32_t get_offset_of_SelectFirstItemOnStart_27() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___SelectFirstItemOnStart_27)); }
	inline bool get_SelectFirstItemOnStart_27() const { return ___SelectFirstItemOnStart_27; }
	inline bool* get_address_of_SelectFirstItemOnStart_27() { return &___SelectFirstItemOnStart_27; }
	inline void set_SelectFirstItemOnStart_27(bool value)
	{
		___SelectFirstItemOnStart_27 = value;
	}

	inline static int32_t get_offset_of__ChangeInputTextColorBasedOnMatchingItems_28() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____ChangeInputTextColorBasedOnMatchingItems_28)); }
	inline bool get__ChangeInputTextColorBasedOnMatchingItems_28() const { return ____ChangeInputTextColorBasedOnMatchingItems_28; }
	inline bool* get_address_of__ChangeInputTextColorBasedOnMatchingItems_28() { return &____ChangeInputTextColorBasedOnMatchingItems_28; }
	inline void set__ChangeInputTextColorBasedOnMatchingItems_28(bool value)
	{
		____ChangeInputTextColorBasedOnMatchingItems_28 = value;
	}

	inline static int32_t get_offset_of_ValidSelectionTextColor_29() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___ValidSelectionTextColor_29)); }
	inline Color_t2555686324  get_ValidSelectionTextColor_29() const { return ___ValidSelectionTextColor_29; }
	inline Color_t2555686324 * get_address_of_ValidSelectionTextColor_29() { return &___ValidSelectionTextColor_29; }
	inline void set_ValidSelectionTextColor_29(Color_t2555686324  value)
	{
		___ValidSelectionTextColor_29 = value;
	}

	inline static int32_t get_offset_of_MatchingItemsRemainingTextColor_30() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___MatchingItemsRemainingTextColor_30)); }
	inline Color_t2555686324  get_MatchingItemsRemainingTextColor_30() const { return ___MatchingItemsRemainingTextColor_30; }
	inline Color_t2555686324 * get_address_of_MatchingItemsRemainingTextColor_30() { return &___MatchingItemsRemainingTextColor_30; }
	inline void set_MatchingItemsRemainingTextColor_30(Color_t2555686324  value)
	{
		___MatchingItemsRemainingTextColor_30 = value;
	}

	inline static int32_t get_offset_of_NoItemsRemainingTextColor_31() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___NoItemsRemainingTextColor_31)); }
	inline Color_t2555686324  get_NoItemsRemainingTextColor_31() const { return ___NoItemsRemainingTextColor_31; }
	inline Color_t2555686324 * get_address_of_NoItemsRemainingTextColor_31() { return &___NoItemsRemainingTextColor_31; }
	inline void set_NoItemsRemainingTextColor_31(Color_t2555686324  value)
	{
		___NoItemsRemainingTextColor_31 = value;
	}

	inline static int32_t get_offset_of_autocompleteSearchType_32() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___autocompleteSearchType_32)); }
	inline int32_t get_autocompleteSearchType_32() const { return ___autocompleteSearchType_32; }
	inline int32_t* get_address_of_autocompleteSearchType_32() { return &___autocompleteSearchType_32; }
	inline void set_autocompleteSearchType_32(int32_t value)
	{
		___autocompleteSearchType_32 = value;
	}

	inline static int32_t get_offset_of__selectionIsValid_33() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____selectionIsValid_33)); }
	inline bool get__selectionIsValid_33() const { return ____selectionIsValid_33; }
	inline bool* get_address_of__selectionIsValid_33() { return &____selectionIsValid_33; }
	inline void set__selectionIsValid_33(bool value)
	{
		____selectionIsValid_33 = value;
	}

	inline static int32_t get_offset_of_OnSelectionTextChanged_34() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___OnSelectionTextChanged_34)); }
	inline SelectionTextChangedEvent_t4051177638 * get_OnSelectionTextChanged_34() const { return ___OnSelectionTextChanged_34; }
	inline SelectionTextChangedEvent_t4051177638 ** get_address_of_OnSelectionTextChanged_34() { return &___OnSelectionTextChanged_34; }
	inline void set_OnSelectionTextChanged_34(SelectionTextChangedEvent_t4051177638 * value)
	{
		___OnSelectionTextChanged_34 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectionTextChanged_34), value);
	}

	inline static int32_t get_offset_of_OnSelectionValidityChanged_35() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___OnSelectionValidityChanged_35)); }
	inline SelectionValidityChangedEvent_t954817928 * get_OnSelectionValidityChanged_35() const { return ___OnSelectionValidityChanged_35; }
	inline SelectionValidityChangedEvent_t954817928 ** get_address_of_OnSelectionValidityChanged_35() { return &___OnSelectionValidityChanged_35; }
	inline void set_OnSelectionValidityChanged_35(SelectionValidityChangedEvent_t954817928 * value)
	{
		___OnSelectionValidityChanged_35 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectionValidityChanged_35), value);
	}

	inline static int32_t get_offset_of_OnSelectionChanged_36() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___OnSelectionChanged_36)); }
	inline SelectionChangedEvent_t1822043360 * get_OnSelectionChanged_36() const { return ___OnSelectionChanged_36; }
	inline SelectionChangedEvent_t1822043360 ** get_address_of_OnSelectionChanged_36() { return &___OnSelectionChanged_36; }
	inline void set_OnSelectionChanged_36(SelectionChangedEvent_t1822043360 * value)
	{
		___OnSelectionChanged_36 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectionChanged_36), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOCOMPLETECOMBOBOX_T2765567798_H
#ifndef CUIBEZIERCURVE_T3136617550_H
#define CUIBEZIERCURVE_T3136617550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CUIBezierCurve
struct  CUIBezierCurve_t3136617550  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3[] UnityEngine.UI.Extensions.CUIBezierCurve::controlPoints
	Vector3U5BU5D_t1718750761* ___controlPoints_5;
	// System.Action UnityEngine.UI.Extensions.CUIBezierCurve::OnRefresh
	Action_t1264377477 * ___OnRefresh_6;

public:
	inline static int32_t get_offset_of_controlPoints_5() { return static_cast<int32_t>(offsetof(CUIBezierCurve_t3136617550, ___controlPoints_5)); }
	inline Vector3U5BU5D_t1718750761* get_controlPoints_5() const { return ___controlPoints_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of_controlPoints_5() { return &___controlPoints_5; }
	inline void set_controlPoints_5(Vector3U5BU5D_t1718750761* value)
	{
		___controlPoints_5 = value;
		Il2CppCodeGenWriteBarrier((&___controlPoints_5), value);
	}

	inline static int32_t get_offset_of_OnRefresh_6() { return static_cast<int32_t>(offsetof(CUIBezierCurve_t3136617550, ___OnRefresh_6)); }
	inline Action_t1264377477 * get_OnRefresh_6() const { return ___OnRefresh_6; }
	inline Action_t1264377477 ** get_address_of_OnRefresh_6() { return &___OnRefresh_6; }
	inline void set_OnRefresh_6(Action_t1264377477 * value)
	{
		___OnRefresh_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnRefresh_6), value);
	}
};

struct CUIBezierCurve_t3136617550_StaticFields
{
public:
	// System.Int32 UnityEngine.UI.Extensions.CUIBezierCurve::CubicBezierCurvePtNum
	int32_t ___CubicBezierCurvePtNum_4;

public:
	inline static int32_t get_offset_of_CubicBezierCurvePtNum_4() { return static_cast<int32_t>(offsetof(CUIBezierCurve_t3136617550_StaticFields, ___CubicBezierCurvePtNum_4)); }
	inline int32_t get_CubicBezierCurvePtNum_4() const { return ___CubicBezierCurvePtNum_4; }
	inline int32_t* get_address_of_CubicBezierCurvePtNum_4() { return &___CubicBezierCurvePtNum_4; }
	inline void set_CubicBezierCurvePtNum_4(int32_t value)
	{
		___CubicBezierCurvePtNum_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUIBEZIERCURVE_T3136617550_H
#ifndef COLORLABEL_T1657108856_H
#define COLORLABEL_T1657108856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorLabel
struct  ColorLabel_t1657108856  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.ColorLabel::picker
	ColorPickerControl_t2793111723 * ___picker_4;
	// UnityEngine.UI.Extensions.ColorPicker.ColorValues UnityEngine.UI.Extensions.ColorPicker.ColorLabel::type
	int32_t ___type_5;
	// System.String UnityEngine.UI.Extensions.ColorPicker.ColorLabel::prefix
	String_t* ___prefix_6;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorLabel::minValue
	float ___minValue_7;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorLabel::maxValue
	float ___maxValue_8;
	// System.Int32 UnityEngine.UI.Extensions.ColorPicker.ColorLabel::precision
	int32_t ___precision_9;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.ColorPicker.ColorLabel::label
	Text_t1901882714 * ___label_10;

public:
	inline static int32_t get_offset_of_picker_4() { return static_cast<int32_t>(offsetof(ColorLabel_t1657108856, ___picker_4)); }
	inline ColorPickerControl_t2793111723 * get_picker_4() const { return ___picker_4; }
	inline ColorPickerControl_t2793111723 ** get_address_of_picker_4() { return &___picker_4; }
	inline void set_picker_4(ColorPickerControl_t2793111723 * value)
	{
		___picker_4 = value;
		Il2CppCodeGenWriteBarrier((&___picker_4), value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(ColorLabel_t1657108856, ___type_5)); }
	inline int32_t get_type_5() const { return ___type_5; }
	inline int32_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(int32_t value)
	{
		___type_5 = value;
	}

	inline static int32_t get_offset_of_prefix_6() { return static_cast<int32_t>(offsetof(ColorLabel_t1657108856, ___prefix_6)); }
	inline String_t* get_prefix_6() const { return ___prefix_6; }
	inline String_t** get_address_of_prefix_6() { return &___prefix_6; }
	inline void set_prefix_6(String_t* value)
	{
		___prefix_6 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_6), value);
	}

	inline static int32_t get_offset_of_minValue_7() { return static_cast<int32_t>(offsetof(ColorLabel_t1657108856, ___minValue_7)); }
	inline float get_minValue_7() const { return ___minValue_7; }
	inline float* get_address_of_minValue_7() { return &___minValue_7; }
	inline void set_minValue_7(float value)
	{
		___minValue_7 = value;
	}

	inline static int32_t get_offset_of_maxValue_8() { return static_cast<int32_t>(offsetof(ColorLabel_t1657108856, ___maxValue_8)); }
	inline float get_maxValue_8() const { return ___maxValue_8; }
	inline float* get_address_of_maxValue_8() { return &___maxValue_8; }
	inline void set_maxValue_8(float value)
	{
		___maxValue_8 = value;
	}

	inline static int32_t get_offset_of_precision_9() { return static_cast<int32_t>(offsetof(ColorLabel_t1657108856, ___precision_9)); }
	inline int32_t get_precision_9() const { return ___precision_9; }
	inline int32_t* get_address_of_precision_9() { return &___precision_9; }
	inline void set_precision_9(int32_t value)
	{
		___precision_9 = value;
	}

	inline static int32_t get_offset_of_label_10() { return static_cast<int32_t>(offsetof(ColorLabel_t1657108856, ___label_10)); }
	inline Text_t1901882714 * get_label_10() const { return ___label_10; }
	inline Text_t1901882714 ** get_address_of_label_10() { return &___label_10; }
	inline void set_label_10(Text_t1901882714 * value)
	{
		___label_10 = value;
		Il2CppCodeGenWriteBarrier((&___label_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORLABEL_T1657108856_H
#ifndef COLORPICKERCONTROL_T2793111723_H
#define COLORPICKERCONTROL_T2793111723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl
struct  ColorPickerControl_t2793111723  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_hue
	float ____hue_4;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_saturation
	float ____saturation_5;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_brightness
	float ____brightness_6;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_red
	float ____red_7;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_green
	float ____green_8;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_blue
	float ____blue_9;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_alpha
	float ____alpha_10;
	// ColorChangedEvent UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::onValueChanged
	ColorChangedEvent_t3019780707 * ___onValueChanged_11;
	// HSVChangedEvent UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::onHSVChanged
	HSVChangedEvent_t911780251 * ___onHSVChanged_12;

public:
	inline static int32_t get_offset_of__hue_4() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ____hue_4)); }
	inline float get__hue_4() const { return ____hue_4; }
	inline float* get_address_of__hue_4() { return &____hue_4; }
	inline void set__hue_4(float value)
	{
		____hue_4 = value;
	}

	inline static int32_t get_offset_of__saturation_5() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ____saturation_5)); }
	inline float get__saturation_5() const { return ____saturation_5; }
	inline float* get_address_of__saturation_5() { return &____saturation_5; }
	inline void set__saturation_5(float value)
	{
		____saturation_5 = value;
	}

	inline static int32_t get_offset_of__brightness_6() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ____brightness_6)); }
	inline float get__brightness_6() const { return ____brightness_6; }
	inline float* get_address_of__brightness_6() { return &____brightness_6; }
	inline void set__brightness_6(float value)
	{
		____brightness_6 = value;
	}

	inline static int32_t get_offset_of__red_7() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ____red_7)); }
	inline float get__red_7() const { return ____red_7; }
	inline float* get_address_of__red_7() { return &____red_7; }
	inline void set__red_7(float value)
	{
		____red_7 = value;
	}

	inline static int32_t get_offset_of__green_8() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ____green_8)); }
	inline float get__green_8() const { return ____green_8; }
	inline float* get_address_of__green_8() { return &____green_8; }
	inline void set__green_8(float value)
	{
		____green_8 = value;
	}

	inline static int32_t get_offset_of__blue_9() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ____blue_9)); }
	inline float get__blue_9() const { return ____blue_9; }
	inline float* get_address_of__blue_9() { return &____blue_9; }
	inline void set__blue_9(float value)
	{
		____blue_9 = value;
	}

	inline static int32_t get_offset_of__alpha_10() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ____alpha_10)); }
	inline float get__alpha_10() const { return ____alpha_10; }
	inline float* get_address_of__alpha_10() { return &____alpha_10; }
	inline void set__alpha_10(float value)
	{
		____alpha_10 = value;
	}

	inline static int32_t get_offset_of_onValueChanged_11() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ___onValueChanged_11)); }
	inline ColorChangedEvent_t3019780707 * get_onValueChanged_11() const { return ___onValueChanged_11; }
	inline ColorChangedEvent_t3019780707 ** get_address_of_onValueChanged_11() { return &___onValueChanged_11; }
	inline void set_onValueChanged_11(ColorChangedEvent_t3019780707 * value)
	{
		___onValueChanged_11 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_11), value);
	}

	inline static int32_t get_offset_of_onHSVChanged_12() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ___onHSVChanged_12)); }
	inline HSVChangedEvent_t911780251 * get_onHSVChanged_12() const { return ___onHSVChanged_12; }
	inline HSVChangedEvent_t911780251 ** get_address_of_onHSVChanged_12() { return &___onHSVChanged_12; }
	inline void set_onHSVChanged_12(HSVChangedEvent_t911780251 * value)
	{
		___onHSVChanged_12 = value;
		Il2CppCodeGenWriteBarrier((&___onHSVChanged_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKERCONTROL_T2793111723_H
#ifndef COLORPICKERPRESETS_T2164031678_H
#define COLORPICKERPRESETS_T2164031678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorPickerPresets
struct  ColorPickerPresets_t2164031678  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.ColorPickerPresets::picker
	ColorPickerControl_t2793111723 * ___picker_4;
	// UnityEngine.GameObject[] UnityEngine.UI.Extensions.ColorPicker.ColorPickerPresets::presets
	GameObjectU5BU5D_t3328599146* ___presets_5;
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.ColorPicker.ColorPickerPresets::createPresetImage
	Image_t2670269651 * ___createPresetImage_6;

public:
	inline static int32_t get_offset_of_picker_4() { return static_cast<int32_t>(offsetof(ColorPickerPresets_t2164031678, ___picker_4)); }
	inline ColorPickerControl_t2793111723 * get_picker_4() const { return ___picker_4; }
	inline ColorPickerControl_t2793111723 ** get_address_of_picker_4() { return &___picker_4; }
	inline void set_picker_4(ColorPickerControl_t2793111723 * value)
	{
		___picker_4 = value;
		Il2CppCodeGenWriteBarrier((&___picker_4), value);
	}

	inline static int32_t get_offset_of_presets_5() { return static_cast<int32_t>(offsetof(ColorPickerPresets_t2164031678, ___presets_5)); }
	inline GameObjectU5BU5D_t3328599146* get_presets_5() const { return ___presets_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_presets_5() { return &___presets_5; }
	inline void set_presets_5(GameObjectU5BU5D_t3328599146* value)
	{
		___presets_5 = value;
		Il2CppCodeGenWriteBarrier((&___presets_5), value);
	}

	inline static int32_t get_offset_of_createPresetImage_6() { return static_cast<int32_t>(offsetof(ColorPickerPresets_t2164031678, ___createPresetImage_6)); }
	inline Image_t2670269651 * get_createPresetImage_6() const { return ___createPresetImage_6; }
	inline Image_t2670269651 ** get_address_of_createPresetImage_6() { return &___createPresetImage_6; }
	inline void set_createPresetImage_6(Image_t2670269651 * value)
	{
		___createPresetImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___createPresetImage_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKERPRESETS_T2164031678_H
#ifndef COLORPICKERTESTER_T1752022104_H
#define COLORPICKERTESTER_T1752022104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorPickerTester
struct  ColorPickerTester_t1752022104  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Renderer UnityEngine.UI.Extensions.ColorPicker.ColorPickerTester::pickerRenderer
	Renderer_t2627027031 * ___pickerRenderer_4;
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.ColorPickerTester::picker
	ColorPickerControl_t2793111723 * ___picker_5;

public:
	inline static int32_t get_offset_of_pickerRenderer_4() { return static_cast<int32_t>(offsetof(ColorPickerTester_t1752022104, ___pickerRenderer_4)); }
	inline Renderer_t2627027031 * get_pickerRenderer_4() const { return ___pickerRenderer_4; }
	inline Renderer_t2627027031 ** get_address_of_pickerRenderer_4() { return &___pickerRenderer_4; }
	inline void set_pickerRenderer_4(Renderer_t2627027031 * value)
	{
		___pickerRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___pickerRenderer_4), value);
	}

	inline static int32_t get_offset_of_picker_5() { return static_cast<int32_t>(offsetof(ColorPickerTester_t1752022104, ___picker_5)); }
	inline ColorPickerControl_t2793111723 * get_picker_5() const { return ___picker_5; }
	inline ColorPickerControl_t2793111723 ** get_address_of_picker_5() { return &___picker_5; }
	inline void set_picker_5(ColorPickerControl_t2793111723 * value)
	{
		___picker_5 = value;
		Il2CppCodeGenWriteBarrier((&___picker_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKERTESTER_T1752022104_H
#ifndef COLORSLIDER_T188049029_H
#define COLORSLIDER_T188049029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorSlider
struct  ColorSlider_t188049029  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.ColorSlider::ColorPicker
	ColorPickerControl_t2793111723 * ___ColorPicker_4;
	// UnityEngine.UI.Extensions.ColorPicker.ColorValues UnityEngine.UI.Extensions.ColorPicker.ColorSlider::type
	int32_t ___type_5;
	// UnityEngine.UI.Slider UnityEngine.UI.Extensions.ColorPicker.ColorSlider::slider
	Slider_t3903728902 * ___slider_6;
	// System.Boolean UnityEngine.UI.Extensions.ColorPicker.ColorSlider::listen
	bool ___listen_7;

public:
	inline static int32_t get_offset_of_ColorPicker_4() { return static_cast<int32_t>(offsetof(ColorSlider_t188049029, ___ColorPicker_4)); }
	inline ColorPickerControl_t2793111723 * get_ColorPicker_4() const { return ___ColorPicker_4; }
	inline ColorPickerControl_t2793111723 ** get_address_of_ColorPicker_4() { return &___ColorPicker_4; }
	inline void set_ColorPicker_4(ColorPickerControl_t2793111723 * value)
	{
		___ColorPicker_4 = value;
		Il2CppCodeGenWriteBarrier((&___ColorPicker_4), value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(ColorSlider_t188049029, ___type_5)); }
	inline int32_t get_type_5() const { return ___type_5; }
	inline int32_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(int32_t value)
	{
		___type_5 = value;
	}

	inline static int32_t get_offset_of_slider_6() { return static_cast<int32_t>(offsetof(ColorSlider_t188049029, ___slider_6)); }
	inline Slider_t3903728902 * get_slider_6() const { return ___slider_6; }
	inline Slider_t3903728902 ** get_address_of_slider_6() { return &___slider_6; }
	inline void set_slider_6(Slider_t3903728902 * value)
	{
		___slider_6 = value;
		Il2CppCodeGenWriteBarrier((&___slider_6), value);
	}

	inline static int32_t get_offset_of_listen_7() { return static_cast<int32_t>(offsetof(ColorSlider_t188049029, ___listen_7)); }
	inline bool get_listen_7() const { return ___listen_7; }
	inline bool* get_address_of_listen_7() { return &___listen_7; }
	inline void set_listen_7(bool value)
	{
		___listen_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSLIDER_T188049029_H
#ifndef COLORSLIDERIMAGE_T1534316151_H
#define COLORSLIDERIMAGE_T1534316151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorSliderImage
struct  ColorSliderImage_t1534316151  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.ColorSliderImage::picker
	ColorPickerControl_t2793111723 * ___picker_4;
	// UnityEngine.UI.Extensions.ColorPicker.ColorValues UnityEngine.UI.Extensions.ColorPicker.ColorSliderImage::type
	int32_t ___type_5;
	// UnityEngine.UI.Slider/Direction UnityEngine.UI.Extensions.ColorPicker.ColorSliderImage::direction
	int32_t ___direction_6;
	// UnityEngine.UI.RawImage UnityEngine.UI.Extensions.ColorPicker.ColorSliderImage::image
	RawImage_t3182918964 * ___image_7;

public:
	inline static int32_t get_offset_of_picker_4() { return static_cast<int32_t>(offsetof(ColorSliderImage_t1534316151, ___picker_4)); }
	inline ColorPickerControl_t2793111723 * get_picker_4() const { return ___picker_4; }
	inline ColorPickerControl_t2793111723 ** get_address_of_picker_4() { return &___picker_4; }
	inline void set_picker_4(ColorPickerControl_t2793111723 * value)
	{
		___picker_4 = value;
		Il2CppCodeGenWriteBarrier((&___picker_4), value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(ColorSliderImage_t1534316151, ___type_5)); }
	inline int32_t get_type_5() const { return ___type_5; }
	inline int32_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(int32_t value)
	{
		___type_5 = value;
	}

	inline static int32_t get_offset_of_direction_6() { return static_cast<int32_t>(offsetof(ColorSliderImage_t1534316151, ___direction_6)); }
	inline int32_t get_direction_6() const { return ___direction_6; }
	inline int32_t* get_address_of_direction_6() { return &___direction_6; }
	inline void set_direction_6(int32_t value)
	{
		___direction_6 = value;
	}

	inline static int32_t get_offset_of_image_7() { return static_cast<int32_t>(offsetof(ColorSliderImage_t1534316151, ___image_7)); }
	inline RawImage_t3182918964 * get_image_7() const { return ___image_7; }
	inline RawImage_t3182918964 ** get_address_of_image_7() { return &___image_7; }
	inline void set_image_7(RawImage_t3182918964 * value)
	{
		___image_7 = value;
		Il2CppCodeGenWriteBarrier((&___image_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSLIDERIMAGE_T1534316151_H
#ifndef HEXCOLORFIELD_T106756477_H
#define HEXCOLORFIELD_T106756477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.HexColorField
struct  HexColorField_t106756477  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.HexColorField::ColorPicker
	ColorPickerControl_t2793111723 * ___ColorPicker_4;
	// System.Boolean UnityEngine.UI.Extensions.ColorPicker.HexColorField::displayAlpha
	bool ___displayAlpha_5;
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.ColorPicker.HexColorField::hexInputField
	InputField_t3762917431 * ___hexInputField_6;

public:
	inline static int32_t get_offset_of_ColorPicker_4() { return static_cast<int32_t>(offsetof(HexColorField_t106756477, ___ColorPicker_4)); }
	inline ColorPickerControl_t2793111723 * get_ColorPicker_4() const { return ___ColorPicker_4; }
	inline ColorPickerControl_t2793111723 ** get_address_of_ColorPicker_4() { return &___ColorPicker_4; }
	inline void set_ColorPicker_4(ColorPickerControl_t2793111723 * value)
	{
		___ColorPicker_4 = value;
		Il2CppCodeGenWriteBarrier((&___ColorPicker_4), value);
	}

	inline static int32_t get_offset_of_displayAlpha_5() { return static_cast<int32_t>(offsetof(HexColorField_t106756477, ___displayAlpha_5)); }
	inline bool get_displayAlpha_5() const { return ___displayAlpha_5; }
	inline bool* get_address_of_displayAlpha_5() { return &___displayAlpha_5; }
	inline void set_displayAlpha_5(bool value)
	{
		___displayAlpha_5 = value;
	}

	inline static int32_t get_offset_of_hexInputField_6() { return static_cast<int32_t>(offsetof(HexColorField_t106756477, ___hexInputField_6)); }
	inline InputField_t3762917431 * get_hexInputField_6() const { return ___hexInputField_6; }
	inline InputField_t3762917431 ** get_address_of_hexInputField_6() { return &___hexInputField_6; }
	inline void set_hexInputField_6(InputField_t3762917431 * value)
	{
		___hexInputField_6 = value;
		Il2CppCodeGenWriteBarrier((&___hexInputField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEXCOLORFIELD_T106756477_H
#ifndef SVBOXSLIDER_T1594361808_H
#define SVBOXSLIDER_T1594361808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider
struct  SVBoxSlider_t1594361808  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider::picker
	ColorPickerControl_t2793111723 * ___picker_4;
	// UnityEngine.UI.Extensions.BoxSlider UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider::slider
	BoxSlider_t3694973841 * ___slider_5;
	// UnityEngine.UI.RawImage UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider::image
	RawImage_t3182918964 * ___image_6;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider::lastH
	float ___lastH_7;
	// System.Boolean UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider::listen
	bool ___listen_8;

public:
	inline static int32_t get_offset_of_picker_4() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1594361808, ___picker_4)); }
	inline ColorPickerControl_t2793111723 * get_picker_4() const { return ___picker_4; }
	inline ColorPickerControl_t2793111723 ** get_address_of_picker_4() { return &___picker_4; }
	inline void set_picker_4(ColorPickerControl_t2793111723 * value)
	{
		___picker_4 = value;
		Il2CppCodeGenWriteBarrier((&___picker_4), value);
	}

	inline static int32_t get_offset_of_slider_5() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1594361808, ___slider_5)); }
	inline BoxSlider_t3694973841 * get_slider_5() const { return ___slider_5; }
	inline BoxSlider_t3694973841 ** get_address_of_slider_5() { return &___slider_5; }
	inline void set_slider_5(BoxSlider_t3694973841 * value)
	{
		___slider_5 = value;
		Il2CppCodeGenWriteBarrier((&___slider_5), value);
	}

	inline static int32_t get_offset_of_image_6() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1594361808, ___image_6)); }
	inline RawImage_t3182918964 * get_image_6() const { return ___image_6; }
	inline RawImage_t3182918964 ** get_address_of_image_6() { return &___image_6; }
	inline void set_image_6(RawImage_t3182918964 * value)
	{
		___image_6 = value;
		Il2CppCodeGenWriteBarrier((&___image_6), value);
	}

	inline static int32_t get_offset_of_lastH_7() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1594361808, ___lastH_7)); }
	inline float get_lastH_7() const { return ___lastH_7; }
	inline float* get_address_of_lastH_7() { return &___lastH_7; }
	inline void set_lastH_7(float value)
	{
		___lastH_7 = value;
	}

	inline static int32_t get_offset_of_listen_8() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1594361808, ___listen_8)); }
	inline bool get_listen_8() const { return ___listen_8; }
	inline bool* get_address_of_listen_8() { return &___listen_8; }
	inline void set_listen_8(bool value)
	{
		___listen_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVBOXSLIDER_T1594361808_H
#ifndef COMBOBOX_T4216213764_H
#define COMBOBOX_T4216213764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ComboBox
struct  ComboBox_t4216213764  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color UnityEngine.UI.Extensions.ComboBox::disabledTextColor
	Color_t2555686324  ___disabledTextColor_4;
	// UnityEngine.UI.Extensions.DropDownListItem UnityEngine.UI.Extensions.ComboBox::<SelectedItem>k__BackingField
	DropDownListItem_t337244270 * ___U3CSelectedItemU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<System.String> UnityEngine.UI.Extensions.ComboBox::AvailableOptions
	List_1_t3319525431 * ___AvailableOptions_6;
	// System.Single UnityEngine.UI.Extensions.ComboBox::_scrollBarWidth
	float ____scrollBarWidth_7;
	// System.Int32 UnityEngine.UI.Extensions.ComboBox::_itemsToDisplay
	int32_t ____itemsToDisplay_8;
	// UnityEngine.UI.Extensions.ComboBox/SelectionChangedEvent UnityEngine.UI.Extensions.ComboBox::OnSelectionChanged
	SelectionChangedEvent_t2252533886 * ___OnSelectionChanged_9;
	// System.Boolean UnityEngine.UI.Extensions.ComboBox::_isPanelActive
	bool ____isPanelActive_10;
	// System.Boolean UnityEngine.UI.Extensions.ComboBox::_hasDrawnOnce
	bool ____hasDrawnOnce_11;
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.ComboBox::_mainInput
	InputField_t3762917431 * ____mainInput_12;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_inputRT
	RectTransform_t3704657025 * ____inputRT_13;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_rectTransform
	RectTransform_t3704657025 * ____rectTransform_14;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_overlayRT
	RectTransform_t3704657025 * ____overlayRT_15;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_scrollPanelRT
	RectTransform_t3704657025 * ____scrollPanelRT_16;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_scrollBarRT
	RectTransform_t3704657025 * ____scrollBarRT_17;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_slidingAreaRT
	RectTransform_t3704657025 * ____slidingAreaRT_18;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_itemsPanelRT
	RectTransform_t3704657025 * ____itemsPanelRT_19;
	// UnityEngine.Canvas UnityEngine.UI.Extensions.ComboBox::_canvas
	Canvas_t3310196443 * ____canvas_20;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_canvasRT
	RectTransform_t3704657025 * ____canvasRT_21;
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.ComboBox::_scrollRect
	ScrollRect_t4137855814 * ____scrollRect_22;
	// System.Collections.Generic.List`1<System.String> UnityEngine.UI.Extensions.ComboBox::_panelItems
	List_1_t3319525431 * ____panelItems_23;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> UnityEngine.UI.Extensions.ComboBox::panelObjects
	Dictionary_2_t898892918 * ___panelObjects_24;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.ComboBox::itemTemplate
	GameObject_t1113636619 * ___itemTemplate_25;
	// System.String UnityEngine.UI.Extensions.ComboBox::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_26;

public:
	inline static int32_t get_offset_of_disabledTextColor_4() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ___disabledTextColor_4)); }
	inline Color_t2555686324  get_disabledTextColor_4() const { return ___disabledTextColor_4; }
	inline Color_t2555686324 * get_address_of_disabledTextColor_4() { return &___disabledTextColor_4; }
	inline void set_disabledTextColor_4(Color_t2555686324  value)
	{
		___disabledTextColor_4 = value;
	}

	inline static int32_t get_offset_of_U3CSelectedItemU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ___U3CSelectedItemU3Ek__BackingField_5)); }
	inline DropDownListItem_t337244270 * get_U3CSelectedItemU3Ek__BackingField_5() const { return ___U3CSelectedItemU3Ek__BackingField_5; }
	inline DropDownListItem_t337244270 ** get_address_of_U3CSelectedItemU3Ek__BackingField_5() { return &___U3CSelectedItemU3Ek__BackingField_5; }
	inline void set_U3CSelectedItemU3Ek__BackingField_5(DropDownListItem_t337244270 * value)
	{
		___U3CSelectedItemU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSelectedItemU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_AvailableOptions_6() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ___AvailableOptions_6)); }
	inline List_1_t3319525431 * get_AvailableOptions_6() const { return ___AvailableOptions_6; }
	inline List_1_t3319525431 ** get_address_of_AvailableOptions_6() { return &___AvailableOptions_6; }
	inline void set_AvailableOptions_6(List_1_t3319525431 * value)
	{
		___AvailableOptions_6 = value;
		Il2CppCodeGenWriteBarrier((&___AvailableOptions_6), value);
	}

	inline static int32_t get_offset_of__scrollBarWidth_7() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____scrollBarWidth_7)); }
	inline float get__scrollBarWidth_7() const { return ____scrollBarWidth_7; }
	inline float* get_address_of__scrollBarWidth_7() { return &____scrollBarWidth_7; }
	inline void set__scrollBarWidth_7(float value)
	{
		____scrollBarWidth_7 = value;
	}

	inline static int32_t get_offset_of__itemsToDisplay_8() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____itemsToDisplay_8)); }
	inline int32_t get__itemsToDisplay_8() const { return ____itemsToDisplay_8; }
	inline int32_t* get_address_of__itemsToDisplay_8() { return &____itemsToDisplay_8; }
	inline void set__itemsToDisplay_8(int32_t value)
	{
		____itemsToDisplay_8 = value;
	}

	inline static int32_t get_offset_of_OnSelectionChanged_9() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ___OnSelectionChanged_9)); }
	inline SelectionChangedEvent_t2252533886 * get_OnSelectionChanged_9() const { return ___OnSelectionChanged_9; }
	inline SelectionChangedEvent_t2252533886 ** get_address_of_OnSelectionChanged_9() { return &___OnSelectionChanged_9; }
	inline void set_OnSelectionChanged_9(SelectionChangedEvent_t2252533886 * value)
	{
		___OnSelectionChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectionChanged_9), value);
	}

	inline static int32_t get_offset_of__isPanelActive_10() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____isPanelActive_10)); }
	inline bool get__isPanelActive_10() const { return ____isPanelActive_10; }
	inline bool* get_address_of__isPanelActive_10() { return &____isPanelActive_10; }
	inline void set__isPanelActive_10(bool value)
	{
		____isPanelActive_10 = value;
	}

	inline static int32_t get_offset_of__hasDrawnOnce_11() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____hasDrawnOnce_11)); }
	inline bool get__hasDrawnOnce_11() const { return ____hasDrawnOnce_11; }
	inline bool* get_address_of__hasDrawnOnce_11() { return &____hasDrawnOnce_11; }
	inline void set__hasDrawnOnce_11(bool value)
	{
		____hasDrawnOnce_11 = value;
	}

	inline static int32_t get_offset_of__mainInput_12() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____mainInput_12)); }
	inline InputField_t3762917431 * get__mainInput_12() const { return ____mainInput_12; }
	inline InputField_t3762917431 ** get_address_of__mainInput_12() { return &____mainInput_12; }
	inline void set__mainInput_12(InputField_t3762917431 * value)
	{
		____mainInput_12 = value;
		Il2CppCodeGenWriteBarrier((&____mainInput_12), value);
	}

	inline static int32_t get_offset_of__inputRT_13() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____inputRT_13)); }
	inline RectTransform_t3704657025 * get__inputRT_13() const { return ____inputRT_13; }
	inline RectTransform_t3704657025 ** get_address_of__inputRT_13() { return &____inputRT_13; }
	inline void set__inputRT_13(RectTransform_t3704657025 * value)
	{
		____inputRT_13 = value;
		Il2CppCodeGenWriteBarrier((&____inputRT_13), value);
	}

	inline static int32_t get_offset_of__rectTransform_14() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____rectTransform_14)); }
	inline RectTransform_t3704657025 * get__rectTransform_14() const { return ____rectTransform_14; }
	inline RectTransform_t3704657025 ** get_address_of__rectTransform_14() { return &____rectTransform_14; }
	inline void set__rectTransform_14(RectTransform_t3704657025 * value)
	{
		____rectTransform_14 = value;
		Il2CppCodeGenWriteBarrier((&____rectTransform_14), value);
	}

	inline static int32_t get_offset_of__overlayRT_15() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____overlayRT_15)); }
	inline RectTransform_t3704657025 * get__overlayRT_15() const { return ____overlayRT_15; }
	inline RectTransform_t3704657025 ** get_address_of__overlayRT_15() { return &____overlayRT_15; }
	inline void set__overlayRT_15(RectTransform_t3704657025 * value)
	{
		____overlayRT_15 = value;
		Il2CppCodeGenWriteBarrier((&____overlayRT_15), value);
	}

	inline static int32_t get_offset_of__scrollPanelRT_16() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____scrollPanelRT_16)); }
	inline RectTransform_t3704657025 * get__scrollPanelRT_16() const { return ____scrollPanelRT_16; }
	inline RectTransform_t3704657025 ** get_address_of__scrollPanelRT_16() { return &____scrollPanelRT_16; }
	inline void set__scrollPanelRT_16(RectTransform_t3704657025 * value)
	{
		____scrollPanelRT_16 = value;
		Il2CppCodeGenWriteBarrier((&____scrollPanelRT_16), value);
	}

	inline static int32_t get_offset_of__scrollBarRT_17() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____scrollBarRT_17)); }
	inline RectTransform_t3704657025 * get__scrollBarRT_17() const { return ____scrollBarRT_17; }
	inline RectTransform_t3704657025 ** get_address_of__scrollBarRT_17() { return &____scrollBarRT_17; }
	inline void set__scrollBarRT_17(RectTransform_t3704657025 * value)
	{
		____scrollBarRT_17 = value;
		Il2CppCodeGenWriteBarrier((&____scrollBarRT_17), value);
	}

	inline static int32_t get_offset_of__slidingAreaRT_18() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____slidingAreaRT_18)); }
	inline RectTransform_t3704657025 * get__slidingAreaRT_18() const { return ____slidingAreaRT_18; }
	inline RectTransform_t3704657025 ** get_address_of__slidingAreaRT_18() { return &____slidingAreaRT_18; }
	inline void set__slidingAreaRT_18(RectTransform_t3704657025 * value)
	{
		____slidingAreaRT_18 = value;
		Il2CppCodeGenWriteBarrier((&____slidingAreaRT_18), value);
	}

	inline static int32_t get_offset_of__itemsPanelRT_19() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____itemsPanelRT_19)); }
	inline RectTransform_t3704657025 * get__itemsPanelRT_19() const { return ____itemsPanelRT_19; }
	inline RectTransform_t3704657025 ** get_address_of__itemsPanelRT_19() { return &____itemsPanelRT_19; }
	inline void set__itemsPanelRT_19(RectTransform_t3704657025 * value)
	{
		____itemsPanelRT_19 = value;
		Il2CppCodeGenWriteBarrier((&____itemsPanelRT_19), value);
	}

	inline static int32_t get_offset_of__canvas_20() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____canvas_20)); }
	inline Canvas_t3310196443 * get__canvas_20() const { return ____canvas_20; }
	inline Canvas_t3310196443 ** get_address_of__canvas_20() { return &____canvas_20; }
	inline void set__canvas_20(Canvas_t3310196443 * value)
	{
		____canvas_20 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_20), value);
	}

	inline static int32_t get_offset_of__canvasRT_21() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____canvasRT_21)); }
	inline RectTransform_t3704657025 * get__canvasRT_21() const { return ____canvasRT_21; }
	inline RectTransform_t3704657025 ** get_address_of__canvasRT_21() { return &____canvasRT_21; }
	inline void set__canvasRT_21(RectTransform_t3704657025 * value)
	{
		____canvasRT_21 = value;
		Il2CppCodeGenWriteBarrier((&____canvasRT_21), value);
	}

	inline static int32_t get_offset_of__scrollRect_22() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____scrollRect_22)); }
	inline ScrollRect_t4137855814 * get__scrollRect_22() const { return ____scrollRect_22; }
	inline ScrollRect_t4137855814 ** get_address_of__scrollRect_22() { return &____scrollRect_22; }
	inline void set__scrollRect_22(ScrollRect_t4137855814 * value)
	{
		____scrollRect_22 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_22), value);
	}

	inline static int32_t get_offset_of__panelItems_23() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____panelItems_23)); }
	inline List_1_t3319525431 * get__panelItems_23() const { return ____panelItems_23; }
	inline List_1_t3319525431 ** get_address_of__panelItems_23() { return &____panelItems_23; }
	inline void set__panelItems_23(List_1_t3319525431 * value)
	{
		____panelItems_23 = value;
		Il2CppCodeGenWriteBarrier((&____panelItems_23), value);
	}

	inline static int32_t get_offset_of_panelObjects_24() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ___panelObjects_24)); }
	inline Dictionary_2_t898892918 * get_panelObjects_24() const { return ___panelObjects_24; }
	inline Dictionary_2_t898892918 ** get_address_of_panelObjects_24() { return &___panelObjects_24; }
	inline void set_panelObjects_24(Dictionary_2_t898892918 * value)
	{
		___panelObjects_24 = value;
		Il2CppCodeGenWriteBarrier((&___panelObjects_24), value);
	}

	inline static int32_t get_offset_of_itemTemplate_25() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ___itemTemplate_25)); }
	inline GameObject_t1113636619 * get_itemTemplate_25() const { return ___itemTemplate_25; }
	inline GameObject_t1113636619 ** get_address_of_itemTemplate_25() { return &___itemTemplate_25; }
	inline void set_itemTemplate_25(GameObject_t1113636619 * value)
	{
		___itemTemplate_25 = value;
		Il2CppCodeGenWriteBarrier((&___itemTemplate_25), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ___U3CTextU3Ek__BackingField_26)); }
	inline String_t* get_U3CTextU3Ek__BackingField_26() const { return ___U3CTextU3Ek__BackingField_26; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_26() { return &___U3CTextU3Ek__BackingField_26; }
	inline void set_U3CTextU3Ek__BackingField_26(String_t* value)
	{
		___U3CTextU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMBOBOX_T4216213764_H
#ifndef COOLDOWNBUTTON_T2372397950_H
#define COOLDOWNBUTTON_T2372397950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CooldownButton
struct  CooldownButton_t2372397950  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityEngine.UI.Extensions.CooldownButton::cooldownTimeout
	float ___cooldownTimeout_4;
	// System.Single UnityEngine.UI.Extensions.CooldownButton::cooldownSpeed
	float ___cooldownSpeed_5;
	// System.Boolean UnityEngine.UI.Extensions.CooldownButton::cooldownActive
	bool ___cooldownActive_6;
	// System.Boolean UnityEngine.UI.Extensions.CooldownButton::cooldownInEffect
	bool ___cooldownInEffect_7;
	// System.Single UnityEngine.UI.Extensions.CooldownButton::cooldownTimeElapsed
	float ___cooldownTimeElapsed_8;
	// System.Single UnityEngine.UI.Extensions.CooldownButton::cooldownTimeRemaining
	float ___cooldownTimeRemaining_9;
	// System.Int32 UnityEngine.UI.Extensions.CooldownButton::cooldownPercentRemaining
	int32_t ___cooldownPercentRemaining_10;
	// System.Int32 UnityEngine.UI.Extensions.CooldownButton::cooldownPercentComplete
	int32_t ___cooldownPercentComplete_11;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.UI.Extensions.CooldownButton::buttonSource
	PointerEventData_t3807901092 * ___buttonSource_12;
	// UnityEngine.UI.Extensions.CooldownButton/CooldownButtonEvent UnityEngine.UI.Extensions.CooldownButton::OnCooldownStart
	CooldownButtonEvent_t856711112 * ___OnCooldownStart_13;
	// UnityEngine.UI.Extensions.CooldownButton/CooldownButtonEvent UnityEngine.UI.Extensions.CooldownButton::OnButtonClickDuringCooldown
	CooldownButtonEvent_t856711112 * ___OnButtonClickDuringCooldown_14;
	// UnityEngine.UI.Extensions.CooldownButton/CooldownButtonEvent UnityEngine.UI.Extensions.CooldownButton::OnCoolDownFinish
	CooldownButtonEvent_t856711112 * ___OnCoolDownFinish_15;

public:
	inline static int32_t get_offset_of_cooldownTimeout_4() { return static_cast<int32_t>(offsetof(CooldownButton_t2372397950, ___cooldownTimeout_4)); }
	inline float get_cooldownTimeout_4() const { return ___cooldownTimeout_4; }
	inline float* get_address_of_cooldownTimeout_4() { return &___cooldownTimeout_4; }
	inline void set_cooldownTimeout_4(float value)
	{
		___cooldownTimeout_4 = value;
	}

	inline static int32_t get_offset_of_cooldownSpeed_5() { return static_cast<int32_t>(offsetof(CooldownButton_t2372397950, ___cooldownSpeed_5)); }
	inline float get_cooldownSpeed_5() const { return ___cooldownSpeed_5; }
	inline float* get_address_of_cooldownSpeed_5() { return &___cooldownSpeed_5; }
	inline void set_cooldownSpeed_5(float value)
	{
		___cooldownSpeed_5 = value;
	}

	inline static int32_t get_offset_of_cooldownActive_6() { return static_cast<int32_t>(offsetof(CooldownButton_t2372397950, ___cooldownActive_6)); }
	inline bool get_cooldownActive_6() const { return ___cooldownActive_6; }
	inline bool* get_address_of_cooldownActive_6() { return &___cooldownActive_6; }
	inline void set_cooldownActive_6(bool value)
	{
		___cooldownActive_6 = value;
	}

	inline static int32_t get_offset_of_cooldownInEffect_7() { return static_cast<int32_t>(offsetof(CooldownButton_t2372397950, ___cooldownInEffect_7)); }
	inline bool get_cooldownInEffect_7() const { return ___cooldownInEffect_7; }
	inline bool* get_address_of_cooldownInEffect_7() { return &___cooldownInEffect_7; }
	inline void set_cooldownInEffect_7(bool value)
	{
		___cooldownInEffect_7 = value;
	}

	inline static int32_t get_offset_of_cooldownTimeElapsed_8() { return static_cast<int32_t>(offsetof(CooldownButton_t2372397950, ___cooldownTimeElapsed_8)); }
	inline float get_cooldownTimeElapsed_8() const { return ___cooldownTimeElapsed_8; }
	inline float* get_address_of_cooldownTimeElapsed_8() { return &___cooldownTimeElapsed_8; }
	inline void set_cooldownTimeElapsed_8(float value)
	{
		___cooldownTimeElapsed_8 = value;
	}

	inline static int32_t get_offset_of_cooldownTimeRemaining_9() { return static_cast<int32_t>(offsetof(CooldownButton_t2372397950, ___cooldownTimeRemaining_9)); }
	inline float get_cooldownTimeRemaining_9() const { return ___cooldownTimeRemaining_9; }
	inline float* get_address_of_cooldownTimeRemaining_9() { return &___cooldownTimeRemaining_9; }
	inline void set_cooldownTimeRemaining_9(float value)
	{
		___cooldownTimeRemaining_9 = value;
	}

	inline static int32_t get_offset_of_cooldownPercentRemaining_10() { return static_cast<int32_t>(offsetof(CooldownButton_t2372397950, ___cooldownPercentRemaining_10)); }
	inline int32_t get_cooldownPercentRemaining_10() const { return ___cooldownPercentRemaining_10; }
	inline int32_t* get_address_of_cooldownPercentRemaining_10() { return &___cooldownPercentRemaining_10; }
	inline void set_cooldownPercentRemaining_10(int32_t value)
	{
		___cooldownPercentRemaining_10 = value;
	}

	inline static int32_t get_offset_of_cooldownPercentComplete_11() { return static_cast<int32_t>(offsetof(CooldownButton_t2372397950, ___cooldownPercentComplete_11)); }
	inline int32_t get_cooldownPercentComplete_11() const { return ___cooldownPercentComplete_11; }
	inline int32_t* get_address_of_cooldownPercentComplete_11() { return &___cooldownPercentComplete_11; }
	inline void set_cooldownPercentComplete_11(int32_t value)
	{
		___cooldownPercentComplete_11 = value;
	}

	inline static int32_t get_offset_of_buttonSource_12() { return static_cast<int32_t>(offsetof(CooldownButton_t2372397950, ___buttonSource_12)); }
	inline PointerEventData_t3807901092 * get_buttonSource_12() const { return ___buttonSource_12; }
	inline PointerEventData_t3807901092 ** get_address_of_buttonSource_12() { return &___buttonSource_12; }
	inline void set_buttonSource_12(PointerEventData_t3807901092 * value)
	{
		___buttonSource_12 = value;
		Il2CppCodeGenWriteBarrier((&___buttonSource_12), value);
	}

	inline static int32_t get_offset_of_OnCooldownStart_13() { return static_cast<int32_t>(offsetof(CooldownButton_t2372397950, ___OnCooldownStart_13)); }
	inline CooldownButtonEvent_t856711112 * get_OnCooldownStart_13() const { return ___OnCooldownStart_13; }
	inline CooldownButtonEvent_t856711112 ** get_address_of_OnCooldownStart_13() { return &___OnCooldownStart_13; }
	inline void set_OnCooldownStart_13(CooldownButtonEvent_t856711112 * value)
	{
		___OnCooldownStart_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnCooldownStart_13), value);
	}

	inline static int32_t get_offset_of_OnButtonClickDuringCooldown_14() { return static_cast<int32_t>(offsetof(CooldownButton_t2372397950, ___OnButtonClickDuringCooldown_14)); }
	inline CooldownButtonEvent_t856711112 * get_OnButtonClickDuringCooldown_14() const { return ___OnButtonClickDuringCooldown_14; }
	inline CooldownButtonEvent_t856711112 ** get_address_of_OnButtonClickDuringCooldown_14() { return &___OnButtonClickDuringCooldown_14; }
	inline void set_OnButtonClickDuringCooldown_14(CooldownButtonEvent_t856711112 * value)
	{
		___OnButtonClickDuringCooldown_14 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonClickDuringCooldown_14), value);
	}

	inline static int32_t get_offset_of_OnCoolDownFinish_15() { return static_cast<int32_t>(offsetof(CooldownButton_t2372397950, ___OnCoolDownFinish_15)); }
	inline CooldownButtonEvent_t856711112 * get_OnCoolDownFinish_15() const { return ___OnCoolDownFinish_15; }
	inline CooldownButtonEvent_t856711112 ** get_address_of_OnCoolDownFinish_15() { return &___OnCoolDownFinish_15; }
	inline void set_OnCoolDownFinish_15(CooldownButtonEvent_t856711112 * value)
	{
		___OnCoolDownFinish_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnCoolDownFinish_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOLDOWNBUTTON_T2372397950_H
#ifndef DROPDOWNLIST_T4179439446_H
#define DROPDOWNLIST_T4179439446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.DropDownList
struct  DropDownList_t4179439446  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color UnityEngine.UI.Extensions.DropDownList::disabledTextColor
	Color_t2555686324  ___disabledTextColor_4;
	// UnityEngine.UI.Extensions.DropDownListItem UnityEngine.UI.Extensions.DropDownList::<SelectedItem>k__BackingField
	DropDownListItem_t337244270 * ___U3CSelectedItemU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.DropDownListItem> UnityEngine.UI.Extensions.DropDownList::Items
	List_1_t1809319012 * ___Items_6;
	// System.Boolean UnityEngine.UI.Extensions.DropDownList::OverrideHighlighted
	bool ___OverrideHighlighted_7;
	// System.Boolean UnityEngine.UI.Extensions.DropDownList::_isPanelActive
	bool ____isPanelActive_8;
	// System.Boolean UnityEngine.UI.Extensions.DropDownList::_hasDrawnOnce
	bool ____hasDrawnOnce_9;
	// UnityEngine.UI.Extensions.DropDownListButton UnityEngine.UI.Extensions.DropDownList::_mainButton
	DropDownListButton_t3241764549 * ____mainButton_10;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.DropDownList::_rectTransform
	RectTransform_t3704657025 * ____rectTransform_11;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.DropDownList::_overlayRT
	RectTransform_t3704657025 * ____overlayRT_12;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.DropDownList::_scrollPanelRT
	RectTransform_t3704657025 * ____scrollPanelRT_13;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.DropDownList::_scrollBarRT
	RectTransform_t3704657025 * ____scrollBarRT_14;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.DropDownList::_slidingAreaRT
	RectTransform_t3704657025 * ____slidingAreaRT_15;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.DropDownList::_itemsPanelRT
	RectTransform_t3704657025 * ____itemsPanelRT_16;
	// UnityEngine.Canvas UnityEngine.UI.Extensions.DropDownList::_canvas
	Canvas_t3310196443 * ____canvas_17;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.DropDownList::_canvasRT
	RectTransform_t3704657025 * ____canvasRT_18;
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.DropDownList::_scrollRect
	ScrollRect_t4137855814 * ____scrollRect_19;
	// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.DropDownListButton> UnityEngine.UI.Extensions.DropDownList::_panelItems
	List_1_t418871995 * ____panelItems_20;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.DropDownList::_itemTemplate
	GameObject_t1113636619 * ____itemTemplate_21;
	// System.Single UnityEngine.UI.Extensions.DropDownList::_scrollBarWidth
	float ____scrollBarWidth_22;
	// System.Int32 UnityEngine.UI.Extensions.DropDownList::_selectedIndex
	int32_t ____selectedIndex_23;
	// System.Int32 UnityEngine.UI.Extensions.DropDownList::_itemsToDisplay
	int32_t ____itemsToDisplay_24;
	// System.Boolean UnityEngine.UI.Extensions.DropDownList::SelectFirstItemOnStart
	bool ___SelectFirstItemOnStart_25;
	// UnityEngine.UI.Extensions.DropDownList/SelectionChangedEvent UnityEngine.UI.Extensions.DropDownList::OnSelectionChanged
	SelectionChangedEvent_t1418459309 * ___OnSelectionChanged_26;

public:
	inline static int32_t get_offset_of_disabledTextColor_4() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ___disabledTextColor_4)); }
	inline Color_t2555686324  get_disabledTextColor_4() const { return ___disabledTextColor_4; }
	inline Color_t2555686324 * get_address_of_disabledTextColor_4() { return &___disabledTextColor_4; }
	inline void set_disabledTextColor_4(Color_t2555686324  value)
	{
		___disabledTextColor_4 = value;
	}

	inline static int32_t get_offset_of_U3CSelectedItemU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ___U3CSelectedItemU3Ek__BackingField_5)); }
	inline DropDownListItem_t337244270 * get_U3CSelectedItemU3Ek__BackingField_5() const { return ___U3CSelectedItemU3Ek__BackingField_5; }
	inline DropDownListItem_t337244270 ** get_address_of_U3CSelectedItemU3Ek__BackingField_5() { return &___U3CSelectedItemU3Ek__BackingField_5; }
	inline void set_U3CSelectedItemU3Ek__BackingField_5(DropDownListItem_t337244270 * value)
	{
		___U3CSelectedItemU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSelectedItemU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_Items_6() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ___Items_6)); }
	inline List_1_t1809319012 * get_Items_6() const { return ___Items_6; }
	inline List_1_t1809319012 ** get_address_of_Items_6() { return &___Items_6; }
	inline void set_Items_6(List_1_t1809319012 * value)
	{
		___Items_6 = value;
		Il2CppCodeGenWriteBarrier((&___Items_6), value);
	}

	inline static int32_t get_offset_of_OverrideHighlighted_7() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ___OverrideHighlighted_7)); }
	inline bool get_OverrideHighlighted_7() const { return ___OverrideHighlighted_7; }
	inline bool* get_address_of_OverrideHighlighted_7() { return &___OverrideHighlighted_7; }
	inline void set_OverrideHighlighted_7(bool value)
	{
		___OverrideHighlighted_7 = value;
	}

	inline static int32_t get_offset_of__isPanelActive_8() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ____isPanelActive_8)); }
	inline bool get__isPanelActive_8() const { return ____isPanelActive_8; }
	inline bool* get_address_of__isPanelActive_8() { return &____isPanelActive_8; }
	inline void set__isPanelActive_8(bool value)
	{
		____isPanelActive_8 = value;
	}

	inline static int32_t get_offset_of__hasDrawnOnce_9() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ____hasDrawnOnce_9)); }
	inline bool get__hasDrawnOnce_9() const { return ____hasDrawnOnce_9; }
	inline bool* get_address_of__hasDrawnOnce_9() { return &____hasDrawnOnce_9; }
	inline void set__hasDrawnOnce_9(bool value)
	{
		____hasDrawnOnce_9 = value;
	}

	inline static int32_t get_offset_of__mainButton_10() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ____mainButton_10)); }
	inline DropDownListButton_t3241764549 * get__mainButton_10() const { return ____mainButton_10; }
	inline DropDownListButton_t3241764549 ** get_address_of__mainButton_10() { return &____mainButton_10; }
	inline void set__mainButton_10(DropDownListButton_t3241764549 * value)
	{
		____mainButton_10 = value;
		Il2CppCodeGenWriteBarrier((&____mainButton_10), value);
	}

	inline static int32_t get_offset_of__rectTransform_11() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ____rectTransform_11)); }
	inline RectTransform_t3704657025 * get__rectTransform_11() const { return ____rectTransform_11; }
	inline RectTransform_t3704657025 ** get_address_of__rectTransform_11() { return &____rectTransform_11; }
	inline void set__rectTransform_11(RectTransform_t3704657025 * value)
	{
		____rectTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&____rectTransform_11), value);
	}

	inline static int32_t get_offset_of__overlayRT_12() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ____overlayRT_12)); }
	inline RectTransform_t3704657025 * get__overlayRT_12() const { return ____overlayRT_12; }
	inline RectTransform_t3704657025 ** get_address_of__overlayRT_12() { return &____overlayRT_12; }
	inline void set__overlayRT_12(RectTransform_t3704657025 * value)
	{
		____overlayRT_12 = value;
		Il2CppCodeGenWriteBarrier((&____overlayRT_12), value);
	}

	inline static int32_t get_offset_of__scrollPanelRT_13() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ____scrollPanelRT_13)); }
	inline RectTransform_t3704657025 * get__scrollPanelRT_13() const { return ____scrollPanelRT_13; }
	inline RectTransform_t3704657025 ** get_address_of__scrollPanelRT_13() { return &____scrollPanelRT_13; }
	inline void set__scrollPanelRT_13(RectTransform_t3704657025 * value)
	{
		____scrollPanelRT_13 = value;
		Il2CppCodeGenWriteBarrier((&____scrollPanelRT_13), value);
	}

	inline static int32_t get_offset_of__scrollBarRT_14() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ____scrollBarRT_14)); }
	inline RectTransform_t3704657025 * get__scrollBarRT_14() const { return ____scrollBarRT_14; }
	inline RectTransform_t3704657025 ** get_address_of__scrollBarRT_14() { return &____scrollBarRT_14; }
	inline void set__scrollBarRT_14(RectTransform_t3704657025 * value)
	{
		____scrollBarRT_14 = value;
		Il2CppCodeGenWriteBarrier((&____scrollBarRT_14), value);
	}

	inline static int32_t get_offset_of__slidingAreaRT_15() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ____slidingAreaRT_15)); }
	inline RectTransform_t3704657025 * get__slidingAreaRT_15() const { return ____slidingAreaRT_15; }
	inline RectTransform_t3704657025 ** get_address_of__slidingAreaRT_15() { return &____slidingAreaRT_15; }
	inline void set__slidingAreaRT_15(RectTransform_t3704657025 * value)
	{
		____slidingAreaRT_15 = value;
		Il2CppCodeGenWriteBarrier((&____slidingAreaRT_15), value);
	}

	inline static int32_t get_offset_of__itemsPanelRT_16() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ____itemsPanelRT_16)); }
	inline RectTransform_t3704657025 * get__itemsPanelRT_16() const { return ____itemsPanelRT_16; }
	inline RectTransform_t3704657025 ** get_address_of__itemsPanelRT_16() { return &____itemsPanelRT_16; }
	inline void set__itemsPanelRT_16(RectTransform_t3704657025 * value)
	{
		____itemsPanelRT_16 = value;
		Il2CppCodeGenWriteBarrier((&____itemsPanelRT_16), value);
	}

	inline static int32_t get_offset_of__canvas_17() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ____canvas_17)); }
	inline Canvas_t3310196443 * get__canvas_17() const { return ____canvas_17; }
	inline Canvas_t3310196443 ** get_address_of__canvas_17() { return &____canvas_17; }
	inline void set__canvas_17(Canvas_t3310196443 * value)
	{
		____canvas_17 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_17), value);
	}

	inline static int32_t get_offset_of__canvasRT_18() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ____canvasRT_18)); }
	inline RectTransform_t3704657025 * get__canvasRT_18() const { return ____canvasRT_18; }
	inline RectTransform_t3704657025 ** get_address_of__canvasRT_18() { return &____canvasRT_18; }
	inline void set__canvasRT_18(RectTransform_t3704657025 * value)
	{
		____canvasRT_18 = value;
		Il2CppCodeGenWriteBarrier((&____canvasRT_18), value);
	}

	inline static int32_t get_offset_of__scrollRect_19() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ____scrollRect_19)); }
	inline ScrollRect_t4137855814 * get__scrollRect_19() const { return ____scrollRect_19; }
	inline ScrollRect_t4137855814 ** get_address_of__scrollRect_19() { return &____scrollRect_19; }
	inline void set__scrollRect_19(ScrollRect_t4137855814 * value)
	{
		____scrollRect_19 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_19), value);
	}

	inline static int32_t get_offset_of__panelItems_20() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ____panelItems_20)); }
	inline List_1_t418871995 * get__panelItems_20() const { return ____panelItems_20; }
	inline List_1_t418871995 ** get_address_of__panelItems_20() { return &____panelItems_20; }
	inline void set__panelItems_20(List_1_t418871995 * value)
	{
		____panelItems_20 = value;
		Il2CppCodeGenWriteBarrier((&____panelItems_20), value);
	}

	inline static int32_t get_offset_of__itemTemplate_21() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ____itemTemplate_21)); }
	inline GameObject_t1113636619 * get__itemTemplate_21() const { return ____itemTemplate_21; }
	inline GameObject_t1113636619 ** get_address_of__itemTemplate_21() { return &____itemTemplate_21; }
	inline void set__itemTemplate_21(GameObject_t1113636619 * value)
	{
		____itemTemplate_21 = value;
		Il2CppCodeGenWriteBarrier((&____itemTemplate_21), value);
	}

	inline static int32_t get_offset_of__scrollBarWidth_22() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ____scrollBarWidth_22)); }
	inline float get__scrollBarWidth_22() const { return ____scrollBarWidth_22; }
	inline float* get_address_of__scrollBarWidth_22() { return &____scrollBarWidth_22; }
	inline void set__scrollBarWidth_22(float value)
	{
		____scrollBarWidth_22 = value;
	}

	inline static int32_t get_offset_of__selectedIndex_23() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ____selectedIndex_23)); }
	inline int32_t get__selectedIndex_23() const { return ____selectedIndex_23; }
	inline int32_t* get_address_of__selectedIndex_23() { return &____selectedIndex_23; }
	inline void set__selectedIndex_23(int32_t value)
	{
		____selectedIndex_23 = value;
	}

	inline static int32_t get_offset_of__itemsToDisplay_24() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ____itemsToDisplay_24)); }
	inline int32_t get__itemsToDisplay_24() const { return ____itemsToDisplay_24; }
	inline int32_t* get_address_of__itemsToDisplay_24() { return &____itemsToDisplay_24; }
	inline void set__itemsToDisplay_24(int32_t value)
	{
		____itemsToDisplay_24 = value;
	}

	inline static int32_t get_offset_of_SelectFirstItemOnStart_25() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ___SelectFirstItemOnStart_25)); }
	inline bool get_SelectFirstItemOnStart_25() const { return ___SelectFirstItemOnStart_25; }
	inline bool* get_address_of_SelectFirstItemOnStart_25() { return &___SelectFirstItemOnStart_25; }
	inline void set_SelectFirstItemOnStart_25(bool value)
	{
		___SelectFirstItemOnStart_25 = value;
	}

	inline static int32_t get_offset_of_OnSelectionChanged_26() { return static_cast<int32_t>(offsetof(DropDownList_t4179439446, ___OnSelectionChanged_26)); }
	inline SelectionChangedEvent_t1418459309 * get_OnSelectionChanged_26() const { return ___OnSelectionChanged_26; }
	inline SelectionChangedEvent_t1418459309 ** get_address_of_OnSelectionChanged_26() { return &___OnSelectionChanged_26; }
	inline void set_OnSelectionChanged_26(SelectionChangedEvent_t1418459309 * value)
	{
		___OnSelectionChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectionChanged_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWNLIST_T4179439446_H
#ifndef EXAMPLESELECTABLE_T1184518557_H
#define EXAMPLESELECTABLE_T1184518557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ExampleSelectable
struct  ExampleSelectable_t1184518557  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UnityEngine.UI.Extensions.ExampleSelectable::_selected
	bool ____selected_4;
	// System.Boolean UnityEngine.UI.Extensions.ExampleSelectable::_preSelected
	bool ____preSelected_5;
	// UnityEngine.SpriteRenderer UnityEngine.UI.Extensions.ExampleSelectable::spriteRenderer
	SpriteRenderer_t3235626157 * ___spriteRenderer_6;
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.ExampleSelectable::image
	Image_t2670269651 * ___image_7;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.ExampleSelectable::text
	Text_t1901882714 * ___text_8;

public:
	inline static int32_t get_offset_of__selected_4() { return static_cast<int32_t>(offsetof(ExampleSelectable_t1184518557, ____selected_4)); }
	inline bool get__selected_4() const { return ____selected_4; }
	inline bool* get_address_of__selected_4() { return &____selected_4; }
	inline void set__selected_4(bool value)
	{
		____selected_4 = value;
	}

	inline static int32_t get_offset_of__preSelected_5() { return static_cast<int32_t>(offsetof(ExampleSelectable_t1184518557, ____preSelected_5)); }
	inline bool get__preSelected_5() const { return ____preSelected_5; }
	inline bool* get_address_of__preSelected_5() { return &____preSelected_5; }
	inline void set__preSelected_5(bool value)
	{
		____preSelected_5 = value;
	}

	inline static int32_t get_offset_of_spriteRenderer_6() { return static_cast<int32_t>(offsetof(ExampleSelectable_t1184518557, ___spriteRenderer_6)); }
	inline SpriteRenderer_t3235626157 * get_spriteRenderer_6() const { return ___spriteRenderer_6; }
	inline SpriteRenderer_t3235626157 ** get_address_of_spriteRenderer_6() { return &___spriteRenderer_6; }
	inline void set_spriteRenderer_6(SpriteRenderer_t3235626157 * value)
	{
		___spriteRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((&___spriteRenderer_6), value);
	}

	inline static int32_t get_offset_of_image_7() { return static_cast<int32_t>(offsetof(ExampleSelectable_t1184518557, ___image_7)); }
	inline Image_t2670269651 * get_image_7() const { return ___image_7; }
	inline Image_t2670269651 ** get_address_of_image_7() { return &___image_7; }
	inline void set_image_7(Image_t2670269651 * value)
	{
		___image_7 = value;
		Il2CppCodeGenWriteBarrier((&___image_7), value);
	}

	inline static int32_t get_offset_of_text_8() { return static_cast<int32_t>(offsetof(ExampleSelectable_t1184518557, ___text_8)); }
	inline Text_t1901882714 * get_text_8() const { return ___text_8; }
	inline Text_t1901882714 ** get_address_of_text_8() { return &___text_8; }
	inline void set_text_8(Text_t1901882714 * value)
	{
		___text_8 = value;
		Il2CppCodeGenWriteBarrier((&___text_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLESELECTABLE_T1184518557_H
#ifndef INPUTFOCUS_T2498000986_H
#define INPUTFOCUS_T2498000986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.InputFocus
struct  InputFocus_t2498000986  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.InputFocus::_inputField
	InputField_t3762917431 * ____inputField_4;
	// System.Boolean UnityEngine.UI.Extensions.InputFocus::_ignoreNextActivation
	bool ____ignoreNextActivation_5;

public:
	inline static int32_t get_offset_of__inputField_4() { return static_cast<int32_t>(offsetof(InputFocus_t2498000986, ____inputField_4)); }
	inline InputField_t3762917431 * get__inputField_4() const { return ____inputField_4; }
	inline InputField_t3762917431 ** get_address_of__inputField_4() { return &____inputField_4; }
	inline void set__inputField_4(InputField_t3762917431 * value)
	{
		____inputField_4 = value;
		Il2CppCodeGenWriteBarrier((&____inputField_4), value);
	}

	inline static int32_t get_offset_of__ignoreNextActivation_5() { return static_cast<int32_t>(offsetof(InputFocus_t2498000986, ____ignoreNextActivation_5)); }
	inline bool get__ignoreNextActivation_5() const { return ____ignoreNextActivation_5; }
	inline bool* get_address_of__ignoreNextActivation_5() { return &____ignoreNextActivation_5; }
	inline void set__ignoreNextActivation_5(bool value)
	{
		____ignoreNextActivation_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTFOCUS_T2498000986_H
#ifndef RADIALSLIDER_T2127270712_H
#define RADIALSLIDER_T2127270712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.RadialSlider
struct  RadialSlider_t2127270712  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UnityEngine.UI.Extensions.RadialSlider::isPointerDown
	bool ___isPointerDown_4;
	// System.Boolean UnityEngine.UI.Extensions.RadialSlider::isPointerReleased
	bool ___isPointerReleased_5;
	// System.Boolean UnityEngine.UI.Extensions.RadialSlider::lerpInProgress
	bool ___lerpInProgress_6;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.RadialSlider::m_localPos
	Vector2_t2156229523  ___m_localPos_7;
	// System.Single UnityEngine.UI.Extensions.RadialSlider::m_targetAngle
	float ___m_targetAngle_8;
	// System.Single UnityEngine.UI.Extensions.RadialSlider::m_lerpTargetAngle
	float ___m_lerpTargetAngle_9;
	// System.Single UnityEngine.UI.Extensions.RadialSlider::m_startAngle
	float ___m_startAngle_10;
	// System.Single UnityEngine.UI.Extensions.RadialSlider::m_currentLerpTime
	float ___m_currentLerpTime_11;
	// System.Single UnityEngine.UI.Extensions.RadialSlider::m_lerpTime
	float ___m_lerpTime_12;
	// UnityEngine.Camera UnityEngine.UI.Extensions.RadialSlider::m_eventCamera
	Camera_t4157153871 * ___m_eventCamera_13;
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.RadialSlider::m_image
	Image_t2670269651 * ___m_image_14;
	// UnityEngine.Color UnityEngine.UI.Extensions.RadialSlider::m_startColor
	Color_t2555686324  ___m_startColor_15;
	// UnityEngine.Color UnityEngine.UI.Extensions.RadialSlider::m_endColor
	Color_t2555686324  ___m_endColor_16;
	// System.Boolean UnityEngine.UI.Extensions.RadialSlider::m_lerpToTarget
	bool ___m_lerpToTarget_17;
	// UnityEngine.AnimationCurve UnityEngine.UI.Extensions.RadialSlider::m_lerpCurve
	AnimationCurve_t3046754366 * ___m_lerpCurve_18;
	// UnityEngine.UI.Extensions.RadialSlider/RadialSliderValueChangedEvent UnityEngine.UI.Extensions.RadialSlider::_onValueChanged
	RadialSliderValueChangedEvent_t1025479356 * ____onValueChanged_19;
	// UnityEngine.UI.Extensions.RadialSlider/RadialSliderTextValueChangedEvent UnityEngine.UI.Extensions.RadialSlider::_onTextValueChanged
	RadialSliderTextValueChangedEvent_t1078616506 * ____onTextValueChanged_20;

public:
	inline static int32_t get_offset_of_isPointerDown_4() { return static_cast<int32_t>(offsetof(RadialSlider_t2127270712, ___isPointerDown_4)); }
	inline bool get_isPointerDown_4() const { return ___isPointerDown_4; }
	inline bool* get_address_of_isPointerDown_4() { return &___isPointerDown_4; }
	inline void set_isPointerDown_4(bool value)
	{
		___isPointerDown_4 = value;
	}

	inline static int32_t get_offset_of_isPointerReleased_5() { return static_cast<int32_t>(offsetof(RadialSlider_t2127270712, ___isPointerReleased_5)); }
	inline bool get_isPointerReleased_5() const { return ___isPointerReleased_5; }
	inline bool* get_address_of_isPointerReleased_5() { return &___isPointerReleased_5; }
	inline void set_isPointerReleased_5(bool value)
	{
		___isPointerReleased_5 = value;
	}

	inline static int32_t get_offset_of_lerpInProgress_6() { return static_cast<int32_t>(offsetof(RadialSlider_t2127270712, ___lerpInProgress_6)); }
	inline bool get_lerpInProgress_6() const { return ___lerpInProgress_6; }
	inline bool* get_address_of_lerpInProgress_6() { return &___lerpInProgress_6; }
	inline void set_lerpInProgress_6(bool value)
	{
		___lerpInProgress_6 = value;
	}

	inline static int32_t get_offset_of_m_localPos_7() { return static_cast<int32_t>(offsetof(RadialSlider_t2127270712, ___m_localPos_7)); }
	inline Vector2_t2156229523  get_m_localPos_7() const { return ___m_localPos_7; }
	inline Vector2_t2156229523 * get_address_of_m_localPos_7() { return &___m_localPos_7; }
	inline void set_m_localPos_7(Vector2_t2156229523  value)
	{
		___m_localPos_7 = value;
	}

	inline static int32_t get_offset_of_m_targetAngle_8() { return static_cast<int32_t>(offsetof(RadialSlider_t2127270712, ___m_targetAngle_8)); }
	inline float get_m_targetAngle_8() const { return ___m_targetAngle_8; }
	inline float* get_address_of_m_targetAngle_8() { return &___m_targetAngle_8; }
	inline void set_m_targetAngle_8(float value)
	{
		___m_targetAngle_8 = value;
	}

	inline static int32_t get_offset_of_m_lerpTargetAngle_9() { return static_cast<int32_t>(offsetof(RadialSlider_t2127270712, ___m_lerpTargetAngle_9)); }
	inline float get_m_lerpTargetAngle_9() const { return ___m_lerpTargetAngle_9; }
	inline float* get_address_of_m_lerpTargetAngle_9() { return &___m_lerpTargetAngle_9; }
	inline void set_m_lerpTargetAngle_9(float value)
	{
		___m_lerpTargetAngle_9 = value;
	}

	inline static int32_t get_offset_of_m_startAngle_10() { return static_cast<int32_t>(offsetof(RadialSlider_t2127270712, ___m_startAngle_10)); }
	inline float get_m_startAngle_10() const { return ___m_startAngle_10; }
	inline float* get_address_of_m_startAngle_10() { return &___m_startAngle_10; }
	inline void set_m_startAngle_10(float value)
	{
		___m_startAngle_10 = value;
	}

	inline static int32_t get_offset_of_m_currentLerpTime_11() { return static_cast<int32_t>(offsetof(RadialSlider_t2127270712, ___m_currentLerpTime_11)); }
	inline float get_m_currentLerpTime_11() const { return ___m_currentLerpTime_11; }
	inline float* get_address_of_m_currentLerpTime_11() { return &___m_currentLerpTime_11; }
	inline void set_m_currentLerpTime_11(float value)
	{
		___m_currentLerpTime_11 = value;
	}

	inline static int32_t get_offset_of_m_lerpTime_12() { return static_cast<int32_t>(offsetof(RadialSlider_t2127270712, ___m_lerpTime_12)); }
	inline float get_m_lerpTime_12() const { return ___m_lerpTime_12; }
	inline float* get_address_of_m_lerpTime_12() { return &___m_lerpTime_12; }
	inline void set_m_lerpTime_12(float value)
	{
		___m_lerpTime_12 = value;
	}

	inline static int32_t get_offset_of_m_eventCamera_13() { return static_cast<int32_t>(offsetof(RadialSlider_t2127270712, ___m_eventCamera_13)); }
	inline Camera_t4157153871 * get_m_eventCamera_13() const { return ___m_eventCamera_13; }
	inline Camera_t4157153871 ** get_address_of_m_eventCamera_13() { return &___m_eventCamera_13; }
	inline void set_m_eventCamera_13(Camera_t4157153871 * value)
	{
		___m_eventCamera_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_eventCamera_13), value);
	}

	inline static int32_t get_offset_of_m_image_14() { return static_cast<int32_t>(offsetof(RadialSlider_t2127270712, ___m_image_14)); }
	inline Image_t2670269651 * get_m_image_14() const { return ___m_image_14; }
	inline Image_t2670269651 ** get_address_of_m_image_14() { return &___m_image_14; }
	inline void set_m_image_14(Image_t2670269651 * value)
	{
		___m_image_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_image_14), value);
	}

	inline static int32_t get_offset_of_m_startColor_15() { return static_cast<int32_t>(offsetof(RadialSlider_t2127270712, ___m_startColor_15)); }
	inline Color_t2555686324  get_m_startColor_15() const { return ___m_startColor_15; }
	inline Color_t2555686324 * get_address_of_m_startColor_15() { return &___m_startColor_15; }
	inline void set_m_startColor_15(Color_t2555686324  value)
	{
		___m_startColor_15 = value;
	}

	inline static int32_t get_offset_of_m_endColor_16() { return static_cast<int32_t>(offsetof(RadialSlider_t2127270712, ___m_endColor_16)); }
	inline Color_t2555686324  get_m_endColor_16() const { return ___m_endColor_16; }
	inline Color_t2555686324 * get_address_of_m_endColor_16() { return &___m_endColor_16; }
	inline void set_m_endColor_16(Color_t2555686324  value)
	{
		___m_endColor_16 = value;
	}

	inline static int32_t get_offset_of_m_lerpToTarget_17() { return static_cast<int32_t>(offsetof(RadialSlider_t2127270712, ___m_lerpToTarget_17)); }
	inline bool get_m_lerpToTarget_17() const { return ___m_lerpToTarget_17; }
	inline bool* get_address_of_m_lerpToTarget_17() { return &___m_lerpToTarget_17; }
	inline void set_m_lerpToTarget_17(bool value)
	{
		___m_lerpToTarget_17 = value;
	}

	inline static int32_t get_offset_of_m_lerpCurve_18() { return static_cast<int32_t>(offsetof(RadialSlider_t2127270712, ___m_lerpCurve_18)); }
	inline AnimationCurve_t3046754366 * get_m_lerpCurve_18() const { return ___m_lerpCurve_18; }
	inline AnimationCurve_t3046754366 ** get_address_of_m_lerpCurve_18() { return &___m_lerpCurve_18; }
	inline void set_m_lerpCurve_18(AnimationCurve_t3046754366 * value)
	{
		___m_lerpCurve_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_lerpCurve_18), value);
	}

	inline static int32_t get_offset_of__onValueChanged_19() { return static_cast<int32_t>(offsetof(RadialSlider_t2127270712, ____onValueChanged_19)); }
	inline RadialSliderValueChangedEvent_t1025479356 * get__onValueChanged_19() const { return ____onValueChanged_19; }
	inline RadialSliderValueChangedEvent_t1025479356 ** get_address_of__onValueChanged_19() { return &____onValueChanged_19; }
	inline void set__onValueChanged_19(RadialSliderValueChangedEvent_t1025479356 * value)
	{
		____onValueChanged_19 = value;
		Il2CppCodeGenWriteBarrier((&____onValueChanged_19), value);
	}

	inline static int32_t get_offset_of__onTextValueChanged_20() { return static_cast<int32_t>(offsetof(RadialSlider_t2127270712, ____onTextValueChanged_20)); }
	inline RadialSliderTextValueChangedEvent_t1078616506 * get__onTextValueChanged_20() const { return ____onTextValueChanged_20; }
	inline RadialSliderTextValueChangedEvent_t1078616506 ** get_address_of__onTextValueChanged_20() { return &____onTextValueChanged_20; }
	inline void set__onTextValueChanged_20(RadialSliderTextValueChangedEvent_t1078616506 * value)
	{
		____onTextValueChanged_20 = value;
		Il2CppCodeGenWriteBarrier((&____onTextValueChanged_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RADIALSLIDER_T2127270712_H
#ifndef RAYCASTMASK_T2166940029_H
#define RAYCASTMASK_T2166940029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.RaycastMask
struct  RaycastMask_t2166940029  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.RaycastMask::_image
	Image_t2670269651 * ____image_4;
	// UnityEngine.Sprite UnityEngine.UI.Extensions.RaycastMask::_sprite
	Sprite_t280657092 * ____sprite_5;

public:
	inline static int32_t get_offset_of__image_4() { return static_cast<int32_t>(offsetof(RaycastMask_t2166940029, ____image_4)); }
	inline Image_t2670269651 * get__image_4() const { return ____image_4; }
	inline Image_t2670269651 ** get_address_of__image_4() { return &____image_4; }
	inline void set__image_4(Image_t2670269651 * value)
	{
		____image_4 = value;
		Il2CppCodeGenWriteBarrier((&____image_4), value);
	}

	inline static int32_t get_offset_of__sprite_5() { return static_cast<int32_t>(offsetof(RaycastMask_t2166940029, ____sprite_5)); }
	inline Sprite_t280657092 * get__sprite_5() const { return ____sprite_5; }
	inline Sprite_t280657092 ** get_address_of__sprite_5() { return &____sprite_5; }
	inline void set__sprite_5(Sprite_t280657092 * value)
	{
		____sprite_5 = value;
		Il2CppCodeGenWriteBarrier((&____sprite_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTMASK_T2166940029_H
#ifndef REORDERABLELIST_T1822109201_H
#define REORDERABLELIST_T1822109201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ReorderableList
struct  ReorderableList_t1822109201  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.LayoutGroup UnityEngine.UI.Extensions.ReorderableList::ContentLayout
	LayoutGroup_t2436138090 * ___ContentLayout_4;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ReorderableList::DraggableArea
	RectTransform_t3704657025 * ___DraggableArea_5;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableList::IsDraggable
	bool ___IsDraggable_6;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableList::CloneDraggedObject
	bool ___CloneDraggedObject_7;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableList::IsDropable
	bool ___IsDropable_8;
	// UnityEngine.UI.Extensions.ReorderableList/ReorderableListHandler UnityEngine.UI.Extensions.ReorderableList::OnElementDropped
	ReorderableListHandler_t1290756480 * ___OnElementDropped_9;
	// UnityEngine.UI.Extensions.ReorderableList/ReorderableListHandler UnityEngine.UI.Extensions.ReorderableList::OnElementGrabbed
	ReorderableListHandler_t1290756480 * ___OnElementGrabbed_10;
	// UnityEngine.UI.Extensions.ReorderableList/ReorderableListHandler UnityEngine.UI.Extensions.ReorderableList::OnElementRemoved
	ReorderableListHandler_t1290756480 * ___OnElementRemoved_11;
	// UnityEngine.UI.Extensions.ReorderableList/ReorderableListHandler UnityEngine.UI.Extensions.ReorderableList::OnElementAdded
	ReorderableListHandler_t1290756480 * ___OnElementAdded_12;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ReorderableList::_content
	RectTransform_t3704657025 * ____content_13;
	// UnityEngine.UI.Extensions.ReorderableListContent UnityEngine.UI.Extensions.ReorderableList::_listContent
	ReorderableListContent_t2633001117 * ____listContent_14;

public:
	inline static int32_t get_offset_of_ContentLayout_4() { return static_cast<int32_t>(offsetof(ReorderableList_t1822109201, ___ContentLayout_4)); }
	inline LayoutGroup_t2436138090 * get_ContentLayout_4() const { return ___ContentLayout_4; }
	inline LayoutGroup_t2436138090 ** get_address_of_ContentLayout_4() { return &___ContentLayout_4; }
	inline void set_ContentLayout_4(LayoutGroup_t2436138090 * value)
	{
		___ContentLayout_4 = value;
		Il2CppCodeGenWriteBarrier((&___ContentLayout_4), value);
	}

	inline static int32_t get_offset_of_DraggableArea_5() { return static_cast<int32_t>(offsetof(ReorderableList_t1822109201, ___DraggableArea_5)); }
	inline RectTransform_t3704657025 * get_DraggableArea_5() const { return ___DraggableArea_5; }
	inline RectTransform_t3704657025 ** get_address_of_DraggableArea_5() { return &___DraggableArea_5; }
	inline void set_DraggableArea_5(RectTransform_t3704657025 * value)
	{
		___DraggableArea_5 = value;
		Il2CppCodeGenWriteBarrier((&___DraggableArea_5), value);
	}

	inline static int32_t get_offset_of_IsDraggable_6() { return static_cast<int32_t>(offsetof(ReorderableList_t1822109201, ___IsDraggable_6)); }
	inline bool get_IsDraggable_6() const { return ___IsDraggable_6; }
	inline bool* get_address_of_IsDraggable_6() { return &___IsDraggable_6; }
	inline void set_IsDraggable_6(bool value)
	{
		___IsDraggable_6 = value;
	}

	inline static int32_t get_offset_of_CloneDraggedObject_7() { return static_cast<int32_t>(offsetof(ReorderableList_t1822109201, ___CloneDraggedObject_7)); }
	inline bool get_CloneDraggedObject_7() const { return ___CloneDraggedObject_7; }
	inline bool* get_address_of_CloneDraggedObject_7() { return &___CloneDraggedObject_7; }
	inline void set_CloneDraggedObject_7(bool value)
	{
		___CloneDraggedObject_7 = value;
	}

	inline static int32_t get_offset_of_IsDropable_8() { return static_cast<int32_t>(offsetof(ReorderableList_t1822109201, ___IsDropable_8)); }
	inline bool get_IsDropable_8() const { return ___IsDropable_8; }
	inline bool* get_address_of_IsDropable_8() { return &___IsDropable_8; }
	inline void set_IsDropable_8(bool value)
	{
		___IsDropable_8 = value;
	}

	inline static int32_t get_offset_of_OnElementDropped_9() { return static_cast<int32_t>(offsetof(ReorderableList_t1822109201, ___OnElementDropped_9)); }
	inline ReorderableListHandler_t1290756480 * get_OnElementDropped_9() const { return ___OnElementDropped_9; }
	inline ReorderableListHandler_t1290756480 ** get_address_of_OnElementDropped_9() { return &___OnElementDropped_9; }
	inline void set_OnElementDropped_9(ReorderableListHandler_t1290756480 * value)
	{
		___OnElementDropped_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnElementDropped_9), value);
	}

	inline static int32_t get_offset_of_OnElementGrabbed_10() { return static_cast<int32_t>(offsetof(ReorderableList_t1822109201, ___OnElementGrabbed_10)); }
	inline ReorderableListHandler_t1290756480 * get_OnElementGrabbed_10() const { return ___OnElementGrabbed_10; }
	inline ReorderableListHandler_t1290756480 ** get_address_of_OnElementGrabbed_10() { return &___OnElementGrabbed_10; }
	inline void set_OnElementGrabbed_10(ReorderableListHandler_t1290756480 * value)
	{
		___OnElementGrabbed_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnElementGrabbed_10), value);
	}

	inline static int32_t get_offset_of_OnElementRemoved_11() { return static_cast<int32_t>(offsetof(ReorderableList_t1822109201, ___OnElementRemoved_11)); }
	inline ReorderableListHandler_t1290756480 * get_OnElementRemoved_11() const { return ___OnElementRemoved_11; }
	inline ReorderableListHandler_t1290756480 ** get_address_of_OnElementRemoved_11() { return &___OnElementRemoved_11; }
	inline void set_OnElementRemoved_11(ReorderableListHandler_t1290756480 * value)
	{
		___OnElementRemoved_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnElementRemoved_11), value);
	}

	inline static int32_t get_offset_of_OnElementAdded_12() { return static_cast<int32_t>(offsetof(ReorderableList_t1822109201, ___OnElementAdded_12)); }
	inline ReorderableListHandler_t1290756480 * get_OnElementAdded_12() const { return ___OnElementAdded_12; }
	inline ReorderableListHandler_t1290756480 ** get_address_of_OnElementAdded_12() { return &___OnElementAdded_12; }
	inline void set_OnElementAdded_12(ReorderableListHandler_t1290756480 * value)
	{
		___OnElementAdded_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnElementAdded_12), value);
	}

	inline static int32_t get_offset_of__content_13() { return static_cast<int32_t>(offsetof(ReorderableList_t1822109201, ____content_13)); }
	inline RectTransform_t3704657025 * get__content_13() const { return ____content_13; }
	inline RectTransform_t3704657025 ** get_address_of__content_13() { return &____content_13; }
	inline void set__content_13(RectTransform_t3704657025 * value)
	{
		____content_13 = value;
		Il2CppCodeGenWriteBarrier((&____content_13), value);
	}

	inline static int32_t get_offset_of__listContent_14() { return static_cast<int32_t>(offsetof(ReorderableList_t1822109201, ____listContent_14)); }
	inline ReorderableListContent_t2633001117 * get__listContent_14() const { return ____listContent_14; }
	inline ReorderableListContent_t2633001117 ** get_address_of__listContent_14() { return &____listContent_14; }
	inline void set__listContent_14(ReorderableListContent_t2633001117 * value)
	{
		____listContent_14 = value;
		Il2CppCodeGenWriteBarrier((&____listContent_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REORDERABLELIST_T1822109201_H
#ifndef REORDERABLELISTCONTENT_T2633001117_H
#define REORDERABLELISTCONTENT_T2633001117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ReorderableListContent
struct  ReorderableListContent_t2633001117  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityEngine.UI.Extensions.ReorderableListContent::_cachedChildren
	List_1_t777473367 * ____cachedChildren_4;
	// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.ReorderableListElement> UnityEngine.UI.Extensions.ReorderableListContent::_cachedListElement
	List_1_t3356400495 * ____cachedListElement_5;
	// UnityEngine.UI.Extensions.ReorderableListElement UnityEngine.UI.Extensions.ReorderableListContent::_ele
	ReorderableListElement_t1884325753 * ____ele_6;
	// UnityEngine.UI.Extensions.ReorderableList UnityEngine.UI.Extensions.ReorderableListContent::_extList
	ReorderableList_t1822109201 * ____extList_7;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ReorderableListContent::_rect
	RectTransform_t3704657025 * ____rect_8;

public:
	inline static int32_t get_offset_of__cachedChildren_4() { return static_cast<int32_t>(offsetof(ReorderableListContent_t2633001117, ____cachedChildren_4)); }
	inline List_1_t777473367 * get__cachedChildren_4() const { return ____cachedChildren_4; }
	inline List_1_t777473367 ** get_address_of__cachedChildren_4() { return &____cachedChildren_4; }
	inline void set__cachedChildren_4(List_1_t777473367 * value)
	{
		____cachedChildren_4 = value;
		Il2CppCodeGenWriteBarrier((&____cachedChildren_4), value);
	}

	inline static int32_t get_offset_of__cachedListElement_5() { return static_cast<int32_t>(offsetof(ReorderableListContent_t2633001117, ____cachedListElement_5)); }
	inline List_1_t3356400495 * get__cachedListElement_5() const { return ____cachedListElement_5; }
	inline List_1_t3356400495 ** get_address_of__cachedListElement_5() { return &____cachedListElement_5; }
	inline void set__cachedListElement_5(List_1_t3356400495 * value)
	{
		____cachedListElement_5 = value;
		Il2CppCodeGenWriteBarrier((&____cachedListElement_5), value);
	}

	inline static int32_t get_offset_of__ele_6() { return static_cast<int32_t>(offsetof(ReorderableListContent_t2633001117, ____ele_6)); }
	inline ReorderableListElement_t1884325753 * get__ele_6() const { return ____ele_6; }
	inline ReorderableListElement_t1884325753 ** get_address_of__ele_6() { return &____ele_6; }
	inline void set__ele_6(ReorderableListElement_t1884325753 * value)
	{
		____ele_6 = value;
		Il2CppCodeGenWriteBarrier((&____ele_6), value);
	}

	inline static int32_t get_offset_of__extList_7() { return static_cast<int32_t>(offsetof(ReorderableListContent_t2633001117, ____extList_7)); }
	inline ReorderableList_t1822109201 * get__extList_7() const { return ____extList_7; }
	inline ReorderableList_t1822109201 ** get_address_of__extList_7() { return &____extList_7; }
	inline void set__extList_7(ReorderableList_t1822109201 * value)
	{
		____extList_7 = value;
		Il2CppCodeGenWriteBarrier((&____extList_7), value);
	}

	inline static int32_t get_offset_of__rect_8() { return static_cast<int32_t>(offsetof(ReorderableListContent_t2633001117, ____rect_8)); }
	inline RectTransform_t3704657025 * get__rect_8() const { return ____rect_8; }
	inline RectTransform_t3704657025 ** get_address_of__rect_8() { return &____rect_8; }
	inline void set__rect_8(RectTransform_t3704657025 * value)
	{
		____rect_8 = value;
		Il2CppCodeGenWriteBarrier((&____rect_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REORDERABLELISTCONTENT_T2633001117_H
#ifndef REORDERABLELISTDEBUG_T2870269905_H
#define REORDERABLELISTDEBUG_T2870269905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ReorderableListDebug
struct  ReorderableListDebug_t2870269905  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.ReorderableListDebug::DebugLabel
	Text_t1901882714 * ___DebugLabel_4;

public:
	inline static int32_t get_offset_of_DebugLabel_4() { return static_cast<int32_t>(offsetof(ReorderableListDebug_t2870269905, ___DebugLabel_4)); }
	inline Text_t1901882714 * get_DebugLabel_4() const { return ___DebugLabel_4; }
	inline Text_t1901882714 ** get_address_of_DebugLabel_4() { return &___DebugLabel_4; }
	inline void set_DebugLabel_4(Text_t1901882714 * value)
	{
		___DebugLabel_4 = value;
		Il2CppCodeGenWriteBarrier((&___DebugLabel_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REORDERABLELISTDEBUG_T2870269905_H
#ifndef REORDERABLELISTELEMENT_T1884325753_H
#define REORDERABLELISTELEMENT_T1884325753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ReorderableListElement
struct  ReorderableListElement_t1884325753  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UnityEngine.UI.Extensions.ReorderableListElement::IsGrabbable
	bool ___IsGrabbable_4;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableListElement::IsTransferable
	bool ___IsTransferable_5;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableListElement::isDroppableInSpace
	bool ___isDroppableInSpace_6;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.UI.Extensions.ReorderableListElement::_raycastResults
	List_1_t537414295 * ____raycastResults_7;
	// UnityEngine.UI.Extensions.ReorderableList UnityEngine.UI.Extensions.ReorderableListElement::_currentReorderableListRaycasted
	ReorderableList_t1822109201 * ____currentReorderableListRaycasted_8;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ReorderableListElement::_draggingObject
	RectTransform_t3704657025 * ____draggingObject_9;
	// UnityEngine.UI.LayoutElement UnityEngine.UI.Extensions.ReorderableListElement::_draggingObjectLE
	LayoutElement_t1785403678 * ____draggingObjectLE_10;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.ReorderableListElement::_draggingObjectOriginalSize
	Vector2_t2156229523  ____draggingObjectOriginalSize_11;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ReorderableListElement::_fakeElement
	RectTransform_t3704657025 * ____fakeElement_12;
	// UnityEngine.UI.LayoutElement UnityEngine.UI.Extensions.ReorderableListElement::_fakeElementLE
	LayoutElement_t1785403678 * ____fakeElementLE_13;
	// System.Int32 UnityEngine.UI.Extensions.ReorderableListElement::_fromIndex
	int32_t ____fromIndex_14;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableListElement::_isDragging
	bool ____isDragging_15;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ReorderableListElement::_rect
	RectTransform_t3704657025 * ____rect_16;
	// UnityEngine.UI.Extensions.ReorderableList UnityEngine.UI.Extensions.ReorderableListElement::_reorderableList
	ReorderableList_t1822109201 * ____reorderableList_17;
	// System.Boolean UnityEngine.UI.Extensions.ReorderableListElement::isValid
	bool ___isValid_18;

public:
	inline static int32_t get_offset_of_IsGrabbable_4() { return static_cast<int32_t>(offsetof(ReorderableListElement_t1884325753, ___IsGrabbable_4)); }
	inline bool get_IsGrabbable_4() const { return ___IsGrabbable_4; }
	inline bool* get_address_of_IsGrabbable_4() { return &___IsGrabbable_4; }
	inline void set_IsGrabbable_4(bool value)
	{
		___IsGrabbable_4 = value;
	}

	inline static int32_t get_offset_of_IsTransferable_5() { return static_cast<int32_t>(offsetof(ReorderableListElement_t1884325753, ___IsTransferable_5)); }
	inline bool get_IsTransferable_5() const { return ___IsTransferable_5; }
	inline bool* get_address_of_IsTransferable_5() { return &___IsTransferable_5; }
	inline void set_IsTransferable_5(bool value)
	{
		___IsTransferable_5 = value;
	}

	inline static int32_t get_offset_of_isDroppableInSpace_6() { return static_cast<int32_t>(offsetof(ReorderableListElement_t1884325753, ___isDroppableInSpace_6)); }
	inline bool get_isDroppableInSpace_6() const { return ___isDroppableInSpace_6; }
	inline bool* get_address_of_isDroppableInSpace_6() { return &___isDroppableInSpace_6; }
	inline void set_isDroppableInSpace_6(bool value)
	{
		___isDroppableInSpace_6 = value;
	}

	inline static int32_t get_offset_of__raycastResults_7() { return static_cast<int32_t>(offsetof(ReorderableListElement_t1884325753, ____raycastResults_7)); }
	inline List_1_t537414295 * get__raycastResults_7() const { return ____raycastResults_7; }
	inline List_1_t537414295 ** get_address_of__raycastResults_7() { return &____raycastResults_7; }
	inline void set__raycastResults_7(List_1_t537414295 * value)
	{
		____raycastResults_7 = value;
		Il2CppCodeGenWriteBarrier((&____raycastResults_7), value);
	}

	inline static int32_t get_offset_of__currentReorderableListRaycasted_8() { return static_cast<int32_t>(offsetof(ReorderableListElement_t1884325753, ____currentReorderableListRaycasted_8)); }
	inline ReorderableList_t1822109201 * get__currentReorderableListRaycasted_8() const { return ____currentReorderableListRaycasted_8; }
	inline ReorderableList_t1822109201 ** get_address_of__currentReorderableListRaycasted_8() { return &____currentReorderableListRaycasted_8; }
	inline void set__currentReorderableListRaycasted_8(ReorderableList_t1822109201 * value)
	{
		____currentReorderableListRaycasted_8 = value;
		Il2CppCodeGenWriteBarrier((&____currentReorderableListRaycasted_8), value);
	}

	inline static int32_t get_offset_of__draggingObject_9() { return static_cast<int32_t>(offsetof(ReorderableListElement_t1884325753, ____draggingObject_9)); }
	inline RectTransform_t3704657025 * get__draggingObject_9() const { return ____draggingObject_9; }
	inline RectTransform_t3704657025 ** get_address_of__draggingObject_9() { return &____draggingObject_9; }
	inline void set__draggingObject_9(RectTransform_t3704657025 * value)
	{
		____draggingObject_9 = value;
		Il2CppCodeGenWriteBarrier((&____draggingObject_9), value);
	}

	inline static int32_t get_offset_of__draggingObjectLE_10() { return static_cast<int32_t>(offsetof(ReorderableListElement_t1884325753, ____draggingObjectLE_10)); }
	inline LayoutElement_t1785403678 * get__draggingObjectLE_10() const { return ____draggingObjectLE_10; }
	inline LayoutElement_t1785403678 ** get_address_of__draggingObjectLE_10() { return &____draggingObjectLE_10; }
	inline void set__draggingObjectLE_10(LayoutElement_t1785403678 * value)
	{
		____draggingObjectLE_10 = value;
		Il2CppCodeGenWriteBarrier((&____draggingObjectLE_10), value);
	}

	inline static int32_t get_offset_of__draggingObjectOriginalSize_11() { return static_cast<int32_t>(offsetof(ReorderableListElement_t1884325753, ____draggingObjectOriginalSize_11)); }
	inline Vector2_t2156229523  get__draggingObjectOriginalSize_11() const { return ____draggingObjectOriginalSize_11; }
	inline Vector2_t2156229523 * get_address_of__draggingObjectOriginalSize_11() { return &____draggingObjectOriginalSize_11; }
	inline void set__draggingObjectOriginalSize_11(Vector2_t2156229523  value)
	{
		____draggingObjectOriginalSize_11 = value;
	}

	inline static int32_t get_offset_of__fakeElement_12() { return static_cast<int32_t>(offsetof(ReorderableListElement_t1884325753, ____fakeElement_12)); }
	inline RectTransform_t3704657025 * get__fakeElement_12() const { return ____fakeElement_12; }
	inline RectTransform_t3704657025 ** get_address_of__fakeElement_12() { return &____fakeElement_12; }
	inline void set__fakeElement_12(RectTransform_t3704657025 * value)
	{
		____fakeElement_12 = value;
		Il2CppCodeGenWriteBarrier((&____fakeElement_12), value);
	}

	inline static int32_t get_offset_of__fakeElementLE_13() { return static_cast<int32_t>(offsetof(ReorderableListElement_t1884325753, ____fakeElementLE_13)); }
	inline LayoutElement_t1785403678 * get__fakeElementLE_13() const { return ____fakeElementLE_13; }
	inline LayoutElement_t1785403678 ** get_address_of__fakeElementLE_13() { return &____fakeElementLE_13; }
	inline void set__fakeElementLE_13(LayoutElement_t1785403678 * value)
	{
		____fakeElementLE_13 = value;
		Il2CppCodeGenWriteBarrier((&____fakeElementLE_13), value);
	}

	inline static int32_t get_offset_of__fromIndex_14() { return static_cast<int32_t>(offsetof(ReorderableListElement_t1884325753, ____fromIndex_14)); }
	inline int32_t get__fromIndex_14() const { return ____fromIndex_14; }
	inline int32_t* get_address_of__fromIndex_14() { return &____fromIndex_14; }
	inline void set__fromIndex_14(int32_t value)
	{
		____fromIndex_14 = value;
	}

	inline static int32_t get_offset_of__isDragging_15() { return static_cast<int32_t>(offsetof(ReorderableListElement_t1884325753, ____isDragging_15)); }
	inline bool get__isDragging_15() const { return ____isDragging_15; }
	inline bool* get_address_of__isDragging_15() { return &____isDragging_15; }
	inline void set__isDragging_15(bool value)
	{
		____isDragging_15 = value;
	}

	inline static int32_t get_offset_of__rect_16() { return static_cast<int32_t>(offsetof(ReorderableListElement_t1884325753, ____rect_16)); }
	inline RectTransform_t3704657025 * get__rect_16() const { return ____rect_16; }
	inline RectTransform_t3704657025 ** get_address_of__rect_16() { return &____rect_16; }
	inline void set__rect_16(RectTransform_t3704657025 * value)
	{
		____rect_16 = value;
		Il2CppCodeGenWriteBarrier((&____rect_16), value);
	}

	inline static int32_t get_offset_of__reorderableList_17() { return static_cast<int32_t>(offsetof(ReorderableListElement_t1884325753, ____reorderableList_17)); }
	inline ReorderableList_t1822109201 * get__reorderableList_17() const { return ____reorderableList_17; }
	inline ReorderableList_t1822109201 ** get_address_of__reorderableList_17() { return &____reorderableList_17; }
	inline void set__reorderableList_17(ReorderableList_t1822109201 * value)
	{
		____reorderableList_17 = value;
		Il2CppCodeGenWriteBarrier((&____reorderableList_17), value);
	}

	inline static int32_t get_offset_of_isValid_18() { return static_cast<int32_t>(offsetof(ReorderableListElement_t1884325753, ___isValid_18)); }
	inline bool get_isValid_18() const { return ___isValid_18; }
	inline bool* get_address_of_isValid_18() { return &___isValid_18; }
	inline void set_isValid_18(bool value)
	{
		___isValid_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REORDERABLELISTELEMENT_T1884325753_H
#ifndef RESCALEDRAGPANEL_T733373206_H
#define RESCALEDRAGPANEL_T733373206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.RescaleDragPanel
struct  RescaleDragPanel_t733373206  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.RescaleDragPanel::pointerOffset
	Vector2_t2156229523  ___pointerOffset_4;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.RescaleDragPanel::canvasRectTransform
	RectTransform_t3704657025 * ___canvasRectTransform_5;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.RescaleDragPanel::panelRectTransform
	RectTransform_t3704657025 * ___panelRectTransform_6;
	// UnityEngine.Transform UnityEngine.UI.Extensions.RescaleDragPanel::goTransform
	Transform_t3600365921 * ___goTransform_7;

public:
	inline static int32_t get_offset_of_pointerOffset_4() { return static_cast<int32_t>(offsetof(RescaleDragPanel_t733373206, ___pointerOffset_4)); }
	inline Vector2_t2156229523  get_pointerOffset_4() const { return ___pointerOffset_4; }
	inline Vector2_t2156229523 * get_address_of_pointerOffset_4() { return &___pointerOffset_4; }
	inline void set_pointerOffset_4(Vector2_t2156229523  value)
	{
		___pointerOffset_4 = value;
	}

	inline static int32_t get_offset_of_canvasRectTransform_5() { return static_cast<int32_t>(offsetof(RescaleDragPanel_t733373206, ___canvasRectTransform_5)); }
	inline RectTransform_t3704657025 * get_canvasRectTransform_5() const { return ___canvasRectTransform_5; }
	inline RectTransform_t3704657025 ** get_address_of_canvasRectTransform_5() { return &___canvasRectTransform_5; }
	inline void set_canvasRectTransform_5(RectTransform_t3704657025 * value)
	{
		___canvasRectTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___canvasRectTransform_5), value);
	}

	inline static int32_t get_offset_of_panelRectTransform_6() { return static_cast<int32_t>(offsetof(RescaleDragPanel_t733373206, ___panelRectTransform_6)); }
	inline RectTransform_t3704657025 * get_panelRectTransform_6() const { return ___panelRectTransform_6; }
	inline RectTransform_t3704657025 ** get_address_of_panelRectTransform_6() { return &___panelRectTransform_6; }
	inline void set_panelRectTransform_6(RectTransform_t3704657025 * value)
	{
		___panelRectTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___panelRectTransform_6), value);
	}

	inline static int32_t get_offset_of_goTransform_7() { return static_cast<int32_t>(offsetof(RescaleDragPanel_t733373206, ___goTransform_7)); }
	inline Transform_t3600365921 * get_goTransform_7() const { return ___goTransform_7; }
	inline Transform_t3600365921 ** get_address_of_goTransform_7() { return &___goTransform_7; }
	inline void set_goTransform_7(Transform_t3600365921 * value)
	{
		___goTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___goTransform_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESCALEDRAGPANEL_T733373206_H
#ifndef RESCALEPANEL_T1067833034_H
#define RESCALEPANEL_T1067833034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.RescalePanel
struct  RescalePanel_t1067833034  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.RescalePanel::minSize
	Vector2_t2156229523  ___minSize_4;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.RescalePanel::maxSize
	Vector2_t2156229523  ___maxSize_5;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.RescalePanel::rectTransform
	RectTransform_t3704657025 * ___rectTransform_6;
	// UnityEngine.Transform UnityEngine.UI.Extensions.RescalePanel::goTransform
	Transform_t3600365921 * ___goTransform_7;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.RescalePanel::currentPointerPosition
	Vector2_t2156229523  ___currentPointerPosition_8;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.RescalePanel::previousPointerPosition
	Vector2_t2156229523  ___previousPointerPosition_9;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.RescalePanel::thisRectTransform
	RectTransform_t3704657025 * ___thisRectTransform_10;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.RescalePanel::sizeDelta
	Vector2_t2156229523  ___sizeDelta_11;

public:
	inline static int32_t get_offset_of_minSize_4() { return static_cast<int32_t>(offsetof(RescalePanel_t1067833034, ___minSize_4)); }
	inline Vector2_t2156229523  get_minSize_4() const { return ___minSize_4; }
	inline Vector2_t2156229523 * get_address_of_minSize_4() { return &___minSize_4; }
	inline void set_minSize_4(Vector2_t2156229523  value)
	{
		___minSize_4 = value;
	}

	inline static int32_t get_offset_of_maxSize_5() { return static_cast<int32_t>(offsetof(RescalePanel_t1067833034, ___maxSize_5)); }
	inline Vector2_t2156229523  get_maxSize_5() const { return ___maxSize_5; }
	inline Vector2_t2156229523 * get_address_of_maxSize_5() { return &___maxSize_5; }
	inline void set_maxSize_5(Vector2_t2156229523  value)
	{
		___maxSize_5 = value;
	}

	inline static int32_t get_offset_of_rectTransform_6() { return static_cast<int32_t>(offsetof(RescalePanel_t1067833034, ___rectTransform_6)); }
	inline RectTransform_t3704657025 * get_rectTransform_6() const { return ___rectTransform_6; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_6() { return &___rectTransform_6; }
	inline void set_rectTransform_6(RectTransform_t3704657025 * value)
	{
		___rectTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_6), value);
	}

	inline static int32_t get_offset_of_goTransform_7() { return static_cast<int32_t>(offsetof(RescalePanel_t1067833034, ___goTransform_7)); }
	inline Transform_t3600365921 * get_goTransform_7() const { return ___goTransform_7; }
	inline Transform_t3600365921 ** get_address_of_goTransform_7() { return &___goTransform_7; }
	inline void set_goTransform_7(Transform_t3600365921 * value)
	{
		___goTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___goTransform_7), value);
	}

	inline static int32_t get_offset_of_currentPointerPosition_8() { return static_cast<int32_t>(offsetof(RescalePanel_t1067833034, ___currentPointerPosition_8)); }
	inline Vector2_t2156229523  get_currentPointerPosition_8() const { return ___currentPointerPosition_8; }
	inline Vector2_t2156229523 * get_address_of_currentPointerPosition_8() { return &___currentPointerPosition_8; }
	inline void set_currentPointerPosition_8(Vector2_t2156229523  value)
	{
		___currentPointerPosition_8 = value;
	}

	inline static int32_t get_offset_of_previousPointerPosition_9() { return static_cast<int32_t>(offsetof(RescalePanel_t1067833034, ___previousPointerPosition_9)); }
	inline Vector2_t2156229523  get_previousPointerPosition_9() const { return ___previousPointerPosition_9; }
	inline Vector2_t2156229523 * get_address_of_previousPointerPosition_9() { return &___previousPointerPosition_9; }
	inline void set_previousPointerPosition_9(Vector2_t2156229523  value)
	{
		___previousPointerPosition_9 = value;
	}

	inline static int32_t get_offset_of_thisRectTransform_10() { return static_cast<int32_t>(offsetof(RescalePanel_t1067833034, ___thisRectTransform_10)); }
	inline RectTransform_t3704657025 * get_thisRectTransform_10() const { return ___thisRectTransform_10; }
	inline RectTransform_t3704657025 ** get_address_of_thisRectTransform_10() { return &___thisRectTransform_10; }
	inline void set_thisRectTransform_10(RectTransform_t3704657025 * value)
	{
		___thisRectTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___thisRectTransform_10), value);
	}

	inline static int32_t get_offset_of_sizeDelta_11() { return static_cast<int32_t>(offsetof(RescalePanel_t1067833034, ___sizeDelta_11)); }
	inline Vector2_t2156229523  get_sizeDelta_11() const { return ___sizeDelta_11; }
	inline Vector2_t2156229523 * get_address_of_sizeDelta_11() { return &___sizeDelta_11; }
	inline void set_sizeDelta_11(Vector2_t2156229523  value)
	{
		___sizeDelta_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESCALEPANEL_T1067833034_H
#ifndef RESIZEPANEL_T686254421_H
#define RESIZEPANEL_T686254421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ResizePanel
struct  ResizePanel_t686254421  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.ResizePanel::minSize
	Vector2_t2156229523  ___minSize_4;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.ResizePanel::maxSize
	Vector2_t2156229523  ___maxSize_5;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ResizePanel::rectTransform
	RectTransform_t3704657025 * ___rectTransform_6;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.ResizePanel::currentPointerPosition
	Vector2_t2156229523  ___currentPointerPosition_7;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.ResizePanel::previousPointerPosition
	Vector2_t2156229523  ___previousPointerPosition_8;
	// System.Single UnityEngine.UI.Extensions.ResizePanel::ratio
	float ___ratio_9;

public:
	inline static int32_t get_offset_of_minSize_4() { return static_cast<int32_t>(offsetof(ResizePanel_t686254421, ___minSize_4)); }
	inline Vector2_t2156229523  get_minSize_4() const { return ___minSize_4; }
	inline Vector2_t2156229523 * get_address_of_minSize_4() { return &___minSize_4; }
	inline void set_minSize_4(Vector2_t2156229523  value)
	{
		___minSize_4 = value;
	}

	inline static int32_t get_offset_of_maxSize_5() { return static_cast<int32_t>(offsetof(ResizePanel_t686254421, ___maxSize_5)); }
	inline Vector2_t2156229523  get_maxSize_5() const { return ___maxSize_5; }
	inline Vector2_t2156229523 * get_address_of_maxSize_5() { return &___maxSize_5; }
	inline void set_maxSize_5(Vector2_t2156229523  value)
	{
		___maxSize_5 = value;
	}

	inline static int32_t get_offset_of_rectTransform_6() { return static_cast<int32_t>(offsetof(ResizePanel_t686254421, ___rectTransform_6)); }
	inline RectTransform_t3704657025 * get_rectTransform_6() const { return ___rectTransform_6; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_6() { return &___rectTransform_6; }
	inline void set_rectTransform_6(RectTransform_t3704657025 * value)
	{
		___rectTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_6), value);
	}

	inline static int32_t get_offset_of_currentPointerPosition_7() { return static_cast<int32_t>(offsetof(ResizePanel_t686254421, ___currentPointerPosition_7)); }
	inline Vector2_t2156229523  get_currentPointerPosition_7() const { return ___currentPointerPosition_7; }
	inline Vector2_t2156229523 * get_address_of_currentPointerPosition_7() { return &___currentPointerPosition_7; }
	inline void set_currentPointerPosition_7(Vector2_t2156229523  value)
	{
		___currentPointerPosition_7 = value;
	}

	inline static int32_t get_offset_of_previousPointerPosition_8() { return static_cast<int32_t>(offsetof(ResizePanel_t686254421, ___previousPointerPosition_8)); }
	inline Vector2_t2156229523  get_previousPointerPosition_8() const { return ___previousPointerPosition_8; }
	inline Vector2_t2156229523 * get_address_of_previousPointerPosition_8() { return &___previousPointerPosition_8; }
	inline void set_previousPointerPosition_8(Vector2_t2156229523  value)
	{
		___previousPointerPosition_8 = value;
	}

	inline static int32_t get_offset_of_ratio_9() { return static_cast<int32_t>(offsetof(ResizePanel_t686254421, ___ratio_9)); }
	inline float get_ratio_9() const { return ___ratio_9; }
	inline float* get_address_of_ratio_9() { return &___ratio_9; }
	inline void set_ratio_9(float value)
	{
		___ratio_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESIZEPANEL_T686254421_H
#ifndef SELECTIONBOX_T2044353942_H
#define SELECTIONBOX_T2044353942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SelectionBox
struct  SelectionBox_t2044353942  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color UnityEngine.UI.Extensions.SelectionBox::color
	Color_t2555686324  ___color_4;
	// UnityEngine.Sprite UnityEngine.UI.Extensions.SelectionBox::art
	Sprite_t280657092 * ___art_5;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.SelectionBox::origin
	Vector2_t2156229523  ___origin_6;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.SelectionBox::selectionMask
	RectTransform_t3704657025 * ___selectionMask_7;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.SelectionBox::boxRect
	RectTransform_t3704657025 * ___boxRect_8;
	// UnityEngine.UI.Extensions.IBoxSelectable[] UnityEngine.UI.Extensions.SelectionBox::selectables
	IBoxSelectableU5BU5D_t183195169* ___selectables_9;
	// UnityEngine.MonoBehaviour[] UnityEngine.UI.Extensions.SelectionBox::selectableGroup
	MonoBehaviourU5BU5D_t2007329276* ___selectableGroup_10;
	// UnityEngine.UI.Extensions.IBoxSelectable UnityEngine.UI.Extensions.SelectionBox::clickedBeforeDrag
	RuntimeObject* ___clickedBeforeDrag_11;
	// UnityEngine.UI.Extensions.IBoxSelectable UnityEngine.UI.Extensions.SelectionBox::clickedAfterDrag
	RuntimeObject* ___clickedAfterDrag_12;
	// UnityEngine.UI.Extensions.SelectionBox/SelectionEvent UnityEngine.UI.Extensions.SelectionBox::onSelectionChange
	SelectionEvent_t3355704588 * ___onSelectionChange_13;

public:
	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(SelectionBox_t2044353942, ___color_4)); }
	inline Color_t2555686324  get_color_4() const { return ___color_4; }
	inline Color_t2555686324 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color_t2555686324  value)
	{
		___color_4 = value;
	}

	inline static int32_t get_offset_of_art_5() { return static_cast<int32_t>(offsetof(SelectionBox_t2044353942, ___art_5)); }
	inline Sprite_t280657092 * get_art_5() const { return ___art_5; }
	inline Sprite_t280657092 ** get_address_of_art_5() { return &___art_5; }
	inline void set_art_5(Sprite_t280657092 * value)
	{
		___art_5 = value;
		Il2CppCodeGenWriteBarrier((&___art_5), value);
	}

	inline static int32_t get_offset_of_origin_6() { return static_cast<int32_t>(offsetof(SelectionBox_t2044353942, ___origin_6)); }
	inline Vector2_t2156229523  get_origin_6() const { return ___origin_6; }
	inline Vector2_t2156229523 * get_address_of_origin_6() { return &___origin_6; }
	inline void set_origin_6(Vector2_t2156229523  value)
	{
		___origin_6 = value;
	}

	inline static int32_t get_offset_of_selectionMask_7() { return static_cast<int32_t>(offsetof(SelectionBox_t2044353942, ___selectionMask_7)); }
	inline RectTransform_t3704657025 * get_selectionMask_7() const { return ___selectionMask_7; }
	inline RectTransform_t3704657025 ** get_address_of_selectionMask_7() { return &___selectionMask_7; }
	inline void set_selectionMask_7(RectTransform_t3704657025 * value)
	{
		___selectionMask_7 = value;
		Il2CppCodeGenWriteBarrier((&___selectionMask_7), value);
	}

	inline static int32_t get_offset_of_boxRect_8() { return static_cast<int32_t>(offsetof(SelectionBox_t2044353942, ___boxRect_8)); }
	inline RectTransform_t3704657025 * get_boxRect_8() const { return ___boxRect_8; }
	inline RectTransform_t3704657025 ** get_address_of_boxRect_8() { return &___boxRect_8; }
	inline void set_boxRect_8(RectTransform_t3704657025 * value)
	{
		___boxRect_8 = value;
		Il2CppCodeGenWriteBarrier((&___boxRect_8), value);
	}

	inline static int32_t get_offset_of_selectables_9() { return static_cast<int32_t>(offsetof(SelectionBox_t2044353942, ___selectables_9)); }
	inline IBoxSelectableU5BU5D_t183195169* get_selectables_9() const { return ___selectables_9; }
	inline IBoxSelectableU5BU5D_t183195169** get_address_of_selectables_9() { return &___selectables_9; }
	inline void set_selectables_9(IBoxSelectableU5BU5D_t183195169* value)
	{
		___selectables_9 = value;
		Il2CppCodeGenWriteBarrier((&___selectables_9), value);
	}

	inline static int32_t get_offset_of_selectableGroup_10() { return static_cast<int32_t>(offsetof(SelectionBox_t2044353942, ___selectableGroup_10)); }
	inline MonoBehaviourU5BU5D_t2007329276* get_selectableGroup_10() const { return ___selectableGroup_10; }
	inline MonoBehaviourU5BU5D_t2007329276** get_address_of_selectableGroup_10() { return &___selectableGroup_10; }
	inline void set_selectableGroup_10(MonoBehaviourU5BU5D_t2007329276* value)
	{
		___selectableGroup_10 = value;
		Il2CppCodeGenWriteBarrier((&___selectableGroup_10), value);
	}

	inline static int32_t get_offset_of_clickedBeforeDrag_11() { return static_cast<int32_t>(offsetof(SelectionBox_t2044353942, ___clickedBeforeDrag_11)); }
	inline RuntimeObject* get_clickedBeforeDrag_11() const { return ___clickedBeforeDrag_11; }
	inline RuntimeObject** get_address_of_clickedBeforeDrag_11() { return &___clickedBeforeDrag_11; }
	inline void set_clickedBeforeDrag_11(RuntimeObject* value)
	{
		___clickedBeforeDrag_11 = value;
		Il2CppCodeGenWriteBarrier((&___clickedBeforeDrag_11), value);
	}

	inline static int32_t get_offset_of_clickedAfterDrag_12() { return static_cast<int32_t>(offsetof(SelectionBox_t2044353942, ___clickedAfterDrag_12)); }
	inline RuntimeObject* get_clickedAfterDrag_12() const { return ___clickedAfterDrag_12; }
	inline RuntimeObject** get_address_of_clickedAfterDrag_12() { return &___clickedAfterDrag_12; }
	inline void set_clickedAfterDrag_12(RuntimeObject* value)
	{
		___clickedAfterDrag_12 = value;
		Il2CppCodeGenWriteBarrier((&___clickedAfterDrag_12), value);
	}

	inline static int32_t get_offset_of_onSelectionChange_13() { return static_cast<int32_t>(offsetof(SelectionBox_t2044353942, ___onSelectionChange_13)); }
	inline SelectionEvent_t3355704588 * get_onSelectionChange_13() const { return ___onSelectionChange_13; }
	inline SelectionEvent_t3355704588 ** get_address_of_onSelectionChange_13() { return &___onSelectionChange_13; }
	inline void set_onSelectionChange_13(SelectionEvent_t3355704588 * value)
	{
		___onSelectionChange_13 = value;
		Il2CppCodeGenWriteBarrier((&___onSelectionChange_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONBOX_T2044353942_H
#ifndef SHINEEFFECTOR_T3463369172_H
#define SHINEEFFECTOR_T3463369172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ShineEffector
struct  ShineEffector_t3463369172  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.ShineEffect UnityEngine.UI.Extensions.ShineEffector::effector
	ShineEffect_t3679628888 * ___effector_4;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.ShineEffector::effectRoot
	GameObject_t1113636619 * ___effectRoot_5;
	// System.Single UnityEngine.UI.Extensions.ShineEffector::yOffset
	float ___yOffset_6;
	// System.Single UnityEngine.UI.Extensions.ShineEffector::width
	float ___width_7;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ShineEffector::effectorRect
	RectTransform_t3704657025 * ___effectorRect_8;

public:
	inline static int32_t get_offset_of_effector_4() { return static_cast<int32_t>(offsetof(ShineEffector_t3463369172, ___effector_4)); }
	inline ShineEffect_t3679628888 * get_effector_4() const { return ___effector_4; }
	inline ShineEffect_t3679628888 ** get_address_of_effector_4() { return &___effector_4; }
	inline void set_effector_4(ShineEffect_t3679628888 * value)
	{
		___effector_4 = value;
		Il2CppCodeGenWriteBarrier((&___effector_4), value);
	}

	inline static int32_t get_offset_of_effectRoot_5() { return static_cast<int32_t>(offsetof(ShineEffector_t3463369172, ___effectRoot_5)); }
	inline GameObject_t1113636619 * get_effectRoot_5() const { return ___effectRoot_5; }
	inline GameObject_t1113636619 ** get_address_of_effectRoot_5() { return &___effectRoot_5; }
	inline void set_effectRoot_5(GameObject_t1113636619 * value)
	{
		___effectRoot_5 = value;
		Il2CppCodeGenWriteBarrier((&___effectRoot_5), value);
	}

	inline static int32_t get_offset_of_yOffset_6() { return static_cast<int32_t>(offsetof(ShineEffector_t3463369172, ___yOffset_6)); }
	inline float get_yOffset_6() const { return ___yOffset_6; }
	inline float* get_address_of_yOffset_6() { return &___yOffset_6; }
	inline void set_yOffset_6(float value)
	{
		___yOffset_6 = value;
	}

	inline static int32_t get_offset_of_width_7() { return static_cast<int32_t>(offsetof(ShineEffector_t3463369172, ___width_7)); }
	inline float get_width_7() const { return ___width_7; }
	inline float* get_address_of_width_7() { return &___width_7; }
	inline void set_width_7(float value)
	{
		___width_7 = value;
	}

	inline static int32_t get_offset_of_effectorRect_8() { return static_cast<int32_t>(offsetof(ShineEffector_t3463369172, ___effectorRect_8)); }
	inline RectTransform_t3704657025 * get_effectorRect_8() const { return ___effectorRect_8; }
	inline RectTransform_t3704657025 ** get_address_of_effectorRect_8() { return &___effectorRect_8; }
	inline void set_effectorRect_8(RectTransform_t3704657025 * value)
	{
		___effectorRect_8 = value;
		Il2CppCodeGenWriteBarrier((&___effectorRect_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHINEEFFECTOR_T3463369172_H
#ifndef SOFTMASKSCRIPT_T2195956899_H
#define SOFTMASKSCRIPT_T2195956899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SoftMaskScript
struct  SoftMaskScript_t2195956899  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material UnityEngine.UI.Extensions.SoftMaskScript::mat
	Material_t340375123 * ___mat_4;
	// UnityEngine.Canvas UnityEngine.UI.Extensions.SoftMaskScript::cachedCanvas
	Canvas_t3310196443 * ___cachedCanvas_5;
	// UnityEngine.Transform UnityEngine.UI.Extensions.SoftMaskScript::cachedCanvasTransform
	Transform_t3600365921 * ___cachedCanvasTransform_6;
	// UnityEngine.Vector3[] UnityEngine.UI.Extensions.SoftMaskScript::m_WorldCorners
	Vector3U5BU5D_t1718750761* ___m_WorldCorners_7;
	// UnityEngine.Vector3[] UnityEngine.UI.Extensions.SoftMaskScript::m_CanvasCorners
	Vector3U5BU5D_t1718750761* ___m_CanvasCorners_8;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.SoftMaskScript::MaskArea
	RectTransform_t3704657025 * ___MaskArea_9;
	// UnityEngine.Texture UnityEngine.UI.Extensions.SoftMaskScript::AlphaMask
	Texture_t3661962703 * ___AlphaMask_10;
	// System.Single UnityEngine.UI.Extensions.SoftMaskScript::CutOff
	float ___CutOff_11;
	// System.Boolean UnityEngine.UI.Extensions.SoftMaskScript::HardBlend
	bool ___HardBlend_12;
	// System.Boolean UnityEngine.UI.Extensions.SoftMaskScript::FlipAlphaMask
	bool ___FlipAlphaMask_13;
	// System.Boolean UnityEngine.UI.Extensions.SoftMaskScript::DontClipMaskScalingRect
	bool ___DontClipMaskScalingRect_14;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.SoftMaskScript::maskOffset
	Vector2_t2156229523  ___maskOffset_15;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.SoftMaskScript::maskScale
	Vector2_t2156229523  ___maskScale_16;

public:
	inline static int32_t get_offset_of_mat_4() { return static_cast<int32_t>(offsetof(SoftMaskScript_t2195956899, ___mat_4)); }
	inline Material_t340375123 * get_mat_4() const { return ___mat_4; }
	inline Material_t340375123 ** get_address_of_mat_4() { return &___mat_4; }
	inline void set_mat_4(Material_t340375123 * value)
	{
		___mat_4 = value;
		Il2CppCodeGenWriteBarrier((&___mat_4), value);
	}

	inline static int32_t get_offset_of_cachedCanvas_5() { return static_cast<int32_t>(offsetof(SoftMaskScript_t2195956899, ___cachedCanvas_5)); }
	inline Canvas_t3310196443 * get_cachedCanvas_5() const { return ___cachedCanvas_5; }
	inline Canvas_t3310196443 ** get_address_of_cachedCanvas_5() { return &___cachedCanvas_5; }
	inline void set_cachedCanvas_5(Canvas_t3310196443 * value)
	{
		___cachedCanvas_5 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCanvas_5), value);
	}

	inline static int32_t get_offset_of_cachedCanvasTransform_6() { return static_cast<int32_t>(offsetof(SoftMaskScript_t2195956899, ___cachedCanvasTransform_6)); }
	inline Transform_t3600365921 * get_cachedCanvasTransform_6() const { return ___cachedCanvasTransform_6; }
	inline Transform_t3600365921 ** get_address_of_cachedCanvasTransform_6() { return &___cachedCanvasTransform_6; }
	inline void set_cachedCanvasTransform_6(Transform_t3600365921 * value)
	{
		___cachedCanvasTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCanvasTransform_6), value);
	}

	inline static int32_t get_offset_of_m_WorldCorners_7() { return static_cast<int32_t>(offsetof(SoftMaskScript_t2195956899, ___m_WorldCorners_7)); }
	inline Vector3U5BU5D_t1718750761* get_m_WorldCorners_7() const { return ___m_WorldCorners_7; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_WorldCorners_7() { return &___m_WorldCorners_7; }
	inline void set_m_WorldCorners_7(Vector3U5BU5D_t1718750761* value)
	{
		___m_WorldCorners_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_WorldCorners_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasCorners_8() { return static_cast<int32_t>(offsetof(SoftMaskScript_t2195956899, ___m_CanvasCorners_8)); }
	inline Vector3U5BU5D_t1718750761* get_m_CanvasCorners_8() const { return ___m_CanvasCorners_8; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_CanvasCorners_8() { return &___m_CanvasCorners_8; }
	inline void set_m_CanvasCorners_8(Vector3U5BU5D_t1718750761* value)
	{
		___m_CanvasCorners_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasCorners_8), value);
	}

	inline static int32_t get_offset_of_MaskArea_9() { return static_cast<int32_t>(offsetof(SoftMaskScript_t2195956899, ___MaskArea_9)); }
	inline RectTransform_t3704657025 * get_MaskArea_9() const { return ___MaskArea_9; }
	inline RectTransform_t3704657025 ** get_address_of_MaskArea_9() { return &___MaskArea_9; }
	inline void set_MaskArea_9(RectTransform_t3704657025 * value)
	{
		___MaskArea_9 = value;
		Il2CppCodeGenWriteBarrier((&___MaskArea_9), value);
	}

	inline static int32_t get_offset_of_AlphaMask_10() { return static_cast<int32_t>(offsetof(SoftMaskScript_t2195956899, ___AlphaMask_10)); }
	inline Texture_t3661962703 * get_AlphaMask_10() const { return ___AlphaMask_10; }
	inline Texture_t3661962703 ** get_address_of_AlphaMask_10() { return &___AlphaMask_10; }
	inline void set_AlphaMask_10(Texture_t3661962703 * value)
	{
		___AlphaMask_10 = value;
		Il2CppCodeGenWriteBarrier((&___AlphaMask_10), value);
	}

	inline static int32_t get_offset_of_CutOff_11() { return static_cast<int32_t>(offsetof(SoftMaskScript_t2195956899, ___CutOff_11)); }
	inline float get_CutOff_11() const { return ___CutOff_11; }
	inline float* get_address_of_CutOff_11() { return &___CutOff_11; }
	inline void set_CutOff_11(float value)
	{
		___CutOff_11 = value;
	}

	inline static int32_t get_offset_of_HardBlend_12() { return static_cast<int32_t>(offsetof(SoftMaskScript_t2195956899, ___HardBlend_12)); }
	inline bool get_HardBlend_12() const { return ___HardBlend_12; }
	inline bool* get_address_of_HardBlend_12() { return &___HardBlend_12; }
	inline void set_HardBlend_12(bool value)
	{
		___HardBlend_12 = value;
	}

	inline static int32_t get_offset_of_FlipAlphaMask_13() { return static_cast<int32_t>(offsetof(SoftMaskScript_t2195956899, ___FlipAlphaMask_13)); }
	inline bool get_FlipAlphaMask_13() const { return ___FlipAlphaMask_13; }
	inline bool* get_address_of_FlipAlphaMask_13() { return &___FlipAlphaMask_13; }
	inline void set_FlipAlphaMask_13(bool value)
	{
		___FlipAlphaMask_13 = value;
	}

	inline static int32_t get_offset_of_DontClipMaskScalingRect_14() { return static_cast<int32_t>(offsetof(SoftMaskScript_t2195956899, ___DontClipMaskScalingRect_14)); }
	inline bool get_DontClipMaskScalingRect_14() const { return ___DontClipMaskScalingRect_14; }
	inline bool* get_address_of_DontClipMaskScalingRect_14() { return &___DontClipMaskScalingRect_14; }
	inline void set_DontClipMaskScalingRect_14(bool value)
	{
		___DontClipMaskScalingRect_14 = value;
	}

	inline static int32_t get_offset_of_maskOffset_15() { return static_cast<int32_t>(offsetof(SoftMaskScript_t2195956899, ___maskOffset_15)); }
	inline Vector2_t2156229523  get_maskOffset_15() const { return ___maskOffset_15; }
	inline Vector2_t2156229523 * get_address_of_maskOffset_15() { return &___maskOffset_15; }
	inline void set_maskOffset_15(Vector2_t2156229523  value)
	{
		___maskOffset_15 = value;
	}

	inline static int32_t get_offset_of_maskScale_16() { return static_cast<int32_t>(offsetof(SoftMaskScript_t2195956899, ___maskScale_16)); }
	inline Vector2_t2156229523  get_maskScale_16() const { return ___maskScale_16; }
	inline Vector2_t2156229523 * get_address_of_maskScale_16() { return &___maskScale_16; }
	inline void set_maskScale_16(Vector2_t2156229523  value)
	{
		___maskScale_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOFTMASKSCRIPT_T2195956899_H
#ifndef TILTWINDOW_T4093125161_H
#define TILTWINDOW_T4093125161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.TiltWindow
struct  TiltWindow_t4093125161  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.TiltWindow::range
	Vector2_t2156229523  ___range_4;
	// UnityEngine.Transform UnityEngine.UI.Extensions.TiltWindow::mTrans
	Transform_t3600365921 * ___mTrans_5;
	// UnityEngine.Quaternion UnityEngine.UI.Extensions.TiltWindow::mStart
	Quaternion_t2301928331  ___mStart_6;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.TiltWindow::mRot
	Vector2_t2156229523  ___mRot_7;

public:
	inline static int32_t get_offset_of_range_4() { return static_cast<int32_t>(offsetof(TiltWindow_t4093125161, ___range_4)); }
	inline Vector2_t2156229523  get_range_4() const { return ___range_4; }
	inline Vector2_t2156229523 * get_address_of_range_4() { return &___range_4; }
	inline void set_range_4(Vector2_t2156229523  value)
	{
		___range_4 = value;
	}

	inline static int32_t get_offset_of_mTrans_5() { return static_cast<int32_t>(offsetof(TiltWindow_t4093125161, ___mTrans_5)); }
	inline Transform_t3600365921 * get_mTrans_5() const { return ___mTrans_5; }
	inline Transform_t3600365921 ** get_address_of_mTrans_5() { return &___mTrans_5; }
	inline void set_mTrans_5(Transform_t3600365921 * value)
	{
		___mTrans_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_5), value);
	}

	inline static int32_t get_offset_of_mStart_6() { return static_cast<int32_t>(offsetof(TiltWindow_t4093125161, ___mStart_6)); }
	inline Quaternion_t2301928331  get_mStart_6() const { return ___mStart_6; }
	inline Quaternion_t2301928331 * get_address_of_mStart_6() { return &___mStart_6; }
	inline void set_mStart_6(Quaternion_t2301928331  value)
	{
		___mStart_6 = value;
	}

	inline static int32_t get_offset_of_mRot_7() { return static_cast<int32_t>(offsetof(TiltWindow_t4093125161, ___mRot_7)); }
	inline Vector2_t2156229523  get_mRot_7() const { return ___mRot_7; }
	inline Vector2_t2156229523 * get_address_of_mRot_7() { return &___mRot_7; }
	inline void set_mRot_7(Vector2_t2156229523  value)
	{
		___mRot_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTWINDOW_T4093125161_H
#ifndef UIADDITIVEEFFECT_T1803193993_H
#define UIADDITIVEEFFECT_T1803193993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIAdditiveEffect
struct  UIAdditiveEffect_t1803193993  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.MaskableGraphic UnityEngine.UI.Extensions.UIAdditiveEffect::mGraphic
	MaskableGraphic_t3839221559 * ___mGraphic_4;

public:
	inline static int32_t get_offset_of_mGraphic_4() { return static_cast<int32_t>(offsetof(UIAdditiveEffect_t1803193993, ___mGraphic_4)); }
	inline MaskableGraphic_t3839221559 * get_mGraphic_4() const { return ___mGraphic_4; }
	inline MaskableGraphic_t3839221559 ** get_address_of_mGraphic_4() { return &___mGraphic_4; }
	inline void set_mGraphic_4(MaskableGraphic_t3839221559 * value)
	{
		___mGraphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___mGraphic_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIADDITIVEEFFECT_T1803193993_H
#ifndef UIIMAGECROP_T41034629_H
#define UIIMAGECROP_T41034629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIImageCrop
struct  UIImageCrop_t41034629  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.MaskableGraphic UnityEngine.UI.Extensions.UIImageCrop::mGraphic
	MaskableGraphic_t3839221559 * ___mGraphic_4;
	// UnityEngine.Material UnityEngine.UI.Extensions.UIImageCrop::mat
	Material_t340375123 * ___mat_5;
	// System.Int32 UnityEngine.UI.Extensions.UIImageCrop::XCropProperty
	int32_t ___XCropProperty_6;
	// System.Int32 UnityEngine.UI.Extensions.UIImageCrop::YCropProperty
	int32_t ___YCropProperty_7;
	// System.Single UnityEngine.UI.Extensions.UIImageCrop::XCrop
	float ___XCrop_8;
	// System.Single UnityEngine.UI.Extensions.UIImageCrop::YCrop
	float ___YCrop_9;

public:
	inline static int32_t get_offset_of_mGraphic_4() { return static_cast<int32_t>(offsetof(UIImageCrop_t41034629, ___mGraphic_4)); }
	inline MaskableGraphic_t3839221559 * get_mGraphic_4() const { return ___mGraphic_4; }
	inline MaskableGraphic_t3839221559 ** get_address_of_mGraphic_4() { return &___mGraphic_4; }
	inline void set_mGraphic_4(MaskableGraphic_t3839221559 * value)
	{
		___mGraphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___mGraphic_4), value);
	}

	inline static int32_t get_offset_of_mat_5() { return static_cast<int32_t>(offsetof(UIImageCrop_t41034629, ___mat_5)); }
	inline Material_t340375123 * get_mat_5() const { return ___mat_5; }
	inline Material_t340375123 ** get_address_of_mat_5() { return &___mat_5; }
	inline void set_mat_5(Material_t340375123 * value)
	{
		___mat_5 = value;
		Il2CppCodeGenWriteBarrier((&___mat_5), value);
	}

	inline static int32_t get_offset_of_XCropProperty_6() { return static_cast<int32_t>(offsetof(UIImageCrop_t41034629, ___XCropProperty_6)); }
	inline int32_t get_XCropProperty_6() const { return ___XCropProperty_6; }
	inline int32_t* get_address_of_XCropProperty_6() { return &___XCropProperty_6; }
	inline void set_XCropProperty_6(int32_t value)
	{
		___XCropProperty_6 = value;
	}

	inline static int32_t get_offset_of_YCropProperty_7() { return static_cast<int32_t>(offsetof(UIImageCrop_t41034629, ___YCropProperty_7)); }
	inline int32_t get_YCropProperty_7() const { return ___YCropProperty_7; }
	inline int32_t* get_address_of_YCropProperty_7() { return &___YCropProperty_7; }
	inline void set_YCropProperty_7(int32_t value)
	{
		___YCropProperty_7 = value;
	}

	inline static int32_t get_offset_of_XCrop_8() { return static_cast<int32_t>(offsetof(UIImageCrop_t41034629, ___XCrop_8)); }
	inline float get_XCrop_8() const { return ___XCrop_8; }
	inline float* get_address_of_XCrop_8() { return &___XCrop_8; }
	inline void set_XCrop_8(float value)
	{
		___XCrop_8 = value;
	}

	inline static int32_t get_offset_of_YCrop_9() { return static_cast<int32_t>(offsetof(UIImageCrop_t41034629, ___YCrop_9)); }
	inline float get_YCrop_9() const { return ___YCrop_9; }
	inline float* get_address_of_YCrop_9() { return &___YCrop_9; }
	inline void set_YCrop_9(float value)
	{
		___YCrop_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIIMAGECROP_T41034629_H
#ifndef UILINEARDODGEEFFECT_T3706909257_H
#define UILINEARDODGEEFFECT_T3706909257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UILinearDodgeEffect
struct  UILinearDodgeEffect_t3706909257  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.MaskableGraphic UnityEngine.UI.Extensions.UILinearDodgeEffect::mGraphic
	MaskableGraphic_t3839221559 * ___mGraphic_4;

public:
	inline static int32_t get_offset_of_mGraphic_4() { return static_cast<int32_t>(offsetof(UILinearDodgeEffect_t3706909257, ___mGraphic_4)); }
	inline MaskableGraphic_t3839221559 * get_mGraphic_4() const { return ___mGraphic_4; }
	inline MaskableGraphic_t3839221559 ** get_address_of_mGraphic_4() { return &___mGraphic_4; }
	inline void set_mGraphic_4(MaskableGraphic_t3839221559 * value)
	{
		___mGraphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___mGraphic_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILINEARDODGEEFFECT_T3706909257_H
#ifndef UIMULTIPLYEFFECT_T3837490265_H
#define UIMULTIPLYEFFECT_T3837490265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIMultiplyEffect
struct  UIMultiplyEffect_t3837490265  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.MaskableGraphic UnityEngine.UI.Extensions.UIMultiplyEffect::mGraphic
	MaskableGraphic_t3839221559 * ___mGraphic_4;

public:
	inline static int32_t get_offset_of_mGraphic_4() { return static_cast<int32_t>(offsetof(UIMultiplyEffect_t3837490265, ___mGraphic_4)); }
	inline MaskableGraphic_t3839221559 * get_mGraphic_4() const { return ___mGraphic_4; }
	inline MaskableGraphic_t3839221559 ** get_address_of_mGraphic_4() { return &___mGraphic_4; }
	inline void set_mGraphic_4(MaskableGraphic_t3839221559 * value)
	{
		___mGraphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___mGraphic_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIMULTIPLYEFFECT_T3837490265_H
#ifndef UISCREENEFFECT_T1657282205_H
#define UISCREENEFFECT_T1657282205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIScreenEffect
struct  UIScreenEffect_t1657282205  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.MaskableGraphic UnityEngine.UI.Extensions.UIScreenEffect::mGraphic
	MaskableGraphic_t3839221559 * ___mGraphic_4;

public:
	inline static int32_t get_offset_of_mGraphic_4() { return static_cast<int32_t>(offsetof(UIScreenEffect_t1657282205, ___mGraphic_4)); }
	inline MaskableGraphic_t3839221559 * get_mGraphic_4() const { return ___mGraphic_4; }
	inline MaskableGraphic_t3839221559 ** get_address_of_mGraphic_4() { return &___mGraphic_4; }
	inline void set_mGraphic_4(MaskableGraphic_t3839221559 * value)
	{
		___mGraphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___mGraphic_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCREENEFFECT_T1657282205_H
#ifndef UISOFTADDITIVEEFFECT_T1842650896_H
#define UISOFTADDITIVEEFFECT_T1842650896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UISoftAdditiveEffect
struct  UISoftAdditiveEffect_t1842650896  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.MaskableGraphic UnityEngine.UI.Extensions.UISoftAdditiveEffect::mGraphic
	MaskableGraphic_t3839221559 * ___mGraphic_4;

public:
	inline static int32_t get_offset_of_mGraphic_4() { return static_cast<int32_t>(offsetof(UISoftAdditiveEffect_t1842650896, ___mGraphic_4)); }
	inline MaskableGraphic_t3839221559 * get_mGraphic_4() const { return ___mGraphic_4; }
	inline MaskableGraphic_t3839221559 ** get_address_of_mGraphic_4() { return &___mGraphic_4; }
	inline void set_mGraphic_4(MaskableGraphic_t3839221559 * value)
	{
		___mGraphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___mGraphic_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISOFTADDITIVEEFFECT_T1842650896_H
#ifndef UI_KNOB_T1735628298_H
#define UI_KNOB_T1735628298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UI_Knob
struct  UI_Knob_t1735628298  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.UI_Knob/Direction UnityEngine.UI.Extensions.UI_Knob::direction
	int32_t ___direction_4;
	// System.Single UnityEngine.UI.Extensions.UI_Knob::knobValue
	float ___knobValue_5;
	// System.Single UnityEngine.UI.Extensions.UI_Knob::maxValue
	float ___maxValue_6;
	// System.Int32 UnityEngine.UI.Extensions.UI_Knob::loops
	int32_t ___loops_7;
	// System.Boolean UnityEngine.UI.Extensions.UI_Knob::clampOutput01
	bool ___clampOutput01_8;
	// System.Boolean UnityEngine.UI.Extensions.UI_Knob::snapToPosition
	bool ___snapToPosition_9;
	// System.Int32 UnityEngine.UI.Extensions.UI_Knob::snapStepsPerLoop
	int32_t ___snapStepsPerLoop_10;
	// UnityEngine.UI.Extensions.KnobFloatValueEvent UnityEngine.UI.Extensions.UI_Knob::OnValueChanged
	KnobFloatValueEvent_t1285673625 * ___OnValueChanged_11;
	// System.Single UnityEngine.UI.Extensions.UI_Knob::_currentLoops
	float ____currentLoops_12;
	// System.Single UnityEngine.UI.Extensions.UI_Knob::_previousValue
	float ____previousValue_13;
	// System.Single UnityEngine.UI.Extensions.UI_Knob::_initAngle
	float ____initAngle_14;
	// System.Single UnityEngine.UI.Extensions.UI_Knob::_currentAngle
	float ____currentAngle_15;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.UI_Knob::_currentVector
	Vector2_t2156229523  ____currentVector_16;
	// UnityEngine.Quaternion UnityEngine.UI.Extensions.UI_Knob::_initRotation
	Quaternion_t2301928331  ____initRotation_17;
	// System.Boolean UnityEngine.UI.Extensions.UI_Knob::_canDrag
	bool ____canDrag_18;

public:
	inline static int32_t get_offset_of_direction_4() { return static_cast<int32_t>(offsetof(UI_Knob_t1735628298, ___direction_4)); }
	inline int32_t get_direction_4() const { return ___direction_4; }
	inline int32_t* get_address_of_direction_4() { return &___direction_4; }
	inline void set_direction_4(int32_t value)
	{
		___direction_4 = value;
	}

	inline static int32_t get_offset_of_knobValue_5() { return static_cast<int32_t>(offsetof(UI_Knob_t1735628298, ___knobValue_5)); }
	inline float get_knobValue_5() const { return ___knobValue_5; }
	inline float* get_address_of_knobValue_5() { return &___knobValue_5; }
	inline void set_knobValue_5(float value)
	{
		___knobValue_5 = value;
	}

	inline static int32_t get_offset_of_maxValue_6() { return static_cast<int32_t>(offsetof(UI_Knob_t1735628298, ___maxValue_6)); }
	inline float get_maxValue_6() const { return ___maxValue_6; }
	inline float* get_address_of_maxValue_6() { return &___maxValue_6; }
	inline void set_maxValue_6(float value)
	{
		___maxValue_6 = value;
	}

	inline static int32_t get_offset_of_loops_7() { return static_cast<int32_t>(offsetof(UI_Knob_t1735628298, ___loops_7)); }
	inline int32_t get_loops_7() const { return ___loops_7; }
	inline int32_t* get_address_of_loops_7() { return &___loops_7; }
	inline void set_loops_7(int32_t value)
	{
		___loops_7 = value;
	}

	inline static int32_t get_offset_of_clampOutput01_8() { return static_cast<int32_t>(offsetof(UI_Knob_t1735628298, ___clampOutput01_8)); }
	inline bool get_clampOutput01_8() const { return ___clampOutput01_8; }
	inline bool* get_address_of_clampOutput01_8() { return &___clampOutput01_8; }
	inline void set_clampOutput01_8(bool value)
	{
		___clampOutput01_8 = value;
	}

	inline static int32_t get_offset_of_snapToPosition_9() { return static_cast<int32_t>(offsetof(UI_Knob_t1735628298, ___snapToPosition_9)); }
	inline bool get_snapToPosition_9() const { return ___snapToPosition_9; }
	inline bool* get_address_of_snapToPosition_9() { return &___snapToPosition_9; }
	inline void set_snapToPosition_9(bool value)
	{
		___snapToPosition_9 = value;
	}

	inline static int32_t get_offset_of_snapStepsPerLoop_10() { return static_cast<int32_t>(offsetof(UI_Knob_t1735628298, ___snapStepsPerLoop_10)); }
	inline int32_t get_snapStepsPerLoop_10() const { return ___snapStepsPerLoop_10; }
	inline int32_t* get_address_of_snapStepsPerLoop_10() { return &___snapStepsPerLoop_10; }
	inline void set_snapStepsPerLoop_10(int32_t value)
	{
		___snapStepsPerLoop_10 = value;
	}

	inline static int32_t get_offset_of_OnValueChanged_11() { return static_cast<int32_t>(offsetof(UI_Knob_t1735628298, ___OnValueChanged_11)); }
	inline KnobFloatValueEvent_t1285673625 * get_OnValueChanged_11() const { return ___OnValueChanged_11; }
	inline KnobFloatValueEvent_t1285673625 ** get_address_of_OnValueChanged_11() { return &___OnValueChanged_11; }
	inline void set_OnValueChanged_11(KnobFloatValueEvent_t1285673625 * value)
	{
		___OnValueChanged_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnValueChanged_11), value);
	}

	inline static int32_t get_offset_of__currentLoops_12() { return static_cast<int32_t>(offsetof(UI_Knob_t1735628298, ____currentLoops_12)); }
	inline float get__currentLoops_12() const { return ____currentLoops_12; }
	inline float* get_address_of__currentLoops_12() { return &____currentLoops_12; }
	inline void set__currentLoops_12(float value)
	{
		____currentLoops_12 = value;
	}

	inline static int32_t get_offset_of__previousValue_13() { return static_cast<int32_t>(offsetof(UI_Knob_t1735628298, ____previousValue_13)); }
	inline float get__previousValue_13() const { return ____previousValue_13; }
	inline float* get_address_of__previousValue_13() { return &____previousValue_13; }
	inline void set__previousValue_13(float value)
	{
		____previousValue_13 = value;
	}

	inline static int32_t get_offset_of__initAngle_14() { return static_cast<int32_t>(offsetof(UI_Knob_t1735628298, ____initAngle_14)); }
	inline float get__initAngle_14() const { return ____initAngle_14; }
	inline float* get_address_of__initAngle_14() { return &____initAngle_14; }
	inline void set__initAngle_14(float value)
	{
		____initAngle_14 = value;
	}

	inline static int32_t get_offset_of__currentAngle_15() { return static_cast<int32_t>(offsetof(UI_Knob_t1735628298, ____currentAngle_15)); }
	inline float get__currentAngle_15() const { return ____currentAngle_15; }
	inline float* get_address_of__currentAngle_15() { return &____currentAngle_15; }
	inline void set__currentAngle_15(float value)
	{
		____currentAngle_15 = value;
	}

	inline static int32_t get_offset_of__currentVector_16() { return static_cast<int32_t>(offsetof(UI_Knob_t1735628298, ____currentVector_16)); }
	inline Vector2_t2156229523  get__currentVector_16() const { return ____currentVector_16; }
	inline Vector2_t2156229523 * get_address_of__currentVector_16() { return &____currentVector_16; }
	inline void set__currentVector_16(Vector2_t2156229523  value)
	{
		____currentVector_16 = value;
	}

	inline static int32_t get_offset_of__initRotation_17() { return static_cast<int32_t>(offsetof(UI_Knob_t1735628298, ____initRotation_17)); }
	inline Quaternion_t2301928331  get__initRotation_17() const { return ____initRotation_17; }
	inline Quaternion_t2301928331 * get_address_of__initRotation_17() { return &____initRotation_17; }
	inline void set__initRotation_17(Quaternion_t2301928331  value)
	{
		____initRotation_17 = value;
	}

	inline static int32_t get_offset_of__canDrag_18() { return static_cast<int32_t>(offsetof(UI_Knob_t1735628298, ____canDrag_18)); }
	inline bool get__canDrag_18() const { return ____canDrag_18; }
	inline bool* get_address_of__canDrag_18() { return &____canDrag_18; }
	inline void set__canDrag_18(bool value)
	{
		____canDrag_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UI_KNOB_T1735628298_H
#ifndef BASEINPUTMODULE_T2019268878_H
#define BASEINPUTMODULE_T2019268878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t2019268878  : public UIBehaviour_t3495933518
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t537414295 * ___m_RaycastResultCache_4;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t2331243652 * ___m_AxisEventData_5;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_6;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t3903027533 * ___m_BaseEventData_7;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t3630163547 * ___m_InputOverride_8;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t3630163547 * ___m_DefaultInput_9;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_RaycastResultCache_4)); }
	inline List_1_t537414295 * get_m_RaycastResultCache_4() const { return ___m_RaycastResultCache_4; }
	inline List_1_t537414295 ** get_address_of_m_RaycastResultCache_4() { return &___m_RaycastResultCache_4; }
	inline void set_m_RaycastResultCache_4(List_1_t537414295 * value)
	{
		___m_RaycastResultCache_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_4), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_AxisEventData_5)); }
	inline AxisEventData_t2331243652 * get_m_AxisEventData_5() const { return ___m_AxisEventData_5; }
	inline AxisEventData_t2331243652 ** get_address_of_m_AxisEventData_5() { return &___m_AxisEventData_5; }
	inline void set_m_AxisEventData_5(AxisEventData_t2331243652 * value)
	{
		___m_AxisEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_5), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_EventSystem_6)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_6() const { return ___m_EventSystem_6; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_6() { return &___m_EventSystem_6; }
	inline void set_m_EventSystem_6(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_6), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_BaseEventData_7)); }
	inline BaseEventData_t3903027533 * get_m_BaseEventData_7() const { return ___m_BaseEventData_7; }
	inline BaseEventData_t3903027533 ** get_address_of_m_BaseEventData_7() { return &___m_BaseEventData_7; }
	inline void set_m_BaseEventData_7(BaseEventData_t3903027533 * value)
	{
		___m_BaseEventData_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_7), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_8() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_InputOverride_8)); }
	inline BaseInput_t3630163547 * get_m_InputOverride_8() const { return ___m_InputOverride_8; }
	inline BaseInput_t3630163547 ** get_address_of_m_InputOverride_8() { return &___m_InputOverride_8; }
	inline void set_m_InputOverride_8(BaseInput_t3630163547 * value)
	{
		___m_InputOverride_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_8), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_9() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_DefaultInput_9)); }
	inline BaseInput_t3630163547 * get_m_DefaultInput_9() const { return ___m_DefaultInput_9; }
	inline BaseInput_t3630163547 ** get_address_of_m_DefaultInput_9() { return &___m_DefaultInput_9; }
	inline void set_m_DefaultInput_9(BaseInput_t3630163547 * value)
	{
		___m_DefaultInput_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T2019268878_H
#ifndef BASEMESHEFFECT_T2440176439_H
#define BASEMESHEFFECT_T2440176439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t2440176439  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t1660335611 * ___m_Graphic_4;

public:
	inline static int32_t get_offset_of_m_Graphic_4() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t2440176439, ___m_Graphic_4)); }
	inline Graphic_t1660335611 * get_m_Graphic_4() const { return ___m_Graphic_4; }
	inline Graphic_t1660335611 ** get_address_of_m_Graphic_4() { return &___m_Graphic_4; }
	inline void set_m_Graphic_4(Graphic_t1660335611 * value)
	{
		___m_Graphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T2440176439_H
#ifndef SEGMENT_T1973990243_H
#define SEGMENT_T1973990243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Segment
struct  Segment_t1973990243  : public UIBehaviour_t3495933518
{
public:
	// System.Int32 UnityEngine.UI.Extensions.Segment::index
	int32_t ___index_4;
	// UnityEngine.Color UnityEngine.UI.Extensions.Segment::textColor
	Color_t2555686324  ___textColor_5;

public:
	inline static int32_t get_offset_of_index_4() { return static_cast<int32_t>(offsetof(Segment_t1973990243, ___index_4)); }
	inline int32_t get_index_4() const { return ___index_4; }
	inline int32_t* get_address_of_index_4() { return &___index_4; }
	inline void set_index_4(int32_t value)
	{
		___index_4 = value;
	}

	inline static int32_t get_offset_of_textColor_5() { return static_cast<int32_t>(offsetof(Segment_t1973990243, ___textColor_5)); }
	inline Color_t2555686324  get_textColor_5() const { return ___textColor_5; }
	inline Color_t2555686324 * get_address_of_textColor_5() { return &___textColor_5; }
	inline void set_textColor_5(Color_t2555686324  value)
	{
		___textColor_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEGMENT_T1973990243_H
#ifndef SEGMENTEDCONTROL_T2965132545_H
#define SEGMENTEDCONTROL_T2965132545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SegmentedControl
struct  SegmentedControl_t2965132545  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Extensions.SegmentedControl::m_segments
	SelectableU5BU5D_t774044132* ___m_segments_4;
	// UnityEngine.UI.Graphic UnityEngine.UI.Extensions.SegmentedControl::m_separator
	Graphic_t1660335611 * ___m_separator_5;
	// System.Single UnityEngine.UI.Extensions.SegmentedControl::m_separatorWidth
	float ___m_separatorWidth_6;
	// System.Boolean UnityEngine.UI.Extensions.SegmentedControl::m_allowSwitchingOff
	bool ___m_allowSwitchingOff_7;
	// System.Int32 UnityEngine.UI.Extensions.SegmentedControl::m_selectedSegmentIndex
	int32_t ___m_selectedSegmentIndex_8;
	// UnityEngine.UI.Extensions.SegmentedControl/SegmentSelectedEvent UnityEngine.UI.Extensions.SegmentedControl::m_onValueChanged
	SegmentSelectedEvent_t878161132 * ___m_onValueChanged_9;
	// UnityEngine.UI.Selectable UnityEngine.UI.Extensions.SegmentedControl::selectedSegment
	Selectable_t3250028441 * ___selectedSegment_10;
	// UnityEngine.Color UnityEngine.UI.Extensions.SegmentedControl::selectedColor
	Color_t2555686324  ___selectedColor_11;

public:
	inline static int32_t get_offset_of_m_segments_4() { return static_cast<int32_t>(offsetof(SegmentedControl_t2965132545, ___m_segments_4)); }
	inline SelectableU5BU5D_t774044132* get_m_segments_4() const { return ___m_segments_4; }
	inline SelectableU5BU5D_t774044132** get_address_of_m_segments_4() { return &___m_segments_4; }
	inline void set_m_segments_4(SelectableU5BU5D_t774044132* value)
	{
		___m_segments_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_segments_4), value);
	}

	inline static int32_t get_offset_of_m_separator_5() { return static_cast<int32_t>(offsetof(SegmentedControl_t2965132545, ___m_separator_5)); }
	inline Graphic_t1660335611 * get_m_separator_5() const { return ___m_separator_5; }
	inline Graphic_t1660335611 ** get_address_of_m_separator_5() { return &___m_separator_5; }
	inline void set_m_separator_5(Graphic_t1660335611 * value)
	{
		___m_separator_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_separator_5), value);
	}

	inline static int32_t get_offset_of_m_separatorWidth_6() { return static_cast<int32_t>(offsetof(SegmentedControl_t2965132545, ___m_separatorWidth_6)); }
	inline float get_m_separatorWidth_6() const { return ___m_separatorWidth_6; }
	inline float* get_address_of_m_separatorWidth_6() { return &___m_separatorWidth_6; }
	inline void set_m_separatorWidth_6(float value)
	{
		___m_separatorWidth_6 = value;
	}

	inline static int32_t get_offset_of_m_allowSwitchingOff_7() { return static_cast<int32_t>(offsetof(SegmentedControl_t2965132545, ___m_allowSwitchingOff_7)); }
	inline bool get_m_allowSwitchingOff_7() const { return ___m_allowSwitchingOff_7; }
	inline bool* get_address_of_m_allowSwitchingOff_7() { return &___m_allowSwitchingOff_7; }
	inline void set_m_allowSwitchingOff_7(bool value)
	{
		___m_allowSwitchingOff_7 = value;
	}

	inline static int32_t get_offset_of_m_selectedSegmentIndex_8() { return static_cast<int32_t>(offsetof(SegmentedControl_t2965132545, ___m_selectedSegmentIndex_8)); }
	inline int32_t get_m_selectedSegmentIndex_8() const { return ___m_selectedSegmentIndex_8; }
	inline int32_t* get_address_of_m_selectedSegmentIndex_8() { return &___m_selectedSegmentIndex_8; }
	inline void set_m_selectedSegmentIndex_8(int32_t value)
	{
		___m_selectedSegmentIndex_8 = value;
	}

	inline static int32_t get_offset_of_m_onValueChanged_9() { return static_cast<int32_t>(offsetof(SegmentedControl_t2965132545, ___m_onValueChanged_9)); }
	inline SegmentSelectedEvent_t878161132 * get_m_onValueChanged_9() const { return ___m_onValueChanged_9; }
	inline SegmentSelectedEvent_t878161132 ** get_address_of_m_onValueChanged_9() { return &___m_onValueChanged_9; }
	inline void set_m_onValueChanged_9(SegmentSelectedEvent_t878161132 * value)
	{
		___m_onValueChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_onValueChanged_9), value);
	}

	inline static int32_t get_offset_of_selectedSegment_10() { return static_cast<int32_t>(offsetof(SegmentedControl_t2965132545, ___selectedSegment_10)); }
	inline Selectable_t3250028441 * get_selectedSegment_10() const { return ___selectedSegment_10; }
	inline Selectable_t3250028441 ** get_address_of_selectedSegment_10() { return &___selectedSegment_10; }
	inline void set_selectedSegment_10(Selectable_t3250028441 * value)
	{
		___selectedSegment_10 = value;
		Il2CppCodeGenWriteBarrier((&___selectedSegment_10), value);
	}

	inline static int32_t get_offset_of_selectedColor_11() { return static_cast<int32_t>(offsetof(SegmentedControl_t2965132545, ___selectedColor_11)); }
	inline Color_t2555686324  get_selectedColor_11() const { return ___selectedColor_11; }
	inline Color_t2555686324 * get_address_of_selectedColor_11() { return &___selectedColor_11; }
	inline void set_selectedColor_11(Color_t2555686324  value)
	{
		___selectedColor_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEGMENTEDCONTROL_T2965132545_H
#ifndef STEPPER_T2492277228_H
#define STEPPER_T2492277228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Stepper
struct  Stepper_t2492277228  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Extensions.Stepper::_sides
	SelectableU5BU5D_t774044132* ____sides_4;
	// System.Int32 UnityEngine.UI.Extensions.Stepper::_value
	int32_t ____value_5;
	// System.Int32 UnityEngine.UI.Extensions.Stepper::_minimum
	int32_t ____minimum_6;
	// System.Int32 UnityEngine.UI.Extensions.Stepper::_maximum
	int32_t ____maximum_7;
	// System.Int32 UnityEngine.UI.Extensions.Stepper::_step
	int32_t ____step_8;
	// System.Boolean UnityEngine.UI.Extensions.Stepper::_wrap
	bool ____wrap_9;
	// UnityEngine.UI.Graphic UnityEngine.UI.Extensions.Stepper::_separator
	Graphic_t1660335611 * ____separator_10;
	// System.Single UnityEngine.UI.Extensions.Stepper::_separatorWidth
	float ____separatorWidth_11;
	// UnityEngine.UI.Extensions.Stepper/StepperValueChangedEvent UnityEngine.UI.Extensions.Stepper::_onValueChanged
	StepperValueChangedEvent_t1994331895 * ____onValueChanged_12;

public:
	inline static int32_t get_offset_of__sides_4() { return static_cast<int32_t>(offsetof(Stepper_t2492277228, ____sides_4)); }
	inline SelectableU5BU5D_t774044132* get__sides_4() const { return ____sides_4; }
	inline SelectableU5BU5D_t774044132** get_address_of__sides_4() { return &____sides_4; }
	inline void set__sides_4(SelectableU5BU5D_t774044132* value)
	{
		____sides_4 = value;
		Il2CppCodeGenWriteBarrier((&____sides_4), value);
	}

	inline static int32_t get_offset_of__value_5() { return static_cast<int32_t>(offsetof(Stepper_t2492277228, ____value_5)); }
	inline int32_t get__value_5() const { return ____value_5; }
	inline int32_t* get_address_of__value_5() { return &____value_5; }
	inline void set__value_5(int32_t value)
	{
		____value_5 = value;
	}

	inline static int32_t get_offset_of__minimum_6() { return static_cast<int32_t>(offsetof(Stepper_t2492277228, ____minimum_6)); }
	inline int32_t get__minimum_6() const { return ____minimum_6; }
	inline int32_t* get_address_of__minimum_6() { return &____minimum_6; }
	inline void set__minimum_6(int32_t value)
	{
		____minimum_6 = value;
	}

	inline static int32_t get_offset_of__maximum_7() { return static_cast<int32_t>(offsetof(Stepper_t2492277228, ____maximum_7)); }
	inline int32_t get__maximum_7() const { return ____maximum_7; }
	inline int32_t* get_address_of__maximum_7() { return &____maximum_7; }
	inline void set__maximum_7(int32_t value)
	{
		____maximum_7 = value;
	}

	inline static int32_t get_offset_of__step_8() { return static_cast<int32_t>(offsetof(Stepper_t2492277228, ____step_8)); }
	inline int32_t get__step_8() const { return ____step_8; }
	inline int32_t* get_address_of__step_8() { return &____step_8; }
	inline void set__step_8(int32_t value)
	{
		____step_8 = value;
	}

	inline static int32_t get_offset_of__wrap_9() { return static_cast<int32_t>(offsetof(Stepper_t2492277228, ____wrap_9)); }
	inline bool get__wrap_9() const { return ____wrap_9; }
	inline bool* get_address_of__wrap_9() { return &____wrap_9; }
	inline void set__wrap_9(bool value)
	{
		____wrap_9 = value;
	}

	inline static int32_t get_offset_of__separator_10() { return static_cast<int32_t>(offsetof(Stepper_t2492277228, ____separator_10)); }
	inline Graphic_t1660335611 * get__separator_10() const { return ____separator_10; }
	inline Graphic_t1660335611 ** get_address_of__separator_10() { return &____separator_10; }
	inline void set__separator_10(Graphic_t1660335611 * value)
	{
		____separator_10 = value;
		Il2CppCodeGenWriteBarrier((&____separator_10), value);
	}

	inline static int32_t get_offset_of__separatorWidth_11() { return static_cast<int32_t>(offsetof(Stepper_t2492277228, ____separatorWidth_11)); }
	inline float get__separatorWidth_11() const { return ____separatorWidth_11; }
	inline float* get_address_of__separatorWidth_11() { return &____separatorWidth_11; }
	inline void set__separatorWidth_11(float value)
	{
		____separatorWidth_11 = value;
	}

	inline static int32_t get_offset_of__onValueChanged_12() { return static_cast<int32_t>(offsetof(Stepper_t2492277228, ____onValueChanged_12)); }
	inline StepperValueChangedEvent_t1994331895 * get__onValueChanged_12() const { return ____onValueChanged_12; }
	inline StepperValueChangedEvent_t1994331895 ** get_address_of__onValueChanged_12() { return &____onValueChanged_12; }
	inline void set__onValueChanged_12(StepperValueChangedEvent_t1994331895 * value)
	{
		____onValueChanged_12 = value;
		Il2CppCodeGenWriteBarrier((&____onValueChanged_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEPPER_T2492277228_H
#ifndef STEPPERSIDE_T4217246429_H
#define STEPPERSIDE_T4217246429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.StepperSide
struct  StepperSide_t4217246429  : public UIBehaviour_t3495933518
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEPPERSIDE_T4217246429_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_6)); }
	inline Material_t340375123 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t340375123 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t340375123 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_7)); }
	inline Color_t2555686324  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t2555686324 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t2555686324  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_9)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_11)); }
	inline Canvas_t3310196443 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t3310196443 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t340375123 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t340375123 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t3648964284 * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t3648964284 * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef LAYOUTGROUP_T2436138090_H
#define LAYOUTGROUP_T2436138090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t2436138090  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_t1369453676 * ___m_Padding_4;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_5;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t3704657025 * ___m_Rect_6;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_t2156229523  ___m_TotalMinSize_8;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_t2156229523  ___m_TotalPreferredSize_9;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_t2156229523  ___m_TotalFlexibleSize_10;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t881764471 * ___m_RectChildren_11;

public:
	inline static int32_t get_offset_of_m_Padding_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Padding_4)); }
	inline RectOffset_t1369453676 * get_m_Padding_4() const { return ___m_Padding_4; }
	inline RectOffset_t1369453676 ** get_address_of_m_Padding_4() { return &___m_Padding_4; }
	inline void set_m_Padding_4(RectOffset_t1369453676 * value)
	{
		___m_Padding_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_4), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_ChildAlignment_5)); }
	inline int32_t get_m_ChildAlignment_5() const { return ___m_ChildAlignment_5; }
	inline int32_t* get_address_of_m_ChildAlignment_5() { return &___m_ChildAlignment_5; }
	inline void set_m_ChildAlignment_5(int32_t value)
	{
		___m_ChildAlignment_5 = value;
	}

	inline static int32_t get_offset_of_m_Rect_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Rect_6)); }
	inline RectTransform_t3704657025 * get_m_Rect_6() const { return ___m_Rect_6; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_6() { return &___m_Rect_6; }
	inline void set_m_Rect_6(RectTransform_t3704657025 * value)
	{
		___m_Rect_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_6), value);
	}

	inline static int32_t get_offset_of_m_Tracker_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Tracker_7)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_7() const { return ___m_Tracker_7; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_7() { return &___m_Tracker_7; }
	inline void set_m_Tracker_7(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalMinSize_8)); }
	inline Vector2_t2156229523  get_m_TotalMinSize_8() const { return ___m_TotalMinSize_8; }
	inline Vector2_t2156229523 * get_address_of_m_TotalMinSize_8() { return &___m_TotalMinSize_8; }
	inline void set_m_TotalMinSize_8(Vector2_t2156229523  value)
	{
		___m_TotalMinSize_8 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalPreferredSize_9)); }
	inline Vector2_t2156229523  get_m_TotalPreferredSize_9() const { return ___m_TotalPreferredSize_9; }
	inline Vector2_t2156229523 * get_address_of_m_TotalPreferredSize_9() { return &___m_TotalPreferredSize_9; }
	inline void set_m_TotalPreferredSize_9(Vector2_t2156229523  value)
	{
		___m_TotalPreferredSize_9 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_10() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalFlexibleSize_10)); }
	inline Vector2_t2156229523  get_m_TotalFlexibleSize_10() const { return ___m_TotalFlexibleSize_10; }
	inline Vector2_t2156229523 * get_address_of_m_TotalFlexibleSize_10() { return &___m_TotalFlexibleSize_10; }
	inline void set_m_TotalFlexibleSize_10(Vector2_t2156229523  value)
	{
		___m_TotalFlexibleSize_10 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_11() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_RectChildren_11)); }
	inline List_1_t881764471 * get_m_RectChildren_11() const { return ___m_RectChildren_11; }
	inline List_1_t881764471 ** get_address_of_m_RectChildren_11() { return &___m_RectChildren_11; }
	inline void set_m_RectChildren_11(List_1_t881764471 * value)
	{
		___m_RectChildren_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T2436138090_H
#ifndef SCROLLRECT_T4137855814_H
#define SCROLLRECT_T4137855814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect
struct  ScrollRect_t4137855814  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Content
	RectTransform_t3704657025 * ___m_Content_4;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Horizontal
	bool ___m_Horizontal_5;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Vertical
	bool ___m_Vertical_6;
	// UnityEngine.UI.ScrollRect/MovementType UnityEngine.UI.ScrollRect::m_MovementType
	int32_t ___m_MovementType_7;
	// System.Single UnityEngine.UI.ScrollRect::m_Elasticity
	float ___m_Elasticity_8;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Inertia
	bool ___m_Inertia_9;
	// System.Single UnityEngine.UI.ScrollRect::m_DecelerationRate
	float ___m_DecelerationRate_10;
	// System.Single UnityEngine.UI.ScrollRect::m_ScrollSensitivity
	float ___m_ScrollSensitivity_11;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Viewport
	RectTransform_t3704657025 * ___m_Viewport_12;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_HorizontalScrollbar
	Scrollbar_t1494447233 * ___m_HorizontalScrollbar_13;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_VerticalScrollbar
	Scrollbar_t1494447233 * ___m_VerticalScrollbar_14;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_HorizontalScrollbarVisibility
	int32_t ___m_HorizontalScrollbarVisibility_15;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_VerticalScrollbarVisibility
	int32_t ___m_VerticalScrollbarVisibility_16;
	// System.Single UnityEngine.UI.ScrollRect::m_HorizontalScrollbarSpacing
	float ___m_HorizontalScrollbarSpacing_17;
	// System.Single UnityEngine.UI.ScrollRect::m_VerticalScrollbarSpacing
	float ___m_VerticalScrollbarSpacing_18;
	// UnityEngine.UI.ScrollRect/ScrollRectEvent UnityEngine.UI.ScrollRect::m_OnValueChanged
	ScrollRectEvent_t343079324 * ___m_OnValueChanged_19;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PointerStartLocalCursor
	Vector2_t2156229523  ___m_PointerStartLocalCursor_20;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_ContentStartPosition
	Vector2_t2156229523  ___m_ContentStartPosition_21;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_ViewRect
	RectTransform_t3704657025 * ___m_ViewRect_22;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ContentBounds
	Bounds_t2266837910  ___m_ContentBounds_23;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ViewBounds
	Bounds_t2266837910  ___m_ViewBounds_24;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_Velocity
	Vector2_t2156229523  ___m_Velocity_25;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Dragging
	bool ___m_Dragging_26;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PrevPosition
	Vector2_t2156229523  ___m_PrevPosition_27;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevContentBounds
	Bounds_t2266837910  ___m_PrevContentBounds_28;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevViewBounds
	Bounds_t2266837910  ___m_PrevViewBounds_29;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HasRebuiltLayout
	bool ___m_HasRebuiltLayout_30;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HSliderExpand
	bool ___m_HSliderExpand_31;
	// System.Boolean UnityEngine.UI.ScrollRect::m_VSliderExpand
	bool ___m_VSliderExpand_32;
	// System.Single UnityEngine.UI.ScrollRect::m_HSliderHeight
	float ___m_HSliderHeight_33;
	// System.Single UnityEngine.UI.ScrollRect::m_VSliderWidth
	float ___m_VSliderWidth_34;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Rect
	RectTransform_t3704657025 * ___m_Rect_35;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_HorizontalScrollbarRect
	RectTransform_t3704657025 * ___m_HorizontalScrollbarRect_36;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_VerticalScrollbarRect
	RectTransform_t3704657025 * ___m_VerticalScrollbarRect_37;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ScrollRect::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_38;
	// UnityEngine.Vector3[] UnityEngine.UI.ScrollRect::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_39;

public:
	inline static int32_t get_offset_of_m_Content_4() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Content_4)); }
	inline RectTransform_t3704657025 * get_m_Content_4() const { return ___m_Content_4; }
	inline RectTransform_t3704657025 ** get_address_of_m_Content_4() { return &___m_Content_4; }
	inline void set_m_Content_4(RectTransform_t3704657025 * value)
	{
		___m_Content_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_4), value);
	}

	inline static int32_t get_offset_of_m_Horizontal_5() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Horizontal_5)); }
	inline bool get_m_Horizontal_5() const { return ___m_Horizontal_5; }
	inline bool* get_address_of_m_Horizontal_5() { return &___m_Horizontal_5; }
	inline void set_m_Horizontal_5(bool value)
	{
		___m_Horizontal_5 = value;
	}

	inline static int32_t get_offset_of_m_Vertical_6() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Vertical_6)); }
	inline bool get_m_Vertical_6() const { return ___m_Vertical_6; }
	inline bool* get_address_of_m_Vertical_6() { return &___m_Vertical_6; }
	inline void set_m_Vertical_6(bool value)
	{
		___m_Vertical_6 = value;
	}

	inline static int32_t get_offset_of_m_MovementType_7() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_MovementType_7)); }
	inline int32_t get_m_MovementType_7() const { return ___m_MovementType_7; }
	inline int32_t* get_address_of_m_MovementType_7() { return &___m_MovementType_7; }
	inline void set_m_MovementType_7(int32_t value)
	{
		___m_MovementType_7 = value;
	}

	inline static int32_t get_offset_of_m_Elasticity_8() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Elasticity_8)); }
	inline float get_m_Elasticity_8() const { return ___m_Elasticity_8; }
	inline float* get_address_of_m_Elasticity_8() { return &___m_Elasticity_8; }
	inline void set_m_Elasticity_8(float value)
	{
		___m_Elasticity_8 = value;
	}

	inline static int32_t get_offset_of_m_Inertia_9() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Inertia_9)); }
	inline bool get_m_Inertia_9() const { return ___m_Inertia_9; }
	inline bool* get_address_of_m_Inertia_9() { return &___m_Inertia_9; }
	inline void set_m_Inertia_9(bool value)
	{
		___m_Inertia_9 = value;
	}

	inline static int32_t get_offset_of_m_DecelerationRate_10() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_DecelerationRate_10)); }
	inline float get_m_DecelerationRate_10() const { return ___m_DecelerationRate_10; }
	inline float* get_address_of_m_DecelerationRate_10() { return &___m_DecelerationRate_10; }
	inline void set_m_DecelerationRate_10(float value)
	{
		___m_DecelerationRate_10 = value;
	}

	inline static int32_t get_offset_of_m_ScrollSensitivity_11() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ScrollSensitivity_11)); }
	inline float get_m_ScrollSensitivity_11() const { return ___m_ScrollSensitivity_11; }
	inline float* get_address_of_m_ScrollSensitivity_11() { return &___m_ScrollSensitivity_11; }
	inline void set_m_ScrollSensitivity_11(float value)
	{
		___m_ScrollSensitivity_11 = value;
	}

	inline static int32_t get_offset_of_m_Viewport_12() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Viewport_12)); }
	inline RectTransform_t3704657025 * get_m_Viewport_12() const { return ___m_Viewport_12; }
	inline RectTransform_t3704657025 ** get_address_of_m_Viewport_12() { return &___m_Viewport_12; }
	inline void set_m_Viewport_12(RectTransform_t3704657025 * value)
	{
		___m_Viewport_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Viewport_12), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbar_13() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HorizontalScrollbar_13)); }
	inline Scrollbar_t1494447233 * get_m_HorizontalScrollbar_13() const { return ___m_HorizontalScrollbar_13; }
	inline Scrollbar_t1494447233 ** get_address_of_m_HorizontalScrollbar_13() { return &___m_HorizontalScrollbar_13; }
	inline void set_m_HorizontalScrollbar_13(Scrollbar_t1494447233 * value)
	{
		___m_HorizontalScrollbar_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalScrollbar_13), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbar_14() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VerticalScrollbar_14)); }
	inline Scrollbar_t1494447233 * get_m_VerticalScrollbar_14() const { return ___m_VerticalScrollbar_14; }
	inline Scrollbar_t1494447233 ** get_address_of_m_VerticalScrollbar_14() { return &___m_VerticalScrollbar_14; }
	inline void set_m_VerticalScrollbar_14(Scrollbar_t1494447233 * value)
	{
		___m_VerticalScrollbar_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbar_14), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarVisibility_15() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HorizontalScrollbarVisibility_15)); }
	inline int32_t get_m_HorizontalScrollbarVisibility_15() const { return ___m_HorizontalScrollbarVisibility_15; }
	inline int32_t* get_address_of_m_HorizontalScrollbarVisibility_15() { return &___m_HorizontalScrollbarVisibility_15; }
	inline void set_m_HorizontalScrollbarVisibility_15(int32_t value)
	{
		___m_HorizontalScrollbarVisibility_15 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarVisibility_16() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VerticalScrollbarVisibility_16)); }
	inline int32_t get_m_VerticalScrollbarVisibility_16() const { return ___m_VerticalScrollbarVisibility_16; }
	inline int32_t* get_address_of_m_VerticalScrollbarVisibility_16() { return &___m_VerticalScrollbarVisibility_16; }
	inline void set_m_VerticalScrollbarVisibility_16(int32_t value)
	{
		___m_VerticalScrollbarVisibility_16 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarSpacing_17() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HorizontalScrollbarSpacing_17)); }
	inline float get_m_HorizontalScrollbarSpacing_17() const { return ___m_HorizontalScrollbarSpacing_17; }
	inline float* get_address_of_m_HorizontalScrollbarSpacing_17() { return &___m_HorizontalScrollbarSpacing_17; }
	inline void set_m_HorizontalScrollbarSpacing_17(float value)
	{
		___m_HorizontalScrollbarSpacing_17 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarSpacing_18() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VerticalScrollbarSpacing_18)); }
	inline float get_m_VerticalScrollbarSpacing_18() const { return ___m_VerticalScrollbarSpacing_18; }
	inline float* get_address_of_m_VerticalScrollbarSpacing_18() { return &___m_VerticalScrollbarSpacing_18; }
	inline void set_m_VerticalScrollbarSpacing_18(float value)
	{
		___m_VerticalScrollbarSpacing_18 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_19() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_OnValueChanged_19)); }
	inline ScrollRectEvent_t343079324 * get_m_OnValueChanged_19() const { return ___m_OnValueChanged_19; }
	inline ScrollRectEvent_t343079324 ** get_address_of_m_OnValueChanged_19() { return &___m_OnValueChanged_19; }
	inline void set_m_OnValueChanged_19(ScrollRectEvent_t343079324 * value)
	{
		___m_OnValueChanged_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_19), value);
	}

	inline static int32_t get_offset_of_m_PointerStartLocalCursor_20() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_PointerStartLocalCursor_20)); }
	inline Vector2_t2156229523  get_m_PointerStartLocalCursor_20() const { return ___m_PointerStartLocalCursor_20; }
	inline Vector2_t2156229523 * get_address_of_m_PointerStartLocalCursor_20() { return &___m_PointerStartLocalCursor_20; }
	inline void set_m_PointerStartLocalCursor_20(Vector2_t2156229523  value)
	{
		___m_PointerStartLocalCursor_20 = value;
	}

	inline static int32_t get_offset_of_m_ContentStartPosition_21() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ContentStartPosition_21)); }
	inline Vector2_t2156229523  get_m_ContentStartPosition_21() const { return ___m_ContentStartPosition_21; }
	inline Vector2_t2156229523 * get_address_of_m_ContentStartPosition_21() { return &___m_ContentStartPosition_21; }
	inline void set_m_ContentStartPosition_21(Vector2_t2156229523  value)
	{
		___m_ContentStartPosition_21 = value;
	}

	inline static int32_t get_offset_of_m_ViewRect_22() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ViewRect_22)); }
	inline RectTransform_t3704657025 * get_m_ViewRect_22() const { return ___m_ViewRect_22; }
	inline RectTransform_t3704657025 ** get_address_of_m_ViewRect_22() { return &___m_ViewRect_22; }
	inline void set_m_ViewRect_22(RectTransform_t3704657025 * value)
	{
		___m_ViewRect_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ViewRect_22), value);
	}

	inline static int32_t get_offset_of_m_ContentBounds_23() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ContentBounds_23)); }
	inline Bounds_t2266837910  get_m_ContentBounds_23() const { return ___m_ContentBounds_23; }
	inline Bounds_t2266837910 * get_address_of_m_ContentBounds_23() { return &___m_ContentBounds_23; }
	inline void set_m_ContentBounds_23(Bounds_t2266837910  value)
	{
		___m_ContentBounds_23 = value;
	}

	inline static int32_t get_offset_of_m_ViewBounds_24() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ViewBounds_24)); }
	inline Bounds_t2266837910  get_m_ViewBounds_24() const { return ___m_ViewBounds_24; }
	inline Bounds_t2266837910 * get_address_of_m_ViewBounds_24() { return &___m_ViewBounds_24; }
	inline void set_m_ViewBounds_24(Bounds_t2266837910  value)
	{
		___m_ViewBounds_24 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_25() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Velocity_25)); }
	inline Vector2_t2156229523  get_m_Velocity_25() const { return ___m_Velocity_25; }
	inline Vector2_t2156229523 * get_address_of_m_Velocity_25() { return &___m_Velocity_25; }
	inline void set_m_Velocity_25(Vector2_t2156229523  value)
	{
		___m_Velocity_25 = value;
	}

	inline static int32_t get_offset_of_m_Dragging_26() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Dragging_26)); }
	inline bool get_m_Dragging_26() const { return ___m_Dragging_26; }
	inline bool* get_address_of_m_Dragging_26() { return &___m_Dragging_26; }
	inline void set_m_Dragging_26(bool value)
	{
		___m_Dragging_26 = value;
	}

	inline static int32_t get_offset_of_m_PrevPosition_27() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_PrevPosition_27)); }
	inline Vector2_t2156229523  get_m_PrevPosition_27() const { return ___m_PrevPosition_27; }
	inline Vector2_t2156229523 * get_address_of_m_PrevPosition_27() { return &___m_PrevPosition_27; }
	inline void set_m_PrevPosition_27(Vector2_t2156229523  value)
	{
		___m_PrevPosition_27 = value;
	}

	inline static int32_t get_offset_of_m_PrevContentBounds_28() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_PrevContentBounds_28)); }
	inline Bounds_t2266837910  get_m_PrevContentBounds_28() const { return ___m_PrevContentBounds_28; }
	inline Bounds_t2266837910 * get_address_of_m_PrevContentBounds_28() { return &___m_PrevContentBounds_28; }
	inline void set_m_PrevContentBounds_28(Bounds_t2266837910  value)
	{
		___m_PrevContentBounds_28 = value;
	}

	inline static int32_t get_offset_of_m_PrevViewBounds_29() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_PrevViewBounds_29)); }
	inline Bounds_t2266837910  get_m_PrevViewBounds_29() const { return ___m_PrevViewBounds_29; }
	inline Bounds_t2266837910 * get_address_of_m_PrevViewBounds_29() { return &___m_PrevViewBounds_29; }
	inline void set_m_PrevViewBounds_29(Bounds_t2266837910  value)
	{
		___m_PrevViewBounds_29 = value;
	}

	inline static int32_t get_offset_of_m_HasRebuiltLayout_30() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HasRebuiltLayout_30)); }
	inline bool get_m_HasRebuiltLayout_30() const { return ___m_HasRebuiltLayout_30; }
	inline bool* get_address_of_m_HasRebuiltLayout_30() { return &___m_HasRebuiltLayout_30; }
	inline void set_m_HasRebuiltLayout_30(bool value)
	{
		___m_HasRebuiltLayout_30 = value;
	}

	inline static int32_t get_offset_of_m_HSliderExpand_31() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HSliderExpand_31)); }
	inline bool get_m_HSliderExpand_31() const { return ___m_HSliderExpand_31; }
	inline bool* get_address_of_m_HSliderExpand_31() { return &___m_HSliderExpand_31; }
	inline void set_m_HSliderExpand_31(bool value)
	{
		___m_HSliderExpand_31 = value;
	}

	inline static int32_t get_offset_of_m_VSliderExpand_32() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VSliderExpand_32)); }
	inline bool get_m_VSliderExpand_32() const { return ___m_VSliderExpand_32; }
	inline bool* get_address_of_m_VSliderExpand_32() { return &___m_VSliderExpand_32; }
	inline void set_m_VSliderExpand_32(bool value)
	{
		___m_VSliderExpand_32 = value;
	}

	inline static int32_t get_offset_of_m_HSliderHeight_33() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HSliderHeight_33)); }
	inline float get_m_HSliderHeight_33() const { return ___m_HSliderHeight_33; }
	inline float* get_address_of_m_HSliderHeight_33() { return &___m_HSliderHeight_33; }
	inline void set_m_HSliderHeight_33(float value)
	{
		___m_HSliderHeight_33 = value;
	}

	inline static int32_t get_offset_of_m_VSliderWidth_34() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VSliderWidth_34)); }
	inline float get_m_VSliderWidth_34() const { return ___m_VSliderWidth_34; }
	inline float* get_address_of_m_VSliderWidth_34() { return &___m_VSliderWidth_34; }
	inline void set_m_VSliderWidth_34(float value)
	{
		___m_VSliderWidth_34 = value;
	}

	inline static int32_t get_offset_of_m_Rect_35() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Rect_35)); }
	inline RectTransform_t3704657025 * get_m_Rect_35() const { return ___m_Rect_35; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_35() { return &___m_Rect_35; }
	inline void set_m_Rect_35(RectTransform_t3704657025 * value)
	{
		___m_Rect_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_35), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarRect_36() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HorizontalScrollbarRect_36)); }
	inline RectTransform_t3704657025 * get_m_HorizontalScrollbarRect_36() const { return ___m_HorizontalScrollbarRect_36; }
	inline RectTransform_t3704657025 ** get_address_of_m_HorizontalScrollbarRect_36() { return &___m_HorizontalScrollbarRect_36; }
	inline void set_m_HorizontalScrollbarRect_36(RectTransform_t3704657025 * value)
	{
		___m_HorizontalScrollbarRect_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalScrollbarRect_36), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarRect_37() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VerticalScrollbarRect_37)); }
	inline RectTransform_t3704657025 * get_m_VerticalScrollbarRect_37() const { return ___m_VerticalScrollbarRect_37; }
	inline RectTransform_t3704657025 ** get_address_of_m_VerticalScrollbarRect_37() { return &___m_VerticalScrollbarRect_37; }
	inline void set_m_VerticalScrollbarRect_37(RectTransform_t3704657025 * value)
	{
		___m_VerticalScrollbarRect_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbarRect_37), value);
	}

	inline static int32_t get_offset_of_m_Tracker_38() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Tracker_38)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_38() const { return ___m_Tracker_38; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_38() { return &___m_Tracker_38; }
	inline void set_m_Tracker_38(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_38 = value;
	}

	inline static int32_t get_offset_of_m_Corners_39() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Corners_39)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_39() const { return ___m_Corners_39; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_39() { return &___m_Corners_39; }
	inline void set_m_Corners_39(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLRECT_T4137855814_H
#ifndef GAMEPADINPUTMODULE_T3186657611_H
#define GAMEPADINPUTMODULE_T3186657611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.GamePadInputModule
struct  GamePadInputModule_t3186657611  : public BaseInputModule_t2019268878
{
public:
	// System.Single UnityEngine.EventSystems.GamePadInputModule::m_PrevActionTime
	float ___m_PrevActionTime_10;
	// UnityEngine.Vector2 UnityEngine.EventSystems.GamePadInputModule::m_LastMoveVector
	Vector2_t2156229523  ___m_LastMoveVector_11;
	// System.Int32 UnityEngine.EventSystems.GamePadInputModule::m_ConsecutiveMoveCount
	int32_t ___m_ConsecutiveMoveCount_12;
	// System.String UnityEngine.EventSystems.GamePadInputModule::m_HorizontalAxis
	String_t* ___m_HorizontalAxis_13;
	// System.String UnityEngine.EventSystems.GamePadInputModule::m_VerticalAxis
	String_t* ___m_VerticalAxis_14;
	// System.String UnityEngine.EventSystems.GamePadInputModule::m_SubmitButton
	String_t* ___m_SubmitButton_15;
	// System.String UnityEngine.EventSystems.GamePadInputModule::m_CancelButton
	String_t* ___m_CancelButton_16;
	// System.Single UnityEngine.EventSystems.GamePadInputModule::m_InputActionsPerSecond
	float ___m_InputActionsPerSecond_17;
	// System.Single UnityEngine.EventSystems.GamePadInputModule::m_RepeatDelay
	float ___m_RepeatDelay_18;

public:
	inline static int32_t get_offset_of_m_PrevActionTime_10() { return static_cast<int32_t>(offsetof(GamePadInputModule_t3186657611, ___m_PrevActionTime_10)); }
	inline float get_m_PrevActionTime_10() const { return ___m_PrevActionTime_10; }
	inline float* get_address_of_m_PrevActionTime_10() { return &___m_PrevActionTime_10; }
	inline void set_m_PrevActionTime_10(float value)
	{
		___m_PrevActionTime_10 = value;
	}

	inline static int32_t get_offset_of_m_LastMoveVector_11() { return static_cast<int32_t>(offsetof(GamePadInputModule_t3186657611, ___m_LastMoveVector_11)); }
	inline Vector2_t2156229523  get_m_LastMoveVector_11() const { return ___m_LastMoveVector_11; }
	inline Vector2_t2156229523 * get_address_of_m_LastMoveVector_11() { return &___m_LastMoveVector_11; }
	inline void set_m_LastMoveVector_11(Vector2_t2156229523  value)
	{
		___m_LastMoveVector_11 = value;
	}

	inline static int32_t get_offset_of_m_ConsecutiveMoveCount_12() { return static_cast<int32_t>(offsetof(GamePadInputModule_t3186657611, ___m_ConsecutiveMoveCount_12)); }
	inline int32_t get_m_ConsecutiveMoveCount_12() const { return ___m_ConsecutiveMoveCount_12; }
	inline int32_t* get_address_of_m_ConsecutiveMoveCount_12() { return &___m_ConsecutiveMoveCount_12; }
	inline void set_m_ConsecutiveMoveCount_12(int32_t value)
	{
		___m_ConsecutiveMoveCount_12 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalAxis_13() { return static_cast<int32_t>(offsetof(GamePadInputModule_t3186657611, ___m_HorizontalAxis_13)); }
	inline String_t* get_m_HorizontalAxis_13() const { return ___m_HorizontalAxis_13; }
	inline String_t** get_address_of_m_HorizontalAxis_13() { return &___m_HorizontalAxis_13; }
	inline void set_m_HorizontalAxis_13(String_t* value)
	{
		___m_HorizontalAxis_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalAxis_13), value);
	}

	inline static int32_t get_offset_of_m_VerticalAxis_14() { return static_cast<int32_t>(offsetof(GamePadInputModule_t3186657611, ___m_VerticalAxis_14)); }
	inline String_t* get_m_VerticalAxis_14() const { return ___m_VerticalAxis_14; }
	inline String_t** get_address_of_m_VerticalAxis_14() { return &___m_VerticalAxis_14; }
	inline void set_m_VerticalAxis_14(String_t* value)
	{
		___m_VerticalAxis_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalAxis_14), value);
	}

	inline static int32_t get_offset_of_m_SubmitButton_15() { return static_cast<int32_t>(offsetof(GamePadInputModule_t3186657611, ___m_SubmitButton_15)); }
	inline String_t* get_m_SubmitButton_15() const { return ___m_SubmitButton_15; }
	inline String_t** get_address_of_m_SubmitButton_15() { return &___m_SubmitButton_15; }
	inline void set_m_SubmitButton_15(String_t* value)
	{
		___m_SubmitButton_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_SubmitButton_15), value);
	}

	inline static int32_t get_offset_of_m_CancelButton_16() { return static_cast<int32_t>(offsetof(GamePadInputModule_t3186657611, ___m_CancelButton_16)); }
	inline String_t* get_m_CancelButton_16() const { return ___m_CancelButton_16; }
	inline String_t** get_address_of_m_CancelButton_16() { return &___m_CancelButton_16; }
	inline void set_m_CancelButton_16(String_t* value)
	{
		___m_CancelButton_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_CancelButton_16), value);
	}

	inline static int32_t get_offset_of_m_InputActionsPerSecond_17() { return static_cast<int32_t>(offsetof(GamePadInputModule_t3186657611, ___m_InputActionsPerSecond_17)); }
	inline float get_m_InputActionsPerSecond_17() const { return ___m_InputActionsPerSecond_17; }
	inline float* get_address_of_m_InputActionsPerSecond_17() { return &___m_InputActionsPerSecond_17; }
	inline void set_m_InputActionsPerSecond_17(float value)
	{
		___m_InputActionsPerSecond_17 = value;
	}

	inline static int32_t get_offset_of_m_RepeatDelay_18() { return static_cast<int32_t>(offsetof(GamePadInputModule_t3186657611, ___m_RepeatDelay_18)); }
	inline float get_m_RepeatDelay_18() const { return ___m_RepeatDelay_18; }
	inline float* get_address_of_m_RepeatDelay_18() { return &___m_RepeatDelay_18; }
	inline void set_m_RepeatDelay_18(float value)
	{
		___m_RepeatDelay_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEPADINPUTMODULE_T3186657611_H
#ifndef POINTERINPUTMODULE_T3453173740_H
#define POINTERINPUTMODULE_T3453173740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule
struct  PointerInputModule_t3453173740  : public BaseInputModule_t2019268878
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> UnityEngine.EventSystems.PointerInputModule::m_PointerData
	Dictionary_2_t2696614423 * ___m_PointerData_14;
	// UnityEngine.EventSystems.PointerInputModule/MouseState UnityEngine.EventSystems.PointerInputModule::m_MouseState
	MouseState_t384203932 * ___m_MouseState_15;

public:
	inline static int32_t get_offset_of_m_PointerData_14() { return static_cast<int32_t>(offsetof(PointerInputModule_t3453173740, ___m_PointerData_14)); }
	inline Dictionary_2_t2696614423 * get_m_PointerData_14() const { return ___m_PointerData_14; }
	inline Dictionary_2_t2696614423 ** get_address_of_m_PointerData_14() { return &___m_PointerData_14; }
	inline void set_m_PointerData_14(Dictionary_2_t2696614423 * value)
	{
		___m_PointerData_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerData_14), value);
	}

	inline static int32_t get_offset_of_m_MouseState_15() { return static_cast<int32_t>(offsetof(PointerInputModule_t3453173740, ___m_MouseState_15)); }
	inline MouseState_t384203932 * get_m_MouseState_15() const { return ___m_MouseState_15; }
	inline MouseState_t384203932 ** get_address_of_m_MouseState_15() { return &___m_MouseState_15; }
	inline void set_m_MouseState_15(MouseState_t384203932 * value)
	{
		___m_MouseState_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseState_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTERINPUTMODULE_T3453173740_H
#ifndef CUIGRAPHIC_T2936003233_H
#define CUIGRAPHIC_T2936003233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CUIGraphic
struct  CUIGraphic_t2936003233  : public BaseMeshEffect_t2440176439
{
public:
	// System.Boolean UnityEngine.UI.Extensions.CUIGraphic::isCurved
	bool ___isCurved_7;
	// System.Boolean UnityEngine.UI.Extensions.CUIGraphic::isLockWithRatio
	bool ___isLockWithRatio_8;
	// System.Single UnityEngine.UI.Extensions.CUIGraphic::resolution
	float ___resolution_9;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.CUIGraphic::rectTrans
	RectTransform_t3704657025 * ___rectTrans_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Extensions.CUIGraphic::uiGraphic
	Graphic_t1660335611 * ___uiGraphic_11;
	// UnityEngine.UI.Extensions.CUIGraphic UnityEngine.UI.Extensions.CUIGraphic::refCUIGraphic
	CUIGraphic_t2936003233 * ___refCUIGraphic_12;
	// UnityEngine.UI.Extensions.CUIBezierCurve[] UnityEngine.UI.Extensions.CUIGraphic::refCurves
	CUIBezierCurveU5BU5D_t3815322299* ___refCurves_13;
	// UnityEngine.UI.Extensions.Vector3_Array2D[] UnityEngine.UI.Extensions.CUIGraphic::refCurvesControlRatioPoints
	Vector3_Array2DU5BU5D_t2158155091* ___refCurvesControlRatioPoints_14;
	// System.Collections.Generic.List`1<UnityEngine.UIVertex> UnityEngine.UI.Extensions.CUIGraphic::reuse_quads
	List_1_t1234605051 * ___reuse_quads_15;

public:
	inline static int32_t get_offset_of_isCurved_7() { return static_cast<int32_t>(offsetof(CUIGraphic_t2936003233, ___isCurved_7)); }
	inline bool get_isCurved_7() const { return ___isCurved_7; }
	inline bool* get_address_of_isCurved_7() { return &___isCurved_7; }
	inline void set_isCurved_7(bool value)
	{
		___isCurved_7 = value;
	}

	inline static int32_t get_offset_of_isLockWithRatio_8() { return static_cast<int32_t>(offsetof(CUIGraphic_t2936003233, ___isLockWithRatio_8)); }
	inline bool get_isLockWithRatio_8() const { return ___isLockWithRatio_8; }
	inline bool* get_address_of_isLockWithRatio_8() { return &___isLockWithRatio_8; }
	inline void set_isLockWithRatio_8(bool value)
	{
		___isLockWithRatio_8 = value;
	}

	inline static int32_t get_offset_of_resolution_9() { return static_cast<int32_t>(offsetof(CUIGraphic_t2936003233, ___resolution_9)); }
	inline float get_resolution_9() const { return ___resolution_9; }
	inline float* get_address_of_resolution_9() { return &___resolution_9; }
	inline void set_resolution_9(float value)
	{
		___resolution_9 = value;
	}

	inline static int32_t get_offset_of_rectTrans_10() { return static_cast<int32_t>(offsetof(CUIGraphic_t2936003233, ___rectTrans_10)); }
	inline RectTransform_t3704657025 * get_rectTrans_10() const { return ___rectTrans_10; }
	inline RectTransform_t3704657025 ** get_address_of_rectTrans_10() { return &___rectTrans_10; }
	inline void set_rectTrans_10(RectTransform_t3704657025 * value)
	{
		___rectTrans_10 = value;
		Il2CppCodeGenWriteBarrier((&___rectTrans_10), value);
	}

	inline static int32_t get_offset_of_uiGraphic_11() { return static_cast<int32_t>(offsetof(CUIGraphic_t2936003233, ___uiGraphic_11)); }
	inline Graphic_t1660335611 * get_uiGraphic_11() const { return ___uiGraphic_11; }
	inline Graphic_t1660335611 ** get_address_of_uiGraphic_11() { return &___uiGraphic_11; }
	inline void set_uiGraphic_11(Graphic_t1660335611 * value)
	{
		___uiGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___uiGraphic_11), value);
	}

	inline static int32_t get_offset_of_refCUIGraphic_12() { return static_cast<int32_t>(offsetof(CUIGraphic_t2936003233, ___refCUIGraphic_12)); }
	inline CUIGraphic_t2936003233 * get_refCUIGraphic_12() const { return ___refCUIGraphic_12; }
	inline CUIGraphic_t2936003233 ** get_address_of_refCUIGraphic_12() { return &___refCUIGraphic_12; }
	inline void set_refCUIGraphic_12(CUIGraphic_t2936003233 * value)
	{
		___refCUIGraphic_12 = value;
		Il2CppCodeGenWriteBarrier((&___refCUIGraphic_12), value);
	}

	inline static int32_t get_offset_of_refCurves_13() { return static_cast<int32_t>(offsetof(CUIGraphic_t2936003233, ___refCurves_13)); }
	inline CUIBezierCurveU5BU5D_t3815322299* get_refCurves_13() const { return ___refCurves_13; }
	inline CUIBezierCurveU5BU5D_t3815322299** get_address_of_refCurves_13() { return &___refCurves_13; }
	inline void set_refCurves_13(CUIBezierCurveU5BU5D_t3815322299* value)
	{
		___refCurves_13 = value;
		Il2CppCodeGenWriteBarrier((&___refCurves_13), value);
	}

	inline static int32_t get_offset_of_refCurvesControlRatioPoints_14() { return static_cast<int32_t>(offsetof(CUIGraphic_t2936003233, ___refCurvesControlRatioPoints_14)); }
	inline Vector3_Array2DU5BU5D_t2158155091* get_refCurvesControlRatioPoints_14() const { return ___refCurvesControlRatioPoints_14; }
	inline Vector3_Array2DU5BU5D_t2158155091** get_address_of_refCurvesControlRatioPoints_14() { return &___refCurvesControlRatioPoints_14; }
	inline void set_refCurvesControlRatioPoints_14(Vector3_Array2DU5BU5D_t2158155091* value)
	{
		___refCurvesControlRatioPoints_14 = value;
		Il2CppCodeGenWriteBarrier((&___refCurvesControlRatioPoints_14), value);
	}

	inline static int32_t get_offset_of_reuse_quads_15() { return static_cast<int32_t>(offsetof(CUIGraphic_t2936003233, ___reuse_quads_15)); }
	inline List_1_t1234605051 * get_reuse_quads_15() const { return ___reuse_quads_15; }
	inline List_1_t1234605051 ** get_address_of_reuse_quads_15() { return &___reuse_quads_15; }
	inline void set_reuse_quads_15(List_1_t1234605051 * value)
	{
		___reuse_quads_15 = value;
		Il2CppCodeGenWriteBarrier((&___reuse_quads_15), value);
	}
};

struct CUIGraphic_t2936003233_StaticFields
{
public:
	// System.Int32 UnityEngine.UI.Extensions.CUIGraphic::bottomCurveIdx
	int32_t ___bottomCurveIdx_5;
	// System.Int32 UnityEngine.UI.Extensions.CUIGraphic::topCurveIdx
	int32_t ___topCurveIdx_6;

public:
	inline static int32_t get_offset_of_bottomCurveIdx_5() { return static_cast<int32_t>(offsetof(CUIGraphic_t2936003233_StaticFields, ___bottomCurveIdx_5)); }
	inline int32_t get_bottomCurveIdx_5() const { return ___bottomCurveIdx_5; }
	inline int32_t* get_address_of_bottomCurveIdx_5() { return &___bottomCurveIdx_5; }
	inline void set_bottomCurveIdx_5(int32_t value)
	{
		___bottomCurveIdx_5 = value;
	}

	inline static int32_t get_offset_of_topCurveIdx_6() { return static_cast<int32_t>(offsetof(CUIGraphic_t2936003233_StaticFields, ___topCurveIdx_6)); }
	inline int32_t get_topCurveIdx_6() const { return ___topCurveIdx_6; }
	inline int32_t* get_address_of_topCurveIdx_6() { return &___topCurveIdx_6; }
	inline void set_topCurveIdx_6(int32_t value)
	{
		___topCurveIdx_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUIGRAPHIC_T2936003233_H
#ifndef CURVEDLAYOUT_T3964158924_H
#define CURVEDLAYOUT_T3964158924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CurvedLayout
struct  CurvedLayout_t3964158924  : public LayoutGroup_t2436138090
{
public:
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.CurvedLayout::CurveOffset
	Vector3_t3722313464  ___CurveOffset_12;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.CurvedLayout::itemAxis
	Vector3_t3722313464  ___itemAxis_13;
	// System.Single UnityEngine.UI.Extensions.CurvedLayout::itemSize
	float ___itemSize_14;
	// System.Single UnityEngine.UI.Extensions.CurvedLayout::centerpoint
	float ___centerpoint_15;

public:
	inline static int32_t get_offset_of_CurveOffset_12() { return static_cast<int32_t>(offsetof(CurvedLayout_t3964158924, ___CurveOffset_12)); }
	inline Vector3_t3722313464  get_CurveOffset_12() const { return ___CurveOffset_12; }
	inline Vector3_t3722313464 * get_address_of_CurveOffset_12() { return &___CurveOffset_12; }
	inline void set_CurveOffset_12(Vector3_t3722313464  value)
	{
		___CurveOffset_12 = value;
	}

	inline static int32_t get_offset_of_itemAxis_13() { return static_cast<int32_t>(offsetof(CurvedLayout_t3964158924, ___itemAxis_13)); }
	inline Vector3_t3722313464  get_itemAxis_13() const { return ___itemAxis_13; }
	inline Vector3_t3722313464 * get_address_of_itemAxis_13() { return &___itemAxis_13; }
	inline void set_itemAxis_13(Vector3_t3722313464  value)
	{
		___itemAxis_13 = value;
	}

	inline static int32_t get_offset_of_itemSize_14() { return static_cast<int32_t>(offsetof(CurvedLayout_t3964158924, ___itemSize_14)); }
	inline float get_itemSize_14() const { return ___itemSize_14; }
	inline float* get_address_of_itemSize_14() { return &___itemSize_14; }
	inline void set_itemSize_14(float value)
	{
		___itemSize_14 = value;
	}

	inline static int32_t get_offset_of_centerpoint_15() { return static_cast<int32_t>(offsetof(CurvedLayout_t3964158924, ___centerpoint_15)); }
	inline float get_centerpoint_15() const { return ___centerpoint_15; }
	inline float* get_address_of_centerpoint_15() { return &___centerpoint_15; }
	inline void set_centerpoint_15(float value)
	{
		___centerpoint_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVEDLAYOUT_T3964158924_H
#ifndef CURVEDTEXT_T1522163716_H
#define CURVEDTEXT_T1522163716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CurvedText
struct  CurvedText_t1522163716  : public BaseMeshEffect_t2440176439
{
public:
	// UnityEngine.AnimationCurve UnityEngine.UI.Extensions.CurvedText::_curveForText
	AnimationCurve_t3046754366 * ____curveForText_5;
	// System.Single UnityEngine.UI.Extensions.CurvedText::_curveMultiplier
	float ____curveMultiplier_6;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.CurvedText::rectTrans
	RectTransform_t3704657025 * ___rectTrans_7;

public:
	inline static int32_t get_offset_of__curveForText_5() { return static_cast<int32_t>(offsetof(CurvedText_t1522163716, ____curveForText_5)); }
	inline AnimationCurve_t3046754366 * get__curveForText_5() const { return ____curveForText_5; }
	inline AnimationCurve_t3046754366 ** get_address_of__curveForText_5() { return &____curveForText_5; }
	inline void set__curveForText_5(AnimationCurve_t3046754366 * value)
	{
		____curveForText_5 = value;
		Il2CppCodeGenWriteBarrier((&____curveForText_5), value);
	}

	inline static int32_t get_offset_of__curveMultiplier_6() { return static_cast<int32_t>(offsetof(CurvedText_t1522163716, ____curveMultiplier_6)); }
	inline float get__curveMultiplier_6() const { return ____curveMultiplier_6; }
	inline float* get_address_of__curveMultiplier_6() { return &____curveMultiplier_6; }
	inline void set__curveMultiplier_6(float value)
	{
		____curveMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_rectTrans_7() { return static_cast<int32_t>(offsetof(CurvedText_t1522163716, ___rectTrans_7)); }
	inline RectTransform_t3704657025 * get_rectTrans_7() const { return ___rectTrans_7; }
	inline RectTransform_t3704657025 ** get_address_of_rectTrans_7() { return &___rectTrans_7; }
	inline void set_rectTrans_7(RectTransform_t3704657025 * value)
	{
		___rectTrans_7 = value;
		Il2CppCodeGenWriteBarrier((&___rectTrans_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVEDTEXT_T1522163716_H
#ifndef CYLINDERTEXT_T364731485_H
#define CYLINDERTEXT_T364731485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CylinderText
struct  CylinderText_t364731485  : public BaseMeshEffect_t2440176439
{
public:
	// System.Single UnityEngine.UI.Extensions.CylinderText::radius
	float ___radius_5;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.CylinderText::rectTrans
	RectTransform_t3704657025 * ___rectTrans_6;

public:
	inline static int32_t get_offset_of_radius_5() { return static_cast<int32_t>(offsetof(CylinderText_t364731485, ___radius_5)); }
	inline float get_radius_5() const { return ___radius_5; }
	inline float* get_address_of_radius_5() { return &___radius_5; }
	inline void set_radius_5(float value)
	{
		___radius_5 = value;
	}

	inline static int32_t get_offset_of_rectTrans_6() { return static_cast<int32_t>(offsetof(CylinderText_t364731485, ___rectTrans_6)); }
	inline RectTransform_t3704657025 * get_rectTrans_6() const { return ___rectTrans_6; }
	inline RectTransform_t3704657025 ** get_address_of_rectTrans_6() { return &___rectTrans_6; }
	inline void set_rectTrans_6(RectTransform_t3704657025 * value)
	{
		___rectTrans_6 = value;
		Il2CppCodeGenWriteBarrier((&___rectTrans_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYLINDERTEXT_T364731485_H
#ifndef GRADIENT_T2828844715_H
#define GRADIENT_T2828844715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Gradient
struct  Gradient_t2828844715  : public BaseMeshEffect_t2440176439
{
public:
	// UnityEngine.UI.Extensions.GradientMode UnityEngine.UI.Extensions.Gradient::_gradientMode
	int32_t ____gradientMode_5;
	// UnityEngine.UI.Extensions.GradientDir UnityEngine.UI.Extensions.Gradient::_gradientDir
	int32_t ____gradientDir_6;
	// System.Boolean UnityEngine.UI.Extensions.Gradient::_overwriteAllColor
	bool ____overwriteAllColor_7;
	// UnityEngine.Color UnityEngine.UI.Extensions.Gradient::_vertex1
	Color_t2555686324  ____vertex1_8;
	// UnityEngine.Color UnityEngine.UI.Extensions.Gradient::_vertex2
	Color_t2555686324  ____vertex2_9;
	// UnityEngine.UI.Graphic UnityEngine.UI.Extensions.Gradient::targetGraphic
	Graphic_t1660335611 * ___targetGraphic_10;

public:
	inline static int32_t get_offset_of__gradientMode_5() { return static_cast<int32_t>(offsetof(Gradient_t2828844715, ____gradientMode_5)); }
	inline int32_t get__gradientMode_5() const { return ____gradientMode_5; }
	inline int32_t* get_address_of__gradientMode_5() { return &____gradientMode_5; }
	inline void set__gradientMode_5(int32_t value)
	{
		____gradientMode_5 = value;
	}

	inline static int32_t get_offset_of__gradientDir_6() { return static_cast<int32_t>(offsetof(Gradient_t2828844715, ____gradientDir_6)); }
	inline int32_t get__gradientDir_6() const { return ____gradientDir_6; }
	inline int32_t* get_address_of__gradientDir_6() { return &____gradientDir_6; }
	inline void set__gradientDir_6(int32_t value)
	{
		____gradientDir_6 = value;
	}

	inline static int32_t get_offset_of__overwriteAllColor_7() { return static_cast<int32_t>(offsetof(Gradient_t2828844715, ____overwriteAllColor_7)); }
	inline bool get__overwriteAllColor_7() const { return ____overwriteAllColor_7; }
	inline bool* get_address_of__overwriteAllColor_7() { return &____overwriteAllColor_7; }
	inline void set__overwriteAllColor_7(bool value)
	{
		____overwriteAllColor_7 = value;
	}

	inline static int32_t get_offset_of__vertex1_8() { return static_cast<int32_t>(offsetof(Gradient_t2828844715, ____vertex1_8)); }
	inline Color_t2555686324  get__vertex1_8() const { return ____vertex1_8; }
	inline Color_t2555686324 * get_address_of__vertex1_8() { return &____vertex1_8; }
	inline void set__vertex1_8(Color_t2555686324  value)
	{
		____vertex1_8 = value;
	}

	inline static int32_t get_offset_of__vertex2_9() { return static_cast<int32_t>(offsetof(Gradient_t2828844715, ____vertex2_9)); }
	inline Color_t2555686324  get__vertex2_9() const { return ____vertex2_9; }
	inline Color_t2555686324 * get_address_of__vertex2_9() { return &____vertex2_9; }
	inline void set__vertex2_9(Color_t2555686324  value)
	{
		____vertex2_9 = value;
	}

	inline static int32_t get_offset_of_targetGraphic_10() { return static_cast<int32_t>(offsetof(Gradient_t2828844715, ___targetGraphic_10)); }
	inline Graphic_t1660335611 * get_targetGraphic_10() const { return ___targetGraphic_10; }
	inline Graphic_t1660335611 ** get_address_of_targetGraphic_10() { return &___targetGraphic_10; }
	inline void set_targetGraphic_10(Graphic_t1660335611 * value)
	{
		___targetGraphic_10 = value;
		Il2CppCodeGenWriteBarrier((&___targetGraphic_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENT_T2828844715_H
#ifndef GRADIENT2_T3786049496_H
#define GRADIENT2_T3786049496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Gradient2
struct  Gradient2_t3786049496  : public BaseMeshEffect_t2440176439
{
public:
	// UnityEngine.UI.Extensions.Gradient2/Type UnityEngine.UI.Extensions.Gradient2::_gradientType
	int32_t ____gradientType_5;
	// UnityEngine.UI.Extensions.Gradient2/Blend UnityEngine.UI.Extensions.Gradient2::_blendMode
	int32_t ____blendMode_6;
	// System.Single UnityEngine.UI.Extensions.Gradient2::_offset
	float ____offset_7;
	// UnityEngine.Gradient UnityEngine.UI.Extensions.Gradient2::_effectGradient
	Gradient_t3067099924 * ____effectGradient_8;

public:
	inline static int32_t get_offset_of__gradientType_5() { return static_cast<int32_t>(offsetof(Gradient2_t3786049496, ____gradientType_5)); }
	inline int32_t get__gradientType_5() const { return ____gradientType_5; }
	inline int32_t* get_address_of__gradientType_5() { return &____gradientType_5; }
	inline void set__gradientType_5(int32_t value)
	{
		____gradientType_5 = value;
	}

	inline static int32_t get_offset_of__blendMode_6() { return static_cast<int32_t>(offsetof(Gradient2_t3786049496, ____blendMode_6)); }
	inline int32_t get__blendMode_6() const { return ____blendMode_6; }
	inline int32_t* get_address_of__blendMode_6() { return &____blendMode_6; }
	inline void set__blendMode_6(int32_t value)
	{
		____blendMode_6 = value;
	}

	inline static int32_t get_offset_of__offset_7() { return static_cast<int32_t>(offsetof(Gradient2_t3786049496, ____offset_7)); }
	inline float get__offset_7() const { return ____offset_7; }
	inline float* get_address_of__offset_7() { return &____offset_7; }
	inline void set__offset_7(float value)
	{
		____offset_7 = value;
	}

	inline static int32_t get_offset_of__effectGradient_8() { return static_cast<int32_t>(offsetof(Gradient2_t3786049496, ____effectGradient_8)); }
	inline Gradient_t3067099924 * get__effectGradient_8() const { return ____effectGradient_8; }
	inline Gradient_t3067099924 ** get_address_of__effectGradient_8() { return &____effectGradient_8; }
	inline void set__effectGradient_8(Gradient_t3067099924 * value)
	{
		____effectGradient_8 = value;
		Il2CppCodeGenWriteBarrier((&____effectGradient_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENT2_T3786049496_H
#ifndef LETTERSPACING_T1421332419_H
#define LETTERSPACING_T1421332419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.LetterSpacing
struct  LetterSpacing_t1421332419  : public BaseMeshEffect_t2440176439
{
public:
	// System.Single UnityEngine.UI.Extensions.LetterSpacing::m_spacing
	float ___m_spacing_5;

public:
	inline static int32_t get_offset_of_m_spacing_5() { return static_cast<int32_t>(offsetof(LetterSpacing_t1421332419, ___m_spacing_5)); }
	inline float get_m_spacing_5() const { return ___m_spacing_5; }
	inline float* get_address_of_m_spacing_5() { return &___m_spacing_5; }
	inline void set_m_spacing_5(float value)
	{
		___m_spacing_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LETTERSPACING_T1421332419_H
#ifndef MONOSPACING_T1166202091_H
#define MONOSPACING_T1166202091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.MonoSpacing
struct  MonoSpacing_t1166202091  : public BaseMeshEffect_t2440176439
{
public:
	// System.Single UnityEngine.UI.Extensions.MonoSpacing::m_spacing
	float ___m_spacing_5;
	// System.Single UnityEngine.UI.Extensions.MonoSpacing::HalfCharWidth
	float ___HalfCharWidth_6;
	// System.Boolean UnityEngine.UI.Extensions.MonoSpacing::UseHalfCharWidth
	bool ___UseHalfCharWidth_7;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.MonoSpacing::rectTransform
	RectTransform_t3704657025 * ___rectTransform_8;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.MonoSpacing::text
	Text_t1901882714 * ___text_9;

public:
	inline static int32_t get_offset_of_m_spacing_5() { return static_cast<int32_t>(offsetof(MonoSpacing_t1166202091, ___m_spacing_5)); }
	inline float get_m_spacing_5() const { return ___m_spacing_5; }
	inline float* get_address_of_m_spacing_5() { return &___m_spacing_5; }
	inline void set_m_spacing_5(float value)
	{
		___m_spacing_5 = value;
	}

	inline static int32_t get_offset_of_HalfCharWidth_6() { return static_cast<int32_t>(offsetof(MonoSpacing_t1166202091, ___HalfCharWidth_6)); }
	inline float get_HalfCharWidth_6() const { return ___HalfCharWidth_6; }
	inline float* get_address_of_HalfCharWidth_6() { return &___HalfCharWidth_6; }
	inline void set_HalfCharWidth_6(float value)
	{
		___HalfCharWidth_6 = value;
	}

	inline static int32_t get_offset_of_UseHalfCharWidth_7() { return static_cast<int32_t>(offsetof(MonoSpacing_t1166202091, ___UseHalfCharWidth_7)); }
	inline bool get_UseHalfCharWidth_7() const { return ___UseHalfCharWidth_7; }
	inline bool* get_address_of_UseHalfCharWidth_7() { return &___UseHalfCharWidth_7; }
	inline void set_UseHalfCharWidth_7(bool value)
	{
		___UseHalfCharWidth_7 = value;
	}

	inline static int32_t get_offset_of_rectTransform_8() { return static_cast<int32_t>(offsetof(MonoSpacing_t1166202091, ___rectTransform_8)); }
	inline RectTransform_t3704657025 * get_rectTransform_8() const { return ___rectTransform_8; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_8() { return &___rectTransform_8; }
	inline void set_rectTransform_8(RectTransform_t3704657025 * value)
	{
		___rectTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_8), value);
	}

	inline static int32_t get_offset_of_text_9() { return static_cast<int32_t>(offsetof(MonoSpacing_t1166202091, ___text_9)); }
	inline Text_t1901882714 * get_text_9() const { return ___text_9; }
	inline Text_t1901882714 ** get_address_of_text_9() { return &___text_9; }
	inline void set_text_9(Text_t1901882714 * value)
	{
		___text_9 = value;
		Il2CppCodeGenWriteBarrier((&___text_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOSPACING_T1166202091_H
#ifndef MULTITOUCHSCROLLRECT_T2699801454_H
#define MULTITOUCHSCROLLRECT_T2699801454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.MultiTouchScrollRect
struct  MultiTouchScrollRect_t2699801454  : public ScrollRect_t4137855814
{
public:
	// System.Int32 UnityEngine.UI.Extensions.MultiTouchScrollRect::pid
	int32_t ___pid_40;

public:
	inline static int32_t get_offset_of_pid_40() { return static_cast<int32_t>(offsetof(MultiTouchScrollRect_t2699801454, ___pid_40)); }
	inline int32_t get_pid_40() const { return ___pid_40; }
	inline int32_t* get_address_of_pid_40() { return &___pid_40; }
	inline void set_pid_40(int32_t value)
	{
		___pid_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTITOUCHSCROLLRECT_T2699801454_H
#ifndef NICEROUTLINE_T22425468_H
#define NICEROUTLINE_T22425468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.NicerOutline
struct  NicerOutline_t22425468  : public BaseMeshEffect_t2440176439
{
public:
	// UnityEngine.Color UnityEngine.UI.Extensions.NicerOutline::m_EffectColor
	Color_t2555686324  ___m_EffectColor_5;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.NicerOutline::m_EffectDistance
	Vector2_t2156229523  ___m_EffectDistance_6;
	// System.Boolean UnityEngine.UI.Extensions.NicerOutline::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_7;

public:
	inline static int32_t get_offset_of_m_EffectColor_5() { return static_cast<int32_t>(offsetof(NicerOutline_t22425468, ___m_EffectColor_5)); }
	inline Color_t2555686324  get_m_EffectColor_5() const { return ___m_EffectColor_5; }
	inline Color_t2555686324 * get_address_of_m_EffectColor_5() { return &___m_EffectColor_5; }
	inline void set_m_EffectColor_5(Color_t2555686324  value)
	{
		___m_EffectColor_5 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_6() { return static_cast<int32_t>(offsetof(NicerOutline_t22425468, ___m_EffectDistance_6)); }
	inline Vector2_t2156229523  get_m_EffectDistance_6() const { return ___m_EffectDistance_6; }
	inline Vector2_t2156229523 * get_address_of_m_EffectDistance_6() { return &___m_EffectDistance_6; }
	inline void set_m_EffectDistance_6(Vector2_t2156229523  value)
	{
		___m_EffectDistance_6 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_7() { return static_cast<int32_t>(offsetof(NicerOutline_t22425468, ___m_UseGraphicAlpha_7)); }
	inline bool get_m_UseGraphicAlpha_7() const { return ___m_UseGraphicAlpha_7; }
	inline bool* get_address_of_m_UseGraphicAlpha_7() { return &___m_UseGraphicAlpha_7; }
	inline void set_m_UseGraphicAlpha_7(bool value)
	{
		___m_UseGraphicAlpha_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NICEROUTLINE_T22425468_H
#ifndef UIFLIPPABLE_T826138922_H
#define UIFLIPPABLE_T826138922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIFlippable
struct  UIFlippable_t826138922  : public BaseMeshEffect_t2440176439
{
public:
	// System.Boolean UnityEngine.UI.Extensions.UIFlippable::m_Horizontal
	bool ___m_Horizontal_5;
	// System.Boolean UnityEngine.UI.Extensions.UIFlippable::m_Veritical
	bool ___m_Veritical_6;

public:
	inline static int32_t get_offset_of_m_Horizontal_5() { return static_cast<int32_t>(offsetof(UIFlippable_t826138922, ___m_Horizontal_5)); }
	inline bool get_m_Horizontal_5() const { return ___m_Horizontal_5; }
	inline bool* get_address_of_m_Horizontal_5() { return &___m_Horizontal_5; }
	inline void set_m_Horizontal_5(bool value)
	{
		___m_Horizontal_5 = value;
	}

	inline static int32_t get_offset_of_m_Veritical_6() { return static_cast<int32_t>(offsetof(UIFlippable_t826138922, ___m_Veritical_6)); }
	inline bool get_m_Veritical_6() const { return ___m_Veritical_6; }
	inline bool* get_address_of_m_Veritical_6() { return &___m_Veritical_6; }
	inline void set_m_Veritical_6(bool value)
	{
		___m_Veritical_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIFLIPPABLE_T826138922_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_22)); }
	inline Material_t340375123 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_t340375123 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_23)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_29)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef SHADOW_T773074319_H
#define SHADOW_T773074319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t773074319  : public BaseMeshEffect_t2440176439
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2555686324  ___m_EffectColor_5;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t2156229523  ___m_EffectDistance_6;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_7;

public:
	inline static int32_t get_offset_of_m_EffectColor_5() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectColor_5)); }
	inline Color_t2555686324  get_m_EffectColor_5() const { return ___m_EffectColor_5; }
	inline Color_t2555686324 * get_address_of_m_EffectColor_5() { return &___m_EffectColor_5; }
	inline void set_m_EffectColor_5(Color_t2555686324  value)
	{
		___m_EffectColor_5 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_6() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectDistance_6)); }
	inline Vector2_t2156229523  get_m_EffectDistance_6() const { return ___m_EffectDistance_6; }
	inline Vector2_t2156229523 * get_address_of_m_EffectDistance_6() { return &___m_EffectDistance_6; }
	inline void set_m_EffectDistance_6(Vector2_t2156229523  value)
	{
		___m_EffectDistance_6 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_7() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_UseGraphicAlpha_7)); }
	inline bool get_m_UseGraphicAlpha_7() const { return ___m_UseGraphicAlpha_7; }
	inline bool* get_address_of_m_UseGraphicAlpha_7() { return &___m_UseGraphicAlpha_7; }
	inline void set_m_UseGraphicAlpha_7(bool value)
	{
		___m_UseGraphicAlpha_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T773074319_H
#ifndef AIMERINPUTMODULE_T3344721356_H
#define AIMERINPUTMODULE_T3344721356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.Extensions.AimerInputModule
struct  AimerInputModule_t3344721356  : public PointerInputModule_t3453173740
{
public:
	// System.String UnityEngine.EventSystems.Extensions.AimerInputModule::activateAxis
	String_t* ___activateAxis_16;
	// UnityEngine.Vector2 UnityEngine.EventSystems.Extensions.AimerInputModule::aimerOffset
	Vector2_t2156229523  ___aimerOffset_17;

public:
	inline static int32_t get_offset_of_activateAxis_16() { return static_cast<int32_t>(offsetof(AimerInputModule_t3344721356, ___activateAxis_16)); }
	inline String_t* get_activateAxis_16() const { return ___activateAxis_16; }
	inline String_t** get_address_of_activateAxis_16() { return &___activateAxis_16; }
	inline void set_activateAxis_16(String_t* value)
	{
		___activateAxis_16 = value;
		Il2CppCodeGenWriteBarrier((&___activateAxis_16), value);
	}

	inline static int32_t get_offset_of_aimerOffset_17() { return static_cast<int32_t>(offsetof(AimerInputModule_t3344721356, ___aimerOffset_17)); }
	inline Vector2_t2156229523  get_aimerOffset_17() const { return ___aimerOffset_17; }
	inline Vector2_t2156229523 * get_address_of_aimerOffset_17() { return &___aimerOffset_17; }
	inline void set_aimerOffset_17(Vector2_t2156229523  value)
	{
		___aimerOffset_17 = value;
	}
};

struct AimerInputModule_t3344721356_StaticFields
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.Extensions.AimerInputModule::objectUnderAimer
	GameObject_t1113636619 * ___objectUnderAimer_18;

public:
	inline static int32_t get_offset_of_objectUnderAimer_18() { return static_cast<int32_t>(offsetof(AimerInputModule_t3344721356_StaticFields, ___objectUnderAimer_18)); }
	inline GameObject_t1113636619 * get_objectUnderAimer_18() const { return ___objectUnderAimer_18; }
	inline GameObject_t1113636619 ** get_address_of_objectUnderAimer_18() { return &___objectUnderAimer_18; }
	inline void set_objectUnderAimer_18(GameObject_t1113636619 * value)
	{
		___objectUnderAimer_18 = value;
		Il2CppCodeGenWriteBarrier((&___objectUnderAimer_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AIMERINPUTMODULE_T3344721356_H
#ifndef BESTFITOUTLINE_T4228912216_H
#define BESTFITOUTLINE_T4228912216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.BestFitOutline
struct  BestFitOutline_t4228912216  : public Shadow_t773074319
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BESTFITOUTLINE_T4228912216_H
#ifndef CUIIMAGE_T1061186866_H
#define CUIIMAGE_T1061186866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CUIImage
struct  CUIImage_t1061186866  : public CUIGraphic_t2936003233
{
public:
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.CUIImage::cornerPosRatio
	Vector2_t2156229523  ___cornerPosRatio_18;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.CUIImage::oriCornerPosRatio
	Vector2_t2156229523  ___oriCornerPosRatio_19;

public:
	inline static int32_t get_offset_of_cornerPosRatio_18() { return static_cast<int32_t>(offsetof(CUIImage_t1061186866, ___cornerPosRatio_18)); }
	inline Vector2_t2156229523  get_cornerPosRatio_18() const { return ___cornerPosRatio_18; }
	inline Vector2_t2156229523 * get_address_of_cornerPosRatio_18() { return &___cornerPosRatio_18; }
	inline void set_cornerPosRatio_18(Vector2_t2156229523  value)
	{
		___cornerPosRatio_18 = value;
	}

	inline static int32_t get_offset_of_oriCornerPosRatio_19() { return static_cast<int32_t>(offsetof(CUIImage_t1061186866, ___oriCornerPosRatio_19)); }
	inline Vector2_t2156229523  get_oriCornerPosRatio_19() const { return ___oriCornerPosRatio_19; }
	inline Vector2_t2156229523 * get_address_of_oriCornerPosRatio_19() { return &___oriCornerPosRatio_19; }
	inline void set_oriCornerPosRatio_19(Vector2_t2156229523  value)
	{
		___oriCornerPosRatio_19 = value;
	}
};

struct CUIImage_t1061186866_StaticFields
{
public:
	// System.Int32 UnityEngine.UI.Extensions.CUIImage::SlicedImageCornerRefVertexIdx
	int32_t ___SlicedImageCornerRefVertexIdx_16;
	// System.Int32 UnityEngine.UI.Extensions.CUIImage::FilledImageCornerRefVertexIdx
	int32_t ___FilledImageCornerRefVertexIdx_17;

public:
	inline static int32_t get_offset_of_SlicedImageCornerRefVertexIdx_16() { return static_cast<int32_t>(offsetof(CUIImage_t1061186866_StaticFields, ___SlicedImageCornerRefVertexIdx_16)); }
	inline int32_t get_SlicedImageCornerRefVertexIdx_16() const { return ___SlicedImageCornerRefVertexIdx_16; }
	inline int32_t* get_address_of_SlicedImageCornerRefVertexIdx_16() { return &___SlicedImageCornerRefVertexIdx_16; }
	inline void set_SlicedImageCornerRefVertexIdx_16(int32_t value)
	{
		___SlicedImageCornerRefVertexIdx_16 = value;
	}

	inline static int32_t get_offset_of_FilledImageCornerRefVertexIdx_17() { return static_cast<int32_t>(offsetof(CUIImage_t1061186866_StaticFields, ___FilledImageCornerRefVertexIdx_17)); }
	inline int32_t get_FilledImageCornerRefVertexIdx_17() const { return ___FilledImageCornerRefVertexIdx_17; }
	inline int32_t* get_address_of_FilledImageCornerRefVertexIdx_17() { return &___FilledImageCornerRefVertexIdx_17; }
	inline void set_FilledImageCornerRefVertexIdx_17(int32_t value)
	{
		___FilledImageCornerRefVertexIdx_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUIIMAGE_T1061186866_H
#ifndef CUITEXT_T1792872957_H
#define CUITEXT_T1792872957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.CUIText
struct  CUIText_t1792872957  : public CUIGraphic_t2936003233
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUITEXT_T1792872957_H
#ifndef SHINEEFFECT_T3679628888_H
#define SHINEEFFECT_T3679628888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ShineEffect
struct  ShineEffect_t3679628888  : public MaskableGraphic_t3839221559
{
public:
	// System.Single UnityEngine.UI.Extensions.ShineEffect::yoffset
	float ___yoffset_30;
	// System.Single UnityEngine.UI.Extensions.ShineEffect::width
	float ___width_31;

public:
	inline static int32_t get_offset_of_yoffset_30() { return static_cast<int32_t>(offsetof(ShineEffect_t3679628888, ___yoffset_30)); }
	inline float get_yoffset_30() const { return ___yoffset_30; }
	inline float* get_address_of_yoffset_30() { return &___yoffset_30; }
	inline void set_yoffset_30(float value)
	{
		___yoffset_30 = value;
	}

	inline static int32_t get_offset_of_width_31() { return static_cast<int32_t>(offsetof(ShineEffect_t3679628888, ___width_31)); }
	inline float get_width_31() const { return ___width_31; }
	inline float* get_address_of_width_31() { return &___width_31; }
	inline void set_width_31(float value)
	{
		___width_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHINEEFFECT_T3679628888_H
#ifndef UIPARTICLESYSTEM_T471890981_H
#define UIPARTICLESYSTEM_T471890981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIParticleSystem
struct  UIParticleSystem_t471890981  : public MaskableGraphic_t3839221559
{
public:
	// System.Boolean UnityEngine.UI.Extensions.UIParticleSystem::fixedTime
	bool ___fixedTime_30;
	// UnityEngine.Transform UnityEngine.UI.Extensions.UIParticleSystem::_transform
	Transform_t3600365921 * ____transform_31;
	// UnityEngine.ParticleSystem UnityEngine.UI.Extensions.UIParticleSystem::pSystem
	ParticleSystem_t1800779281 * ___pSystem_32;
	// UnityEngine.ParticleSystem/Particle[] UnityEngine.UI.Extensions.UIParticleSystem::particles
	ParticleU5BU5D_t3069227754* ___particles_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Extensions.UIParticleSystem::_quad
	UIVertexU5BU5D_t1981460040* ____quad_34;
	// UnityEngine.Vector4 UnityEngine.UI.Extensions.UIParticleSystem::imageUV
	Vector4_t3319028937  ___imageUV_35;
	// UnityEngine.ParticleSystem/TextureSheetAnimationModule UnityEngine.UI.Extensions.UIParticleSystem::textureSheetAnimation
	TextureSheetAnimationModule_t738696839  ___textureSheetAnimation_36;
	// System.Int32 UnityEngine.UI.Extensions.UIParticleSystem::textureSheetAnimationFrames
	int32_t ___textureSheetAnimationFrames_37;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.UIParticleSystem::textureSheetAnimationFrameSize
	Vector2_t2156229523  ___textureSheetAnimationFrameSize_38;
	// UnityEngine.ParticleSystemRenderer UnityEngine.UI.Extensions.UIParticleSystem::pRenderer
	ParticleSystemRenderer_t2065813411 * ___pRenderer_39;
	// UnityEngine.Material UnityEngine.UI.Extensions.UIParticleSystem::currentMaterial
	Material_t340375123 * ___currentMaterial_40;
	// UnityEngine.Texture UnityEngine.UI.Extensions.UIParticleSystem::currentTexture
	Texture_t3661962703 * ___currentTexture_41;
	// UnityEngine.ParticleSystem/MainModule UnityEngine.UI.Extensions.UIParticleSystem::mainModule
	MainModule_t2320046318  ___mainModule_42;

public:
	inline static int32_t get_offset_of_fixedTime_30() { return static_cast<int32_t>(offsetof(UIParticleSystem_t471890981, ___fixedTime_30)); }
	inline bool get_fixedTime_30() const { return ___fixedTime_30; }
	inline bool* get_address_of_fixedTime_30() { return &___fixedTime_30; }
	inline void set_fixedTime_30(bool value)
	{
		___fixedTime_30 = value;
	}

	inline static int32_t get_offset_of__transform_31() { return static_cast<int32_t>(offsetof(UIParticleSystem_t471890981, ____transform_31)); }
	inline Transform_t3600365921 * get__transform_31() const { return ____transform_31; }
	inline Transform_t3600365921 ** get_address_of__transform_31() { return &____transform_31; }
	inline void set__transform_31(Transform_t3600365921 * value)
	{
		____transform_31 = value;
		Il2CppCodeGenWriteBarrier((&____transform_31), value);
	}

	inline static int32_t get_offset_of_pSystem_32() { return static_cast<int32_t>(offsetof(UIParticleSystem_t471890981, ___pSystem_32)); }
	inline ParticleSystem_t1800779281 * get_pSystem_32() const { return ___pSystem_32; }
	inline ParticleSystem_t1800779281 ** get_address_of_pSystem_32() { return &___pSystem_32; }
	inline void set_pSystem_32(ParticleSystem_t1800779281 * value)
	{
		___pSystem_32 = value;
		Il2CppCodeGenWriteBarrier((&___pSystem_32), value);
	}

	inline static int32_t get_offset_of_particles_33() { return static_cast<int32_t>(offsetof(UIParticleSystem_t471890981, ___particles_33)); }
	inline ParticleU5BU5D_t3069227754* get_particles_33() const { return ___particles_33; }
	inline ParticleU5BU5D_t3069227754** get_address_of_particles_33() { return &___particles_33; }
	inline void set_particles_33(ParticleU5BU5D_t3069227754* value)
	{
		___particles_33 = value;
		Il2CppCodeGenWriteBarrier((&___particles_33), value);
	}

	inline static int32_t get_offset_of__quad_34() { return static_cast<int32_t>(offsetof(UIParticleSystem_t471890981, ____quad_34)); }
	inline UIVertexU5BU5D_t1981460040* get__quad_34() const { return ____quad_34; }
	inline UIVertexU5BU5D_t1981460040** get_address_of__quad_34() { return &____quad_34; }
	inline void set__quad_34(UIVertexU5BU5D_t1981460040* value)
	{
		____quad_34 = value;
		Il2CppCodeGenWriteBarrier((&____quad_34), value);
	}

	inline static int32_t get_offset_of_imageUV_35() { return static_cast<int32_t>(offsetof(UIParticleSystem_t471890981, ___imageUV_35)); }
	inline Vector4_t3319028937  get_imageUV_35() const { return ___imageUV_35; }
	inline Vector4_t3319028937 * get_address_of_imageUV_35() { return &___imageUV_35; }
	inline void set_imageUV_35(Vector4_t3319028937  value)
	{
		___imageUV_35 = value;
	}

	inline static int32_t get_offset_of_textureSheetAnimation_36() { return static_cast<int32_t>(offsetof(UIParticleSystem_t471890981, ___textureSheetAnimation_36)); }
	inline TextureSheetAnimationModule_t738696839  get_textureSheetAnimation_36() const { return ___textureSheetAnimation_36; }
	inline TextureSheetAnimationModule_t738696839 * get_address_of_textureSheetAnimation_36() { return &___textureSheetAnimation_36; }
	inline void set_textureSheetAnimation_36(TextureSheetAnimationModule_t738696839  value)
	{
		___textureSheetAnimation_36 = value;
	}

	inline static int32_t get_offset_of_textureSheetAnimationFrames_37() { return static_cast<int32_t>(offsetof(UIParticleSystem_t471890981, ___textureSheetAnimationFrames_37)); }
	inline int32_t get_textureSheetAnimationFrames_37() const { return ___textureSheetAnimationFrames_37; }
	inline int32_t* get_address_of_textureSheetAnimationFrames_37() { return &___textureSheetAnimationFrames_37; }
	inline void set_textureSheetAnimationFrames_37(int32_t value)
	{
		___textureSheetAnimationFrames_37 = value;
	}

	inline static int32_t get_offset_of_textureSheetAnimationFrameSize_38() { return static_cast<int32_t>(offsetof(UIParticleSystem_t471890981, ___textureSheetAnimationFrameSize_38)); }
	inline Vector2_t2156229523  get_textureSheetAnimationFrameSize_38() const { return ___textureSheetAnimationFrameSize_38; }
	inline Vector2_t2156229523 * get_address_of_textureSheetAnimationFrameSize_38() { return &___textureSheetAnimationFrameSize_38; }
	inline void set_textureSheetAnimationFrameSize_38(Vector2_t2156229523  value)
	{
		___textureSheetAnimationFrameSize_38 = value;
	}

	inline static int32_t get_offset_of_pRenderer_39() { return static_cast<int32_t>(offsetof(UIParticleSystem_t471890981, ___pRenderer_39)); }
	inline ParticleSystemRenderer_t2065813411 * get_pRenderer_39() const { return ___pRenderer_39; }
	inline ParticleSystemRenderer_t2065813411 ** get_address_of_pRenderer_39() { return &___pRenderer_39; }
	inline void set_pRenderer_39(ParticleSystemRenderer_t2065813411 * value)
	{
		___pRenderer_39 = value;
		Il2CppCodeGenWriteBarrier((&___pRenderer_39), value);
	}

	inline static int32_t get_offset_of_currentMaterial_40() { return static_cast<int32_t>(offsetof(UIParticleSystem_t471890981, ___currentMaterial_40)); }
	inline Material_t340375123 * get_currentMaterial_40() const { return ___currentMaterial_40; }
	inline Material_t340375123 ** get_address_of_currentMaterial_40() { return &___currentMaterial_40; }
	inline void set_currentMaterial_40(Material_t340375123 * value)
	{
		___currentMaterial_40 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_40), value);
	}

	inline static int32_t get_offset_of_currentTexture_41() { return static_cast<int32_t>(offsetof(UIParticleSystem_t471890981, ___currentTexture_41)); }
	inline Texture_t3661962703 * get_currentTexture_41() const { return ___currentTexture_41; }
	inline Texture_t3661962703 ** get_address_of_currentTexture_41() { return &___currentTexture_41; }
	inline void set_currentTexture_41(Texture_t3661962703 * value)
	{
		___currentTexture_41 = value;
		Il2CppCodeGenWriteBarrier((&___currentTexture_41), value);
	}

	inline static int32_t get_offset_of_mainModule_42() { return static_cast<int32_t>(offsetof(UIParticleSystem_t471890981, ___mainModule_42)); }
	inline MainModule_t2320046318  get_mainModule_42() const { return ___mainModule_42; }
	inline MainModule_t2320046318 * get_address_of_mainModule_42() { return &___mainModule_42; }
	inline void set_mainModule_42(MainModule_t2320046318  value)
	{
		___mainModule_42 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPARTICLESYSTEM_T471890981_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_30;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_31;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_32;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_33;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_35;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_36;

public:
	inline static int32_t get_offset_of_m_FontData_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_30)); }
	inline FontData_t746620069 * get_m_FontData_30() const { return ___m_FontData_30; }
	inline FontData_t746620069 ** get_address_of_m_FontData_30() { return &___m_FontData_30; }
	inline void set_m_FontData_30(FontData_t746620069 * value)
	{
		___m_FontData_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_30), value);
	}

	inline static int32_t get_offset_of_m_Text_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_31)); }
	inline String_t* get_m_Text_31() const { return ___m_Text_31; }
	inline String_t** get_address_of_m_Text_31() { return &___m_Text_31; }
	inline void set_m_Text_31(String_t* value)
	{
		___m_Text_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_31), value);
	}

	inline static int32_t get_offset_of_m_TextCache_32() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_32)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_32() const { return ___m_TextCache_32; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_32() { return &___m_TextCache_32; }
	inline void set_m_TextCache_32(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_32), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_33)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_33() const { return ___m_TextCacheForLayout_33; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_33() { return &___m_TextCacheForLayout_33; }
	inline void set_m_TextCacheForLayout_33(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_33), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_35() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_35)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_35() const { return ___m_DisableFontTextureRebuiltCallback_35; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_35() { return &___m_DisableFontTextureRebuiltCallback_35; }
	inline void set_m_DisableFontTextureRebuiltCallback_35(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_35 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_36() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_36)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_36() const { return ___m_TempVerts_36; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_36() { return &___m_TempVerts_36; }
	inline void set_m_TempVerts_36(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_36), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_34;

public:
	inline static int32_t get_offset_of_s_DefaultText_34() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_34)); }
	inline Material_t340375123 * get_s_DefaultText_34() const { return ___s_DefaultText_34; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_34() { return &___s_DefaultText_34; }
	inline void set_s_DefaultText_34(Material_t340375123 * value)
	{
		___s_DefaultText_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
#ifndef TEXTPIC_T3289895869_H
#define TEXTPIC_T3289895869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.TextPic
struct  TextPic_t3289895869  : public Text_t1901882714
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Extensions.TextPic::m_ImagesPool
	List_1_t4142344393 * ___m_ImagesPool_37;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.UI.Extensions.TextPic::culled_ImagesPool
	List_1_t2585711361 * ___culled_ImagesPool_38;
	// System.Boolean UnityEngine.UI.Extensions.TextPic::clearImages
	bool ___clearImages_39;
	// UnityEngine.Object UnityEngine.UI.Extensions.TextPic::thisLock
	Object_t631007953 * ___thisLock_40;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.Extensions.TextPic::m_ImagesVertexIndex
	List_1_t128053199 * ___m_ImagesVertexIndex_41;
	// System.String UnityEngine.UI.Extensions.TextPic::fixedString
	String_t* ___fixedString_43;
	// System.Boolean UnityEngine.UI.Extensions.TextPic::m_ClickParents
	bool ___m_ClickParents_44;
	// System.String UnityEngine.UI.Extensions.TextPic::m_OutputText
	String_t* ___m_OutputText_45;
	// UnityEngine.UI.Extensions.TextPic/IconName[] UnityEngine.UI.Extensions.TextPic::inspectorIconList
	IconNameU5BU5D_t3003742235* ___inspectorIconList_46;
	// System.Single UnityEngine.UI.Extensions.TextPic::ImageScalingFactor
	float ___ImageScalingFactor_47;
	// System.String UnityEngine.UI.Extensions.TextPic::hyperlinkColor
	String_t* ___hyperlinkColor_48;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.TextPic::imageOffset
	Vector2_t2156229523  ___imageOffset_49;
	// UnityEngine.UI.Button UnityEngine.UI.Extensions.TextPic::button
	Button_t4055032469 * ___button_50;
	// UnityEngine.UI.Selectable UnityEngine.UI.Extensions.TextPic::highlightselectable
	Selectable_t3250028441 * ___highlightselectable_51;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.Extensions.TextPic::positions
	List_1_t3628304265 * ___positions_52;
	// System.String UnityEngine.UI.Extensions.TextPic::previousText
	String_t* ___previousText_53;
	// System.Boolean UnityEngine.UI.Extensions.TextPic::isCreating_m_HrefInfos
	bool ___isCreating_m_HrefInfos_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.TextPic/HrefInfo> UnityEngine.UI.Extensions.TextPic::m_HrefInfos
	List_1_t337968791 * ___m_HrefInfos_55;
	// UnityEngine.UI.Extensions.TextPic/HrefClickEvent UnityEngine.UI.Extensions.TextPic::m_OnHrefClick
	HrefClickEvent_t324372001 * ___m_OnHrefClick_58;

public:
	inline static int32_t get_offset_of_m_ImagesPool_37() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___m_ImagesPool_37)); }
	inline List_1_t4142344393 * get_m_ImagesPool_37() const { return ___m_ImagesPool_37; }
	inline List_1_t4142344393 ** get_address_of_m_ImagesPool_37() { return &___m_ImagesPool_37; }
	inline void set_m_ImagesPool_37(List_1_t4142344393 * value)
	{
		___m_ImagesPool_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_ImagesPool_37), value);
	}

	inline static int32_t get_offset_of_culled_ImagesPool_38() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___culled_ImagesPool_38)); }
	inline List_1_t2585711361 * get_culled_ImagesPool_38() const { return ___culled_ImagesPool_38; }
	inline List_1_t2585711361 ** get_address_of_culled_ImagesPool_38() { return &___culled_ImagesPool_38; }
	inline void set_culled_ImagesPool_38(List_1_t2585711361 * value)
	{
		___culled_ImagesPool_38 = value;
		Il2CppCodeGenWriteBarrier((&___culled_ImagesPool_38), value);
	}

	inline static int32_t get_offset_of_clearImages_39() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___clearImages_39)); }
	inline bool get_clearImages_39() const { return ___clearImages_39; }
	inline bool* get_address_of_clearImages_39() { return &___clearImages_39; }
	inline void set_clearImages_39(bool value)
	{
		___clearImages_39 = value;
	}

	inline static int32_t get_offset_of_thisLock_40() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___thisLock_40)); }
	inline Object_t631007953 * get_thisLock_40() const { return ___thisLock_40; }
	inline Object_t631007953 ** get_address_of_thisLock_40() { return &___thisLock_40; }
	inline void set_thisLock_40(Object_t631007953 * value)
	{
		___thisLock_40 = value;
		Il2CppCodeGenWriteBarrier((&___thisLock_40), value);
	}

	inline static int32_t get_offset_of_m_ImagesVertexIndex_41() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___m_ImagesVertexIndex_41)); }
	inline List_1_t128053199 * get_m_ImagesVertexIndex_41() const { return ___m_ImagesVertexIndex_41; }
	inline List_1_t128053199 ** get_address_of_m_ImagesVertexIndex_41() { return &___m_ImagesVertexIndex_41; }
	inline void set_m_ImagesVertexIndex_41(List_1_t128053199 * value)
	{
		___m_ImagesVertexIndex_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_ImagesVertexIndex_41), value);
	}

	inline static int32_t get_offset_of_fixedString_43() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___fixedString_43)); }
	inline String_t* get_fixedString_43() const { return ___fixedString_43; }
	inline String_t** get_address_of_fixedString_43() { return &___fixedString_43; }
	inline void set_fixedString_43(String_t* value)
	{
		___fixedString_43 = value;
		Il2CppCodeGenWriteBarrier((&___fixedString_43), value);
	}

	inline static int32_t get_offset_of_m_ClickParents_44() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___m_ClickParents_44)); }
	inline bool get_m_ClickParents_44() const { return ___m_ClickParents_44; }
	inline bool* get_address_of_m_ClickParents_44() { return &___m_ClickParents_44; }
	inline void set_m_ClickParents_44(bool value)
	{
		___m_ClickParents_44 = value;
	}

	inline static int32_t get_offset_of_m_OutputText_45() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___m_OutputText_45)); }
	inline String_t* get_m_OutputText_45() const { return ___m_OutputText_45; }
	inline String_t** get_address_of_m_OutputText_45() { return &___m_OutputText_45; }
	inline void set_m_OutputText_45(String_t* value)
	{
		___m_OutputText_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_OutputText_45), value);
	}

	inline static int32_t get_offset_of_inspectorIconList_46() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___inspectorIconList_46)); }
	inline IconNameU5BU5D_t3003742235* get_inspectorIconList_46() const { return ___inspectorIconList_46; }
	inline IconNameU5BU5D_t3003742235** get_address_of_inspectorIconList_46() { return &___inspectorIconList_46; }
	inline void set_inspectorIconList_46(IconNameU5BU5D_t3003742235* value)
	{
		___inspectorIconList_46 = value;
		Il2CppCodeGenWriteBarrier((&___inspectorIconList_46), value);
	}

	inline static int32_t get_offset_of_ImageScalingFactor_47() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___ImageScalingFactor_47)); }
	inline float get_ImageScalingFactor_47() const { return ___ImageScalingFactor_47; }
	inline float* get_address_of_ImageScalingFactor_47() { return &___ImageScalingFactor_47; }
	inline void set_ImageScalingFactor_47(float value)
	{
		___ImageScalingFactor_47 = value;
	}

	inline static int32_t get_offset_of_hyperlinkColor_48() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___hyperlinkColor_48)); }
	inline String_t* get_hyperlinkColor_48() const { return ___hyperlinkColor_48; }
	inline String_t** get_address_of_hyperlinkColor_48() { return &___hyperlinkColor_48; }
	inline void set_hyperlinkColor_48(String_t* value)
	{
		___hyperlinkColor_48 = value;
		Il2CppCodeGenWriteBarrier((&___hyperlinkColor_48), value);
	}

	inline static int32_t get_offset_of_imageOffset_49() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___imageOffset_49)); }
	inline Vector2_t2156229523  get_imageOffset_49() const { return ___imageOffset_49; }
	inline Vector2_t2156229523 * get_address_of_imageOffset_49() { return &___imageOffset_49; }
	inline void set_imageOffset_49(Vector2_t2156229523  value)
	{
		___imageOffset_49 = value;
	}

	inline static int32_t get_offset_of_button_50() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___button_50)); }
	inline Button_t4055032469 * get_button_50() const { return ___button_50; }
	inline Button_t4055032469 ** get_address_of_button_50() { return &___button_50; }
	inline void set_button_50(Button_t4055032469 * value)
	{
		___button_50 = value;
		Il2CppCodeGenWriteBarrier((&___button_50), value);
	}

	inline static int32_t get_offset_of_highlightselectable_51() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___highlightselectable_51)); }
	inline Selectable_t3250028441 * get_highlightselectable_51() const { return ___highlightselectable_51; }
	inline Selectable_t3250028441 ** get_address_of_highlightselectable_51() { return &___highlightselectable_51; }
	inline void set_highlightselectable_51(Selectable_t3250028441 * value)
	{
		___highlightselectable_51 = value;
		Il2CppCodeGenWriteBarrier((&___highlightselectable_51), value);
	}

	inline static int32_t get_offset_of_positions_52() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___positions_52)); }
	inline List_1_t3628304265 * get_positions_52() const { return ___positions_52; }
	inline List_1_t3628304265 ** get_address_of_positions_52() { return &___positions_52; }
	inline void set_positions_52(List_1_t3628304265 * value)
	{
		___positions_52 = value;
		Il2CppCodeGenWriteBarrier((&___positions_52), value);
	}

	inline static int32_t get_offset_of_previousText_53() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___previousText_53)); }
	inline String_t* get_previousText_53() const { return ___previousText_53; }
	inline String_t** get_address_of_previousText_53() { return &___previousText_53; }
	inline void set_previousText_53(String_t* value)
	{
		___previousText_53 = value;
		Il2CppCodeGenWriteBarrier((&___previousText_53), value);
	}

	inline static int32_t get_offset_of_isCreating_m_HrefInfos_54() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___isCreating_m_HrefInfos_54)); }
	inline bool get_isCreating_m_HrefInfos_54() const { return ___isCreating_m_HrefInfos_54; }
	inline bool* get_address_of_isCreating_m_HrefInfos_54() { return &___isCreating_m_HrefInfos_54; }
	inline void set_isCreating_m_HrefInfos_54(bool value)
	{
		___isCreating_m_HrefInfos_54 = value;
	}

	inline static int32_t get_offset_of_m_HrefInfos_55() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___m_HrefInfos_55)); }
	inline List_1_t337968791 * get_m_HrefInfos_55() const { return ___m_HrefInfos_55; }
	inline List_1_t337968791 ** get_address_of_m_HrefInfos_55() { return &___m_HrefInfos_55; }
	inline void set_m_HrefInfos_55(List_1_t337968791 * value)
	{
		___m_HrefInfos_55 = value;
		Il2CppCodeGenWriteBarrier((&___m_HrefInfos_55), value);
	}

	inline static int32_t get_offset_of_m_OnHrefClick_58() { return static_cast<int32_t>(offsetof(TextPic_t3289895869, ___m_OnHrefClick_58)); }
	inline HrefClickEvent_t324372001 * get_m_OnHrefClick_58() const { return ___m_OnHrefClick_58; }
	inline HrefClickEvent_t324372001 ** get_address_of_m_OnHrefClick_58() { return &___m_OnHrefClick_58; }
	inline void set_m_OnHrefClick_58(HrefClickEvent_t324372001 * value)
	{
		___m_OnHrefClick_58 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnHrefClick_58), value);
	}
};

struct TextPic_t3289895869_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex UnityEngine.UI.Extensions.TextPic::s_Regex
	Regex_t3657309853 * ___s_Regex_42;
	// System.Text.StringBuilder UnityEngine.UI.Extensions.TextPic::s_TextBuilder
	StringBuilder_t * ___s_TextBuilder_56;
	// System.Text.RegularExpressions.Regex UnityEngine.UI.Extensions.TextPic::s_HrefRegex
	Regex_t3657309853 * ___s_HrefRegex_57;
	// System.Predicate`1<UnityEngine.UI.Image> UnityEngine.UI.Extensions.TextPic::<>f__am$cache0
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache0_59;

public:
	inline static int32_t get_offset_of_s_Regex_42() { return static_cast<int32_t>(offsetof(TextPic_t3289895869_StaticFields, ___s_Regex_42)); }
	inline Regex_t3657309853 * get_s_Regex_42() const { return ___s_Regex_42; }
	inline Regex_t3657309853 ** get_address_of_s_Regex_42() { return &___s_Regex_42; }
	inline void set_s_Regex_42(Regex_t3657309853 * value)
	{
		___s_Regex_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_Regex_42), value);
	}

	inline static int32_t get_offset_of_s_TextBuilder_56() { return static_cast<int32_t>(offsetof(TextPic_t3289895869_StaticFields, ___s_TextBuilder_56)); }
	inline StringBuilder_t * get_s_TextBuilder_56() const { return ___s_TextBuilder_56; }
	inline StringBuilder_t ** get_address_of_s_TextBuilder_56() { return &___s_TextBuilder_56; }
	inline void set_s_TextBuilder_56(StringBuilder_t * value)
	{
		___s_TextBuilder_56 = value;
		Il2CppCodeGenWriteBarrier((&___s_TextBuilder_56), value);
	}

	inline static int32_t get_offset_of_s_HrefRegex_57() { return static_cast<int32_t>(offsetof(TextPic_t3289895869_StaticFields, ___s_HrefRegex_57)); }
	inline Regex_t3657309853 * get_s_HrefRegex_57() const { return ___s_HrefRegex_57; }
	inline Regex_t3657309853 ** get_address_of_s_HrefRegex_57() { return &___s_HrefRegex_57; }
	inline void set_s_HrefRegex_57(Regex_t3657309853 * value)
	{
		___s_HrefRegex_57 = value;
		Il2CppCodeGenWriteBarrier((&___s_HrefRegex_57), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_59() { return static_cast<int32_t>(offsetof(TextPic_t3289895869_StaticFields, ___U3CU3Ef__amU24cache0_59)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache0_59() const { return ___U3CU3Ef__amU24cache0_59; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache0_59() { return &___U3CU3Ef__amU24cache0_59; }
	inline void set_U3CU3Ef__amU24cache0_59(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache0_59 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_59), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTPIC_T3289895869_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4600 = { sizeof (ColorLabel_t1657108856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4600[7] = 
{
	ColorLabel_t1657108856::get_offset_of_picker_4(),
	ColorLabel_t1657108856::get_offset_of_type_5(),
	ColorLabel_t1657108856::get_offset_of_prefix_6(),
	ColorLabel_t1657108856::get_offset_of_minValue_7(),
	ColorLabel_t1657108856::get_offset_of_maxValue_8(),
	ColorLabel_t1657108856::get_offset_of_precision_9(),
	ColorLabel_t1657108856::get_offset_of_label_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4601 = { sizeof (ColorPickerControl_t2793111723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4601[9] = 
{
	ColorPickerControl_t2793111723::get_offset_of__hue_4(),
	ColorPickerControl_t2793111723::get_offset_of__saturation_5(),
	ColorPickerControl_t2793111723::get_offset_of__brightness_6(),
	ColorPickerControl_t2793111723::get_offset_of__red_7(),
	ColorPickerControl_t2793111723::get_offset_of__green_8(),
	ColorPickerControl_t2793111723::get_offset_of__blue_9(),
	ColorPickerControl_t2793111723::get_offset_of__alpha_10(),
	ColorPickerControl_t2793111723::get_offset_of_onValueChanged_11(),
	ColorPickerControl_t2793111723::get_offset_of_onHSVChanged_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4602 = { sizeof (ColorPickerPresets_t2164031678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4602[3] = 
{
	ColorPickerPresets_t2164031678::get_offset_of_picker_4(),
	ColorPickerPresets_t2164031678::get_offset_of_presets_5(),
	ColorPickerPresets_t2164031678::get_offset_of_createPresetImage_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4603 = { sizeof (ColorPickerTester_t1752022104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4603[2] = 
{
	ColorPickerTester_t1752022104::get_offset_of_pickerRenderer_4(),
	ColorPickerTester_t1752022104::get_offset_of_picker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4604 = { sizeof (ColorSlider_t188049029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4604[4] = 
{
	ColorSlider_t188049029::get_offset_of_ColorPicker_4(),
	ColorSlider_t188049029::get_offset_of_type_5(),
	ColorSlider_t188049029::get_offset_of_slider_6(),
	ColorSlider_t188049029::get_offset_of_listen_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4605 = { sizeof (ColorSliderImage_t1534316151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4605[4] = 
{
	ColorSliderImage_t1534316151::get_offset_of_picker_4(),
	ColorSliderImage_t1534316151::get_offset_of_type_5(),
	ColorSliderImage_t1534316151::get_offset_of_direction_6(),
	ColorSliderImage_t1534316151::get_offset_of_image_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4606 = { sizeof (ColorValues_t950091080)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4606[8] = 
{
	ColorValues_t950091080::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4607 = { sizeof (ColorChangedEvent_t3019780707), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4608 = { sizeof (HSVChangedEvent_t911780251), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4609 = { sizeof (HexColorField_t106756477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4609[4] = 
{
	HexColorField_t106756477::get_offset_of_ColorPicker_4(),
	HexColorField_t106756477::get_offset_of_displayAlpha_5(),
	HexColorField_t106756477::get_offset_of_hexInputField_6(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4610 = { sizeof (HSVUtil_t2354927206), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4611 = { sizeof (HsvColor_t3316572990)+ sizeof (RuntimeObject), sizeof(HsvColor_t3316572990 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4611[3] = 
{
	HsvColor_t3316572990::get_offset_of_H_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HsvColor_t3316572990::get_offset_of_S_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HsvColor_t3316572990::get_offset_of_V_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4612 = { sizeof (SVBoxSlider_t1594361808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4612[5] = 
{
	SVBoxSlider_t1594361808::get_offset_of_picker_4(),
	SVBoxSlider_t1594361808::get_offset_of_slider_5(),
	SVBoxSlider_t1594361808::get_offset_of_image_6(),
	SVBoxSlider_t1594361808::get_offset_of_lastH_7(),
	SVBoxSlider_t1594361808::get_offset_of_listen_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4613 = { sizeof (TiltWindow_t4093125161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4613[4] = 
{
	TiltWindow_t4093125161::get_offset_of_range_4(),
	TiltWindow_t4093125161::get_offset_of_mTrans_5(),
	TiltWindow_t4093125161::get_offset_of_mStart_6(),
	TiltWindow_t4093125161::get_offset_of_mRot_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4614 = { sizeof (AutoCompleteSearchType_t2628651075)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4614[3] = 
{
	AutoCompleteSearchType_t2628651075::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4615 = { sizeof (AutoCompleteComboBox_t2765567798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4615[33] = 
{
	AutoCompleteComboBox_t2765567798::get_offset_of_disabledTextColor_4(),
	AutoCompleteComboBox_t2765567798::get_offset_of_U3CSelectedItemU3Ek__BackingField_5(),
	AutoCompleteComboBox_t2765567798::get_offset_of_AvailableOptions_6(),
	AutoCompleteComboBox_t2765567798::get_offset_of__isPanelActive_7(),
	AutoCompleteComboBox_t2765567798::get_offset_of__hasDrawnOnce_8(),
	AutoCompleteComboBox_t2765567798::get_offset_of__mainInput_9(),
	AutoCompleteComboBox_t2765567798::get_offset_of__inputRT_10(),
	AutoCompleteComboBox_t2765567798::get_offset_of__rectTransform_11(),
	AutoCompleteComboBox_t2765567798::get_offset_of__overlayRT_12(),
	AutoCompleteComboBox_t2765567798::get_offset_of__scrollPanelRT_13(),
	AutoCompleteComboBox_t2765567798::get_offset_of__scrollBarRT_14(),
	AutoCompleteComboBox_t2765567798::get_offset_of__slidingAreaRT_15(),
	AutoCompleteComboBox_t2765567798::get_offset_of__itemsPanelRT_16(),
	AutoCompleteComboBox_t2765567798::get_offset_of__canvas_17(),
	AutoCompleteComboBox_t2765567798::get_offset_of__canvasRT_18(),
	AutoCompleteComboBox_t2765567798::get_offset_of__scrollRect_19(),
	AutoCompleteComboBox_t2765567798::get_offset_of__panelItems_20(),
	AutoCompleteComboBox_t2765567798::get_offset_of__prunedPanelItems_21(),
	AutoCompleteComboBox_t2765567798::get_offset_of_panelObjects_22(),
	AutoCompleteComboBox_t2765567798::get_offset_of_itemTemplate_23(),
	AutoCompleteComboBox_t2765567798::get_offset_of_U3CTextU3Ek__BackingField_24(),
	AutoCompleteComboBox_t2765567798::get_offset_of__scrollBarWidth_25(),
	AutoCompleteComboBox_t2765567798::get_offset_of__itemsToDisplay_26(),
	AutoCompleteComboBox_t2765567798::get_offset_of_SelectFirstItemOnStart_27(),
	AutoCompleteComboBox_t2765567798::get_offset_of__ChangeInputTextColorBasedOnMatchingItems_28(),
	AutoCompleteComboBox_t2765567798::get_offset_of_ValidSelectionTextColor_29(),
	AutoCompleteComboBox_t2765567798::get_offset_of_MatchingItemsRemainingTextColor_30(),
	AutoCompleteComboBox_t2765567798::get_offset_of_NoItemsRemainingTextColor_31(),
	AutoCompleteComboBox_t2765567798::get_offset_of_autocompleteSearchType_32(),
	AutoCompleteComboBox_t2765567798::get_offset_of__selectionIsValid_33(),
	AutoCompleteComboBox_t2765567798::get_offset_of_OnSelectionTextChanged_34(),
	AutoCompleteComboBox_t2765567798::get_offset_of_OnSelectionValidityChanged_35(),
	AutoCompleteComboBox_t2765567798::get_offset_of_OnSelectionChanged_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4616 = { sizeof (SelectionChangedEvent_t1822043360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4617 = { sizeof (SelectionTextChangedEvent_t4051177638), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4618 = { sizeof (SelectionValidityChangedEvent_t954817928), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4619 = { sizeof (U3CRebuildPanelU3Ec__AnonStorey0_t1110430399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4619[2] = 
{
	U3CRebuildPanelU3Ec__AnonStorey0_t1110430399::get_offset_of_textOfItem_0(),
	U3CRebuildPanelU3Ec__AnonStorey0_t1110430399::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4620 = { sizeof (U3CPruneItemsLinqU3Ec__AnonStorey1_t1526519783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4620[1] = 
{
	U3CPruneItemsLinqU3Ec__AnonStorey1_t1526519783::get_offset_of_currText_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4621 = { sizeof (ComboBox_t4216213764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4621[23] = 
{
	ComboBox_t4216213764::get_offset_of_disabledTextColor_4(),
	ComboBox_t4216213764::get_offset_of_U3CSelectedItemU3Ek__BackingField_5(),
	ComboBox_t4216213764::get_offset_of_AvailableOptions_6(),
	ComboBox_t4216213764::get_offset_of__scrollBarWidth_7(),
	ComboBox_t4216213764::get_offset_of__itemsToDisplay_8(),
	ComboBox_t4216213764::get_offset_of_OnSelectionChanged_9(),
	ComboBox_t4216213764::get_offset_of__isPanelActive_10(),
	ComboBox_t4216213764::get_offset_of__hasDrawnOnce_11(),
	ComboBox_t4216213764::get_offset_of__mainInput_12(),
	ComboBox_t4216213764::get_offset_of__inputRT_13(),
	ComboBox_t4216213764::get_offset_of__rectTransform_14(),
	ComboBox_t4216213764::get_offset_of__overlayRT_15(),
	ComboBox_t4216213764::get_offset_of__scrollPanelRT_16(),
	ComboBox_t4216213764::get_offset_of__scrollBarRT_17(),
	ComboBox_t4216213764::get_offset_of__slidingAreaRT_18(),
	ComboBox_t4216213764::get_offset_of__itemsPanelRT_19(),
	ComboBox_t4216213764::get_offset_of__canvas_20(),
	ComboBox_t4216213764::get_offset_of__canvasRT_21(),
	ComboBox_t4216213764::get_offset_of__scrollRect_22(),
	ComboBox_t4216213764::get_offset_of__panelItems_23(),
	ComboBox_t4216213764::get_offset_of_panelObjects_24(),
	ComboBox_t4216213764::get_offset_of_itemTemplate_25(),
	ComboBox_t4216213764::get_offset_of_U3CTextU3Ek__BackingField_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4622 = { sizeof (SelectionChangedEvent_t2252533886), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4623 = { sizeof (U3CRebuildPanelU3Ec__AnonStorey0_t2402133322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4623[2] = 
{
	U3CRebuildPanelU3Ec__AnonStorey0_t2402133322::get_offset_of_textOfItem_0(),
	U3CRebuildPanelU3Ec__AnonStorey0_t2402133322::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4624 = { sizeof (DropDownList_t4179439446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4624[23] = 
{
	DropDownList_t4179439446::get_offset_of_disabledTextColor_4(),
	DropDownList_t4179439446::get_offset_of_U3CSelectedItemU3Ek__BackingField_5(),
	DropDownList_t4179439446::get_offset_of_Items_6(),
	DropDownList_t4179439446::get_offset_of_OverrideHighlighted_7(),
	DropDownList_t4179439446::get_offset_of__isPanelActive_8(),
	DropDownList_t4179439446::get_offset_of__hasDrawnOnce_9(),
	DropDownList_t4179439446::get_offset_of__mainButton_10(),
	DropDownList_t4179439446::get_offset_of__rectTransform_11(),
	DropDownList_t4179439446::get_offset_of__overlayRT_12(),
	DropDownList_t4179439446::get_offset_of__scrollPanelRT_13(),
	DropDownList_t4179439446::get_offset_of__scrollBarRT_14(),
	DropDownList_t4179439446::get_offset_of__slidingAreaRT_15(),
	DropDownList_t4179439446::get_offset_of__itemsPanelRT_16(),
	DropDownList_t4179439446::get_offset_of__canvas_17(),
	DropDownList_t4179439446::get_offset_of__canvasRT_18(),
	DropDownList_t4179439446::get_offset_of__scrollRect_19(),
	DropDownList_t4179439446::get_offset_of__panelItems_20(),
	DropDownList_t4179439446::get_offset_of__itemTemplate_21(),
	DropDownList_t4179439446::get_offset_of__scrollBarWidth_22(),
	DropDownList_t4179439446::get_offset_of__selectedIndex_23(),
	DropDownList_t4179439446::get_offset_of__itemsToDisplay_24(),
	DropDownList_t4179439446::get_offset_of_SelectFirstItemOnStart_25(),
	DropDownList_t4179439446::get_offset_of_OnSelectionChanged_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4625 = { sizeof (SelectionChangedEvent_t1418459309), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4626 = { sizeof (U3CRebuildPanelU3Ec__AnonStorey0_t4108379701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4626[3] = 
{
	U3CRebuildPanelU3Ec__AnonStorey0_t4108379701::get_offset_of_ii_0(),
	U3CRebuildPanelU3Ec__AnonStorey0_t4108379701::get_offset_of_item_1(),
	U3CRebuildPanelU3Ec__AnonStorey0_t4108379701::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4627 = { sizeof (DropDownListButton_t3241764549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4627[6] = 
{
	DropDownListButton_t3241764549::get_offset_of_rectTransform_0(),
	DropDownListButton_t3241764549::get_offset_of_btn_1(),
	DropDownListButton_t3241764549::get_offset_of_txt_2(),
	DropDownListButton_t3241764549::get_offset_of_btnImg_3(),
	DropDownListButton_t3241764549::get_offset_of_img_4(),
	DropDownListButton_t3241764549::get_offset_of_gameobject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4628 = { sizeof (DropDownListItem_t337244270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4628[6] = 
{
	DropDownListItem_t337244270::get_offset_of__caption_0(),
	DropDownListItem_t337244270::get_offset_of__image_1(),
	DropDownListItem_t337244270::get_offset_of__isDisabled_2(),
	DropDownListItem_t337244270::get_offset_of__id_3(),
	DropDownListItem_t337244270::get_offset_of_OnSelect_4(),
	DropDownListItem_t337244270::get_offset_of_OnUpdate_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4629 = { sizeof (CooldownButton_t2372397950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4629[12] = 
{
	CooldownButton_t2372397950::get_offset_of_cooldownTimeout_4(),
	CooldownButton_t2372397950::get_offset_of_cooldownSpeed_5(),
	CooldownButton_t2372397950::get_offset_of_cooldownActive_6(),
	CooldownButton_t2372397950::get_offset_of_cooldownInEffect_7(),
	CooldownButton_t2372397950::get_offset_of_cooldownTimeElapsed_8(),
	CooldownButton_t2372397950::get_offset_of_cooldownTimeRemaining_9(),
	CooldownButton_t2372397950::get_offset_of_cooldownPercentRemaining_10(),
	CooldownButton_t2372397950::get_offset_of_cooldownPercentComplete_11(),
	CooldownButton_t2372397950::get_offset_of_buttonSource_12(),
	CooldownButton_t2372397950::get_offset_of_OnCooldownStart_13(),
	CooldownButton_t2372397950::get_offset_of_OnButtonClickDuringCooldown_14(),
	CooldownButton_t2372397950::get_offset_of_OnCoolDownFinish_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4630 = { sizeof (CooldownButtonEvent_t856711112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4631 = { sizeof (InputFocus_t2498000986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4631[2] = 
{
	InputFocus_t2498000986::get_offset_of__inputField_4(),
	InputFocus_t2498000986::get_offset_of__ignoreNextActivation_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4632 = { sizeof (MultiTouchScrollRect_t2699801454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4632[1] = 
{
	MultiTouchScrollRect_t2699801454::get_offset_of_pid_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4633 = { sizeof (RadialSlider_t2127270712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4633[17] = 
{
	RadialSlider_t2127270712::get_offset_of_isPointerDown_4(),
	RadialSlider_t2127270712::get_offset_of_isPointerReleased_5(),
	RadialSlider_t2127270712::get_offset_of_lerpInProgress_6(),
	RadialSlider_t2127270712::get_offset_of_m_localPos_7(),
	RadialSlider_t2127270712::get_offset_of_m_targetAngle_8(),
	RadialSlider_t2127270712::get_offset_of_m_lerpTargetAngle_9(),
	RadialSlider_t2127270712::get_offset_of_m_startAngle_10(),
	RadialSlider_t2127270712::get_offset_of_m_currentLerpTime_11(),
	RadialSlider_t2127270712::get_offset_of_m_lerpTime_12(),
	RadialSlider_t2127270712::get_offset_of_m_eventCamera_13(),
	RadialSlider_t2127270712::get_offset_of_m_image_14(),
	RadialSlider_t2127270712::get_offset_of_m_startColor_15(),
	RadialSlider_t2127270712::get_offset_of_m_endColor_16(),
	RadialSlider_t2127270712::get_offset_of_m_lerpToTarget_17(),
	RadialSlider_t2127270712::get_offset_of_m_lerpCurve_18(),
	RadialSlider_t2127270712::get_offset_of__onValueChanged_19(),
	RadialSlider_t2127270712::get_offset_of__onTextValueChanged_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4634 = { sizeof (RadialSliderValueChangedEvent_t1025479356), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4635 = { sizeof (RadialSliderTextValueChangedEvent_t1078616506), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4636 = { sizeof (ReorderableList_t1822109201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4636[11] = 
{
	ReorderableList_t1822109201::get_offset_of_ContentLayout_4(),
	ReorderableList_t1822109201::get_offset_of_DraggableArea_5(),
	ReorderableList_t1822109201::get_offset_of_IsDraggable_6(),
	ReorderableList_t1822109201::get_offset_of_CloneDraggedObject_7(),
	ReorderableList_t1822109201::get_offset_of_IsDropable_8(),
	ReorderableList_t1822109201::get_offset_of_OnElementDropped_9(),
	ReorderableList_t1822109201::get_offset_of_OnElementGrabbed_10(),
	ReorderableList_t1822109201::get_offset_of_OnElementRemoved_11(),
	ReorderableList_t1822109201::get_offset_of_OnElementAdded_12(),
	ReorderableList_t1822109201::get_offset_of__content_13(),
	ReorderableList_t1822109201::get_offset_of__listContent_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4637 = { sizeof (ReorderableListEventStruct_t1762416412)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4637[7] = 
{
	ReorderableListEventStruct_t1762416412::get_offset_of_DroppedObject_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReorderableListEventStruct_t1762416412::get_offset_of_FromIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReorderableListEventStruct_t1762416412::get_offset_of_FromList_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReorderableListEventStruct_t1762416412::get_offset_of_IsAClone_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReorderableListEventStruct_t1762416412::get_offset_of_SourceObject_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReorderableListEventStruct_t1762416412::get_offset_of_ToIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReorderableListEventStruct_t1762416412::get_offset_of_ToList_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4638 = { sizeof (ReorderableListHandler_t1290756480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4639 = { sizeof (ReorderableListContent_t2633001117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4639[5] = 
{
	ReorderableListContent_t2633001117::get_offset_of__cachedChildren_4(),
	ReorderableListContent_t2633001117::get_offset_of__cachedListElement_5(),
	ReorderableListContent_t2633001117::get_offset_of__ele_6(),
	ReorderableListContent_t2633001117::get_offset_of__extList_7(),
	ReorderableListContent_t2633001117::get_offset_of__rect_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4640 = { sizeof (U3CRefreshChildrenU3Ec__Iterator0_t2101808732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4640[4] = 
{
	U3CRefreshChildrenU3Ec__Iterator0_t2101808732::get_offset_of_U24this_0(),
	U3CRefreshChildrenU3Ec__Iterator0_t2101808732::get_offset_of_U24current_1(),
	U3CRefreshChildrenU3Ec__Iterator0_t2101808732::get_offset_of_U24disposing_2(),
	U3CRefreshChildrenU3Ec__Iterator0_t2101808732::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4641 = { sizeof (ReorderableListDebug_t2870269905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4641[1] = 
{
	ReorderableListDebug_t2870269905::get_offset_of_DebugLabel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4642 = { sizeof (ReorderableListElement_t1884325753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4642[15] = 
{
	ReorderableListElement_t1884325753::get_offset_of_IsGrabbable_4(),
	ReorderableListElement_t1884325753::get_offset_of_IsTransferable_5(),
	ReorderableListElement_t1884325753::get_offset_of_isDroppableInSpace_6(),
	ReorderableListElement_t1884325753::get_offset_of__raycastResults_7(),
	ReorderableListElement_t1884325753::get_offset_of__currentReorderableListRaycasted_8(),
	ReorderableListElement_t1884325753::get_offset_of__draggingObject_9(),
	ReorderableListElement_t1884325753::get_offset_of__draggingObjectLE_10(),
	ReorderableListElement_t1884325753::get_offset_of__draggingObjectOriginalSize_11(),
	ReorderableListElement_t1884325753::get_offset_of__fakeElement_12(),
	ReorderableListElement_t1884325753::get_offset_of__fakeElementLE_13(),
	ReorderableListElement_t1884325753::get_offset_of__fromIndex_14(),
	ReorderableListElement_t1884325753::get_offset_of__isDragging_15(),
	ReorderableListElement_t1884325753::get_offset_of__rect_16(),
	ReorderableListElement_t1884325753::get_offset_of__reorderableList_17(),
	ReorderableListElement_t1884325753::get_offset_of_isValid_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4643 = { sizeof (RescaleDragPanel_t733373206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4643[4] = 
{
	RescaleDragPanel_t733373206::get_offset_of_pointerOffset_4(),
	RescaleDragPanel_t733373206::get_offset_of_canvasRectTransform_5(),
	RescaleDragPanel_t733373206::get_offset_of_panelRectTransform_6(),
	RescaleDragPanel_t733373206::get_offset_of_goTransform_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4644 = { sizeof (RescalePanel_t1067833034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4644[8] = 
{
	RescalePanel_t1067833034::get_offset_of_minSize_4(),
	RescalePanel_t1067833034::get_offset_of_maxSize_5(),
	RescalePanel_t1067833034::get_offset_of_rectTransform_6(),
	RescalePanel_t1067833034::get_offset_of_goTransform_7(),
	RescalePanel_t1067833034::get_offset_of_currentPointerPosition_8(),
	RescalePanel_t1067833034::get_offset_of_previousPointerPosition_9(),
	RescalePanel_t1067833034::get_offset_of_thisRectTransform_10(),
	RescalePanel_t1067833034::get_offset_of_sizeDelta_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4645 = { sizeof (ResizePanel_t686254421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4645[6] = 
{
	ResizePanel_t686254421::get_offset_of_minSize_4(),
	ResizePanel_t686254421::get_offset_of_maxSize_5(),
	ResizePanel_t686254421::get_offset_of_rectTransform_6(),
	ResizePanel_t686254421::get_offset_of_currentPointerPosition_7(),
	ResizePanel_t686254421::get_offset_of_previousPointerPosition_8(),
	ResizePanel_t686254421::get_offset_of_ratio_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4646 = { sizeof (SegmentedControl_t2965132545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4646[8] = 
{
	SegmentedControl_t2965132545::get_offset_of_m_segments_4(),
	SegmentedControl_t2965132545::get_offset_of_m_separator_5(),
	SegmentedControl_t2965132545::get_offset_of_m_separatorWidth_6(),
	SegmentedControl_t2965132545::get_offset_of_m_allowSwitchingOff_7(),
	SegmentedControl_t2965132545::get_offset_of_m_selectedSegmentIndex_8(),
	SegmentedControl_t2965132545::get_offset_of_m_onValueChanged_9(),
	SegmentedControl_t2965132545::get_offset_of_selectedSegment_10(),
	SegmentedControl_t2965132545::get_offset_of_selectedColor_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4647 = { sizeof (SegmentSelectedEvent_t878161132), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4648 = { sizeof (Segment_t1973990243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4648[2] = 
{
	Segment_t1973990243::get_offset_of_index_4(),
	Segment_t1973990243::get_offset_of_textColor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4649 = { sizeof (ExampleSelectable_t1184518557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4649[5] = 
{
	ExampleSelectable_t1184518557::get_offset_of__selected_4(),
	ExampleSelectable_t1184518557::get_offset_of__preSelected_5(),
	ExampleSelectable_t1184518557::get_offset_of_spriteRenderer_6(),
	ExampleSelectable_t1184518557::get_offset_of_image_7(),
	ExampleSelectable_t1184518557::get_offset_of_text_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4650 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4651 = { sizeof (SelectionBox_t2044353942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4651[10] = 
{
	SelectionBox_t2044353942::get_offset_of_color_4(),
	SelectionBox_t2044353942::get_offset_of_art_5(),
	SelectionBox_t2044353942::get_offset_of_origin_6(),
	SelectionBox_t2044353942::get_offset_of_selectionMask_7(),
	SelectionBox_t2044353942::get_offset_of_boxRect_8(),
	SelectionBox_t2044353942::get_offset_of_selectables_9(),
	SelectionBox_t2044353942::get_offset_of_selectableGroup_10(),
	SelectionBox_t2044353942::get_offset_of_clickedBeforeDrag_11(),
	SelectionBox_t2044353942::get_offset_of_clickedAfterDrag_12(),
	SelectionBox_t2044353942::get_offset_of_onSelectionChange_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4652 = { sizeof (SelectionEvent_t3355704588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4653 = { sizeof (Stepper_t2492277228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4653[9] = 
{
	Stepper_t2492277228::get_offset_of__sides_4(),
	Stepper_t2492277228::get_offset_of__value_5(),
	Stepper_t2492277228::get_offset_of__minimum_6(),
	Stepper_t2492277228::get_offset_of__maximum_7(),
	Stepper_t2492277228::get_offset_of__step_8(),
	Stepper_t2492277228::get_offset_of__wrap_9(),
	Stepper_t2492277228::get_offset_of__separator_10(),
	Stepper_t2492277228::get_offset_of__separatorWidth_11(),
	Stepper_t2492277228::get_offset_of__onValueChanged_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4654 = { sizeof (StepperValueChangedEvent_t1994331895), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4655 = { sizeof (StepperSide_t4217246429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4656 = { sizeof (TextPic_t3289895869), -1, sizeof(TextPic_t3289895869_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4656[23] = 
{
	TextPic_t3289895869::get_offset_of_m_ImagesPool_37(),
	TextPic_t3289895869::get_offset_of_culled_ImagesPool_38(),
	TextPic_t3289895869::get_offset_of_clearImages_39(),
	TextPic_t3289895869::get_offset_of_thisLock_40(),
	TextPic_t3289895869::get_offset_of_m_ImagesVertexIndex_41(),
	TextPic_t3289895869_StaticFields::get_offset_of_s_Regex_42(),
	TextPic_t3289895869::get_offset_of_fixedString_43(),
	TextPic_t3289895869::get_offset_of_m_ClickParents_44(),
	TextPic_t3289895869::get_offset_of_m_OutputText_45(),
	TextPic_t3289895869::get_offset_of_inspectorIconList_46(),
	TextPic_t3289895869::get_offset_of_ImageScalingFactor_47(),
	TextPic_t3289895869::get_offset_of_hyperlinkColor_48(),
	TextPic_t3289895869::get_offset_of_imageOffset_49(),
	TextPic_t3289895869::get_offset_of_button_50(),
	TextPic_t3289895869::get_offset_of_highlightselectable_51(),
	TextPic_t3289895869::get_offset_of_positions_52(),
	TextPic_t3289895869::get_offset_of_previousText_53(),
	TextPic_t3289895869::get_offset_of_isCreating_m_HrefInfos_54(),
	TextPic_t3289895869::get_offset_of_m_HrefInfos_55(),
	TextPic_t3289895869_StaticFields::get_offset_of_s_TextBuilder_56(),
	TextPic_t3289895869_StaticFields::get_offset_of_s_HrefRegex_57(),
	TextPic_t3289895869::get_offset_of_m_OnHrefClick_58(),
	TextPic_t3289895869_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_59(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4657 = { sizeof (IconName_t399235694)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4657[4] = 
{
	IconName_t399235694::get_offset_of_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IconName_t399235694::get_offset_of_sprite_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IconName_t399235694::get_offset_of_offset_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IconName_t399235694::get_offset_of_scale_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4658 = { sizeof (HrefClickEvent_t324372001), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4659 = { sizeof (HrefInfo_t3160861345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4659[4] = 
{
	HrefInfo_t3160861345::get_offset_of_startIndex_0(),
	HrefInfo_t3160861345::get_offset_of_endIndex_1(),
	HrefInfo_t3160861345::get_offset_of_name_2(),
	HrefInfo_t3160861345::get_offset_of_boxes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4660 = { sizeof (UI_Knob_t1735628298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4660[15] = 
{
	UI_Knob_t1735628298::get_offset_of_direction_4(),
	UI_Knob_t1735628298::get_offset_of_knobValue_5(),
	UI_Knob_t1735628298::get_offset_of_maxValue_6(),
	UI_Knob_t1735628298::get_offset_of_loops_7(),
	UI_Knob_t1735628298::get_offset_of_clampOutput01_8(),
	UI_Knob_t1735628298::get_offset_of_snapToPosition_9(),
	UI_Knob_t1735628298::get_offset_of_snapStepsPerLoop_10(),
	UI_Knob_t1735628298::get_offset_of_OnValueChanged_11(),
	UI_Knob_t1735628298::get_offset_of__currentLoops_12(),
	UI_Knob_t1735628298::get_offset_of__previousValue_13(),
	UI_Knob_t1735628298::get_offset_of__initAngle_14(),
	UI_Knob_t1735628298::get_offset_of__currentAngle_15(),
	UI_Knob_t1735628298::get_offset_of__currentVector_16(),
	UI_Knob_t1735628298::get_offset_of__initRotation_17(),
	UI_Knob_t1735628298::get_offset_of__canDrag_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4661 = { sizeof (Direction_t2018151358)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4661[3] = 
{
	Direction_t2018151358::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4662 = { sizeof (KnobFloatValueEvent_t1285673625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4663 = { sizeof (BestFitOutline_t4228912216), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4664 = { sizeof (CUIBezierCurve_t3136617550), -1, sizeof(CUIBezierCurve_t3136617550_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4664[3] = 
{
	CUIBezierCurve_t3136617550_StaticFields::get_offset_of_CubicBezierCurvePtNum_4(),
	CUIBezierCurve_t3136617550::get_offset_of_controlPoints_5(),
	CUIBezierCurve_t3136617550::get_offset_of_OnRefresh_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4665 = { sizeof (CUIGraphic_t2936003233), -1, sizeof(CUIGraphic_t2936003233_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4665[11] = 
{
	CUIGraphic_t2936003233_StaticFields::get_offset_of_bottomCurveIdx_5(),
	CUIGraphic_t2936003233_StaticFields::get_offset_of_topCurveIdx_6(),
	CUIGraphic_t2936003233::get_offset_of_isCurved_7(),
	CUIGraphic_t2936003233::get_offset_of_isLockWithRatio_8(),
	CUIGraphic_t2936003233::get_offset_of_resolution_9(),
	CUIGraphic_t2936003233::get_offset_of_rectTrans_10(),
	CUIGraphic_t2936003233::get_offset_of_uiGraphic_11(),
	CUIGraphic_t2936003233::get_offset_of_refCUIGraphic_12(),
	CUIGraphic_t2936003233::get_offset_of_refCurves_13(),
	CUIGraphic_t2936003233::get_offset_of_refCurvesControlRatioPoints_14(),
	CUIGraphic_t2936003233::get_offset_of_reuse_quads_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4666 = { sizeof (CUIImage_t1061186866), -1, sizeof(CUIImage_t1061186866_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4666[4] = 
{
	CUIImage_t1061186866_StaticFields::get_offset_of_SlicedImageCornerRefVertexIdx_16(),
	CUIImage_t1061186866_StaticFields::get_offset_of_FilledImageCornerRefVertexIdx_17(),
	CUIImage_t1061186866::get_offset_of_cornerPosRatio_18(),
	CUIImage_t1061186866::get_offset_of_oriCornerPosRatio_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4667 = { sizeof (Vector3_Array2D_t2295860118)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4667[1] = 
{
	Vector3_Array2D_t2295860118::get_offset_of_array_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4668 = { sizeof (CUIText_t1792872957), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4669 = { sizeof (CurvedText_t1522163716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4669[3] = 
{
	CurvedText_t1522163716::get_offset_of__curveForText_5(),
	CurvedText_t1522163716::get_offset_of__curveMultiplier_6(),
	CurvedText_t1522163716::get_offset_of_rectTrans_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4670 = { sizeof (CylinderText_t364731485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4670[2] = 
{
	CylinderText_t364731485::get_offset_of_radius_5(),
	CylinderText_t364731485::get_offset_of_rectTrans_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4671 = { sizeof (Gradient_t2828844715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4671[6] = 
{
	Gradient_t2828844715::get_offset_of__gradientMode_5(),
	Gradient_t2828844715::get_offset_of__gradientDir_6(),
	Gradient_t2828844715::get_offset_of__overwriteAllColor_7(),
	Gradient_t2828844715::get_offset_of__vertex1_8(),
	Gradient_t2828844715::get_offset_of__vertex2_9(),
	Gradient_t2828844715::get_offset_of_targetGraphic_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4672 = { sizeof (GradientMode_t3981626032)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4672[3] = 
{
	GradientMode_t3981626032::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4673 = { sizeof (GradientDir_t1285337419)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4673[5] = 
{
	GradientDir_t1285337419::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4674 = { sizeof (Gradient2_t3786049496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4674[4] = 
{
	Gradient2_t3786049496::get_offset_of__gradientType_5(),
	Gradient2_t3786049496::get_offset_of__blendMode_6(),
	Gradient2_t3786049496::get_offset_of__offset_7(),
	Gradient2_t3786049496::get_offset_of__effectGradient_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4675 = { sizeof (Type_t3681360936)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4675[5] = 
{
	Type_t3681360936::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4676 = { sizeof (Blend_t976317323)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4676[4] = 
{
	Blend_t976317323::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4677 = { sizeof (LetterSpacing_t1421332419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4677[1] = 
{
	LetterSpacing_t1421332419::get_offset_of_m_spacing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4678 = { sizeof (MonoSpacing_t1166202091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4678[5] = 
{
	MonoSpacing_t1166202091::get_offset_of_m_spacing_5(),
	MonoSpacing_t1166202091::get_offset_of_HalfCharWidth_6(),
	MonoSpacing_t1166202091::get_offset_of_UseHalfCharWidth_7(),
	MonoSpacing_t1166202091::get_offset_of_rectTransform_8(),
	MonoSpacing_t1166202091::get_offset_of_text_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4679 = { sizeof (NicerOutline_t22425468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4679[3] = 
{
	NicerOutline_t22425468::get_offset_of_m_EffectColor_5(),
	NicerOutline_t22425468::get_offset_of_m_EffectDistance_6(),
	NicerOutline_t22425468::get_offset_of_m_UseGraphicAlpha_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4680 = { sizeof (RaycastMask_t2166940029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4680[2] = 
{
	RaycastMask_t2166940029::get_offset_of__image_4(),
	RaycastMask_t2166940029::get_offset_of__sprite_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4681 = { sizeof (UIAdditiveEffect_t1803193993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4681[1] = 
{
	UIAdditiveEffect_t1803193993::get_offset_of_mGraphic_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4682 = { sizeof (UIImageCrop_t41034629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4682[6] = 
{
	UIImageCrop_t41034629::get_offset_of_mGraphic_4(),
	UIImageCrop_t41034629::get_offset_of_mat_5(),
	UIImageCrop_t41034629::get_offset_of_XCropProperty_6(),
	UIImageCrop_t41034629::get_offset_of_YCropProperty_7(),
	UIImageCrop_t41034629::get_offset_of_XCrop_8(),
	UIImageCrop_t41034629::get_offset_of_YCrop_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4683 = { sizeof (UILinearDodgeEffect_t3706909257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4683[1] = 
{
	UILinearDodgeEffect_t3706909257::get_offset_of_mGraphic_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4684 = { sizeof (UIMultiplyEffect_t3837490265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4684[1] = 
{
	UIMultiplyEffect_t3837490265::get_offset_of_mGraphic_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4685 = { sizeof (UIScreenEffect_t1657282205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4685[1] = 
{
	UIScreenEffect_t1657282205::get_offset_of_mGraphic_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4686 = { sizeof (UISoftAdditiveEffect_t1842650896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4686[1] = 
{
	UISoftAdditiveEffect_t1842650896::get_offset_of_mGraphic_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4687 = { sizeof (ShineEffect_t3679628888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4687[2] = 
{
	ShineEffect_t3679628888::get_offset_of_yoffset_30(),
	ShineEffect_t3679628888::get_offset_of_width_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4688 = { sizeof (ShineEffector_t3463369172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4688[5] = 
{
	ShineEffector_t3463369172::get_offset_of_effector_4(),
	ShineEffector_t3463369172::get_offset_of_effectRoot_5(),
	ShineEffector_t3463369172::get_offset_of_yOffset_6(),
	ShineEffector_t3463369172::get_offset_of_width_7(),
	ShineEffector_t3463369172::get_offset_of_effectorRect_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4689 = { sizeof (SoftMaskScript_t2195956899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4689[13] = 
{
	SoftMaskScript_t2195956899::get_offset_of_mat_4(),
	SoftMaskScript_t2195956899::get_offset_of_cachedCanvas_5(),
	SoftMaskScript_t2195956899::get_offset_of_cachedCanvasTransform_6(),
	SoftMaskScript_t2195956899::get_offset_of_m_WorldCorners_7(),
	SoftMaskScript_t2195956899::get_offset_of_m_CanvasCorners_8(),
	SoftMaskScript_t2195956899::get_offset_of_MaskArea_9(),
	SoftMaskScript_t2195956899::get_offset_of_AlphaMask_10(),
	SoftMaskScript_t2195956899::get_offset_of_CutOff_11(),
	SoftMaskScript_t2195956899::get_offset_of_HardBlend_12(),
	SoftMaskScript_t2195956899::get_offset_of_FlipAlphaMask_13(),
	SoftMaskScript_t2195956899::get_offset_of_DontClipMaskScalingRect_14(),
	SoftMaskScript_t2195956899::get_offset_of_maskOffset_15(),
	SoftMaskScript_t2195956899::get_offset_of_maskScale_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4690 = { sizeof (UIFlippable_t826138922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4690[2] = 
{
	UIFlippable_t826138922::get_offset_of_m_Horizontal_5(),
	UIFlippable_t826138922::get_offset_of_m_Veritical_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4691 = { sizeof (UIParticleSystem_t471890981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4691[13] = 
{
	UIParticleSystem_t471890981::get_offset_of_fixedTime_30(),
	UIParticleSystem_t471890981::get_offset_of__transform_31(),
	UIParticleSystem_t471890981::get_offset_of_pSystem_32(),
	UIParticleSystem_t471890981::get_offset_of_particles_33(),
	UIParticleSystem_t471890981::get_offset_of__quad_34(),
	UIParticleSystem_t471890981::get_offset_of_imageUV_35(),
	UIParticleSystem_t471890981::get_offset_of_textureSheetAnimation_36(),
	UIParticleSystem_t471890981::get_offset_of_textureSheetAnimationFrames_37(),
	UIParticleSystem_t471890981::get_offset_of_textureSheetAnimationFrameSize_38(),
	UIParticleSystem_t471890981::get_offset_of_pRenderer_39(),
	UIParticleSystem_t471890981::get_offset_of_currentMaterial_40(),
	UIParticleSystem_t471890981::get_offset_of_currentTexture_41(),
	UIParticleSystem_t471890981::get_offset_of_mainModule_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4692 = { sizeof (AimerInputModule_t3344721356), -1, sizeof(AimerInputModule_t3344721356_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4692[3] = 
{
	AimerInputModule_t3344721356::get_offset_of_activateAxis_16(),
	AimerInputModule_t3344721356::get_offset_of_aimerOffset_17(),
	AimerInputModule_t3344721356_StaticFields::get_offset_of_objectUnderAimer_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4693 = { sizeof (GamePadInputModule_t3186657611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4693[9] = 
{
	GamePadInputModule_t3186657611::get_offset_of_m_PrevActionTime_10(),
	GamePadInputModule_t3186657611::get_offset_of_m_LastMoveVector_11(),
	GamePadInputModule_t3186657611::get_offset_of_m_ConsecutiveMoveCount_12(),
	GamePadInputModule_t3186657611::get_offset_of_m_HorizontalAxis_13(),
	GamePadInputModule_t3186657611::get_offset_of_m_VerticalAxis_14(),
	GamePadInputModule_t3186657611::get_offset_of_m_SubmitButton_15(),
	GamePadInputModule_t3186657611::get_offset_of_m_CancelButton_16(),
	GamePadInputModule_t3186657611::get_offset_of_m_InputActionsPerSecond_17(),
	GamePadInputModule_t3186657611::get_offset_of_m_RepeatDelay_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4694 = { sizeof (CurvedLayout_t3964158924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4694[4] = 
{
	CurvedLayout_t3964158924::get_offset_of_CurveOffset_12(),
	CurvedLayout_t3964158924::get_offset_of_itemAxis_13(),
	CurvedLayout_t3964158924::get_offset_of_itemSize_14(),
	CurvedLayout_t3964158924::get_offset_of_centerpoint_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4695 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4695[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4696 = { sizeof (FancyScrollViewNullContext_t3783020080), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4697 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4698 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4698[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4699 = { 0, 0, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
