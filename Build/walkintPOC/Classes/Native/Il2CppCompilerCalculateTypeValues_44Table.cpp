﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Coffee.UIExtensions.SoftMask
struct SoftMask_t1817791576;
// GetPointer
struct GetPointer_t3074287824;
// InstantiateObject
struct InstantiateObject_t3169048492;
// InstantiateObject/SerializablePreviewProperties
struct SerializablePreviewProperties_t1656375491;
// PlaneManager
struct PlaneManager_t2021199913;
// ProductPage
struct ProductPage_t3134713323;
// ScrollRectEx
struct ScrollRectEx_t4149042633;
// ScrollSnapEx/PageSnapChange
struct PageSnapChange_t2018612193;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AssetBundle>
struct Dictionary_2_t939163551;
// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>
struct List_1_t3289866318;
// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>[]
struct List_1U5BU5D_t521977531;
// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMaskable>
struct List_1_t1825180314;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<TMPro.KerningPair>
struct List_1_t3742930331;
// System.Collections.Generic.List`1<TMPro.TMP_Text>
struct List_1_t4071693616;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t881764471;
// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>
struct List_1_t3593973608;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Func`2<TMPro.KerningPair,System.UInt32>
struct Func_2_t2163425431;
// System.Func`2<Vuforia.ModelTargetBehaviour,System.Boolean>
struct Func_2_t1155892566;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Predicate`1<Coffee.UIExtensions.SoftMask>
struct Predicate_1_t2643085700;
// System.Predicate`1<UnityEngine.GameObject>
struct Predicate_1_t1938930743;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// TMPro.FastAction
struct FastAction_t3491443480;
// TMPro.FastAction`1<System.Boolean>
struct FastAction_1_t3758825850;
// TMPro.FastAction`1<TMPro.TMP_ColorGradient>
struct FastAction_1_t3044626357;
// TMPro.FastAction`1<UnityEngine.Object>
struct FastAction_1_t4292545838;
// TMPro.FastAction`2<System.Boolean,TMPro.TMP_FontAsset>
struct FastAction_2_t2230967576;
// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshPro>
struct FastAction_2_t4260179116;
// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshProUGUI>
struct FastAction_2_t2395899227;
// TMPro.FastAction`2<System.Boolean,UnityEngine.Material>
struct FastAction_2_t2206961073;
// TMPro.FastAction`2<System.Boolean,UnityEngine.Object>
struct FastAction_2_t2497593903;
// TMPro.FastAction`2<System.Object,TMPro.Compute_DT_EventArgs>
struct FastAction_2_t2884460249;
// TMPro.FastAction`3<UnityEngine.GameObject,UnityEngine.Material,UnityEngine.Material>
struct FastAction_3_t58649212;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t648826345;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t1930184704;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_t3678055768;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t2496920137;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// TMPro.TMP_Glyph
struct TMP_Glyph_t581847833;
// TMPro.TMP_LineInfo[]
struct TMP_LineInfoU5BU5D_t4120149533;
// TMPro.TMP_LinkInfo[]
struct TMP_LinkInfoU5BU5D_t3558768157;
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t3365986247;
// TMPro.TMP_PageInfo[]
struct TMP_PageInfoU5BU5D_t2463031060;
// TMPro.TMP_SpriteAnimator
struct TMP_SpriteAnimator_t2836635477;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t484820633;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// TMPro.TMP_TextElement
struct TMP_TextElement_t129727469;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t3598145122;
// TMPro.TMP_WordInfo[]
struct TMP_WordInfoU5BU5D_t3766301798;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t3552942253;
// TMPro.XML_TagAttribute[]
struct XML_TagAttributeU5BU5D_t284240280;
// TouchHandler
struct TouchHandler_t3441426771;
// UIController
struct UIController_t2237998930;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.BoxCollider
struct BoxCollider_t1640800422;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t3309123499;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3213117958;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// UnityEngine.RectOffset
struct RectOffset_t1369453676;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t2206337031;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Touch[]
struct TouchU5BU5D_t1849554061;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t1785403678;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.ScrollRect/ScrollRectEvent
struct ScrollRectEvent_t343079324;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Toggle[]
struct ToggleU5BU5D_t2531460392;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.WWW
struct WWW_t3688466362;
// Vuforia.ModelRecoBehaviour
struct ModelRecoBehaviour_t2312633019;
// Vuforia.ModelTargetBehaviour
struct ModelTargetBehaviour_t712978329;
// Vuforia.TargetFinder
struct TargetFinder_t2439332195;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1113559212;




#ifndef U3CMODULEU3E_T692745565_H
#define U3CMODULEU3E_T692745565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745565 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745565_H
#ifndef U3CMODULEU3E_T692745564_H
#define U3CMODULEU3E_T692745564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745564 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745564_H
#ifndef U3CMODULEU3E_T692745563_H
#define U3CMODULEU3E_T692745563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745563 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745563_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CLOADOBJECTU3EC__ITERATOR0_T3760357558_H
#define U3CLOADOBJECTU3EC__ITERATOR0_T3760357558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstantiateObject/<LoadObject>c__Iterator0
struct  U3CLoadObjectU3Ec__Iterator0_t3760357558  : public RuntimeObject
{
public:
	// System.String InstantiateObject/<LoadObject>c__Iterator0::ObjectLink
	String_t* ___ObjectLink_0;
	// UnityEngine.WWW InstantiateObject/<LoadObject>c__Iterator0::<ObjectRequest>__0
	WWW_t3688466362 * ___U3CObjectRequestU3E__0_1;
	// System.Boolean InstantiateObject/<LoadObject>c__Iterator0::ARmode
	bool ___ARmode_2;
	// InstantiateObject InstantiateObject/<LoadObject>c__Iterator0::$this
	InstantiateObject_t3169048492 * ___U24this_3;
	// System.Object InstantiateObject/<LoadObject>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean InstantiateObject/<LoadObject>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 InstantiateObject/<LoadObject>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_ObjectLink_0() { return static_cast<int32_t>(offsetof(U3CLoadObjectU3Ec__Iterator0_t3760357558, ___ObjectLink_0)); }
	inline String_t* get_ObjectLink_0() const { return ___ObjectLink_0; }
	inline String_t** get_address_of_ObjectLink_0() { return &___ObjectLink_0; }
	inline void set_ObjectLink_0(String_t* value)
	{
		___ObjectLink_0 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectLink_0), value);
	}

	inline static int32_t get_offset_of_U3CObjectRequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadObjectU3Ec__Iterator0_t3760357558, ___U3CObjectRequestU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CObjectRequestU3E__0_1() const { return ___U3CObjectRequestU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CObjectRequestU3E__0_1() { return &___U3CObjectRequestU3E__0_1; }
	inline void set_U3CObjectRequestU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CObjectRequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjectRequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_ARmode_2() { return static_cast<int32_t>(offsetof(U3CLoadObjectU3Ec__Iterator0_t3760357558, ___ARmode_2)); }
	inline bool get_ARmode_2() const { return ___ARmode_2; }
	inline bool* get_address_of_ARmode_2() { return &___ARmode_2; }
	inline void set_ARmode_2(bool value)
	{
		___ARmode_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CLoadObjectU3Ec__Iterator0_t3760357558, ___U24this_3)); }
	inline InstantiateObject_t3169048492 * get_U24this_3() const { return ___U24this_3; }
	inline InstantiateObject_t3169048492 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(InstantiateObject_t3169048492 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CLoadObjectU3Ec__Iterator0_t3760357558, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CLoadObjectU3Ec__Iterator0_t3760357558, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CLoadObjectU3Ec__Iterator0_t3760357558, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADOBJECTU3EC__ITERATOR0_T3760357558_H
#ifndef U3CLOADVERSIONU3EC__ITERATOR1_T1405279486_H
#define U3CLOADVERSIONU3EC__ITERATOR1_T1405279486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstantiateObject/<LoadVersion>c__Iterator1
struct  U3CLoadVersionU3Ec__Iterator1_t1405279486  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest InstantiateObject/<LoadVersion>c__Iterator1::<GetAssetConfigurationRequest>__0
	UnityWebRequest_t463507806 * ___U3CGetAssetConfigurationRequestU3E__0_0;
	// InstantiateObject InstantiateObject/<LoadVersion>c__Iterator1::$this
	InstantiateObject_t3169048492 * ___U24this_1;
	// System.Object InstantiateObject/<LoadVersion>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean InstantiateObject/<LoadVersion>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 InstantiateObject/<LoadVersion>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CGetAssetConfigurationRequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadVersionU3Ec__Iterator1_t1405279486, ___U3CGetAssetConfigurationRequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CGetAssetConfigurationRequestU3E__0_0() const { return ___U3CGetAssetConfigurationRequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CGetAssetConfigurationRequestU3E__0_0() { return &___U3CGetAssetConfigurationRequestU3E__0_0; }
	inline void set_U3CGetAssetConfigurationRequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CGetAssetConfigurationRequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetAssetConfigurationRequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CLoadVersionU3Ec__Iterator1_t1405279486, ___U24this_1)); }
	inline InstantiateObject_t3169048492 * get_U24this_1() const { return ___U24this_1; }
	inline InstantiateObject_t3169048492 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(InstantiateObject_t3169048492 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CLoadVersionU3Ec__Iterator1_t1405279486, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CLoadVersionU3Ec__Iterator1_t1405279486, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CLoadVersionU3Ec__Iterator1_t1405279486, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADVERSIONU3EC__ITERATOR1_T1405279486_H
#ifndef SERIALIZABLEPREVIEWPROPERTIES_T1656375491_H
#define SERIALIZABLEPREVIEWPROPERTIES_T1656375491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstantiateObject/SerializablePreviewProperties
struct  SerializablePreviewProperties_t1656375491  : public RuntimeObject
{
public:
	// System.Int32 InstantiateObject/SerializablePreviewProperties::Version
	int32_t ___Version_0;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(SerializablePreviewProperties_t1656375491, ___Version_0)); }
	inline int32_t get_Version_0() const { return ___Version_0; }
	inline int32_t* get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(int32_t value)
	{
		___Version_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEPREVIEWPROPERTIES_T1656375491_H
#ifndef U3CINVOKEBUTTONU3EC__ITERATOR0_T2072859715_H
#define U3CINVOKEBUTTONU3EC__ITERATOR0_T2072859715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProductPage/<InvokeButton>c__Iterator0
struct  U3CInvokeButtonU3Ec__Iterator0_t2072859715  : public RuntimeObject
{
public:
	// ProductPage ProductPage/<InvokeButton>c__Iterator0::$this
	ProductPage_t3134713323 * ___U24this_0;
	// System.Object ProductPage/<InvokeButton>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ProductPage/<InvokeButton>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 ProductPage/<InvokeButton>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CInvokeButtonU3Ec__Iterator0_t2072859715, ___U24this_0)); }
	inline ProductPage_t3134713323 * get_U24this_0() const { return ___U24this_0; }
	inline ProductPage_t3134713323 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ProductPage_t3134713323 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CInvokeButtonU3Ec__Iterator0_t2072859715, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CInvokeButtonU3Ec__Iterator0_t2072859715, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CInvokeButtonU3Ec__Iterator0_t2072859715, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINVOKEBUTTONU3EC__ITERATOR0_T2072859715_H
#ifndef U3CONBEGINDRAGU3EC__ANONSTOREY2_T3997952331_H
#define U3CONBEGINDRAGU3EC__ANONSTOREY2_T3997952331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollRectEx/<OnBeginDrag>c__AnonStorey2
struct  U3COnBeginDragU3Ec__AnonStorey2_t3997952331  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData ScrollRectEx/<OnBeginDrag>c__AnonStorey2::eventData
	PointerEventData_t3807901092 * ___eventData_0;

public:
	inline static int32_t get_offset_of_eventData_0() { return static_cast<int32_t>(offsetof(U3COnBeginDragU3Ec__AnonStorey2_t3997952331, ___eventData_0)); }
	inline PointerEventData_t3807901092 * get_eventData_0() const { return ___eventData_0; }
	inline PointerEventData_t3807901092 ** get_address_of_eventData_0() { return &___eventData_0; }
	inline void set_eventData_0(PointerEventData_t3807901092 * value)
	{
		___eventData_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONBEGINDRAGU3EC__ANONSTOREY2_T3997952331_H
#ifndef U3CONDRAGU3EC__ANONSTOREY1_T3609824325_H
#define U3CONDRAGU3EC__ANONSTOREY1_T3609824325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollRectEx/<OnDrag>c__AnonStorey1
struct  U3COnDragU3Ec__AnonStorey1_t3609824325  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData ScrollRectEx/<OnDrag>c__AnonStorey1::eventData
	PointerEventData_t3807901092 * ___eventData_0;

public:
	inline static int32_t get_offset_of_eventData_0() { return static_cast<int32_t>(offsetof(U3COnDragU3Ec__AnonStorey1_t3609824325, ___eventData_0)); }
	inline PointerEventData_t3807901092 * get_eventData_0() const { return ___eventData_0; }
	inline PointerEventData_t3807901092 ** get_address_of_eventData_0() { return &___eventData_0; }
	inline void set_eventData_0(PointerEventData_t3807901092 * value)
	{
		___eventData_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONDRAGU3EC__ANONSTOREY1_T3609824325_H
#ifndef U3CONENDDRAGU3EC__ANONSTOREY3_T36866175_H
#define U3CONENDDRAGU3EC__ANONSTOREY3_T36866175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollRectEx/<OnEndDrag>c__AnonStorey3
struct  U3COnEndDragU3Ec__AnonStorey3_t36866175  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData ScrollRectEx/<OnEndDrag>c__AnonStorey3::eventData
	PointerEventData_t3807901092 * ___eventData_0;

public:
	inline static int32_t get_offset_of_eventData_0() { return static_cast<int32_t>(offsetof(U3COnEndDragU3Ec__AnonStorey3_t36866175, ___eventData_0)); }
	inline PointerEventData_t3807901092 * get_eventData_0() const { return ___eventData_0; }
	inline PointerEventData_t3807901092 ** get_address_of_eventData_0() { return &___eventData_0; }
	inline void set_eventData_0(PointerEventData_t3807901092 * value)
	{
		___eventData_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONENDDRAGU3EC__ANONSTOREY3_T36866175_H
#ifndef U3CONINITIALIZEPOTENTIALDRAGU3EC__ANONSTOREY0_T250711124_H
#define U3CONINITIALIZEPOTENTIALDRAGU3EC__ANONSTOREY0_T250711124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollRectEx/<OnInitializePotentialDrag>c__AnonStorey0
struct  U3COnInitializePotentialDragU3Ec__AnonStorey0_t250711124  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData ScrollRectEx/<OnInitializePotentialDrag>c__AnonStorey0::eventData
	PointerEventData_t3807901092 * ___eventData_0;

public:
	inline static int32_t get_offset_of_eventData_0() { return static_cast<int32_t>(offsetof(U3COnInitializePotentialDragU3Ec__AnonStorey0_t250711124, ___eventData_0)); }
	inline PointerEventData_t3807901092 * get_eventData_0() const { return ___eventData_0; }
	inline PointerEventData_t3807901092 ** get_address_of_eventData_0() { return &___eventData_0; }
	inline void set_eventData_0(PointerEventData_t3807901092 * value)
	{
		___eventData_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONINITIALIZEPOTENTIALDRAGU3EC__ANONSTOREY0_T250711124_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef FACEINFO_T2243299176_H
#define FACEINFO_T2243299176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FaceInfo
struct  FaceInfo_t2243299176  : public RuntimeObject
{
public:
	// System.String TMPro.FaceInfo::Name
	String_t* ___Name_0;
	// System.Single TMPro.FaceInfo::PointSize
	float ___PointSize_1;
	// System.Single TMPro.FaceInfo::Scale
	float ___Scale_2;
	// System.Int32 TMPro.FaceInfo::CharacterCount
	int32_t ___CharacterCount_3;
	// System.Single TMPro.FaceInfo::LineHeight
	float ___LineHeight_4;
	// System.Single TMPro.FaceInfo::Baseline
	float ___Baseline_5;
	// System.Single TMPro.FaceInfo::Ascender
	float ___Ascender_6;
	// System.Single TMPro.FaceInfo::CapHeight
	float ___CapHeight_7;
	// System.Single TMPro.FaceInfo::Descender
	float ___Descender_8;
	// System.Single TMPro.FaceInfo::CenterLine
	float ___CenterLine_9;
	// System.Single TMPro.FaceInfo::SuperscriptOffset
	float ___SuperscriptOffset_10;
	// System.Single TMPro.FaceInfo::SubscriptOffset
	float ___SubscriptOffset_11;
	// System.Single TMPro.FaceInfo::SubSize
	float ___SubSize_12;
	// System.Single TMPro.FaceInfo::Underline
	float ___Underline_13;
	// System.Single TMPro.FaceInfo::UnderlineThickness
	float ___UnderlineThickness_14;
	// System.Single TMPro.FaceInfo::strikethrough
	float ___strikethrough_15;
	// System.Single TMPro.FaceInfo::strikethroughThickness
	float ___strikethroughThickness_16;
	// System.Single TMPro.FaceInfo::TabWidth
	float ___TabWidth_17;
	// System.Single TMPro.FaceInfo::Padding
	float ___Padding_18;
	// System.Single TMPro.FaceInfo::AtlasWidth
	float ___AtlasWidth_19;
	// System.Single TMPro.FaceInfo::AtlasHeight
	float ___AtlasHeight_20;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_PointSize_1() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___PointSize_1)); }
	inline float get_PointSize_1() const { return ___PointSize_1; }
	inline float* get_address_of_PointSize_1() { return &___PointSize_1; }
	inline void set_PointSize_1(float value)
	{
		___PointSize_1 = value;
	}

	inline static int32_t get_offset_of_Scale_2() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Scale_2)); }
	inline float get_Scale_2() const { return ___Scale_2; }
	inline float* get_address_of_Scale_2() { return &___Scale_2; }
	inline void set_Scale_2(float value)
	{
		___Scale_2 = value;
	}

	inline static int32_t get_offset_of_CharacterCount_3() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___CharacterCount_3)); }
	inline int32_t get_CharacterCount_3() const { return ___CharacterCount_3; }
	inline int32_t* get_address_of_CharacterCount_3() { return &___CharacterCount_3; }
	inline void set_CharacterCount_3(int32_t value)
	{
		___CharacterCount_3 = value;
	}

	inline static int32_t get_offset_of_LineHeight_4() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___LineHeight_4)); }
	inline float get_LineHeight_4() const { return ___LineHeight_4; }
	inline float* get_address_of_LineHeight_4() { return &___LineHeight_4; }
	inline void set_LineHeight_4(float value)
	{
		___LineHeight_4 = value;
	}

	inline static int32_t get_offset_of_Baseline_5() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Baseline_5)); }
	inline float get_Baseline_5() const { return ___Baseline_5; }
	inline float* get_address_of_Baseline_5() { return &___Baseline_5; }
	inline void set_Baseline_5(float value)
	{
		___Baseline_5 = value;
	}

	inline static int32_t get_offset_of_Ascender_6() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Ascender_6)); }
	inline float get_Ascender_6() const { return ___Ascender_6; }
	inline float* get_address_of_Ascender_6() { return &___Ascender_6; }
	inline void set_Ascender_6(float value)
	{
		___Ascender_6 = value;
	}

	inline static int32_t get_offset_of_CapHeight_7() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___CapHeight_7)); }
	inline float get_CapHeight_7() const { return ___CapHeight_7; }
	inline float* get_address_of_CapHeight_7() { return &___CapHeight_7; }
	inline void set_CapHeight_7(float value)
	{
		___CapHeight_7 = value;
	}

	inline static int32_t get_offset_of_Descender_8() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Descender_8)); }
	inline float get_Descender_8() const { return ___Descender_8; }
	inline float* get_address_of_Descender_8() { return &___Descender_8; }
	inline void set_Descender_8(float value)
	{
		___Descender_8 = value;
	}

	inline static int32_t get_offset_of_CenterLine_9() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___CenterLine_9)); }
	inline float get_CenterLine_9() const { return ___CenterLine_9; }
	inline float* get_address_of_CenterLine_9() { return &___CenterLine_9; }
	inline void set_CenterLine_9(float value)
	{
		___CenterLine_9 = value;
	}

	inline static int32_t get_offset_of_SuperscriptOffset_10() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___SuperscriptOffset_10)); }
	inline float get_SuperscriptOffset_10() const { return ___SuperscriptOffset_10; }
	inline float* get_address_of_SuperscriptOffset_10() { return &___SuperscriptOffset_10; }
	inline void set_SuperscriptOffset_10(float value)
	{
		___SuperscriptOffset_10 = value;
	}

	inline static int32_t get_offset_of_SubscriptOffset_11() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___SubscriptOffset_11)); }
	inline float get_SubscriptOffset_11() const { return ___SubscriptOffset_11; }
	inline float* get_address_of_SubscriptOffset_11() { return &___SubscriptOffset_11; }
	inline void set_SubscriptOffset_11(float value)
	{
		___SubscriptOffset_11 = value;
	}

	inline static int32_t get_offset_of_SubSize_12() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___SubSize_12)); }
	inline float get_SubSize_12() const { return ___SubSize_12; }
	inline float* get_address_of_SubSize_12() { return &___SubSize_12; }
	inline void set_SubSize_12(float value)
	{
		___SubSize_12 = value;
	}

	inline static int32_t get_offset_of_Underline_13() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Underline_13)); }
	inline float get_Underline_13() const { return ___Underline_13; }
	inline float* get_address_of_Underline_13() { return &___Underline_13; }
	inline void set_Underline_13(float value)
	{
		___Underline_13 = value;
	}

	inline static int32_t get_offset_of_UnderlineThickness_14() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___UnderlineThickness_14)); }
	inline float get_UnderlineThickness_14() const { return ___UnderlineThickness_14; }
	inline float* get_address_of_UnderlineThickness_14() { return &___UnderlineThickness_14; }
	inline void set_UnderlineThickness_14(float value)
	{
		___UnderlineThickness_14 = value;
	}

	inline static int32_t get_offset_of_strikethrough_15() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___strikethrough_15)); }
	inline float get_strikethrough_15() const { return ___strikethrough_15; }
	inline float* get_address_of_strikethrough_15() { return &___strikethrough_15; }
	inline void set_strikethrough_15(float value)
	{
		___strikethrough_15 = value;
	}

	inline static int32_t get_offset_of_strikethroughThickness_16() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___strikethroughThickness_16)); }
	inline float get_strikethroughThickness_16() const { return ___strikethroughThickness_16; }
	inline float* get_address_of_strikethroughThickness_16() { return &___strikethroughThickness_16; }
	inline void set_strikethroughThickness_16(float value)
	{
		___strikethroughThickness_16 = value;
	}

	inline static int32_t get_offset_of_TabWidth_17() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___TabWidth_17)); }
	inline float get_TabWidth_17() const { return ___TabWidth_17; }
	inline float* get_address_of_TabWidth_17() { return &___TabWidth_17; }
	inline void set_TabWidth_17(float value)
	{
		___TabWidth_17 = value;
	}

	inline static int32_t get_offset_of_Padding_18() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Padding_18)); }
	inline float get_Padding_18() const { return ___Padding_18; }
	inline float* get_address_of_Padding_18() { return &___Padding_18; }
	inline void set_Padding_18(float value)
	{
		___Padding_18 = value;
	}

	inline static int32_t get_offset_of_AtlasWidth_19() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___AtlasWidth_19)); }
	inline float get_AtlasWidth_19() const { return ___AtlasWidth_19; }
	inline float* get_address_of_AtlasWidth_19() { return &___AtlasWidth_19; }
	inline void set_AtlasWidth_19(float value)
	{
		___AtlasWidth_19 = value;
	}

	inline static int32_t get_offset_of_AtlasHeight_20() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___AtlasHeight_20)); }
	inline float get_AtlasHeight_20() const { return ___AtlasHeight_20; }
	inline float* get_address_of_AtlasHeight_20() { return &___AtlasHeight_20; }
	inline void set_AtlasHeight_20(float value)
	{
		___AtlasHeight_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEINFO_T2243299176_H
#ifndef KERNINGTABLE_T2322366871_H
#define KERNINGTABLE_T2322366871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable
struct  KerningTable_t2322366871  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.KerningPair> TMPro.KerningTable::kerningPairs
	List_1_t3742930331 * ___kerningPairs_0;

public:
	inline static int32_t get_offset_of_kerningPairs_0() { return static_cast<int32_t>(offsetof(KerningTable_t2322366871, ___kerningPairs_0)); }
	inline List_1_t3742930331 * get_kerningPairs_0() const { return ___kerningPairs_0; }
	inline List_1_t3742930331 ** get_address_of_kerningPairs_0() { return &___kerningPairs_0; }
	inline void set_kerningPairs_0(List_1_t3742930331 * value)
	{
		___kerningPairs_0 = value;
		Il2CppCodeGenWriteBarrier((&___kerningPairs_0), value);
	}
};

struct KerningTable_t2322366871_StaticFields
{
public:
	// System.Func`2<TMPro.KerningPair,System.UInt32> TMPro.KerningTable::<>f__am$cache0
	Func_2_t2163425431 * ___U3CU3Ef__amU24cache0_1;
	// System.Func`2<TMPro.KerningPair,System.UInt32> TMPro.KerningTable::<>f__am$cache1
	Func_2_t2163425431 * ___U3CU3Ef__amU24cache1_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(KerningTable_t2322366871_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Func_2_t2163425431 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Func_2_t2163425431 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Func_2_t2163425431 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_2() { return static_cast<int32_t>(offsetof(KerningTable_t2322366871_StaticFields, ___U3CU3Ef__amU24cache1_2)); }
	inline Func_2_t2163425431 * get_U3CU3Ef__amU24cache1_2() const { return ___U3CU3Ef__amU24cache1_2; }
	inline Func_2_t2163425431 ** get_address_of_U3CU3Ef__amU24cache1_2() { return &___U3CU3Ef__amU24cache1_2; }
	inline void set_U3CU3Ef__amU24cache1_2(Func_2_t2163425431 * value)
	{
		___U3CU3Ef__amU24cache1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGTABLE_T2322366871_H
#ifndef U3CADDGLYPHPAIRADJUSTMENTRECORDU3EC__ANONSTOREY1_T3257710262_H
#define U3CADDGLYPHPAIRADJUSTMENTRECORDU3EC__ANONSTOREY1_T3257710262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<AddGlyphPairAdjustmentRecord>c__AnonStorey1
struct  U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262  : public RuntimeObject
{
public:
	// System.UInt32 TMPro.KerningTable/<AddGlyphPairAdjustmentRecord>c__AnonStorey1::first
	uint32_t ___first_0;
	// System.UInt32 TMPro.KerningTable/<AddGlyphPairAdjustmentRecord>c__AnonStorey1::second
	uint32_t ___second_1;

public:
	inline static int32_t get_offset_of_first_0() { return static_cast<int32_t>(offsetof(U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262, ___first_0)); }
	inline uint32_t get_first_0() const { return ___first_0; }
	inline uint32_t* get_address_of_first_0() { return &___first_0; }
	inline void set_first_0(uint32_t value)
	{
		___first_0 = value;
	}

	inline static int32_t get_offset_of_second_1() { return static_cast<int32_t>(offsetof(U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262, ___second_1)); }
	inline uint32_t get_second_1() const { return ___second_1; }
	inline uint32_t* get_address_of_second_1() { return &___second_1; }
	inline void set_second_1(uint32_t value)
	{
		___second_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDGLYPHPAIRADJUSTMENTRECORDU3EC__ANONSTOREY1_T3257710262_H
#ifndef U3CADDKERNINGPAIRU3EC__ANONSTOREY0_T2688361982_H
#define U3CADDKERNINGPAIRU3EC__ANONSTOREY0_T2688361982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<AddKerningPair>c__AnonStorey0
struct  U3CAddKerningPairU3Ec__AnonStorey0_t2688361982  : public RuntimeObject
{
public:
	// System.UInt32 TMPro.KerningTable/<AddKerningPair>c__AnonStorey0::first
	uint32_t ___first_0;
	// System.UInt32 TMPro.KerningTable/<AddKerningPair>c__AnonStorey0::second
	uint32_t ___second_1;

public:
	inline static int32_t get_offset_of_first_0() { return static_cast<int32_t>(offsetof(U3CAddKerningPairU3Ec__AnonStorey0_t2688361982, ___first_0)); }
	inline uint32_t get_first_0() const { return ___first_0; }
	inline uint32_t* get_address_of_first_0() { return &___first_0; }
	inline void set_first_0(uint32_t value)
	{
		___first_0 = value;
	}

	inline static int32_t get_offset_of_second_1() { return static_cast<int32_t>(offsetof(U3CAddKerningPairU3Ec__AnonStorey0_t2688361982, ___second_1)); }
	inline uint32_t get_second_1() const { return ___second_1; }
	inline uint32_t* get_address_of_second_1() { return &___second_1; }
	inline void set_second_1(uint32_t value)
	{
		___second_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDKERNINGPAIRU3EC__ANONSTOREY0_T2688361982_H
#ifndef U3CREMOVEKERNINGPAIRU3EC__ANONSTOREY2_T1355166074_H
#define U3CREMOVEKERNINGPAIRU3EC__ANONSTOREY2_T1355166074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey2
struct  U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074  : public RuntimeObject
{
public:
	// System.Int32 TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey2::left
	int32_t ___left_0;
	// System.Int32 TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey2::right
	int32_t ___right_1;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074, ___left_0)); }
	inline int32_t get_left_0() const { return ___left_0; }
	inline int32_t* get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(int32_t value)
	{
		___left_0 = value;
	}

	inline static int32_t get_offset_of_right_1() { return static_cast<int32_t>(offsetof(U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074, ___right_1)); }
	inline int32_t get_right_1() const { return ___right_1; }
	inline int32_t* get_address_of_right_1() { return &___right_1; }
	inline void set_right_1(int32_t value)
	{
		___right_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEKERNINGPAIRU3EC__ANONSTOREY2_T1355166074_H
#ifndef SHADERUTILITIES_T714255158_H
#define SHADERUTILITIES_T714255158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ShaderUtilities
struct  ShaderUtilities_t714255158  : public RuntimeObject
{
public:

public:
};

struct ShaderUtilities_t714255158_StaticFields
{
public:
	// System.Int32 TMPro.ShaderUtilities::ID_MainTex
	int32_t ___ID_MainTex_0;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceTex
	int32_t ___ID_FaceTex_1;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceColor
	int32_t ___ID_FaceColor_2;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceDilate
	int32_t ___ID_FaceDilate_3;
	// System.Int32 TMPro.ShaderUtilities::ID_Shininess
	int32_t ___ID_Shininess_4;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayColor
	int32_t ___ID_UnderlayColor_5;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetX
	int32_t ___ID_UnderlayOffsetX_6;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetY
	int32_t ___ID_UnderlayOffsetY_7;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayDilate
	int32_t ___ID_UnderlayDilate_8;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlaySoftness
	int32_t ___ID_UnderlaySoftness_9;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightNormal
	int32_t ___ID_WeightNormal_10;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightBold
	int32_t ___ID_WeightBold_11;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineTex
	int32_t ___ID_OutlineTex_12;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineWidth
	int32_t ___ID_OutlineWidth_13;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineSoftness
	int32_t ___ID_OutlineSoftness_14;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineColor
	int32_t ___ID_OutlineColor_15;
	// System.Int32 TMPro.ShaderUtilities::ID_Padding
	int32_t ___ID_Padding_16;
	// System.Int32 TMPro.ShaderUtilities::ID_GradientScale
	int32_t ___ID_GradientScale_17;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleX
	int32_t ___ID_ScaleX_18;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleY
	int32_t ___ID_ScaleY_19;
	// System.Int32 TMPro.ShaderUtilities::ID_PerspectiveFilter
	int32_t ___ID_PerspectiveFilter_20;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureWidth
	int32_t ___ID_TextureWidth_21;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureHeight
	int32_t ___ID_TextureHeight_22;
	// System.Int32 TMPro.ShaderUtilities::ID_BevelAmount
	int32_t ___ID_BevelAmount_23;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowColor
	int32_t ___ID_GlowColor_24;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOffset
	int32_t ___ID_GlowOffset_25;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowPower
	int32_t ___ID_GlowPower_26;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOuter
	int32_t ___ID_GlowOuter_27;
	// System.Int32 TMPro.ShaderUtilities::ID_LightAngle
	int32_t ___ID_LightAngle_28;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMap
	int32_t ___ID_EnvMap_29;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrix
	int32_t ___ID_EnvMatrix_30;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrixRotation
	int32_t ___ID_EnvMatrixRotation_31;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskCoord
	int32_t ___ID_MaskCoord_32;
	// System.Int32 TMPro.ShaderUtilities::ID_ClipRect
	int32_t ___ID_ClipRect_33;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessX
	int32_t ___ID_MaskSoftnessX_34;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessY
	int32_t ___ID_MaskSoftnessY_35;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetX
	int32_t ___ID_VertexOffsetX_36;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetY
	int32_t ___ID_VertexOffsetY_37;
	// System.Int32 TMPro.ShaderUtilities::ID_UseClipRect
	int32_t ___ID_UseClipRect_38;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilID
	int32_t ___ID_StencilID_39;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilOp
	int32_t ___ID_StencilOp_40;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilComp
	int32_t ___ID_StencilComp_41;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilReadMask
	int32_t ___ID_StencilReadMask_42;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilWriteMask
	int32_t ___ID_StencilWriteMask_43;
	// System.Int32 TMPro.ShaderUtilities::ID_ShaderFlags
	int32_t ___ID_ShaderFlags_44;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_A
	int32_t ___ID_ScaleRatio_A_45;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_B
	int32_t ___ID_ScaleRatio_B_46;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_C
	int32_t ___ID_ScaleRatio_C_47;
	// System.String TMPro.ShaderUtilities::Keyword_Bevel
	String_t* ___Keyword_Bevel_48;
	// System.String TMPro.ShaderUtilities::Keyword_Glow
	String_t* ___Keyword_Glow_49;
	// System.String TMPro.ShaderUtilities::Keyword_Underlay
	String_t* ___Keyword_Underlay_50;
	// System.String TMPro.ShaderUtilities::Keyword_Ratios
	String_t* ___Keyword_Ratios_51;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_SOFT
	String_t* ___Keyword_MASK_SOFT_52;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_HARD
	String_t* ___Keyword_MASK_HARD_53;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_TEX
	String_t* ___Keyword_MASK_TEX_54;
	// System.String TMPro.ShaderUtilities::Keyword_Outline
	String_t* ___Keyword_Outline_55;
	// System.String TMPro.ShaderUtilities::ShaderTag_ZTestMode
	String_t* ___ShaderTag_ZTestMode_56;
	// System.String TMPro.ShaderUtilities::ShaderTag_CullMode
	String_t* ___ShaderTag_CullMode_57;
	// System.Single TMPro.ShaderUtilities::m_clamp
	float ___m_clamp_58;
	// System.Boolean TMPro.ShaderUtilities::isInitialized
	bool ___isInitialized_59;

public:
	inline static int32_t get_offset_of_ID_MainTex_0() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MainTex_0)); }
	inline int32_t get_ID_MainTex_0() const { return ___ID_MainTex_0; }
	inline int32_t* get_address_of_ID_MainTex_0() { return &___ID_MainTex_0; }
	inline void set_ID_MainTex_0(int32_t value)
	{
		___ID_MainTex_0 = value;
	}

	inline static int32_t get_offset_of_ID_FaceTex_1() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_FaceTex_1)); }
	inline int32_t get_ID_FaceTex_1() const { return ___ID_FaceTex_1; }
	inline int32_t* get_address_of_ID_FaceTex_1() { return &___ID_FaceTex_1; }
	inline void set_ID_FaceTex_1(int32_t value)
	{
		___ID_FaceTex_1 = value;
	}

	inline static int32_t get_offset_of_ID_FaceColor_2() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_FaceColor_2)); }
	inline int32_t get_ID_FaceColor_2() const { return ___ID_FaceColor_2; }
	inline int32_t* get_address_of_ID_FaceColor_2() { return &___ID_FaceColor_2; }
	inline void set_ID_FaceColor_2(int32_t value)
	{
		___ID_FaceColor_2 = value;
	}

	inline static int32_t get_offset_of_ID_FaceDilate_3() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_FaceDilate_3)); }
	inline int32_t get_ID_FaceDilate_3() const { return ___ID_FaceDilate_3; }
	inline int32_t* get_address_of_ID_FaceDilate_3() { return &___ID_FaceDilate_3; }
	inline void set_ID_FaceDilate_3(int32_t value)
	{
		___ID_FaceDilate_3 = value;
	}

	inline static int32_t get_offset_of_ID_Shininess_4() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_Shininess_4)); }
	inline int32_t get_ID_Shininess_4() const { return ___ID_Shininess_4; }
	inline int32_t* get_address_of_ID_Shininess_4() { return &___ID_Shininess_4; }
	inline void set_ID_Shininess_4(int32_t value)
	{
		___ID_Shininess_4 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayColor_5() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayColor_5)); }
	inline int32_t get_ID_UnderlayColor_5() const { return ___ID_UnderlayColor_5; }
	inline int32_t* get_address_of_ID_UnderlayColor_5() { return &___ID_UnderlayColor_5; }
	inline void set_ID_UnderlayColor_5(int32_t value)
	{
		___ID_UnderlayColor_5 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetX_6() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayOffsetX_6)); }
	inline int32_t get_ID_UnderlayOffsetX_6() const { return ___ID_UnderlayOffsetX_6; }
	inline int32_t* get_address_of_ID_UnderlayOffsetX_6() { return &___ID_UnderlayOffsetX_6; }
	inline void set_ID_UnderlayOffsetX_6(int32_t value)
	{
		___ID_UnderlayOffsetX_6 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetY_7() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayOffsetY_7)); }
	inline int32_t get_ID_UnderlayOffsetY_7() const { return ___ID_UnderlayOffsetY_7; }
	inline int32_t* get_address_of_ID_UnderlayOffsetY_7() { return &___ID_UnderlayOffsetY_7; }
	inline void set_ID_UnderlayOffsetY_7(int32_t value)
	{
		___ID_UnderlayOffsetY_7 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayDilate_8() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayDilate_8)); }
	inline int32_t get_ID_UnderlayDilate_8() const { return ___ID_UnderlayDilate_8; }
	inline int32_t* get_address_of_ID_UnderlayDilate_8() { return &___ID_UnderlayDilate_8; }
	inline void set_ID_UnderlayDilate_8(int32_t value)
	{
		___ID_UnderlayDilate_8 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlaySoftness_9() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlaySoftness_9)); }
	inline int32_t get_ID_UnderlaySoftness_9() const { return ___ID_UnderlaySoftness_9; }
	inline int32_t* get_address_of_ID_UnderlaySoftness_9() { return &___ID_UnderlaySoftness_9; }
	inline void set_ID_UnderlaySoftness_9(int32_t value)
	{
		___ID_UnderlaySoftness_9 = value;
	}

	inline static int32_t get_offset_of_ID_WeightNormal_10() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_WeightNormal_10)); }
	inline int32_t get_ID_WeightNormal_10() const { return ___ID_WeightNormal_10; }
	inline int32_t* get_address_of_ID_WeightNormal_10() { return &___ID_WeightNormal_10; }
	inline void set_ID_WeightNormal_10(int32_t value)
	{
		___ID_WeightNormal_10 = value;
	}

	inline static int32_t get_offset_of_ID_WeightBold_11() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_WeightBold_11)); }
	inline int32_t get_ID_WeightBold_11() const { return ___ID_WeightBold_11; }
	inline int32_t* get_address_of_ID_WeightBold_11() { return &___ID_WeightBold_11; }
	inline void set_ID_WeightBold_11(int32_t value)
	{
		___ID_WeightBold_11 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineTex_12() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineTex_12)); }
	inline int32_t get_ID_OutlineTex_12() const { return ___ID_OutlineTex_12; }
	inline int32_t* get_address_of_ID_OutlineTex_12() { return &___ID_OutlineTex_12; }
	inline void set_ID_OutlineTex_12(int32_t value)
	{
		___ID_OutlineTex_12 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineWidth_13() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineWidth_13)); }
	inline int32_t get_ID_OutlineWidth_13() const { return ___ID_OutlineWidth_13; }
	inline int32_t* get_address_of_ID_OutlineWidth_13() { return &___ID_OutlineWidth_13; }
	inline void set_ID_OutlineWidth_13(int32_t value)
	{
		___ID_OutlineWidth_13 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineSoftness_14() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineSoftness_14)); }
	inline int32_t get_ID_OutlineSoftness_14() const { return ___ID_OutlineSoftness_14; }
	inline int32_t* get_address_of_ID_OutlineSoftness_14() { return &___ID_OutlineSoftness_14; }
	inline void set_ID_OutlineSoftness_14(int32_t value)
	{
		___ID_OutlineSoftness_14 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineColor_15() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineColor_15)); }
	inline int32_t get_ID_OutlineColor_15() const { return ___ID_OutlineColor_15; }
	inline int32_t* get_address_of_ID_OutlineColor_15() { return &___ID_OutlineColor_15; }
	inline void set_ID_OutlineColor_15(int32_t value)
	{
		___ID_OutlineColor_15 = value;
	}

	inline static int32_t get_offset_of_ID_Padding_16() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_Padding_16)); }
	inline int32_t get_ID_Padding_16() const { return ___ID_Padding_16; }
	inline int32_t* get_address_of_ID_Padding_16() { return &___ID_Padding_16; }
	inline void set_ID_Padding_16(int32_t value)
	{
		___ID_Padding_16 = value;
	}

	inline static int32_t get_offset_of_ID_GradientScale_17() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GradientScale_17)); }
	inline int32_t get_ID_GradientScale_17() const { return ___ID_GradientScale_17; }
	inline int32_t* get_address_of_ID_GradientScale_17() { return &___ID_GradientScale_17; }
	inline void set_ID_GradientScale_17(int32_t value)
	{
		___ID_GradientScale_17 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleX_18() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleX_18)); }
	inline int32_t get_ID_ScaleX_18() const { return ___ID_ScaleX_18; }
	inline int32_t* get_address_of_ID_ScaleX_18() { return &___ID_ScaleX_18; }
	inline void set_ID_ScaleX_18(int32_t value)
	{
		___ID_ScaleX_18 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleY_19() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleY_19)); }
	inline int32_t get_ID_ScaleY_19() const { return ___ID_ScaleY_19; }
	inline int32_t* get_address_of_ID_ScaleY_19() { return &___ID_ScaleY_19; }
	inline void set_ID_ScaleY_19(int32_t value)
	{
		___ID_ScaleY_19 = value;
	}

	inline static int32_t get_offset_of_ID_PerspectiveFilter_20() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_PerspectiveFilter_20)); }
	inline int32_t get_ID_PerspectiveFilter_20() const { return ___ID_PerspectiveFilter_20; }
	inline int32_t* get_address_of_ID_PerspectiveFilter_20() { return &___ID_PerspectiveFilter_20; }
	inline void set_ID_PerspectiveFilter_20(int32_t value)
	{
		___ID_PerspectiveFilter_20 = value;
	}

	inline static int32_t get_offset_of_ID_TextureWidth_21() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_TextureWidth_21)); }
	inline int32_t get_ID_TextureWidth_21() const { return ___ID_TextureWidth_21; }
	inline int32_t* get_address_of_ID_TextureWidth_21() { return &___ID_TextureWidth_21; }
	inline void set_ID_TextureWidth_21(int32_t value)
	{
		___ID_TextureWidth_21 = value;
	}

	inline static int32_t get_offset_of_ID_TextureHeight_22() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_TextureHeight_22)); }
	inline int32_t get_ID_TextureHeight_22() const { return ___ID_TextureHeight_22; }
	inline int32_t* get_address_of_ID_TextureHeight_22() { return &___ID_TextureHeight_22; }
	inline void set_ID_TextureHeight_22(int32_t value)
	{
		___ID_TextureHeight_22 = value;
	}

	inline static int32_t get_offset_of_ID_BevelAmount_23() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_BevelAmount_23)); }
	inline int32_t get_ID_BevelAmount_23() const { return ___ID_BevelAmount_23; }
	inline int32_t* get_address_of_ID_BevelAmount_23() { return &___ID_BevelAmount_23; }
	inline void set_ID_BevelAmount_23(int32_t value)
	{
		___ID_BevelAmount_23 = value;
	}

	inline static int32_t get_offset_of_ID_GlowColor_24() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowColor_24)); }
	inline int32_t get_ID_GlowColor_24() const { return ___ID_GlowColor_24; }
	inline int32_t* get_address_of_ID_GlowColor_24() { return &___ID_GlowColor_24; }
	inline void set_ID_GlowColor_24(int32_t value)
	{
		___ID_GlowColor_24 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOffset_25() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowOffset_25)); }
	inline int32_t get_ID_GlowOffset_25() const { return ___ID_GlowOffset_25; }
	inline int32_t* get_address_of_ID_GlowOffset_25() { return &___ID_GlowOffset_25; }
	inline void set_ID_GlowOffset_25(int32_t value)
	{
		___ID_GlowOffset_25 = value;
	}

	inline static int32_t get_offset_of_ID_GlowPower_26() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowPower_26)); }
	inline int32_t get_ID_GlowPower_26() const { return ___ID_GlowPower_26; }
	inline int32_t* get_address_of_ID_GlowPower_26() { return &___ID_GlowPower_26; }
	inline void set_ID_GlowPower_26(int32_t value)
	{
		___ID_GlowPower_26 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOuter_27() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowOuter_27)); }
	inline int32_t get_ID_GlowOuter_27() const { return ___ID_GlowOuter_27; }
	inline int32_t* get_address_of_ID_GlowOuter_27() { return &___ID_GlowOuter_27; }
	inline void set_ID_GlowOuter_27(int32_t value)
	{
		___ID_GlowOuter_27 = value;
	}

	inline static int32_t get_offset_of_ID_LightAngle_28() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_LightAngle_28)); }
	inline int32_t get_ID_LightAngle_28() const { return ___ID_LightAngle_28; }
	inline int32_t* get_address_of_ID_LightAngle_28() { return &___ID_LightAngle_28; }
	inline void set_ID_LightAngle_28(int32_t value)
	{
		___ID_LightAngle_28 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMap_29() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_EnvMap_29)); }
	inline int32_t get_ID_EnvMap_29() const { return ___ID_EnvMap_29; }
	inline int32_t* get_address_of_ID_EnvMap_29() { return &___ID_EnvMap_29; }
	inline void set_ID_EnvMap_29(int32_t value)
	{
		___ID_EnvMap_29 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrix_30() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_EnvMatrix_30)); }
	inline int32_t get_ID_EnvMatrix_30() const { return ___ID_EnvMatrix_30; }
	inline int32_t* get_address_of_ID_EnvMatrix_30() { return &___ID_EnvMatrix_30; }
	inline void set_ID_EnvMatrix_30(int32_t value)
	{
		___ID_EnvMatrix_30 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrixRotation_31() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_EnvMatrixRotation_31)); }
	inline int32_t get_ID_EnvMatrixRotation_31() const { return ___ID_EnvMatrixRotation_31; }
	inline int32_t* get_address_of_ID_EnvMatrixRotation_31() { return &___ID_EnvMatrixRotation_31; }
	inline void set_ID_EnvMatrixRotation_31(int32_t value)
	{
		___ID_EnvMatrixRotation_31 = value;
	}

	inline static int32_t get_offset_of_ID_MaskCoord_32() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MaskCoord_32)); }
	inline int32_t get_ID_MaskCoord_32() const { return ___ID_MaskCoord_32; }
	inline int32_t* get_address_of_ID_MaskCoord_32() { return &___ID_MaskCoord_32; }
	inline void set_ID_MaskCoord_32(int32_t value)
	{
		___ID_MaskCoord_32 = value;
	}

	inline static int32_t get_offset_of_ID_ClipRect_33() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ClipRect_33)); }
	inline int32_t get_ID_ClipRect_33() const { return ___ID_ClipRect_33; }
	inline int32_t* get_address_of_ID_ClipRect_33() { return &___ID_ClipRect_33; }
	inline void set_ID_ClipRect_33(int32_t value)
	{
		___ID_ClipRect_33 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessX_34() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MaskSoftnessX_34)); }
	inline int32_t get_ID_MaskSoftnessX_34() const { return ___ID_MaskSoftnessX_34; }
	inline int32_t* get_address_of_ID_MaskSoftnessX_34() { return &___ID_MaskSoftnessX_34; }
	inline void set_ID_MaskSoftnessX_34(int32_t value)
	{
		___ID_MaskSoftnessX_34 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessY_35() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MaskSoftnessY_35)); }
	inline int32_t get_ID_MaskSoftnessY_35() const { return ___ID_MaskSoftnessY_35; }
	inline int32_t* get_address_of_ID_MaskSoftnessY_35() { return &___ID_MaskSoftnessY_35; }
	inline void set_ID_MaskSoftnessY_35(int32_t value)
	{
		___ID_MaskSoftnessY_35 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetX_36() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_VertexOffsetX_36)); }
	inline int32_t get_ID_VertexOffsetX_36() const { return ___ID_VertexOffsetX_36; }
	inline int32_t* get_address_of_ID_VertexOffsetX_36() { return &___ID_VertexOffsetX_36; }
	inline void set_ID_VertexOffsetX_36(int32_t value)
	{
		___ID_VertexOffsetX_36 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetY_37() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_VertexOffsetY_37)); }
	inline int32_t get_ID_VertexOffsetY_37() const { return ___ID_VertexOffsetY_37; }
	inline int32_t* get_address_of_ID_VertexOffsetY_37() { return &___ID_VertexOffsetY_37; }
	inline void set_ID_VertexOffsetY_37(int32_t value)
	{
		___ID_VertexOffsetY_37 = value;
	}

	inline static int32_t get_offset_of_ID_UseClipRect_38() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UseClipRect_38)); }
	inline int32_t get_ID_UseClipRect_38() const { return ___ID_UseClipRect_38; }
	inline int32_t* get_address_of_ID_UseClipRect_38() { return &___ID_UseClipRect_38; }
	inline void set_ID_UseClipRect_38(int32_t value)
	{
		___ID_UseClipRect_38 = value;
	}

	inline static int32_t get_offset_of_ID_StencilID_39() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilID_39)); }
	inline int32_t get_ID_StencilID_39() const { return ___ID_StencilID_39; }
	inline int32_t* get_address_of_ID_StencilID_39() { return &___ID_StencilID_39; }
	inline void set_ID_StencilID_39(int32_t value)
	{
		___ID_StencilID_39 = value;
	}

	inline static int32_t get_offset_of_ID_StencilOp_40() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilOp_40)); }
	inline int32_t get_ID_StencilOp_40() const { return ___ID_StencilOp_40; }
	inline int32_t* get_address_of_ID_StencilOp_40() { return &___ID_StencilOp_40; }
	inline void set_ID_StencilOp_40(int32_t value)
	{
		___ID_StencilOp_40 = value;
	}

	inline static int32_t get_offset_of_ID_StencilComp_41() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilComp_41)); }
	inline int32_t get_ID_StencilComp_41() const { return ___ID_StencilComp_41; }
	inline int32_t* get_address_of_ID_StencilComp_41() { return &___ID_StencilComp_41; }
	inline void set_ID_StencilComp_41(int32_t value)
	{
		___ID_StencilComp_41 = value;
	}

	inline static int32_t get_offset_of_ID_StencilReadMask_42() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilReadMask_42)); }
	inline int32_t get_ID_StencilReadMask_42() const { return ___ID_StencilReadMask_42; }
	inline int32_t* get_address_of_ID_StencilReadMask_42() { return &___ID_StencilReadMask_42; }
	inline void set_ID_StencilReadMask_42(int32_t value)
	{
		___ID_StencilReadMask_42 = value;
	}

	inline static int32_t get_offset_of_ID_StencilWriteMask_43() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilWriteMask_43)); }
	inline int32_t get_ID_StencilWriteMask_43() const { return ___ID_StencilWriteMask_43; }
	inline int32_t* get_address_of_ID_StencilWriteMask_43() { return &___ID_StencilWriteMask_43; }
	inline void set_ID_StencilWriteMask_43(int32_t value)
	{
		___ID_StencilWriteMask_43 = value;
	}

	inline static int32_t get_offset_of_ID_ShaderFlags_44() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ShaderFlags_44)); }
	inline int32_t get_ID_ShaderFlags_44() const { return ___ID_ShaderFlags_44; }
	inline int32_t* get_address_of_ID_ShaderFlags_44() { return &___ID_ShaderFlags_44; }
	inline void set_ID_ShaderFlags_44(int32_t value)
	{
		___ID_ShaderFlags_44 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_A_45() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleRatio_A_45)); }
	inline int32_t get_ID_ScaleRatio_A_45() const { return ___ID_ScaleRatio_A_45; }
	inline int32_t* get_address_of_ID_ScaleRatio_A_45() { return &___ID_ScaleRatio_A_45; }
	inline void set_ID_ScaleRatio_A_45(int32_t value)
	{
		___ID_ScaleRatio_A_45 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_B_46() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleRatio_B_46)); }
	inline int32_t get_ID_ScaleRatio_B_46() const { return ___ID_ScaleRatio_B_46; }
	inline int32_t* get_address_of_ID_ScaleRatio_B_46() { return &___ID_ScaleRatio_B_46; }
	inline void set_ID_ScaleRatio_B_46(int32_t value)
	{
		___ID_ScaleRatio_B_46 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_C_47() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleRatio_C_47)); }
	inline int32_t get_ID_ScaleRatio_C_47() const { return ___ID_ScaleRatio_C_47; }
	inline int32_t* get_address_of_ID_ScaleRatio_C_47() { return &___ID_ScaleRatio_C_47; }
	inline void set_ID_ScaleRatio_C_47(int32_t value)
	{
		___ID_ScaleRatio_C_47 = value;
	}

	inline static int32_t get_offset_of_Keyword_Bevel_48() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Bevel_48)); }
	inline String_t* get_Keyword_Bevel_48() const { return ___Keyword_Bevel_48; }
	inline String_t** get_address_of_Keyword_Bevel_48() { return &___Keyword_Bevel_48; }
	inline void set_Keyword_Bevel_48(String_t* value)
	{
		___Keyword_Bevel_48 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Bevel_48), value);
	}

	inline static int32_t get_offset_of_Keyword_Glow_49() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Glow_49)); }
	inline String_t* get_Keyword_Glow_49() const { return ___Keyword_Glow_49; }
	inline String_t** get_address_of_Keyword_Glow_49() { return &___Keyword_Glow_49; }
	inline void set_Keyword_Glow_49(String_t* value)
	{
		___Keyword_Glow_49 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Glow_49), value);
	}

	inline static int32_t get_offset_of_Keyword_Underlay_50() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Underlay_50)); }
	inline String_t* get_Keyword_Underlay_50() const { return ___Keyword_Underlay_50; }
	inline String_t** get_address_of_Keyword_Underlay_50() { return &___Keyword_Underlay_50; }
	inline void set_Keyword_Underlay_50(String_t* value)
	{
		___Keyword_Underlay_50 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Underlay_50), value);
	}

	inline static int32_t get_offset_of_Keyword_Ratios_51() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Ratios_51)); }
	inline String_t* get_Keyword_Ratios_51() const { return ___Keyword_Ratios_51; }
	inline String_t** get_address_of_Keyword_Ratios_51() { return &___Keyword_Ratios_51; }
	inline void set_Keyword_Ratios_51(String_t* value)
	{
		___Keyword_Ratios_51 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Ratios_51), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_SOFT_52() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_MASK_SOFT_52)); }
	inline String_t* get_Keyword_MASK_SOFT_52() const { return ___Keyword_MASK_SOFT_52; }
	inline String_t** get_address_of_Keyword_MASK_SOFT_52() { return &___Keyword_MASK_SOFT_52; }
	inline void set_Keyword_MASK_SOFT_52(String_t* value)
	{
		___Keyword_MASK_SOFT_52 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_SOFT_52), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_HARD_53() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_MASK_HARD_53)); }
	inline String_t* get_Keyword_MASK_HARD_53() const { return ___Keyword_MASK_HARD_53; }
	inline String_t** get_address_of_Keyword_MASK_HARD_53() { return &___Keyword_MASK_HARD_53; }
	inline void set_Keyword_MASK_HARD_53(String_t* value)
	{
		___Keyword_MASK_HARD_53 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_HARD_53), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_TEX_54() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_MASK_TEX_54)); }
	inline String_t* get_Keyword_MASK_TEX_54() const { return ___Keyword_MASK_TEX_54; }
	inline String_t** get_address_of_Keyword_MASK_TEX_54() { return &___Keyword_MASK_TEX_54; }
	inline void set_Keyword_MASK_TEX_54(String_t* value)
	{
		___Keyword_MASK_TEX_54 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_TEX_54), value);
	}

	inline static int32_t get_offset_of_Keyword_Outline_55() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Outline_55)); }
	inline String_t* get_Keyword_Outline_55() const { return ___Keyword_Outline_55; }
	inline String_t** get_address_of_Keyword_Outline_55() { return &___Keyword_Outline_55; }
	inline void set_Keyword_Outline_55(String_t* value)
	{
		___Keyword_Outline_55 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Outline_55), value);
	}

	inline static int32_t get_offset_of_ShaderTag_ZTestMode_56() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ShaderTag_ZTestMode_56)); }
	inline String_t* get_ShaderTag_ZTestMode_56() const { return ___ShaderTag_ZTestMode_56; }
	inline String_t** get_address_of_ShaderTag_ZTestMode_56() { return &___ShaderTag_ZTestMode_56; }
	inline void set_ShaderTag_ZTestMode_56(String_t* value)
	{
		___ShaderTag_ZTestMode_56 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_ZTestMode_56), value);
	}

	inline static int32_t get_offset_of_ShaderTag_CullMode_57() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ShaderTag_CullMode_57)); }
	inline String_t* get_ShaderTag_CullMode_57() const { return ___ShaderTag_CullMode_57; }
	inline String_t** get_address_of_ShaderTag_CullMode_57() { return &___ShaderTag_CullMode_57; }
	inline void set_ShaderTag_CullMode_57(String_t* value)
	{
		___ShaderTag_CullMode_57 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_CullMode_57), value);
	}

	inline static int32_t get_offset_of_m_clamp_58() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___m_clamp_58)); }
	inline float get_m_clamp_58() const { return ___m_clamp_58; }
	inline float* get_address_of_m_clamp_58() { return &___m_clamp_58; }
	inline void set_m_clamp_58(float value)
	{
		___m_clamp_58 = value;
	}

	inline static int32_t get_offset_of_isInitialized_59() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___isInitialized_59)); }
	inline bool get_isInitialized_59() const { return ___isInitialized_59; }
	inline bool* get_address_of_isInitialized_59() { return &___isInitialized_59; }
	inline void set_isInitialized_59(bool value)
	{
		___isInitialized_59 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERUTILITIES_T714255158_H
#ifndef TMP_FONTUTILITIES_T2599150238_H
#define TMP_FONTUTILITIES_T2599150238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontUtilities
struct  TMP_FontUtilities_t2599150238  : public RuntimeObject
{
public:

public:
};

struct TMP_FontUtilities_t2599150238_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Int32> TMPro.TMP_FontUtilities::k_searchedFontAssets
	List_1_t128053199 * ___k_searchedFontAssets_0;

public:
	inline static int32_t get_offset_of_k_searchedFontAssets_0() { return static_cast<int32_t>(offsetof(TMP_FontUtilities_t2599150238_StaticFields, ___k_searchedFontAssets_0)); }
	inline List_1_t128053199 * get_k_searchedFontAssets_0() const { return ___k_searchedFontAssets_0; }
	inline List_1_t128053199 ** get_address_of_k_searchedFontAssets_0() { return &___k_searchedFontAssets_0; }
	inline void set_k_searchedFontAssets_0(List_1_t128053199 * value)
	{
		___k_searchedFontAssets_0 = value;
		Il2CppCodeGenWriteBarrier((&___k_searchedFontAssets_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTUTILITIES_T2599150238_H
#ifndef TMP_TEXTELEMENT_T129727469_H
#define TMP_TEXTELEMENT_T129727469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElement
struct  TMP_TextElement_t129727469  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_TextElement::id
	int32_t ___id_0;
	// System.Single TMPro.TMP_TextElement::x
	float ___x_1;
	// System.Single TMPro.TMP_TextElement::y
	float ___y_2;
	// System.Single TMPro.TMP_TextElement::width
	float ___width_3;
	// System.Single TMPro.TMP_TextElement::height
	float ___height_4;
	// System.Single TMPro.TMP_TextElement::xOffset
	float ___xOffset_5;
	// System.Single TMPro.TMP_TextElement::yOffset
	float ___yOffset_6;
	// System.Single TMPro.TMP_TextElement::xAdvance
	float ___xAdvance_7;
	// System.Single TMPro.TMP_TextElement::scale
	float ___scale_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_xOffset_5() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___xOffset_5)); }
	inline float get_xOffset_5() const { return ___xOffset_5; }
	inline float* get_address_of_xOffset_5() { return &___xOffset_5; }
	inline void set_xOffset_5(float value)
	{
		___xOffset_5 = value;
	}

	inline static int32_t get_offset_of_yOffset_6() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___yOffset_6)); }
	inline float get_yOffset_6() const { return ___yOffset_6; }
	inline float* get_address_of_yOffset_6() { return &___yOffset_6; }
	inline void set_yOffset_6(float value)
	{
		___yOffset_6 = value;
	}

	inline static int32_t get_offset_of_xAdvance_7() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___xAdvance_7)); }
	inline float get_xAdvance_7() const { return ___xAdvance_7; }
	inline float* get_address_of_xAdvance_7() { return &___xAdvance_7; }
	inline void set_xAdvance_7(float value)
	{
		___xAdvance_7 = value;
	}

	inline static int32_t get_offset_of_scale_8() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___scale_8)); }
	inline float get_scale_8() const { return ___scale_8; }
	inline float* get_address_of_scale_8() { return &___scale_8; }
	inline void set_scale_8(float value)
	{
		___scale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENT_T129727469_H
#ifndef TMP_TEXTUTILITIES_T2105690005_H
#define TMP_TEXTUTILITIES_T2105690005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextUtilities
struct  TMP_TextUtilities_t2105690005  : public RuntimeObject
{
public:

public:
};

struct TMP_TextUtilities_t2105690005_StaticFields
{
public:
	// UnityEngine.Vector3[] TMPro.TMP_TextUtilities::m_rectWorldCorners
	Vector3U5BU5D_t1718750761* ___m_rectWorldCorners_0;

public:
	inline static int32_t get_offset_of_m_rectWorldCorners_0() { return static_cast<int32_t>(offsetof(TMP_TextUtilities_t2105690005_StaticFields, ___m_rectWorldCorners_0)); }
	inline Vector3U5BU5D_t1718750761* get_m_rectWorldCorners_0() const { return ___m_rectWorldCorners_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_rectWorldCorners_0() { return &___m_rectWorldCorners_0; }
	inline void set_m_rectWorldCorners_0(Vector3U5BU5D_t1718750761* value)
	{
		___m_rectWorldCorners_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectWorldCorners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTUTILITIES_T2105690005_H
#ifndef TMP_UPDATEMANAGER_T4114267509_H
#define TMP_UPDATEMANAGER_T4114267509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_UpdateManager
struct  TMP_UpdateManager_t4114267509  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_LayoutRebuildQueue
	List_1_t4071693616 * ___m_LayoutRebuildQueue_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_LayoutQueueLookup
	Dictionary_2_t1839659084 * ___m_LayoutQueueLookup_2;
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_GraphicRebuildQueue
	List_1_t4071693616 * ___m_GraphicRebuildQueue_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_GraphicQueueLookup
	Dictionary_2_t1839659084 * ___m_GraphicQueueLookup_4;

public:
	inline static int32_t get_offset_of_m_LayoutRebuildQueue_1() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_LayoutRebuildQueue_1)); }
	inline List_1_t4071693616 * get_m_LayoutRebuildQueue_1() const { return ___m_LayoutRebuildQueue_1; }
	inline List_1_t4071693616 ** get_address_of_m_LayoutRebuildQueue_1() { return &___m_LayoutRebuildQueue_1; }
	inline void set_m_LayoutRebuildQueue_1(List_1_t4071693616 * value)
	{
		___m_LayoutRebuildQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutRebuildQueue_1), value);
	}

	inline static int32_t get_offset_of_m_LayoutQueueLookup_2() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_LayoutQueueLookup_2)); }
	inline Dictionary_2_t1839659084 * get_m_LayoutQueueLookup_2() const { return ___m_LayoutQueueLookup_2; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_LayoutQueueLookup_2() { return &___m_LayoutQueueLookup_2; }
	inline void set_m_LayoutQueueLookup_2(Dictionary_2_t1839659084 * value)
	{
		___m_LayoutQueueLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutQueueLookup_2), value);
	}

	inline static int32_t get_offset_of_m_GraphicRebuildQueue_3() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_GraphicRebuildQueue_3)); }
	inline List_1_t4071693616 * get_m_GraphicRebuildQueue_3() const { return ___m_GraphicRebuildQueue_3; }
	inline List_1_t4071693616 ** get_address_of_m_GraphicRebuildQueue_3() { return &___m_GraphicRebuildQueue_3; }
	inline void set_m_GraphicRebuildQueue_3(List_1_t4071693616 * value)
	{
		___m_GraphicRebuildQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicRebuildQueue_3), value);
	}

	inline static int32_t get_offset_of_m_GraphicQueueLookup_4() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_GraphicQueueLookup_4)); }
	inline Dictionary_2_t1839659084 * get_m_GraphicQueueLookup_4() const { return ___m_GraphicQueueLookup_4; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_GraphicQueueLookup_4() { return &___m_GraphicQueueLookup_4; }
	inline void set_m_GraphicQueueLookup_4(Dictionary_2_t1839659084 * value)
	{
		___m_GraphicQueueLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicQueueLookup_4), value);
	}
};

struct TMP_UpdateManager_t4114267509_StaticFields
{
public:
	// TMPro.TMP_UpdateManager TMPro.TMP_UpdateManager::s_Instance
	TMP_UpdateManager_t4114267509 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509_StaticFields, ___s_Instance_0)); }
	inline TMP_UpdateManager_t4114267509 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_UpdateManager_t4114267509 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_UpdateManager_t4114267509 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UPDATEMANAGER_T4114267509_H
#ifndef TMP_UPDATEREGISTRY_T461608481_H
#define TMP_UPDATEREGISTRY_T461608481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_UpdateRegistry
struct  TMP_UpdateRegistry_t461608481  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement> TMPro.TMP_UpdateRegistry::m_LayoutRebuildQueue
	List_1_t3593973608 * ___m_LayoutRebuildQueue_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateRegistry::m_LayoutQueueLookup
	Dictionary_2_t1839659084 * ___m_LayoutQueueLookup_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement> TMPro.TMP_UpdateRegistry::m_GraphicRebuildQueue
	List_1_t3593973608 * ___m_GraphicRebuildQueue_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateRegistry::m_GraphicQueueLookup
	Dictionary_2_t1839659084 * ___m_GraphicQueueLookup_4;

public:
	inline static int32_t get_offset_of_m_LayoutRebuildQueue_1() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_LayoutRebuildQueue_1)); }
	inline List_1_t3593973608 * get_m_LayoutRebuildQueue_1() const { return ___m_LayoutRebuildQueue_1; }
	inline List_1_t3593973608 ** get_address_of_m_LayoutRebuildQueue_1() { return &___m_LayoutRebuildQueue_1; }
	inline void set_m_LayoutRebuildQueue_1(List_1_t3593973608 * value)
	{
		___m_LayoutRebuildQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutRebuildQueue_1), value);
	}

	inline static int32_t get_offset_of_m_LayoutQueueLookup_2() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_LayoutQueueLookup_2)); }
	inline Dictionary_2_t1839659084 * get_m_LayoutQueueLookup_2() const { return ___m_LayoutQueueLookup_2; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_LayoutQueueLookup_2() { return &___m_LayoutQueueLookup_2; }
	inline void set_m_LayoutQueueLookup_2(Dictionary_2_t1839659084 * value)
	{
		___m_LayoutQueueLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutQueueLookup_2), value);
	}

	inline static int32_t get_offset_of_m_GraphicRebuildQueue_3() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_GraphicRebuildQueue_3)); }
	inline List_1_t3593973608 * get_m_GraphicRebuildQueue_3() const { return ___m_GraphicRebuildQueue_3; }
	inline List_1_t3593973608 ** get_address_of_m_GraphicRebuildQueue_3() { return &___m_GraphicRebuildQueue_3; }
	inline void set_m_GraphicRebuildQueue_3(List_1_t3593973608 * value)
	{
		___m_GraphicRebuildQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicRebuildQueue_3), value);
	}

	inline static int32_t get_offset_of_m_GraphicQueueLookup_4() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_GraphicQueueLookup_4)); }
	inline Dictionary_2_t1839659084 * get_m_GraphicQueueLookup_4() const { return ___m_GraphicQueueLookup_4; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_GraphicQueueLookup_4() { return &___m_GraphicQueueLookup_4; }
	inline void set_m_GraphicQueueLookup_4(Dictionary_2_t1839659084 * value)
	{
		___m_GraphicQueueLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicQueueLookup_4), value);
	}
};

struct TMP_UpdateRegistry_t461608481_StaticFields
{
public:
	// TMPro.TMP_UpdateRegistry TMPro.TMP_UpdateRegistry::s_Instance
	TMP_UpdateRegistry_t461608481 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481_StaticFields, ___s_Instance_0)); }
	inline TMP_UpdateRegistry_t461608481 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_UpdateRegistry_t461608481 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_UpdateRegistry_t461608481 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UPDATEREGISTRY_T461608481_H
#ifndef TMPRO_EVENTMANAGER_T712497257_H
#define TMPRO_EVENTMANAGER_T712497257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMPro_EventManager
struct  TMPro_EventManager_t712497257  : public RuntimeObject
{
public:

public:
};

struct TMPro_EventManager_t712497257_StaticFields
{
public:
	// TMPro.FastAction`2<System.Object,TMPro.Compute_DT_EventArgs> TMPro.TMPro_EventManager::COMPUTE_DT_EVENT
	FastAction_2_t2884460249 * ___COMPUTE_DT_EVENT_0;
	// TMPro.FastAction`2<System.Boolean,UnityEngine.Material> TMPro.TMPro_EventManager::MATERIAL_PROPERTY_EVENT
	FastAction_2_t2206961073 * ___MATERIAL_PROPERTY_EVENT_1;
	// TMPro.FastAction`2<System.Boolean,TMPro.TMP_FontAsset> TMPro.TMPro_EventManager::FONT_PROPERTY_EVENT
	FastAction_2_t2230967576 * ___FONT_PROPERTY_EVENT_2;
	// TMPro.FastAction`2<System.Boolean,UnityEngine.Object> TMPro.TMPro_EventManager::SPRITE_ASSET_PROPERTY_EVENT
	FastAction_2_t2497593903 * ___SPRITE_ASSET_PROPERTY_EVENT_3;
	// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshPro> TMPro.TMPro_EventManager::TEXTMESHPRO_PROPERTY_EVENT
	FastAction_2_t4260179116 * ___TEXTMESHPRO_PROPERTY_EVENT_4;
	// TMPro.FastAction`3<UnityEngine.GameObject,UnityEngine.Material,UnityEngine.Material> TMPro.TMPro_EventManager::DRAG_AND_DROP_MATERIAL_EVENT
	FastAction_3_t58649212 * ___DRAG_AND_DROP_MATERIAL_EVENT_5;
	// TMPro.FastAction`1<System.Boolean> TMPro.TMPro_EventManager::TEXT_STYLE_PROPERTY_EVENT
	FastAction_1_t3758825850 * ___TEXT_STYLE_PROPERTY_EVENT_6;
	// TMPro.FastAction`1<TMPro.TMP_ColorGradient> TMPro.TMPro_EventManager::COLOR_GRADIENT_PROPERTY_EVENT
	FastAction_1_t3044626357 * ___COLOR_GRADIENT_PROPERTY_EVENT_7;
	// TMPro.FastAction TMPro.TMPro_EventManager::TMP_SETTINGS_PROPERTY_EVENT
	FastAction_t3491443480 * ___TMP_SETTINGS_PROPERTY_EVENT_8;
	// TMPro.FastAction TMPro.TMPro_EventManager::RESOURCE_LOAD_EVENT
	FastAction_t3491443480 * ___RESOURCE_LOAD_EVENT_9;
	// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshProUGUI> TMPro.TMPro_EventManager::TEXTMESHPRO_UGUI_PROPERTY_EVENT
	FastAction_2_t2395899227 * ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10;
	// TMPro.FastAction TMPro.TMPro_EventManager::OnPreRenderObject_Event
	FastAction_t3491443480 * ___OnPreRenderObject_Event_11;
	// TMPro.FastAction`1<UnityEngine.Object> TMPro.TMPro_EventManager::TEXT_CHANGED_EVENT
	FastAction_1_t4292545838 * ___TEXT_CHANGED_EVENT_12;

public:
	inline static int32_t get_offset_of_COMPUTE_DT_EVENT_0() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___COMPUTE_DT_EVENT_0)); }
	inline FastAction_2_t2884460249 * get_COMPUTE_DT_EVENT_0() const { return ___COMPUTE_DT_EVENT_0; }
	inline FastAction_2_t2884460249 ** get_address_of_COMPUTE_DT_EVENT_0() { return &___COMPUTE_DT_EVENT_0; }
	inline void set_COMPUTE_DT_EVENT_0(FastAction_2_t2884460249 * value)
	{
		___COMPUTE_DT_EVENT_0 = value;
		Il2CppCodeGenWriteBarrier((&___COMPUTE_DT_EVENT_0), value);
	}

	inline static int32_t get_offset_of_MATERIAL_PROPERTY_EVENT_1() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___MATERIAL_PROPERTY_EVENT_1)); }
	inline FastAction_2_t2206961073 * get_MATERIAL_PROPERTY_EVENT_1() const { return ___MATERIAL_PROPERTY_EVENT_1; }
	inline FastAction_2_t2206961073 ** get_address_of_MATERIAL_PROPERTY_EVENT_1() { return &___MATERIAL_PROPERTY_EVENT_1; }
	inline void set_MATERIAL_PROPERTY_EVENT_1(FastAction_2_t2206961073 * value)
	{
		___MATERIAL_PROPERTY_EVENT_1 = value;
		Il2CppCodeGenWriteBarrier((&___MATERIAL_PROPERTY_EVENT_1), value);
	}

	inline static int32_t get_offset_of_FONT_PROPERTY_EVENT_2() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___FONT_PROPERTY_EVENT_2)); }
	inline FastAction_2_t2230967576 * get_FONT_PROPERTY_EVENT_2() const { return ___FONT_PROPERTY_EVENT_2; }
	inline FastAction_2_t2230967576 ** get_address_of_FONT_PROPERTY_EVENT_2() { return &___FONT_PROPERTY_EVENT_2; }
	inline void set_FONT_PROPERTY_EVENT_2(FastAction_2_t2230967576 * value)
	{
		___FONT_PROPERTY_EVENT_2 = value;
		Il2CppCodeGenWriteBarrier((&___FONT_PROPERTY_EVENT_2), value);
	}

	inline static int32_t get_offset_of_SPRITE_ASSET_PROPERTY_EVENT_3() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___SPRITE_ASSET_PROPERTY_EVENT_3)); }
	inline FastAction_2_t2497593903 * get_SPRITE_ASSET_PROPERTY_EVENT_3() const { return ___SPRITE_ASSET_PROPERTY_EVENT_3; }
	inline FastAction_2_t2497593903 ** get_address_of_SPRITE_ASSET_PROPERTY_EVENT_3() { return &___SPRITE_ASSET_PROPERTY_EVENT_3; }
	inline void set_SPRITE_ASSET_PROPERTY_EVENT_3(FastAction_2_t2497593903 * value)
	{
		___SPRITE_ASSET_PROPERTY_EVENT_3 = value;
		Il2CppCodeGenWriteBarrier((&___SPRITE_ASSET_PROPERTY_EVENT_3), value);
	}

	inline static int32_t get_offset_of_TEXTMESHPRO_PROPERTY_EVENT_4() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___TEXTMESHPRO_PROPERTY_EVENT_4)); }
	inline FastAction_2_t4260179116 * get_TEXTMESHPRO_PROPERTY_EVENT_4() const { return ___TEXTMESHPRO_PROPERTY_EVENT_4; }
	inline FastAction_2_t4260179116 ** get_address_of_TEXTMESHPRO_PROPERTY_EVENT_4() { return &___TEXTMESHPRO_PROPERTY_EVENT_4; }
	inline void set_TEXTMESHPRO_PROPERTY_EVENT_4(FastAction_2_t4260179116 * value)
	{
		___TEXTMESHPRO_PROPERTY_EVENT_4 = value;
		Il2CppCodeGenWriteBarrier((&___TEXTMESHPRO_PROPERTY_EVENT_4), value);
	}

	inline static int32_t get_offset_of_DRAG_AND_DROP_MATERIAL_EVENT_5() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___DRAG_AND_DROP_MATERIAL_EVENT_5)); }
	inline FastAction_3_t58649212 * get_DRAG_AND_DROP_MATERIAL_EVENT_5() const { return ___DRAG_AND_DROP_MATERIAL_EVENT_5; }
	inline FastAction_3_t58649212 ** get_address_of_DRAG_AND_DROP_MATERIAL_EVENT_5() { return &___DRAG_AND_DROP_MATERIAL_EVENT_5; }
	inline void set_DRAG_AND_DROP_MATERIAL_EVENT_5(FastAction_3_t58649212 * value)
	{
		___DRAG_AND_DROP_MATERIAL_EVENT_5 = value;
		Il2CppCodeGenWriteBarrier((&___DRAG_AND_DROP_MATERIAL_EVENT_5), value);
	}

	inline static int32_t get_offset_of_TEXT_STYLE_PROPERTY_EVENT_6() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___TEXT_STYLE_PROPERTY_EVENT_6)); }
	inline FastAction_1_t3758825850 * get_TEXT_STYLE_PROPERTY_EVENT_6() const { return ___TEXT_STYLE_PROPERTY_EVENT_6; }
	inline FastAction_1_t3758825850 ** get_address_of_TEXT_STYLE_PROPERTY_EVENT_6() { return &___TEXT_STYLE_PROPERTY_EVENT_6; }
	inline void set_TEXT_STYLE_PROPERTY_EVENT_6(FastAction_1_t3758825850 * value)
	{
		___TEXT_STYLE_PROPERTY_EVENT_6 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_STYLE_PROPERTY_EVENT_6), value);
	}

	inline static int32_t get_offset_of_COLOR_GRADIENT_PROPERTY_EVENT_7() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___COLOR_GRADIENT_PROPERTY_EVENT_7)); }
	inline FastAction_1_t3044626357 * get_COLOR_GRADIENT_PROPERTY_EVENT_7() const { return ___COLOR_GRADIENT_PROPERTY_EVENT_7; }
	inline FastAction_1_t3044626357 ** get_address_of_COLOR_GRADIENT_PROPERTY_EVENT_7() { return &___COLOR_GRADIENT_PROPERTY_EVENT_7; }
	inline void set_COLOR_GRADIENT_PROPERTY_EVENT_7(FastAction_1_t3044626357 * value)
	{
		___COLOR_GRADIENT_PROPERTY_EVENT_7 = value;
		Il2CppCodeGenWriteBarrier((&___COLOR_GRADIENT_PROPERTY_EVENT_7), value);
	}

	inline static int32_t get_offset_of_TMP_SETTINGS_PROPERTY_EVENT_8() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___TMP_SETTINGS_PROPERTY_EVENT_8)); }
	inline FastAction_t3491443480 * get_TMP_SETTINGS_PROPERTY_EVENT_8() const { return ___TMP_SETTINGS_PROPERTY_EVENT_8; }
	inline FastAction_t3491443480 ** get_address_of_TMP_SETTINGS_PROPERTY_EVENT_8() { return &___TMP_SETTINGS_PROPERTY_EVENT_8; }
	inline void set_TMP_SETTINGS_PROPERTY_EVENT_8(FastAction_t3491443480 * value)
	{
		___TMP_SETTINGS_PROPERTY_EVENT_8 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_SETTINGS_PROPERTY_EVENT_8), value);
	}

	inline static int32_t get_offset_of_RESOURCE_LOAD_EVENT_9() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___RESOURCE_LOAD_EVENT_9)); }
	inline FastAction_t3491443480 * get_RESOURCE_LOAD_EVENT_9() const { return ___RESOURCE_LOAD_EVENT_9; }
	inline FastAction_t3491443480 ** get_address_of_RESOURCE_LOAD_EVENT_9() { return &___RESOURCE_LOAD_EVENT_9; }
	inline void set_RESOURCE_LOAD_EVENT_9(FastAction_t3491443480 * value)
	{
		___RESOURCE_LOAD_EVENT_9 = value;
		Il2CppCodeGenWriteBarrier((&___RESOURCE_LOAD_EVENT_9), value);
	}

	inline static int32_t get_offset_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10)); }
	inline FastAction_2_t2395899227 * get_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10() const { return ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10; }
	inline FastAction_2_t2395899227 ** get_address_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10() { return &___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10; }
	inline void set_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10(FastAction_2_t2395899227 * value)
	{
		___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10 = value;
		Il2CppCodeGenWriteBarrier((&___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10), value);
	}

	inline static int32_t get_offset_of_OnPreRenderObject_Event_11() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___OnPreRenderObject_Event_11)); }
	inline FastAction_t3491443480 * get_OnPreRenderObject_Event_11() const { return ___OnPreRenderObject_Event_11; }
	inline FastAction_t3491443480 ** get_address_of_OnPreRenderObject_Event_11() { return &___OnPreRenderObject_Event_11; }
	inline void set_OnPreRenderObject_Event_11(FastAction_t3491443480 * value)
	{
		___OnPreRenderObject_Event_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnPreRenderObject_Event_11), value);
	}

	inline static int32_t get_offset_of_TEXT_CHANGED_EVENT_12() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___TEXT_CHANGED_EVENT_12)); }
	inline FastAction_1_t4292545838 * get_TEXT_CHANGED_EVENT_12() const { return ___TEXT_CHANGED_EVENT_12; }
	inline FastAction_1_t4292545838 ** get_address_of_TEXT_CHANGED_EVENT_12() { return &___TEXT_CHANGED_EVENT_12; }
	inline void set_TEXT_CHANGED_EVENT_12(FastAction_1_t4292545838 * value)
	{
		___TEXT_CHANGED_EVENT_12 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_CHANGED_EVENT_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_EVENTMANAGER_T712497257_H
#ifndef TMPRO_EXTENSIONMETHODS_T1453208966_H
#define TMPRO_EXTENSIONMETHODS_T1453208966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMPro_ExtensionMethods
struct  TMPro_ExtensionMethods_t1453208966  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_EXTENSIONMETHODS_T1453208966_H
#ifndef U3CSTARTSPLASHU3EC__ITERATOR0_T1313349864_H
#define U3CSTARTSPLASHU3EC__ITERATOR0_T1313349864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIController/<StartSplash>c__Iterator0
struct  U3CStartSplashU3Ec__Iterator0_t1313349864  : public RuntimeObject
{
public:
	// UIController UIController/<StartSplash>c__Iterator0::$this
	UIController_t2237998930 * ___U24this_0;
	// System.Object UIController/<StartSplash>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UIController/<StartSplash>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UIController/<StartSplash>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartSplashU3Ec__Iterator0_t1313349864, ___U24this_0)); }
	inline UIController_t2237998930 * get_U24this_0() const { return ___U24this_0; }
	inline UIController_t2237998930 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(UIController_t2237998930 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartSplashU3Ec__Iterator0_t1313349864, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartSplashU3Ec__Iterator0_t1313349864, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartSplashU3Ec__Iterator0_t1313349864, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTSPLASHU3EC__ITERATOR0_T1313349864_H
#ifndef U24ARRAYTYPEU3D12_T2488454197_H
#define U24ARRAYTYPEU3D12_T2488454197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454197 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454197__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454197_H
#ifndef U24ARRAYTYPEU3D40_T2865632059_H
#define U24ARRAYTYPEU3D40_T2865632059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=40
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D40_t2865632059 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D40_t2865632059__padding[40];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D40_T2865632059_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef FONTASSETCREATIONSETTINGS_T359369028_H
#define FONTASSETCREATIONSETTINGS_T359369028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontAssetCreationSettings
struct  FontAssetCreationSettings_t359369028 
{
public:
	// System.String TMPro.FontAssetCreationSettings::sourceFontFileName
	String_t* ___sourceFontFileName_0;
	// System.String TMPro.FontAssetCreationSettings::sourceFontFileGUID
	String_t* ___sourceFontFileGUID_1;
	// System.Int32 TMPro.FontAssetCreationSettings::pointSizeSamplingMode
	int32_t ___pointSizeSamplingMode_2;
	// System.Int32 TMPro.FontAssetCreationSettings::pointSize
	int32_t ___pointSize_3;
	// System.Int32 TMPro.FontAssetCreationSettings::padding
	int32_t ___padding_4;
	// System.Int32 TMPro.FontAssetCreationSettings::packingMode
	int32_t ___packingMode_5;
	// System.Int32 TMPro.FontAssetCreationSettings::atlasWidth
	int32_t ___atlasWidth_6;
	// System.Int32 TMPro.FontAssetCreationSettings::atlasHeight
	int32_t ___atlasHeight_7;
	// System.Int32 TMPro.FontAssetCreationSettings::characterSetSelectionMode
	int32_t ___characterSetSelectionMode_8;
	// System.String TMPro.FontAssetCreationSettings::characterSequence
	String_t* ___characterSequence_9;
	// System.String TMPro.FontAssetCreationSettings::referencedFontAssetGUID
	String_t* ___referencedFontAssetGUID_10;
	// System.String TMPro.FontAssetCreationSettings::referencedTextAssetGUID
	String_t* ___referencedTextAssetGUID_11;
	// System.Int32 TMPro.FontAssetCreationSettings::fontStyle
	int32_t ___fontStyle_12;
	// System.Single TMPro.FontAssetCreationSettings::fontStyleModifier
	float ___fontStyleModifier_13;
	// System.Int32 TMPro.FontAssetCreationSettings::renderMode
	int32_t ___renderMode_14;
	// System.Boolean TMPro.FontAssetCreationSettings::includeFontFeatures
	bool ___includeFontFeatures_15;

public:
	inline static int32_t get_offset_of_sourceFontFileName_0() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___sourceFontFileName_0)); }
	inline String_t* get_sourceFontFileName_0() const { return ___sourceFontFileName_0; }
	inline String_t** get_address_of_sourceFontFileName_0() { return &___sourceFontFileName_0; }
	inline void set_sourceFontFileName_0(String_t* value)
	{
		___sourceFontFileName_0 = value;
		Il2CppCodeGenWriteBarrier((&___sourceFontFileName_0), value);
	}

	inline static int32_t get_offset_of_sourceFontFileGUID_1() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___sourceFontFileGUID_1)); }
	inline String_t* get_sourceFontFileGUID_1() const { return ___sourceFontFileGUID_1; }
	inline String_t** get_address_of_sourceFontFileGUID_1() { return &___sourceFontFileGUID_1; }
	inline void set_sourceFontFileGUID_1(String_t* value)
	{
		___sourceFontFileGUID_1 = value;
		Il2CppCodeGenWriteBarrier((&___sourceFontFileGUID_1), value);
	}

	inline static int32_t get_offset_of_pointSizeSamplingMode_2() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___pointSizeSamplingMode_2)); }
	inline int32_t get_pointSizeSamplingMode_2() const { return ___pointSizeSamplingMode_2; }
	inline int32_t* get_address_of_pointSizeSamplingMode_2() { return &___pointSizeSamplingMode_2; }
	inline void set_pointSizeSamplingMode_2(int32_t value)
	{
		___pointSizeSamplingMode_2 = value;
	}

	inline static int32_t get_offset_of_pointSize_3() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___pointSize_3)); }
	inline int32_t get_pointSize_3() const { return ___pointSize_3; }
	inline int32_t* get_address_of_pointSize_3() { return &___pointSize_3; }
	inline void set_pointSize_3(int32_t value)
	{
		___pointSize_3 = value;
	}

	inline static int32_t get_offset_of_padding_4() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___padding_4)); }
	inline int32_t get_padding_4() const { return ___padding_4; }
	inline int32_t* get_address_of_padding_4() { return &___padding_4; }
	inline void set_padding_4(int32_t value)
	{
		___padding_4 = value;
	}

	inline static int32_t get_offset_of_packingMode_5() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___packingMode_5)); }
	inline int32_t get_packingMode_5() const { return ___packingMode_5; }
	inline int32_t* get_address_of_packingMode_5() { return &___packingMode_5; }
	inline void set_packingMode_5(int32_t value)
	{
		___packingMode_5 = value;
	}

	inline static int32_t get_offset_of_atlasWidth_6() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___atlasWidth_6)); }
	inline int32_t get_atlasWidth_6() const { return ___atlasWidth_6; }
	inline int32_t* get_address_of_atlasWidth_6() { return &___atlasWidth_6; }
	inline void set_atlasWidth_6(int32_t value)
	{
		___atlasWidth_6 = value;
	}

	inline static int32_t get_offset_of_atlasHeight_7() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___atlasHeight_7)); }
	inline int32_t get_atlasHeight_7() const { return ___atlasHeight_7; }
	inline int32_t* get_address_of_atlasHeight_7() { return &___atlasHeight_7; }
	inline void set_atlasHeight_7(int32_t value)
	{
		___atlasHeight_7 = value;
	}

	inline static int32_t get_offset_of_characterSetSelectionMode_8() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___characterSetSelectionMode_8)); }
	inline int32_t get_characterSetSelectionMode_8() const { return ___characterSetSelectionMode_8; }
	inline int32_t* get_address_of_characterSetSelectionMode_8() { return &___characterSetSelectionMode_8; }
	inline void set_characterSetSelectionMode_8(int32_t value)
	{
		___characterSetSelectionMode_8 = value;
	}

	inline static int32_t get_offset_of_characterSequence_9() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___characterSequence_9)); }
	inline String_t* get_characterSequence_9() const { return ___characterSequence_9; }
	inline String_t** get_address_of_characterSequence_9() { return &___characterSequence_9; }
	inline void set_characterSequence_9(String_t* value)
	{
		___characterSequence_9 = value;
		Il2CppCodeGenWriteBarrier((&___characterSequence_9), value);
	}

	inline static int32_t get_offset_of_referencedFontAssetGUID_10() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___referencedFontAssetGUID_10)); }
	inline String_t* get_referencedFontAssetGUID_10() const { return ___referencedFontAssetGUID_10; }
	inline String_t** get_address_of_referencedFontAssetGUID_10() { return &___referencedFontAssetGUID_10; }
	inline void set_referencedFontAssetGUID_10(String_t* value)
	{
		___referencedFontAssetGUID_10 = value;
		Il2CppCodeGenWriteBarrier((&___referencedFontAssetGUID_10), value);
	}

	inline static int32_t get_offset_of_referencedTextAssetGUID_11() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___referencedTextAssetGUID_11)); }
	inline String_t* get_referencedTextAssetGUID_11() const { return ___referencedTextAssetGUID_11; }
	inline String_t** get_address_of_referencedTextAssetGUID_11() { return &___referencedTextAssetGUID_11; }
	inline void set_referencedTextAssetGUID_11(String_t* value)
	{
		___referencedTextAssetGUID_11 = value;
		Il2CppCodeGenWriteBarrier((&___referencedTextAssetGUID_11), value);
	}

	inline static int32_t get_offset_of_fontStyle_12() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___fontStyle_12)); }
	inline int32_t get_fontStyle_12() const { return ___fontStyle_12; }
	inline int32_t* get_address_of_fontStyle_12() { return &___fontStyle_12; }
	inline void set_fontStyle_12(int32_t value)
	{
		___fontStyle_12 = value;
	}

	inline static int32_t get_offset_of_fontStyleModifier_13() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___fontStyleModifier_13)); }
	inline float get_fontStyleModifier_13() const { return ___fontStyleModifier_13; }
	inline float* get_address_of_fontStyleModifier_13() { return &___fontStyleModifier_13; }
	inline void set_fontStyleModifier_13(float value)
	{
		___fontStyleModifier_13 = value;
	}

	inline static int32_t get_offset_of_renderMode_14() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___renderMode_14)); }
	inline int32_t get_renderMode_14() const { return ___renderMode_14; }
	inline int32_t* get_address_of_renderMode_14() { return &___renderMode_14; }
	inline void set_renderMode_14(int32_t value)
	{
		___renderMode_14 = value;
	}

	inline static int32_t get_offset_of_includeFontFeatures_15() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___includeFontFeatures_15)); }
	inline bool get_includeFontFeatures_15() const { return ___includeFontFeatures_15; }
	inline bool* get_address_of_includeFontFeatures_15() { return &___includeFontFeatures_15; }
	inline void set_includeFontFeatures_15(bool value)
	{
		___includeFontFeatures_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.FontAssetCreationSettings
struct FontAssetCreationSettings_t359369028_marshaled_pinvoke
{
	char* ___sourceFontFileName_0;
	char* ___sourceFontFileGUID_1;
	int32_t ___pointSizeSamplingMode_2;
	int32_t ___pointSize_3;
	int32_t ___padding_4;
	int32_t ___packingMode_5;
	int32_t ___atlasWidth_6;
	int32_t ___atlasHeight_7;
	int32_t ___characterSetSelectionMode_8;
	char* ___characterSequence_9;
	char* ___referencedFontAssetGUID_10;
	char* ___referencedTextAssetGUID_11;
	int32_t ___fontStyle_12;
	float ___fontStyleModifier_13;
	int32_t ___renderMode_14;
	int32_t ___includeFontFeatures_15;
};
// Native definition for COM marshalling of TMPro.FontAssetCreationSettings
struct FontAssetCreationSettings_t359369028_marshaled_com
{
	Il2CppChar* ___sourceFontFileName_0;
	Il2CppChar* ___sourceFontFileGUID_1;
	int32_t ___pointSizeSamplingMode_2;
	int32_t ___pointSize_3;
	int32_t ___padding_4;
	int32_t ___packingMode_5;
	int32_t ___atlasWidth_6;
	int32_t ___atlasHeight_7;
	int32_t ___characterSetSelectionMode_8;
	Il2CppChar* ___characterSequence_9;
	Il2CppChar* ___referencedFontAssetGUID_10;
	Il2CppChar* ___referencedTextAssetGUID_11;
	int32_t ___fontStyle_12;
	float ___fontStyleModifier_13;
	int32_t ___renderMode_14;
	int32_t ___includeFontFeatures_15;
};
#endif // FONTASSETCREATIONSETTINGS_T359369028_H
#ifndef GLYPHVALUERECORD_T4065874512_H
#define GLYPHVALUERECORD_T4065874512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.GlyphValueRecord
struct  GlyphValueRecord_t4065874512 
{
public:
	// System.Single TMPro.GlyphValueRecord::xPlacement
	float ___xPlacement_0;
	// System.Single TMPro.GlyphValueRecord::yPlacement
	float ___yPlacement_1;
	// System.Single TMPro.GlyphValueRecord::xAdvance
	float ___xAdvance_2;
	// System.Single TMPro.GlyphValueRecord::yAdvance
	float ___yAdvance_3;

public:
	inline static int32_t get_offset_of_xPlacement_0() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t4065874512, ___xPlacement_0)); }
	inline float get_xPlacement_0() const { return ___xPlacement_0; }
	inline float* get_address_of_xPlacement_0() { return &___xPlacement_0; }
	inline void set_xPlacement_0(float value)
	{
		___xPlacement_0 = value;
	}

	inline static int32_t get_offset_of_yPlacement_1() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t4065874512, ___yPlacement_1)); }
	inline float get_yPlacement_1() const { return ___yPlacement_1; }
	inline float* get_address_of_yPlacement_1() { return &___yPlacement_1; }
	inline void set_yPlacement_1(float value)
	{
		___yPlacement_1 = value;
	}

	inline static int32_t get_offset_of_xAdvance_2() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t4065874512, ___xAdvance_2)); }
	inline float get_xAdvance_2() const { return ___xAdvance_2; }
	inline float* get_address_of_xAdvance_2() { return &___xAdvance_2; }
	inline void set_xAdvance_2(float value)
	{
		___xAdvance_2 = value;
	}

	inline static int32_t get_offset_of_yAdvance_3() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t4065874512, ___yAdvance_3)); }
	inline float get_yAdvance_3() const { return ___yAdvance_3; }
	inline float* get_address_of_yAdvance_3() { return &___yAdvance_3; }
	inline void set_yAdvance_3(float value)
	{
		___yAdvance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHVALUERECORD_T4065874512_H
#ifndef KERNINGPAIRKEY_T536493877_H
#define KERNINGPAIRKEY_T536493877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningPairKey
struct  KerningPairKey_t536493877 
{
public:
	// System.UInt32 TMPro.KerningPairKey::ascii_Left
	uint32_t ___ascii_Left_0;
	// System.UInt32 TMPro.KerningPairKey::ascii_Right
	uint32_t ___ascii_Right_1;
	// System.UInt32 TMPro.KerningPairKey::key
	uint32_t ___key_2;

public:
	inline static int32_t get_offset_of_ascii_Left_0() { return static_cast<int32_t>(offsetof(KerningPairKey_t536493877, ___ascii_Left_0)); }
	inline uint32_t get_ascii_Left_0() const { return ___ascii_Left_0; }
	inline uint32_t* get_address_of_ascii_Left_0() { return &___ascii_Left_0; }
	inline void set_ascii_Left_0(uint32_t value)
	{
		___ascii_Left_0 = value;
	}

	inline static int32_t get_offset_of_ascii_Right_1() { return static_cast<int32_t>(offsetof(KerningPairKey_t536493877, ___ascii_Right_1)); }
	inline uint32_t get_ascii_Right_1() const { return ___ascii_Right_1; }
	inline uint32_t* get_address_of_ascii_Right_1() { return &___ascii_Right_1; }
	inline void set_ascii_Right_1(uint32_t value)
	{
		___ascii_Right_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(KerningPairKey_t536493877, ___key_2)); }
	inline uint32_t get_key_2() const { return ___key_2; }
	inline uint32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(uint32_t value)
	{
		___key_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGPAIRKEY_T536493877_H
#ifndef MATERIALREFERENCE_T1952344632_H
#define MATERIALREFERENCE_T1952344632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_t1952344632 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_t340375123 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_t340375123 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fontAsset_1)); }
	inline TMP_FontAsset_t364381626 * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t364381626 ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t364381626 * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___material_3)); }
	inline Material_t340375123 * get_material_3() const { return ___material_3; }
	inline Material_t340375123 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t340375123 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fallbackMaterial_6)); }
	inline Material_t340375123 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_t340375123 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_t340375123 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_T1952344632_H
#ifndef TMP_BASICXMLTAGSTACK_T2962628096_H
#define TMP_BASICXMLTAGSTACK_T2962628096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_BasicXmlTagStack
struct  TMP_BasicXmlTagStack_t2962628096 
{
public:
	// System.Byte TMPro.TMP_BasicXmlTagStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_BasicXmlTagStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_BasicXmlTagStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_BasicXmlTagStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_BasicXmlTagStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_BasicXmlTagStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_BasicXmlTagStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_BasicXmlTagStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_BasicXmlTagStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_BasicXmlTagStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_BASICXMLTAGSTACK_T2962628096_H
#ifndef TMP_GLYPH_T581847833_H
#define TMP_GLYPH_T581847833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Glyph
struct  TMP_Glyph_t581847833  : public TMP_TextElement_t129727469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_GLYPH_T581847833_H
#ifndef TMP_LINKINFO_T1092083476_H
#define TMP_LINKINFO_T1092083476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LinkInfo
struct  TMP_LinkInfo_t1092083476 
{
public:
	// TMPro.TMP_Text TMPro.TMP_LinkInfo::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// System.Int32 TMPro.TMP_LinkInfo::hashCode
	int32_t ___hashCode_1;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdFirstCharacterIndex
	int32_t ___linkIdFirstCharacterIndex_2;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdLength
	int32_t ___linkIdLength_3;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextfirstCharacterIndex
	int32_t ___linkTextfirstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextLength
	int32_t ___linkTextLength_5;
	// System.Char[] TMPro.TMP_LinkInfo::linkID
	CharU5BU5D_t3528271667* ___linkID_6;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_hashCode_1() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___hashCode_1)); }
	inline int32_t get_hashCode_1() const { return ___hashCode_1; }
	inline int32_t* get_address_of_hashCode_1() { return &___hashCode_1; }
	inline void set_hashCode_1(int32_t value)
	{
		___hashCode_1 = value;
	}

	inline static int32_t get_offset_of_linkIdFirstCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkIdFirstCharacterIndex_2)); }
	inline int32_t get_linkIdFirstCharacterIndex_2() const { return ___linkIdFirstCharacterIndex_2; }
	inline int32_t* get_address_of_linkIdFirstCharacterIndex_2() { return &___linkIdFirstCharacterIndex_2; }
	inline void set_linkIdFirstCharacterIndex_2(int32_t value)
	{
		___linkIdFirstCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_linkIdLength_3() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkIdLength_3)); }
	inline int32_t get_linkIdLength_3() const { return ___linkIdLength_3; }
	inline int32_t* get_address_of_linkIdLength_3() { return &___linkIdLength_3; }
	inline void set_linkIdLength_3(int32_t value)
	{
		___linkIdLength_3 = value;
	}

	inline static int32_t get_offset_of_linkTextfirstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkTextfirstCharacterIndex_4)); }
	inline int32_t get_linkTextfirstCharacterIndex_4() const { return ___linkTextfirstCharacterIndex_4; }
	inline int32_t* get_address_of_linkTextfirstCharacterIndex_4() { return &___linkTextfirstCharacterIndex_4; }
	inline void set_linkTextfirstCharacterIndex_4(int32_t value)
	{
		___linkTextfirstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_linkTextLength_5() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkTextLength_5)); }
	inline int32_t get_linkTextLength_5() const { return ___linkTextLength_5; }
	inline int32_t* get_address_of_linkTextLength_5() { return &___linkTextLength_5; }
	inline void set_linkTextLength_5(int32_t value)
	{
		___linkTextLength_5 = value;
	}

	inline static int32_t get_offset_of_linkID_6() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkID_6)); }
	inline CharU5BU5D_t3528271667* get_linkID_6() const { return ___linkID_6; }
	inline CharU5BU5D_t3528271667** get_address_of_linkID_6() { return &___linkID_6; }
	inline void set_linkID_6(CharU5BU5D_t3528271667* value)
	{
		___linkID_6 = value;
		Il2CppCodeGenWriteBarrier((&___linkID_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t1092083476_marshaled_pinvoke
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
// Native definition for COM marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t1092083476_marshaled_com
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
#endif // TMP_LINKINFO_T1092083476_H
#ifndef TMP_PAGEINFO_T2608430633_H
#define TMP_PAGEINFO_T2608430633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PageInfo
struct  TMP_PageInfo_t2608430633 
{
public:
	// System.Int32 TMPro.TMP_PageInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_0;
	// System.Int32 TMPro.TMP_PageInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_1;
	// System.Single TMPro.TMP_PageInfo::ascender
	float ___ascender_2;
	// System.Single TMPro.TMP_PageInfo::baseLine
	float ___baseLine_3;
	// System.Single TMPro.TMP_PageInfo::descender
	float ___descender_4;

public:
	inline static int32_t get_offset_of_firstCharacterIndex_0() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___firstCharacterIndex_0)); }
	inline int32_t get_firstCharacterIndex_0() const { return ___firstCharacterIndex_0; }
	inline int32_t* get_address_of_firstCharacterIndex_0() { return &___firstCharacterIndex_0; }
	inline void set_firstCharacterIndex_0(int32_t value)
	{
		___firstCharacterIndex_0 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___lastCharacterIndex_1)); }
	inline int32_t get_lastCharacterIndex_1() const { return ___lastCharacterIndex_1; }
	inline int32_t* get_address_of_lastCharacterIndex_1() { return &___lastCharacterIndex_1; }
	inline void set_lastCharacterIndex_1(int32_t value)
	{
		___lastCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_ascender_2() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___ascender_2)); }
	inline float get_ascender_2() const { return ___ascender_2; }
	inline float* get_address_of_ascender_2() { return &___ascender_2; }
	inline void set_ascender_2(float value)
	{
		___ascender_2 = value;
	}

	inline static int32_t get_offset_of_baseLine_3() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___baseLine_3)); }
	inline float get_baseLine_3() const { return ___baseLine_3; }
	inline float* get_address_of_baseLine_3() { return &___baseLine_3; }
	inline void set_baseLine_3(float value)
	{
		___baseLine_3 = value;
	}

	inline static int32_t get_offset_of_descender_4() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___descender_4)); }
	inline float get_descender_4() const { return ___descender_4; }
	inline float* get_address_of_descender_4() { return &___descender_4; }
	inline void set_descender_4(float value)
	{
		___descender_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PAGEINFO_T2608430633_H
#ifndef TMP_SPRITEINFO_T2726321384_H
#define TMP_SPRITEINFO_T2726321384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteInfo
struct  TMP_SpriteInfo_t2726321384 
{
public:
	// System.Int32 TMPro.TMP_SpriteInfo::spriteIndex
	int32_t ___spriteIndex_0;
	// System.Int32 TMPro.TMP_SpriteInfo::characterIndex
	int32_t ___characterIndex_1;
	// System.Int32 TMPro.TMP_SpriteInfo::vertexIndex
	int32_t ___vertexIndex_2;

public:
	inline static int32_t get_offset_of_spriteIndex_0() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t2726321384, ___spriteIndex_0)); }
	inline int32_t get_spriteIndex_0() const { return ___spriteIndex_0; }
	inline int32_t* get_address_of_spriteIndex_0() { return &___spriteIndex_0; }
	inline void set_spriteIndex_0(int32_t value)
	{
		___spriteIndex_0 = value;
	}

	inline static int32_t get_offset_of_characterIndex_1() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t2726321384, ___characterIndex_1)); }
	inline int32_t get_characterIndex_1() const { return ___characterIndex_1; }
	inline int32_t* get_address_of_characterIndex_1() { return &___characterIndex_1; }
	inline void set_characterIndex_1(int32_t value)
	{
		___characterIndex_1 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_2() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t2726321384, ___vertexIndex_2)); }
	inline int32_t get_vertexIndex_2() const { return ___vertexIndex_2; }
	inline int32_t* get_address_of_vertexIndex_2() { return &___vertexIndex_2; }
	inline void set_vertexIndex_2(int32_t value)
	{
		___vertexIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEINFO_T2726321384_H
#ifndef TMP_WORDINFO_T3331066303_H
#define TMP_WORDINFO_T3331066303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_WordInfo
struct  TMP_WordInfo_t3331066303 
{
public:
	// TMPro.TMP_Text TMPro.TMP_WordInfo::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// System.Int32 TMPro.TMP_WordInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_1;
	// System.Int32 TMPro.TMP_WordInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_2;
	// System.Int32 TMPro.TMP_WordInfo::characterCount
	int32_t ___characterCount_3;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_firstCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___firstCharacterIndex_1)); }
	inline int32_t get_firstCharacterIndex_1() const { return ___firstCharacterIndex_1; }
	inline int32_t* get_address_of_firstCharacterIndex_1() { return &___firstCharacterIndex_1; }
	inline void set_firstCharacterIndex_1(int32_t value)
	{
		___firstCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___lastCharacterIndex_2)); }
	inline int32_t get_lastCharacterIndex_2() const { return ___lastCharacterIndex_2; }
	inline int32_t* get_address_of_lastCharacterIndex_2() { return &___lastCharacterIndex_2; }
	inline void set_lastCharacterIndex_2(int32_t value)
	{
		___lastCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t3331066303_marshaled_pinvoke
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
// Native definition for COM marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t3331066303_marshaled_com
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
#endif // TMP_WORDINFO_T3331066303_H
#ifndef TMP_XMLTAGSTACK_1_T2514600297_H
#define TMP_XMLTAGSTACK_1_T2514600297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Int32>
struct  TMP_XmlTagStack_1_t2514600297 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Int32U5BU5D_t385246372* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___itemStack_0)); }
	inline Int32U5BU5D_t385246372* get_itemStack_0() const { return ___itemStack_0; }
	inline Int32U5BU5D_t385246372** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Int32U5BU5D_t385246372* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2514600297_H
#ifndef TMP_XMLTAGSTACK_1_T960921318_H
#define TMP_XMLTAGSTACK_1_T960921318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Single>
struct  TMP_XmlTagStack_1_t960921318 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	SingleU5BU5D_t1444911251* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	float ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___itemStack_0)); }
	inline SingleU5BU5D_t1444911251* get_itemStack_0() const { return ___itemStack_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(SingleU5BU5D_t1444911251* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___m_defaultItem_3)); }
	inline float get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline float* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(float value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T960921318_H
#ifndef TMP_XMLTAGSTACK_1_T3241710312_H
#define TMP_XMLTAGSTACK_1_T3241710312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient>
struct  TMP_XmlTagStack_1_t3241710312 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TMP_ColorGradientU5BU5D_t2496920137* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	TMP_ColorGradient_t3678055768 * ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___itemStack_0)); }
	inline TMP_ColorGradientU5BU5D_t2496920137* get_itemStack_0() const { return ___itemStack_0; }
	inline TMP_ColorGradientU5BU5D_t2496920137** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TMP_ColorGradientU5BU5D_t2496920137* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___m_defaultItem_3)); }
	inline TMP_ColorGradient_t3678055768 * get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline TMP_ColorGradient_t3678055768 ** get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(TMP_ColorGradient_t3678055768 * value)
	{
		___m_defaultItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultItem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T3241710312_H
#ifndef TAGATTRIBUTE_T688278634_H
#define TAGATTRIBUTE_T688278634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagAttribute
struct  TagAttribute_t688278634 
{
public:
	// System.Int32 TMPro.TagAttribute::startIndex
	int32_t ___startIndex_0;
	// System.Int32 TMPro.TagAttribute::length
	int32_t ___length_1;
	// System.Int32 TMPro.TagAttribute::hashCode
	int32_t ___hashCode_2;

public:
	inline static int32_t get_offset_of_startIndex_0() { return static_cast<int32_t>(offsetof(TagAttribute_t688278634, ___startIndex_0)); }
	inline int32_t get_startIndex_0() const { return ___startIndex_0; }
	inline int32_t* get_address_of_startIndex_0() { return &___startIndex_0; }
	inline void set_startIndex_0(int32_t value)
	{
		___startIndex_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(TagAttribute_t688278634, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_hashCode_2() { return static_cast<int32_t>(offsetof(TagAttribute_t688278634, ___hashCode_2)); }
	inline int32_t get_hashCode_2() const { return ___hashCode_2; }
	inline int32_t* get_address_of_hashCode_2() { return &___hashCode_2; }
	inline void set_hashCode_2(int32_t value)
	{
		___hashCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGATTRIBUTE_T688278634_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_t2562230146__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255369  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;
	// <PrivateImplementationDetails>/$ArrayType=40 <PrivateImplementationDetails>::$field-9E6378168821DBABB7EE3D0154346480FAC8AEF1
	U24ArrayTypeU3D40_t2865632059  ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1)); }
	inline U24ArrayTypeU3D40_t2865632059  get_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() const { return ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline U24ArrayTypeU3D40_t2865632059 * get_address_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return &___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline void set_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1(U24ArrayTypeU3D40_t2865632059  value)
	{
		___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#ifndef ABOUTSCREENSAMPLE_T2468051143_H
#define ABOUTSCREENSAMPLE_T2468051143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AboutManager/AboutScreenSample
struct  AboutScreenSample_t2468051143 
{
public:
	// System.Int32 AboutManager/AboutScreenSample::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AboutScreenSample_t2468051143, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABOUTSCREENSAMPLE_T2468051143_H
#ifndef ALIGNMENT_T2118925705_H
#define ALIGNMENT_T2118925705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.UnityUIMods.Alignment
struct  Alignment_t2118925705 
{
public:
	// System.Int32 BMJ.UnityUIMods.Alignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Alignment_t2118925705, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIGNMENT_T2118925705_H
#ifndef SCROLLALIGNMENT_T1755443740_H
#define SCROLLALIGNMENT_T1755443740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.UnityUIMods.ScrollRect_Mod001/ScrollAlignment
struct  ScrollAlignment_t1755443740 
{
public:
	// System.Int32 BMJ.UnityUIMods.ScrollRect_Mod001/ScrollAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScrollAlignment_t1755443740, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLALIGNMENT_T1755443740_H
#ifndef DESAMPLINGRATE_T2371977868_H
#define DESAMPLINGRATE_T2371977868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Coffee.UIExtensions.SoftMask/DesamplingRate
struct  DesamplingRate_t2371977868 
{
public:
	// System.Int32 Coffee.UIExtensions.SoftMask/DesamplingRate::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DesamplingRate_t2371977868, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESAMPLINGRATE_T2371977868_H
#ifndef SCROLLDIRECTION_T416655783_H
#define SCROLLDIRECTION_T416655783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollSnapEx/ScrollDirection
struct  ScrollDirection_t416655783 
{
public:
	// System.Int32 ScrollSnapEx/ScrollDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScrollDirection_t416655783, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLDIRECTION_T416655783_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef CARETPOSITION_T3997512201_H
#define CARETPOSITION_T3997512201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.CaretPosition
struct  CaretPosition_t3997512201 
{
public:
	// System.Int32 TMPro.CaretPosition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CaretPosition_t3997512201, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARETPOSITION_T3997512201_H
#ifndef COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T2554612104_H
#define COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T2554612104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Compute_DistanceTransform_EventTypes
struct  Compute_DistanceTransform_EventTypes_t2554612104 
{
public:
	// System.Int32 TMPro.Compute_DistanceTransform_EventTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Compute_DistanceTransform_EventTypes_t2554612104, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T2554612104_H
#ifndef EXTENTS_T3837212874_H
#define EXTENTS_T3837212874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_t3837212874 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_t2156229523  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_t2156229523  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___min_0)); }
	inline Vector2_t2156229523  get_min_0() const { return ___min_0; }
	inline Vector2_t2156229523 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2156229523  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___max_1)); }
	inline Vector2_t2156229523  get_max_1() const { return ___max_1; }
	inline Vector2_t2156229523 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2156229523  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_T3837212874_H
#ifndef FONTSTYLES_T3828945032_H
#define FONTSTYLES_T3828945032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t3828945032 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontStyles_t3828945032, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T3828945032_H
#ifndef FONTWEIGHTS_T3122883458_H
#define FONTWEIGHTS_T3122883458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontWeights
struct  FontWeights_t3122883458 
{
public:
	// System.Int32 TMPro.FontWeights::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontWeights_t3122883458, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTWEIGHTS_T3122883458_H
#ifndef KERNINGPAIR_T2270855589_H
#define KERNINGPAIR_T2270855589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningPair
struct  KerningPair_t2270855589  : public RuntimeObject
{
public:
	// System.UInt32 TMPro.KerningPair::m_FirstGlyph
	uint32_t ___m_FirstGlyph_0;
	// TMPro.GlyphValueRecord TMPro.KerningPair::m_FirstGlyphAdjustments
	GlyphValueRecord_t4065874512  ___m_FirstGlyphAdjustments_1;
	// System.UInt32 TMPro.KerningPair::m_SecondGlyph
	uint32_t ___m_SecondGlyph_2;
	// TMPro.GlyphValueRecord TMPro.KerningPair::m_SecondGlyphAdjustments
	GlyphValueRecord_t4065874512  ___m_SecondGlyphAdjustments_3;
	// System.Single TMPro.KerningPair::xOffset
	float ___xOffset_4;

public:
	inline static int32_t get_offset_of_m_FirstGlyph_0() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___m_FirstGlyph_0)); }
	inline uint32_t get_m_FirstGlyph_0() const { return ___m_FirstGlyph_0; }
	inline uint32_t* get_address_of_m_FirstGlyph_0() { return &___m_FirstGlyph_0; }
	inline void set_m_FirstGlyph_0(uint32_t value)
	{
		___m_FirstGlyph_0 = value;
	}

	inline static int32_t get_offset_of_m_FirstGlyphAdjustments_1() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___m_FirstGlyphAdjustments_1)); }
	inline GlyphValueRecord_t4065874512  get_m_FirstGlyphAdjustments_1() const { return ___m_FirstGlyphAdjustments_1; }
	inline GlyphValueRecord_t4065874512 * get_address_of_m_FirstGlyphAdjustments_1() { return &___m_FirstGlyphAdjustments_1; }
	inline void set_m_FirstGlyphAdjustments_1(GlyphValueRecord_t4065874512  value)
	{
		___m_FirstGlyphAdjustments_1 = value;
	}

	inline static int32_t get_offset_of_m_SecondGlyph_2() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___m_SecondGlyph_2)); }
	inline uint32_t get_m_SecondGlyph_2() const { return ___m_SecondGlyph_2; }
	inline uint32_t* get_address_of_m_SecondGlyph_2() { return &___m_SecondGlyph_2; }
	inline void set_m_SecondGlyph_2(uint32_t value)
	{
		___m_SecondGlyph_2 = value;
	}

	inline static int32_t get_offset_of_m_SecondGlyphAdjustments_3() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___m_SecondGlyphAdjustments_3)); }
	inline GlyphValueRecord_t4065874512  get_m_SecondGlyphAdjustments_3() const { return ___m_SecondGlyphAdjustments_3; }
	inline GlyphValueRecord_t4065874512 * get_address_of_m_SecondGlyphAdjustments_3() { return &___m_SecondGlyphAdjustments_3; }
	inline void set_m_SecondGlyphAdjustments_3(GlyphValueRecord_t4065874512  value)
	{
		___m_SecondGlyphAdjustments_3 = value;
	}

	inline static int32_t get_offset_of_xOffset_4() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___xOffset_4)); }
	inline float get_xOffset_4() const { return ___xOffset_4; }
	inline float* get_address_of_xOffset_4() { return &___xOffset_4; }
	inline void set_xOffset_4(float value)
	{
		___xOffset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGPAIR_T2270855589_H
#ifndef MASKINGOFFSETMODE_T2266644590_H
#define MASKINGOFFSETMODE_T2266644590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingOffsetMode
struct  MaskingOffsetMode_t2266644590 
{
public:
	// System.Int32 TMPro.MaskingOffsetMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaskingOffsetMode_t2266644590, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGOFFSETMODE_T2266644590_H
#ifndef MESH_EXTENTS_T3388355125_H
#define MESH_EXTENTS_T3388355125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Mesh_Extents
struct  Mesh_Extents_t3388355125 
{
public:
	// UnityEngine.Vector2 TMPro.Mesh_Extents::min
	Vector2_t2156229523  ___min_0;
	// UnityEngine.Vector2 TMPro.Mesh_Extents::max
	Vector2_t2156229523  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Mesh_Extents_t3388355125, ___min_0)); }
	inline Vector2_t2156229523  get_min_0() const { return ___min_0; }
	inline Vector2_t2156229523 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2156229523  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Mesh_Extents_t3388355125, ___max_1)); }
	inline Vector2_t2156229523  get_max_1() const { return ___max_1; }
	inline Vector2_t2156229523 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2156229523  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_EXTENTS_T3388355125_H
#ifndef TMP_MATH_T624304809_H
#define TMP_MATH_T624304809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Math
struct  TMP_Math_t624304809  : public RuntimeObject
{
public:

public:
};

struct TMP_Math_t624304809_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_Math::MAX_16BIT
	Vector2_t2156229523  ___MAX_16BIT_6;
	// UnityEngine.Vector2 TMPro.TMP_Math::MIN_16BIT
	Vector2_t2156229523  ___MIN_16BIT_7;

public:
	inline static int32_t get_offset_of_MAX_16BIT_6() { return static_cast<int32_t>(offsetof(TMP_Math_t624304809_StaticFields, ___MAX_16BIT_6)); }
	inline Vector2_t2156229523  get_MAX_16BIT_6() const { return ___MAX_16BIT_6; }
	inline Vector2_t2156229523 * get_address_of_MAX_16BIT_6() { return &___MAX_16BIT_6; }
	inline void set_MAX_16BIT_6(Vector2_t2156229523  value)
	{
		___MAX_16BIT_6 = value;
	}

	inline static int32_t get_offset_of_MIN_16BIT_7() { return static_cast<int32_t>(offsetof(TMP_Math_t624304809_StaticFields, ___MIN_16BIT_7)); }
	inline Vector2_t2156229523  get_MIN_16BIT_7() const { return ___MIN_16BIT_7; }
	inline Vector2_t2156229523 * get_address_of_MIN_16BIT_7() { return &___MIN_16BIT_7; }
	inline void set_MIN_16BIT_7(Vector2_t2156229523  value)
	{
		___MIN_16BIT_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_MATH_T624304809_H
#ifndef TEXTINPUTSOURCES_T1522115805_H
#define TEXTINPUTSOURCES_T1522115805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text/TextInputSources
struct  TextInputSources_t1522115805 
{
public:
	// System.Int32 TMPro.TMP_Text/TextInputSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextInputSources_t1522115805, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTINPUTSOURCES_T1522115805_H
#ifndef TMP_TEXTELEMENTTYPE_T1276645592_H
#define TMP_TEXTELEMENTTYPE_T1276645592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_t1276645592 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TMP_TextElementType_t1276645592, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_T1276645592_H
#ifndef TMP_TEXTINFO_T3598145122_H
#define TMP_TEXTINFO_T3598145122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextInfo
struct  TMP_TextInfo_t3598145122  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.TMP_TextInfo::textComponent
	TMP_Text_t2599618874 * ___textComponent_2;
	// System.Int32 TMPro.TMP_TextInfo::characterCount
	int32_t ___characterCount_3;
	// System.Int32 TMPro.TMP_TextInfo::spriteCount
	int32_t ___spriteCount_4;
	// System.Int32 TMPro.TMP_TextInfo::spaceCount
	int32_t ___spaceCount_5;
	// System.Int32 TMPro.TMP_TextInfo::wordCount
	int32_t ___wordCount_6;
	// System.Int32 TMPro.TMP_TextInfo::linkCount
	int32_t ___linkCount_7;
	// System.Int32 TMPro.TMP_TextInfo::lineCount
	int32_t ___lineCount_8;
	// System.Int32 TMPro.TMP_TextInfo::pageCount
	int32_t ___pageCount_9;
	// System.Int32 TMPro.TMP_TextInfo::materialCount
	int32_t ___materialCount_10;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_TextInfo::characterInfo
	TMP_CharacterInfoU5BU5D_t1930184704* ___characterInfo_11;
	// TMPro.TMP_WordInfo[] TMPro.TMP_TextInfo::wordInfo
	TMP_WordInfoU5BU5D_t3766301798* ___wordInfo_12;
	// TMPro.TMP_LinkInfo[] TMPro.TMP_TextInfo::linkInfo
	TMP_LinkInfoU5BU5D_t3558768157* ___linkInfo_13;
	// TMPro.TMP_LineInfo[] TMPro.TMP_TextInfo::lineInfo
	TMP_LineInfoU5BU5D_t4120149533* ___lineInfo_14;
	// TMPro.TMP_PageInfo[] TMPro.TMP_TextInfo::pageInfo
	TMP_PageInfoU5BU5D_t2463031060* ___pageInfo_15;
	// TMPro.TMP_MeshInfo[] TMPro.TMP_TextInfo::meshInfo
	TMP_MeshInfoU5BU5D_t3365986247* ___meshInfo_16;
	// TMPro.TMP_MeshInfo[] TMPro.TMP_TextInfo::m_CachedMeshInfo
	TMP_MeshInfoU5BU5D_t3365986247* ___m_CachedMeshInfo_17;

public:
	inline static int32_t get_offset_of_textComponent_2() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___textComponent_2)); }
	inline TMP_Text_t2599618874 * get_textComponent_2() const { return ___textComponent_2; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_2() { return &___textComponent_2; }
	inline void set_textComponent_2(TMP_Text_t2599618874 * value)
	{
		___textComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_2), value);
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}

	inline static int32_t get_offset_of_spriteCount_4() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___spriteCount_4)); }
	inline int32_t get_spriteCount_4() const { return ___spriteCount_4; }
	inline int32_t* get_address_of_spriteCount_4() { return &___spriteCount_4; }
	inline void set_spriteCount_4(int32_t value)
	{
		___spriteCount_4 = value;
	}

	inline static int32_t get_offset_of_spaceCount_5() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___spaceCount_5)); }
	inline int32_t get_spaceCount_5() const { return ___spaceCount_5; }
	inline int32_t* get_address_of_spaceCount_5() { return &___spaceCount_5; }
	inline void set_spaceCount_5(int32_t value)
	{
		___spaceCount_5 = value;
	}

	inline static int32_t get_offset_of_wordCount_6() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___wordCount_6)); }
	inline int32_t get_wordCount_6() const { return ___wordCount_6; }
	inline int32_t* get_address_of_wordCount_6() { return &___wordCount_6; }
	inline void set_wordCount_6(int32_t value)
	{
		___wordCount_6 = value;
	}

	inline static int32_t get_offset_of_linkCount_7() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___linkCount_7)); }
	inline int32_t get_linkCount_7() const { return ___linkCount_7; }
	inline int32_t* get_address_of_linkCount_7() { return &___linkCount_7; }
	inline void set_linkCount_7(int32_t value)
	{
		___linkCount_7 = value;
	}

	inline static int32_t get_offset_of_lineCount_8() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___lineCount_8)); }
	inline int32_t get_lineCount_8() const { return ___lineCount_8; }
	inline int32_t* get_address_of_lineCount_8() { return &___lineCount_8; }
	inline void set_lineCount_8(int32_t value)
	{
		___lineCount_8 = value;
	}

	inline static int32_t get_offset_of_pageCount_9() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___pageCount_9)); }
	inline int32_t get_pageCount_9() const { return ___pageCount_9; }
	inline int32_t* get_address_of_pageCount_9() { return &___pageCount_9; }
	inline void set_pageCount_9(int32_t value)
	{
		___pageCount_9 = value;
	}

	inline static int32_t get_offset_of_materialCount_10() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___materialCount_10)); }
	inline int32_t get_materialCount_10() const { return ___materialCount_10; }
	inline int32_t* get_address_of_materialCount_10() { return &___materialCount_10; }
	inline void set_materialCount_10(int32_t value)
	{
		___materialCount_10 = value;
	}

	inline static int32_t get_offset_of_characterInfo_11() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___characterInfo_11)); }
	inline TMP_CharacterInfoU5BU5D_t1930184704* get_characterInfo_11() const { return ___characterInfo_11; }
	inline TMP_CharacterInfoU5BU5D_t1930184704** get_address_of_characterInfo_11() { return &___characterInfo_11; }
	inline void set_characterInfo_11(TMP_CharacterInfoU5BU5D_t1930184704* value)
	{
		___characterInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___characterInfo_11), value);
	}

	inline static int32_t get_offset_of_wordInfo_12() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___wordInfo_12)); }
	inline TMP_WordInfoU5BU5D_t3766301798* get_wordInfo_12() const { return ___wordInfo_12; }
	inline TMP_WordInfoU5BU5D_t3766301798** get_address_of_wordInfo_12() { return &___wordInfo_12; }
	inline void set_wordInfo_12(TMP_WordInfoU5BU5D_t3766301798* value)
	{
		___wordInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___wordInfo_12), value);
	}

	inline static int32_t get_offset_of_linkInfo_13() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___linkInfo_13)); }
	inline TMP_LinkInfoU5BU5D_t3558768157* get_linkInfo_13() const { return ___linkInfo_13; }
	inline TMP_LinkInfoU5BU5D_t3558768157** get_address_of_linkInfo_13() { return &___linkInfo_13; }
	inline void set_linkInfo_13(TMP_LinkInfoU5BU5D_t3558768157* value)
	{
		___linkInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&___linkInfo_13), value);
	}

	inline static int32_t get_offset_of_lineInfo_14() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___lineInfo_14)); }
	inline TMP_LineInfoU5BU5D_t4120149533* get_lineInfo_14() const { return ___lineInfo_14; }
	inline TMP_LineInfoU5BU5D_t4120149533** get_address_of_lineInfo_14() { return &___lineInfo_14; }
	inline void set_lineInfo_14(TMP_LineInfoU5BU5D_t4120149533* value)
	{
		___lineInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___lineInfo_14), value);
	}

	inline static int32_t get_offset_of_pageInfo_15() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___pageInfo_15)); }
	inline TMP_PageInfoU5BU5D_t2463031060* get_pageInfo_15() const { return ___pageInfo_15; }
	inline TMP_PageInfoU5BU5D_t2463031060** get_address_of_pageInfo_15() { return &___pageInfo_15; }
	inline void set_pageInfo_15(TMP_PageInfoU5BU5D_t2463031060* value)
	{
		___pageInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___pageInfo_15), value);
	}

	inline static int32_t get_offset_of_meshInfo_16() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___meshInfo_16)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_meshInfo_16() const { return ___meshInfo_16; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_meshInfo_16() { return &___meshInfo_16; }
	inline void set_meshInfo_16(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___meshInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___meshInfo_16), value);
	}

	inline static int32_t get_offset_of_m_CachedMeshInfo_17() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___m_CachedMeshInfo_17)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_m_CachedMeshInfo_17() const { return ___m_CachedMeshInfo_17; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_m_CachedMeshInfo_17() { return &___m_CachedMeshInfo_17; }
	inline void set_m_CachedMeshInfo_17(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___m_CachedMeshInfo_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedMeshInfo_17), value);
	}
};

struct TMP_TextInfo_t3598145122_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_TextInfo::k_InfinityVectorPositive
	Vector2_t2156229523  ___k_InfinityVectorPositive_0;
	// UnityEngine.Vector2 TMPro.TMP_TextInfo::k_InfinityVectorNegative
	Vector2_t2156229523  ___k_InfinityVectorNegative_1;

public:
	inline static int32_t get_offset_of_k_InfinityVectorPositive_0() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122_StaticFields, ___k_InfinityVectorPositive_0)); }
	inline Vector2_t2156229523  get_k_InfinityVectorPositive_0() const { return ___k_InfinityVectorPositive_0; }
	inline Vector2_t2156229523 * get_address_of_k_InfinityVectorPositive_0() { return &___k_InfinityVectorPositive_0; }
	inline void set_k_InfinityVectorPositive_0(Vector2_t2156229523  value)
	{
		___k_InfinityVectorPositive_0 = value;
	}

	inline static int32_t get_offset_of_k_InfinityVectorNegative_1() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122_StaticFields, ___k_InfinityVectorNegative_1)); }
	inline Vector2_t2156229523  get_k_InfinityVectorNegative_1() const { return ___k_InfinityVectorNegative_1; }
	inline Vector2_t2156229523 * get_address_of_k_InfinityVectorNegative_1() { return &___k_InfinityVectorNegative_1; }
	inline void set_k_InfinityVectorNegative_1(Vector2_t2156229523  value)
	{
		___k_InfinityVectorNegative_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTINFO_T3598145122_H
#ifndef LINESEGMENT_T1526544958_H
#define LINESEGMENT_T1526544958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextUtilities/LineSegment
struct  LineSegment_t1526544958 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_TextUtilities/LineSegment::Point1
	Vector3_t3722313464  ___Point1_0;
	// UnityEngine.Vector3 TMPro.TMP_TextUtilities/LineSegment::Point2
	Vector3_t3722313464  ___Point2_1;

public:
	inline static int32_t get_offset_of_Point1_0() { return static_cast<int32_t>(offsetof(LineSegment_t1526544958, ___Point1_0)); }
	inline Vector3_t3722313464  get_Point1_0() const { return ___Point1_0; }
	inline Vector3_t3722313464 * get_address_of_Point1_0() { return &___Point1_0; }
	inline void set_Point1_0(Vector3_t3722313464  value)
	{
		___Point1_0 = value;
	}

	inline static int32_t get_offset_of_Point2_1() { return static_cast<int32_t>(offsetof(LineSegment_t1526544958, ___Point2_1)); }
	inline Vector3_t3722313464  get_Point2_1() const { return ___Point2_1; }
	inline Vector3_t3722313464 * get_address_of_Point2_1() { return &___Point2_1; }
	inline void set_Point2_1(Vector3_t3722313464  value)
	{
		___Point2_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESEGMENT_T1526544958_H
#ifndef TMP_VERTEX_T2404176824_H
#define TMP_VERTEX_T2404176824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Vertex
struct  TMP_Vertex_t2404176824 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_Vertex::position
	Vector3_t3722313464  ___position_0;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv
	Vector2_t2156229523  ___uv_1;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv2
	Vector2_t2156229523  ___uv2_2;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv4
	Vector2_t2156229523  ___uv4_3;
	// UnityEngine.Color32 TMPro.TMP_Vertex::color
	Color32_t2600501292  ___color_4;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___position_0)); }
	inline Vector3_t3722313464  get_position_0() const { return ___position_0; }
	inline Vector3_t3722313464 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3722313464  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv_1)); }
	inline Vector2_t2156229523  get_uv_1() const { return ___uv_1; }
	inline Vector2_t2156229523 * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Vector2_t2156229523  value)
	{
		___uv_1 = value;
	}

	inline static int32_t get_offset_of_uv2_2() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv2_2)); }
	inline Vector2_t2156229523  get_uv2_2() const { return ___uv2_2; }
	inline Vector2_t2156229523 * get_address_of_uv2_2() { return &___uv2_2; }
	inline void set_uv2_2(Vector2_t2156229523  value)
	{
		___uv2_2 = value;
	}

	inline static int32_t get_offset_of_uv4_3() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv4_3)); }
	inline Vector2_t2156229523  get_uv4_3() const { return ___uv4_3; }
	inline Vector2_t2156229523 * get_address_of_uv4_3() { return &___uv4_3; }
	inline void set_uv4_3(Vector2_t2156229523  value)
	{
		___uv4_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___color_4)); }
	inline Color32_t2600501292  get_color_4() const { return ___color_4; }
	inline Color32_t2600501292 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color32_t2600501292  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEX_T2404176824_H
#ifndef TMP_VERTEXDATAUPDATEFLAGS_T388000256_H
#define TMP_VERTEXDATAUPDATEFLAGS_T388000256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_VertexDataUpdateFlags
struct  TMP_VertexDataUpdateFlags_t388000256 
{
public:
	// System.Int32 TMPro.TMP_VertexDataUpdateFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TMP_VertexDataUpdateFlags_t388000256, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEXDATAUPDATEFLAGS_T388000256_H
#ifndef TMP_XMLTAGSTACK_1_T1515999176_H
#define TMP_XMLTAGSTACK_1_T1515999176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference>
struct  TMP_XmlTagStack_1_t1515999176 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	MaterialReferenceU5BU5D_t648826345* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	MaterialReference_t1952344632  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___itemStack_0)); }
	inline MaterialReferenceU5BU5D_t648826345* get_itemStack_0() const { return ___itemStack_0; }
	inline MaterialReferenceU5BU5D_t648826345** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(MaterialReferenceU5BU5D_t648826345* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___m_defaultItem_3)); }
	inline MaterialReference_t1952344632  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline MaterialReference_t1952344632 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(MaterialReference_t1952344632  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T1515999176_H
#ifndef TMP_XMLTAGSTACK_1_T2164155836_H
#define TMP_XMLTAGSTACK_1_T2164155836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>
struct  TMP_XmlTagStack_1_t2164155836 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Color32U5BU5D_t3850468773* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	Color32_t2600501292  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___itemStack_0)); }
	inline Color32U5BU5D_t3850468773* get_itemStack_0() const { return ___itemStack_0; }
	inline Color32U5BU5D_t3850468773** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Color32U5BU5D_t3850468773* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___m_defaultItem_3)); }
	inline Color32_t2600501292  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline Color32_t2600501292 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(Color32_t2600501292  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2164155836_H
#ifndef TAGTYPE_T123236451_H
#define TAGTYPE_T123236451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagType
struct  TagType_t123236451 
{
public:
	// System.Int32 TMPro.TagType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TagType_t123236451, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGTYPE_T123236451_H
#ifndef TAGUNITS_T1169424683_H
#define TAGUNITS_T1169424683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagUnits
struct  TagUnits_t1169424683 
{
public:
	// System.Int32 TMPro.TagUnits::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TagUnits_t1169424683, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGUNITS_T1169424683_H
#ifndef TEXTALIGNMENTOPTIONS_T4036791236_H
#define TEXTALIGNMENTOPTIONS_T4036791236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t4036791236 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t4036791236, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T4036791236_H
#ifndef TEXTOVERFLOWMODES_T1430035314_H
#define TEXTOVERFLOWMODES_T1430035314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextOverflowModes
struct  TextOverflowModes_t1430035314 
{
public:
	// System.Int32 TMPro.TextOverflowModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextOverflowModes_t1430035314, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTOVERFLOWMODES_T1430035314_H
#ifndef TEXTRENDERFLAGS_T2418684345_H
#define TEXTRENDERFLAGS_T2418684345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextRenderFlags
struct  TextRenderFlags_t2418684345 
{
public:
	// System.Int32 TMPro.TextRenderFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextRenderFlags_t2418684345, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRENDERFLAGS_T2418684345_H
#ifndef TEXTUREMAPPINGOPTIONS_T270963663_H
#define TEXTUREMAPPINGOPTIONS_T270963663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextureMappingOptions
struct  TextureMappingOptions_t270963663 
{
public:
	// System.Int32 TMPro.TextureMappingOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureMappingOptions_t270963663, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREMAPPINGOPTIONS_T270963663_H
#ifndef VERTEXGRADIENT_T345148380_H
#define VERTEXGRADIENT_T345148380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_t345148380 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t2555686324  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t2555686324  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t2555686324  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t2555686324  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___topLeft_0)); }
	inline Color_t2555686324  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t2555686324 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t2555686324  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___topRight_1)); }
	inline Color_t2555686324  get_topRight_1() const { return ___topRight_1; }
	inline Color_t2555686324 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t2555686324  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___bottomLeft_2)); }
	inline Color_t2555686324  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t2555686324 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t2555686324  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___bottomRight_3)); }
	inline Color_t2555686324  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t2555686324 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t2555686324  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_T345148380_H
#ifndef VERTEXSORTINGORDER_T2659893934_H
#define VERTEXSORTINGORDER_T2659893934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexSortingOrder
struct  VertexSortingOrder_t2659893934 
{
public:
	// System.Int32 TMPro.VertexSortingOrder::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VertexSortingOrder_t2659893934, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSORTINGORDER_T2659893934_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TEXTANCHOR_T2035777396_H
#define TEXTANCHOR_T2035777396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t2035777396 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAnchor_t2035777396, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T2035777396_H
#ifndef ASPECTMODE_T3417192999_H
#define ASPECTMODE_T3417192999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.AspectRatioFitter/AspectMode
struct  AspectMode_t3417192999 
{
public:
	// System.Int32 UnityEngine.UI.AspectRatioFitter/AspectMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AspectMode_t3417192999, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTMODE_T3417192999_H
#ifndef AXIS_T3613393006_H
#define AXIS_T3613393006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Axis
struct  Axis_t3613393006 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Axis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Axis_t3613393006, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T3613393006_H
#ifndef CONSTRAINT_T814224393_H
#define CONSTRAINT_T814224393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Constraint
struct  Constraint_t814224393 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Constraint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Constraint_t814224393, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINT_T814224393_H
#ifndef CORNER_T1493259673_H
#define CORNER_T1493259673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Corner
struct  Corner_t1493259673 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Corner::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Corner_t1493259673, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNER_T1493259673_H
#ifndef MOVEMENTTYPE_T4072922106_H
#define MOVEMENTTYPE_T4072922106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect/MovementType
struct  MovementType_t4072922106 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/MovementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MovementType_t4072922106, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTTYPE_T4072922106_H
#ifndef SCROLLBARVISIBILITY_T705693775_H
#define SCROLLBARVISIBILITY_T705693775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect/ScrollbarVisibility
struct  ScrollbarVisibility_t705693775 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/ScrollbarVisibility::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScrollbarVisibility_t705693775, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLBARVISIBILITY_T705693775_H
#ifndef STATUS_T1100905814_H
#define STATUS_T1100905814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/Status
struct  Status_t1100905814 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/Status::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Status_t1100905814, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T1100905814_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef CARETINFO_T841780893_H
#define CARETINFO_T841780893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.CaretInfo
struct  CaretInfo_t841780893 
{
public:
	// System.Int32 TMPro.CaretInfo::index
	int32_t ___index_0;
	// TMPro.CaretPosition TMPro.CaretInfo::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(CaretInfo_t841780893, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(CaretInfo_t841780893, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARETINFO_T841780893_H
#ifndef COMPUTE_DT_EVENTARGS_T1071353166_H
#define COMPUTE_DT_EVENTARGS_T1071353166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Compute_DT_EventArgs
struct  Compute_DT_EventArgs_t1071353166  : public RuntimeObject
{
public:
	// TMPro.Compute_DistanceTransform_EventTypes TMPro.Compute_DT_EventArgs::EventType
	int32_t ___EventType_0;
	// System.Single TMPro.Compute_DT_EventArgs::ProgressPercentage
	float ___ProgressPercentage_1;
	// UnityEngine.Color[] TMPro.Compute_DT_EventArgs::Colors
	ColorU5BU5D_t941916413* ___Colors_2;

public:
	inline static int32_t get_offset_of_EventType_0() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t1071353166, ___EventType_0)); }
	inline int32_t get_EventType_0() const { return ___EventType_0; }
	inline int32_t* get_address_of_EventType_0() { return &___EventType_0; }
	inline void set_EventType_0(int32_t value)
	{
		___EventType_0 = value;
	}

	inline static int32_t get_offset_of_ProgressPercentage_1() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t1071353166, ___ProgressPercentage_1)); }
	inline float get_ProgressPercentage_1() const { return ___ProgressPercentage_1; }
	inline float* get_address_of_ProgressPercentage_1() { return &___ProgressPercentage_1; }
	inline void set_ProgressPercentage_1(float value)
	{
		___ProgressPercentage_1 = value;
	}

	inline static int32_t get_offset_of_Colors_2() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t1071353166, ___Colors_2)); }
	inline ColorU5BU5D_t941916413* get_Colors_2() const { return ___Colors_2; }
	inline ColorU5BU5D_t941916413** get_address_of_Colors_2() { return &___Colors_2; }
	inline void set_Colors_2(ColorU5BU5D_t941916413* value)
	{
		___Colors_2 = value;
		Il2CppCodeGenWriteBarrier((&___Colors_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTE_DT_EVENTARGS_T1071353166_H
#ifndef TMP_CHARACTERINFO_T3185626797_H
#define TMP_CHARACTERINFO_T3185626797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_CharacterInfo
struct  TMP_CharacterInfo_t3185626797 
{
public:
	// System.Char TMPro.TMP_CharacterInfo::character
	Il2CppChar ___character_0;
	// System.Int32 TMPro.TMP_CharacterInfo::index
	int32_t ___index_1;
	// TMPro.TMP_TextElementType TMPro.TMP_CharacterInfo::elementType
	int32_t ___elementType_2;
	// TMPro.TMP_TextElement TMPro.TMP_CharacterInfo::textElement
	TMP_TextElement_t129727469 * ___textElement_3;
	// TMPro.TMP_FontAsset TMPro.TMP_CharacterInfo::fontAsset
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	// TMPro.TMP_SpriteAsset TMPro.TMP_CharacterInfo::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	// System.Int32 TMPro.TMP_CharacterInfo::spriteIndex
	int32_t ___spriteIndex_6;
	// UnityEngine.Material TMPro.TMP_CharacterInfo::material
	Material_t340375123 * ___material_7;
	// System.Int32 TMPro.TMP_CharacterInfo::materialReferenceIndex
	int32_t ___materialReferenceIndex_8;
	// System.Boolean TMPro.TMP_CharacterInfo::isUsingAlternateTypeface
	bool ___isUsingAlternateTypeface_9;
	// System.Single TMPro.TMP_CharacterInfo::pointSize
	float ___pointSize_10;
	// System.Int32 TMPro.TMP_CharacterInfo::lineNumber
	int32_t ___lineNumber_11;
	// System.Int32 TMPro.TMP_CharacterInfo::pageNumber
	int32_t ___pageNumber_12;
	// System.Int32 TMPro.TMP_CharacterInfo::vertexIndex
	int32_t ___vertexIndex_13;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TL
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BL
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TR
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BR
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topLeft
	Vector3_t3722313464  ___topLeft_18;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomLeft
	Vector3_t3722313464  ___bottomLeft_19;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topRight
	Vector3_t3722313464  ___topRight_20;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomRight
	Vector3_t3722313464  ___bottomRight_21;
	// System.Single TMPro.TMP_CharacterInfo::origin
	float ___origin_22;
	// System.Single TMPro.TMP_CharacterInfo::ascender
	float ___ascender_23;
	// System.Single TMPro.TMP_CharacterInfo::baseLine
	float ___baseLine_24;
	// System.Single TMPro.TMP_CharacterInfo::descender
	float ___descender_25;
	// System.Single TMPro.TMP_CharacterInfo::xAdvance
	float ___xAdvance_26;
	// System.Single TMPro.TMP_CharacterInfo::aspectRatio
	float ___aspectRatio_27;
	// System.Single TMPro.TMP_CharacterInfo::scale
	float ___scale_28;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::color
	Color32_t2600501292  ___color_29;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::underlineColor
	Color32_t2600501292  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::strikethroughColor
	Color32_t2600501292  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::highlightColor
	Color32_t2600501292  ___highlightColor_32;
	// TMPro.FontStyles TMPro.TMP_CharacterInfo::style
	int32_t ___style_33;
	// System.Boolean TMPro.TMP_CharacterInfo::isVisible
	bool ___isVisible_34;

public:
	inline static int32_t get_offset_of_character_0() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___character_0)); }
	inline Il2CppChar get_character_0() const { return ___character_0; }
	inline Il2CppChar* get_address_of_character_0() { return &___character_0; }
	inline void set_character_0(Il2CppChar value)
	{
		___character_0 = value;
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_elementType_2() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___elementType_2)); }
	inline int32_t get_elementType_2() const { return ___elementType_2; }
	inline int32_t* get_address_of_elementType_2() { return &___elementType_2; }
	inline void set_elementType_2(int32_t value)
	{
		___elementType_2 = value;
	}

	inline static int32_t get_offset_of_textElement_3() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___textElement_3)); }
	inline TMP_TextElement_t129727469 * get_textElement_3() const { return ___textElement_3; }
	inline TMP_TextElement_t129727469 ** get_address_of_textElement_3() { return &___textElement_3; }
	inline void set_textElement_3(TMP_TextElement_t129727469 * value)
	{
		___textElement_3 = value;
		Il2CppCodeGenWriteBarrier((&___textElement_3), value);
	}

	inline static int32_t get_offset_of_fontAsset_4() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___fontAsset_4)); }
	inline TMP_FontAsset_t364381626 * get_fontAsset_4() const { return ___fontAsset_4; }
	inline TMP_FontAsset_t364381626 ** get_address_of_fontAsset_4() { return &___fontAsset_4; }
	inline void set_fontAsset_4(TMP_FontAsset_t364381626 * value)
	{
		___fontAsset_4 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_4), value);
	}

	inline static int32_t get_offset_of_spriteAsset_5() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___spriteAsset_5)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_5() const { return ___spriteAsset_5; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_5() { return &___spriteAsset_5; }
	inline void set_spriteAsset_5(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_5), value);
	}

	inline static int32_t get_offset_of_spriteIndex_6() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___spriteIndex_6)); }
	inline int32_t get_spriteIndex_6() const { return ___spriteIndex_6; }
	inline int32_t* get_address_of_spriteIndex_6() { return &___spriteIndex_6; }
	inline void set_spriteIndex_6(int32_t value)
	{
		___spriteIndex_6 = value;
	}

	inline static int32_t get_offset_of_material_7() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___material_7)); }
	inline Material_t340375123 * get_material_7() const { return ___material_7; }
	inline Material_t340375123 ** get_address_of_material_7() { return &___material_7; }
	inline void set_material_7(Material_t340375123 * value)
	{
		___material_7 = value;
		Il2CppCodeGenWriteBarrier((&___material_7), value);
	}

	inline static int32_t get_offset_of_materialReferenceIndex_8() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___materialReferenceIndex_8)); }
	inline int32_t get_materialReferenceIndex_8() const { return ___materialReferenceIndex_8; }
	inline int32_t* get_address_of_materialReferenceIndex_8() { return &___materialReferenceIndex_8; }
	inline void set_materialReferenceIndex_8(int32_t value)
	{
		___materialReferenceIndex_8 = value;
	}

	inline static int32_t get_offset_of_isUsingAlternateTypeface_9() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___isUsingAlternateTypeface_9)); }
	inline bool get_isUsingAlternateTypeface_9() const { return ___isUsingAlternateTypeface_9; }
	inline bool* get_address_of_isUsingAlternateTypeface_9() { return &___isUsingAlternateTypeface_9; }
	inline void set_isUsingAlternateTypeface_9(bool value)
	{
		___isUsingAlternateTypeface_9 = value;
	}

	inline static int32_t get_offset_of_pointSize_10() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___pointSize_10)); }
	inline float get_pointSize_10() const { return ___pointSize_10; }
	inline float* get_address_of_pointSize_10() { return &___pointSize_10; }
	inline void set_pointSize_10(float value)
	{
		___pointSize_10 = value;
	}

	inline static int32_t get_offset_of_lineNumber_11() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___lineNumber_11)); }
	inline int32_t get_lineNumber_11() const { return ___lineNumber_11; }
	inline int32_t* get_address_of_lineNumber_11() { return &___lineNumber_11; }
	inline void set_lineNumber_11(int32_t value)
	{
		___lineNumber_11 = value;
	}

	inline static int32_t get_offset_of_pageNumber_12() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___pageNumber_12)); }
	inline int32_t get_pageNumber_12() const { return ___pageNumber_12; }
	inline int32_t* get_address_of_pageNumber_12() { return &___pageNumber_12; }
	inline void set_pageNumber_12(int32_t value)
	{
		___pageNumber_12 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_13() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertexIndex_13)); }
	inline int32_t get_vertexIndex_13() const { return ___vertexIndex_13; }
	inline int32_t* get_address_of_vertexIndex_13() { return &___vertexIndex_13; }
	inline void set_vertexIndex_13(int32_t value)
	{
		___vertexIndex_13 = value;
	}

	inline static int32_t get_offset_of_vertex_TL_14() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_TL_14)); }
	inline TMP_Vertex_t2404176824  get_vertex_TL_14() const { return ___vertex_TL_14; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_TL_14() { return &___vertex_TL_14; }
	inline void set_vertex_TL_14(TMP_Vertex_t2404176824  value)
	{
		___vertex_TL_14 = value;
	}

	inline static int32_t get_offset_of_vertex_BL_15() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_BL_15)); }
	inline TMP_Vertex_t2404176824  get_vertex_BL_15() const { return ___vertex_BL_15; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_BL_15() { return &___vertex_BL_15; }
	inline void set_vertex_BL_15(TMP_Vertex_t2404176824  value)
	{
		___vertex_BL_15 = value;
	}

	inline static int32_t get_offset_of_vertex_TR_16() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_TR_16)); }
	inline TMP_Vertex_t2404176824  get_vertex_TR_16() const { return ___vertex_TR_16; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_TR_16() { return &___vertex_TR_16; }
	inline void set_vertex_TR_16(TMP_Vertex_t2404176824  value)
	{
		___vertex_TR_16 = value;
	}

	inline static int32_t get_offset_of_vertex_BR_17() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_BR_17)); }
	inline TMP_Vertex_t2404176824  get_vertex_BR_17() const { return ___vertex_BR_17; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_BR_17() { return &___vertex_BR_17; }
	inline void set_vertex_BR_17(TMP_Vertex_t2404176824  value)
	{
		___vertex_BR_17 = value;
	}

	inline static int32_t get_offset_of_topLeft_18() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___topLeft_18)); }
	inline Vector3_t3722313464  get_topLeft_18() const { return ___topLeft_18; }
	inline Vector3_t3722313464 * get_address_of_topLeft_18() { return &___topLeft_18; }
	inline void set_topLeft_18(Vector3_t3722313464  value)
	{
		___topLeft_18 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_19() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___bottomLeft_19)); }
	inline Vector3_t3722313464  get_bottomLeft_19() const { return ___bottomLeft_19; }
	inline Vector3_t3722313464 * get_address_of_bottomLeft_19() { return &___bottomLeft_19; }
	inline void set_bottomLeft_19(Vector3_t3722313464  value)
	{
		___bottomLeft_19 = value;
	}

	inline static int32_t get_offset_of_topRight_20() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___topRight_20)); }
	inline Vector3_t3722313464  get_topRight_20() const { return ___topRight_20; }
	inline Vector3_t3722313464 * get_address_of_topRight_20() { return &___topRight_20; }
	inline void set_topRight_20(Vector3_t3722313464  value)
	{
		___topRight_20 = value;
	}

	inline static int32_t get_offset_of_bottomRight_21() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___bottomRight_21)); }
	inline Vector3_t3722313464  get_bottomRight_21() const { return ___bottomRight_21; }
	inline Vector3_t3722313464 * get_address_of_bottomRight_21() { return &___bottomRight_21; }
	inline void set_bottomRight_21(Vector3_t3722313464  value)
	{
		___bottomRight_21 = value;
	}

	inline static int32_t get_offset_of_origin_22() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___origin_22)); }
	inline float get_origin_22() const { return ___origin_22; }
	inline float* get_address_of_origin_22() { return &___origin_22; }
	inline void set_origin_22(float value)
	{
		___origin_22 = value;
	}

	inline static int32_t get_offset_of_ascender_23() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___ascender_23)); }
	inline float get_ascender_23() const { return ___ascender_23; }
	inline float* get_address_of_ascender_23() { return &___ascender_23; }
	inline void set_ascender_23(float value)
	{
		___ascender_23 = value;
	}

	inline static int32_t get_offset_of_baseLine_24() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___baseLine_24)); }
	inline float get_baseLine_24() const { return ___baseLine_24; }
	inline float* get_address_of_baseLine_24() { return &___baseLine_24; }
	inline void set_baseLine_24(float value)
	{
		___baseLine_24 = value;
	}

	inline static int32_t get_offset_of_descender_25() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___descender_25)); }
	inline float get_descender_25() const { return ___descender_25; }
	inline float* get_address_of_descender_25() { return &___descender_25; }
	inline void set_descender_25(float value)
	{
		___descender_25 = value;
	}

	inline static int32_t get_offset_of_xAdvance_26() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___xAdvance_26)); }
	inline float get_xAdvance_26() const { return ___xAdvance_26; }
	inline float* get_address_of_xAdvance_26() { return &___xAdvance_26; }
	inline void set_xAdvance_26(float value)
	{
		___xAdvance_26 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_27() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___aspectRatio_27)); }
	inline float get_aspectRatio_27() const { return ___aspectRatio_27; }
	inline float* get_address_of_aspectRatio_27() { return &___aspectRatio_27; }
	inline void set_aspectRatio_27(float value)
	{
		___aspectRatio_27 = value;
	}

	inline static int32_t get_offset_of_scale_28() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___scale_28)); }
	inline float get_scale_28() const { return ___scale_28; }
	inline float* get_address_of_scale_28() { return &___scale_28; }
	inline void set_scale_28(float value)
	{
		___scale_28 = value;
	}

	inline static int32_t get_offset_of_color_29() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___color_29)); }
	inline Color32_t2600501292  get_color_29() const { return ___color_29; }
	inline Color32_t2600501292 * get_address_of_color_29() { return &___color_29; }
	inline void set_color_29(Color32_t2600501292  value)
	{
		___color_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___underlineColor_30)); }
	inline Color32_t2600501292  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t2600501292 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t2600501292  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___strikethroughColor_31)); }
	inline Color32_t2600501292  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t2600501292 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t2600501292  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___highlightColor_32)); }
	inline Color32_t2600501292  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t2600501292 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t2600501292  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_style_33() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___style_33)); }
	inline int32_t get_style_33() const { return ___style_33; }
	inline int32_t* get_address_of_style_33() { return &___style_33; }
	inline void set_style_33(int32_t value)
	{
		___style_33 = value;
	}

	inline static int32_t get_offset_of_isVisible_34() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___isVisible_34)); }
	inline bool get_isVisible_34() const { return ___isVisible_34; }
	inline bool* get_address_of_isVisible_34() { return &___isVisible_34; }
	inline void set_isVisible_34(bool value)
	{
		___isVisible_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t3185626797_marshaled_pinvoke
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t129727469 * ___textElement_3;
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t340375123 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	Vector3_t3722313464  ___topLeft_18;
	Vector3_t3722313464  ___bottomLeft_19;
	Vector3_t3722313464  ___topRight_20;
	Vector3_t3722313464  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t2600501292  ___color_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
// Native definition for COM marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t3185626797_marshaled_com
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t129727469 * ___textElement_3;
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t340375123 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	Vector3_t3722313464  ___topLeft_18;
	Vector3_t3722313464  ___bottomLeft_19;
	Vector3_t3722313464  ___topRight_20;
	Vector3_t3722313464  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t2600501292  ___color_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
#endif // TMP_CHARACTERINFO_T3185626797_H
#ifndef TMP_LINEINFO_T1079631636_H
#define TMP_LINEINFO_T1079631636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_t1079631636 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::controlCharacterCount
	int32_t ___controlCharacterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_2;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_3;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_4;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_8;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_9;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_10;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_11;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_12;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_13;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_14;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_15;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_16;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_17;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_18;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_t3837212874  ___lineExtents_19;

public:
	inline static int32_t get_offset_of_controlCharacterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___controlCharacterCount_0)); }
	inline int32_t get_controlCharacterCount_0() const { return ___controlCharacterCount_0; }
	inline int32_t* get_address_of_controlCharacterCount_0() { return &___controlCharacterCount_0; }
	inline void set_controlCharacterCount_0(int32_t value)
	{
		___controlCharacterCount_0 = value;
	}

	inline static int32_t get_offset_of_characterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___characterCount_1)); }
	inline int32_t get_characterCount_1() const { return ___characterCount_1; }
	inline int32_t* get_address_of_characterCount_1() { return &___characterCount_1; }
	inline void set_characterCount_1(int32_t value)
	{
		___characterCount_1 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___visibleCharacterCount_2)); }
	inline int32_t get_visibleCharacterCount_2() const { return ___visibleCharacterCount_2; }
	inline int32_t* get_address_of_visibleCharacterCount_2() { return &___visibleCharacterCount_2; }
	inline void set_visibleCharacterCount_2(int32_t value)
	{
		___visibleCharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_spaceCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___spaceCount_3)); }
	inline int32_t get_spaceCount_3() const { return ___spaceCount_3; }
	inline int32_t* get_address_of_spaceCount_3() { return &___spaceCount_3; }
	inline void set_spaceCount_3(int32_t value)
	{
		___spaceCount_3 = value;
	}

	inline static int32_t get_offset_of_wordCount_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___wordCount_4)); }
	inline int32_t get_wordCount_4() const { return ___wordCount_4; }
	inline int32_t* get_address_of_wordCount_4() { return &___wordCount_4; }
	inline void set_wordCount_4(int32_t value)
	{
		___wordCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastVisibleCharacterIndex_8)); }
	inline int32_t get_lastVisibleCharacterIndex_8() const { return ___lastVisibleCharacterIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_8() { return &___lastVisibleCharacterIndex_8; }
	inline void set_lastVisibleCharacterIndex_8(int32_t value)
	{
		___lastVisibleCharacterIndex_8 = value;
	}

	inline static int32_t get_offset_of_length_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___length_9)); }
	inline float get_length_9() const { return ___length_9; }
	inline float* get_address_of_length_9() { return &___length_9; }
	inline void set_length_9(float value)
	{
		___length_9 = value;
	}

	inline static int32_t get_offset_of_lineHeight_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineHeight_10)); }
	inline float get_lineHeight_10() const { return ___lineHeight_10; }
	inline float* get_address_of_lineHeight_10() { return &___lineHeight_10; }
	inline void set_lineHeight_10(float value)
	{
		___lineHeight_10 = value;
	}

	inline static int32_t get_offset_of_ascender_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___ascender_11)); }
	inline float get_ascender_11() const { return ___ascender_11; }
	inline float* get_address_of_ascender_11() { return &___ascender_11; }
	inline void set_ascender_11(float value)
	{
		___ascender_11 = value;
	}

	inline static int32_t get_offset_of_baseline_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___baseline_12)); }
	inline float get_baseline_12() const { return ___baseline_12; }
	inline float* get_address_of_baseline_12() { return &___baseline_12; }
	inline void set_baseline_12(float value)
	{
		___baseline_12 = value;
	}

	inline static int32_t get_offset_of_descender_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___descender_13)); }
	inline float get_descender_13() const { return ___descender_13; }
	inline float* get_address_of_descender_13() { return &___descender_13; }
	inline void set_descender_13(float value)
	{
		___descender_13 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___maxAdvance_14)); }
	inline float get_maxAdvance_14() const { return ___maxAdvance_14; }
	inline float* get_address_of_maxAdvance_14() { return &___maxAdvance_14; }
	inline void set_maxAdvance_14(float value)
	{
		___maxAdvance_14 = value;
	}

	inline static int32_t get_offset_of_width_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___width_15)); }
	inline float get_width_15() const { return ___width_15; }
	inline float* get_address_of_width_15() { return &___width_15; }
	inline void set_width_15(float value)
	{
		___width_15 = value;
	}

	inline static int32_t get_offset_of_marginLeft_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginLeft_16)); }
	inline float get_marginLeft_16() const { return ___marginLeft_16; }
	inline float* get_address_of_marginLeft_16() { return &___marginLeft_16; }
	inline void set_marginLeft_16(float value)
	{
		___marginLeft_16 = value;
	}

	inline static int32_t get_offset_of_marginRight_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginRight_17)); }
	inline float get_marginRight_17() const { return ___marginRight_17; }
	inline float* get_address_of_marginRight_17() { return &___marginRight_17; }
	inline void set_marginRight_17(float value)
	{
		___marginRight_17 = value;
	}

	inline static int32_t get_offset_of_alignment_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___alignment_18)); }
	inline int32_t get_alignment_18() const { return ___alignment_18; }
	inline int32_t* get_address_of_alignment_18() { return &___alignment_18; }
	inline void set_alignment_18(int32_t value)
	{
		___alignment_18 = value;
	}

	inline static int32_t get_offset_of_lineExtents_19() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineExtents_19)); }
	inline Extents_t3837212874  get_lineExtents_19() const { return ___lineExtents_19; }
	inline Extents_t3837212874 * get_address_of_lineExtents_19() { return &___lineExtents_19; }
	inline void set_lineExtents_19(Extents_t3837212874  value)
	{
		___lineExtents_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_T1079631636_H
#ifndef TMP_XMLTAGSTACK_1_T3600445780_H
#define TMP_XMLTAGSTACK_1_T3600445780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_XmlTagStack_1_t3600445780 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TextAlignmentOptionsU5BU5D_t3552942253* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___itemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t3552942253* get_itemStack_0() const { return ___itemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t3552942253** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TextAlignmentOptionsU5BU5D_t3552942253* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T3600445780_H
#ifndef XML_TAGATTRIBUTE_T1174424309_H
#define XML_TAGATTRIBUTE_T1174424309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.XML_TagAttribute
struct  XML_TagAttribute_t1174424309 
{
public:
	// System.Int32 TMPro.XML_TagAttribute::nameHashCode
	int32_t ___nameHashCode_0;
	// TMPro.TagType TMPro.XML_TagAttribute::valueType
	int32_t ___valueType_1;
	// System.Int32 TMPro.XML_TagAttribute::valueStartIndex
	int32_t ___valueStartIndex_2;
	// System.Int32 TMPro.XML_TagAttribute::valueLength
	int32_t ___valueLength_3;
	// System.Int32 TMPro.XML_TagAttribute::valueHashCode
	int32_t ___valueHashCode_4;

public:
	inline static int32_t get_offset_of_nameHashCode_0() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___nameHashCode_0)); }
	inline int32_t get_nameHashCode_0() const { return ___nameHashCode_0; }
	inline int32_t* get_address_of_nameHashCode_0() { return &___nameHashCode_0; }
	inline void set_nameHashCode_0(int32_t value)
	{
		___nameHashCode_0 = value;
	}

	inline static int32_t get_offset_of_valueType_1() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueType_1)); }
	inline int32_t get_valueType_1() const { return ___valueType_1; }
	inline int32_t* get_address_of_valueType_1() { return &___valueType_1; }
	inline void set_valueType_1(int32_t value)
	{
		___valueType_1 = value;
	}

	inline static int32_t get_offset_of_valueStartIndex_2() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueStartIndex_2)); }
	inline int32_t get_valueStartIndex_2() const { return ___valueStartIndex_2; }
	inline int32_t* get_address_of_valueStartIndex_2() { return &___valueStartIndex_2; }
	inline void set_valueStartIndex_2(int32_t value)
	{
		___valueStartIndex_2 = value;
	}

	inline static int32_t get_offset_of_valueLength_3() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueLength_3)); }
	inline int32_t get_valueLength_3() const { return ___valueLength_3; }
	inline int32_t* get_address_of_valueLength_3() { return &___valueLength_3; }
	inline void set_valueLength_3(int32_t value)
	{
		___valueLength_3 = value;
	}

	inline static int32_t get_offset_of_valueHashCode_4() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueHashCode_4)); }
	inline int32_t get_valueHashCode_4() const { return ___valueHashCode_4; }
	inline int32_t* get_address_of_valueHashCode_4() { return &___valueHashCode_4; }
	inline void set_valueHashCode_4(int32_t value)
	{
		___valueHashCode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XML_TAGATTRIBUTE_T1174424309_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef PAGESNAPCHANGE_T2018612193_H
#define PAGESNAPCHANGE_T2018612193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollSnapEx/PageSnapChange
struct  PageSnapChange_t2018612193  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGESNAPCHANGE_T2018612193_H
#ifndef WORDWRAPSTATE_T341939652_H
#define WORDWRAPSTATE_T341939652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t341939652 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t2600501292  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t2600501292  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t2600501292  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t2600501292  ___highlightColor_32;
	// TMPro.TMP_BasicXmlTagStack TMPro.WordWrapState::basicStyleStack
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::fontWeightStack
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_47;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_t340375123 * ___currentMaterial_50;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_51;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_t3837212874  ___meshExtents_52;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_53;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_54;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___textInfo_27)); }
	inline TMP_TextInfo_t3598145122 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_t3598145122 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineInfo_28)); }
	inline TMP_LineInfo_t1079631636  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_t1079631636 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_t1079631636  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___vertexColor_29)); }
	inline Color32_t2600501292  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t2600501292 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t2600501292  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColor_30)); }
	inline Color32_t2600501292  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t2600501292 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t2600501292  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColor_31)); }
	inline Color32_t2600501292  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t2600501292 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t2600501292  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColor_32)); }
	inline Color32_t2600501292  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t2600501292 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t2600501292  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___basicStyleStack_33)); }
	inline TMP_BasicXmlTagStack_t2962628096  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_BasicXmlTagStack_t2962628096 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_BasicXmlTagStack_t2962628096  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___colorStack_34)); }
	inline TMP_XmlTagStack_1_t2164155836  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_XmlTagStack_1_t2164155836  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColorStack_35)); }
	inline TMP_XmlTagStack_1_t2164155836  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_XmlTagStack_1_t2164155836  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColorStack_36)); }
	inline TMP_XmlTagStack_1_t2164155836  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_XmlTagStack_1_t2164155836  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColorStack_37)); }
	inline TMP_XmlTagStack_1_t2164155836  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_XmlTagStack_1_t2164155836  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_colorGradientStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___colorGradientStack_38)); }
	inline TMP_XmlTagStack_1_t3241710312  get_colorGradientStack_38() const { return ___colorGradientStack_38; }
	inline TMP_XmlTagStack_1_t3241710312 * get_address_of_colorGradientStack_38() { return &___colorGradientStack_38; }
	inline void set_colorGradientStack_38(TMP_XmlTagStack_1_t3241710312  value)
	{
		___colorGradientStack_38 = value;
	}

	inline static int32_t get_offset_of_sizeStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___sizeStack_39)); }
	inline TMP_XmlTagStack_1_t960921318  get_sizeStack_39() const { return ___sizeStack_39; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_sizeStack_39() { return &___sizeStack_39; }
	inline void set_sizeStack_39(TMP_XmlTagStack_1_t960921318  value)
	{
		___sizeStack_39 = value;
	}

	inline static int32_t get_offset_of_indentStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___indentStack_40)); }
	inline TMP_XmlTagStack_1_t960921318  get_indentStack_40() const { return ___indentStack_40; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_indentStack_40() { return &___indentStack_40; }
	inline void set_indentStack_40(TMP_XmlTagStack_1_t960921318  value)
	{
		___indentStack_40 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontWeightStack_41)); }
	inline TMP_XmlTagStack_1_t2514600297  get_fontWeightStack_41() const { return ___fontWeightStack_41; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_fontWeightStack_41() { return &___fontWeightStack_41; }
	inline void set_fontWeightStack_41(TMP_XmlTagStack_1_t2514600297  value)
	{
		___fontWeightStack_41 = value;
	}

	inline static int32_t get_offset_of_styleStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___styleStack_42)); }
	inline TMP_XmlTagStack_1_t2514600297  get_styleStack_42() const { return ___styleStack_42; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_styleStack_42() { return &___styleStack_42; }
	inline void set_styleStack_42(TMP_XmlTagStack_1_t2514600297  value)
	{
		___styleStack_42 = value;
	}

	inline static int32_t get_offset_of_baselineStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineStack_43)); }
	inline TMP_XmlTagStack_1_t960921318  get_baselineStack_43() const { return ___baselineStack_43; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_baselineStack_43() { return &___baselineStack_43; }
	inline void set_baselineStack_43(TMP_XmlTagStack_1_t960921318  value)
	{
		___baselineStack_43 = value;
	}

	inline static int32_t get_offset_of_actionStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___actionStack_44)); }
	inline TMP_XmlTagStack_1_t2514600297  get_actionStack_44() const { return ___actionStack_44; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_actionStack_44() { return &___actionStack_44; }
	inline void set_actionStack_44(TMP_XmlTagStack_1_t2514600297  value)
	{
		___actionStack_44 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___materialReferenceStack_45)); }
	inline TMP_XmlTagStack_1_t1515999176  get_materialReferenceStack_45() const { return ___materialReferenceStack_45; }
	inline TMP_XmlTagStack_1_t1515999176 * get_address_of_materialReferenceStack_45() { return &___materialReferenceStack_45; }
	inline void set_materialReferenceStack_45(TMP_XmlTagStack_1_t1515999176  value)
	{
		___materialReferenceStack_45 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_46() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineJustificationStack_46)); }
	inline TMP_XmlTagStack_1_t3600445780  get_lineJustificationStack_46() const { return ___lineJustificationStack_46; }
	inline TMP_XmlTagStack_1_t3600445780 * get_address_of_lineJustificationStack_46() { return &___lineJustificationStack_46; }
	inline void set_lineJustificationStack_46(TMP_XmlTagStack_1_t3600445780  value)
	{
		___lineJustificationStack_46 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_47() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___spriteAnimationID_47)); }
	inline int32_t get_spriteAnimationID_47() const { return ___spriteAnimationID_47; }
	inline int32_t* get_address_of_spriteAnimationID_47() { return &___spriteAnimationID_47; }
	inline void set_spriteAnimationID_47(int32_t value)
	{
		___spriteAnimationID_47 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontAsset_48)); }
	inline TMP_FontAsset_t364381626 * get_currentFontAsset_48() const { return ___currentFontAsset_48; }
	inline TMP_FontAsset_t364381626 ** get_address_of_currentFontAsset_48() { return &___currentFontAsset_48; }
	inline void set_currentFontAsset_48(TMP_FontAsset_t364381626 * value)
	{
		___currentFontAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_48), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_49() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentSpriteAsset_49)); }
	inline TMP_SpriteAsset_t484820633 * get_currentSpriteAsset_49() const { return ___currentSpriteAsset_49; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_currentSpriteAsset_49() { return &___currentSpriteAsset_49; }
	inline void set_currentSpriteAsset_49(TMP_SpriteAsset_t484820633 * value)
	{
		___currentSpriteAsset_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_49), value);
	}

	inline static int32_t get_offset_of_currentMaterial_50() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterial_50)); }
	inline Material_t340375123 * get_currentMaterial_50() const { return ___currentMaterial_50; }
	inline Material_t340375123 ** get_address_of_currentMaterial_50() { return &___currentMaterial_50; }
	inline void set_currentMaterial_50(Material_t340375123 * value)
	{
		___currentMaterial_50 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_50), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_51() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterialIndex_51)); }
	inline int32_t get_currentMaterialIndex_51() const { return ___currentMaterialIndex_51; }
	inline int32_t* get_address_of_currentMaterialIndex_51() { return &___currentMaterialIndex_51; }
	inline void set_currentMaterialIndex_51(int32_t value)
	{
		___currentMaterialIndex_51 = value;
	}

	inline static int32_t get_offset_of_meshExtents_52() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___meshExtents_52)); }
	inline Extents_t3837212874  get_meshExtents_52() const { return ___meshExtents_52; }
	inline Extents_t3837212874 * get_address_of_meshExtents_52() { return &___meshExtents_52; }
	inline void set_meshExtents_52(Extents_t3837212874  value)
	{
		___meshExtents_52 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_53() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___tagNoParsing_53)); }
	inline bool get_tagNoParsing_53() const { return ___tagNoParsing_53; }
	inline bool* get_address_of_tagNoParsing_53() { return &___tagNoParsing_53; }
	inline void set_tagNoParsing_53(bool value)
	{
		___tagNoParsing_53 = value;
	}

	inline static int32_t get_offset_of_isNonBreakingSpace_54() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___isNonBreakingSpace_54)); }
	inline bool get_isNonBreakingSpace_54() const { return ___isNonBreakingSpace_54; }
	inline bool* get_address_of_isNonBreakingSpace_54() { return &___isNonBreakingSpace_54; }
	inline void set_isNonBreakingSpace_54(bool value)
	{
		___isNonBreakingSpace_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	Material_t340375123 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t3837212874  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	Material_t340375123 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t3837212874  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
#endif // WORDWRAPSTATE_T341939652_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ABOUTMANAGER_T2961629990_H
#define ABOUTMANAGER_T2961629990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AboutManager
struct  AboutManager_t2961629990  : public MonoBehaviour_t3962482529
{
public:
	// AboutManager/AboutScreenSample AboutManager::m_AboutScreenSample
	int32_t ___m_AboutScreenSample_4;

public:
	inline static int32_t get_offset_of_m_AboutScreenSample_4() { return static_cast<int32_t>(offsetof(AboutManager_t2961629990, ___m_AboutScreenSample_4)); }
	inline int32_t get_m_AboutScreenSample_4() const { return ___m_AboutScreenSample_4; }
	inline int32_t* get_address_of_m_AboutScreenSample_4() { return &___m_AboutScreenSample_4; }
	inline void set_m_AboutScreenSample_4(int32_t value)
	{
		___m_AboutScreenSample_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABOUTMANAGER_T2961629990_H
#ifndef ABOUTSCREEN_T2183797299_H
#define ABOUTSCREEN_T2183797299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AboutScreen
struct  AboutScreen_t2183797299  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABOUTSCREEN_T2183797299_H
#ifndef CONTENTSCALER_T903206615_H
#define CONTENTSCALER_T903206615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.UnityUIMods.ContentScaler
struct  ContentScaler_t903206615  : public MonoBehaviour_t3962482529
{
public:
	// BMJ.UnityUIMods.Alignment BMJ.UnityUIMods.ContentScaler::SetAlignment
	int32_t ___SetAlignment_4;

public:
	inline static int32_t get_offset_of_SetAlignment_4() { return static_cast<int32_t>(offsetof(ContentScaler_t903206615, ___SetAlignment_4)); }
	inline int32_t get_SetAlignment_4() const { return ___SetAlignment_4; }
	inline int32_t* get_address_of_SetAlignment_4() { return &___SetAlignment_4; }
	inline void set_SetAlignment_4(int32_t value)
	{
		___SetAlignment_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTSCALER_T903206615_H
#ifndef BOTTOMSIDEPAGE_T4207383372_H
#define BOTTOMSIDEPAGE_T4207383372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BottomSidePage
struct  BottomSidePage_t4207383372  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject BottomSidePage::page
	GameObject_t1113636619 * ___page_4;
	// System.Boolean BottomSidePage::getActivate
	bool ___getActivate_5;
	// System.Single BottomSidePage::speed
	float ___speed_6;
	// System.Single BottomSidePage::startTime
	float ___startTime_7;

public:
	inline static int32_t get_offset_of_page_4() { return static_cast<int32_t>(offsetof(BottomSidePage_t4207383372, ___page_4)); }
	inline GameObject_t1113636619 * get_page_4() const { return ___page_4; }
	inline GameObject_t1113636619 ** get_address_of_page_4() { return &___page_4; }
	inline void set_page_4(GameObject_t1113636619 * value)
	{
		___page_4 = value;
		Il2CppCodeGenWriteBarrier((&___page_4), value);
	}

	inline static int32_t get_offset_of_getActivate_5() { return static_cast<int32_t>(offsetof(BottomSidePage_t4207383372, ___getActivate_5)); }
	inline bool get_getActivate_5() const { return ___getActivate_5; }
	inline bool* get_address_of_getActivate_5() { return &___getActivate_5; }
	inline void set_getActivate_5(bool value)
	{
		___getActivate_5 = value;
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(BottomSidePage_t4207383372, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}

	inline static int32_t get_offset_of_startTime_7() { return static_cast<int32_t>(offsetof(BottomSidePage_t4207383372, ___startTime_7)); }
	inline float get_startTime_7() const { return ___startTime_7; }
	inline float* get_address_of_startTime_7() { return &___startTime_7; }
	inline void set_startTime_7(float value)
	{
		___startTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOTTOMSIDEPAGE_T4207383372_H
#ifndef BOUNDINGBOXRENDERER_T3223945734_H
#define BOUNDINGBOXRENDERER_T3223945734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoundingBoxRenderer
struct  BoundingBoxRenderer_t3223945734  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material BoundingBoxRenderer::mLineMaterial
	Material_t340375123 * ___mLineMaterial_4;

public:
	inline static int32_t get_offset_of_mLineMaterial_4() { return static_cast<int32_t>(offsetof(BoundingBoxRenderer_t3223945734, ___mLineMaterial_4)); }
	inline Material_t340375123 * get_mLineMaterial_4() const { return ___mLineMaterial_4; }
	inline Material_t340375123 ** get_address_of_mLineMaterial_4() { return &___mLineMaterial_4; }
	inline void set_mLineMaterial_4(Material_t340375123 * value)
	{
		___mLineMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___mLineMaterial_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXRENDERER_T3223945734_H
#ifndef SOFTMASKABLE_T353105572_H
#define SOFTMASKABLE_T353105572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Coffee.UIExtensions.SoftMaskable
struct  SoftMaskable_t353105572  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Coffee.UIExtensions.SoftMaskable::m_Inverse
	bool ___m_Inverse_6;
	// System.Int32 Coffee.UIExtensions.SoftMaskable::m_MaskInteraction
	int32_t ___m_MaskInteraction_7;
	// System.Boolean Coffee.UIExtensions.SoftMaskable::m_UseStencil
	bool ___m_UseStencil_8;
	// UnityEngine.UI.Graphic Coffee.UIExtensions.SoftMaskable::_graphic
	Graphic_t1660335611 * ____graphic_9;
	// Coffee.UIExtensions.SoftMask Coffee.UIExtensions.SoftMaskable::_softMask
	SoftMask_t1817791576 * ____softMask_10;
	// UnityEngine.Material Coffee.UIExtensions.SoftMaskable::_maskMaterial
	Material_t340375123 * ____maskMaterial_11;

public:
	inline static int32_t get_offset_of_m_Inverse_6() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572, ___m_Inverse_6)); }
	inline bool get_m_Inverse_6() const { return ___m_Inverse_6; }
	inline bool* get_address_of_m_Inverse_6() { return &___m_Inverse_6; }
	inline void set_m_Inverse_6(bool value)
	{
		___m_Inverse_6 = value;
	}

	inline static int32_t get_offset_of_m_MaskInteraction_7() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572, ___m_MaskInteraction_7)); }
	inline int32_t get_m_MaskInteraction_7() const { return ___m_MaskInteraction_7; }
	inline int32_t* get_address_of_m_MaskInteraction_7() { return &___m_MaskInteraction_7; }
	inline void set_m_MaskInteraction_7(int32_t value)
	{
		___m_MaskInteraction_7 = value;
	}

	inline static int32_t get_offset_of_m_UseStencil_8() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572, ___m_UseStencil_8)); }
	inline bool get_m_UseStencil_8() const { return ___m_UseStencil_8; }
	inline bool* get_address_of_m_UseStencil_8() { return &___m_UseStencil_8; }
	inline void set_m_UseStencil_8(bool value)
	{
		___m_UseStencil_8 = value;
	}

	inline static int32_t get_offset_of__graphic_9() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572, ____graphic_9)); }
	inline Graphic_t1660335611 * get__graphic_9() const { return ____graphic_9; }
	inline Graphic_t1660335611 ** get_address_of__graphic_9() { return &____graphic_9; }
	inline void set__graphic_9(Graphic_t1660335611 * value)
	{
		____graphic_9 = value;
		Il2CppCodeGenWriteBarrier((&____graphic_9), value);
	}

	inline static int32_t get_offset_of__softMask_10() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572, ____softMask_10)); }
	inline SoftMask_t1817791576 * get__softMask_10() const { return ____softMask_10; }
	inline SoftMask_t1817791576 ** get_address_of__softMask_10() { return &____softMask_10; }
	inline void set__softMask_10(SoftMask_t1817791576 * value)
	{
		____softMask_10 = value;
		Il2CppCodeGenWriteBarrier((&____softMask_10), value);
	}

	inline static int32_t get_offset_of__maskMaterial_11() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572, ____maskMaterial_11)); }
	inline Material_t340375123 * get__maskMaterial_11() const { return ____maskMaterial_11; }
	inline Material_t340375123 ** get_address_of__maskMaterial_11() { return &____maskMaterial_11; }
	inline void set__maskMaterial_11(Material_t340375123 * value)
	{
		____maskMaterial_11 = value;
		Il2CppCodeGenWriteBarrier((&____maskMaterial_11), value);
	}
};

struct SoftMaskable_t353105572_StaticFields
{
public:
	// System.Int32 Coffee.UIExtensions.SoftMaskable::s_SoftMaskTexId
	int32_t ___s_SoftMaskTexId_12;
	// System.Int32 Coffee.UIExtensions.SoftMaskable::s_StencilCompId
	int32_t ___s_StencilCompId_13;
	// System.Int32 Coffee.UIExtensions.SoftMaskable::s_MaskInteractionId
	int32_t ___s_MaskInteractionId_14;
	// System.Int32 Coffee.UIExtensions.SoftMaskable::s_GameVPId
	int32_t ___s_GameVPId_15;
	// System.Int32 Coffee.UIExtensions.SoftMaskable::s_GameTVPId
	int32_t ___s_GameTVPId_16;
	// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMaskable> Coffee.UIExtensions.SoftMaskable::s_ActiveSoftMaskables
	List_1_t1825180314 * ___s_ActiveSoftMaskables_17;
	// System.Int32[] Coffee.UIExtensions.SoftMaskable::s_Interactions
	Int32U5BU5D_t385246372* ___s_Interactions_18;
	// UnityEngine.Material Coffee.UIExtensions.SoftMaskable::s_DefaultMaterial
	Material_t340375123 * ___s_DefaultMaterial_19;

public:
	inline static int32_t get_offset_of_s_SoftMaskTexId_12() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572_StaticFields, ___s_SoftMaskTexId_12)); }
	inline int32_t get_s_SoftMaskTexId_12() const { return ___s_SoftMaskTexId_12; }
	inline int32_t* get_address_of_s_SoftMaskTexId_12() { return &___s_SoftMaskTexId_12; }
	inline void set_s_SoftMaskTexId_12(int32_t value)
	{
		___s_SoftMaskTexId_12 = value;
	}

	inline static int32_t get_offset_of_s_StencilCompId_13() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572_StaticFields, ___s_StencilCompId_13)); }
	inline int32_t get_s_StencilCompId_13() const { return ___s_StencilCompId_13; }
	inline int32_t* get_address_of_s_StencilCompId_13() { return &___s_StencilCompId_13; }
	inline void set_s_StencilCompId_13(int32_t value)
	{
		___s_StencilCompId_13 = value;
	}

	inline static int32_t get_offset_of_s_MaskInteractionId_14() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572_StaticFields, ___s_MaskInteractionId_14)); }
	inline int32_t get_s_MaskInteractionId_14() const { return ___s_MaskInteractionId_14; }
	inline int32_t* get_address_of_s_MaskInteractionId_14() { return &___s_MaskInteractionId_14; }
	inline void set_s_MaskInteractionId_14(int32_t value)
	{
		___s_MaskInteractionId_14 = value;
	}

	inline static int32_t get_offset_of_s_GameVPId_15() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572_StaticFields, ___s_GameVPId_15)); }
	inline int32_t get_s_GameVPId_15() const { return ___s_GameVPId_15; }
	inline int32_t* get_address_of_s_GameVPId_15() { return &___s_GameVPId_15; }
	inline void set_s_GameVPId_15(int32_t value)
	{
		___s_GameVPId_15 = value;
	}

	inline static int32_t get_offset_of_s_GameTVPId_16() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572_StaticFields, ___s_GameTVPId_16)); }
	inline int32_t get_s_GameTVPId_16() const { return ___s_GameTVPId_16; }
	inline int32_t* get_address_of_s_GameTVPId_16() { return &___s_GameTVPId_16; }
	inline void set_s_GameTVPId_16(int32_t value)
	{
		___s_GameTVPId_16 = value;
	}

	inline static int32_t get_offset_of_s_ActiveSoftMaskables_17() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572_StaticFields, ___s_ActiveSoftMaskables_17)); }
	inline List_1_t1825180314 * get_s_ActiveSoftMaskables_17() const { return ___s_ActiveSoftMaskables_17; }
	inline List_1_t1825180314 ** get_address_of_s_ActiveSoftMaskables_17() { return &___s_ActiveSoftMaskables_17; }
	inline void set_s_ActiveSoftMaskables_17(List_1_t1825180314 * value)
	{
		___s_ActiveSoftMaskables_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_ActiveSoftMaskables_17), value);
	}

	inline static int32_t get_offset_of_s_Interactions_18() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572_StaticFields, ___s_Interactions_18)); }
	inline Int32U5BU5D_t385246372* get_s_Interactions_18() const { return ___s_Interactions_18; }
	inline Int32U5BU5D_t385246372** get_address_of_s_Interactions_18() { return &___s_Interactions_18; }
	inline void set_s_Interactions_18(Int32U5BU5D_t385246372* value)
	{
		___s_Interactions_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_Interactions_18), value);
	}

	inline static int32_t get_offset_of_s_DefaultMaterial_19() { return static_cast<int32_t>(offsetof(SoftMaskable_t353105572_StaticFields, ___s_DefaultMaterial_19)); }
	inline Material_t340375123 * get_s_DefaultMaterial_19() const { return ___s_DefaultMaterial_19; }
	inline Material_t340375123 ** get_address_of_s_DefaultMaterial_19() { return &___s_DefaultMaterial_19; }
	inline void set_s_DefaultMaterial_19(Material_t340375123 * value)
	{
		___s_DefaultMaterial_19 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultMaterial_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOFTMASKABLE_T353105572_H
#ifndef DEFAULTMODELRECOEVENTHANDLER_T1899894298_H
#define DEFAULTMODELRECOEVENTHANDLER_T1899894298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultModelRecoEventHandler
struct  DefaultModelRecoEventHandler_t1899894298  : public MonoBehaviour_t3962482529
{
public:
	// Vuforia.ModelTargetBehaviour DefaultModelRecoEventHandler::mLastRecoModelTarget
	ModelTargetBehaviour_t712978329 * ___mLastRecoModelTarget_4;
	// System.Boolean DefaultModelRecoEventHandler::mSearching
	bool ___mSearching_5;
	// System.Single DefaultModelRecoEventHandler::mLastStatusCheckTime
	float ___mLastStatusCheckTime_6;
	// Vuforia.ModelRecoBehaviour DefaultModelRecoEventHandler::mModelRecoBehaviour
	ModelRecoBehaviour_t2312633019 * ___mModelRecoBehaviour_7;
	// Vuforia.TargetFinder DefaultModelRecoEventHandler::mTargetFinder
	TargetFinder_t2439332195 * ___mTargetFinder_8;
	// Vuforia.ModelTargetBehaviour DefaultModelRecoEventHandler::ModelTargetTemplate
	ModelTargetBehaviour_t712978329 * ___ModelTargetTemplate_9;
	// System.Boolean DefaultModelRecoEventHandler::ShowBoundingBox
	bool ___ShowBoundingBox_10;
	// UnityEngine.UI.Text DefaultModelRecoEventHandler::ModelRecoErrorText
	Text_t1901882714 * ___ModelRecoErrorText_11;
	// System.Boolean DefaultModelRecoEventHandler::StopSearchWhenModelFound
	bool ___StopSearchWhenModelFound_12;
	// System.Boolean DefaultModelRecoEventHandler::StopSearchWhileTracking
	bool ___StopSearchWhileTracking_13;

public:
	inline static int32_t get_offset_of_mLastRecoModelTarget_4() { return static_cast<int32_t>(offsetof(DefaultModelRecoEventHandler_t1899894298, ___mLastRecoModelTarget_4)); }
	inline ModelTargetBehaviour_t712978329 * get_mLastRecoModelTarget_4() const { return ___mLastRecoModelTarget_4; }
	inline ModelTargetBehaviour_t712978329 ** get_address_of_mLastRecoModelTarget_4() { return &___mLastRecoModelTarget_4; }
	inline void set_mLastRecoModelTarget_4(ModelTargetBehaviour_t712978329 * value)
	{
		___mLastRecoModelTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&___mLastRecoModelTarget_4), value);
	}

	inline static int32_t get_offset_of_mSearching_5() { return static_cast<int32_t>(offsetof(DefaultModelRecoEventHandler_t1899894298, ___mSearching_5)); }
	inline bool get_mSearching_5() const { return ___mSearching_5; }
	inline bool* get_address_of_mSearching_5() { return &___mSearching_5; }
	inline void set_mSearching_5(bool value)
	{
		___mSearching_5 = value;
	}

	inline static int32_t get_offset_of_mLastStatusCheckTime_6() { return static_cast<int32_t>(offsetof(DefaultModelRecoEventHandler_t1899894298, ___mLastStatusCheckTime_6)); }
	inline float get_mLastStatusCheckTime_6() const { return ___mLastStatusCheckTime_6; }
	inline float* get_address_of_mLastStatusCheckTime_6() { return &___mLastStatusCheckTime_6; }
	inline void set_mLastStatusCheckTime_6(float value)
	{
		___mLastStatusCheckTime_6 = value;
	}

	inline static int32_t get_offset_of_mModelRecoBehaviour_7() { return static_cast<int32_t>(offsetof(DefaultModelRecoEventHandler_t1899894298, ___mModelRecoBehaviour_7)); }
	inline ModelRecoBehaviour_t2312633019 * get_mModelRecoBehaviour_7() const { return ___mModelRecoBehaviour_7; }
	inline ModelRecoBehaviour_t2312633019 ** get_address_of_mModelRecoBehaviour_7() { return &___mModelRecoBehaviour_7; }
	inline void set_mModelRecoBehaviour_7(ModelRecoBehaviour_t2312633019 * value)
	{
		___mModelRecoBehaviour_7 = value;
		Il2CppCodeGenWriteBarrier((&___mModelRecoBehaviour_7), value);
	}

	inline static int32_t get_offset_of_mTargetFinder_8() { return static_cast<int32_t>(offsetof(DefaultModelRecoEventHandler_t1899894298, ___mTargetFinder_8)); }
	inline TargetFinder_t2439332195 * get_mTargetFinder_8() const { return ___mTargetFinder_8; }
	inline TargetFinder_t2439332195 ** get_address_of_mTargetFinder_8() { return &___mTargetFinder_8; }
	inline void set_mTargetFinder_8(TargetFinder_t2439332195 * value)
	{
		___mTargetFinder_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTargetFinder_8), value);
	}

	inline static int32_t get_offset_of_ModelTargetTemplate_9() { return static_cast<int32_t>(offsetof(DefaultModelRecoEventHandler_t1899894298, ___ModelTargetTemplate_9)); }
	inline ModelTargetBehaviour_t712978329 * get_ModelTargetTemplate_9() const { return ___ModelTargetTemplate_9; }
	inline ModelTargetBehaviour_t712978329 ** get_address_of_ModelTargetTemplate_9() { return &___ModelTargetTemplate_9; }
	inline void set_ModelTargetTemplate_9(ModelTargetBehaviour_t712978329 * value)
	{
		___ModelTargetTemplate_9 = value;
		Il2CppCodeGenWriteBarrier((&___ModelTargetTemplate_9), value);
	}

	inline static int32_t get_offset_of_ShowBoundingBox_10() { return static_cast<int32_t>(offsetof(DefaultModelRecoEventHandler_t1899894298, ___ShowBoundingBox_10)); }
	inline bool get_ShowBoundingBox_10() const { return ___ShowBoundingBox_10; }
	inline bool* get_address_of_ShowBoundingBox_10() { return &___ShowBoundingBox_10; }
	inline void set_ShowBoundingBox_10(bool value)
	{
		___ShowBoundingBox_10 = value;
	}

	inline static int32_t get_offset_of_ModelRecoErrorText_11() { return static_cast<int32_t>(offsetof(DefaultModelRecoEventHandler_t1899894298, ___ModelRecoErrorText_11)); }
	inline Text_t1901882714 * get_ModelRecoErrorText_11() const { return ___ModelRecoErrorText_11; }
	inline Text_t1901882714 ** get_address_of_ModelRecoErrorText_11() { return &___ModelRecoErrorText_11; }
	inline void set_ModelRecoErrorText_11(Text_t1901882714 * value)
	{
		___ModelRecoErrorText_11 = value;
		Il2CppCodeGenWriteBarrier((&___ModelRecoErrorText_11), value);
	}

	inline static int32_t get_offset_of_StopSearchWhenModelFound_12() { return static_cast<int32_t>(offsetof(DefaultModelRecoEventHandler_t1899894298, ___StopSearchWhenModelFound_12)); }
	inline bool get_StopSearchWhenModelFound_12() const { return ___StopSearchWhenModelFound_12; }
	inline bool* get_address_of_StopSearchWhenModelFound_12() { return &___StopSearchWhenModelFound_12; }
	inline void set_StopSearchWhenModelFound_12(bool value)
	{
		___StopSearchWhenModelFound_12 = value;
	}

	inline static int32_t get_offset_of_StopSearchWhileTracking_13() { return static_cast<int32_t>(offsetof(DefaultModelRecoEventHandler_t1899894298, ___StopSearchWhileTracking_13)); }
	inline bool get_StopSearchWhileTracking_13() const { return ___StopSearchWhileTracking_13; }
	inline bool* get_address_of_StopSearchWhileTracking_13() { return &___StopSearchWhileTracking_13; }
	inline void set_StopSearchWhileTracking_13(bool value)
	{
		___StopSearchWhileTracking_13 = value;
	}
};

struct DefaultModelRecoEventHandler_t1899894298_StaticFields
{
public:
	// System.Func`2<Vuforia.ModelTargetBehaviour,System.Boolean> DefaultModelRecoEventHandler::<>f__am$cache0
	Func_2_t1155892566 * ___U3CU3Ef__amU24cache0_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_14() { return static_cast<int32_t>(offsetof(DefaultModelRecoEventHandler_t1899894298_StaticFields, ___U3CU3Ef__amU24cache0_14)); }
	inline Func_2_t1155892566 * get_U3CU3Ef__amU24cache0_14() const { return ___U3CU3Ef__amU24cache0_14; }
	inline Func_2_t1155892566 ** get_address_of_U3CU3Ef__amU24cache0_14() { return &___U3CU3Ef__amU24cache0_14; }
	inline void set_U3CU3Ef__amU24cache0_14(Func_2_t1155892566 * value)
	{
		___U3CU3Ef__amU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTMODELRECOEVENTHANDLER_T1899894298_H
#ifndef DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#define DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t1588957063  : public MonoBehaviour_t3962482529
{
public:
	// Vuforia.TrackableBehaviour DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_4;
	// Vuforia.TrackableBehaviour/Status DefaultTrackableEventHandler::m_PreviousStatus
	int32_t ___m_PreviousStatus_5;
	// Vuforia.TrackableBehaviour/Status DefaultTrackableEventHandler::m_NewStatus
	int32_t ___m_NewStatus_6;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_4), value);
	}

	inline static int32_t get_offset_of_m_PreviousStatus_5() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___m_PreviousStatus_5)); }
	inline int32_t get_m_PreviousStatus_5() const { return ___m_PreviousStatus_5; }
	inline int32_t* get_address_of_m_PreviousStatus_5() { return &___m_PreviousStatus_5; }
	inline void set_m_PreviousStatus_5(int32_t value)
	{
		___m_PreviousStatus_5 = value;
	}

	inline static int32_t get_offset_of_m_NewStatus_6() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___m_NewStatus_6)); }
	inline int32_t get_m_NewStatus_6() const { return ___m_NewStatus_6; }
	inline int32_t* get_address_of_m_NewStatus_6() { return &___m_NewStatus_6; }
	inline void set_m_NewStatus_6(int32_t value)
	{
		___m_NewStatus_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifndef GETPOINTER_T3074287824_H
#define GETPOINTER_T3074287824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetPointer
struct  GetPointer_t3074287824  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean GetPointer::pointerActivated
	bool ___pointerActivated_4;

public:
	inline static int32_t get_offset_of_pointerActivated_4() { return static_cast<int32_t>(offsetof(GetPointer_t3074287824, ___pointerActivated_4)); }
	inline bool get_pointerActivated_4() const { return ___pointerActivated_4; }
	inline bool* get_address_of_pointerActivated_4() { return &___pointerActivated_4; }
	inline void set_pointerActivated_4(bool value)
	{
		___pointerActivated_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPOINTER_T3074287824_H
#ifndef INSTANTIATEOBJECT_T3169048492_H
#define INSTANTIATEOBJECT_T3169048492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstantiateObject
struct  InstantiateObject_t3169048492  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject InstantiateObject::anchorPlacements
	GameObject_t1113636619 * ___anchorPlacements_4;
	// UnityEngine.GameObject InstantiateObject::modelHolder
	GameObject_t1113636619 * ___modelHolder_5;
	// UnityEngine.GameObject InstantiateObject::loadingPanel
	GameObject_t1113636619 * ___loadingPanel_6;
	// UnityEngine.UI.Image InstantiateObject::loadingBar
	Image_t2670269651 * ___loadingBar_7;
	// UnityEngine.UI.Text InstantiateObject::loadingProgress
	Text_t1901882714 * ___loadingProgress_8;
	// UnityEngine.GameObject InstantiateObject::TargetGoAR
	GameObject_t1113636619 * ___TargetGoAR_9;
	// UnityEngine.GameObject InstantiateObject::TargetGoModel
	GameObject_t1113636619 * ___TargetGoModel_10;
	// UnityEngine.Material[] InstantiateObject::TargetMats
	MaterialU5BU5D_t561872642* ___TargetMats_11;
	// PlaneManager InstantiateObject::planeManager
	PlaneManager_t2021199913 * ___planeManager_12;
	// TouchHandler InstantiateObject::touchHandler
	TouchHandler_t3441426771 * ___touchHandler_13;
	// System.Collections.Generic.List`1<System.String> InstantiateObject::LoadingReferenceTable
	List_1_t3319525431 * ___LoadingReferenceTable_15;
	// System.String InstantiateObject::ConfigurationRequestURL
	String_t* ___ConfigurationRequestURL_16;
	// System.String InstantiateObject::AssetsVersion
	String_t* ___AssetsVersion_17;
	// System.Int32 InstantiateObject::currentMatNum
	int32_t ___currentMatNum_18;
	// InstantiateObject/SerializablePreviewProperties InstantiateObject::SerializablePreviewPropertiesObject
	SerializablePreviewProperties_t1656375491 * ___SerializablePreviewPropertiesObject_19;

public:
	inline static int32_t get_offset_of_anchorPlacements_4() { return static_cast<int32_t>(offsetof(InstantiateObject_t3169048492, ___anchorPlacements_4)); }
	inline GameObject_t1113636619 * get_anchorPlacements_4() const { return ___anchorPlacements_4; }
	inline GameObject_t1113636619 ** get_address_of_anchorPlacements_4() { return &___anchorPlacements_4; }
	inline void set_anchorPlacements_4(GameObject_t1113636619 * value)
	{
		___anchorPlacements_4 = value;
		Il2CppCodeGenWriteBarrier((&___anchorPlacements_4), value);
	}

	inline static int32_t get_offset_of_modelHolder_5() { return static_cast<int32_t>(offsetof(InstantiateObject_t3169048492, ___modelHolder_5)); }
	inline GameObject_t1113636619 * get_modelHolder_5() const { return ___modelHolder_5; }
	inline GameObject_t1113636619 ** get_address_of_modelHolder_5() { return &___modelHolder_5; }
	inline void set_modelHolder_5(GameObject_t1113636619 * value)
	{
		___modelHolder_5 = value;
		Il2CppCodeGenWriteBarrier((&___modelHolder_5), value);
	}

	inline static int32_t get_offset_of_loadingPanel_6() { return static_cast<int32_t>(offsetof(InstantiateObject_t3169048492, ___loadingPanel_6)); }
	inline GameObject_t1113636619 * get_loadingPanel_6() const { return ___loadingPanel_6; }
	inline GameObject_t1113636619 ** get_address_of_loadingPanel_6() { return &___loadingPanel_6; }
	inline void set_loadingPanel_6(GameObject_t1113636619 * value)
	{
		___loadingPanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___loadingPanel_6), value);
	}

	inline static int32_t get_offset_of_loadingBar_7() { return static_cast<int32_t>(offsetof(InstantiateObject_t3169048492, ___loadingBar_7)); }
	inline Image_t2670269651 * get_loadingBar_7() const { return ___loadingBar_7; }
	inline Image_t2670269651 ** get_address_of_loadingBar_7() { return &___loadingBar_7; }
	inline void set_loadingBar_7(Image_t2670269651 * value)
	{
		___loadingBar_7 = value;
		Il2CppCodeGenWriteBarrier((&___loadingBar_7), value);
	}

	inline static int32_t get_offset_of_loadingProgress_8() { return static_cast<int32_t>(offsetof(InstantiateObject_t3169048492, ___loadingProgress_8)); }
	inline Text_t1901882714 * get_loadingProgress_8() const { return ___loadingProgress_8; }
	inline Text_t1901882714 ** get_address_of_loadingProgress_8() { return &___loadingProgress_8; }
	inline void set_loadingProgress_8(Text_t1901882714 * value)
	{
		___loadingProgress_8 = value;
		Il2CppCodeGenWriteBarrier((&___loadingProgress_8), value);
	}

	inline static int32_t get_offset_of_TargetGoAR_9() { return static_cast<int32_t>(offsetof(InstantiateObject_t3169048492, ___TargetGoAR_9)); }
	inline GameObject_t1113636619 * get_TargetGoAR_9() const { return ___TargetGoAR_9; }
	inline GameObject_t1113636619 ** get_address_of_TargetGoAR_9() { return &___TargetGoAR_9; }
	inline void set_TargetGoAR_9(GameObject_t1113636619 * value)
	{
		___TargetGoAR_9 = value;
		Il2CppCodeGenWriteBarrier((&___TargetGoAR_9), value);
	}

	inline static int32_t get_offset_of_TargetGoModel_10() { return static_cast<int32_t>(offsetof(InstantiateObject_t3169048492, ___TargetGoModel_10)); }
	inline GameObject_t1113636619 * get_TargetGoModel_10() const { return ___TargetGoModel_10; }
	inline GameObject_t1113636619 ** get_address_of_TargetGoModel_10() { return &___TargetGoModel_10; }
	inline void set_TargetGoModel_10(GameObject_t1113636619 * value)
	{
		___TargetGoModel_10 = value;
		Il2CppCodeGenWriteBarrier((&___TargetGoModel_10), value);
	}

	inline static int32_t get_offset_of_TargetMats_11() { return static_cast<int32_t>(offsetof(InstantiateObject_t3169048492, ___TargetMats_11)); }
	inline MaterialU5BU5D_t561872642* get_TargetMats_11() const { return ___TargetMats_11; }
	inline MaterialU5BU5D_t561872642** get_address_of_TargetMats_11() { return &___TargetMats_11; }
	inline void set_TargetMats_11(MaterialU5BU5D_t561872642* value)
	{
		___TargetMats_11 = value;
		Il2CppCodeGenWriteBarrier((&___TargetMats_11), value);
	}

	inline static int32_t get_offset_of_planeManager_12() { return static_cast<int32_t>(offsetof(InstantiateObject_t3169048492, ___planeManager_12)); }
	inline PlaneManager_t2021199913 * get_planeManager_12() const { return ___planeManager_12; }
	inline PlaneManager_t2021199913 ** get_address_of_planeManager_12() { return &___planeManager_12; }
	inline void set_planeManager_12(PlaneManager_t2021199913 * value)
	{
		___planeManager_12 = value;
		Il2CppCodeGenWriteBarrier((&___planeManager_12), value);
	}

	inline static int32_t get_offset_of_touchHandler_13() { return static_cast<int32_t>(offsetof(InstantiateObject_t3169048492, ___touchHandler_13)); }
	inline TouchHandler_t3441426771 * get_touchHandler_13() const { return ___touchHandler_13; }
	inline TouchHandler_t3441426771 ** get_address_of_touchHandler_13() { return &___touchHandler_13; }
	inline void set_touchHandler_13(TouchHandler_t3441426771 * value)
	{
		___touchHandler_13 = value;
		Il2CppCodeGenWriteBarrier((&___touchHandler_13), value);
	}

	inline static int32_t get_offset_of_LoadingReferenceTable_15() { return static_cast<int32_t>(offsetof(InstantiateObject_t3169048492, ___LoadingReferenceTable_15)); }
	inline List_1_t3319525431 * get_LoadingReferenceTable_15() const { return ___LoadingReferenceTable_15; }
	inline List_1_t3319525431 ** get_address_of_LoadingReferenceTable_15() { return &___LoadingReferenceTable_15; }
	inline void set_LoadingReferenceTable_15(List_1_t3319525431 * value)
	{
		___LoadingReferenceTable_15 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingReferenceTable_15), value);
	}

	inline static int32_t get_offset_of_ConfigurationRequestURL_16() { return static_cast<int32_t>(offsetof(InstantiateObject_t3169048492, ___ConfigurationRequestURL_16)); }
	inline String_t* get_ConfigurationRequestURL_16() const { return ___ConfigurationRequestURL_16; }
	inline String_t** get_address_of_ConfigurationRequestURL_16() { return &___ConfigurationRequestURL_16; }
	inline void set_ConfigurationRequestURL_16(String_t* value)
	{
		___ConfigurationRequestURL_16 = value;
		Il2CppCodeGenWriteBarrier((&___ConfigurationRequestURL_16), value);
	}

	inline static int32_t get_offset_of_AssetsVersion_17() { return static_cast<int32_t>(offsetof(InstantiateObject_t3169048492, ___AssetsVersion_17)); }
	inline String_t* get_AssetsVersion_17() const { return ___AssetsVersion_17; }
	inline String_t** get_address_of_AssetsVersion_17() { return &___AssetsVersion_17; }
	inline void set_AssetsVersion_17(String_t* value)
	{
		___AssetsVersion_17 = value;
		Il2CppCodeGenWriteBarrier((&___AssetsVersion_17), value);
	}

	inline static int32_t get_offset_of_currentMatNum_18() { return static_cast<int32_t>(offsetof(InstantiateObject_t3169048492, ___currentMatNum_18)); }
	inline int32_t get_currentMatNum_18() const { return ___currentMatNum_18; }
	inline int32_t* get_address_of_currentMatNum_18() { return &___currentMatNum_18; }
	inline void set_currentMatNum_18(int32_t value)
	{
		___currentMatNum_18 = value;
	}

	inline static int32_t get_offset_of_SerializablePreviewPropertiesObject_19() { return static_cast<int32_t>(offsetof(InstantiateObject_t3169048492, ___SerializablePreviewPropertiesObject_19)); }
	inline SerializablePreviewProperties_t1656375491 * get_SerializablePreviewPropertiesObject_19() const { return ___SerializablePreviewPropertiesObject_19; }
	inline SerializablePreviewProperties_t1656375491 ** get_address_of_SerializablePreviewPropertiesObject_19() { return &___SerializablePreviewPropertiesObject_19; }
	inline void set_SerializablePreviewPropertiesObject_19(SerializablePreviewProperties_t1656375491 * value)
	{
		___SerializablePreviewPropertiesObject_19 = value;
		Il2CppCodeGenWriteBarrier((&___SerializablePreviewPropertiesObject_19), value);
	}
};

struct InstantiateObject_t3169048492_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AssetBundle> InstantiateObject::ObjectReference
	Dictionary_2_t939163551 * ___ObjectReference_14;

public:
	inline static int32_t get_offset_of_ObjectReference_14() { return static_cast<int32_t>(offsetof(InstantiateObject_t3169048492_StaticFields, ___ObjectReference_14)); }
	inline Dictionary_2_t939163551 * get_ObjectReference_14() const { return ___ObjectReference_14; }
	inline Dictionary_2_t939163551 ** get_address_of_ObjectReference_14() { return &___ObjectReference_14; }
	inline void set_ObjectReference_14(Dictionary_2_t939163551 * value)
	{
		___ObjectReference_14 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectReference_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTIATEOBJECT_T3169048492_H
#ifndef INVOKEBUTTON_T2298937379_H
#define INVOKEBUTTON_T2298937379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InvokeButton
struct  InvokeButton_t2298937379  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button InvokeButton::button
	Button_t4055032469 * ___button_4;

public:
	inline static int32_t get_offset_of_button_4() { return static_cast<int32_t>(offsetof(InvokeButton_t2298937379, ___button_4)); }
	inline Button_t4055032469 * get_button_4() const { return ___button_4; }
	inline Button_t4055032469 ** get_address_of_button_4() { return &___button_4; }
	inline void set_button_4(Button_t4055032469 * value)
	{
		___button_4 = value;
		Il2CppCodeGenWriteBarrier((&___button_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKEBUTTON_T2298937379_H
#ifndef LEFTSIDEPAGE_T1462537719_H
#define LEFTSIDEPAGE_T1462537719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeftSidePage
struct  LeftSidePage_t1462537719  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject LeftSidePage::page
	GameObject_t1113636619 * ___page_4;
	// System.Boolean LeftSidePage::getActivate
	bool ___getActivate_5;
	// System.Single LeftSidePage::speed
	float ___speed_6;
	// System.Single LeftSidePage::startTime
	float ___startTime_7;

public:
	inline static int32_t get_offset_of_page_4() { return static_cast<int32_t>(offsetof(LeftSidePage_t1462537719, ___page_4)); }
	inline GameObject_t1113636619 * get_page_4() const { return ___page_4; }
	inline GameObject_t1113636619 ** get_address_of_page_4() { return &___page_4; }
	inline void set_page_4(GameObject_t1113636619 * value)
	{
		___page_4 = value;
		Il2CppCodeGenWriteBarrier((&___page_4), value);
	}

	inline static int32_t get_offset_of_getActivate_5() { return static_cast<int32_t>(offsetof(LeftSidePage_t1462537719, ___getActivate_5)); }
	inline bool get_getActivate_5() const { return ___getActivate_5; }
	inline bool* get_address_of_getActivate_5() { return &___getActivate_5; }
	inline void set_getActivate_5(bool value)
	{
		___getActivate_5 = value;
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(LeftSidePage_t1462537719, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}

	inline static int32_t get_offset_of_startTime_7() { return static_cast<int32_t>(offsetof(LeftSidePage_t1462537719, ___startTime_7)); }
	inline float get_startTime_7() const { return ___startTime_7; }
	inline float* get_address_of_startTime_7() { return &___startTime_7; }
	inline void set_startTime_7(float value)
	{
		___startTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEFTSIDEPAGE_T1462537719_H
#ifndef MENUSIDEPAGE_T3698810816_H
#define MENUSIDEPAGE_T3698810816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuSidePage
struct  MenuSidePage_t3698810816  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MenuSidePage::page
	GameObject_t1113636619 * ___page_4;
	// System.Boolean MenuSidePage::getActivate
	bool ___getActivate_5;
	// System.Single MenuSidePage::speed
	float ___speed_6;
	// System.Single MenuSidePage::startTime
	float ___startTime_7;

public:
	inline static int32_t get_offset_of_page_4() { return static_cast<int32_t>(offsetof(MenuSidePage_t3698810816, ___page_4)); }
	inline GameObject_t1113636619 * get_page_4() const { return ___page_4; }
	inline GameObject_t1113636619 ** get_address_of_page_4() { return &___page_4; }
	inline void set_page_4(GameObject_t1113636619 * value)
	{
		___page_4 = value;
		Il2CppCodeGenWriteBarrier((&___page_4), value);
	}

	inline static int32_t get_offset_of_getActivate_5() { return static_cast<int32_t>(offsetof(MenuSidePage_t3698810816, ___getActivate_5)); }
	inline bool get_getActivate_5() const { return ___getActivate_5; }
	inline bool* get_address_of_getActivate_5() { return &___getActivate_5; }
	inline void set_getActivate_5(bool value)
	{
		___getActivate_5 = value;
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(MenuSidePage_t3698810816, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}

	inline static int32_t get_offset_of_startTime_7() { return static_cast<int32_t>(offsetof(MenuSidePage_t3698810816, ___startTime_7)); }
	inline float get_startTime_7() const { return ___startTime_7; }
	inline float* get_address_of_startTime_7() { return &___startTime_7; }
	inline void set_startTime_7(float value)
	{
		___startTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUSIDEPAGE_T3698810816_H
#ifndef MODELMOVER_T1710746260_H
#define MODELMOVER_T1710746260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModelMover
struct  ModelMover_t1710746260  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean ModelMover::VerticalAxisMoveable
	bool ___VerticalAxisMoveable_4;
	// System.Boolean ModelMover::enablePinchScaling
	bool ___enablePinchScaling_5;
	// UnityEngine.Transform ModelMover::TargetRotationTransform
	Transform_t3600365921 * ___TargetRotationTransform_6;
	// GetPointer ModelMover::getPointer
	GetPointer_t3074287824 * ___getPointer_7;
	// System.Boolean ModelMover::InputDetected
	bool ___InputDetected_9;
	// System.Boolean ModelMover::HorizontalMovementDetected
	bool ___HorizontalMovementDetected_10;
	// System.Boolean ModelMover::SensitivityThresholdPassed
	bool ___SensitivityThresholdPassed_11;
	// System.Single ModelMover::rotY
	float ___rotY_12;
	// UnityEngine.Transform ModelMover::VerticalAxisTransformer
	Transform_t3600365921 * ___VerticalAxisTransformer_13;
	// UnityEngine.Vector3 ModelMover::LastInputPosition
	Vector3_t3722313464  ___LastInputPosition_14;
	// UnityEngine.Vector3 ModelMover::HRotationVector
	Vector3_t3722313464  ___HRotationVector_15;
	// UnityEngine.Vector3 ModelMover::VRotationVector
	Vector3_t3722313464  ___VRotationVector_16;
	// UnityEngine.Touch[] ModelMover::touches
	TouchU5BU5D_t1849554061* ___touches_19;
	// System.Boolean ModelMover::isFirstFrameWithTwoTouches
	bool ___isFirstFrameWithTwoTouches_21;
	// System.Single ModelMover::cachedTouchAngle
	float ___cachedTouchAngle_22;
	// System.Single ModelMover::cachedTouchDistance
	float ___cachedTouchDistance_23;
	// System.Single ModelMover::cachedAugmentationScale
	float ___cachedAugmentationScale_24;
	// UnityEngine.Vector3 ModelMover::cachedAugmentationRotation
	Vector3_t3722313464  ___cachedAugmentationRotation_25;

public:
	inline static int32_t get_offset_of_VerticalAxisMoveable_4() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___VerticalAxisMoveable_4)); }
	inline bool get_VerticalAxisMoveable_4() const { return ___VerticalAxisMoveable_4; }
	inline bool* get_address_of_VerticalAxisMoveable_4() { return &___VerticalAxisMoveable_4; }
	inline void set_VerticalAxisMoveable_4(bool value)
	{
		___VerticalAxisMoveable_4 = value;
	}

	inline static int32_t get_offset_of_enablePinchScaling_5() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___enablePinchScaling_5)); }
	inline bool get_enablePinchScaling_5() const { return ___enablePinchScaling_5; }
	inline bool* get_address_of_enablePinchScaling_5() { return &___enablePinchScaling_5; }
	inline void set_enablePinchScaling_5(bool value)
	{
		___enablePinchScaling_5 = value;
	}

	inline static int32_t get_offset_of_TargetRotationTransform_6() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___TargetRotationTransform_6)); }
	inline Transform_t3600365921 * get_TargetRotationTransform_6() const { return ___TargetRotationTransform_6; }
	inline Transform_t3600365921 ** get_address_of_TargetRotationTransform_6() { return &___TargetRotationTransform_6; }
	inline void set_TargetRotationTransform_6(Transform_t3600365921 * value)
	{
		___TargetRotationTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___TargetRotationTransform_6), value);
	}

	inline static int32_t get_offset_of_getPointer_7() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___getPointer_7)); }
	inline GetPointer_t3074287824 * get_getPointer_7() const { return ___getPointer_7; }
	inline GetPointer_t3074287824 ** get_address_of_getPointer_7() { return &___getPointer_7; }
	inline void set_getPointer_7(GetPointer_t3074287824 * value)
	{
		___getPointer_7 = value;
		Il2CppCodeGenWriteBarrier((&___getPointer_7), value);
	}

	inline static int32_t get_offset_of_InputDetected_9() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___InputDetected_9)); }
	inline bool get_InputDetected_9() const { return ___InputDetected_9; }
	inline bool* get_address_of_InputDetected_9() { return &___InputDetected_9; }
	inline void set_InputDetected_9(bool value)
	{
		___InputDetected_9 = value;
	}

	inline static int32_t get_offset_of_HorizontalMovementDetected_10() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___HorizontalMovementDetected_10)); }
	inline bool get_HorizontalMovementDetected_10() const { return ___HorizontalMovementDetected_10; }
	inline bool* get_address_of_HorizontalMovementDetected_10() { return &___HorizontalMovementDetected_10; }
	inline void set_HorizontalMovementDetected_10(bool value)
	{
		___HorizontalMovementDetected_10 = value;
	}

	inline static int32_t get_offset_of_SensitivityThresholdPassed_11() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___SensitivityThresholdPassed_11)); }
	inline bool get_SensitivityThresholdPassed_11() const { return ___SensitivityThresholdPassed_11; }
	inline bool* get_address_of_SensitivityThresholdPassed_11() { return &___SensitivityThresholdPassed_11; }
	inline void set_SensitivityThresholdPassed_11(bool value)
	{
		___SensitivityThresholdPassed_11 = value;
	}

	inline static int32_t get_offset_of_rotY_12() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___rotY_12)); }
	inline float get_rotY_12() const { return ___rotY_12; }
	inline float* get_address_of_rotY_12() { return &___rotY_12; }
	inline void set_rotY_12(float value)
	{
		___rotY_12 = value;
	}

	inline static int32_t get_offset_of_VerticalAxisTransformer_13() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___VerticalAxisTransformer_13)); }
	inline Transform_t3600365921 * get_VerticalAxisTransformer_13() const { return ___VerticalAxisTransformer_13; }
	inline Transform_t3600365921 ** get_address_of_VerticalAxisTransformer_13() { return &___VerticalAxisTransformer_13; }
	inline void set_VerticalAxisTransformer_13(Transform_t3600365921 * value)
	{
		___VerticalAxisTransformer_13 = value;
		Il2CppCodeGenWriteBarrier((&___VerticalAxisTransformer_13), value);
	}

	inline static int32_t get_offset_of_LastInputPosition_14() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___LastInputPosition_14)); }
	inline Vector3_t3722313464  get_LastInputPosition_14() const { return ___LastInputPosition_14; }
	inline Vector3_t3722313464 * get_address_of_LastInputPosition_14() { return &___LastInputPosition_14; }
	inline void set_LastInputPosition_14(Vector3_t3722313464  value)
	{
		___LastInputPosition_14 = value;
	}

	inline static int32_t get_offset_of_HRotationVector_15() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___HRotationVector_15)); }
	inline Vector3_t3722313464  get_HRotationVector_15() const { return ___HRotationVector_15; }
	inline Vector3_t3722313464 * get_address_of_HRotationVector_15() { return &___HRotationVector_15; }
	inline void set_HRotationVector_15(Vector3_t3722313464  value)
	{
		___HRotationVector_15 = value;
	}

	inline static int32_t get_offset_of_VRotationVector_16() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___VRotationVector_16)); }
	inline Vector3_t3722313464  get_VRotationVector_16() const { return ___VRotationVector_16; }
	inline Vector3_t3722313464 * get_address_of_VRotationVector_16() { return &___VRotationVector_16; }
	inline void set_VRotationVector_16(Vector3_t3722313464  value)
	{
		___VRotationVector_16 = value;
	}

	inline static int32_t get_offset_of_touches_19() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___touches_19)); }
	inline TouchU5BU5D_t1849554061* get_touches_19() const { return ___touches_19; }
	inline TouchU5BU5D_t1849554061** get_address_of_touches_19() { return &___touches_19; }
	inline void set_touches_19(TouchU5BU5D_t1849554061* value)
	{
		___touches_19 = value;
		Il2CppCodeGenWriteBarrier((&___touches_19), value);
	}

	inline static int32_t get_offset_of_isFirstFrameWithTwoTouches_21() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___isFirstFrameWithTwoTouches_21)); }
	inline bool get_isFirstFrameWithTwoTouches_21() const { return ___isFirstFrameWithTwoTouches_21; }
	inline bool* get_address_of_isFirstFrameWithTwoTouches_21() { return &___isFirstFrameWithTwoTouches_21; }
	inline void set_isFirstFrameWithTwoTouches_21(bool value)
	{
		___isFirstFrameWithTwoTouches_21 = value;
	}

	inline static int32_t get_offset_of_cachedTouchAngle_22() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___cachedTouchAngle_22)); }
	inline float get_cachedTouchAngle_22() const { return ___cachedTouchAngle_22; }
	inline float* get_address_of_cachedTouchAngle_22() { return &___cachedTouchAngle_22; }
	inline void set_cachedTouchAngle_22(float value)
	{
		___cachedTouchAngle_22 = value;
	}

	inline static int32_t get_offset_of_cachedTouchDistance_23() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___cachedTouchDistance_23)); }
	inline float get_cachedTouchDistance_23() const { return ___cachedTouchDistance_23; }
	inline float* get_address_of_cachedTouchDistance_23() { return &___cachedTouchDistance_23; }
	inline void set_cachedTouchDistance_23(float value)
	{
		___cachedTouchDistance_23 = value;
	}

	inline static int32_t get_offset_of_cachedAugmentationScale_24() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___cachedAugmentationScale_24)); }
	inline float get_cachedAugmentationScale_24() const { return ___cachedAugmentationScale_24; }
	inline float* get_address_of_cachedAugmentationScale_24() { return &___cachedAugmentationScale_24; }
	inline void set_cachedAugmentationScale_24(float value)
	{
		___cachedAugmentationScale_24 = value;
	}

	inline static int32_t get_offset_of_cachedAugmentationRotation_25() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260, ___cachedAugmentationRotation_25)); }
	inline Vector3_t3722313464  get_cachedAugmentationRotation_25() const { return ___cachedAugmentationRotation_25; }
	inline Vector3_t3722313464 * get_address_of_cachedAugmentationRotation_25() { return &___cachedAugmentationRotation_25; }
	inline void set_cachedAugmentationRotation_25(Vector3_t3722313464  value)
	{
		___cachedAugmentationRotation_25 = value;
	}
};

struct ModelMover_t1710746260_StaticFields
{
public:
	// System.Boolean ModelMover::Active
	bool ___Active_8;
	// System.Int32 ModelMover::lastTouchCount
	int32_t ___lastTouchCount_20;

public:
	inline static int32_t get_offset_of_Active_8() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260_StaticFields, ___Active_8)); }
	inline bool get_Active_8() const { return ___Active_8; }
	inline bool* get_address_of_Active_8() { return &___Active_8; }
	inline void set_Active_8(bool value)
	{
		___Active_8 = value;
	}

	inline static int32_t get_offset_of_lastTouchCount_20() { return static_cast<int32_t>(offsetof(ModelMover_t1710746260_StaticFields, ___lastTouchCount_20)); }
	inline int32_t get_lastTouchCount_20() const { return ___lastTouchCount_20; }
	inline int32_t* get_address_of_lastTouchCount_20() { return &___lastTouchCount_20; }
	inline void set_lastTouchCount_20(int32_t value)
	{
		___lastTouchCount_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELMOVER_T1710746260_H
#ifndef OBJECTCOMPONENTS_T1076493220_H
#define OBJECTCOMPONENTS_T1076493220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectComponents
struct  ObjectComponents_t1076493220  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ObjectComponents::objectSelf
	GameObject_t1113636619 * ___objectSelf_4;
	// UnityEngine.Transform ObjectComponents::objectPivot
	Transform_t3600365921 * ___objectPivot_5;
	// UnityEngine.Transform ObjectComponents::objectHolder
	Transform_t3600365921 * ___objectHolder_6;
	// UnityEngine.Transform ObjectComponents::objectVisual
	Transform_t3600365921 * ___objectVisual_7;
	// UnityEngine.BoxCollider ObjectComponents::objectBoxCollider
	BoxCollider_t1640800422 * ___objectBoxCollider_8;
	// UnityEngine.MeshRenderer ObjectComponents::objectRenderer
	MeshRenderer_t587009260 * ___objectRenderer_9;
	// UnityEngine.MeshRenderer ObjectComponents::objectShadow
	MeshRenderer_t587009260 * ___objectShadow_10;
	// UnityEngine.Animator ObjectComponents::objectAnimator
	Animator_t434523843 * ___objectAnimator_11;

public:
	inline static int32_t get_offset_of_objectSelf_4() { return static_cast<int32_t>(offsetof(ObjectComponents_t1076493220, ___objectSelf_4)); }
	inline GameObject_t1113636619 * get_objectSelf_4() const { return ___objectSelf_4; }
	inline GameObject_t1113636619 ** get_address_of_objectSelf_4() { return &___objectSelf_4; }
	inline void set_objectSelf_4(GameObject_t1113636619 * value)
	{
		___objectSelf_4 = value;
		Il2CppCodeGenWriteBarrier((&___objectSelf_4), value);
	}

	inline static int32_t get_offset_of_objectPivot_5() { return static_cast<int32_t>(offsetof(ObjectComponents_t1076493220, ___objectPivot_5)); }
	inline Transform_t3600365921 * get_objectPivot_5() const { return ___objectPivot_5; }
	inline Transform_t3600365921 ** get_address_of_objectPivot_5() { return &___objectPivot_5; }
	inline void set_objectPivot_5(Transform_t3600365921 * value)
	{
		___objectPivot_5 = value;
		Il2CppCodeGenWriteBarrier((&___objectPivot_5), value);
	}

	inline static int32_t get_offset_of_objectHolder_6() { return static_cast<int32_t>(offsetof(ObjectComponents_t1076493220, ___objectHolder_6)); }
	inline Transform_t3600365921 * get_objectHolder_6() const { return ___objectHolder_6; }
	inline Transform_t3600365921 ** get_address_of_objectHolder_6() { return &___objectHolder_6; }
	inline void set_objectHolder_6(Transform_t3600365921 * value)
	{
		___objectHolder_6 = value;
		Il2CppCodeGenWriteBarrier((&___objectHolder_6), value);
	}

	inline static int32_t get_offset_of_objectVisual_7() { return static_cast<int32_t>(offsetof(ObjectComponents_t1076493220, ___objectVisual_7)); }
	inline Transform_t3600365921 * get_objectVisual_7() const { return ___objectVisual_7; }
	inline Transform_t3600365921 ** get_address_of_objectVisual_7() { return &___objectVisual_7; }
	inline void set_objectVisual_7(Transform_t3600365921 * value)
	{
		___objectVisual_7 = value;
		Il2CppCodeGenWriteBarrier((&___objectVisual_7), value);
	}

	inline static int32_t get_offset_of_objectBoxCollider_8() { return static_cast<int32_t>(offsetof(ObjectComponents_t1076493220, ___objectBoxCollider_8)); }
	inline BoxCollider_t1640800422 * get_objectBoxCollider_8() const { return ___objectBoxCollider_8; }
	inline BoxCollider_t1640800422 ** get_address_of_objectBoxCollider_8() { return &___objectBoxCollider_8; }
	inline void set_objectBoxCollider_8(BoxCollider_t1640800422 * value)
	{
		___objectBoxCollider_8 = value;
		Il2CppCodeGenWriteBarrier((&___objectBoxCollider_8), value);
	}

	inline static int32_t get_offset_of_objectRenderer_9() { return static_cast<int32_t>(offsetof(ObjectComponents_t1076493220, ___objectRenderer_9)); }
	inline MeshRenderer_t587009260 * get_objectRenderer_9() const { return ___objectRenderer_9; }
	inline MeshRenderer_t587009260 ** get_address_of_objectRenderer_9() { return &___objectRenderer_9; }
	inline void set_objectRenderer_9(MeshRenderer_t587009260 * value)
	{
		___objectRenderer_9 = value;
		Il2CppCodeGenWriteBarrier((&___objectRenderer_9), value);
	}

	inline static int32_t get_offset_of_objectShadow_10() { return static_cast<int32_t>(offsetof(ObjectComponents_t1076493220, ___objectShadow_10)); }
	inline MeshRenderer_t587009260 * get_objectShadow_10() const { return ___objectShadow_10; }
	inline MeshRenderer_t587009260 ** get_address_of_objectShadow_10() { return &___objectShadow_10; }
	inline void set_objectShadow_10(MeshRenderer_t587009260 * value)
	{
		___objectShadow_10 = value;
		Il2CppCodeGenWriteBarrier((&___objectShadow_10), value);
	}

	inline static int32_t get_offset_of_objectAnimator_11() { return static_cast<int32_t>(offsetof(ObjectComponents_t1076493220, ___objectAnimator_11)); }
	inline Animator_t434523843 * get_objectAnimator_11() const { return ___objectAnimator_11; }
	inline Animator_t434523843 ** get_address_of_objectAnimator_11() { return &___objectAnimator_11; }
	inline void set_objectAnimator_11(Animator_t434523843 * value)
	{
		___objectAnimator_11 = value;
		Il2CppCodeGenWriteBarrier((&___objectAnimator_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCOMPONENTS_T1076493220_H
#ifndef PRODUCTPAGE_T3134713323_H
#define PRODUCTPAGE_T3134713323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProductPage
struct  ProductPage_t3134713323  : public MonoBehaviour_t3962482529
{
public:
	// InstantiateObject ProductPage::instantiateObject
	InstantiateObject_t3169048492 * ___instantiateObject_4;
	// GetPointer ProductPage::getPointer
	GetPointer_t3074287824 * ___getPointer_5;
	// UnityEngine.GameObject ProductPage::page
	GameObject_t1113636619 * ___page_6;
	// UnityEngine.GameObject ProductPage::part2Page
	GameObject_t1113636619 * ___part2Page_7;
	// UnityEngine.GameObject ProductPage::threeDButton
	GameObject_t1113636619 * ___threeDButton_8;
	// UnityEngine.GameObject ProductPage::partA
	GameObject_t1113636619 * ___partA_9;
	// UnityEngine.GameObject ProductPage::partB
	GameObject_t1113636619 * ___partB_10;
	// System.Boolean ProductPage::getActivate
	bool ___getActivate_11;
	// UnityEngine.GameObject ProductPage::material1
	GameObject_t1113636619 * ___material1_12;
	// UnityEngine.GameObject ProductPage::returnPage
	GameObject_t1113636619 * ___returnPage_13;
	// System.String ProductPage::currentObjectName
	String_t* ___currentObjectName_14;
	// System.String ProductPage::mode
	String_t* ___mode_15;
	// System.Single ProductPage::updateHeight
	float ___updateHeight_16;
	// System.Single ProductPage::speed
	float ___speed_17;
	// System.Single ProductPage::startTime
	float ___startTime_18;
	// System.Single ProductPage::offset
	float ___offset_19;

public:
	inline static int32_t get_offset_of_instantiateObject_4() { return static_cast<int32_t>(offsetof(ProductPage_t3134713323, ___instantiateObject_4)); }
	inline InstantiateObject_t3169048492 * get_instantiateObject_4() const { return ___instantiateObject_4; }
	inline InstantiateObject_t3169048492 ** get_address_of_instantiateObject_4() { return &___instantiateObject_4; }
	inline void set_instantiateObject_4(InstantiateObject_t3169048492 * value)
	{
		___instantiateObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___instantiateObject_4), value);
	}

	inline static int32_t get_offset_of_getPointer_5() { return static_cast<int32_t>(offsetof(ProductPage_t3134713323, ___getPointer_5)); }
	inline GetPointer_t3074287824 * get_getPointer_5() const { return ___getPointer_5; }
	inline GetPointer_t3074287824 ** get_address_of_getPointer_5() { return &___getPointer_5; }
	inline void set_getPointer_5(GetPointer_t3074287824 * value)
	{
		___getPointer_5 = value;
		Il2CppCodeGenWriteBarrier((&___getPointer_5), value);
	}

	inline static int32_t get_offset_of_page_6() { return static_cast<int32_t>(offsetof(ProductPage_t3134713323, ___page_6)); }
	inline GameObject_t1113636619 * get_page_6() const { return ___page_6; }
	inline GameObject_t1113636619 ** get_address_of_page_6() { return &___page_6; }
	inline void set_page_6(GameObject_t1113636619 * value)
	{
		___page_6 = value;
		Il2CppCodeGenWriteBarrier((&___page_6), value);
	}

	inline static int32_t get_offset_of_part2Page_7() { return static_cast<int32_t>(offsetof(ProductPage_t3134713323, ___part2Page_7)); }
	inline GameObject_t1113636619 * get_part2Page_7() const { return ___part2Page_7; }
	inline GameObject_t1113636619 ** get_address_of_part2Page_7() { return &___part2Page_7; }
	inline void set_part2Page_7(GameObject_t1113636619 * value)
	{
		___part2Page_7 = value;
		Il2CppCodeGenWriteBarrier((&___part2Page_7), value);
	}

	inline static int32_t get_offset_of_threeDButton_8() { return static_cast<int32_t>(offsetof(ProductPage_t3134713323, ___threeDButton_8)); }
	inline GameObject_t1113636619 * get_threeDButton_8() const { return ___threeDButton_8; }
	inline GameObject_t1113636619 ** get_address_of_threeDButton_8() { return &___threeDButton_8; }
	inline void set_threeDButton_8(GameObject_t1113636619 * value)
	{
		___threeDButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___threeDButton_8), value);
	}

	inline static int32_t get_offset_of_partA_9() { return static_cast<int32_t>(offsetof(ProductPage_t3134713323, ___partA_9)); }
	inline GameObject_t1113636619 * get_partA_9() const { return ___partA_9; }
	inline GameObject_t1113636619 ** get_address_of_partA_9() { return &___partA_9; }
	inline void set_partA_9(GameObject_t1113636619 * value)
	{
		___partA_9 = value;
		Il2CppCodeGenWriteBarrier((&___partA_9), value);
	}

	inline static int32_t get_offset_of_partB_10() { return static_cast<int32_t>(offsetof(ProductPage_t3134713323, ___partB_10)); }
	inline GameObject_t1113636619 * get_partB_10() const { return ___partB_10; }
	inline GameObject_t1113636619 ** get_address_of_partB_10() { return &___partB_10; }
	inline void set_partB_10(GameObject_t1113636619 * value)
	{
		___partB_10 = value;
		Il2CppCodeGenWriteBarrier((&___partB_10), value);
	}

	inline static int32_t get_offset_of_getActivate_11() { return static_cast<int32_t>(offsetof(ProductPage_t3134713323, ___getActivate_11)); }
	inline bool get_getActivate_11() const { return ___getActivate_11; }
	inline bool* get_address_of_getActivate_11() { return &___getActivate_11; }
	inline void set_getActivate_11(bool value)
	{
		___getActivate_11 = value;
	}

	inline static int32_t get_offset_of_material1_12() { return static_cast<int32_t>(offsetof(ProductPage_t3134713323, ___material1_12)); }
	inline GameObject_t1113636619 * get_material1_12() const { return ___material1_12; }
	inline GameObject_t1113636619 ** get_address_of_material1_12() { return &___material1_12; }
	inline void set_material1_12(GameObject_t1113636619 * value)
	{
		___material1_12 = value;
		Il2CppCodeGenWriteBarrier((&___material1_12), value);
	}

	inline static int32_t get_offset_of_returnPage_13() { return static_cast<int32_t>(offsetof(ProductPage_t3134713323, ___returnPage_13)); }
	inline GameObject_t1113636619 * get_returnPage_13() const { return ___returnPage_13; }
	inline GameObject_t1113636619 ** get_address_of_returnPage_13() { return &___returnPage_13; }
	inline void set_returnPage_13(GameObject_t1113636619 * value)
	{
		___returnPage_13 = value;
		Il2CppCodeGenWriteBarrier((&___returnPage_13), value);
	}

	inline static int32_t get_offset_of_currentObjectName_14() { return static_cast<int32_t>(offsetof(ProductPage_t3134713323, ___currentObjectName_14)); }
	inline String_t* get_currentObjectName_14() const { return ___currentObjectName_14; }
	inline String_t** get_address_of_currentObjectName_14() { return &___currentObjectName_14; }
	inline void set_currentObjectName_14(String_t* value)
	{
		___currentObjectName_14 = value;
		Il2CppCodeGenWriteBarrier((&___currentObjectName_14), value);
	}

	inline static int32_t get_offset_of_mode_15() { return static_cast<int32_t>(offsetof(ProductPage_t3134713323, ___mode_15)); }
	inline String_t* get_mode_15() const { return ___mode_15; }
	inline String_t** get_address_of_mode_15() { return &___mode_15; }
	inline void set_mode_15(String_t* value)
	{
		___mode_15 = value;
		Il2CppCodeGenWriteBarrier((&___mode_15), value);
	}

	inline static int32_t get_offset_of_updateHeight_16() { return static_cast<int32_t>(offsetof(ProductPage_t3134713323, ___updateHeight_16)); }
	inline float get_updateHeight_16() const { return ___updateHeight_16; }
	inline float* get_address_of_updateHeight_16() { return &___updateHeight_16; }
	inline void set_updateHeight_16(float value)
	{
		___updateHeight_16 = value;
	}

	inline static int32_t get_offset_of_speed_17() { return static_cast<int32_t>(offsetof(ProductPage_t3134713323, ___speed_17)); }
	inline float get_speed_17() const { return ___speed_17; }
	inline float* get_address_of_speed_17() { return &___speed_17; }
	inline void set_speed_17(float value)
	{
		___speed_17 = value;
	}

	inline static int32_t get_offset_of_startTime_18() { return static_cast<int32_t>(offsetof(ProductPage_t3134713323, ___startTime_18)); }
	inline float get_startTime_18() const { return ___startTime_18; }
	inline float* get_address_of_startTime_18() { return &___startTime_18; }
	inline void set_startTime_18(float value)
	{
		___startTime_18 = value;
	}

	inline static int32_t get_offset_of_offset_19() { return static_cast<int32_t>(offsetof(ProductPage_t3134713323, ___offset_19)); }
	inline float get_offset_19() const { return ___offset_19; }
	inline float* get_address_of_offset_19() { return &___offset_19; }
	inline void set_offset_19(float value)
	{
		___offset_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTPAGE_T3134713323_H
#ifndef RIGHTSIDEPAGE_T131624286_H
#define RIGHTSIDEPAGE_T131624286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RightSidePage
struct  RightSidePage_t131624286  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject RightSidePage::page
	GameObject_t1113636619 * ___page_4;
	// System.Boolean RightSidePage::getActivate
	bool ___getActivate_5;
	// System.Single RightSidePage::speed
	float ___speed_6;
	// System.Single RightSidePage::startTime
	float ___startTime_7;

public:
	inline static int32_t get_offset_of_page_4() { return static_cast<int32_t>(offsetof(RightSidePage_t131624286, ___page_4)); }
	inline GameObject_t1113636619 * get_page_4() const { return ___page_4; }
	inline GameObject_t1113636619 ** get_address_of_page_4() { return &___page_4; }
	inline void set_page_4(GameObject_t1113636619 * value)
	{
		___page_4 = value;
		Il2CppCodeGenWriteBarrier((&___page_4), value);
	}

	inline static int32_t get_offset_of_getActivate_5() { return static_cast<int32_t>(offsetof(RightSidePage_t131624286, ___getActivate_5)); }
	inline bool get_getActivate_5() const { return ___getActivate_5; }
	inline bool* get_address_of_getActivate_5() { return &___getActivate_5; }
	inline void set_getActivate_5(bool value)
	{
		___getActivate_5 = value;
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(RightSidePage_t131624286, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}

	inline static int32_t get_offset_of_startTime_7() { return static_cast<int32_t>(offsetof(RightSidePage_t131624286, ___startTime_7)); }
	inline float get_startTime_7() const { return ___startTime_7; }
	inline float* get_address_of_startTime_7() { return &___startTime_7; }
	inline void set_startTime_7(float value)
	{
		___startTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGHTSIDEPAGE_T131624286_H
#ifndef SCROLLSNAPEX_T1558793980_H
#define SCROLLSNAPEX_T1558793980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollSnapEx
struct  ScrollSnapEx_t1558793980  : public MonoBehaviour_t3962482529
{
public:
	// ScrollRectEx ScrollSnapEx::_scroll_rect_ex
	ScrollRectEx_t4149042633 * ____scroll_rect_ex_4;
	// UnityEngine.RectTransform ScrollSnapEx::_scrollRectTransform
	RectTransform_t3704657025 * ____scrollRectTransform_5;
	// UnityEngine.Transform ScrollSnapEx::_listContainerTransform
	Transform_t3600365921 * ____listContainerTransform_6;
	// System.Int32 ScrollSnapEx::_pages
	int32_t ____pages_7;
	// System.Int32 ScrollSnapEx::_startingPage
	int32_t ____startingPage_8;
	// UnityEngine.Vector3[] ScrollSnapEx::_pageAnchorPositions
	Vector3U5BU5D_t1718750761* ____pageAnchorPositions_9;
	// UnityEngine.Vector3 ScrollSnapEx::_lerpTarget
	Vector3_t3722313464  ____lerpTarget_10;
	// System.Boolean ScrollSnapEx::_lerp
	bool ____lerp_11;
	// System.Single ScrollSnapEx::_listContainerMinPosition
	float ____listContainerMinPosition_12;
	// System.Single ScrollSnapEx::_listContainerMaxPosition
	float ____listContainerMaxPosition_13;
	// System.Single ScrollSnapEx::_listContainerSize
	float ____listContainerSize_14;
	// UnityEngine.RectTransform ScrollSnapEx::_listContainerRectTransform
	RectTransform_t3704657025 * ____listContainerRectTransform_15;
	// UnityEngine.Vector2 ScrollSnapEx::_listContainerCachedSize
	Vector2_t2156229523  ____listContainerCachedSize_16;
	// System.Single ScrollSnapEx::_itemSize
	float ____itemSize_17;
	// System.Int32 ScrollSnapEx::_itemsCount
	int32_t ____itemsCount_18;
	// System.Boolean ScrollSnapEx::_startDrag
	bool ____startDrag_19;
	// UnityEngine.Vector3 ScrollSnapEx::_positionOnDragStart
	Vector3_t3722313464  ____positionOnDragStart_20;
	// System.Int32 ScrollSnapEx::_pageOnDragStart
	int32_t ____pageOnDragStart_21;
	// System.Boolean ScrollSnapEx::_fastSwipeTimer
	bool ____fastSwipeTimer_22;
	// System.Int32 ScrollSnapEx::_fastSwipeCounter
	int32_t ____fastSwipeCounter_23;
	// System.Int32 ScrollSnapEx::_fastSwipeTarget
	int32_t ____fastSwipeTarget_24;
	// UnityEngine.UI.Button ScrollSnapEx::NextButton
	Button_t4055032469 * ___NextButton_25;
	// UnityEngine.UI.Button ScrollSnapEx::PrevButton
	Button_t4055032469 * ___PrevButton_26;
	// System.Int32 ScrollSnapEx::ItemsVisibleAtOnce
	int32_t ___ItemsVisibleAtOnce_27;
	// System.Boolean ScrollSnapEx::AutoLayoutItems
	bool ___AutoLayoutItems_28;
	// System.Boolean ScrollSnapEx::LinkScrolbarSteps
	bool ___LinkScrolbarSteps_29;
	// System.Boolean ScrollSnapEx::LinkScrolrectScrollSensitivity
	bool ___LinkScrolrectScrollSensitivity_30;
	// System.Boolean ScrollSnapEx::UseFastSwipe
	bool ___UseFastSwipe_31;
	// System.Int32 ScrollSnapEx::FastSwipeThreshold
	int32_t ___FastSwipeThreshold_32;
	// ScrollSnapEx/PageSnapChange ScrollSnapEx::onPageChange
	PageSnapChange_t2018612193 * ___onPageChange_33;
	// ScrollSnapEx/ScrollDirection ScrollSnapEx::direction
	int32_t ___direction_34;
	// UnityEngine.Transform ScrollSnapEx::dotsPosition
	Transform_t3600365921 * ___dotsPosition_35;
	// System.Boolean ScrollSnapEx::fastSwipe
	bool ___fastSwipe_36;

public:
	inline static int32_t get_offset_of__scroll_rect_ex_4() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____scroll_rect_ex_4)); }
	inline ScrollRectEx_t4149042633 * get__scroll_rect_ex_4() const { return ____scroll_rect_ex_4; }
	inline ScrollRectEx_t4149042633 ** get_address_of__scroll_rect_ex_4() { return &____scroll_rect_ex_4; }
	inline void set__scroll_rect_ex_4(ScrollRectEx_t4149042633 * value)
	{
		____scroll_rect_ex_4 = value;
		Il2CppCodeGenWriteBarrier((&____scroll_rect_ex_4), value);
	}

	inline static int32_t get_offset_of__scrollRectTransform_5() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____scrollRectTransform_5)); }
	inline RectTransform_t3704657025 * get__scrollRectTransform_5() const { return ____scrollRectTransform_5; }
	inline RectTransform_t3704657025 ** get_address_of__scrollRectTransform_5() { return &____scrollRectTransform_5; }
	inline void set__scrollRectTransform_5(RectTransform_t3704657025 * value)
	{
		____scrollRectTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRectTransform_5), value);
	}

	inline static int32_t get_offset_of__listContainerTransform_6() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____listContainerTransform_6)); }
	inline Transform_t3600365921 * get__listContainerTransform_6() const { return ____listContainerTransform_6; }
	inline Transform_t3600365921 ** get_address_of__listContainerTransform_6() { return &____listContainerTransform_6; }
	inline void set__listContainerTransform_6(Transform_t3600365921 * value)
	{
		____listContainerTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&____listContainerTransform_6), value);
	}

	inline static int32_t get_offset_of__pages_7() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____pages_7)); }
	inline int32_t get__pages_7() const { return ____pages_7; }
	inline int32_t* get_address_of__pages_7() { return &____pages_7; }
	inline void set__pages_7(int32_t value)
	{
		____pages_7 = value;
	}

	inline static int32_t get_offset_of__startingPage_8() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____startingPage_8)); }
	inline int32_t get__startingPage_8() const { return ____startingPage_8; }
	inline int32_t* get_address_of__startingPage_8() { return &____startingPage_8; }
	inline void set__startingPage_8(int32_t value)
	{
		____startingPage_8 = value;
	}

	inline static int32_t get_offset_of__pageAnchorPositions_9() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____pageAnchorPositions_9)); }
	inline Vector3U5BU5D_t1718750761* get__pageAnchorPositions_9() const { return ____pageAnchorPositions_9; }
	inline Vector3U5BU5D_t1718750761** get_address_of__pageAnchorPositions_9() { return &____pageAnchorPositions_9; }
	inline void set__pageAnchorPositions_9(Vector3U5BU5D_t1718750761* value)
	{
		____pageAnchorPositions_9 = value;
		Il2CppCodeGenWriteBarrier((&____pageAnchorPositions_9), value);
	}

	inline static int32_t get_offset_of__lerpTarget_10() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____lerpTarget_10)); }
	inline Vector3_t3722313464  get__lerpTarget_10() const { return ____lerpTarget_10; }
	inline Vector3_t3722313464 * get_address_of__lerpTarget_10() { return &____lerpTarget_10; }
	inline void set__lerpTarget_10(Vector3_t3722313464  value)
	{
		____lerpTarget_10 = value;
	}

	inline static int32_t get_offset_of__lerp_11() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____lerp_11)); }
	inline bool get__lerp_11() const { return ____lerp_11; }
	inline bool* get_address_of__lerp_11() { return &____lerp_11; }
	inline void set__lerp_11(bool value)
	{
		____lerp_11 = value;
	}

	inline static int32_t get_offset_of__listContainerMinPosition_12() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____listContainerMinPosition_12)); }
	inline float get__listContainerMinPosition_12() const { return ____listContainerMinPosition_12; }
	inline float* get_address_of__listContainerMinPosition_12() { return &____listContainerMinPosition_12; }
	inline void set__listContainerMinPosition_12(float value)
	{
		____listContainerMinPosition_12 = value;
	}

	inline static int32_t get_offset_of__listContainerMaxPosition_13() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____listContainerMaxPosition_13)); }
	inline float get__listContainerMaxPosition_13() const { return ____listContainerMaxPosition_13; }
	inline float* get_address_of__listContainerMaxPosition_13() { return &____listContainerMaxPosition_13; }
	inline void set__listContainerMaxPosition_13(float value)
	{
		____listContainerMaxPosition_13 = value;
	}

	inline static int32_t get_offset_of__listContainerSize_14() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____listContainerSize_14)); }
	inline float get__listContainerSize_14() const { return ____listContainerSize_14; }
	inline float* get_address_of__listContainerSize_14() { return &____listContainerSize_14; }
	inline void set__listContainerSize_14(float value)
	{
		____listContainerSize_14 = value;
	}

	inline static int32_t get_offset_of__listContainerRectTransform_15() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____listContainerRectTransform_15)); }
	inline RectTransform_t3704657025 * get__listContainerRectTransform_15() const { return ____listContainerRectTransform_15; }
	inline RectTransform_t3704657025 ** get_address_of__listContainerRectTransform_15() { return &____listContainerRectTransform_15; }
	inline void set__listContainerRectTransform_15(RectTransform_t3704657025 * value)
	{
		____listContainerRectTransform_15 = value;
		Il2CppCodeGenWriteBarrier((&____listContainerRectTransform_15), value);
	}

	inline static int32_t get_offset_of__listContainerCachedSize_16() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____listContainerCachedSize_16)); }
	inline Vector2_t2156229523  get__listContainerCachedSize_16() const { return ____listContainerCachedSize_16; }
	inline Vector2_t2156229523 * get_address_of__listContainerCachedSize_16() { return &____listContainerCachedSize_16; }
	inline void set__listContainerCachedSize_16(Vector2_t2156229523  value)
	{
		____listContainerCachedSize_16 = value;
	}

	inline static int32_t get_offset_of__itemSize_17() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____itemSize_17)); }
	inline float get__itemSize_17() const { return ____itemSize_17; }
	inline float* get_address_of__itemSize_17() { return &____itemSize_17; }
	inline void set__itemSize_17(float value)
	{
		____itemSize_17 = value;
	}

	inline static int32_t get_offset_of__itemsCount_18() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____itemsCount_18)); }
	inline int32_t get__itemsCount_18() const { return ____itemsCount_18; }
	inline int32_t* get_address_of__itemsCount_18() { return &____itemsCount_18; }
	inline void set__itemsCount_18(int32_t value)
	{
		____itemsCount_18 = value;
	}

	inline static int32_t get_offset_of__startDrag_19() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____startDrag_19)); }
	inline bool get__startDrag_19() const { return ____startDrag_19; }
	inline bool* get_address_of__startDrag_19() { return &____startDrag_19; }
	inline void set__startDrag_19(bool value)
	{
		____startDrag_19 = value;
	}

	inline static int32_t get_offset_of__positionOnDragStart_20() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____positionOnDragStart_20)); }
	inline Vector3_t3722313464  get__positionOnDragStart_20() const { return ____positionOnDragStart_20; }
	inline Vector3_t3722313464 * get_address_of__positionOnDragStart_20() { return &____positionOnDragStart_20; }
	inline void set__positionOnDragStart_20(Vector3_t3722313464  value)
	{
		____positionOnDragStart_20 = value;
	}

	inline static int32_t get_offset_of__pageOnDragStart_21() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____pageOnDragStart_21)); }
	inline int32_t get__pageOnDragStart_21() const { return ____pageOnDragStart_21; }
	inline int32_t* get_address_of__pageOnDragStart_21() { return &____pageOnDragStart_21; }
	inline void set__pageOnDragStart_21(int32_t value)
	{
		____pageOnDragStart_21 = value;
	}

	inline static int32_t get_offset_of__fastSwipeTimer_22() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____fastSwipeTimer_22)); }
	inline bool get__fastSwipeTimer_22() const { return ____fastSwipeTimer_22; }
	inline bool* get_address_of__fastSwipeTimer_22() { return &____fastSwipeTimer_22; }
	inline void set__fastSwipeTimer_22(bool value)
	{
		____fastSwipeTimer_22 = value;
	}

	inline static int32_t get_offset_of__fastSwipeCounter_23() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____fastSwipeCounter_23)); }
	inline int32_t get__fastSwipeCounter_23() const { return ____fastSwipeCounter_23; }
	inline int32_t* get_address_of__fastSwipeCounter_23() { return &____fastSwipeCounter_23; }
	inline void set__fastSwipeCounter_23(int32_t value)
	{
		____fastSwipeCounter_23 = value;
	}

	inline static int32_t get_offset_of__fastSwipeTarget_24() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ____fastSwipeTarget_24)); }
	inline int32_t get__fastSwipeTarget_24() const { return ____fastSwipeTarget_24; }
	inline int32_t* get_address_of__fastSwipeTarget_24() { return &____fastSwipeTarget_24; }
	inline void set__fastSwipeTarget_24(int32_t value)
	{
		____fastSwipeTarget_24 = value;
	}

	inline static int32_t get_offset_of_NextButton_25() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ___NextButton_25)); }
	inline Button_t4055032469 * get_NextButton_25() const { return ___NextButton_25; }
	inline Button_t4055032469 ** get_address_of_NextButton_25() { return &___NextButton_25; }
	inline void set_NextButton_25(Button_t4055032469 * value)
	{
		___NextButton_25 = value;
		Il2CppCodeGenWriteBarrier((&___NextButton_25), value);
	}

	inline static int32_t get_offset_of_PrevButton_26() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ___PrevButton_26)); }
	inline Button_t4055032469 * get_PrevButton_26() const { return ___PrevButton_26; }
	inline Button_t4055032469 ** get_address_of_PrevButton_26() { return &___PrevButton_26; }
	inline void set_PrevButton_26(Button_t4055032469 * value)
	{
		___PrevButton_26 = value;
		Il2CppCodeGenWriteBarrier((&___PrevButton_26), value);
	}

	inline static int32_t get_offset_of_ItemsVisibleAtOnce_27() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ___ItemsVisibleAtOnce_27)); }
	inline int32_t get_ItemsVisibleAtOnce_27() const { return ___ItemsVisibleAtOnce_27; }
	inline int32_t* get_address_of_ItemsVisibleAtOnce_27() { return &___ItemsVisibleAtOnce_27; }
	inline void set_ItemsVisibleAtOnce_27(int32_t value)
	{
		___ItemsVisibleAtOnce_27 = value;
	}

	inline static int32_t get_offset_of_AutoLayoutItems_28() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ___AutoLayoutItems_28)); }
	inline bool get_AutoLayoutItems_28() const { return ___AutoLayoutItems_28; }
	inline bool* get_address_of_AutoLayoutItems_28() { return &___AutoLayoutItems_28; }
	inline void set_AutoLayoutItems_28(bool value)
	{
		___AutoLayoutItems_28 = value;
	}

	inline static int32_t get_offset_of_LinkScrolbarSteps_29() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ___LinkScrolbarSteps_29)); }
	inline bool get_LinkScrolbarSteps_29() const { return ___LinkScrolbarSteps_29; }
	inline bool* get_address_of_LinkScrolbarSteps_29() { return &___LinkScrolbarSteps_29; }
	inline void set_LinkScrolbarSteps_29(bool value)
	{
		___LinkScrolbarSteps_29 = value;
	}

	inline static int32_t get_offset_of_LinkScrolrectScrollSensitivity_30() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ___LinkScrolrectScrollSensitivity_30)); }
	inline bool get_LinkScrolrectScrollSensitivity_30() const { return ___LinkScrolrectScrollSensitivity_30; }
	inline bool* get_address_of_LinkScrolrectScrollSensitivity_30() { return &___LinkScrolrectScrollSensitivity_30; }
	inline void set_LinkScrolrectScrollSensitivity_30(bool value)
	{
		___LinkScrolrectScrollSensitivity_30 = value;
	}

	inline static int32_t get_offset_of_UseFastSwipe_31() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ___UseFastSwipe_31)); }
	inline bool get_UseFastSwipe_31() const { return ___UseFastSwipe_31; }
	inline bool* get_address_of_UseFastSwipe_31() { return &___UseFastSwipe_31; }
	inline void set_UseFastSwipe_31(bool value)
	{
		___UseFastSwipe_31 = value;
	}

	inline static int32_t get_offset_of_FastSwipeThreshold_32() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ___FastSwipeThreshold_32)); }
	inline int32_t get_FastSwipeThreshold_32() const { return ___FastSwipeThreshold_32; }
	inline int32_t* get_address_of_FastSwipeThreshold_32() { return &___FastSwipeThreshold_32; }
	inline void set_FastSwipeThreshold_32(int32_t value)
	{
		___FastSwipeThreshold_32 = value;
	}

	inline static int32_t get_offset_of_onPageChange_33() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ___onPageChange_33)); }
	inline PageSnapChange_t2018612193 * get_onPageChange_33() const { return ___onPageChange_33; }
	inline PageSnapChange_t2018612193 ** get_address_of_onPageChange_33() { return &___onPageChange_33; }
	inline void set_onPageChange_33(PageSnapChange_t2018612193 * value)
	{
		___onPageChange_33 = value;
		Il2CppCodeGenWriteBarrier((&___onPageChange_33), value);
	}

	inline static int32_t get_offset_of_direction_34() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ___direction_34)); }
	inline int32_t get_direction_34() const { return ___direction_34; }
	inline int32_t* get_address_of_direction_34() { return &___direction_34; }
	inline void set_direction_34(int32_t value)
	{
		___direction_34 = value;
	}

	inline static int32_t get_offset_of_dotsPosition_35() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ___dotsPosition_35)); }
	inline Transform_t3600365921 * get_dotsPosition_35() const { return ___dotsPosition_35; }
	inline Transform_t3600365921 ** get_address_of_dotsPosition_35() { return &___dotsPosition_35; }
	inline void set_dotsPosition_35(Transform_t3600365921 * value)
	{
		___dotsPosition_35 = value;
		Il2CppCodeGenWriteBarrier((&___dotsPosition_35), value);
	}

	inline static int32_t get_offset_of_fastSwipe_36() { return static_cast<int32_t>(offsetof(ScrollSnapEx_t1558793980, ___fastSwipe_36)); }
	inline bool get_fastSwipe_36() const { return ___fastSwipe_36; }
	inline bool* get_address_of_fastSwipe_36() { return &___fastSwipe_36; }
	inline void set_fastSwipe_36(bool value)
	{
		___fastSwipe_36 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLSNAPEX_T1558793980_H
#ifndef SEARCHPAGE_T3430791159_H
#define SEARCHPAGE_T3430791159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SearchPage
struct  SearchPage_t3430791159  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject SearchPage::page
	GameObject_t1113636619 * ___page_4;
	// UnityEngine.UI.InputField SearchPage::inputField
	InputField_t3762917431 * ___inputField_5;
	// UnityEngine.GameObject SearchPage::brandContent
	GameObject_t1113636619 * ___brandContent_6;
	// UnityEngine.GameObject SearchPage::categoryContent
	GameObject_t1113636619 * ___categoryContent_7;
	// System.Boolean SearchPage::searching
	bool ___searching_8;
	// System.Boolean SearchPage::getActivate
	bool ___getActivate_9;
	// UnityEngine.GameObject SearchPage::go
	GameObject_t1113636619 * ___go_10;
	// System.Single SearchPage::speed
	float ___speed_11;
	// System.Single SearchPage::startTime
	float ___startTime_12;

public:
	inline static int32_t get_offset_of_page_4() { return static_cast<int32_t>(offsetof(SearchPage_t3430791159, ___page_4)); }
	inline GameObject_t1113636619 * get_page_4() const { return ___page_4; }
	inline GameObject_t1113636619 ** get_address_of_page_4() { return &___page_4; }
	inline void set_page_4(GameObject_t1113636619 * value)
	{
		___page_4 = value;
		Il2CppCodeGenWriteBarrier((&___page_4), value);
	}

	inline static int32_t get_offset_of_inputField_5() { return static_cast<int32_t>(offsetof(SearchPage_t3430791159, ___inputField_5)); }
	inline InputField_t3762917431 * get_inputField_5() const { return ___inputField_5; }
	inline InputField_t3762917431 ** get_address_of_inputField_5() { return &___inputField_5; }
	inline void set_inputField_5(InputField_t3762917431 * value)
	{
		___inputField_5 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_5), value);
	}

	inline static int32_t get_offset_of_brandContent_6() { return static_cast<int32_t>(offsetof(SearchPage_t3430791159, ___brandContent_6)); }
	inline GameObject_t1113636619 * get_brandContent_6() const { return ___brandContent_6; }
	inline GameObject_t1113636619 ** get_address_of_brandContent_6() { return &___brandContent_6; }
	inline void set_brandContent_6(GameObject_t1113636619 * value)
	{
		___brandContent_6 = value;
		Il2CppCodeGenWriteBarrier((&___brandContent_6), value);
	}

	inline static int32_t get_offset_of_categoryContent_7() { return static_cast<int32_t>(offsetof(SearchPage_t3430791159, ___categoryContent_7)); }
	inline GameObject_t1113636619 * get_categoryContent_7() const { return ___categoryContent_7; }
	inline GameObject_t1113636619 ** get_address_of_categoryContent_7() { return &___categoryContent_7; }
	inline void set_categoryContent_7(GameObject_t1113636619 * value)
	{
		___categoryContent_7 = value;
		Il2CppCodeGenWriteBarrier((&___categoryContent_7), value);
	}

	inline static int32_t get_offset_of_searching_8() { return static_cast<int32_t>(offsetof(SearchPage_t3430791159, ___searching_8)); }
	inline bool get_searching_8() const { return ___searching_8; }
	inline bool* get_address_of_searching_8() { return &___searching_8; }
	inline void set_searching_8(bool value)
	{
		___searching_8 = value;
	}

	inline static int32_t get_offset_of_getActivate_9() { return static_cast<int32_t>(offsetof(SearchPage_t3430791159, ___getActivate_9)); }
	inline bool get_getActivate_9() const { return ___getActivate_9; }
	inline bool* get_address_of_getActivate_9() { return &___getActivate_9; }
	inline void set_getActivate_9(bool value)
	{
		___getActivate_9 = value;
	}

	inline static int32_t get_offset_of_go_10() { return static_cast<int32_t>(offsetof(SearchPage_t3430791159, ___go_10)); }
	inline GameObject_t1113636619 * get_go_10() const { return ___go_10; }
	inline GameObject_t1113636619 ** get_address_of_go_10() { return &___go_10; }
	inline void set_go_10(GameObject_t1113636619 * value)
	{
		___go_10 = value;
		Il2CppCodeGenWriteBarrier((&___go_10), value);
	}

	inline static int32_t get_offset_of_speed_11() { return static_cast<int32_t>(offsetof(SearchPage_t3430791159, ___speed_11)); }
	inline float get_speed_11() const { return ___speed_11; }
	inline float* get_address_of_speed_11() { return &___speed_11; }
	inline void set_speed_11(float value)
	{
		___speed_11 = value;
	}

	inline static int32_t get_offset_of_startTime_12() { return static_cast<int32_t>(offsetof(SearchPage_t3430791159, ___startTime_12)); }
	inline float get_startTime_12() const { return ___startTime_12; }
	inline float* get_address_of_startTime_12() { return &___startTime_12; }
	inline void set_startTime_12(float value)
	{
		___startTime_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEARCHPAGE_T3430791159_H
#ifndef SORTPAGE_T2404422720_H
#define SORTPAGE_T2404422720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SortPage
struct  SortPage_t2404422720  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject SortPage::FilterPanel
	GameObject_t1113636619 * ___FilterPanel_4;
	// UnityEngine.GameObject SortPage::SortPanel
	GameObject_t1113636619 * ___SortPanel_5;
	// UnityEngine.UI.Toggle[] SortPage::toggles1
	ToggleU5BU5D_t2531460392* ___toggles1_6;
	// UnityEngine.UI.Toggle[] SortPage::toggles2
	ToggleU5BU5D_t2531460392* ___toggles2_7;

public:
	inline static int32_t get_offset_of_FilterPanel_4() { return static_cast<int32_t>(offsetof(SortPage_t2404422720, ___FilterPanel_4)); }
	inline GameObject_t1113636619 * get_FilterPanel_4() const { return ___FilterPanel_4; }
	inline GameObject_t1113636619 ** get_address_of_FilterPanel_4() { return &___FilterPanel_4; }
	inline void set_FilterPanel_4(GameObject_t1113636619 * value)
	{
		___FilterPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterPanel_4), value);
	}

	inline static int32_t get_offset_of_SortPanel_5() { return static_cast<int32_t>(offsetof(SortPage_t2404422720, ___SortPanel_5)); }
	inline GameObject_t1113636619 * get_SortPanel_5() const { return ___SortPanel_5; }
	inline GameObject_t1113636619 ** get_address_of_SortPanel_5() { return &___SortPanel_5; }
	inline void set_SortPanel_5(GameObject_t1113636619 * value)
	{
		___SortPanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___SortPanel_5), value);
	}

	inline static int32_t get_offset_of_toggles1_6() { return static_cast<int32_t>(offsetof(SortPage_t2404422720, ___toggles1_6)); }
	inline ToggleU5BU5D_t2531460392* get_toggles1_6() const { return ___toggles1_6; }
	inline ToggleU5BU5D_t2531460392** get_address_of_toggles1_6() { return &___toggles1_6; }
	inline void set_toggles1_6(ToggleU5BU5D_t2531460392* value)
	{
		___toggles1_6 = value;
		Il2CppCodeGenWriteBarrier((&___toggles1_6), value);
	}

	inline static int32_t get_offset_of_toggles2_7() { return static_cast<int32_t>(offsetof(SortPage_t2404422720, ___toggles2_7)); }
	inline ToggleU5BU5D_t2531460392* get_toggles2_7() const { return ___toggles2_7; }
	inline ToggleU5BU5D_t2531460392** get_address_of_toggles2_7() { return &___toggles2_7; }
	inline void set_toggles2_7(ToggleU5BU5D_t2531460392* value)
	{
		___toggles2_7 = value;
		Il2CppCodeGenWriteBarrier((&___toggles2_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTPAGE_T2404422720_H
#ifndef SWIPEDETECTOR_T548578696_H
#define SWIPEDETECTOR_T548578696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SwipeDetector
struct  SwipeDetector_t548578696  : public MonoBehaviour_t3962482529
{
public:
	// GetPointer SwipeDetector::getPointer
	GetPointer_t3074287824 * ___getPointer_4;
	// ProductPage SwipeDetector::productPage
	ProductPage_t3134713323 * ___productPage_5;
	// UnityEngine.Vector2 SwipeDetector::fingerDown
	Vector2_t2156229523  ___fingerDown_6;
	// UnityEngine.Vector2 SwipeDetector::fingerUp
	Vector2_t2156229523  ___fingerUp_7;
	// System.Boolean SwipeDetector::detectSwipeOnlyAfterRelease
	bool ___detectSwipeOnlyAfterRelease_8;
	// System.Single SwipeDetector::SWIPE_THRESHOLD
	float ___SWIPE_THRESHOLD_9;
	// System.Boolean SwipeDetector::doOnce
	bool ___doOnce_10;

public:
	inline static int32_t get_offset_of_getPointer_4() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___getPointer_4)); }
	inline GetPointer_t3074287824 * get_getPointer_4() const { return ___getPointer_4; }
	inline GetPointer_t3074287824 ** get_address_of_getPointer_4() { return &___getPointer_4; }
	inline void set_getPointer_4(GetPointer_t3074287824 * value)
	{
		___getPointer_4 = value;
		Il2CppCodeGenWriteBarrier((&___getPointer_4), value);
	}

	inline static int32_t get_offset_of_productPage_5() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___productPage_5)); }
	inline ProductPage_t3134713323 * get_productPage_5() const { return ___productPage_5; }
	inline ProductPage_t3134713323 ** get_address_of_productPage_5() { return &___productPage_5; }
	inline void set_productPage_5(ProductPage_t3134713323 * value)
	{
		___productPage_5 = value;
		Il2CppCodeGenWriteBarrier((&___productPage_5), value);
	}

	inline static int32_t get_offset_of_fingerDown_6() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___fingerDown_6)); }
	inline Vector2_t2156229523  get_fingerDown_6() const { return ___fingerDown_6; }
	inline Vector2_t2156229523 * get_address_of_fingerDown_6() { return &___fingerDown_6; }
	inline void set_fingerDown_6(Vector2_t2156229523  value)
	{
		___fingerDown_6 = value;
	}

	inline static int32_t get_offset_of_fingerUp_7() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___fingerUp_7)); }
	inline Vector2_t2156229523  get_fingerUp_7() const { return ___fingerUp_7; }
	inline Vector2_t2156229523 * get_address_of_fingerUp_7() { return &___fingerUp_7; }
	inline void set_fingerUp_7(Vector2_t2156229523  value)
	{
		___fingerUp_7 = value;
	}

	inline static int32_t get_offset_of_detectSwipeOnlyAfterRelease_8() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___detectSwipeOnlyAfterRelease_8)); }
	inline bool get_detectSwipeOnlyAfterRelease_8() const { return ___detectSwipeOnlyAfterRelease_8; }
	inline bool* get_address_of_detectSwipeOnlyAfterRelease_8() { return &___detectSwipeOnlyAfterRelease_8; }
	inline void set_detectSwipeOnlyAfterRelease_8(bool value)
	{
		___detectSwipeOnlyAfterRelease_8 = value;
	}

	inline static int32_t get_offset_of_SWIPE_THRESHOLD_9() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___SWIPE_THRESHOLD_9)); }
	inline float get_SWIPE_THRESHOLD_9() const { return ___SWIPE_THRESHOLD_9; }
	inline float* get_address_of_SWIPE_THRESHOLD_9() { return &___SWIPE_THRESHOLD_9; }
	inline void set_SWIPE_THRESHOLD_9(float value)
	{
		___SWIPE_THRESHOLD_9 = value;
	}

	inline static int32_t get_offset_of_doOnce_10() { return static_cast<int32_t>(offsetof(SwipeDetector_t548578696, ___doOnce_10)); }
	inline bool get_doOnce_10() const { return ___doOnce_10; }
	inline bool* get_address_of_doOnce_10() { return &___doOnce_10; }
	inline void set_doOnce_10(bool value)
	{
		___doOnce_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEDETECTOR_T548578696_H
#ifndef UICONTROLLER_T2237998930_H
#define UICONTROLLER_T2237998930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIController
struct  UIController_t2237998930  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UIController::SplashScreen
	GameObject_t1113636619 * ___SplashScreen_4;
	// UnityEngine.GameObject UIController::StartVideo
	GameObject_t1113636619 * ___StartVideo_5;
	// UnityEngine.GameObject UIController::StartPanel
	GameObject_t1113636619 * ___StartPanel_6;
	// UnityEngine.GameObject UIController::Back1
	GameObject_t1113636619 * ___Back1_7;
	// UnityEngine.GameObject UIController::Back2
	GameObject_t1113636619 * ___Back2_8;
	// UnityEngine.GameObject UIController::Panel1
	GameObject_t1113636619 * ___Panel1_9;

public:
	inline static int32_t get_offset_of_SplashScreen_4() { return static_cast<int32_t>(offsetof(UIController_t2237998930, ___SplashScreen_4)); }
	inline GameObject_t1113636619 * get_SplashScreen_4() const { return ___SplashScreen_4; }
	inline GameObject_t1113636619 ** get_address_of_SplashScreen_4() { return &___SplashScreen_4; }
	inline void set_SplashScreen_4(GameObject_t1113636619 * value)
	{
		___SplashScreen_4 = value;
		Il2CppCodeGenWriteBarrier((&___SplashScreen_4), value);
	}

	inline static int32_t get_offset_of_StartVideo_5() { return static_cast<int32_t>(offsetof(UIController_t2237998930, ___StartVideo_5)); }
	inline GameObject_t1113636619 * get_StartVideo_5() const { return ___StartVideo_5; }
	inline GameObject_t1113636619 ** get_address_of_StartVideo_5() { return &___StartVideo_5; }
	inline void set_StartVideo_5(GameObject_t1113636619 * value)
	{
		___StartVideo_5 = value;
		Il2CppCodeGenWriteBarrier((&___StartVideo_5), value);
	}

	inline static int32_t get_offset_of_StartPanel_6() { return static_cast<int32_t>(offsetof(UIController_t2237998930, ___StartPanel_6)); }
	inline GameObject_t1113636619 * get_StartPanel_6() const { return ___StartPanel_6; }
	inline GameObject_t1113636619 ** get_address_of_StartPanel_6() { return &___StartPanel_6; }
	inline void set_StartPanel_6(GameObject_t1113636619 * value)
	{
		___StartPanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___StartPanel_6), value);
	}

	inline static int32_t get_offset_of_Back1_7() { return static_cast<int32_t>(offsetof(UIController_t2237998930, ___Back1_7)); }
	inline GameObject_t1113636619 * get_Back1_7() const { return ___Back1_7; }
	inline GameObject_t1113636619 ** get_address_of_Back1_7() { return &___Back1_7; }
	inline void set_Back1_7(GameObject_t1113636619 * value)
	{
		___Back1_7 = value;
		Il2CppCodeGenWriteBarrier((&___Back1_7), value);
	}

	inline static int32_t get_offset_of_Back2_8() { return static_cast<int32_t>(offsetof(UIController_t2237998930, ___Back2_8)); }
	inline GameObject_t1113636619 * get_Back2_8() const { return ___Back2_8; }
	inline GameObject_t1113636619 ** get_address_of_Back2_8() { return &___Back2_8; }
	inline void set_Back2_8(GameObject_t1113636619 * value)
	{
		___Back2_8 = value;
		Il2CppCodeGenWriteBarrier((&___Back2_8), value);
	}

	inline static int32_t get_offset_of_Panel1_9() { return static_cast<int32_t>(offsetof(UIController_t2237998930, ___Panel1_9)); }
	inline GameObject_t1113636619 * get_Panel1_9() const { return ___Panel1_9; }
	inline GameObject_t1113636619 ** get_address_of_Panel1_9() { return &___Panel1_9; }
	inline void set_Panel1_9(GameObject_t1113636619 * value)
	{
		___Panel1_9 = value;
		Il2CppCodeGenWriteBarrier((&___Panel1_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICONTROLLER_T2237998930_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef VUFORIAMONOBEHAVIOUR_T1150221792_H
#define VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VuforiaMonoBehaviour
struct  VuforiaMonoBehaviour_t1150221792  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifndef DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#define DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultInitializationErrorHandler
struct  DefaultInitializationErrorHandler_t3109936861  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// System.String DefaultInitializationErrorHandler::mErrorText
	String_t* ___mErrorText_4;
	// System.Boolean DefaultInitializationErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_5;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::bodyStyle
	GUIStyle_t3956901511 * ___bodyStyle_7;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::headerStyle
	GUIStyle_t3956901511 * ___headerStyle_8;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::footerStyle
	GUIStyle_t3956901511 * ___footerStyle_9;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::bodyTexture
	Texture2D_t3840446185 * ___bodyTexture_10;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::headerTexture
	Texture2D_t3840446185 * ___headerTexture_11;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::footerTexture
	Texture2D_t3840446185 * ___footerTexture_12;

public:
	inline static int32_t get_offset_of_mErrorText_4() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___mErrorText_4)); }
	inline String_t* get_mErrorText_4() const { return ___mErrorText_4; }
	inline String_t** get_address_of_mErrorText_4() { return &___mErrorText_4; }
	inline void set_mErrorText_4(String_t* value)
	{
		___mErrorText_4 = value;
		Il2CppCodeGenWriteBarrier((&___mErrorText_4), value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_5() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___mErrorOccurred_5)); }
	inline bool get_mErrorOccurred_5() const { return ___mErrorOccurred_5; }
	inline bool* get_address_of_mErrorOccurred_5() { return &___mErrorOccurred_5; }
	inline void set_mErrorOccurred_5(bool value)
	{
		___mErrorOccurred_5 = value;
	}

	inline static int32_t get_offset_of_bodyStyle_7() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___bodyStyle_7)); }
	inline GUIStyle_t3956901511 * get_bodyStyle_7() const { return ___bodyStyle_7; }
	inline GUIStyle_t3956901511 ** get_address_of_bodyStyle_7() { return &___bodyStyle_7; }
	inline void set_bodyStyle_7(GUIStyle_t3956901511 * value)
	{
		___bodyStyle_7 = value;
		Il2CppCodeGenWriteBarrier((&___bodyStyle_7), value);
	}

	inline static int32_t get_offset_of_headerStyle_8() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___headerStyle_8)); }
	inline GUIStyle_t3956901511 * get_headerStyle_8() const { return ___headerStyle_8; }
	inline GUIStyle_t3956901511 ** get_address_of_headerStyle_8() { return &___headerStyle_8; }
	inline void set_headerStyle_8(GUIStyle_t3956901511 * value)
	{
		___headerStyle_8 = value;
		Il2CppCodeGenWriteBarrier((&___headerStyle_8), value);
	}

	inline static int32_t get_offset_of_footerStyle_9() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___footerStyle_9)); }
	inline GUIStyle_t3956901511 * get_footerStyle_9() const { return ___footerStyle_9; }
	inline GUIStyle_t3956901511 ** get_address_of_footerStyle_9() { return &___footerStyle_9; }
	inline void set_footerStyle_9(GUIStyle_t3956901511 * value)
	{
		___footerStyle_9 = value;
		Il2CppCodeGenWriteBarrier((&___footerStyle_9), value);
	}

	inline static int32_t get_offset_of_bodyTexture_10() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___bodyTexture_10)); }
	inline Texture2D_t3840446185 * get_bodyTexture_10() const { return ___bodyTexture_10; }
	inline Texture2D_t3840446185 ** get_address_of_bodyTexture_10() { return &___bodyTexture_10; }
	inline void set_bodyTexture_10(Texture2D_t3840446185 * value)
	{
		___bodyTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___bodyTexture_10), value);
	}

	inline static int32_t get_offset_of_headerTexture_11() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___headerTexture_11)); }
	inline Texture2D_t3840446185 * get_headerTexture_11() const { return ___headerTexture_11; }
	inline Texture2D_t3840446185 ** get_address_of_headerTexture_11() { return &___headerTexture_11; }
	inline void set_headerTexture_11(Texture2D_t3840446185 * value)
	{
		___headerTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___headerTexture_11), value);
	}

	inline static int32_t get_offset_of_footerTexture_12() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___footerTexture_12)); }
	inline Texture2D_t3840446185 * get_footerTexture_12() const { return ___footerTexture_12; }
	inline Texture2D_t3840446185 ** get_address_of_footerTexture_12() { return &___footerTexture_12; }
	inline void set_footerTexture_12(Texture2D_t3840446185 * value)
	{
		___footerTexture_12 = value;
		Il2CppCodeGenWriteBarrier((&___footerTexture_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#ifndef ASPECTRATIOFITTER_T3312407083_H
#define ASPECTRATIOFITTER_T3312407083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.AspectRatioFitter
struct  AspectRatioFitter_t3312407083  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.AspectRatioFitter/AspectMode UnityEngine.UI.AspectRatioFitter::m_AspectMode
	int32_t ___m_AspectMode_4;
	// System.Single UnityEngine.UI.AspectRatioFitter::m_AspectRatio
	float ___m_AspectRatio_5;
	// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::m_Rect
	RectTransform_t3704657025 * ___m_Rect_6;
	// System.Boolean UnityEngine.UI.AspectRatioFitter::m_DelayedSetDirty
	bool ___m_DelayedSetDirty_7;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.AspectRatioFitter::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_8;

public:
	inline static int32_t get_offset_of_m_AspectMode_4() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3312407083, ___m_AspectMode_4)); }
	inline int32_t get_m_AspectMode_4() const { return ___m_AspectMode_4; }
	inline int32_t* get_address_of_m_AspectMode_4() { return &___m_AspectMode_4; }
	inline void set_m_AspectMode_4(int32_t value)
	{
		___m_AspectMode_4 = value;
	}

	inline static int32_t get_offset_of_m_AspectRatio_5() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3312407083, ___m_AspectRatio_5)); }
	inline float get_m_AspectRatio_5() const { return ___m_AspectRatio_5; }
	inline float* get_address_of_m_AspectRatio_5() { return &___m_AspectRatio_5; }
	inline void set_m_AspectRatio_5(float value)
	{
		___m_AspectRatio_5 = value;
	}

	inline static int32_t get_offset_of_m_Rect_6() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3312407083, ___m_Rect_6)); }
	inline RectTransform_t3704657025 * get_m_Rect_6() const { return ___m_Rect_6; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_6() { return &___m_Rect_6; }
	inline void set_m_Rect_6(RectTransform_t3704657025 * value)
	{
		___m_Rect_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_6), value);
	}

	inline static int32_t get_offset_of_m_DelayedSetDirty_7() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3312407083, ___m_DelayedSetDirty_7)); }
	inline bool get_m_DelayedSetDirty_7() const { return ___m_DelayedSetDirty_7; }
	inline bool* get_address_of_m_DelayedSetDirty_7() { return &___m_DelayedSetDirty_7; }
	inline void set_m_DelayedSetDirty_7(bool value)
	{
		___m_DelayedSetDirty_7 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_8() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3312407083, ___m_Tracker_8)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_8() const { return ___m_Tracker_8; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_8() { return &___m_Tracker_8; }
	inline void set_m_Tracker_8(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTRATIOFITTER_T3312407083_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_6)); }
	inline Material_t340375123 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t340375123 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t340375123 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_7)); }
	inline Color_t2555686324  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t2555686324 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t2555686324  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_9)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_11)); }
	inline Canvas_t3310196443 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t3310196443 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t340375123 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t340375123 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t3648964284 * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t3648964284 * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef LAYOUTGROUP_T2436138090_H
#define LAYOUTGROUP_T2436138090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t2436138090  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_t1369453676 * ___m_Padding_4;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_5;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t3704657025 * ___m_Rect_6;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_t2156229523  ___m_TotalMinSize_8;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_t2156229523  ___m_TotalPreferredSize_9;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_t2156229523  ___m_TotalFlexibleSize_10;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t881764471 * ___m_RectChildren_11;

public:
	inline static int32_t get_offset_of_m_Padding_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Padding_4)); }
	inline RectOffset_t1369453676 * get_m_Padding_4() const { return ___m_Padding_4; }
	inline RectOffset_t1369453676 ** get_address_of_m_Padding_4() { return &___m_Padding_4; }
	inline void set_m_Padding_4(RectOffset_t1369453676 * value)
	{
		___m_Padding_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_4), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_ChildAlignment_5)); }
	inline int32_t get_m_ChildAlignment_5() const { return ___m_ChildAlignment_5; }
	inline int32_t* get_address_of_m_ChildAlignment_5() { return &___m_ChildAlignment_5; }
	inline void set_m_ChildAlignment_5(int32_t value)
	{
		___m_ChildAlignment_5 = value;
	}

	inline static int32_t get_offset_of_m_Rect_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Rect_6)); }
	inline RectTransform_t3704657025 * get_m_Rect_6() const { return ___m_Rect_6; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_6() { return &___m_Rect_6; }
	inline void set_m_Rect_6(RectTransform_t3704657025 * value)
	{
		___m_Rect_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_6), value);
	}

	inline static int32_t get_offset_of_m_Tracker_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Tracker_7)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_7() const { return ___m_Tracker_7; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_7() { return &___m_Tracker_7; }
	inline void set_m_Tracker_7(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalMinSize_8)); }
	inline Vector2_t2156229523  get_m_TotalMinSize_8() const { return ___m_TotalMinSize_8; }
	inline Vector2_t2156229523 * get_address_of_m_TotalMinSize_8() { return &___m_TotalMinSize_8; }
	inline void set_m_TotalMinSize_8(Vector2_t2156229523  value)
	{
		___m_TotalMinSize_8 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalPreferredSize_9)); }
	inline Vector2_t2156229523  get_m_TotalPreferredSize_9() const { return ___m_TotalPreferredSize_9; }
	inline Vector2_t2156229523 * get_address_of_m_TotalPreferredSize_9() { return &___m_TotalPreferredSize_9; }
	inline void set_m_TotalPreferredSize_9(Vector2_t2156229523  value)
	{
		___m_TotalPreferredSize_9 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_10() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalFlexibleSize_10)); }
	inline Vector2_t2156229523  get_m_TotalFlexibleSize_10() const { return ___m_TotalFlexibleSize_10; }
	inline Vector2_t2156229523 * get_address_of_m_TotalFlexibleSize_10() { return &___m_TotalFlexibleSize_10; }
	inline void set_m_TotalFlexibleSize_10(Vector2_t2156229523  value)
	{
		___m_TotalFlexibleSize_10 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_11() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_RectChildren_11)); }
	inline List_1_t881764471 * get_m_RectChildren_11() const { return ___m_RectChildren_11; }
	inline List_1_t881764471 ** get_address_of_m_RectChildren_11() { return &___m_RectChildren_11; }
	inline void set_m_RectChildren_11(List_1_t881764471 * value)
	{
		___m_RectChildren_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T2436138090_H
#ifndef MASK_T1803652131_H
#define MASK_T1803652131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Mask
struct  Mask_t1803652131  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Mask::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_4;
	// System.Boolean UnityEngine.UI.Mask::m_ShowMaskGraphic
	bool ___m_ShowMaskGraphic_5;
	// UnityEngine.UI.Graphic UnityEngine.UI.Mask::m_Graphic
	Graphic_t1660335611 * ___m_Graphic_6;
	// UnityEngine.Material UnityEngine.UI.Mask::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_7;
	// UnityEngine.Material UnityEngine.UI.Mask::m_UnmaskMaterial
	Material_t340375123 * ___m_UnmaskMaterial_8;

public:
	inline static int32_t get_offset_of_m_RectTransform_4() { return static_cast<int32_t>(offsetof(Mask_t1803652131, ___m_RectTransform_4)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_4() const { return ___m_RectTransform_4; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_4() { return &___m_RectTransform_4; }
	inline void set_m_RectTransform_4(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_4), value);
	}

	inline static int32_t get_offset_of_m_ShowMaskGraphic_5() { return static_cast<int32_t>(offsetof(Mask_t1803652131, ___m_ShowMaskGraphic_5)); }
	inline bool get_m_ShowMaskGraphic_5() const { return ___m_ShowMaskGraphic_5; }
	inline bool* get_address_of_m_ShowMaskGraphic_5() { return &___m_ShowMaskGraphic_5; }
	inline void set_m_ShowMaskGraphic_5(bool value)
	{
		___m_ShowMaskGraphic_5 = value;
	}

	inline static int32_t get_offset_of_m_Graphic_6() { return static_cast<int32_t>(offsetof(Mask_t1803652131, ___m_Graphic_6)); }
	inline Graphic_t1660335611 * get_m_Graphic_6() const { return ___m_Graphic_6; }
	inline Graphic_t1660335611 ** get_address_of_m_Graphic_6() { return &___m_Graphic_6; }
	inline void set_m_Graphic_6(Graphic_t1660335611 * value)
	{
		___m_Graphic_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_6), value);
	}

	inline static int32_t get_offset_of_m_MaskMaterial_7() { return static_cast<int32_t>(offsetof(Mask_t1803652131, ___m_MaskMaterial_7)); }
	inline Material_t340375123 * get_m_MaskMaterial_7() const { return ___m_MaskMaterial_7; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_7() { return &___m_MaskMaterial_7; }
	inline void set_m_MaskMaterial_7(Material_t340375123 * value)
	{
		___m_MaskMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_7), value);
	}

	inline static int32_t get_offset_of_m_UnmaskMaterial_8() { return static_cast<int32_t>(offsetof(Mask_t1803652131, ___m_UnmaskMaterial_8)); }
	inline Material_t340375123 * get_m_UnmaskMaterial_8() const { return ___m_UnmaskMaterial_8; }
	inline Material_t340375123 ** get_address_of_m_UnmaskMaterial_8() { return &___m_UnmaskMaterial_8; }
	inline void set_m_UnmaskMaterial_8(Material_t340375123 * value)
	{
		___m_UnmaskMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_UnmaskMaterial_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASK_T1803652131_H
#ifndef SCROLLRECT_T4137855814_H
#define SCROLLRECT_T4137855814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect
struct  ScrollRect_t4137855814  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Content
	RectTransform_t3704657025 * ___m_Content_4;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Horizontal
	bool ___m_Horizontal_5;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Vertical
	bool ___m_Vertical_6;
	// UnityEngine.UI.ScrollRect/MovementType UnityEngine.UI.ScrollRect::m_MovementType
	int32_t ___m_MovementType_7;
	// System.Single UnityEngine.UI.ScrollRect::m_Elasticity
	float ___m_Elasticity_8;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Inertia
	bool ___m_Inertia_9;
	// System.Single UnityEngine.UI.ScrollRect::m_DecelerationRate
	float ___m_DecelerationRate_10;
	// System.Single UnityEngine.UI.ScrollRect::m_ScrollSensitivity
	float ___m_ScrollSensitivity_11;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Viewport
	RectTransform_t3704657025 * ___m_Viewport_12;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_HorizontalScrollbar
	Scrollbar_t1494447233 * ___m_HorizontalScrollbar_13;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_VerticalScrollbar
	Scrollbar_t1494447233 * ___m_VerticalScrollbar_14;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_HorizontalScrollbarVisibility
	int32_t ___m_HorizontalScrollbarVisibility_15;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_VerticalScrollbarVisibility
	int32_t ___m_VerticalScrollbarVisibility_16;
	// System.Single UnityEngine.UI.ScrollRect::m_HorizontalScrollbarSpacing
	float ___m_HorizontalScrollbarSpacing_17;
	// System.Single UnityEngine.UI.ScrollRect::m_VerticalScrollbarSpacing
	float ___m_VerticalScrollbarSpacing_18;
	// UnityEngine.UI.ScrollRect/ScrollRectEvent UnityEngine.UI.ScrollRect::m_OnValueChanged
	ScrollRectEvent_t343079324 * ___m_OnValueChanged_19;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PointerStartLocalCursor
	Vector2_t2156229523  ___m_PointerStartLocalCursor_20;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_ContentStartPosition
	Vector2_t2156229523  ___m_ContentStartPosition_21;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_ViewRect
	RectTransform_t3704657025 * ___m_ViewRect_22;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ContentBounds
	Bounds_t2266837910  ___m_ContentBounds_23;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ViewBounds
	Bounds_t2266837910  ___m_ViewBounds_24;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_Velocity
	Vector2_t2156229523  ___m_Velocity_25;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Dragging
	bool ___m_Dragging_26;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PrevPosition
	Vector2_t2156229523  ___m_PrevPosition_27;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevContentBounds
	Bounds_t2266837910  ___m_PrevContentBounds_28;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevViewBounds
	Bounds_t2266837910  ___m_PrevViewBounds_29;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HasRebuiltLayout
	bool ___m_HasRebuiltLayout_30;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HSliderExpand
	bool ___m_HSliderExpand_31;
	// System.Boolean UnityEngine.UI.ScrollRect::m_VSliderExpand
	bool ___m_VSliderExpand_32;
	// System.Single UnityEngine.UI.ScrollRect::m_HSliderHeight
	float ___m_HSliderHeight_33;
	// System.Single UnityEngine.UI.ScrollRect::m_VSliderWidth
	float ___m_VSliderWidth_34;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Rect
	RectTransform_t3704657025 * ___m_Rect_35;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_HorizontalScrollbarRect
	RectTransform_t3704657025 * ___m_HorizontalScrollbarRect_36;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_VerticalScrollbarRect
	RectTransform_t3704657025 * ___m_VerticalScrollbarRect_37;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ScrollRect::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_38;
	// UnityEngine.Vector3[] UnityEngine.UI.ScrollRect::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_39;

public:
	inline static int32_t get_offset_of_m_Content_4() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Content_4)); }
	inline RectTransform_t3704657025 * get_m_Content_4() const { return ___m_Content_4; }
	inline RectTransform_t3704657025 ** get_address_of_m_Content_4() { return &___m_Content_4; }
	inline void set_m_Content_4(RectTransform_t3704657025 * value)
	{
		___m_Content_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_4), value);
	}

	inline static int32_t get_offset_of_m_Horizontal_5() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Horizontal_5)); }
	inline bool get_m_Horizontal_5() const { return ___m_Horizontal_5; }
	inline bool* get_address_of_m_Horizontal_5() { return &___m_Horizontal_5; }
	inline void set_m_Horizontal_5(bool value)
	{
		___m_Horizontal_5 = value;
	}

	inline static int32_t get_offset_of_m_Vertical_6() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Vertical_6)); }
	inline bool get_m_Vertical_6() const { return ___m_Vertical_6; }
	inline bool* get_address_of_m_Vertical_6() { return &___m_Vertical_6; }
	inline void set_m_Vertical_6(bool value)
	{
		___m_Vertical_6 = value;
	}

	inline static int32_t get_offset_of_m_MovementType_7() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_MovementType_7)); }
	inline int32_t get_m_MovementType_7() const { return ___m_MovementType_7; }
	inline int32_t* get_address_of_m_MovementType_7() { return &___m_MovementType_7; }
	inline void set_m_MovementType_7(int32_t value)
	{
		___m_MovementType_7 = value;
	}

	inline static int32_t get_offset_of_m_Elasticity_8() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Elasticity_8)); }
	inline float get_m_Elasticity_8() const { return ___m_Elasticity_8; }
	inline float* get_address_of_m_Elasticity_8() { return &___m_Elasticity_8; }
	inline void set_m_Elasticity_8(float value)
	{
		___m_Elasticity_8 = value;
	}

	inline static int32_t get_offset_of_m_Inertia_9() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Inertia_9)); }
	inline bool get_m_Inertia_9() const { return ___m_Inertia_9; }
	inline bool* get_address_of_m_Inertia_9() { return &___m_Inertia_9; }
	inline void set_m_Inertia_9(bool value)
	{
		___m_Inertia_9 = value;
	}

	inline static int32_t get_offset_of_m_DecelerationRate_10() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_DecelerationRate_10)); }
	inline float get_m_DecelerationRate_10() const { return ___m_DecelerationRate_10; }
	inline float* get_address_of_m_DecelerationRate_10() { return &___m_DecelerationRate_10; }
	inline void set_m_DecelerationRate_10(float value)
	{
		___m_DecelerationRate_10 = value;
	}

	inline static int32_t get_offset_of_m_ScrollSensitivity_11() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ScrollSensitivity_11)); }
	inline float get_m_ScrollSensitivity_11() const { return ___m_ScrollSensitivity_11; }
	inline float* get_address_of_m_ScrollSensitivity_11() { return &___m_ScrollSensitivity_11; }
	inline void set_m_ScrollSensitivity_11(float value)
	{
		___m_ScrollSensitivity_11 = value;
	}

	inline static int32_t get_offset_of_m_Viewport_12() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Viewport_12)); }
	inline RectTransform_t3704657025 * get_m_Viewport_12() const { return ___m_Viewport_12; }
	inline RectTransform_t3704657025 ** get_address_of_m_Viewport_12() { return &___m_Viewport_12; }
	inline void set_m_Viewport_12(RectTransform_t3704657025 * value)
	{
		___m_Viewport_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Viewport_12), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbar_13() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HorizontalScrollbar_13)); }
	inline Scrollbar_t1494447233 * get_m_HorizontalScrollbar_13() const { return ___m_HorizontalScrollbar_13; }
	inline Scrollbar_t1494447233 ** get_address_of_m_HorizontalScrollbar_13() { return &___m_HorizontalScrollbar_13; }
	inline void set_m_HorizontalScrollbar_13(Scrollbar_t1494447233 * value)
	{
		___m_HorizontalScrollbar_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalScrollbar_13), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbar_14() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VerticalScrollbar_14)); }
	inline Scrollbar_t1494447233 * get_m_VerticalScrollbar_14() const { return ___m_VerticalScrollbar_14; }
	inline Scrollbar_t1494447233 ** get_address_of_m_VerticalScrollbar_14() { return &___m_VerticalScrollbar_14; }
	inline void set_m_VerticalScrollbar_14(Scrollbar_t1494447233 * value)
	{
		___m_VerticalScrollbar_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbar_14), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarVisibility_15() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HorizontalScrollbarVisibility_15)); }
	inline int32_t get_m_HorizontalScrollbarVisibility_15() const { return ___m_HorizontalScrollbarVisibility_15; }
	inline int32_t* get_address_of_m_HorizontalScrollbarVisibility_15() { return &___m_HorizontalScrollbarVisibility_15; }
	inline void set_m_HorizontalScrollbarVisibility_15(int32_t value)
	{
		___m_HorizontalScrollbarVisibility_15 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarVisibility_16() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VerticalScrollbarVisibility_16)); }
	inline int32_t get_m_VerticalScrollbarVisibility_16() const { return ___m_VerticalScrollbarVisibility_16; }
	inline int32_t* get_address_of_m_VerticalScrollbarVisibility_16() { return &___m_VerticalScrollbarVisibility_16; }
	inline void set_m_VerticalScrollbarVisibility_16(int32_t value)
	{
		___m_VerticalScrollbarVisibility_16 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarSpacing_17() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HorizontalScrollbarSpacing_17)); }
	inline float get_m_HorizontalScrollbarSpacing_17() const { return ___m_HorizontalScrollbarSpacing_17; }
	inline float* get_address_of_m_HorizontalScrollbarSpacing_17() { return &___m_HorizontalScrollbarSpacing_17; }
	inline void set_m_HorizontalScrollbarSpacing_17(float value)
	{
		___m_HorizontalScrollbarSpacing_17 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarSpacing_18() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VerticalScrollbarSpacing_18)); }
	inline float get_m_VerticalScrollbarSpacing_18() const { return ___m_VerticalScrollbarSpacing_18; }
	inline float* get_address_of_m_VerticalScrollbarSpacing_18() { return &___m_VerticalScrollbarSpacing_18; }
	inline void set_m_VerticalScrollbarSpacing_18(float value)
	{
		___m_VerticalScrollbarSpacing_18 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_19() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_OnValueChanged_19)); }
	inline ScrollRectEvent_t343079324 * get_m_OnValueChanged_19() const { return ___m_OnValueChanged_19; }
	inline ScrollRectEvent_t343079324 ** get_address_of_m_OnValueChanged_19() { return &___m_OnValueChanged_19; }
	inline void set_m_OnValueChanged_19(ScrollRectEvent_t343079324 * value)
	{
		___m_OnValueChanged_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_19), value);
	}

	inline static int32_t get_offset_of_m_PointerStartLocalCursor_20() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_PointerStartLocalCursor_20)); }
	inline Vector2_t2156229523  get_m_PointerStartLocalCursor_20() const { return ___m_PointerStartLocalCursor_20; }
	inline Vector2_t2156229523 * get_address_of_m_PointerStartLocalCursor_20() { return &___m_PointerStartLocalCursor_20; }
	inline void set_m_PointerStartLocalCursor_20(Vector2_t2156229523  value)
	{
		___m_PointerStartLocalCursor_20 = value;
	}

	inline static int32_t get_offset_of_m_ContentStartPosition_21() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ContentStartPosition_21)); }
	inline Vector2_t2156229523  get_m_ContentStartPosition_21() const { return ___m_ContentStartPosition_21; }
	inline Vector2_t2156229523 * get_address_of_m_ContentStartPosition_21() { return &___m_ContentStartPosition_21; }
	inline void set_m_ContentStartPosition_21(Vector2_t2156229523  value)
	{
		___m_ContentStartPosition_21 = value;
	}

	inline static int32_t get_offset_of_m_ViewRect_22() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ViewRect_22)); }
	inline RectTransform_t3704657025 * get_m_ViewRect_22() const { return ___m_ViewRect_22; }
	inline RectTransform_t3704657025 ** get_address_of_m_ViewRect_22() { return &___m_ViewRect_22; }
	inline void set_m_ViewRect_22(RectTransform_t3704657025 * value)
	{
		___m_ViewRect_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ViewRect_22), value);
	}

	inline static int32_t get_offset_of_m_ContentBounds_23() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ContentBounds_23)); }
	inline Bounds_t2266837910  get_m_ContentBounds_23() const { return ___m_ContentBounds_23; }
	inline Bounds_t2266837910 * get_address_of_m_ContentBounds_23() { return &___m_ContentBounds_23; }
	inline void set_m_ContentBounds_23(Bounds_t2266837910  value)
	{
		___m_ContentBounds_23 = value;
	}

	inline static int32_t get_offset_of_m_ViewBounds_24() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ViewBounds_24)); }
	inline Bounds_t2266837910  get_m_ViewBounds_24() const { return ___m_ViewBounds_24; }
	inline Bounds_t2266837910 * get_address_of_m_ViewBounds_24() { return &___m_ViewBounds_24; }
	inline void set_m_ViewBounds_24(Bounds_t2266837910  value)
	{
		___m_ViewBounds_24 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_25() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Velocity_25)); }
	inline Vector2_t2156229523  get_m_Velocity_25() const { return ___m_Velocity_25; }
	inline Vector2_t2156229523 * get_address_of_m_Velocity_25() { return &___m_Velocity_25; }
	inline void set_m_Velocity_25(Vector2_t2156229523  value)
	{
		___m_Velocity_25 = value;
	}

	inline static int32_t get_offset_of_m_Dragging_26() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Dragging_26)); }
	inline bool get_m_Dragging_26() const { return ___m_Dragging_26; }
	inline bool* get_address_of_m_Dragging_26() { return &___m_Dragging_26; }
	inline void set_m_Dragging_26(bool value)
	{
		___m_Dragging_26 = value;
	}

	inline static int32_t get_offset_of_m_PrevPosition_27() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_PrevPosition_27)); }
	inline Vector2_t2156229523  get_m_PrevPosition_27() const { return ___m_PrevPosition_27; }
	inline Vector2_t2156229523 * get_address_of_m_PrevPosition_27() { return &___m_PrevPosition_27; }
	inline void set_m_PrevPosition_27(Vector2_t2156229523  value)
	{
		___m_PrevPosition_27 = value;
	}

	inline static int32_t get_offset_of_m_PrevContentBounds_28() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_PrevContentBounds_28)); }
	inline Bounds_t2266837910  get_m_PrevContentBounds_28() const { return ___m_PrevContentBounds_28; }
	inline Bounds_t2266837910 * get_address_of_m_PrevContentBounds_28() { return &___m_PrevContentBounds_28; }
	inline void set_m_PrevContentBounds_28(Bounds_t2266837910  value)
	{
		___m_PrevContentBounds_28 = value;
	}

	inline static int32_t get_offset_of_m_PrevViewBounds_29() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_PrevViewBounds_29)); }
	inline Bounds_t2266837910  get_m_PrevViewBounds_29() const { return ___m_PrevViewBounds_29; }
	inline Bounds_t2266837910 * get_address_of_m_PrevViewBounds_29() { return &___m_PrevViewBounds_29; }
	inline void set_m_PrevViewBounds_29(Bounds_t2266837910  value)
	{
		___m_PrevViewBounds_29 = value;
	}

	inline static int32_t get_offset_of_m_HasRebuiltLayout_30() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HasRebuiltLayout_30)); }
	inline bool get_m_HasRebuiltLayout_30() const { return ___m_HasRebuiltLayout_30; }
	inline bool* get_address_of_m_HasRebuiltLayout_30() { return &___m_HasRebuiltLayout_30; }
	inline void set_m_HasRebuiltLayout_30(bool value)
	{
		___m_HasRebuiltLayout_30 = value;
	}

	inline static int32_t get_offset_of_m_HSliderExpand_31() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HSliderExpand_31)); }
	inline bool get_m_HSliderExpand_31() const { return ___m_HSliderExpand_31; }
	inline bool* get_address_of_m_HSliderExpand_31() { return &___m_HSliderExpand_31; }
	inline void set_m_HSliderExpand_31(bool value)
	{
		___m_HSliderExpand_31 = value;
	}

	inline static int32_t get_offset_of_m_VSliderExpand_32() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VSliderExpand_32)); }
	inline bool get_m_VSliderExpand_32() const { return ___m_VSliderExpand_32; }
	inline bool* get_address_of_m_VSliderExpand_32() { return &___m_VSliderExpand_32; }
	inline void set_m_VSliderExpand_32(bool value)
	{
		___m_VSliderExpand_32 = value;
	}

	inline static int32_t get_offset_of_m_HSliderHeight_33() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HSliderHeight_33)); }
	inline float get_m_HSliderHeight_33() const { return ___m_HSliderHeight_33; }
	inline float* get_address_of_m_HSliderHeight_33() { return &___m_HSliderHeight_33; }
	inline void set_m_HSliderHeight_33(float value)
	{
		___m_HSliderHeight_33 = value;
	}

	inline static int32_t get_offset_of_m_VSliderWidth_34() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VSliderWidth_34)); }
	inline float get_m_VSliderWidth_34() const { return ___m_VSliderWidth_34; }
	inline float* get_address_of_m_VSliderWidth_34() { return &___m_VSliderWidth_34; }
	inline void set_m_VSliderWidth_34(float value)
	{
		___m_VSliderWidth_34 = value;
	}

	inline static int32_t get_offset_of_m_Rect_35() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Rect_35)); }
	inline RectTransform_t3704657025 * get_m_Rect_35() const { return ___m_Rect_35; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_35() { return &___m_Rect_35; }
	inline void set_m_Rect_35(RectTransform_t3704657025 * value)
	{
		___m_Rect_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_35), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarRect_36() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HorizontalScrollbarRect_36)); }
	inline RectTransform_t3704657025 * get_m_HorizontalScrollbarRect_36() const { return ___m_HorizontalScrollbarRect_36; }
	inline RectTransform_t3704657025 ** get_address_of_m_HorizontalScrollbarRect_36() { return &___m_HorizontalScrollbarRect_36; }
	inline void set_m_HorizontalScrollbarRect_36(RectTransform_t3704657025 * value)
	{
		___m_HorizontalScrollbarRect_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalScrollbarRect_36), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarRect_37() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VerticalScrollbarRect_37)); }
	inline RectTransform_t3704657025 * get_m_VerticalScrollbarRect_37() const { return ___m_VerticalScrollbarRect_37; }
	inline RectTransform_t3704657025 ** get_address_of_m_VerticalScrollbarRect_37() { return &___m_VerticalScrollbarRect_37; }
	inline void set_m_VerticalScrollbarRect_37(RectTransform_t3704657025 * value)
	{
		___m_VerticalScrollbarRect_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbarRect_37), value);
	}

	inline static int32_t get_offset_of_m_Tracker_38() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Tracker_38)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_38() const { return ___m_Tracker_38; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_38() { return &___m_Tracker_38; }
	inline void set_m_Tracker_38(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_38 = value;
	}

	inline static int32_t get_offset_of_m_Corners_39() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Corners_39)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_39() const { return ___m_Corners_39; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_39() { return &___m_Corners_39; }
	inline void set_m_Corners_39(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLRECT_T4137855814_H
#ifndef ASPECTRATIOFITTER_MOD001_T3563600902_H
#define ASPECTRATIOFITTER_MOD001_T3563600902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.UnityUIMods.AspectRatioFitter_Mod001
struct  AspectRatioFitter_Mod001_t3563600902  : public AspectRatioFitter_t3312407083
{
public:
	// System.Boolean BMJ.UnityUIMods.AspectRatioFitter_Mod001::Horizontal
	bool ___Horizontal_9;
	// System.Boolean BMJ.UnityUIMods.AspectRatioFitter_Mod001::Vertical
	bool ___Vertical_10;

public:
	inline static int32_t get_offset_of_Horizontal_9() { return static_cast<int32_t>(offsetof(AspectRatioFitter_Mod001_t3563600902, ___Horizontal_9)); }
	inline bool get_Horizontal_9() const { return ___Horizontal_9; }
	inline bool* get_address_of_Horizontal_9() { return &___Horizontal_9; }
	inline void set_Horizontal_9(bool value)
	{
		___Horizontal_9 = value;
	}

	inline static int32_t get_offset_of_Vertical_10() { return static_cast<int32_t>(offsetof(AspectRatioFitter_Mod001_t3563600902, ___Vertical_10)); }
	inline bool get_Vertical_10() const { return ___Vertical_10; }
	inline bool* get_address_of_Vertical_10() { return &___Vertical_10; }
	inline void set_Vertical_10(bool value)
	{
		___Vertical_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTRATIOFITTER_MOD001_T3563600902_H
#ifndef ASPECTRATIOFITTER_MOD002_T1607285766_H
#define ASPECTRATIOFITTER_MOD002_T1607285766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.UnityUIMods.AspectRatioFitter_Mod002
struct  AspectRatioFitter_Mod002_t1607285766  : public AspectRatioFitter_t3312407083
{
public:
	// System.Boolean BMJ.UnityUIMods.AspectRatioFitter_Mod002::Horizontal
	bool ___Horizontal_9;
	// System.Boolean BMJ.UnityUIMods.AspectRatioFitter_Mod002::Vertical
	bool ___Vertical_10;

public:
	inline static int32_t get_offset_of_Horizontal_9() { return static_cast<int32_t>(offsetof(AspectRatioFitter_Mod002_t1607285766, ___Horizontal_9)); }
	inline bool get_Horizontal_9() const { return ___Horizontal_9; }
	inline bool* get_address_of_Horizontal_9() { return &___Horizontal_9; }
	inline void set_Horizontal_9(bool value)
	{
		___Horizontal_9 = value;
	}

	inline static int32_t get_offset_of_Vertical_10() { return static_cast<int32_t>(offsetof(AspectRatioFitter_Mod002_t1607285766, ___Vertical_10)); }
	inline bool get_Vertical_10() const { return ___Vertical_10; }
	inline bool* get_address_of_Vertical_10() { return &___Vertical_10; }
	inline void set_Vertical_10(bool value)
	{
		___Vertical_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTRATIOFITTER_MOD002_T1607285766_H
#ifndef SCROLLRECT_MOD001_T3784143312_H
#define SCROLLRECT_MOD001_T3784143312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.UnityUIMods.ScrollRect_Mod001
struct  ScrollRect_Mod001_t3784143312  : public ScrollRect_t4137855814
{
public:
	// BMJ.UnityUIMods.ScrollRect_Mod001/ScrollAlignment BMJ.UnityUIMods.ScrollRect_Mod001::SetScrollAlignment
	int32_t ___SetScrollAlignment_40;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> BMJ.UnityUIMods.ScrollRect_Mod001::Outlines
	List_1_t2585711361 * ___Outlines_41;
	// UnityEngine.Vector2 BMJ.UnityUIMods.ScrollRect_Mod001::SnapVector
	Vector2_t2156229523  ___SnapVector_42;
	// System.Boolean BMJ.UnityUIMods.ScrollRect_Mod001::IsDragging
	bool ___IsDragging_43;

public:
	inline static int32_t get_offset_of_SetScrollAlignment_40() { return static_cast<int32_t>(offsetof(ScrollRect_Mod001_t3784143312, ___SetScrollAlignment_40)); }
	inline int32_t get_SetScrollAlignment_40() const { return ___SetScrollAlignment_40; }
	inline int32_t* get_address_of_SetScrollAlignment_40() { return &___SetScrollAlignment_40; }
	inline void set_SetScrollAlignment_40(int32_t value)
	{
		___SetScrollAlignment_40 = value;
	}

	inline static int32_t get_offset_of_Outlines_41() { return static_cast<int32_t>(offsetof(ScrollRect_Mod001_t3784143312, ___Outlines_41)); }
	inline List_1_t2585711361 * get_Outlines_41() const { return ___Outlines_41; }
	inline List_1_t2585711361 ** get_address_of_Outlines_41() { return &___Outlines_41; }
	inline void set_Outlines_41(List_1_t2585711361 * value)
	{
		___Outlines_41 = value;
		Il2CppCodeGenWriteBarrier((&___Outlines_41), value);
	}

	inline static int32_t get_offset_of_SnapVector_42() { return static_cast<int32_t>(offsetof(ScrollRect_Mod001_t3784143312, ___SnapVector_42)); }
	inline Vector2_t2156229523  get_SnapVector_42() const { return ___SnapVector_42; }
	inline Vector2_t2156229523 * get_address_of_SnapVector_42() { return &___SnapVector_42; }
	inline void set_SnapVector_42(Vector2_t2156229523  value)
	{
		___SnapVector_42 = value;
	}

	inline static int32_t get_offset_of_IsDragging_43() { return static_cast<int32_t>(offsetof(ScrollRect_Mod001_t3784143312, ___IsDragging_43)); }
	inline bool get_IsDragging_43() const { return ___IsDragging_43; }
	inline bool* get_address_of_IsDragging_43() { return &___IsDragging_43; }
	inline void set_IsDragging_43(bool value)
	{
		___IsDragging_43 = value;
	}
};

struct ScrollRect_Mod001_t3784143312_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.GameObject> BMJ.UnityUIMods.ScrollRect_Mod001::<>f__am$cache0
	Predicate_1_t1938930743 * ___U3CU3Ef__amU24cache0_44;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_44() { return static_cast<int32_t>(offsetof(ScrollRect_Mod001_t3784143312_StaticFields, ___U3CU3Ef__amU24cache0_44)); }
	inline Predicate_1_t1938930743 * get_U3CU3Ef__amU24cache0_44() const { return ___U3CU3Ef__amU24cache0_44; }
	inline Predicate_1_t1938930743 ** get_address_of_U3CU3Ef__amU24cache0_44() { return &___U3CU3Ef__amU24cache0_44; }
	inline void set_U3CU3Ef__amU24cache0_44(Predicate_1_t1938930743 * value)
	{
		___U3CU3Ef__amU24cache0_44 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_44), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLRECT_MOD001_T3784143312_H
#ifndef SOFTMASK_T1817791576_H
#define SOFTMASK_T1817791576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Coffee.UIExtensions.SoftMask
struct  SoftMask_t1817791576  : public Mask_t1803652131
{
public:
	// Coffee.UIExtensions.SoftMask/DesamplingRate Coffee.UIExtensions.SoftMask::m_DesamplingRate
	int32_t ___m_DesamplingRate_11;
	// System.Single Coffee.UIExtensions.SoftMask::m_Softness
	float ___m_Softness_12;
	// System.Boolean Coffee.UIExtensions.SoftMask::m_IgnoreParent
	bool ___m_IgnoreParent_13;
	// System.Boolean Coffee.UIExtensions.SoftMask::m_PartOfParent
	bool ___m_PartOfParent_14;
	// UnityEngine.MaterialPropertyBlock Coffee.UIExtensions.SoftMask::_mpb
	MaterialPropertyBlock_t3213117958 * ____mpb_23;
	// UnityEngine.Rendering.CommandBuffer Coffee.UIExtensions.SoftMask::_cb
	CommandBuffer_t2206337031 * ____cb_24;
	// UnityEngine.Material Coffee.UIExtensions.SoftMask::_material
	Material_t340375123 * ____material_25;
	// UnityEngine.RenderTexture Coffee.UIExtensions.SoftMask::_softMaskBuffer
	RenderTexture_t2108887433 * ____softMaskBuffer_26;
	// System.Int32 Coffee.UIExtensions.SoftMask::_stencilDepth
	int32_t ____stencilDepth_27;
	// UnityEngine.Mesh Coffee.UIExtensions.SoftMask::_mesh
	Mesh_t3648964284 * ____mesh_28;
	// Coffee.UIExtensions.SoftMask Coffee.UIExtensions.SoftMask::_parent
	SoftMask_t1817791576 * ____parent_29;
	// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask> Coffee.UIExtensions.SoftMask::_children
	List_1_t3289866318 * ____children_30;
	// System.Boolean Coffee.UIExtensions.SoftMask::_hasChanged
	bool ____hasChanged_31;
	// System.Boolean Coffee.UIExtensions.SoftMask::_hasStencilStateChanged
	bool ____hasStencilStateChanged_32;

public:
	inline static int32_t get_offset_of_m_DesamplingRate_11() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ___m_DesamplingRate_11)); }
	inline int32_t get_m_DesamplingRate_11() const { return ___m_DesamplingRate_11; }
	inline int32_t* get_address_of_m_DesamplingRate_11() { return &___m_DesamplingRate_11; }
	inline void set_m_DesamplingRate_11(int32_t value)
	{
		___m_DesamplingRate_11 = value;
	}

	inline static int32_t get_offset_of_m_Softness_12() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ___m_Softness_12)); }
	inline float get_m_Softness_12() const { return ___m_Softness_12; }
	inline float* get_address_of_m_Softness_12() { return &___m_Softness_12; }
	inline void set_m_Softness_12(float value)
	{
		___m_Softness_12 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreParent_13() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ___m_IgnoreParent_13)); }
	inline bool get_m_IgnoreParent_13() const { return ___m_IgnoreParent_13; }
	inline bool* get_address_of_m_IgnoreParent_13() { return &___m_IgnoreParent_13; }
	inline void set_m_IgnoreParent_13(bool value)
	{
		___m_IgnoreParent_13 = value;
	}

	inline static int32_t get_offset_of_m_PartOfParent_14() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ___m_PartOfParent_14)); }
	inline bool get_m_PartOfParent_14() const { return ___m_PartOfParent_14; }
	inline bool* get_address_of_m_PartOfParent_14() { return &___m_PartOfParent_14; }
	inline void set_m_PartOfParent_14(bool value)
	{
		___m_PartOfParent_14 = value;
	}

	inline static int32_t get_offset_of__mpb_23() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____mpb_23)); }
	inline MaterialPropertyBlock_t3213117958 * get__mpb_23() const { return ____mpb_23; }
	inline MaterialPropertyBlock_t3213117958 ** get_address_of__mpb_23() { return &____mpb_23; }
	inline void set__mpb_23(MaterialPropertyBlock_t3213117958 * value)
	{
		____mpb_23 = value;
		Il2CppCodeGenWriteBarrier((&____mpb_23), value);
	}

	inline static int32_t get_offset_of__cb_24() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____cb_24)); }
	inline CommandBuffer_t2206337031 * get__cb_24() const { return ____cb_24; }
	inline CommandBuffer_t2206337031 ** get_address_of__cb_24() { return &____cb_24; }
	inline void set__cb_24(CommandBuffer_t2206337031 * value)
	{
		____cb_24 = value;
		Il2CppCodeGenWriteBarrier((&____cb_24), value);
	}

	inline static int32_t get_offset_of__material_25() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____material_25)); }
	inline Material_t340375123 * get__material_25() const { return ____material_25; }
	inline Material_t340375123 ** get_address_of__material_25() { return &____material_25; }
	inline void set__material_25(Material_t340375123 * value)
	{
		____material_25 = value;
		Il2CppCodeGenWriteBarrier((&____material_25), value);
	}

	inline static int32_t get_offset_of__softMaskBuffer_26() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____softMaskBuffer_26)); }
	inline RenderTexture_t2108887433 * get__softMaskBuffer_26() const { return ____softMaskBuffer_26; }
	inline RenderTexture_t2108887433 ** get_address_of__softMaskBuffer_26() { return &____softMaskBuffer_26; }
	inline void set__softMaskBuffer_26(RenderTexture_t2108887433 * value)
	{
		____softMaskBuffer_26 = value;
		Il2CppCodeGenWriteBarrier((&____softMaskBuffer_26), value);
	}

	inline static int32_t get_offset_of__stencilDepth_27() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____stencilDepth_27)); }
	inline int32_t get__stencilDepth_27() const { return ____stencilDepth_27; }
	inline int32_t* get_address_of__stencilDepth_27() { return &____stencilDepth_27; }
	inline void set__stencilDepth_27(int32_t value)
	{
		____stencilDepth_27 = value;
	}

	inline static int32_t get_offset_of__mesh_28() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____mesh_28)); }
	inline Mesh_t3648964284 * get__mesh_28() const { return ____mesh_28; }
	inline Mesh_t3648964284 ** get_address_of__mesh_28() { return &____mesh_28; }
	inline void set__mesh_28(Mesh_t3648964284 * value)
	{
		____mesh_28 = value;
		Il2CppCodeGenWriteBarrier((&____mesh_28), value);
	}

	inline static int32_t get_offset_of__parent_29() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____parent_29)); }
	inline SoftMask_t1817791576 * get__parent_29() const { return ____parent_29; }
	inline SoftMask_t1817791576 ** get_address_of__parent_29() { return &____parent_29; }
	inline void set__parent_29(SoftMask_t1817791576 * value)
	{
		____parent_29 = value;
		Il2CppCodeGenWriteBarrier((&____parent_29), value);
	}

	inline static int32_t get_offset_of__children_30() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____children_30)); }
	inline List_1_t3289866318 * get__children_30() const { return ____children_30; }
	inline List_1_t3289866318 ** get_address_of__children_30() { return &____children_30; }
	inline void set__children_30(List_1_t3289866318 * value)
	{
		____children_30 = value;
		Il2CppCodeGenWriteBarrier((&____children_30), value);
	}

	inline static int32_t get_offset_of__hasChanged_31() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____hasChanged_31)); }
	inline bool get__hasChanged_31() const { return ____hasChanged_31; }
	inline bool* get_address_of__hasChanged_31() { return &____hasChanged_31; }
	inline void set__hasChanged_31(bool value)
	{
		____hasChanged_31 = value;
	}

	inline static int32_t get_offset_of__hasStencilStateChanged_32() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576, ____hasStencilStateChanged_32)); }
	inline bool get__hasStencilStateChanged_32() const { return ____hasStencilStateChanged_32; }
	inline bool* get_address_of__hasStencilStateChanged_32() { return &____hasStencilStateChanged_32; }
	inline void set__hasStencilStateChanged_32(bool value)
	{
		____hasStencilStateChanged_32 = value;
	}
};

struct SoftMask_t1817791576_StaticFields
{
public:
	// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>[] Coffee.UIExtensions.SoftMask::s_TmpSoftMasks
	List_1U5BU5D_t521977531* ___s_TmpSoftMasks_9;
	// UnityEngine.Color[] Coffee.UIExtensions.SoftMask::s_ClearColors
	ColorU5BU5D_t941916413* ___s_ClearColors_10;
	// UnityEngine.Shader Coffee.UIExtensions.SoftMask::s_SoftMaskShader
	Shader_t4151988712 * ___s_SoftMaskShader_15;
	// UnityEngine.Texture2D Coffee.UIExtensions.SoftMask::s_ReadTexture
	Texture2D_t3840446185 * ___s_ReadTexture_16;
	// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask> Coffee.UIExtensions.SoftMask::s_ActiveSoftMasks
	List_1_t3289866318 * ___s_ActiveSoftMasks_17;
	// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask> Coffee.UIExtensions.SoftMask::s_TempRelatables
	List_1_t3289866318 * ___s_TempRelatables_18;
	// System.Int32 Coffee.UIExtensions.SoftMask::s_StencilCompId
	int32_t ___s_StencilCompId_19;
	// System.Int32 Coffee.UIExtensions.SoftMask::s_ColorMaskId
	int32_t ___s_ColorMaskId_20;
	// System.Int32 Coffee.UIExtensions.SoftMask::s_MainTexId
	int32_t ___s_MainTexId_21;
	// System.Int32 Coffee.UIExtensions.SoftMask::s_SoftnessId
	int32_t ___s_SoftnessId_22;
	// UnityEngine.Canvas/WillRenderCanvases Coffee.UIExtensions.SoftMask::<>f__mg$cache0
	WillRenderCanvases_t3309123499 * ___U3CU3Ef__mgU24cache0_33;
	// UnityEngine.Canvas/WillRenderCanvases Coffee.UIExtensions.SoftMask::<>f__mg$cache1
	WillRenderCanvases_t3309123499 * ___U3CU3Ef__mgU24cache1_34;
	// System.Predicate`1<Coffee.UIExtensions.SoftMask> Coffee.UIExtensions.SoftMask::<>f__am$cache0
	Predicate_1_t2643085700 * ___U3CU3Ef__amU24cache0_35;

public:
	inline static int32_t get_offset_of_s_TmpSoftMasks_9() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_TmpSoftMasks_9)); }
	inline List_1U5BU5D_t521977531* get_s_TmpSoftMasks_9() const { return ___s_TmpSoftMasks_9; }
	inline List_1U5BU5D_t521977531** get_address_of_s_TmpSoftMasks_9() { return &___s_TmpSoftMasks_9; }
	inline void set_s_TmpSoftMasks_9(List_1U5BU5D_t521977531* value)
	{
		___s_TmpSoftMasks_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_TmpSoftMasks_9), value);
	}

	inline static int32_t get_offset_of_s_ClearColors_10() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_ClearColors_10)); }
	inline ColorU5BU5D_t941916413* get_s_ClearColors_10() const { return ___s_ClearColors_10; }
	inline ColorU5BU5D_t941916413** get_address_of_s_ClearColors_10() { return &___s_ClearColors_10; }
	inline void set_s_ClearColors_10(ColorU5BU5D_t941916413* value)
	{
		___s_ClearColors_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_ClearColors_10), value);
	}

	inline static int32_t get_offset_of_s_SoftMaskShader_15() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_SoftMaskShader_15)); }
	inline Shader_t4151988712 * get_s_SoftMaskShader_15() const { return ___s_SoftMaskShader_15; }
	inline Shader_t4151988712 ** get_address_of_s_SoftMaskShader_15() { return &___s_SoftMaskShader_15; }
	inline void set_s_SoftMaskShader_15(Shader_t4151988712 * value)
	{
		___s_SoftMaskShader_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_SoftMaskShader_15), value);
	}

	inline static int32_t get_offset_of_s_ReadTexture_16() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_ReadTexture_16)); }
	inline Texture2D_t3840446185 * get_s_ReadTexture_16() const { return ___s_ReadTexture_16; }
	inline Texture2D_t3840446185 ** get_address_of_s_ReadTexture_16() { return &___s_ReadTexture_16; }
	inline void set_s_ReadTexture_16(Texture2D_t3840446185 * value)
	{
		___s_ReadTexture_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReadTexture_16), value);
	}

	inline static int32_t get_offset_of_s_ActiveSoftMasks_17() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_ActiveSoftMasks_17)); }
	inline List_1_t3289866318 * get_s_ActiveSoftMasks_17() const { return ___s_ActiveSoftMasks_17; }
	inline List_1_t3289866318 ** get_address_of_s_ActiveSoftMasks_17() { return &___s_ActiveSoftMasks_17; }
	inline void set_s_ActiveSoftMasks_17(List_1_t3289866318 * value)
	{
		___s_ActiveSoftMasks_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_ActiveSoftMasks_17), value);
	}

	inline static int32_t get_offset_of_s_TempRelatables_18() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_TempRelatables_18)); }
	inline List_1_t3289866318 * get_s_TempRelatables_18() const { return ___s_TempRelatables_18; }
	inline List_1_t3289866318 ** get_address_of_s_TempRelatables_18() { return &___s_TempRelatables_18; }
	inline void set_s_TempRelatables_18(List_1_t3289866318 * value)
	{
		___s_TempRelatables_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_TempRelatables_18), value);
	}

	inline static int32_t get_offset_of_s_StencilCompId_19() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_StencilCompId_19)); }
	inline int32_t get_s_StencilCompId_19() const { return ___s_StencilCompId_19; }
	inline int32_t* get_address_of_s_StencilCompId_19() { return &___s_StencilCompId_19; }
	inline void set_s_StencilCompId_19(int32_t value)
	{
		___s_StencilCompId_19 = value;
	}

	inline static int32_t get_offset_of_s_ColorMaskId_20() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_ColorMaskId_20)); }
	inline int32_t get_s_ColorMaskId_20() const { return ___s_ColorMaskId_20; }
	inline int32_t* get_address_of_s_ColorMaskId_20() { return &___s_ColorMaskId_20; }
	inline void set_s_ColorMaskId_20(int32_t value)
	{
		___s_ColorMaskId_20 = value;
	}

	inline static int32_t get_offset_of_s_MainTexId_21() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_MainTexId_21)); }
	inline int32_t get_s_MainTexId_21() const { return ___s_MainTexId_21; }
	inline int32_t* get_address_of_s_MainTexId_21() { return &___s_MainTexId_21; }
	inline void set_s_MainTexId_21(int32_t value)
	{
		___s_MainTexId_21 = value;
	}

	inline static int32_t get_offset_of_s_SoftnessId_22() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___s_SoftnessId_22)); }
	inline int32_t get_s_SoftnessId_22() const { return ___s_SoftnessId_22; }
	inline int32_t* get_address_of_s_SoftnessId_22() { return &___s_SoftnessId_22; }
	inline void set_s_SoftnessId_22(int32_t value)
	{
		___s_SoftnessId_22 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_33() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___U3CU3Ef__mgU24cache0_33)); }
	inline WillRenderCanvases_t3309123499 * get_U3CU3Ef__mgU24cache0_33() const { return ___U3CU3Ef__mgU24cache0_33; }
	inline WillRenderCanvases_t3309123499 ** get_address_of_U3CU3Ef__mgU24cache0_33() { return &___U3CU3Ef__mgU24cache0_33; }
	inline void set_U3CU3Ef__mgU24cache0_33(WillRenderCanvases_t3309123499 * value)
	{
		___U3CU3Ef__mgU24cache0_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_34() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___U3CU3Ef__mgU24cache1_34)); }
	inline WillRenderCanvases_t3309123499 * get_U3CU3Ef__mgU24cache1_34() const { return ___U3CU3Ef__mgU24cache1_34; }
	inline WillRenderCanvases_t3309123499 ** get_address_of_U3CU3Ef__mgU24cache1_34() { return &___U3CU3Ef__mgU24cache1_34; }
	inline void set_U3CU3Ef__mgU24cache1_34(WillRenderCanvases_t3309123499 * value)
	{
		___U3CU3Ef__mgU24cache1_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_35() { return static_cast<int32_t>(offsetof(SoftMask_t1817791576_StaticFields, ___U3CU3Ef__amU24cache0_35)); }
	inline Predicate_1_t2643085700 * get_U3CU3Ef__amU24cache0_35() const { return ___U3CU3Ef__amU24cache0_35; }
	inline Predicate_1_t2643085700 ** get_address_of_U3CU3Ef__amU24cache0_35() { return &___U3CU3Ef__amU24cache0_35; }
	inline void set_U3CU3Ef__amU24cache0_35(Predicate_1_t2643085700 * value)
	{
		___U3CU3Ef__amU24cache0_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOFTMASK_T1817791576_H
#ifndef SCROLLRECTEX_T4149042633_H
#define SCROLLRECTEX_T4149042633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollRectEx
struct  ScrollRectEx_t4149042633  : public ScrollRect_t4137855814
{
public:
	// System.Boolean ScrollRectEx::routeToParent
	bool ___routeToParent_40;

public:
	inline static int32_t get_offset_of_routeToParent_40() { return static_cast<int32_t>(offsetof(ScrollRectEx_t4149042633, ___routeToParent_40)); }
	inline bool get_routeToParent_40() const { return ___routeToParent_40; }
	inline bool* get_address_of_routeToParent_40() { return &___routeToParent_40; }
	inline void set_routeToParent_40(bool value)
	{
		___routeToParent_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLRECTEX_T4149042633_H
#ifndef GRIDLAYOUTGROUP_T3046220461_H
#define GRIDLAYOUTGROUP_T3046220461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup
struct  GridLayoutGroup_t3046220461  : public LayoutGroup_t2436138090
{
public:
	// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::m_StartCorner
	int32_t ___m_StartCorner_12;
	// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::m_StartAxis
	int32_t ___m_StartAxis_13;
	// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::m_CellSize
	Vector2_t2156229523  ___m_CellSize_14;
	// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::m_Spacing
	Vector2_t2156229523  ___m_Spacing_15;
	// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::m_Constraint
	int32_t ___m_Constraint_16;
	// System.Int32 UnityEngine.UI.GridLayoutGroup::m_ConstraintCount
	int32_t ___m_ConstraintCount_17;

public:
	inline static int32_t get_offset_of_m_StartCorner_12() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_StartCorner_12)); }
	inline int32_t get_m_StartCorner_12() const { return ___m_StartCorner_12; }
	inline int32_t* get_address_of_m_StartCorner_12() { return &___m_StartCorner_12; }
	inline void set_m_StartCorner_12(int32_t value)
	{
		___m_StartCorner_12 = value;
	}

	inline static int32_t get_offset_of_m_StartAxis_13() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_StartAxis_13)); }
	inline int32_t get_m_StartAxis_13() const { return ___m_StartAxis_13; }
	inline int32_t* get_address_of_m_StartAxis_13() { return &___m_StartAxis_13; }
	inline void set_m_StartAxis_13(int32_t value)
	{
		___m_StartAxis_13 = value;
	}

	inline static int32_t get_offset_of_m_CellSize_14() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_CellSize_14)); }
	inline Vector2_t2156229523  get_m_CellSize_14() const { return ___m_CellSize_14; }
	inline Vector2_t2156229523 * get_address_of_m_CellSize_14() { return &___m_CellSize_14; }
	inline void set_m_CellSize_14(Vector2_t2156229523  value)
	{
		___m_CellSize_14 = value;
	}

	inline static int32_t get_offset_of_m_Spacing_15() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_Spacing_15)); }
	inline Vector2_t2156229523  get_m_Spacing_15() const { return ___m_Spacing_15; }
	inline Vector2_t2156229523 * get_address_of_m_Spacing_15() { return &___m_Spacing_15; }
	inline void set_m_Spacing_15(Vector2_t2156229523  value)
	{
		___m_Spacing_15 = value;
	}

	inline static int32_t get_offset_of_m_Constraint_16() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_Constraint_16)); }
	inline int32_t get_m_Constraint_16() const { return ___m_Constraint_16; }
	inline int32_t* get_address_of_m_Constraint_16() { return &___m_Constraint_16; }
	inline void set_m_Constraint_16(int32_t value)
	{
		___m_Constraint_16 = value;
	}

	inline static int32_t get_offset_of_m_ConstraintCount_17() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_ConstraintCount_17)); }
	inline int32_t get_m_ConstraintCount_17() const { return ___m_ConstraintCount_17; }
	inline int32_t* get_address_of_m_ConstraintCount_17() { return &___m_ConstraintCount_17; }
	inline void set_m_ConstraintCount_17(int32_t value)
	{
		___m_ConstraintCount_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRIDLAYOUTGROUP_T3046220461_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_22)); }
	inline Material_t340375123 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_t340375123 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_23)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_29)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef FLEXIBLEGRIDLAYOUT_T617703811_H
#define FLEXIBLEGRIDLAYOUT_T617703811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.UnityUIMods.FlexibleGridLayout
struct  FlexibleGridLayout_t617703811  : public GridLayoutGroup_t3046220461
{
public:
	// UnityEngine.Vector2 BMJ.UnityUIMods.FlexibleGridLayout::TargetCellSize
	Vector2_t2156229523  ___TargetCellSize_18;

public:
	inline static int32_t get_offset_of_TargetCellSize_18() { return static_cast<int32_t>(offsetof(FlexibleGridLayout_t617703811, ___TargetCellSize_18)); }
	inline Vector2_t2156229523  get_TargetCellSize_18() const { return ___TargetCellSize_18; }
	inline Vector2_t2156229523 * get_address_of_TargetCellSize_18() { return &___TargetCellSize_18; }
	inline void set_TargetCellSize_18(Vector2_t2156229523  value)
	{
		___TargetCellSize_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLEXIBLEGRIDLAYOUT_T617703811_H
#ifndef TMP_TEXT_T2599618874_H
#define TMP_TEXT_T2599618874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text
struct  TMP_Text_t2599618874  : public MaskableGraphic_t3839221559
{
public:
	// System.String TMPro.TMP_Text::m_text
	String_t* ___m_text_30;
	// System.Boolean TMPro.TMP_Text::m_isRightToLeft
	bool ___m_isRightToLeft_31;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_fontAsset
	TMP_FontAsset_t364381626 * ___m_fontAsset_32;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_currentFontAsset
	TMP_FontAsset_t364381626 * ___m_currentFontAsset_33;
	// System.Boolean TMPro.TMP_Text::m_isSDFShader
	bool ___m_isSDFShader_34;
	// UnityEngine.Material TMPro.TMP_Text::m_sharedMaterial
	Material_t340375123 * ___m_sharedMaterial_35;
	// UnityEngine.Material TMPro.TMP_Text::m_currentMaterial
	Material_t340375123 * ___m_currentMaterial_36;
	// TMPro.MaterialReference[] TMPro.TMP_Text::m_materialReferences
	MaterialReferenceU5BU5D_t648826345* ___m_materialReferences_37;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_Text::m_materialReferenceIndexLookup
	Dictionary_2_t1839659084 * ___m_materialReferenceIndexLookup_38;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.TMP_Text::m_materialReferenceStack
	TMP_XmlTagStack_1_t1515999176  ___m_materialReferenceStack_39;
	// System.Int32 TMPro.TMP_Text::m_currentMaterialIndex
	int32_t ___m_currentMaterialIndex_40;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontSharedMaterials
	MaterialU5BU5D_t561872642* ___m_fontSharedMaterials_41;
	// UnityEngine.Material TMPro.TMP_Text::m_fontMaterial
	Material_t340375123 * ___m_fontMaterial_42;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontMaterials
	MaterialU5BU5D_t561872642* ___m_fontMaterials_43;
	// System.Boolean TMPro.TMP_Text::m_isMaterialDirty
	bool ___m_isMaterialDirty_44;
	// UnityEngine.Color32 TMPro.TMP_Text::m_fontColor32
	Color32_t2600501292  ___m_fontColor32_45;
	// UnityEngine.Color TMPro.TMP_Text::m_fontColor
	Color_t2555686324  ___m_fontColor_46;
	// UnityEngine.Color32 TMPro.TMP_Text::m_underlineColor
	Color32_t2600501292  ___m_underlineColor_48;
	// UnityEngine.Color32 TMPro.TMP_Text::m_strikethroughColor
	Color32_t2600501292  ___m_strikethroughColor_49;
	// UnityEngine.Color32 TMPro.TMP_Text::m_highlightColor
	Color32_t2600501292  ___m_highlightColor_50;
	// System.Boolean TMPro.TMP_Text::m_enableVertexGradient
	bool ___m_enableVertexGradient_51;
	// TMPro.VertexGradient TMPro.TMP_Text::m_fontColorGradient
	VertexGradient_t345148380  ___m_fontColorGradient_52;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_fontColorGradientPreset
	TMP_ColorGradient_t3678055768 * ___m_fontColorGradientPreset_53;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_spriteAsset
	TMP_SpriteAsset_t484820633 * ___m_spriteAsset_54;
	// System.Boolean TMPro.TMP_Text::m_tintAllSprites
	bool ___m_tintAllSprites_55;
	// System.Boolean TMPro.TMP_Text::m_tintSprite
	bool ___m_tintSprite_56;
	// UnityEngine.Color32 TMPro.TMP_Text::m_spriteColor
	Color32_t2600501292  ___m_spriteColor_57;
	// System.Boolean TMPro.TMP_Text::m_overrideHtmlColors
	bool ___m_overrideHtmlColors_58;
	// UnityEngine.Color32 TMPro.TMP_Text::m_faceColor
	Color32_t2600501292  ___m_faceColor_59;
	// UnityEngine.Color32 TMPro.TMP_Text::m_outlineColor
	Color32_t2600501292  ___m_outlineColor_60;
	// System.Single TMPro.TMP_Text::m_outlineWidth
	float ___m_outlineWidth_61;
	// System.Single TMPro.TMP_Text::m_fontSize
	float ___m_fontSize_62;
	// System.Single TMPro.TMP_Text::m_currentFontSize
	float ___m_currentFontSize_63;
	// System.Single TMPro.TMP_Text::m_fontSizeBase
	float ___m_fontSizeBase_64;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_sizeStack
	TMP_XmlTagStack_1_t960921318  ___m_sizeStack_65;
	// System.Int32 TMPro.TMP_Text::m_fontWeight
	int32_t ___m_fontWeight_66;
	// System.Int32 TMPro.TMP_Text::m_fontWeightInternal
	int32_t ___m_fontWeightInternal_67;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_fontWeightStack
	TMP_XmlTagStack_1_t2514600297  ___m_fontWeightStack_68;
	// System.Boolean TMPro.TMP_Text::m_enableAutoSizing
	bool ___m_enableAutoSizing_69;
	// System.Single TMPro.TMP_Text::m_maxFontSize
	float ___m_maxFontSize_70;
	// System.Single TMPro.TMP_Text::m_minFontSize
	float ___m_minFontSize_71;
	// System.Single TMPro.TMP_Text::m_fontSizeMin
	float ___m_fontSizeMin_72;
	// System.Single TMPro.TMP_Text::m_fontSizeMax
	float ___m_fontSizeMax_73;
	// TMPro.FontStyles TMPro.TMP_Text::m_fontStyle
	int32_t ___m_fontStyle_74;
	// TMPro.FontStyles TMPro.TMP_Text::m_style
	int32_t ___m_style_75;
	// TMPro.TMP_BasicXmlTagStack TMPro.TMP_Text::m_fontStyleStack
	TMP_BasicXmlTagStack_t2962628096  ___m_fontStyleStack_76;
	// System.Boolean TMPro.TMP_Text::m_isUsingBold
	bool ___m_isUsingBold_77;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_textAlignment
	int32_t ___m_textAlignment_78;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_lineJustification
	int32_t ___m_lineJustification_79;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.TMP_Text::m_lineJustificationStack
	TMP_XmlTagStack_1_t3600445780  ___m_lineJustificationStack_80;
	// UnityEngine.Vector3[] TMPro.TMP_Text::m_textContainerLocalCorners
	Vector3U5BU5D_t1718750761* ___m_textContainerLocalCorners_81;
	// System.Boolean TMPro.TMP_Text::m_isAlignmentEnumConverted
	bool ___m_isAlignmentEnumConverted_82;
	// System.Single TMPro.TMP_Text::m_characterSpacing
	float ___m_characterSpacing_83;
	// System.Single TMPro.TMP_Text::m_cSpacing
	float ___m_cSpacing_84;
	// System.Single TMPro.TMP_Text::m_monoSpacing
	float ___m_monoSpacing_85;
	// System.Single TMPro.TMP_Text::m_wordSpacing
	float ___m_wordSpacing_86;
	// System.Single TMPro.TMP_Text::m_lineSpacing
	float ___m_lineSpacing_87;
	// System.Single TMPro.TMP_Text::m_lineSpacingDelta
	float ___m_lineSpacingDelta_88;
	// System.Single TMPro.TMP_Text::m_lineHeight
	float ___m_lineHeight_89;
	// System.Single TMPro.TMP_Text::m_lineSpacingMax
	float ___m_lineSpacingMax_90;
	// System.Single TMPro.TMP_Text::m_paragraphSpacing
	float ___m_paragraphSpacing_91;
	// System.Single TMPro.TMP_Text::m_charWidthMaxAdj
	float ___m_charWidthMaxAdj_92;
	// System.Single TMPro.TMP_Text::m_charWidthAdjDelta
	float ___m_charWidthAdjDelta_93;
	// System.Boolean TMPro.TMP_Text::m_enableWordWrapping
	bool ___m_enableWordWrapping_94;
	// System.Boolean TMPro.TMP_Text::m_isCharacterWrappingEnabled
	bool ___m_isCharacterWrappingEnabled_95;
	// System.Boolean TMPro.TMP_Text::m_isNonBreakingSpace
	bool ___m_isNonBreakingSpace_96;
	// System.Boolean TMPro.TMP_Text::m_isIgnoringAlignment
	bool ___m_isIgnoringAlignment_97;
	// System.Single TMPro.TMP_Text::m_wordWrappingRatios
	float ___m_wordWrappingRatios_98;
	// TMPro.TextOverflowModes TMPro.TMP_Text::m_overflowMode
	int32_t ___m_overflowMode_99;
	// System.Int32 TMPro.TMP_Text::m_firstOverflowCharacterIndex
	int32_t ___m_firstOverflowCharacterIndex_100;
	// TMPro.TMP_Text TMPro.TMP_Text::m_linkedTextComponent
	TMP_Text_t2599618874 * ___m_linkedTextComponent_101;
	// System.Boolean TMPro.TMP_Text::m_isLinkedTextComponent
	bool ___m_isLinkedTextComponent_102;
	// System.Boolean TMPro.TMP_Text::m_isTextTruncated
	bool ___m_isTextTruncated_103;
	// System.Boolean TMPro.TMP_Text::m_enableKerning
	bool ___m_enableKerning_104;
	// System.Boolean TMPro.TMP_Text::m_enableExtraPadding
	bool ___m_enableExtraPadding_105;
	// System.Boolean TMPro.TMP_Text::checkPaddingRequired
	bool ___checkPaddingRequired_106;
	// System.Boolean TMPro.TMP_Text::m_isRichText
	bool ___m_isRichText_107;
	// System.Boolean TMPro.TMP_Text::m_parseCtrlCharacters
	bool ___m_parseCtrlCharacters_108;
	// System.Boolean TMPro.TMP_Text::m_isOverlay
	bool ___m_isOverlay_109;
	// System.Boolean TMPro.TMP_Text::m_isOrthographic
	bool ___m_isOrthographic_110;
	// System.Boolean TMPro.TMP_Text::m_isCullingEnabled
	bool ___m_isCullingEnabled_111;
	// System.Boolean TMPro.TMP_Text::m_ignoreRectMaskCulling
	bool ___m_ignoreRectMaskCulling_112;
	// System.Boolean TMPro.TMP_Text::m_ignoreCulling
	bool ___m_ignoreCulling_113;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_horizontalMapping
	int32_t ___m_horizontalMapping_114;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_verticalMapping
	int32_t ___m_verticalMapping_115;
	// System.Single TMPro.TMP_Text::m_uvLineOffset
	float ___m_uvLineOffset_116;
	// TMPro.TextRenderFlags TMPro.TMP_Text::m_renderMode
	int32_t ___m_renderMode_117;
	// TMPro.VertexSortingOrder TMPro.TMP_Text::m_geometrySortingOrder
	int32_t ___m_geometrySortingOrder_118;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacter
	int32_t ___m_firstVisibleCharacter_119;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleCharacters
	int32_t ___m_maxVisibleCharacters_120;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleWords
	int32_t ___m_maxVisibleWords_121;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleLines
	int32_t ___m_maxVisibleLines_122;
	// System.Boolean TMPro.TMP_Text::m_useMaxVisibleDescender
	bool ___m_useMaxVisibleDescender_123;
	// System.Int32 TMPro.TMP_Text::m_pageToDisplay
	int32_t ___m_pageToDisplay_124;
	// System.Boolean TMPro.TMP_Text::m_isNewPage
	bool ___m_isNewPage_125;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_margin
	Vector4_t3319028937  ___m_margin_126;
	// System.Single TMPro.TMP_Text::m_marginLeft
	float ___m_marginLeft_127;
	// System.Single TMPro.TMP_Text::m_marginRight
	float ___m_marginRight_128;
	// System.Single TMPro.TMP_Text::m_marginWidth
	float ___m_marginWidth_129;
	// System.Single TMPro.TMP_Text::m_marginHeight
	float ___m_marginHeight_130;
	// System.Single TMPro.TMP_Text::m_width
	float ___m_width_131;
	// TMPro.TMP_TextInfo TMPro.TMP_Text::m_textInfo
	TMP_TextInfo_t3598145122 * ___m_textInfo_132;
	// System.Boolean TMPro.TMP_Text::m_havePropertiesChanged
	bool ___m_havePropertiesChanged_133;
	// System.Boolean TMPro.TMP_Text::m_isUsingLegacyAnimationComponent
	bool ___m_isUsingLegacyAnimationComponent_134;
	// UnityEngine.Transform TMPro.TMP_Text::m_transform
	Transform_t3600365921 * ___m_transform_135;
	// UnityEngine.RectTransform TMPro.TMP_Text::m_rectTransform
	RectTransform_t3704657025 * ___m_rectTransform_136;
	// System.Boolean TMPro.TMP_Text::<autoSizeTextContainer>k__BackingField
	bool ___U3CautoSizeTextContainerU3Ek__BackingField_137;
	// System.Boolean TMPro.TMP_Text::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_138;
	// UnityEngine.Mesh TMPro.TMP_Text::m_mesh
	Mesh_t3648964284 * ___m_mesh_139;
	// System.Boolean TMPro.TMP_Text::m_isVolumetricText
	bool ___m_isVolumetricText_140;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_Text::m_spriteAnimator
	TMP_SpriteAnimator_t2836635477 * ___m_spriteAnimator_141;
	// System.Single TMPro.TMP_Text::m_flexibleHeight
	float ___m_flexibleHeight_142;
	// System.Single TMPro.TMP_Text::m_flexibleWidth
	float ___m_flexibleWidth_143;
	// System.Single TMPro.TMP_Text::m_minWidth
	float ___m_minWidth_144;
	// System.Single TMPro.TMP_Text::m_minHeight
	float ___m_minHeight_145;
	// System.Single TMPro.TMP_Text::m_maxWidth
	float ___m_maxWidth_146;
	// System.Single TMPro.TMP_Text::m_maxHeight
	float ___m_maxHeight_147;
	// UnityEngine.UI.LayoutElement TMPro.TMP_Text::m_LayoutElement
	LayoutElement_t1785403678 * ___m_LayoutElement_148;
	// System.Single TMPro.TMP_Text::m_preferredWidth
	float ___m_preferredWidth_149;
	// System.Single TMPro.TMP_Text::m_renderedWidth
	float ___m_renderedWidth_150;
	// System.Boolean TMPro.TMP_Text::m_isPreferredWidthDirty
	bool ___m_isPreferredWidthDirty_151;
	// System.Single TMPro.TMP_Text::m_preferredHeight
	float ___m_preferredHeight_152;
	// System.Single TMPro.TMP_Text::m_renderedHeight
	float ___m_renderedHeight_153;
	// System.Boolean TMPro.TMP_Text::m_isPreferredHeightDirty
	bool ___m_isPreferredHeightDirty_154;
	// System.Boolean TMPro.TMP_Text::m_isCalculatingPreferredValues
	bool ___m_isCalculatingPreferredValues_155;
	// System.Int32 TMPro.TMP_Text::m_recursiveCount
	int32_t ___m_recursiveCount_156;
	// System.Int32 TMPro.TMP_Text::m_layoutPriority
	int32_t ___m_layoutPriority_157;
	// System.Boolean TMPro.TMP_Text::m_isCalculateSizeRequired
	bool ___m_isCalculateSizeRequired_158;
	// System.Boolean TMPro.TMP_Text::m_isLayoutDirty
	bool ___m_isLayoutDirty_159;
	// System.Boolean TMPro.TMP_Text::m_verticesAlreadyDirty
	bool ___m_verticesAlreadyDirty_160;
	// System.Boolean TMPro.TMP_Text::m_layoutAlreadyDirty
	bool ___m_layoutAlreadyDirty_161;
	// System.Boolean TMPro.TMP_Text::m_isAwake
	bool ___m_isAwake_162;
	// System.Boolean TMPro.TMP_Text::m_isWaitingOnResourceLoad
	bool ___m_isWaitingOnResourceLoad_163;
	// System.Boolean TMPro.TMP_Text::m_isInputParsingRequired
	bool ___m_isInputParsingRequired_164;
	// TMPro.TMP_Text/TextInputSources TMPro.TMP_Text::m_inputSource
	int32_t ___m_inputSource_165;
	// System.String TMPro.TMP_Text::old_text
	String_t* ___old_text_166;
	// System.Single TMPro.TMP_Text::m_fontScale
	float ___m_fontScale_167;
	// System.Single TMPro.TMP_Text::m_fontScaleMultiplier
	float ___m_fontScaleMultiplier_168;
	// System.Char[] TMPro.TMP_Text::m_htmlTag
	CharU5BU5D_t3528271667* ___m_htmlTag_169;
	// TMPro.XML_TagAttribute[] TMPro.TMP_Text::m_xmlAttribute
	XML_TagAttributeU5BU5D_t284240280* ___m_xmlAttribute_170;
	// System.Single[] TMPro.TMP_Text::m_attributeParameterValues
	SingleU5BU5D_t1444911251* ___m_attributeParameterValues_171;
	// System.Single TMPro.TMP_Text::tag_LineIndent
	float ___tag_LineIndent_172;
	// System.Single TMPro.TMP_Text::tag_Indent
	float ___tag_Indent_173;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_indentStack
	TMP_XmlTagStack_1_t960921318  ___m_indentStack_174;
	// System.Boolean TMPro.TMP_Text::tag_NoParsing
	bool ___tag_NoParsing_175;
	// System.Boolean TMPro.TMP_Text::m_isParsingText
	bool ___m_isParsingText_176;
	// UnityEngine.Matrix4x4 TMPro.TMP_Text::m_FXMatrix
	Matrix4x4_t1817901843  ___m_FXMatrix_177;
	// System.Boolean TMPro.TMP_Text::m_isFXMatrixSet
	bool ___m_isFXMatrixSet_178;
	// System.Int32[] TMPro.TMP_Text::m_char_buffer
	Int32U5BU5D_t385246372* ___m_char_buffer_179;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_Text::m_internalCharacterInfo
	TMP_CharacterInfoU5BU5D_t1930184704* ___m_internalCharacterInfo_180;
	// System.Char[] TMPro.TMP_Text::m_input_CharArray
	CharU5BU5D_t3528271667* ___m_input_CharArray_181;
	// System.Int32 TMPro.TMP_Text::m_charArray_Length
	int32_t ___m_charArray_Length_182;
	// System.Int32 TMPro.TMP_Text::m_totalCharacterCount
	int32_t ___m_totalCharacterCount_183;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedWordWrapState
	WordWrapState_t341939652  ___m_SavedWordWrapState_184;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLineState
	WordWrapState_t341939652  ___m_SavedLineState_185;
	// System.Int32 TMPro.TMP_Text::m_characterCount
	int32_t ___m_characterCount_186;
	// System.Int32 TMPro.TMP_Text::m_firstCharacterOfLine
	int32_t ___m_firstCharacterOfLine_187;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacterOfLine
	int32_t ___m_firstVisibleCharacterOfLine_188;
	// System.Int32 TMPro.TMP_Text::m_lastCharacterOfLine
	int32_t ___m_lastCharacterOfLine_189;
	// System.Int32 TMPro.TMP_Text::m_lastVisibleCharacterOfLine
	int32_t ___m_lastVisibleCharacterOfLine_190;
	// System.Int32 TMPro.TMP_Text::m_lineNumber
	int32_t ___m_lineNumber_191;
	// System.Int32 TMPro.TMP_Text::m_lineVisibleCharacterCount
	int32_t ___m_lineVisibleCharacterCount_192;
	// System.Int32 TMPro.TMP_Text::m_pageNumber
	int32_t ___m_pageNumber_193;
	// System.Single TMPro.TMP_Text::m_maxAscender
	float ___m_maxAscender_194;
	// System.Single TMPro.TMP_Text::m_maxCapHeight
	float ___m_maxCapHeight_195;
	// System.Single TMPro.TMP_Text::m_maxDescender
	float ___m_maxDescender_196;
	// System.Single TMPro.TMP_Text::m_maxLineAscender
	float ___m_maxLineAscender_197;
	// System.Single TMPro.TMP_Text::m_maxLineDescender
	float ___m_maxLineDescender_198;
	// System.Single TMPro.TMP_Text::m_startOfLineAscender
	float ___m_startOfLineAscender_199;
	// System.Single TMPro.TMP_Text::m_lineOffset
	float ___m_lineOffset_200;
	// TMPro.Extents TMPro.TMP_Text::m_meshExtents
	Extents_t3837212874  ___m_meshExtents_201;
	// UnityEngine.Color32 TMPro.TMP_Text::m_htmlColor
	Color32_t2600501292  ___m_htmlColor_202;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_colorStack
	TMP_XmlTagStack_1_t2164155836  ___m_colorStack_203;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_underlineColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_underlineColorStack_204;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_strikethroughColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_strikethroughColorStack_205;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_highlightColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_highlightColorStack_206;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_colorGradientPreset
	TMP_ColorGradient_t3678055768 * ___m_colorGradientPreset_207;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.TMP_Text::m_colorGradientStack
	TMP_XmlTagStack_1_t3241710312  ___m_colorGradientStack_208;
	// System.Single TMPro.TMP_Text::m_tabSpacing
	float ___m_tabSpacing_209;
	// System.Single TMPro.TMP_Text::m_spacing
	float ___m_spacing_210;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_styleStack
	TMP_XmlTagStack_1_t2514600297  ___m_styleStack_211;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_actionStack
	TMP_XmlTagStack_1_t2514600297  ___m_actionStack_212;
	// System.Single TMPro.TMP_Text::m_padding
	float ___m_padding_213;
	// System.Single TMPro.TMP_Text::m_baselineOffset
	float ___m_baselineOffset_214;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_baselineOffsetStack
	TMP_XmlTagStack_1_t960921318  ___m_baselineOffsetStack_215;
	// System.Single TMPro.TMP_Text::m_xAdvance
	float ___m_xAdvance_216;
	// TMPro.TMP_TextElementType TMPro.TMP_Text::m_textElementType
	int32_t ___m_textElementType_217;
	// TMPro.TMP_TextElement TMPro.TMP_Text::m_cached_TextElement
	TMP_TextElement_t129727469 * ___m_cached_TextElement_218;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Underline_GlyphInfo
	TMP_Glyph_t581847833 * ___m_cached_Underline_GlyphInfo_219;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Ellipsis_GlyphInfo
	TMP_Glyph_t581847833 * ___m_cached_Ellipsis_GlyphInfo_220;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_defaultSpriteAsset
	TMP_SpriteAsset_t484820633 * ___m_defaultSpriteAsset_221;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_currentSpriteAsset
	TMP_SpriteAsset_t484820633 * ___m_currentSpriteAsset_222;
	// System.Int32 TMPro.TMP_Text::m_spriteCount
	int32_t ___m_spriteCount_223;
	// System.Int32 TMPro.TMP_Text::m_spriteIndex
	int32_t ___m_spriteIndex_224;
	// System.Int32 TMPro.TMP_Text::m_spriteAnimationID
	int32_t ___m_spriteAnimationID_225;
	// System.Boolean TMPro.TMP_Text::m_ignoreActiveState
	bool ___m_ignoreActiveState_226;
	// System.Single[] TMPro.TMP_Text::k_Power
	SingleU5BU5D_t1444911251* ___k_Power_227;

public:
	inline static int32_t get_offset_of_m_text_30() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_text_30)); }
	inline String_t* get_m_text_30() const { return ___m_text_30; }
	inline String_t** get_address_of_m_text_30() { return &___m_text_30; }
	inline void set_m_text_30(String_t* value)
	{
		___m_text_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_30), value);
	}

	inline static int32_t get_offset_of_m_isRightToLeft_31() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isRightToLeft_31)); }
	inline bool get_m_isRightToLeft_31() const { return ___m_isRightToLeft_31; }
	inline bool* get_address_of_m_isRightToLeft_31() { return &___m_isRightToLeft_31; }
	inline void set_m_isRightToLeft_31(bool value)
	{
		___m_isRightToLeft_31 = value;
	}

	inline static int32_t get_offset_of_m_fontAsset_32() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontAsset_32)); }
	inline TMP_FontAsset_t364381626 * get_m_fontAsset_32() const { return ___m_fontAsset_32; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_fontAsset_32() { return &___m_fontAsset_32; }
	inline void set_m_fontAsset_32(TMP_FontAsset_t364381626 * value)
	{
		___m_fontAsset_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_32), value);
	}

	inline static int32_t get_offset_of_m_currentFontAsset_33() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentFontAsset_33)); }
	inline TMP_FontAsset_t364381626 * get_m_currentFontAsset_33() const { return ___m_currentFontAsset_33; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_currentFontAsset_33() { return &___m_currentFontAsset_33; }
	inline void set_m_currentFontAsset_33(TMP_FontAsset_t364381626 * value)
	{
		___m_currentFontAsset_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentFontAsset_33), value);
	}

	inline static int32_t get_offset_of_m_isSDFShader_34() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isSDFShader_34)); }
	inline bool get_m_isSDFShader_34() const { return ___m_isSDFShader_34; }
	inline bool* get_address_of_m_isSDFShader_34() { return &___m_isSDFShader_34; }
	inline void set_m_isSDFShader_34(bool value)
	{
		___m_isSDFShader_34 = value;
	}

	inline static int32_t get_offset_of_m_sharedMaterial_35() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_sharedMaterial_35)); }
	inline Material_t340375123 * get_m_sharedMaterial_35() const { return ___m_sharedMaterial_35; }
	inline Material_t340375123 ** get_address_of_m_sharedMaterial_35() { return &___m_sharedMaterial_35; }
	inline void set_m_sharedMaterial_35(Material_t340375123 * value)
	{
		___m_sharedMaterial_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_35), value);
	}

	inline static int32_t get_offset_of_m_currentMaterial_36() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentMaterial_36)); }
	inline Material_t340375123 * get_m_currentMaterial_36() const { return ___m_currentMaterial_36; }
	inline Material_t340375123 ** get_address_of_m_currentMaterial_36() { return &___m_currentMaterial_36; }
	inline void set_m_currentMaterial_36(Material_t340375123 * value)
	{
		___m_currentMaterial_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentMaterial_36), value);
	}

	inline static int32_t get_offset_of_m_materialReferences_37() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferences_37)); }
	inline MaterialReferenceU5BU5D_t648826345* get_m_materialReferences_37() const { return ___m_materialReferences_37; }
	inline MaterialReferenceU5BU5D_t648826345** get_address_of_m_materialReferences_37() { return &___m_materialReferences_37; }
	inline void set_m_materialReferences_37(MaterialReferenceU5BU5D_t648826345* value)
	{
		___m_materialReferences_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferences_37), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceIndexLookup_38() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferenceIndexLookup_38)); }
	inline Dictionary_2_t1839659084 * get_m_materialReferenceIndexLookup_38() const { return ___m_materialReferenceIndexLookup_38; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_materialReferenceIndexLookup_38() { return &___m_materialReferenceIndexLookup_38; }
	inline void set_m_materialReferenceIndexLookup_38(Dictionary_2_t1839659084 * value)
	{
		___m_materialReferenceIndexLookup_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferenceIndexLookup_38), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceStack_39() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferenceStack_39)); }
	inline TMP_XmlTagStack_1_t1515999176  get_m_materialReferenceStack_39() const { return ___m_materialReferenceStack_39; }
	inline TMP_XmlTagStack_1_t1515999176 * get_address_of_m_materialReferenceStack_39() { return &___m_materialReferenceStack_39; }
	inline void set_m_materialReferenceStack_39(TMP_XmlTagStack_1_t1515999176  value)
	{
		___m_materialReferenceStack_39 = value;
	}

	inline static int32_t get_offset_of_m_currentMaterialIndex_40() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentMaterialIndex_40)); }
	inline int32_t get_m_currentMaterialIndex_40() const { return ___m_currentMaterialIndex_40; }
	inline int32_t* get_address_of_m_currentMaterialIndex_40() { return &___m_currentMaterialIndex_40; }
	inline void set_m_currentMaterialIndex_40(int32_t value)
	{
		___m_currentMaterialIndex_40 = value;
	}

	inline static int32_t get_offset_of_m_fontSharedMaterials_41() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSharedMaterials_41)); }
	inline MaterialU5BU5D_t561872642* get_m_fontSharedMaterials_41() const { return ___m_fontSharedMaterials_41; }
	inline MaterialU5BU5D_t561872642** get_address_of_m_fontSharedMaterials_41() { return &___m_fontSharedMaterials_41; }
	inline void set_m_fontSharedMaterials_41(MaterialU5BU5D_t561872642* value)
	{
		___m_fontSharedMaterials_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontSharedMaterials_41), value);
	}

	inline static int32_t get_offset_of_m_fontMaterial_42() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontMaterial_42)); }
	inline Material_t340375123 * get_m_fontMaterial_42() const { return ___m_fontMaterial_42; }
	inline Material_t340375123 ** get_address_of_m_fontMaterial_42() { return &___m_fontMaterial_42; }
	inline void set_m_fontMaterial_42(Material_t340375123 * value)
	{
		___m_fontMaterial_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterial_42), value);
	}

	inline static int32_t get_offset_of_m_fontMaterials_43() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontMaterials_43)); }
	inline MaterialU5BU5D_t561872642* get_m_fontMaterials_43() const { return ___m_fontMaterials_43; }
	inline MaterialU5BU5D_t561872642** get_address_of_m_fontMaterials_43() { return &___m_fontMaterials_43; }
	inline void set_m_fontMaterials_43(MaterialU5BU5D_t561872642* value)
	{
		___m_fontMaterials_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterials_43), value);
	}

	inline static int32_t get_offset_of_m_isMaterialDirty_44() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isMaterialDirty_44)); }
	inline bool get_m_isMaterialDirty_44() const { return ___m_isMaterialDirty_44; }
	inline bool* get_address_of_m_isMaterialDirty_44() { return &___m_isMaterialDirty_44; }
	inline void set_m_isMaterialDirty_44(bool value)
	{
		___m_isMaterialDirty_44 = value;
	}

	inline static int32_t get_offset_of_m_fontColor32_45() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColor32_45)); }
	inline Color32_t2600501292  get_m_fontColor32_45() const { return ___m_fontColor32_45; }
	inline Color32_t2600501292 * get_address_of_m_fontColor32_45() { return &___m_fontColor32_45; }
	inline void set_m_fontColor32_45(Color32_t2600501292  value)
	{
		___m_fontColor32_45 = value;
	}

	inline static int32_t get_offset_of_m_fontColor_46() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColor_46)); }
	inline Color_t2555686324  get_m_fontColor_46() const { return ___m_fontColor_46; }
	inline Color_t2555686324 * get_address_of_m_fontColor_46() { return &___m_fontColor_46; }
	inline void set_m_fontColor_46(Color_t2555686324  value)
	{
		___m_fontColor_46 = value;
	}

	inline static int32_t get_offset_of_m_underlineColor_48() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_underlineColor_48)); }
	inline Color32_t2600501292  get_m_underlineColor_48() const { return ___m_underlineColor_48; }
	inline Color32_t2600501292 * get_address_of_m_underlineColor_48() { return &___m_underlineColor_48; }
	inline void set_m_underlineColor_48(Color32_t2600501292  value)
	{
		___m_underlineColor_48 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColor_49() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_strikethroughColor_49)); }
	inline Color32_t2600501292  get_m_strikethroughColor_49() const { return ___m_strikethroughColor_49; }
	inline Color32_t2600501292 * get_address_of_m_strikethroughColor_49() { return &___m_strikethroughColor_49; }
	inline void set_m_strikethroughColor_49(Color32_t2600501292  value)
	{
		___m_strikethroughColor_49 = value;
	}

	inline static int32_t get_offset_of_m_highlightColor_50() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_highlightColor_50)); }
	inline Color32_t2600501292  get_m_highlightColor_50() const { return ___m_highlightColor_50; }
	inline Color32_t2600501292 * get_address_of_m_highlightColor_50() { return &___m_highlightColor_50; }
	inline void set_m_highlightColor_50(Color32_t2600501292  value)
	{
		___m_highlightColor_50 = value;
	}

	inline static int32_t get_offset_of_m_enableVertexGradient_51() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableVertexGradient_51)); }
	inline bool get_m_enableVertexGradient_51() const { return ___m_enableVertexGradient_51; }
	inline bool* get_address_of_m_enableVertexGradient_51() { return &___m_enableVertexGradient_51; }
	inline void set_m_enableVertexGradient_51(bool value)
	{
		___m_enableVertexGradient_51 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradient_52() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColorGradient_52)); }
	inline VertexGradient_t345148380  get_m_fontColorGradient_52() const { return ___m_fontColorGradient_52; }
	inline VertexGradient_t345148380 * get_address_of_m_fontColorGradient_52() { return &___m_fontColorGradient_52; }
	inline void set_m_fontColorGradient_52(VertexGradient_t345148380  value)
	{
		___m_fontColorGradient_52 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradientPreset_53() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColorGradientPreset_53)); }
	inline TMP_ColorGradient_t3678055768 * get_m_fontColorGradientPreset_53() const { return ___m_fontColorGradientPreset_53; }
	inline TMP_ColorGradient_t3678055768 ** get_address_of_m_fontColorGradientPreset_53() { return &___m_fontColorGradientPreset_53; }
	inline void set_m_fontColorGradientPreset_53(TMP_ColorGradient_t3678055768 * value)
	{
		___m_fontColorGradientPreset_53 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontColorGradientPreset_53), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_54() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAsset_54)); }
	inline TMP_SpriteAsset_t484820633 * get_m_spriteAsset_54() const { return ___m_spriteAsset_54; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_spriteAsset_54() { return &___m_spriteAsset_54; }
	inline void set_m_spriteAsset_54(TMP_SpriteAsset_t484820633 * value)
	{
		___m_spriteAsset_54 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_54), value);
	}

	inline static int32_t get_offset_of_m_tintAllSprites_55() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tintAllSprites_55)); }
	inline bool get_m_tintAllSprites_55() const { return ___m_tintAllSprites_55; }
	inline bool* get_address_of_m_tintAllSprites_55() { return &___m_tintAllSprites_55; }
	inline void set_m_tintAllSprites_55(bool value)
	{
		___m_tintAllSprites_55 = value;
	}

	inline static int32_t get_offset_of_m_tintSprite_56() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tintSprite_56)); }
	inline bool get_m_tintSprite_56() const { return ___m_tintSprite_56; }
	inline bool* get_address_of_m_tintSprite_56() { return &___m_tintSprite_56; }
	inline void set_m_tintSprite_56(bool value)
	{
		___m_tintSprite_56 = value;
	}

	inline static int32_t get_offset_of_m_spriteColor_57() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteColor_57)); }
	inline Color32_t2600501292  get_m_spriteColor_57() const { return ___m_spriteColor_57; }
	inline Color32_t2600501292 * get_address_of_m_spriteColor_57() { return &___m_spriteColor_57; }
	inline void set_m_spriteColor_57(Color32_t2600501292  value)
	{
		___m_spriteColor_57 = value;
	}

	inline static int32_t get_offset_of_m_overrideHtmlColors_58() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_overrideHtmlColors_58)); }
	inline bool get_m_overrideHtmlColors_58() const { return ___m_overrideHtmlColors_58; }
	inline bool* get_address_of_m_overrideHtmlColors_58() { return &___m_overrideHtmlColors_58; }
	inline void set_m_overrideHtmlColors_58(bool value)
	{
		___m_overrideHtmlColors_58 = value;
	}

	inline static int32_t get_offset_of_m_faceColor_59() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_faceColor_59)); }
	inline Color32_t2600501292  get_m_faceColor_59() const { return ___m_faceColor_59; }
	inline Color32_t2600501292 * get_address_of_m_faceColor_59() { return &___m_faceColor_59; }
	inline void set_m_faceColor_59(Color32_t2600501292  value)
	{
		___m_faceColor_59 = value;
	}

	inline static int32_t get_offset_of_m_outlineColor_60() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_outlineColor_60)); }
	inline Color32_t2600501292  get_m_outlineColor_60() const { return ___m_outlineColor_60; }
	inline Color32_t2600501292 * get_address_of_m_outlineColor_60() { return &___m_outlineColor_60; }
	inline void set_m_outlineColor_60(Color32_t2600501292  value)
	{
		___m_outlineColor_60 = value;
	}

	inline static int32_t get_offset_of_m_outlineWidth_61() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_outlineWidth_61)); }
	inline float get_m_outlineWidth_61() const { return ___m_outlineWidth_61; }
	inline float* get_address_of_m_outlineWidth_61() { return &___m_outlineWidth_61; }
	inline void set_m_outlineWidth_61(float value)
	{
		___m_outlineWidth_61 = value;
	}

	inline static int32_t get_offset_of_m_fontSize_62() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSize_62)); }
	inline float get_m_fontSize_62() const { return ___m_fontSize_62; }
	inline float* get_address_of_m_fontSize_62() { return &___m_fontSize_62; }
	inline void set_m_fontSize_62(float value)
	{
		___m_fontSize_62 = value;
	}

	inline static int32_t get_offset_of_m_currentFontSize_63() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentFontSize_63)); }
	inline float get_m_currentFontSize_63() const { return ___m_currentFontSize_63; }
	inline float* get_address_of_m_currentFontSize_63() { return &___m_currentFontSize_63; }
	inline void set_m_currentFontSize_63(float value)
	{
		___m_currentFontSize_63 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeBase_64() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeBase_64)); }
	inline float get_m_fontSizeBase_64() const { return ___m_fontSizeBase_64; }
	inline float* get_address_of_m_fontSizeBase_64() { return &___m_fontSizeBase_64; }
	inline void set_m_fontSizeBase_64(float value)
	{
		___m_fontSizeBase_64 = value;
	}

	inline static int32_t get_offset_of_m_sizeStack_65() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_sizeStack_65)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_sizeStack_65() const { return ___m_sizeStack_65; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_sizeStack_65() { return &___m_sizeStack_65; }
	inline void set_m_sizeStack_65(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_sizeStack_65 = value;
	}

	inline static int32_t get_offset_of_m_fontWeight_66() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeight_66)); }
	inline int32_t get_m_fontWeight_66() const { return ___m_fontWeight_66; }
	inline int32_t* get_address_of_m_fontWeight_66() { return &___m_fontWeight_66; }
	inline void set_m_fontWeight_66(int32_t value)
	{
		___m_fontWeight_66 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightInternal_67() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeightInternal_67)); }
	inline int32_t get_m_fontWeightInternal_67() const { return ___m_fontWeightInternal_67; }
	inline int32_t* get_address_of_m_fontWeightInternal_67() { return &___m_fontWeightInternal_67; }
	inline void set_m_fontWeightInternal_67(int32_t value)
	{
		___m_fontWeightInternal_67 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightStack_68() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeightStack_68)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_fontWeightStack_68() const { return ___m_fontWeightStack_68; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_fontWeightStack_68() { return &___m_fontWeightStack_68; }
	inline void set_m_fontWeightStack_68(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_fontWeightStack_68 = value;
	}

	inline static int32_t get_offset_of_m_enableAutoSizing_69() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableAutoSizing_69)); }
	inline bool get_m_enableAutoSizing_69() const { return ___m_enableAutoSizing_69; }
	inline bool* get_address_of_m_enableAutoSizing_69() { return &___m_enableAutoSizing_69; }
	inline void set_m_enableAutoSizing_69(bool value)
	{
		___m_enableAutoSizing_69 = value;
	}

	inline static int32_t get_offset_of_m_maxFontSize_70() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxFontSize_70)); }
	inline float get_m_maxFontSize_70() const { return ___m_maxFontSize_70; }
	inline float* get_address_of_m_maxFontSize_70() { return &___m_maxFontSize_70; }
	inline void set_m_maxFontSize_70(float value)
	{
		___m_maxFontSize_70 = value;
	}

	inline static int32_t get_offset_of_m_minFontSize_71() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minFontSize_71)); }
	inline float get_m_minFontSize_71() const { return ___m_minFontSize_71; }
	inline float* get_address_of_m_minFontSize_71() { return &___m_minFontSize_71; }
	inline void set_m_minFontSize_71(float value)
	{
		___m_minFontSize_71 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMin_72() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeMin_72)); }
	inline float get_m_fontSizeMin_72() const { return ___m_fontSizeMin_72; }
	inline float* get_address_of_m_fontSizeMin_72() { return &___m_fontSizeMin_72; }
	inline void set_m_fontSizeMin_72(float value)
	{
		___m_fontSizeMin_72 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMax_73() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeMax_73)); }
	inline float get_m_fontSizeMax_73() const { return ___m_fontSizeMax_73; }
	inline float* get_address_of_m_fontSizeMax_73() { return &___m_fontSizeMax_73; }
	inline void set_m_fontSizeMax_73(float value)
	{
		___m_fontSizeMax_73 = value;
	}

	inline static int32_t get_offset_of_m_fontStyle_74() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontStyle_74)); }
	inline int32_t get_m_fontStyle_74() const { return ___m_fontStyle_74; }
	inline int32_t* get_address_of_m_fontStyle_74() { return &___m_fontStyle_74; }
	inline void set_m_fontStyle_74(int32_t value)
	{
		___m_fontStyle_74 = value;
	}

	inline static int32_t get_offset_of_m_style_75() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_style_75)); }
	inline int32_t get_m_style_75() const { return ___m_style_75; }
	inline int32_t* get_address_of_m_style_75() { return &___m_style_75; }
	inline void set_m_style_75(int32_t value)
	{
		___m_style_75 = value;
	}

	inline static int32_t get_offset_of_m_fontStyleStack_76() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontStyleStack_76)); }
	inline TMP_BasicXmlTagStack_t2962628096  get_m_fontStyleStack_76() const { return ___m_fontStyleStack_76; }
	inline TMP_BasicXmlTagStack_t2962628096 * get_address_of_m_fontStyleStack_76() { return &___m_fontStyleStack_76; }
	inline void set_m_fontStyleStack_76(TMP_BasicXmlTagStack_t2962628096  value)
	{
		___m_fontStyleStack_76 = value;
	}

	inline static int32_t get_offset_of_m_isUsingBold_77() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isUsingBold_77)); }
	inline bool get_m_isUsingBold_77() const { return ___m_isUsingBold_77; }
	inline bool* get_address_of_m_isUsingBold_77() { return &___m_isUsingBold_77; }
	inline void set_m_isUsingBold_77(bool value)
	{
		___m_isUsingBold_77 = value;
	}

	inline static int32_t get_offset_of_m_textAlignment_78() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textAlignment_78)); }
	inline int32_t get_m_textAlignment_78() const { return ___m_textAlignment_78; }
	inline int32_t* get_address_of_m_textAlignment_78() { return &___m_textAlignment_78; }
	inline void set_m_textAlignment_78(int32_t value)
	{
		___m_textAlignment_78 = value;
	}

	inline static int32_t get_offset_of_m_lineJustification_79() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineJustification_79)); }
	inline int32_t get_m_lineJustification_79() const { return ___m_lineJustification_79; }
	inline int32_t* get_address_of_m_lineJustification_79() { return &___m_lineJustification_79; }
	inline void set_m_lineJustification_79(int32_t value)
	{
		___m_lineJustification_79 = value;
	}

	inline static int32_t get_offset_of_m_lineJustificationStack_80() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineJustificationStack_80)); }
	inline TMP_XmlTagStack_1_t3600445780  get_m_lineJustificationStack_80() const { return ___m_lineJustificationStack_80; }
	inline TMP_XmlTagStack_1_t3600445780 * get_address_of_m_lineJustificationStack_80() { return &___m_lineJustificationStack_80; }
	inline void set_m_lineJustificationStack_80(TMP_XmlTagStack_1_t3600445780  value)
	{
		___m_lineJustificationStack_80 = value;
	}

	inline static int32_t get_offset_of_m_textContainerLocalCorners_81() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textContainerLocalCorners_81)); }
	inline Vector3U5BU5D_t1718750761* get_m_textContainerLocalCorners_81() const { return ___m_textContainerLocalCorners_81; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_textContainerLocalCorners_81() { return &___m_textContainerLocalCorners_81; }
	inline void set_m_textContainerLocalCorners_81(Vector3U5BU5D_t1718750761* value)
	{
		___m_textContainerLocalCorners_81 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainerLocalCorners_81), value);
	}

	inline static int32_t get_offset_of_m_isAlignmentEnumConverted_82() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isAlignmentEnumConverted_82)); }
	inline bool get_m_isAlignmentEnumConverted_82() const { return ___m_isAlignmentEnumConverted_82; }
	inline bool* get_address_of_m_isAlignmentEnumConverted_82() { return &___m_isAlignmentEnumConverted_82; }
	inline void set_m_isAlignmentEnumConverted_82(bool value)
	{
		___m_isAlignmentEnumConverted_82 = value;
	}

	inline static int32_t get_offset_of_m_characterSpacing_83() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_characterSpacing_83)); }
	inline float get_m_characterSpacing_83() const { return ___m_characterSpacing_83; }
	inline float* get_address_of_m_characterSpacing_83() { return &___m_characterSpacing_83; }
	inline void set_m_characterSpacing_83(float value)
	{
		___m_characterSpacing_83 = value;
	}

	inline static int32_t get_offset_of_m_cSpacing_84() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cSpacing_84)); }
	inline float get_m_cSpacing_84() const { return ___m_cSpacing_84; }
	inline float* get_address_of_m_cSpacing_84() { return &___m_cSpacing_84; }
	inline void set_m_cSpacing_84(float value)
	{
		___m_cSpacing_84 = value;
	}

	inline static int32_t get_offset_of_m_monoSpacing_85() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_monoSpacing_85)); }
	inline float get_m_monoSpacing_85() const { return ___m_monoSpacing_85; }
	inline float* get_address_of_m_monoSpacing_85() { return &___m_monoSpacing_85; }
	inline void set_m_monoSpacing_85(float value)
	{
		___m_monoSpacing_85 = value;
	}

	inline static int32_t get_offset_of_m_wordSpacing_86() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_wordSpacing_86)); }
	inline float get_m_wordSpacing_86() const { return ___m_wordSpacing_86; }
	inline float* get_address_of_m_wordSpacing_86() { return &___m_wordSpacing_86; }
	inline void set_m_wordSpacing_86(float value)
	{
		___m_wordSpacing_86 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacing_87() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacing_87)); }
	inline float get_m_lineSpacing_87() const { return ___m_lineSpacing_87; }
	inline float* get_address_of_m_lineSpacing_87() { return &___m_lineSpacing_87; }
	inline void set_m_lineSpacing_87(float value)
	{
		___m_lineSpacing_87 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingDelta_88() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacingDelta_88)); }
	inline float get_m_lineSpacingDelta_88() const { return ___m_lineSpacingDelta_88; }
	inline float* get_address_of_m_lineSpacingDelta_88() { return &___m_lineSpacingDelta_88; }
	inline void set_m_lineSpacingDelta_88(float value)
	{
		___m_lineSpacingDelta_88 = value;
	}

	inline static int32_t get_offset_of_m_lineHeight_89() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineHeight_89)); }
	inline float get_m_lineHeight_89() const { return ___m_lineHeight_89; }
	inline float* get_address_of_m_lineHeight_89() { return &___m_lineHeight_89; }
	inline void set_m_lineHeight_89(float value)
	{
		___m_lineHeight_89 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingMax_90() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacingMax_90)); }
	inline float get_m_lineSpacingMax_90() const { return ___m_lineSpacingMax_90; }
	inline float* get_address_of_m_lineSpacingMax_90() { return &___m_lineSpacingMax_90; }
	inline void set_m_lineSpacingMax_90(float value)
	{
		___m_lineSpacingMax_90 = value;
	}

	inline static int32_t get_offset_of_m_paragraphSpacing_91() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_paragraphSpacing_91)); }
	inline float get_m_paragraphSpacing_91() const { return ___m_paragraphSpacing_91; }
	inline float* get_address_of_m_paragraphSpacing_91() { return &___m_paragraphSpacing_91; }
	inline void set_m_paragraphSpacing_91(float value)
	{
		___m_paragraphSpacing_91 = value;
	}

	inline static int32_t get_offset_of_m_charWidthMaxAdj_92() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charWidthMaxAdj_92)); }
	inline float get_m_charWidthMaxAdj_92() const { return ___m_charWidthMaxAdj_92; }
	inline float* get_address_of_m_charWidthMaxAdj_92() { return &___m_charWidthMaxAdj_92; }
	inline void set_m_charWidthMaxAdj_92(float value)
	{
		___m_charWidthMaxAdj_92 = value;
	}

	inline static int32_t get_offset_of_m_charWidthAdjDelta_93() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charWidthAdjDelta_93)); }
	inline float get_m_charWidthAdjDelta_93() const { return ___m_charWidthAdjDelta_93; }
	inline float* get_address_of_m_charWidthAdjDelta_93() { return &___m_charWidthAdjDelta_93; }
	inline void set_m_charWidthAdjDelta_93(float value)
	{
		___m_charWidthAdjDelta_93 = value;
	}

	inline static int32_t get_offset_of_m_enableWordWrapping_94() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableWordWrapping_94)); }
	inline bool get_m_enableWordWrapping_94() const { return ___m_enableWordWrapping_94; }
	inline bool* get_address_of_m_enableWordWrapping_94() { return &___m_enableWordWrapping_94; }
	inline void set_m_enableWordWrapping_94(bool value)
	{
		___m_enableWordWrapping_94 = value;
	}

	inline static int32_t get_offset_of_m_isCharacterWrappingEnabled_95() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCharacterWrappingEnabled_95)); }
	inline bool get_m_isCharacterWrappingEnabled_95() const { return ___m_isCharacterWrappingEnabled_95; }
	inline bool* get_address_of_m_isCharacterWrappingEnabled_95() { return &___m_isCharacterWrappingEnabled_95; }
	inline void set_m_isCharacterWrappingEnabled_95(bool value)
	{
		___m_isCharacterWrappingEnabled_95 = value;
	}

	inline static int32_t get_offset_of_m_isNonBreakingSpace_96() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isNonBreakingSpace_96)); }
	inline bool get_m_isNonBreakingSpace_96() const { return ___m_isNonBreakingSpace_96; }
	inline bool* get_address_of_m_isNonBreakingSpace_96() { return &___m_isNonBreakingSpace_96; }
	inline void set_m_isNonBreakingSpace_96(bool value)
	{
		___m_isNonBreakingSpace_96 = value;
	}

	inline static int32_t get_offset_of_m_isIgnoringAlignment_97() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isIgnoringAlignment_97)); }
	inline bool get_m_isIgnoringAlignment_97() const { return ___m_isIgnoringAlignment_97; }
	inline bool* get_address_of_m_isIgnoringAlignment_97() { return &___m_isIgnoringAlignment_97; }
	inline void set_m_isIgnoringAlignment_97(bool value)
	{
		___m_isIgnoringAlignment_97 = value;
	}

	inline static int32_t get_offset_of_m_wordWrappingRatios_98() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_wordWrappingRatios_98)); }
	inline float get_m_wordWrappingRatios_98() const { return ___m_wordWrappingRatios_98; }
	inline float* get_address_of_m_wordWrappingRatios_98() { return &___m_wordWrappingRatios_98; }
	inline void set_m_wordWrappingRatios_98(float value)
	{
		___m_wordWrappingRatios_98 = value;
	}

	inline static int32_t get_offset_of_m_overflowMode_99() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_overflowMode_99)); }
	inline int32_t get_m_overflowMode_99() const { return ___m_overflowMode_99; }
	inline int32_t* get_address_of_m_overflowMode_99() { return &___m_overflowMode_99; }
	inline void set_m_overflowMode_99(int32_t value)
	{
		___m_overflowMode_99 = value;
	}

	inline static int32_t get_offset_of_m_firstOverflowCharacterIndex_100() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstOverflowCharacterIndex_100)); }
	inline int32_t get_m_firstOverflowCharacterIndex_100() const { return ___m_firstOverflowCharacterIndex_100; }
	inline int32_t* get_address_of_m_firstOverflowCharacterIndex_100() { return &___m_firstOverflowCharacterIndex_100; }
	inline void set_m_firstOverflowCharacterIndex_100(int32_t value)
	{
		___m_firstOverflowCharacterIndex_100 = value;
	}

	inline static int32_t get_offset_of_m_linkedTextComponent_101() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_linkedTextComponent_101)); }
	inline TMP_Text_t2599618874 * get_m_linkedTextComponent_101() const { return ___m_linkedTextComponent_101; }
	inline TMP_Text_t2599618874 ** get_address_of_m_linkedTextComponent_101() { return &___m_linkedTextComponent_101; }
	inline void set_m_linkedTextComponent_101(TMP_Text_t2599618874 * value)
	{
		___m_linkedTextComponent_101 = value;
		Il2CppCodeGenWriteBarrier((&___m_linkedTextComponent_101), value);
	}

	inline static int32_t get_offset_of_m_isLinkedTextComponent_102() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isLinkedTextComponent_102)); }
	inline bool get_m_isLinkedTextComponent_102() const { return ___m_isLinkedTextComponent_102; }
	inline bool* get_address_of_m_isLinkedTextComponent_102() { return &___m_isLinkedTextComponent_102; }
	inline void set_m_isLinkedTextComponent_102(bool value)
	{
		___m_isLinkedTextComponent_102 = value;
	}

	inline static int32_t get_offset_of_m_isTextTruncated_103() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isTextTruncated_103)); }
	inline bool get_m_isTextTruncated_103() const { return ___m_isTextTruncated_103; }
	inline bool* get_address_of_m_isTextTruncated_103() { return &___m_isTextTruncated_103; }
	inline void set_m_isTextTruncated_103(bool value)
	{
		___m_isTextTruncated_103 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_104() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableKerning_104)); }
	inline bool get_m_enableKerning_104() const { return ___m_enableKerning_104; }
	inline bool* get_address_of_m_enableKerning_104() { return &___m_enableKerning_104; }
	inline void set_m_enableKerning_104(bool value)
	{
		___m_enableKerning_104 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_105() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableExtraPadding_105)); }
	inline bool get_m_enableExtraPadding_105() const { return ___m_enableExtraPadding_105; }
	inline bool* get_address_of_m_enableExtraPadding_105() { return &___m_enableExtraPadding_105; }
	inline void set_m_enableExtraPadding_105(bool value)
	{
		___m_enableExtraPadding_105 = value;
	}

	inline static int32_t get_offset_of_checkPaddingRequired_106() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___checkPaddingRequired_106)); }
	inline bool get_checkPaddingRequired_106() const { return ___checkPaddingRequired_106; }
	inline bool* get_address_of_checkPaddingRequired_106() { return &___checkPaddingRequired_106; }
	inline void set_checkPaddingRequired_106(bool value)
	{
		___checkPaddingRequired_106 = value;
	}

	inline static int32_t get_offset_of_m_isRichText_107() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isRichText_107)); }
	inline bool get_m_isRichText_107() const { return ___m_isRichText_107; }
	inline bool* get_address_of_m_isRichText_107() { return &___m_isRichText_107; }
	inline void set_m_isRichText_107(bool value)
	{
		___m_isRichText_107 = value;
	}

	inline static int32_t get_offset_of_m_parseCtrlCharacters_108() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_parseCtrlCharacters_108)); }
	inline bool get_m_parseCtrlCharacters_108() const { return ___m_parseCtrlCharacters_108; }
	inline bool* get_address_of_m_parseCtrlCharacters_108() { return &___m_parseCtrlCharacters_108; }
	inline void set_m_parseCtrlCharacters_108(bool value)
	{
		___m_parseCtrlCharacters_108 = value;
	}

	inline static int32_t get_offset_of_m_isOverlay_109() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isOverlay_109)); }
	inline bool get_m_isOverlay_109() const { return ___m_isOverlay_109; }
	inline bool* get_address_of_m_isOverlay_109() { return &___m_isOverlay_109; }
	inline void set_m_isOverlay_109(bool value)
	{
		___m_isOverlay_109 = value;
	}

	inline static int32_t get_offset_of_m_isOrthographic_110() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isOrthographic_110)); }
	inline bool get_m_isOrthographic_110() const { return ___m_isOrthographic_110; }
	inline bool* get_address_of_m_isOrthographic_110() { return &___m_isOrthographic_110; }
	inline void set_m_isOrthographic_110(bool value)
	{
		___m_isOrthographic_110 = value;
	}

	inline static int32_t get_offset_of_m_isCullingEnabled_111() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCullingEnabled_111)); }
	inline bool get_m_isCullingEnabled_111() const { return ___m_isCullingEnabled_111; }
	inline bool* get_address_of_m_isCullingEnabled_111() { return &___m_isCullingEnabled_111; }
	inline void set_m_isCullingEnabled_111(bool value)
	{
		___m_isCullingEnabled_111 = value;
	}

	inline static int32_t get_offset_of_m_ignoreRectMaskCulling_112() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreRectMaskCulling_112)); }
	inline bool get_m_ignoreRectMaskCulling_112() const { return ___m_ignoreRectMaskCulling_112; }
	inline bool* get_address_of_m_ignoreRectMaskCulling_112() { return &___m_ignoreRectMaskCulling_112; }
	inline void set_m_ignoreRectMaskCulling_112(bool value)
	{
		___m_ignoreRectMaskCulling_112 = value;
	}

	inline static int32_t get_offset_of_m_ignoreCulling_113() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreCulling_113)); }
	inline bool get_m_ignoreCulling_113() const { return ___m_ignoreCulling_113; }
	inline bool* get_address_of_m_ignoreCulling_113() { return &___m_ignoreCulling_113; }
	inline void set_m_ignoreCulling_113(bool value)
	{
		___m_ignoreCulling_113 = value;
	}

	inline static int32_t get_offset_of_m_horizontalMapping_114() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_horizontalMapping_114)); }
	inline int32_t get_m_horizontalMapping_114() const { return ___m_horizontalMapping_114; }
	inline int32_t* get_address_of_m_horizontalMapping_114() { return &___m_horizontalMapping_114; }
	inline void set_m_horizontalMapping_114(int32_t value)
	{
		___m_horizontalMapping_114 = value;
	}

	inline static int32_t get_offset_of_m_verticalMapping_115() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_verticalMapping_115)); }
	inline int32_t get_m_verticalMapping_115() const { return ___m_verticalMapping_115; }
	inline int32_t* get_address_of_m_verticalMapping_115() { return &___m_verticalMapping_115; }
	inline void set_m_verticalMapping_115(int32_t value)
	{
		___m_verticalMapping_115 = value;
	}

	inline static int32_t get_offset_of_m_uvLineOffset_116() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_uvLineOffset_116)); }
	inline float get_m_uvLineOffset_116() const { return ___m_uvLineOffset_116; }
	inline float* get_address_of_m_uvLineOffset_116() { return &___m_uvLineOffset_116; }
	inline void set_m_uvLineOffset_116(float value)
	{
		___m_uvLineOffset_116 = value;
	}

	inline static int32_t get_offset_of_m_renderMode_117() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderMode_117)); }
	inline int32_t get_m_renderMode_117() const { return ___m_renderMode_117; }
	inline int32_t* get_address_of_m_renderMode_117() { return &___m_renderMode_117; }
	inline void set_m_renderMode_117(int32_t value)
	{
		___m_renderMode_117 = value;
	}

	inline static int32_t get_offset_of_m_geometrySortingOrder_118() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_geometrySortingOrder_118)); }
	inline int32_t get_m_geometrySortingOrder_118() const { return ___m_geometrySortingOrder_118; }
	inline int32_t* get_address_of_m_geometrySortingOrder_118() { return &___m_geometrySortingOrder_118; }
	inline void set_m_geometrySortingOrder_118(int32_t value)
	{
		___m_geometrySortingOrder_118 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacter_119() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstVisibleCharacter_119)); }
	inline int32_t get_m_firstVisibleCharacter_119() const { return ___m_firstVisibleCharacter_119; }
	inline int32_t* get_address_of_m_firstVisibleCharacter_119() { return &___m_firstVisibleCharacter_119; }
	inline void set_m_firstVisibleCharacter_119(int32_t value)
	{
		___m_firstVisibleCharacter_119 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleCharacters_120() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleCharacters_120)); }
	inline int32_t get_m_maxVisibleCharacters_120() const { return ___m_maxVisibleCharacters_120; }
	inline int32_t* get_address_of_m_maxVisibleCharacters_120() { return &___m_maxVisibleCharacters_120; }
	inline void set_m_maxVisibleCharacters_120(int32_t value)
	{
		___m_maxVisibleCharacters_120 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleWords_121() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleWords_121)); }
	inline int32_t get_m_maxVisibleWords_121() const { return ___m_maxVisibleWords_121; }
	inline int32_t* get_address_of_m_maxVisibleWords_121() { return &___m_maxVisibleWords_121; }
	inline void set_m_maxVisibleWords_121(int32_t value)
	{
		___m_maxVisibleWords_121 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleLines_122() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleLines_122)); }
	inline int32_t get_m_maxVisibleLines_122() const { return ___m_maxVisibleLines_122; }
	inline int32_t* get_address_of_m_maxVisibleLines_122() { return &___m_maxVisibleLines_122; }
	inline void set_m_maxVisibleLines_122(int32_t value)
	{
		___m_maxVisibleLines_122 = value;
	}

	inline static int32_t get_offset_of_m_useMaxVisibleDescender_123() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_useMaxVisibleDescender_123)); }
	inline bool get_m_useMaxVisibleDescender_123() const { return ___m_useMaxVisibleDescender_123; }
	inline bool* get_address_of_m_useMaxVisibleDescender_123() { return &___m_useMaxVisibleDescender_123; }
	inline void set_m_useMaxVisibleDescender_123(bool value)
	{
		___m_useMaxVisibleDescender_123 = value;
	}

	inline static int32_t get_offset_of_m_pageToDisplay_124() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_pageToDisplay_124)); }
	inline int32_t get_m_pageToDisplay_124() const { return ___m_pageToDisplay_124; }
	inline int32_t* get_address_of_m_pageToDisplay_124() { return &___m_pageToDisplay_124; }
	inline void set_m_pageToDisplay_124(int32_t value)
	{
		___m_pageToDisplay_124 = value;
	}

	inline static int32_t get_offset_of_m_isNewPage_125() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isNewPage_125)); }
	inline bool get_m_isNewPage_125() const { return ___m_isNewPage_125; }
	inline bool* get_address_of_m_isNewPage_125() { return &___m_isNewPage_125; }
	inline void set_m_isNewPage_125(bool value)
	{
		___m_isNewPage_125 = value;
	}

	inline static int32_t get_offset_of_m_margin_126() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_margin_126)); }
	inline Vector4_t3319028937  get_m_margin_126() const { return ___m_margin_126; }
	inline Vector4_t3319028937 * get_address_of_m_margin_126() { return &___m_margin_126; }
	inline void set_m_margin_126(Vector4_t3319028937  value)
	{
		___m_margin_126 = value;
	}

	inline static int32_t get_offset_of_m_marginLeft_127() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginLeft_127)); }
	inline float get_m_marginLeft_127() const { return ___m_marginLeft_127; }
	inline float* get_address_of_m_marginLeft_127() { return &___m_marginLeft_127; }
	inline void set_m_marginLeft_127(float value)
	{
		___m_marginLeft_127 = value;
	}

	inline static int32_t get_offset_of_m_marginRight_128() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginRight_128)); }
	inline float get_m_marginRight_128() const { return ___m_marginRight_128; }
	inline float* get_address_of_m_marginRight_128() { return &___m_marginRight_128; }
	inline void set_m_marginRight_128(float value)
	{
		___m_marginRight_128 = value;
	}

	inline static int32_t get_offset_of_m_marginWidth_129() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginWidth_129)); }
	inline float get_m_marginWidth_129() const { return ___m_marginWidth_129; }
	inline float* get_address_of_m_marginWidth_129() { return &___m_marginWidth_129; }
	inline void set_m_marginWidth_129(float value)
	{
		___m_marginWidth_129 = value;
	}

	inline static int32_t get_offset_of_m_marginHeight_130() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginHeight_130)); }
	inline float get_m_marginHeight_130() const { return ___m_marginHeight_130; }
	inline float* get_address_of_m_marginHeight_130() { return &___m_marginHeight_130; }
	inline void set_m_marginHeight_130(float value)
	{
		___m_marginHeight_130 = value;
	}

	inline static int32_t get_offset_of_m_width_131() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_width_131)); }
	inline float get_m_width_131() const { return ___m_width_131; }
	inline float* get_address_of_m_width_131() { return &___m_width_131; }
	inline void set_m_width_131(float value)
	{
		___m_width_131 = value;
	}

	inline static int32_t get_offset_of_m_textInfo_132() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textInfo_132)); }
	inline TMP_TextInfo_t3598145122 * get_m_textInfo_132() const { return ___m_textInfo_132; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_m_textInfo_132() { return &___m_textInfo_132; }
	inline void set_m_textInfo_132(TMP_TextInfo_t3598145122 * value)
	{
		___m_textInfo_132 = value;
		Il2CppCodeGenWriteBarrier((&___m_textInfo_132), value);
	}

	inline static int32_t get_offset_of_m_havePropertiesChanged_133() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_havePropertiesChanged_133)); }
	inline bool get_m_havePropertiesChanged_133() const { return ___m_havePropertiesChanged_133; }
	inline bool* get_address_of_m_havePropertiesChanged_133() { return &___m_havePropertiesChanged_133; }
	inline void set_m_havePropertiesChanged_133(bool value)
	{
		___m_havePropertiesChanged_133 = value;
	}

	inline static int32_t get_offset_of_m_isUsingLegacyAnimationComponent_134() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isUsingLegacyAnimationComponent_134)); }
	inline bool get_m_isUsingLegacyAnimationComponent_134() const { return ___m_isUsingLegacyAnimationComponent_134; }
	inline bool* get_address_of_m_isUsingLegacyAnimationComponent_134() { return &___m_isUsingLegacyAnimationComponent_134; }
	inline void set_m_isUsingLegacyAnimationComponent_134(bool value)
	{
		___m_isUsingLegacyAnimationComponent_134 = value;
	}

	inline static int32_t get_offset_of_m_transform_135() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_transform_135)); }
	inline Transform_t3600365921 * get_m_transform_135() const { return ___m_transform_135; }
	inline Transform_t3600365921 ** get_address_of_m_transform_135() { return &___m_transform_135; }
	inline void set_m_transform_135(Transform_t3600365921 * value)
	{
		___m_transform_135 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_135), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_136() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_rectTransform_136)); }
	inline RectTransform_t3704657025 * get_m_rectTransform_136() const { return ___m_rectTransform_136; }
	inline RectTransform_t3704657025 ** get_address_of_m_rectTransform_136() { return &___m_rectTransform_136; }
	inline void set_m_rectTransform_136(RectTransform_t3704657025 * value)
	{
		___m_rectTransform_136 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_136), value);
	}

	inline static int32_t get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_137() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___U3CautoSizeTextContainerU3Ek__BackingField_137)); }
	inline bool get_U3CautoSizeTextContainerU3Ek__BackingField_137() const { return ___U3CautoSizeTextContainerU3Ek__BackingField_137; }
	inline bool* get_address_of_U3CautoSizeTextContainerU3Ek__BackingField_137() { return &___U3CautoSizeTextContainerU3Ek__BackingField_137; }
	inline void set_U3CautoSizeTextContainerU3Ek__BackingField_137(bool value)
	{
		___U3CautoSizeTextContainerU3Ek__BackingField_137 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_138() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_autoSizeTextContainer_138)); }
	inline bool get_m_autoSizeTextContainer_138() const { return ___m_autoSizeTextContainer_138; }
	inline bool* get_address_of_m_autoSizeTextContainer_138() { return &___m_autoSizeTextContainer_138; }
	inline void set_m_autoSizeTextContainer_138(bool value)
	{
		___m_autoSizeTextContainer_138 = value;
	}

	inline static int32_t get_offset_of_m_mesh_139() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_mesh_139)); }
	inline Mesh_t3648964284 * get_m_mesh_139() const { return ___m_mesh_139; }
	inline Mesh_t3648964284 ** get_address_of_m_mesh_139() { return &___m_mesh_139; }
	inline void set_m_mesh_139(Mesh_t3648964284 * value)
	{
		___m_mesh_139 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_139), value);
	}

	inline static int32_t get_offset_of_m_isVolumetricText_140() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isVolumetricText_140)); }
	inline bool get_m_isVolumetricText_140() const { return ___m_isVolumetricText_140; }
	inline bool* get_address_of_m_isVolumetricText_140() { return &___m_isVolumetricText_140; }
	inline void set_m_isVolumetricText_140(bool value)
	{
		___m_isVolumetricText_140 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimator_141() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAnimator_141)); }
	inline TMP_SpriteAnimator_t2836635477 * get_m_spriteAnimator_141() const { return ___m_spriteAnimator_141; }
	inline TMP_SpriteAnimator_t2836635477 ** get_address_of_m_spriteAnimator_141() { return &___m_spriteAnimator_141; }
	inline void set_m_spriteAnimator_141(TMP_SpriteAnimator_t2836635477 * value)
	{
		___m_spriteAnimator_141 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAnimator_141), value);
	}

	inline static int32_t get_offset_of_m_flexibleHeight_142() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_flexibleHeight_142)); }
	inline float get_m_flexibleHeight_142() const { return ___m_flexibleHeight_142; }
	inline float* get_address_of_m_flexibleHeight_142() { return &___m_flexibleHeight_142; }
	inline void set_m_flexibleHeight_142(float value)
	{
		___m_flexibleHeight_142 = value;
	}

	inline static int32_t get_offset_of_m_flexibleWidth_143() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_flexibleWidth_143)); }
	inline float get_m_flexibleWidth_143() const { return ___m_flexibleWidth_143; }
	inline float* get_address_of_m_flexibleWidth_143() { return &___m_flexibleWidth_143; }
	inline void set_m_flexibleWidth_143(float value)
	{
		___m_flexibleWidth_143 = value;
	}

	inline static int32_t get_offset_of_m_minWidth_144() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minWidth_144)); }
	inline float get_m_minWidth_144() const { return ___m_minWidth_144; }
	inline float* get_address_of_m_minWidth_144() { return &___m_minWidth_144; }
	inline void set_m_minWidth_144(float value)
	{
		___m_minWidth_144 = value;
	}

	inline static int32_t get_offset_of_m_minHeight_145() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minHeight_145)); }
	inline float get_m_minHeight_145() const { return ___m_minHeight_145; }
	inline float* get_address_of_m_minHeight_145() { return &___m_minHeight_145; }
	inline void set_m_minHeight_145(float value)
	{
		___m_minHeight_145 = value;
	}

	inline static int32_t get_offset_of_m_maxWidth_146() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxWidth_146)); }
	inline float get_m_maxWidth_146() const { return ___m_maxWidth_146; }
	inline float* get_address_of_m_maxWidth_146() { return &___m_maxWidth_146; }
	inline void set_m_maxWidth_146(float value)
	{
		___m_maxWidth_146 = value;
	}

	inline static int32_t get_offset_of_m_maxHeight_147() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxHeight_147)); }
	inline float get_m_maxHeight_147() const { return ___m_maxHeight_147; }
	inline float* get_address_of_m_maxHeight_147() { return &___m_maxHeight_147; }
	inline void set_m_maxHeight_147(float value)
	{
		___m_maxHeight_147 = value;
	}

	inline static int32_t get_offset_of_m_LayoutElement_148() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_LayoutElement_148)); }
	inline LayoutElement_t1785403678 * get_m_LayoutElement_148() const { return ___m_LayoutElement_148; }
	inline LayoutElement_t1785403678 ** get_address_of_m_LayoutElement_148() { return &___m_LayoutElement_148; }
	inline void set_m_LayoutElement_148(LayoutElement_t1785403678 * value)
	{
		___m_LayoutElement_148 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutElement_148), value);
	}

	inline static int32_t get_offset_of_m_preferredWidth_149() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_preferredWidth_149)); }
	inline float get_m_preferredWidth_149() const { return ___m_preferredWidth_149; }
	inline float* get_address_of_m_preferredWidth_149() { return &___m_preferredWidth_149; }
	inline void set_m_preferredWidth_149(float value)
	{
		___m_preferredWidth_149 = value;
	}

	inline static int32_t get_offset_of_m_renderedWidth_150() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderedWidth_150)); }
	inline float get_m_renderedWidth_150() const { return ___m_renderedWidth_150; }
	inline float* get_address_of_m_renderedWidth_150() { return &___m_renderedWidth_150; }
	inline void set_m_renderedWidth_150(float value)
	{
		___m_renderedWidth_150 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredWidthDirty_151() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isPreferredWidthDirty_151)); }
	inline bool get_m_isPreferredWidthDirty_151() const { return ___m_isPreferredWidthDirty_151; }
	inline bool* get_address_of_m_isPreferredWidthDirty_151() { return &___m_isPreferredWidthDirty_151; }
	inline void set_m_isPreferredWidthDirty_151(bool value)
	{
		___m_isPreferredWidthDirty_151 = value;
	}

	inline static int32_t get_offset_of_m_preferredHeight_152() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_preferredHeight_152)); }
	inline float get_m_preferredHeight_152() const { return ___m_preferredHeight_152; }
	inline float* get_address_of_m_preferredHeight_152() { return &___m_preferredHeight_152; }
	inline void set_m_preferredHeight_152(float value)
	{
		___m_preferredHeight_152 = value;
	}

	inline static int32_t get_offset_of_m_renderedHeight_153() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderedHeight_153)); }
	inline float get_m_renderedHeight_153() const { return ___m_renderedHeight_153; }
	inline float* get_address_of_m_renderedHeight_153() { return &___m_renderedHeight_153; }
	inline void set_m_renderedHeight_153(float value)
	{
		___m_renderedHeight_153 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredHeightDirty_154() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isPreferredHeightDirty_154)); }
	inline bool get_m_isPreferredHeightDirty_154() const { return ___m_isPreferredHeightDirty_154; }
	inline bool* get_address_of_m_isPreferredHeightDirty_154() { return &___m_isPreferredHeightDirty_154; }
	inline void set_m_isPreferredHeightDirty_154(bool value)
	{
		___m_isPreferredHeightDirty_154 = value;
	}

	inline static int32_t get_offset_of_m_isCalculatingPreferredValues_155() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCalculatingPreferredValues_155)); }
	inline bool get_m_isCalculatingPreferredValues_155() const { return ___m_isCalculatingPreferredValues_155; }
	inline bool* get_address_of_m_isCalculatingPreferredValues_155() { return &___m_isCalculatingPreferredValues_155; }
	inline void set_m_isCalculatingPreferredValues_155(bool value)
	{
		___m_isCalculatingPreferredValues_155 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCount_156() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_recursiveCount_156)); }
	inline int32_t get_m_recursiveCount_156() const { return ___m_recursiveCount_156; }
	inline int32_t* get_address_of_m_recursiveCount_156() { return &___m_recursiveCount_156; }
	inline void set_m_recursiveCount_156(int32_t value)
	{
		___m_recursiveCount_156 = value;
	}

	inline static int32_t get_offset_of_m_layoutPriority_157() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_layoutPriority_157)); }
	inline int32_t get_m_layoutPriority_157() const { return ___m_layoutPriority_157; }
	inline int32_t* get_address_of_m_layoutPriority_157() { return &___m_layoutPriority_157; }
	inline void set_m_layoutPriority_157(int32_t value)
	{
		___m_layoutPriority_157 = value;
	}

	inline static int32_t get_offset_of_m_isCalculateSizeRequired_158() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCalculateSizeRequired_158)); }
	inline bool get_m_isCalculateSizeRequired_158() const { return ___m_isCalculateSizeRequired_158; }
	inline bool* get_address_of_m_isCalculateSizeRequired_158() { return &___m_isCalculateSizeRequired_158; }
	inline void set_m_isCalculateSizeRequired_158(bool value)
	{
		___m_isCalculateSizeRequired_158 = value;
	}

	inline static int32_t get_offset_of_m_isLayoutDirty_159() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isLayoutDirty_159)); }
	inline bool get_m_isLayoutDirty_159() const { return ___m_isLayoutDirty_159; }
	inline bool* get_address_of_m_isLayoutDirty_159() { return &___m_isLayoutDirty_159; }
	inline void set_m_isLayoutDirty_159(bool value)
	{
		___m_isLayoutDirty_159 = value;
	}

	inline static int32_t get_offset_of_m_verticesAlreadyDirty_160() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_verticesAlreadyDirty_160)); }
	inline bool get_m_verticesAlreadyDirty_160() const { return ___m_verticesAlreadyDirty_160; }
	inline bool* get_address_of_m_verticesAlreadyDirty_160() { return &___m_verticesAlreadyDirty_160; }
	inline void set_m_verticesAlreadyDirty_160(bool value)
	{
		___m_verticesAlreadyDirty_160 = value;
	}

	inline static int32_t get_offset_of_m_layoutAlreadyDirty_161() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_layoutAlreadyDirty_161)); }
	inline bool get_m_layoutAlreadyDirty_161() const { return ___m_layoutAlreadyDirty_161; }
	inline bool* get_address_of_m_layoutAlreadyDirty_161() { return &___m_layoutAlreadyDirty_161; }
	inline void set_m_layoutAlreadyDirty_161(bool value)
	{
		___m_layoutAlreadyDirty_161 = value;
	}

	inline static int32_t get_offset_of_m_isAwake_162() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isAwake_162)); }
	inline bool get_m_isAwake_162() const { return ___m_isAwake_162; }
	inline bool* get_address_of_m_isAwake_162() { return &___m_isAwake_162; }
	inline void set_m_isAwake_162(bool value)
	{
		___m_isAwake_162 = value;
	}

	inline static int32_t get_offset_of_m_isWaitingOnResourceLoad_163() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isWaitingOnResourceLoad_163)); }
	inline bool get_m_isWaitingOnResourceLoad_163() const { return ___m_isWaitingOnResourceLoad_163; }
	inline bool* get_address_of_m_isWaitingOnResourceLoad_163() { return &___m_isWaitingOnResourceLoad_163; }
	inline void set_m_isWaitingOnResourceLoad_163(bool value)
	{
		___m_isWaitingOnResourceLoad_163 = value;
	}

	inline static int32_t get_offset_of_m_isInputParsingRequired_164() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isInputParsingRequired_164)); }
	inline bool get_m_isInputParsingRequired_164() const { return ___m_isInputParsingRequired_164; }
	inline bool* get_address_of_m_isInputParsingRequired_164() { return &___m_isInputParsingRequired_164; }
	inline void set_m_isInputParsingRequired_164(bool value)
	{
		___m_isInputParsingRequired_164 = value;
	}

	inline static int32_t get_offset_of_m_inputSource_165() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_inputSource_165)); }
	inline int32_t get_m_inputSource_165() const { return ___m_inputSource_165; }
	inline int32_t* get_address_of_m_inputSource_165() { return &___m_inputSource_165; }
	inline void set_m_inputSource_165(int32_t value)
	{
		___m_inputSource_165 = value;
	}

	inline static int32_t get_offset_of_old_text_166() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___old_text_166)); }
	inline String_t* get_old_text_166() const { return ___old_text_166; }
	inline String_t** get_address_of_old_text_166() { return &___old_text_166; }
	inline void set_old_text_166(String_t* value)
	{
		___old_text_166 = value;
		Il2CppCodeGenWriteBarrier((&___old_text_166), value);
	}

	inline static int32_t get_offset_of_m_fontScale_167() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontScale_167)); }
	inline float get_m_fontScale_167() const { return ___m_fontScale_167; }
	inline float* get_address_of_m_fontScale_167() { return &___m_fontScale_167; }
	inline void set_m_fontScale_167(float value)
	{
		___m_fontScale_167 = value;
	}

	inline static int32_t get_offset_of_m_fontScaleMultiplier_168() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontScaleMultiplier_168)); }
	inline float get_m_fontScaleMultiplier_168() const { return ___m_fontScaleMultiplier_168; }
	inline float* get_address_of_m_fontScaleMultiplier_168() { return &___m_fontScaleMultiplier_168; }
	inline void set_m_fontScaleMultiplier_168(float value)
	{
		___m_fontScaleMultiplier_168 = value;
	}

	inline static int32_t get_offset_of_m_htmlTag_169() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_htmlTag_169)); }
	inline CharU5BU5D_t3528271667* get_m_htmlTag_169() const { return ___m_htmlTag_169; }
	inline CharU5BU5D_t3528271667** get_address_of_m_htmlTag_169() { return &___m_htmlTag_169; }
	inline void set_m_htmlTag_169(CharU5BU5D_t3528271667* value)
	{
		___m_htmlTag_169 = value;
		Il2CppCodeGenWriteBarrier((&___m_htmlTag_169), value);
	}

	inline static int32_t get_offset_of_m_xmlAttribute_170() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_xmlAttribute_170)); }
	inline XML_TagAttributeU5BU5D_t284240280* get_m_xmlAttribute_170() const { return ___m_xmlAttribute_170; }
	inline XML_TagAttributeU5BU5D_t284240280** get_address_of_m_xmlAttribute_170() { return &___m_xmlAttribute_170; }
	inline void set_m_xmlAttribute_170(XML_TagAttributeU5BU5D_t284240280* value)
	{
		___m_xmlAttribute_170 = value;
		Il2CppCodeGenWriteBarrier((&___m_xmlAttribute_170), value);
	}

	inline static int32_t get_offset_of_m_attributeParameterValues_171() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_attributeParameterValues_171)); }
	inline SingleU5BU5D_t1444911251* get_m_attributeParameterValues_171() const { return ___m_attributeParameterValues_171; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_attributeParameterValues_171() { return &___m_attributeParameterValues_171; }
	inline void set_m_attributeParameterValues_171(SingleU5BU5D_t1444911251* value)
	{
		___m_attributeParameterValues_171 = value;
		Il2CppCodeGenWriteBarrier((&___m_attributeParameterValues_171), value);
	}

	inline static int32_t get_offset_of_tag_LineIndent_172() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_LineIndent_172)); }
	inline float get_tag_LineIndent_172() const { return ___tag_LineIndent_172; }
	inline float* get_address_of_tag_LineIndent_172() { return &___tag_LineIndent_172; }
	inline void set_tag_LineIndent_172(float value)
	{
		___tag_LineIndent_172 = value;
	}

	inline static int32_t get_offset_of_tag_Indent_173() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_Indent_173)); }
	inline float get_tag_Indent_173() const { return ___tag_Indent_173; }
	inline float* get_address_of_tag_Indent_173() { return &___tag_Indent_173; }
	inline void set_tag_Indent_173(float value)
	{
		___tag_Indent_173 = value;
	}

	inline static int32_t get_offset_of_m_indentStack_174() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_indentStack_174)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_indentStack_174() const { return ___m_indentStack_174; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_indentStack_174() { return &___m_indentStack_174; }
	inline void set_m_indentStack_174(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_indentStack_174 = value;
	}

	inline static int32_t get_offset_of_tag_NoParsing_175() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_NoParsing_175)); }
	inline bool get_tag_NoParsing_175() const { return ___tag_NoParsing_175; }
	inline bool* get_address_of_tag_NoParsing_175() { return &___tag_NoParsing_175; }
	inline void set_tag_NoParsing_175(bool value)
	{
		___tag_NoParsing_175 = value;
	}

	inline static int32_t get_offset_of_m_isParsingText_176() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isParsingText_176)); }
	inline bool get_m_isParsingText_176() const { return ___m_isParsingText_176; }
	inline bool* get_address_of_m_isParsingText_176() { return &___m_isParsingText_176; }
	inline void set_m_isParsingText_176(bool value)
	{
		___m_isParsingText_176 = value;
	}

	inline static int32_t get_offset_of_m_FXMatrix_177() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_FXMatrix_177)); }
	inline Matrix4x4_t1817901843  get_m_FXMatrix_177() const { return ___m_FXMatrix_177; }
	inline Matrix4x4_t1817901843 * get_address_of_m_FXMatrix_177() { return &___m_FXMatrix_177; }
	inline void set_m_FXMatrix_177(Matrix4x4_t1817901843  value)
	{
		___m_FXMatrix_177 = value;
	}

	inline static int32_t get_offset_of_m_isFXMatrixSet_178() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isFXMatrixSet_178)); }
	inline bool get_m_isFXMatrixSet_178() const { return ___m_isFXMatrixSet_178; }
	inline bool* get_address_of_m_isFXMatrixSet_178() { return &___m_isFXMatrixSet_178; }
	inline void set_m_isFXMatrixSet_178(bool value)
	{
		___m_isFXMatrixSet_178 = value;
	}

	inline static int32_t get_offset_of_m_char_buffer_179() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_char_buffer_179)); }
	inline Int32U5BU5D_t385246372* get_m_char_buffer_179() const { return ___m_char_buffer_179; }
	inline Int32U5BU5D_t385246372** get_address_of_m_char_buffer_179() { return &___m_char_buffer_179; }
	inline void set_m_char_buffer_179(Int32U5BU5D_t385246372* value)
	{
		___m_char_buffer_179 = value;
		Il2CppCodeGenWriteBarrier((&___m_char_buffer_179), value);
	}

	inline static int32_t get_offset_of_m_internalCharacterInfo_180() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_internalCharacterInfo_180)); }
	inline TMP_CharacterInfoU5BU5D_t1930184704* get_m_internalCharacterInfo_180() const { return ___m_internalCharacterInfo_180; }
	inline TMP_CharacterInfoU5BU5D_t1930184704** get_address_of_m_internalCharacterInfo_180() { return &___m_internalCharacterInfo_180; }
	inline void set_m_internalCharacterInfo_180(TMP_CharacterInfoU5BU5D_t1930184704* value)
	{
		___m_internalCharacterInfo_180 = value;
		Il2CppCodeGenWriteBarrier((&___m_internalCharacterInfo_180), value);
	}

	inline static int32_t get_offset_of_m_input_CharArray_181() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_input_CharArray_181)); }
	inline CharU5BU5D_t3528271667* get_m_input_CharArray_181() const { return ___m_input_CharArray_181; }
	inline CharU5BU5D_t3528271667** get_address_of_m_input_CharArray_181() { return &___m_input_CharArray_181; }
	inline void set_m_input_CharArray_181(CharU5BU5D_t3528271667* value)
	{
		___m_input_CharArray_181 = value;
		Il2CppCodeGenWriteBarrier((&___m_input_CharArray_181), value);
	}

	inline static int32_t get_offset_of_m_charArray_Length_182() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charArray_Length_182)); }
	inline int32_t get_m_charArray_Length_182() const { return ___m_charArray_Length_182; }
	inline int32_t* get_address_of_m_charArray_Length_182() { return &___m_charArray_Length_182; }
	inline void set_m_charArray_Length_182(int32_t value)
	{
		___m_charArray_Length_182 = value;
	}

	inline static int32_t get_offset_of_m_totalCharacterCount_183() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_totalCharacterCount_183)); }
	inline int32_t get_m_totalCharacterCount_183() const { return ___m_totalCharacterCount_183; }
	inline int32_t* get_address_of_m_totalCharacterCount_183() { return &___m_totalCharacterCount_183; }
	inline void set_m_totalCharacterCount_183(int32_t value)
	{
		___m_totalCharacterCount_183 = value;
	}

	inline static int32_t get_offset_of_m_SavedWordWrapState_184() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_SavedWordWrapState_184)); }
	inline WordWrapState_t341939652  get_m_SavedWordWrapState_184() const { return ___m_SavedWordWrapState_184; }
	inline WordWrapState_t341939652 * get_address_of_m_SavedWordWrapState_184() { return &___m_SavedWordWrapState_184; }
	inline void set_m_SavedWordWrapState_184(WordWrapState_t341939652  value)
	{
		___m_SavedWordWrapState_184 = value;
	}

	inline static int32_t get_offset_of_m_SavedLineState_185() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_SavedLineState_185)); }
	inline WordWrapState_t341939652  get_m_SavedLineState_185() const { return ___m_SavedLineState_185; }
	inline WordWrapState_t341939652 * get_address_of_m_SavedLineState_185() { return &___m_SavedLineState_185; }
	inline void set_m_SavedLineState_185(WordWrapState_t341939652  value)
	{
		___m_SavedLineState_185 = value;
	}

	inline static int32_t get_offset_of_m_characterCount_186() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_characterCount_186)); }
	inline int32_t get_m_characterCount_186() const { return ___m_characterCount_186; }
	inline int32_t* get_address_of_m_characterCount_186() { return &___m_characterCount_186; }
	inline void set_m_characterCount_186(int32_t value)
	{
		___m_characterCount_186 = value;
	}

	inline static int32_t get_offset_of_m_firstCharacterOfLine_187() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstCharacterOfLine_187)); }
	inline int32_t get_m_firstCharacterOfLine_187() const { return ___m_firstCharacterOfLine_187; }
	inline int32_t* get_address_of_m_firstCharacterOfLine_187() { return &___m_firstCharacterOfLine_187; }
	inline void set_m_firstCharacterOfLine_187(int32_t value)
	{
		___m_firstCharacterOfLine_187 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacterOfLine_188() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstVisibleCharacterOfLine_188)); }
	inline int32_t get_m_firstVisibleCharacterOfLine_188() const { return ___m_firstVisibleCharacterOfLine_188; }
	inline int32_t* get_address_of_m_firstVisibleCharacterOfLine_188() { return &___m_firstVisibleCharacterOfLine_188; }
	inline void set_m_firstVisibleCharacterOfLine_188(int32_t value)
	{
		___m_firstVisibleCharacterOfLine_188 = value;
	}

	inline static int32_t get_offset_of_m_lastCharacterOfLine_189() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lastCharacterOfLine_189)); }
	inline int32_t get_m_lastCharacterOfLine_189() const { return ___m_lastCharacterOfLine_189; }
	inline int32_t* get_address_of_m_lastCharacterOfLine_189() { return &___m_lastCharacterOfLine_189; }
	inline void set_m_lastCharacterOfLine_189(int32_t value)
	{
		___m_lastCharacterOfLine_189 = value;
	}

	inline static int32_t get_offset_of_m_lastVisibleCharacterOfLine_190() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lastVisibleCharacterOfLine_190)); }
	inline int32_t get_m_lastVisibleCharacterOfLine_190() const { return ___m_lastVisibleCharacterOfLine_190; }
	inline int32_t* get_address_of_m_lastVisibleCharacterOfLine_190() { return &___m_lastVisibleCharacterOfLine_190; }
	inline void set_m_lastVisibleCharacterOfLine_190(int32_t value)
	{
		___m_lastVisibleCharacterOfLine_190 = value;
	}

	inline static int32_t get_offset_of_m_lineNumber_191() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineNumber_191)); }
	inline int32_t get_m_lineNumber_191() const { return ___m_lineNumber_191; }
	inline int32_t* get_address_of_m_lineNumber_191() { return &___m_lineNumber_191; }
	inline void set_m_lineNumber_191(int32_t value)
	{
		___m_lineNumber_191 = value;
	}

	inline static int32_t get_offset_of_m_lineVisibleCharacterCount_192() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineVisibleCharacterCount_192)); }
	inline int32_t get_m_lineVisibleCharacterCount_192() const { return ___m_lineVisibleCharacterCount_192; }
	inline int32_t* get_address_of_m_lineVisibleCharacterCount_192() { return &___m_lineVisibleCharacterCount_192; }
	inline void set_m_lineVisibleCharacterCount_192(int32_t value)
	{
		___m_lineVisibleCharacterCount_192 = value;
	}

	inline static int32_t get_offset_of_m_pageNumber_193() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_pageNumber_193)); }
	inline int32_t get_m_pageNumber_193() const { return ___m_pageNumber_193; }
	inline int32_t* get_address_of_m_pageNumber_193() { return &___m_pageNumber_193; }
	inline void set_m_pageNumber_193(int32_t value)
	{
		___m_pageNumber_193 = value;
	}

	inline static int32_t get_offset_of_m_maxAscender_194() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxAscender_194)); }
	inline float get_m_maxAscender_194() const { return ___m_maxAscender_194; }
	inline float* get_address_of_m_maxAscender_194() { return &___m_maxAscender_194; }
	inline void set_m_maxAscender_194(float value)
	{
		___m_maxAscender_194 = value;
	}

	inline static int32_t get_offset_of_m_maxCapHeight_195() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxCapHeight_195)); }
	inline float get_m_maxCapHeight_195() const { return ___m_maxCapHeight_195; }
	inline float* get_address_of_m_maxCapHeight_195() { return &___m_maxCapHeight_195; }
	inline void set_m_maxCapHeight_195(float value)
	{
		___m_maxCapHeight_195 = value;
	}

	inline static int32_t get_offset_of_m_maxDescender_196() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxDescender_196)); }
	inline float get_m_maxDescender_196() const { return ___m_maxDescender_196; }
	inline float* get_address_of_m_maxDescender_196() { return &___m_maxDescender_196; }
	inline void set_m_maxDescender_196(float value)
	{
		___m_maxDescender_196 = value;
	}

	inline static int32_t get_offset_of_m_maxLineAscender_197() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxLineAscender_197)); }
	inline float get_m_maxLineAscender_197() const { return ___m_maxLineAscender_197; }
	inline float* get_address_of_m_maxLineAscender_197() { return &___m_maxLineAscender_197; }
	inline void set_m_maxLineAscender_197(float value)
	{
		___m_maxLineAscender_197 = value;
	}

	inline static int32_t get_offset_of_m_maxLineDescender_198() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxLineDescender_198)); }
	inline float get_m_maxLineDescender_198() const { return ___m_maxLineDescender_198; }
	inline float* get_address_of_m_maxLineDescender_198() { return &___m_maxLineDescender_198; }
	inline void set_m_maxLineDescender_198(float value)
	{
		___m_maxLineDescender_198 = value;
	}

	inline static int32_t get_offset_of_m_startOfLineAscender_199() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_startOfLineAscender_199)); }
	inline float get_m_startOfLineAscender_199() const { return ___m_startOfLineAscender_199; }
	inline float* get_address_of_m_startOfLineAscender_199() { return &___m_startOfLineAscender_199; }
	inline void set_m_startOfLineAscender_199(float value)
	{
		___m_startOfLineAscender_199 = value;
	}

	inline static int32_t get_offset_of_m_lineOffset_200() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineOffset_200)); }
	inline float get_m_lineOffset_200() const { return ___m_lineOffset_200; }
	inline float* get_address_of_m_lineOffset_200() { return &___m_lineOffset_200; }
	inline void set_m_lineOffset_200(float value)
	{
		___m_lineOffset_200 = value;
	}

	inline static int32_t get_offset_of_m_meshExtents_201() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_meshExtents_201)); }
	inline Extents_t3837212874  get_m_meshExtents_201() const { return ___m_meshExtents_201; }
	inline Extents_t3837212874 * get_address_of_m_meshExtents_201() { return &___m_meshExtents_201; }
	inline void set_m_meshExtents_201(Extents_t3837212874  value)
	{
		___m_meshExtents_201 = value;
	}

	inline static int32_t get_offset_of_m_htmlColor_202() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_htmlColor_202)); }
	inline Color32_t2600501292  get_m_htmlColor_202() const { return ___m_htmlColor_202; }
	inline Color32_t2600501292 * get_address_of_m_htmlColor_202() { return &___m_htmlColor_202; }
	inline void set_m_htmlColor_202(Color32_t2600501292  value)
	{
		___m_htmlColor_202 = value;
	}

	inline static int32_t get_offset_of_m_colorStack_203() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_colorStack_203)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_colorStack_203() const { return ___m_colorStack_203; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_colorStack_203() { return &___m_colorStack_203; }
	inline void set_m_colorStack_203(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_colorStack_203 = value;
	}

	inline static int32_t get_offset_of_m_underlineColorStack_204() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_underlineColorStack_204)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_underlineColorStack_204() const { return ___m_underlineColorStack_204; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_underlineColorStack_204() { return &___m_underlineColorStack_204; }
	inline void set_m_underlineColorStack_204(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_underlineColorStack_204 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColorStack_205() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_strikethroughColorStack_205)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_strikethroughColorStack_205() const { return ___m_strikethroughColorStack_205; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_strikethroughColorStack_205() { return &___m_strikethroughColorStack_205; }
	inline void set_m_strikethroughColorStack_205(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_strikethroughColorStack_205 = value;
	}

	inline static int32_t get_offset_of_m_highlightColorStack_206() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_highlightColorStack_206)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_highlightColorStack_206() const { return ___m_highlightColorStack_206; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_highlightColorStack_206() { return &___m_highlightColorStack_206; }
	inline void set_m_highlightColorStack_206(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_highlightColorStack_206 = value;
	}

	inline static int32_t get_offset_of_m_colorGradientPreset_207() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_colorGradientPreset_207)); }
	inline TMP_ColorGradient_t3678055768 * get_m_colorGradientPreset_207() const { return ___m_colorGradientPreset_207; }
	inline TMP_ColorGradient_t3678055768 ** get_address_of_m_colorGradientPreset_207() { return &___m_colorGradientPreset_207; }
	inline void set_m_colorGradientPreset_207(TMP_ColorGradient_t3678055768 * value)
	{
		___m_colorGradientPreset_207 = value;
		Il2CppCodeGenWriteBarrier((&___m_colorGradientPreset_207), value);
	}

	inline static int32_t get_offset_of_m_colorGradientStack_208() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_colorGradientStack_208)); }
	inline TMP_XmlTagStack_1_t3241710312  get_m_colorGradientStack_208() const { return ___m_colorGradientStack_208; }
	inline TMP_XmlTagStack_1_t3241710312 * get_address_of_m_colorGradientStack_208() { return &___m_colorGradientStack_208; }
	inline void set_m_colorGradientStack_208(TMP_XmlTagStack_1_t3241710312  value)
	{
		___m_colorGradientStack_208 = value;
	}

	inline static int32_t get_offset_of_m_tabSpacing_209() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tabSpacing_209)); }
	inline float get_m_tabSpacing_209() const { return ___m_tabSpacing_209; }
	inline float* get_address_of_m_tabSpacing_209() { return &___m_tabSpacing_209; }
	inline void set_m_tabSpacing_209(float value)
	{
		___m_tabSpacing_209 = value;
	}

	inline static int32_t get_offset_of_m_spacing_210() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spacing_210)); }
	inline float get_m_spacing_210() const { return ___m_spacing_210; }
	inline float* get_address_of_m_spacing_210() { return &___m_spacing_210; }
	inline void set_m_spacing_210(float value)
	{
		___m_spacing_210 = value;
	}

	inline static int32_t get_offset_of_m_styleStack_211() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_styleStack_211)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_styleStack_211() const { return ___m_styleStack_211; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_styleStack_211() { return &___m_styleStack_211; }
	inline void set_m_styleStack_211(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_styleStack_211 = value;
	}

	inline static int32_t get_offset_of_m_actionStack_212() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_actionStack_212)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_actionStack_212() const { return ___m_actionStack_212; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_actionStack_212() { return &___m_actionStack_212; }
	inline void set_m_actionStack_212(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_actionStack_212 = value;
	}

	inline static int32_t get_offset_of_m_padding_213() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_padding_213)); }
	inline float get_m_padding_213() const { return ___m_padding_213; }
	inline float* get_address_of_m_padding_213() { return &___m_padding_213; }
	inline void set_m_padding_213(float value)
	{
		___m_padding_213 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffset_214() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_baselineOffset_214)); }
	inline float get_m_baselineOffset_214() const { return ___m_baselineOffset_214; }
	inline float* get_address_of_m_baselineOffset_214() { return &___m_baselineOffset_214; }
	inline void set_m_baselineOffset_214(float value)
	{
		___m_baselineOffset_214 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffsetStack_215() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_baselineOffsetStack_215)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_baselineOffsetStack_215() const { return ___m_baselineOffsetStack_215; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_baselineOffsetStack_215() { return &___m_baselineOffsetStack_215; }
	inline void set_m_baselineOffsetStack_215(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_baselineOffsetStack_215 = value;
	}

	inline static int32_t get_offset_of_m_xAdvance_216() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_xAdvance_216)); }
	inline float get_m_xAdvance_216() const { return ___m_xAdvance_216; }
	inline float* get_address_of_m_xAdvance_216() { return &___m_xAdvance_216; }
	inline void set_m_xAdvance_216(float value)
	{
		___m_xAdvance_216 = value;
	}

	inline static int32_t get_offset_of_m_textElementType_217() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textElementType_217)); }
	inline int32_t get_m_textElementType_217() const { return ___m_textElementType_217; }
	inline int32_t* get_address_of_m_textElementType_217() { return &___m_textElementType_217; }
	inline void set_m_textElementType_217(int32_t value)
	{
		___m_textElementType_217 = value;
	}

	inline static int32_t get_offset_of_m_cached_TextElement_218() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_TextElement_218)); }
	inline TMP_TextElement_t129727469 * get_m_cached_TextElement_218() const { return ___m_cached_TextElement_218; }
	inline TMP_TextElement_t129727469 ** get_address_of_m_cached_TextElement_218() { return &___m_cached_TextElement_218; }
	inline void set_m_cached_TextElement_218(TMP_TextElement_t129727469 * value)
	{
		___m_cached_TextElement_218 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_TextElement_218), value);
	}

	inline static int32_t get_offset_of_m_cached_Underline_GlyphInfo_219() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_Underline_GlyphInfo_219)); }
	inline TMP_Glyph_t581847833 * get_m_cached_Underline_GlyphInfo_219() const { return ___m_cached_Underline_GlyphInfo_219; }
	inline TMP_Glyph_t581847833 ** get_address_of_m_cached_Underline_GlyphInfo_219() { return &___m_cached_Underline_GlyphInfo_219; }
	inline void set_m_cached_Underline_GlyphInfo_219(TMP_Glyph_t581847833 * value)
	{
		___m_cached_Underline_GlyphInfo_219 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Underline_GlyphInfo_219), value);
	}

	inline static int32_t get_offset_of_m_cached_Ellipsis_GlyphInfo_220() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_Ellipsis_GlyphInfo_220)); }
	inline TMP_Glyph_t581847833 * get_m_cached_Ellipsis_GlyphInfo_220() const { return ___m_cached_Ellipsis_GlyphInfo_220; }
	inline TMP_Glyph_t581847833 ** get_address_of_m_cached_Ellipsis_GlyphInfo_220() { return &___m_cached_Ellipsis_GlyphInfo_220; }
	inline void set_m_cached_Ellipsis_GlyphInfo_220(TMP_Glyph_t581847833 * value)
	{
		___m_cached_Ellipsis_GlyphInfo_220 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Ellipsis_GlyphInfo_220), value);
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_221() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_defaultSpriteAsset_221)); }
	inline TMP_SpriteAsset_t484820633 * get_m_defaultSpriteAsset_221() const { return ___m_defaultSpriteAsset_221; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_defaultSpriteAsset_221() { return &___m_defaultSpriteAsset_221; }
	inline void set_m_defaultSpriteAsset_221(TMP_SpriteAsset_t484820633 * value)
	{
		___m_defaultSpriteAsset_221 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_221), value);
	}

	inline static int32_t get_offset_of_m_currentSpriteAsset_222() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentSpriteAsset_222)); }
	inline TMP_SpriteAsset_t484820633 * get_m_currentSpriteAsset_222() const { return ___m_currentSpriteAsset_222; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_currentSpriteAsset_222() { return &___m_currentSpriteAsset_222; }
	inline void set_m_currentSpriteAsset_222(TMP_SpriteAsset_t484820633 * value)
	{
		___m_currentSpriteAsset_222 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentSpriteAsset_222), value);
	}

	inline static int32_t get_offset_of_m_spriteCount_223() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteCount_223)); }
	inline int32_t get_m_spriteCount_223() const { return ___m_spriteCount_223; }
	inline int32_t* get_address_of_m_spriteCount_223() { return &___m_spriteCount_223; }
	inline void set_m_spriteCount_223(int32_t value)
	{
		___m_spriteCount_223 = value;
	}

	inline static int32_t get_offset_of_m_spriteIndex_224() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteIndex_224)); }
	inline int32_t get_m_spriteIndex_224() const { return ___m_spriteIndex_224; }
	inline int32_t* get_address_of_m_spriteIndex_224() { return &___m_spriteIndex_224; }
	inline void set_m_spriteIndex_224(int32_t value)
	{
		___m_spriteIndex_224 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimationID_225() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAnimationID_225)); }
	inline int32_t get_m_spriteAnimationID_225() const { return ___m_spriteAnimationID_225; }
	inline int32_t* get_address_of_m_spriteAnimationID_225() { return &___m_spriteAnimationID_225; }
	inline void set_m_spriteAnimationID_225(int32_t value)
	{
		___m_spriteAnimationID_225 = value;
	}

	inline static int32_t get_offset_of_m_ignoreActiveState_226() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreActiveState_226)); }
	inline bool get_m_ignoreActiveState_226() const { return ___m_ignoreActiveState_226; }
	inline bool* get_address_of_m_ignoreActiveState_226() { return &___m_ignoreActiveState_226; }
	inline void set_m_ignoreActiveState_226(bool value)
	{
		___m_ignoreActiveState_226 = value;
	}

	inline static int32_t get_offset_of_k_Power_227() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___k_Power_227)); }
	inline SingleU5BU5D_t1444911251* get_k_Power_227() const { return ___k_Power_227; }
	inline SingleU5BU5D_t1444911251** get_address_of_k_Power_227() { return &___k_Power_227; }
	inline void set_k_Power_227(SingleU5BU5D_t1444911251* value)
	{
		___k_Power_227 = value;
		Il2CppCodeGenWriteBarrier((&___k_Power_227), value);
	}
};

struct TMP_Text_t2599618874_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_Text::s_colorWhite
	Color32_t2600501292  ___s_colorWhite_47;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargePositiveVector2
	Vector2_t2156229523  ___k_LargePositiveVector2_228;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargeNegativeVector2
	Vector2_t2156229523  ___k_LargeNegativeVector2_229;
	// System.Single TMPro.TMP_Text::k_LargePositiveFloat
	float ___k_LargePositiveFloat_230;
	// System.Single TMPro.TMP_Text::k_LargeNegativeFloat
	float ___k_LargeNegativeFloat_231;
	// System.Int32 TMPro.TMP_Text::k_LargePositiveInt
	int32_t ___k_LargePositiveInt_232;
	// System.Int32 TMPro.TMP_Text::k_LargeNegativeInt
	int32_t ___k_LargeNegativeInt_233;

public:
	inline static int32_t get_offset_of_s_colorWhite_47() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___s_colorWhite_47)); }
	inline Color32_t2600501292  get_s_colorWhite_47() const { return ___s_colorWhite_47; }
	inline Color32_t2600501292 * get_address_of_s_colorWhite_47() { return &___s_colorWhite_47; }
	inline void set_s_colorWhite_47(Color32_t2600501292  value)
	{
		___s_colorWhite_47 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveVector2_228() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveVector2_228)); }
	inline Vector2_t2156229523  get_k_LargePositiveVector2_228() const { return ___k_LargePositiveVector2_228; }
	inline Vector2_t2156229523 * get_address_of_k_LargePositiveVector2_228() { return &___k_LargePositiveVector2_228; }
	inline void set_k_LargePositiveVector2_228(Vector2_t2156229523  value)
	{
		___k_LargePositiveVector2_228 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeVector2_229() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeVector2_229)); }
	inline Vector2_t2156229523  get_k_LargeNegativeVector2_229() const { return ___k_LargeNegativeVector2_229; }
	inline Vector2_t2156229523 * get_address_of_k_LargeNegativeVector2_229() { return &___k_LargeNegativeVector2_229; }
	inline void set_k_LargeNegativeVector2_229(Vector2_t2156229523  value)
	{
		___k_LargeNegativeVector2_229 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveFloat_230() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveFloat_230)); }
	inline float get_k_LargePositiveFloat_230() const { return ___k_LargePositiveFloat_230; }
	inline float* get_address_of_k_LargePositiveFloat_230() { return &___k_LargePositiveFloat_230; }
	inline void set_k_LargePositiveFloat_230(float value)
	{
		___k_LargePositiveFloat_230 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeFloat_231() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeFloat_231)); }
	inline float get_k_LargeNegativeFloat_231() const { return ___k_LargeNegativeFloat_231; }
	inline float* get_address_of_k_LargeNegativeFloat_231() { return &___k_LargeNegativeFloat_231; }
	inline void set_k_LargeNegativeFloat_231(float value)
	{
		___k_LargeNegativeFloat_231 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveInt_232() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveInt_232)); }
	inline int32_t get_k_LargePositiveInt_232() const { return ___k_LargePositiveInt_232; }
	inline int32_t* get_address_of_k_LargePositiveInt_232() { return &___k_LargePositiveInt_232; }
	inline void set_k_LargePositiveInt_232(int32_t value)
	{
		___k_LargePositiveInt_232 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeInt_233() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeInt_233)); }
	inline int32_t get_k_LargeNegativeInt_233() const { return ___k_LargeNegativeInt_233; }
	inline int32_t* get_address_of_k_LargeNegativeInt_233() { return &___k_LargeNegativeInt_233; }
	inline void set_k_LargeNegativeInt_233(int32_t value)
	{
		___k_LargeNegativeInt_233 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXT_T2599618874_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4400 = { sizeof (TextOverflowModes_t1430035314)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4400[8] = 
{
	TextOverflowModes_t1430035314::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4401 = { sizeof (MaskingOffsetMode_t2266644590)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4401[3] = 
{
	MaskingOffsetMode_t2266644590::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4402 = { sizeof (TextureMappingOptions_t270963663)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4402[5] = 
{
	TextureMappingOptions_t270963663::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4403 = { sizeof (FontStyles_t3828945032)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4403[12] = 
{
	FontStyles_t3828945032::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4404 = { sizeof (FontWeights_t3122883458)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4404[10] = 
{
	FontWeights_t3122883458::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4405 = { sizeof (TagUnits_t1169424683)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4405[4] = 
{
	TagUnits_t1169424683::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4406 = { sizeof (TagType_t123236451)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4406[5] = 
{
	TagType_t123236451::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4407 = { sizeof (TMP_Text_t2599618874), -1, sizeof(TMP_Text_t2599618874_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4407[204] = 
{
	TMP_Text_t2599618874::get_offset_of_m_text_30(),
	TMP_Text_t2599618874::get_offset_of_m_isRightToLeft_31(),
	TMP_Text_t2599618874::get_offset_of_m_fontAsset_32(),
	TMP_Text_t2599618874::get_offset_of_m_currentFontAsset_33(),
	TMP_Text_t2599618874::get_offset_of_m_isSDFShader_34(),
	TMP_Text_t2599618874::get_offset_of_m_sharedMaterial_35(),
	TMP_Text_t2599618874::get_offset_of_m_currentMaterial_36(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferences_37(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferenceIndexLookup_38(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferenceStack_39(),
	TMP_Text_t2599618874::get_offset_of_m_currentMaterialIndex_40(),
	TMP_Text_t2599618874::get_offset_of_m_fontSharedMaterials_41(),
	TMP_Text_t2599618874::get_offset_of_m_fontMaterial_42(),
	TMP_Text_t2599618874::get_offset_of_m_fontMaterials_43(),
	TMP_Text_t2599618874::get_offset_of_m_isMaterialDirty_44(),
	TMP_Text_t2599618874::get_offset_of_m_fontColor32_45(),
	TMP_Text_t2599618874::get_offset_of_m_fontColor_46(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_s_colorWhite_47(),
	TMP_Text_t2599618874::get_offset_of_m_underlineColor_48(),
	TMP_Text_t2599618874::get_offset_of_m_strikethroughColor_49(),
	TMP_Text_t2599618874::get_offset_of_m_highlightColor_50(),
	TMP_Text_t2599618874::get_offset_of_m_enableVertexGradient_51(),
	TMP_Text_t2599618874::get_offset_of_m_fontColorGradient_52(),
	TMP_Text_t2599618874::get_offset_of_m_fontColorGradientPreset_53(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAsset_54(),
	TMP_Text_t2599618874::get_offset_of_m_tintAllSprites_55(),
	TMP_Text_t2599618874::get_offset_of_m_tintSprite_56(),
	TMP_Text_t2599618874::get_offset_of_m_spriteColor_57(),
	TMP_Text_t2599618874::get_offset_of_m_overrideHtmlColors_58(),
	TMP_Text_t2599618874::get_offset_of_m_faceColor_59(),
	TMP_Text_t2599618874::get_offset_of_m_outlineColor_60(),
	TMP_Text_t2599618874::get_offset_of_m_outlineWidth_61(),
	TMP_Text_t2599618874::get_offset_of_m_fontSize_62(),
	TMP_Text_t2599618874::get_offset_of_m_currentFontSize_63(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeBase_64(),
	TMP_Text_t2599618874::get_offset_of_m_sizeStack_65(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeight_66(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeightInternal_67(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeightStack_68(),
	TMP_Text_t2599618874::get_offset_of_m_enableAutoSizing_69(),
	TMP_Text_t2599618874::get_offset_of_m_maxFontSize_70(),
	TMP_Text_t2599618874::get_offset_of_m_minFontSize_71(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeMin_72(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeMax_73(),
	TMP_Text_t2599618874::get_offset_of_m_fontStyle_74(),
	TMP_Text_t2599618874::get_offset_of_m_style_75(),
	TMP_Text_t2599618874::get_offset_of_m_fontStyleStack_76(),
	TMP_Text_t2599618874::get_offset_of_m_isUsingBold_77(),
	TMP_Text_t2599618874::get_offset_of_m_textAlignment_78(),
	TMP_Text_t2599618874::get_offset_of_m_lineJustification_79(),
	TMP_Text_t2599618874::get_offset_of_m_lineJustificationStack_80(),
	TMP_Text_t2599618874::get_offset_of_m_textContainerLocalCorners_81(),
	TMP_Text_t2599618874::get_offset_of_m_isAlignmentEnumConverted_82(),
	TMP_Text_t2599618874::get_offset_of_m_characterSpacing_83(),
	TMP_Text_t2599618874::get_offset_of_m_cSpacing_84(),
	TMP_Text_t2599618874::get_offset_of_m_monoSpacing_85(),
	TMP_Text_t2599618874::get_offset_of_m_wordSpacing_86(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacing_87(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacingDelta_88(),
	TMP_Text_t2599618874::get_offset_of_m_lineHeight_89(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacingMax_90(),
	TMP_Text_t2599618874::get_offset_of_m_paragraphSpacing_91(),
	TMP_Text_t2599618874::get_offset_of_m_charWidthMaxAdj_92(),
	TMP_Text_t2599618874::get_offset_of_m_charWidthAdjDelta_93(),
	TMP_Text_t2599618874::get_offset_of_m_enableWordWrapping_94(),
	TMP_Text_t2599618874::get_offset_of_m_isCharacterWrappingEnabled_95(),
	TMP_Text_t2599618874::get_offset_of_m_isNonBreakingSpace_96(),
	TMP_Text_t2599618874::get_offset_of_m_isIgnoringAlignment_97(),
	TMP_Text_t2599618874::get_offset_of_m_wordWrappingRatios_98(),
	TMP_Text_t2599618874::get_offset_of_m_overflowMode_99(),
	TMP_Text_t2599618874::get_offset_of_m_firstOverflowCharacterIndex_100(),
	TMP_Text_t2599618874::get_offset_of_m_linkedTextComponent_101(),
	TMP_Text_t2599618874::get_offset_of_m_isLinkedTextComponent_102(),
	TMP_Text_t2599618874::get_offset_of_m_isTextTruncated_103(),
	TMP_Text_t2599618874::get_offset_of_m_enableKerning_104(),
	TMP_Text_t2599618874::get_offset_of_m_enableExtraPadding_105(),
	TMP_Text_t2599618874::get_offset_of_checkPaddingRequired_106(),
	TMP_Text_t2599618874::get_offset_of_m_isRichText_107(),
	TMP_Text_t2599618874::get_offset_of_m_parseCtrlCharacters_108(),
	TMP_Text_t2599618874::get_offset_of_m_isOverlay_109(),
	TMP_Text_t2599618874::get_offset_of_m_isOrthographic_110(),
	TMP_Text_t2599618874::get_offset_of_m_isCullingEnabled_111(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreRectMaskCulling_112(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreCulling_113(),
	TMP_Text_t2599618874::get_offset_of_m_horizontalMapping_114(),
	TMP_Text_t2599618874::get_offset_of_m_verticalMapping_115(),
	TMP_Text_t2599618874::get_offset_of_m_uvLineOffset_116(),
	TMP_Text_t2599618874::get_offset_of_m_renderMode_117(),
	TMP_Text_t2599618874::get_offset_of_m_geometrySortingOrder_118(),
	TMP_Text_t2599618874::get_offset_of_m_firstVisibleCharacter_119(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleCharacters_120(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleWords_121(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleLines_122(),
	TMP_Text_t2599618874::get_offset_of_m_useMaxVisibleDescender_123(),
	TMP_Text_t2599618874::get_offset_of_m_pageToDisplay_124(),
	TMP_Text_t2599618874::get_offset_of_m_isNewPage_125(),
	TMP_Text_t2599618874::get_offset_of_m_margin_126(),
	TMP_Text_t2599618874::get_offset_of_m_marginLeft_127(),
	TMP_Text_t2599618874::get_offset_of_m_marginRight_128(),
	TMP_Text_t2599618874::get_offset_of_m_marginWidth_129(),
	TMP_Text_t2599618874::get_offset_of_m_marginHeight_130(),
	TMP_Text_t2599618874::get_offset_of_m_width_131(),
	TMP_Text_t2599618874::get_offset_of_m_textInfo_132(),
	TMP_Text_t2599618874::get_offset_of_m_havePropertiesChanged_133(),
	TMP_Text_t2599618874::get_offset_of_m_isUsingLegacyAnimationComponent_134(),
	TMP_Text_t2599618874::get_offset_of_m_transform_135(),
	TMP_Text_t2599618874::get_offset_of_m_rectTransform_136(),
	TMP_Text_t2599618874::get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_137(),
	TMP_Text_t2599618874::get_offset_of_m_autoSizeTextContainer_138(),
	TMP_Text_t2599618874::get_offset_of_m_mesh_139(),
	TMP_Text_t2599618874::get_offset_of_m_isVolumetricText_140(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAnimator_141(),
	TMP_Text_t2599618874::get_offset_of_m_flexibleHeight_142(),
	TMP_Text_t2599618874::get_offset_of_m_flexibleWidth_143(),
	TMP_Text_t2599618874::get_offset_of_m_minWidth_144(),
	TMP_Text_t2599618874::get_offset_of_m_minHeight_145(),
	TMP_Text_t2599618874::get_offset_of_m_maxWidth_146(),
	TMP_Text_t2599618874::get_offset_of_m_maxHeight_147(),
	TMP_Text_t2599618874::get_offset_of_m_LayoutElement_148(),
	TMP_Text_t2599618874::get_offset_of_m_preferredWidth_149(),
	TMP_Text_t2599618874::get_offset_of_m_renderedWidth_150(),
	TMP_Text_t2599618874::get_offset_of_m_isPreferredWidthDirty_151(),
	TMP_Text_t2599618874::get_offset_of_m_preferredHeight_152(),
	TMP_Text_t2599618874::get_offset_of_m_renderedHeight_153(),
	TMP_Text_t2599618874::get_offset_of_m_isPreferredHeightDirty_154(),
	TMP_Text_t2599618874::get_offset_of_m_isCalculatingPreferredValues_155(),
	TMP_Text_t2599618874::get_offset_of_m_recursiveCount_156(),
	TMP_Text_t2599618874::get_offset_of_m_layoutPriority_157(),
	TMP_Text_t2599618874::get_offset_of_m_isCalculateSizeRequired_158(),
	TMP_Text_t2599618874::get_offset_of_m_isLayoutDirty_159(),
	TMP_Text_t2599618874::get_offset_of_m_verticesAlreadyDirty_160(),
	TMP_Text_t2599618874::get_offset_of_m_layoutAlreadyDirty_161(),
	TMP_Text_t2599618874::get_offset_of_m_isAwake_162(),
	TMP_Text_t2599618874::get_offset_of_m_isWaitingOnResourceLoad_163(),
	TMP_Text_t2599618874::get_offset_of_m_isInputParsingRequired_164(),
	TMP_Text_t2599618874::get_offset_of_m_inputSource_165(),
	TMP_Text_t2599618874::get_offset_of_old_text_166(),
	TMP_Text_t2599618874::get_offset_of_m_fontScale_167(),
	TMP_Text_t2599618874::get_offset_of_m_fontScaleMultiplier_168(),
	TMP_Text_t2599618874::get_offset_of_m_htmlTag_169(),
	TMP_Text_t2599618874::get_offset_of_m_xmlAttribute_170(),
	TMP_Text_t2599618874::get_offset_of_m_attributeParameterValues_171(),
	TMP_Text_t2599618874::get_offset_of_tag_LineIndent_172(),
	TMP_Text_t2599618874::get_offset_of_tag_Indent_173(),
	TMP_Text_t2599618874::get_offset_of_m_indentStack_174(),
	TMP_Text_t2599618874::get_offset_of_tag_NoParsing_175(),
	TMP_Text_t2599618874::get_offset_of_m_isParsingText_176(),
	TMP_Text_t2599618874::get_offset_of_m_FXMatrix_177(),
	TMP_Text_t2599618874::get_offset_of_m_isFXMatrixSet_178(),
	TMP_Text_t2599618874::get_offset_of_m_char_buffer_179(),
	TMP_Text_t2599618874::get_offset_of_m_internalCharacterInfo_180(),
	TMP_Text_t2599618874::get_offset_of_m_input_CharArray_181(),
	TMP_Text_t2599618874::get_offset_of_m_charArray_Length_182(),
	TMP_Text_t2599618874::get_offset_of_m_totalCharacterCount_183(),
	TMP_Text_t2599618874::get_offset_of_m_SavedWordWrapState_184(),
	TMP_Text_t2599618874::get_offset_of_m_SavedLineState_185(),
	TMP_Text_t2599618874::get_offset_of_m_characterCount_186(),
	TMP_Text_t2599618874::get_offset_of_m_firstCharacterOfLine_187(),
	TMP_Text_t2599618874::get_offset_of_m_firstVisibleCharacterOfLine_188(),
	TMP_Text_t2599618874::get_offset_of_m_lastCharacterOfLine_189(),
	TMP_Text_t2599618874::get_offset_of_m_lastVisibleCharacterOfLine_190(),
	TMP_Text_t2599618874::get_offset_of_m_lineNumber_191(),
	TMP_Text_t2599618874::get_offset_of_m_lineVisibleCharacterCount_192(),
	TMP_Text_t2599618874::get_offset_of_m_pageNumber_193(),
	TMP_Text_t2599618874::get_offset_of_m_maxAscender_194(),
	TMP_Text_t2599618874::get_offset_of_m_maxCapHeight_195(),
	TMP_Text_t2599618874::get_offset_of_m_maxDescender_196(),
	TMP_Text_t2599618874::get_offset_of_m_maxLineAscender_197(),
	TMP_Text_t2599618874::get_offset_of_m_maxLineDescender_198(),
	TMP_Text_t2599618874::get_offset_of_m_startOfLineAscender_199(),
	TMP_Text_t2599618874::get_offset_of_m_lineOffset_200(),
	TMP_Text_t2599618874::get_offset_of_m_meshExtents_201(),
	TMP_Text_t2599618874::get_offset_of_m_htmlColor_202(),
	TMP_Text_t2599618874::get_offset_of_m_colorStack_203(),
	TMP_Text_t2599618874::get_offset_of_m_underlineColorStack_204(),
	TMP_Text_t2599618874::get_offset_of_m_strikethroughColorStack_205(),
	TMP_Text_t2599618874::get_offset_of_m_highlightColorStack_206(),
	TMP_Text_t2599618874::get_offset_of_m_colorGradientPreset_207(),
	TMP_Text_t2599618874::get_offset_of_m_colorGradientStack_208(),
	TMP_Text_t2599618874::get_offset_of_m_tabSpacing_209(),
	TMP_Text_t2599618874::get_offset_of_m_spacing_210(),
	TMP_Text_t2599618874::get_offset_of_m_styleStack_211(),
	TMP_Text_t2599618874::get_offset_of_m_actionStack_212(),
	TMP_Text_t2599618874::get_offset_of_m_padding_213(),
	TMP_Text_t2599618874::get_offset_of_m_baselineOffset_214(),
	TMP_Text_t2599618874::get_offset_of_m_baselineOffsetStack_215(),
	TMP_Text_t2599618874::get_offset_of_m_xAdvance_216(),
	TMP_Text_t2599618874::get_offset_of_m_textElementType_217(),
	TMP_Text_t2599618874::get_offset_of_m_cached_TextElement_218(),
	TMP_Text_t2599618874::get_offset_of_m_cached_Underline_GlyphInfo_219(),
	TMP_Text_t2599618874::get_offset_of_m_cached_Ellipsis_GlyphInfo_220(),
	TMP_Text_t2599618874::get_offset_of_m_defaultSpriteAsset_221(),
	TMP_Text_t2599618874::get_offset_of_m_currentSpriteAsset_222(),
	TMP_Text_t2599618874::get_offset_of_m_spriteCount_223(),
	TMP_Text_t2599618874::get_offset_of_m_spriteIndex_224(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAnimationID_225(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreActiveState_226(),
	TMP_Text_t2599618874::get_offset_of_k_Power_227(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveVector2_228(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeVector2_229(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveFloat_230(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeFloat_231(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveInt_232(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeInt_233(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4408 = { sizeof (TextInputSources_t1522115805)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4408[5] = 
{
	TextInputSources_t1522115805::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4409 = { sizeof (TMP_TextElement_t129727469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4409[9] = 
{
	TMP_TextElement_t129727469::get_offset_of_id_0(),
	TMP_TextElement_t129727469::get_offset_of_x_1(),
	TMP_TextElement_t129727469::get_offset_of_y_2(),
	TMP_TextElement_t129727469::get_offset_of_width_3(),
	TMP_TextElement_t129727469::get_offset_of_height_4(),
	TMP_TextElement_t129727469::get_offset_of_xOffset_5(),
	TMP_TextElement_t129727469::get_offset_of_yOffset_6(),
	TMP_TextElement_t129727469::get_offset_of_xAdvance_7(),
	TMP_TextElement_t129727469::get_offset_of_scale_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4410 = { sizeof (TMP_TextInfo_t3598145122), -1, sizeof(TMP_TextInfo_t3598145122_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4410[18] = 
{
	TMP_TextInfo_t3598145122_StaticFields::get_offset_of_k_InfinityVectorPositive_0(),
	TMP_TextInfo_t3598145122_StaticFields::get_offset_of_k_InfinityVectorNegative_1(),
	TMP_TextInfo_t3598145122::get_offset_of_textComponent_2(),
	TMP_TextInfo_t3598145122::get_offset_of_characterCount_3(),
	TMP_TextInfo_t3598145122::get_offset_of_spriteCount_4(),
	TMP_TextInfo_t3598145122::get_offset_of_spaceCount_5(),
	TMP_TextInfo_t3598145122::get_offset_of_wordCount_6(),
	TMP_TextInfo_t3598145122::get_offset_of_linkCount_7(),
	TMP_TextInfo_t3598145122::get_offset_of_lineCount_8(),
	TMP_TextInfo_t3598145122::get_offset_of_pageCount_9(),
	TMP_TextInfo_t3598145122::get_offset_of_materialCount_10(),
	TMP_TextInfo_t3598145122::get_offset_of_characterInfo_11(),
	TMP_TextInfo_t3598145122::get_offset_of_wordInfo_12(),
	TMP_TextInfo_t3598145122::get_offset_of_linkInfo_13(),
	TMP_TextInfo_t3598145122::get_offset_of_lineInfo_14(),
	TMP_TextInfo_t3598145122::get_offset_of_pageInfo_15(),
	TMP_TextInfo_t3598145122::get_offset_of_meshInfo_16(),
	TMP_TextInfo_t3598145122::get_offset_of_m_CachedMeshInfo_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4411 = { sizeof (CaretPosition_t3997512201)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4411[4] = 
{
	CaretPosition_t3997512201::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4412 = { sizeof (CaretInfo_t841780893)+ sizeof (RuntimeObject), sizeof(CaretInfo_t841780893 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4412[2] = 
{
	CaretInfo_t841780893::get_offset_of_index_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CaretInfo_t841780893::get_offset_of_position_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4413 = { sizeof (TMP_TextUtilities_t2105690005), -1, sizeof(TMP_TextUtilities_t2105690005_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4413[3] = 
{
	TMP_TextUtilities_t2105690005_StaticFields::get_offset_of_m_rectWorldCorners_0(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4414 = { sizeof (LineSegment_t1526544958)+ sizeof (RuntimeObject), sizeof(LineSegment_t1526544958 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4414[2] = 
{
	LineSegment_t1526544958::get_offset_of_Point1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LineSegment_t1526544958::get_offset_of_Point2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4415 = { sizeof (TMP_UpdateManager_t4114267509), -1, sizeof(TMP_UpdateManager_t4114267509_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4415[5] = 
{
	TMP_UpdateManager_t4114267509_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_GraphicQueueLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4416 = { sizeof (TMP_UpdateRegistry_t461608481), -1, sizeof(TMP_UpdateRegistry_t461608481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4416[5] = 
{
	TMP_UpdateRegistry_t461608481_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_GraphicQueueLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4417 = { sizeof (TMP_BasicXmlTagStack_t2962628096)+ sizeof (RuntimeObject), sizeof(TMP_BasicXmlTagStack_t2962628096 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4417[10] = 
{
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_bold_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_italic_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_underline_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_strikethrough_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_highlight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_superscript_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_subscript_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_uppercase_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_lowercase_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_smallcaps_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4418 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4418[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4419 = { sizeof (Compute_DistanceTransform_EventTypes_t2554612104)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4419[3] = 
{
	Compute_DistanceTransform_EventTypes_t2554612104::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4420 = { sizeof (TMPro_EventManager_t712497257), -1, sizeof(TMPro_EventManager_t712497257_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4420[13] = 
{
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_COMPUTE_DT_EVENT_0(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_MATERIAL_PROPERTY_EVENT_1(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_FONT_PROPERTY_EVENT_2(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_SPRITE_ASSET_PROPERTY_EVENT_3(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TEXTMESHPRO_PROPERTY_EVENT_4(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_DRAG_AND_DROP_MATERIAL_EVENT_5(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TEXT_STYLE_PROPERTY_EVENT_6(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_COLOR_GRADIENT_PROPERTY_EVENT_7(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TMP_SETTINGS_PROPERTY_EVENT_8(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_RESOURCE_LOAD_EVENT_9(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_OnPreRenderObject_Event_11(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TEXT_CHANGED_EVENT_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4421 = { sizeof (Compute_DT_EventArgs_t1071353166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4421[3] = 
{
	Compute_DT_EventArgs_t1071353166::get_offset_of_EventType_0(),
	Compute_DT_EventArgs_t1071353166::get_offset_of_ProgressPercentage_1(),
	Compute_DT_EventArgs_t1071353166::get_offset_of_Colors_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4422 = { sizeof (TMPro_ExtensionMethods_t1453208966), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4423 = { sizeof (TMP_Math_t624304809), -1, sizeof(TMP_Math_t624304809_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4423[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	TMP_Math_t624304809_StaticFields::get_offset_of_MAX_16BIT_6(),
	TMP_Math_t624304809_StaticFields::get_offset_of_MIN_16BIT_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4424 = { sizeof (FaceInfo_t2243299176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4424[21] = 
{
	FaceInfo_t2243299176::get_offset_of_Name_0(),
	FaceInfo_t2243299176::get_offset_of_PointSize_1(),
	FaceInfo_t2243299176::get_offset_of_Scale_2(),
	FaceInfo_t2243299176::get_offset_of_CharacterCount_3(),
	FaceInfo_t2243299176::get_offset_of_LineHeight_4(),
	FaceInfo_t2243299176::get_offset_of_Baseline_5(),
	FaceInfo_t2243299176::get_offset_of_Ascender_6(),
	FaceInfo_t2243299176::get_offset_of_CapHeight_7(),
	FaceInfo_t2243299176::get_offset_of_Descender_8(),
	FaceInfo_t2243299176::get_offset_of_CenterLine_9(),
	FaceInfo_t2243299176::get_offset_of_SuperscriptOffset_10(),
	FaceInfo_t2243299176::get_offset_of_SubscriptOffset_11(),
	FaceInfo_t2243299176::get_offset_of_SubSize_12(),
	FaceInfo_t2243299176::get_offset_of_Underline_13(),
	FaceInfo_t2243299176::get_offset_of_UnderlineThickness_14(),
	FaceInfo_t2243299176::get_offset_of_strikethrough_15(),
	FaceInfo_t2243299176::get_offset_of_strikethroughThickness_16(),
	FaceInfo_t2243299176::get_offset_of_TabWidth_17(),
	FaceInfo_t2243299176::get_offset_of_Padding_18(),
	FaceInfo_t2243299176::get_offset_of_AtlasWidth_19(),
	FaceInfo_t2243299176::get_offset_of_AtlasHeight_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4425 = { sizeof (TMP_Glyph_t581847833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4426 = { sizeof (FontAssetCreationSettings_t359369028)+ sizeof (RuntimeObject), sizeof(FontAssetCreationSettings_t359369028_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4426[16] = 
{
	FontAssetCreationSettings_t359369028::get_offset_of_sourceFontFileName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_sourceFontFileGUID_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_pointSizeSamplingMode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_pointSize_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_padding_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_packingMode_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_atlasWidth_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_atlasHeight_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_characterSetSelectionMode_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_characterSequence_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_referencedFontAssetGUID_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_referencedTextAssetGUID_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_fontStyle_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_fontStyleModifier_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_renderMode_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_includeFontFeatures_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4427 = { sizeof (KerningPairKey_t536493877)+ sizeof (RuntimeObject), sizeof(KerningPairKey_t536493877 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4427[3] = 
{
	KerningPairKey_t536493877::get_offset_of_ascii_Left_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KerningPairKey_t536493877::get_offset_of_ascii_Right_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KerningPairKey_t536493877::get_offset_of_key_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4428 = { sizeof (GlyphValueRecord_t4065874512)+ sizeof (RuntimeObject), sizeof(GlyphValueRecord_t4065874512 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4428[4] = 
{
	GlyphValueRecord_t4065874512::get_offset_of_xPlacement_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_t4065874512::get_offset_of_yPlacement_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_t4065874512::get_offset_of_xAdvance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_t4065874512::get_offset_of_yAdvance_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4429 = { sizeof (KerningPair_t2270855589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4429[5] = 
{
	KerningPair_t2270855589::get_offset_of_m_FirstGlyph_0(),
	KerningPair_t2270855589::get_offset_of_m_FirstGlyphAdjustments_1(),
	KerningPair_t2270855589::get_offset_of_m_SecondGlyph_2(),
	KerningPair_t2270855589::get_offset_of_m_SecondGlyphAdjustments_3(),
	KerningPair_t2270855589::get_offset_of_xOffset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4430 = { sizeof (KerningTable_t2322366871), -1, sizeof(KerningTable_t2322366871_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4430[3] = 
{
	KerningTable_t2322366871::get_offset_of_kerningPairs_0(),
	KerningTable_t2322366871_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
	KerningTable_t2322366871_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4431 = { sizeof (U3CAddKerningPairU3Ec__AnonStorey0_t2688361982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4431[2] = 
{
	U3CAddKerningPairU3Ec__AnonStorey0_t2688361982::get_offset_of_first_0(),
	U3CAddKerningPairU3Ec__AnonStorey0_t2688361982::get_offset_of_second_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4432 = { sizeof (U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4432[2] = 
{
	U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262::get_offset_of_first_0(),
	U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262::get_offset_of_second_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4433 = { sizeof (U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4433[2] = 
{
	U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074::get_offset_of_left_0(),
	U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4434 = { sizeof (TMP_FontUtilities_t2599150238), -1, sizeof(TMP_FontUtilities_t2599150238_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4434[1] = 
{
	TMP_FontUtilities_t2599150238_StaticFields::get_offset_of_k_searchedFontAssets_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4435 = { sizeof (TMP_VertexDataUpdateFlags_t388000256)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4435[8] = 
{
	TMP_VertexDataUpdateFlags_t388000256::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4436 = { sizeof (TMP_CharacterInfo_t3185626797)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4436[35] = 
{
	TMP_CharacterInfo_t3185626797::get_offset_of_character_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_index_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_elementType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_textElement_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_fontAsset_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_spriteAsset_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_spriteIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_material_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_materialReferenceIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_isUsingAlternateTypeface_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_pointSize_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_lineNumber_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_pageNumber_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertexIndex_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_TL_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_BL_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_TR_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_BR_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_topLeft_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_bottomLeft_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_topRight_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_bottomRight_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_origin_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_ascender_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_baseLine_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_descender_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_xAdvance_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_aspectRatio_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_scale_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_color_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_style_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_isVisible_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4437 = { sizeof (TMP_Vertex_t2404176824)+ sizeof (RuntimeObject), sizeof(TMP_Vertex_t2404176824 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4437[5] = 
{
	TMP_Vertex_t2404176824::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv4_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_color_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4438 = { sizeof (VertexGradient_t345148380)+ sizeof (RuntimeObject), sizeof(VertexGradient_t345148380 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4438[4] = 
{
	VertexGradient_t345148380::get_offset_of_topLeft_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t345148380::get_offset_of_topRight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t345148380::get_offset_of_bottomLeft_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t345148380::get_offset_of_bottomRight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4439 = { sizeof (TMP_PageInfo_t2608430633)+ sizeof (RuntimeObject), sizeof(TMP_PageInfo_t2608430633 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4439[5] = 
{
	TMP_PageInfo_t2608430633::get_offset_of_firstCharacterIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_lastCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_ascender_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_baseLine_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_descender_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4440 = { sizeof (TMP_LinkInfo_t1092083476)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4440[7] = 
{
	TMP_LinkInfo_t1092083476::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_hashCode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkIdFirstCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkIdLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkTextfirstCharacterIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkTextLength_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkID_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4441 = { sizeof (TMP_WordInfo_t3331066303)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4441[4] = 
{
	TMP_WordInfo_t3331066303::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3331066303::get_offset_of_firstCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3331066303::get_offset_of_lastCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3331066303::get_offset_of_characterCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4442 = { sizeof (TMP_SpriteInfo_t2726321384)+ sizeof (RuntimeObject), sizeof(TMP_SpriteInfo_t2726321384 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4442[3] = 
{
	TMP_SpriteInfo_t2726321384::get_offset_of_spriteIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t2726321384::get_offset_of_characterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t2726321384::get_offset_of_vertexIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4443 = { sizeof (Extents_t3837212874)+ sizeof (RuntimeObject), sizeof(Extents_t3837212874 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4443[2] = 
{
	Extents_t3837212874::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Extents_t3837212874::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4444 = { sizeof (Mesh_Extents_t3388355125)+ sizeof (RuntimeObject), sizeof(Mesh_Extents_t3388355125 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4444[2] = 
{
	Mesh_Extents_t3388355125::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Mesh_Extents_t3388355125::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4445 = { sizeof (WordWrapState_t341939652)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4445[55] = 
{
	WordWrapState_t341939652::get_offset_of_previous_WordBreak_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_total_CharacterCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_visible_CharacterCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_visible_SpriteCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_visible_LinkCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_firstCharacterIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_firstVisibleCharacterIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lastCharacterIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lastVisibleCharIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineNumber_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxCapHeight_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxAscender_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxDescender_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxLineAscender_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxLineDescender_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_previousLineAscender_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_xAdvance_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_preferredWidth_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_preferredHeight_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_previousLineScale_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_wordCount_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontStyle_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontScale_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontScaleMultiplier_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentFontSize_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_baselineOffset_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineOffset_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_textInfo_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineInfo_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_vertexColor_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_basicStyleStack_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_colorStack_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_underlineColorStack_35() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_strikethroughColorStack_36() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_highlightColorStack_37() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_colorGradientStack_38() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_sizeStack_39() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_indentStack_40() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontWeightStack_41() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_styleStack_42() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_baselineStack_43() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_actionStack_44() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_materialReferenceStack_45() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineJustificationStack_46() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_spriteAnimationID_47() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentFontAsset_48() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentSpriteAsset_49() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentMaterial_50() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentMaterialIndex_51() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_meshExtents_52() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_tagNoParsing_53() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_isNonBreakingSpace_54() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4446 = { sizeof (TagAttribute_t688278634)+ sizeof (RuntimeObject), sizeof(TagAttribute_t688278634 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4446[3] = 
{
	TagAttribute_t688278634::get_offset_of_startIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t688278634::get_offset_of_length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t688278634::get_offset_of_hashCode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4447 = { sizeof (XML_TagAttribute_t1174424309)+ sizeof (RuntimeObject), sizeof(XML_TagAttribute_t1174424309 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4447[5] = 
{
	XML_TagAttribute_t1174424309::get_offset_of_nameHashCode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueStartIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueHashCode_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4448 = { sizeof (ShaderUtilities_t714255158), -1, sizeof(ShaderUtilities_t714255158_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4448[60] = 
{
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MainTex_0(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceTex_1(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceColor_2(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceDilate_3(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_Shininess_4(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayColor_5(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayOffsetX_6(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayOffsetY_7(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayDilate_8(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlaySoftness_9(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_WeightNormal_10(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_WeightBold_11(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineTex_12(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineWidth_13(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineSoftness_14(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineColor_15(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_Padding_16(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GradientScale_17(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleX_18(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleY_19(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_PerspectiveFilter_20(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_TextureWidth_21(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_TextureHeight_22(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_BevelAmount_23(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowColor_24(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowOffset_25(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowPower_26(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowOuter_27(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_LightAngle_28(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMap_29(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMatrix_30(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMatrixRotation_31(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskCoord_32(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ClipRect_33(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskSoftnessX_34(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskSoftnessY_35(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_VertexOffsetX_36(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_VertexOffsetY_37(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UseClipRect_38(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilID_39(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilOp_40(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilComp_41(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilReadMask_42(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilWriteMask_43(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ShaderFlags_44(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_A_45(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_B_46(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_C_47(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Bevel_48(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Glow_49(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Underlay_50(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Ratios_51(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_SOFT_52(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_HARD_53(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_TEX_54(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Outline_55(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ShaderTag_ZTestMode_56(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ShaderTag_CullMode_57(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_m_clamp_58(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_isInitialized_59(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4449 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255369), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4449[2] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4450 = { sizeof (U24ArrayTypeU3D12_t2488454197)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454197 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4451 = { sizeof (U24ArrayTypeU3D40_t2865632059)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D40_t2865632059 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4452 = { sizeof (U3CModuleU3E_t692745563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4453 = { sizeof (BoundingBoxRenderer_t3223945734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4453[1] = 
{
	BoundingBoxRenderer_t3223945734::get_offset_of_mLineMaterial_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4454 = { sizeof (DefaultInitializationErrorHandler_t3109936861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4454[9] = 
{
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_mErrorText_4(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_mErrorOccurred_5(),
	0,
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_bodyStyle_7(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_headerStyle_8(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_footerStyle_9(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_bodyTexture_10(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_headerTexture_11(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_footerTexture_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4455 = { sizeof (DefaultModelRecoEventHandler_t1899894298), -1, sizeof(DefaultModelRecoEventHandler_t1899894298_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4455[11] = 
{
	DefaultModelRecoEventHandler_t1899894298::get_offset_of_mLastRecoModelTarget_4(),
	DefaultModelRecoEventHandler_t1899894298::get_offset_of_mSearching_5(),
	DefaultModelRecoEventHandler_t1899894298::get_offset_of_mLastStatusCheckTime_6(),
	DefaultModelRecoEventHandler_t1899894298::get_offset_of_mModelRecoBehaviour_7(),
	DefaultModelRecoEventHandler_t1899894298::get_offset_of_mTargetFinder_8(),
	DefaultModelRecoEventHandler_t1899894298::get_offset_of_ModelTargetTemplate_9(),
	DefaultModelRecoEventHandler_t1899894298::get_offset_of_ShowBoundingBox_10(),
	DefaultModelRecoEventHandler_t1899894298::get_offset_of_ModelRecoErrorText_11(),
	DefaultModelRecoEventHandler_t1899894298::get_offset_of_StopSearchWhenModelFound_12(),
	DefaultModelRecoEventHandler_t1899894298::get_offset_of_StopSearchWhileTracking_13(),
	DefaultModelRecoEventHandler_t1899894298_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4456 = { sizeof (DefaultTrackableEventHandler_t1588957063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4456[3] = 
{
	DefaultTrackableEventHandler_t1588957063::get_offset_of_mTrackableBehaviour_4(),
	DefaultTrackableEventHandler_t1588957063::get_offset_of_m_PreviousStatus_5(),
	DefaultTrackableEventHandler_t1588957063::get_offset_of_m_NewStatus_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4457 = { sizeof (U3CModuleU3E_t692745564), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4458 = { sizeof (SoftMask_t1817791576), -1, sizeof(SoftMask_t1817791576_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4458[27] = 
{
	SoftMask_t1817791576_StaticFields::get_offset_of_s_TmpSoftMasks_9(),
	SoftMask_t1817791576_StaticFields::get_offset_of_s_ClearColors_10(),
	SoftMask_t1817791576::get_offset_of_m_DesamplingRate_11(),
	SoftMask_t1817791576::get_offset_of_m_Softness_12(),
	SoftMask_t1817791576::get_offset_of_m_IgnoreParent_13(),
	SoftMask_t1817791576::get_offset_of_m_PartOfParent_14(),
	SoftMask_t1817791576_StaticFields::get_offset_of_s_SoftMaskShader_15(),
	SoftMask_t1817791576_StaticFields::get_offset_of_s_ReadTexture_16(),
	SoftMask_t1817791576_StaticFields::get_offset_of_s_ActiveSoftMasks_17(),
	SoftMask_t1817791576_StaticFields::get_offset_of_s_TempRelatables_18(),
	SoftMask_t1817791576_StaticFields::get_offset_of_s_StencilCompId_19(),
	SoftMask_t1817791576_StaticFields::get_offset_of_s_ColorMaskId_20(),
	SoftMask_t1817791576_StaticFields::get_offset_of_s_MainTexId_21(),
	SoftMask_t1817791576_StaticFields::get_offset_of_s_SoftnessId_22(),
	SoftMask_t1817791576::get_offset_of__mpb_23(),
	SoftMask_t1817791576::get_offset_of__cb_24(),
	SoftMask_t1817791576::get_offset_of__material_25(),
	SoftMask_t1817791576::get_offset_of__softMaskBuffer_26(),
	SoftMask_t1817791576::get_offset_of__stencilDepth_27(),
	SoftMask_t1817791576::get_offset_of__mesh_28(),
	SoftMask_t1817791576::get_offset_of__parent_29(),
	SoftMask_t1817791576::get_offset_of__children_30(),
	SoftMask_t1817791576::get_offset_of__hasChanged_31(),
	SoftMask_t1817791576::get_offset_of__hasStencilStateChanged_32(),
	SoftMask_t1817791576_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_33(),
	SoftMask_t1817791576_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_34(),
	SoftMask_t1817791576_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4459 = { sizeof (DesamplingRate_t2371977868)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4459[6] = 
{
	DesamplingRate_t2371977868::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4460 = { sizeof (SoftMaskable_t353105572), -1, sizeof(SoftMaskable_t353105572_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4460[16] = 
{
	0,
	0,
	SoftMaskable_t353105572::get_offset_of_m_Inverse_6(),
	SoftMaskable_t353105572::get_offset_of_m_MaskInteraction_7(),
	SoftMaskable_t353105572::get_offset_of_m_UseStencil_8(),
	SoftMaskable_t353105572::get_offset_of__graphic_9(),
	SoftMaskable_t353105572::get_offset_of__softMask_10(),
	SoftMaskable_t353105572::get_offset_of__maskMaterial_11(),
	SoftMaskable_t353105572_StaticFields::get_offset_of_s_SoftMaskTexId_12(),
	SoftMaskable_t353105572_StaticFields::get_offset_of_s_StencilCompId_13(),
	SoftMaskable_t353105572_StaticFields::get_offset_of_s_MaskInteractionId_14(),
	SoftMaskable_t353105572_StaticFields::get_offset_of_s_GameVPId_15(),
	SoftMaskable_t353105572_StaticFields::get_offset_of_s_GameTVPId_16(),
	SoftMaskable_t353105572_StaticFields::get_offset_of_s_ActiveSoftMaskables_17(),
	SoftMaskable_t353105572_StaticFields::get_offset_of_s_Interactions_18(),
	SoftMaskable_t353105572_StaticFields::get_offset_of_s_DefaultMaterial_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4461 = { sizeof (U3CModuleU3E_t692745565), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4462 = { sizeof (AspectRatioFitter_Mod001_t3563600902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4462[2] = 
{
	AspectRatioFitter_Mod001_t3563600902::get_offset_of_Horizontal_9(),
	AspectRatioFitter_Mod001_t3563600902::get_offset_of_Vertical_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4463 = { sizeof (AspectRatioFitter_Mod002_t1607285766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4463[2] = 
{
	AspectRatioFitter_Mod002_t1607285766::get_offset_of_Horizontal_9(),
	AspectRatioFitter_Mod002_t1607285766::get_offset_of_Vertical_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4464 = { sizeof (ContentScaler_t903206615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4464[1] = 
{
	ContentScaler_t903206615::get_offset_of_SetAlignment_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4465 = { sizeof (Alignment_t2118925705)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4465[5] = 
{
	Alignment_t2118925705::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4466 = { sizeof (FlexibleGridLayout_t617703811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4466[1] = 
{
	FlexibleGridLayout_t617703811::get_offset_of_TargetCellSize_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4467 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4467[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4468 = { sizeof (ScrollRect_Mod001_t3784143312), -1, sizeof(ScrollRect_Mod001_t3784143312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4468[5] = 
{
	ScrollRect_Mod001_t3784143312::get_offset_of_SetScrollAlignment_40(),
	ScrollRect_Mod001_t3784143312::get_offset_of_Outlines_41(),
	ScrollRect_Mod001_t3784143312::get_offset_of_SnapVector_42(),
	ScrollRect_Mod001_t3784143312::get_offset_of_IsDragging_43(),
	ScrollRect_Mod001_t3784143312_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4469 = { sizeof (ScrollAlignment_t1755443740)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4469[5] = 
{
	ScrollAlignment_t1755443740::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4470 = { sizeof (BottomSidePage_t4207383372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4470[4] = 
{
	BottomSidePage_t4207383372::get_offset_of_page_4(),
	BottomSidePage_t4207383372::get_offset_of_getActivate_5(),
	BottomSidePage_t4207383372::get_offset_of_speed_6(),
	BottomSidePage_t4207383372::get_offset_of_startTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4471 = { sizeof (GetPointer_t3074287824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4471[1] = 
{
	GetPointer_t3074287824::get_offset_of_pointerActivated_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4472 = { sizeof (InstantiateObject_t3169048492), -1, sizeof(InstantiateObject_t3169048492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4472[16] = 
{
	InstantiateObject_t3169048492::get_offset_of_anchorPlacements_4(),
	InstantiateObject_t3169048492::get_offset_of_modelHolder_5(),
	InstantiateObject_t3169048492::get_offset_of_loadingPanel_6(),
	InstantiateObject_t3169048492::get_offset_of_loadingBar_7(),
	InstantiateObject_t3169048492::get_offset_of_loadingProgress_8(),
	InstantiateObject_t3169048492::get_offset_of_TargetGoAR_9(),
	InstantiateObject_t3169048492::get_offset_of_TargetGoModel_10(),
	InstantiateObject_t3169048492::get_offset_of_TargetMats_11(),
	InstantiateObject_t3169048492::get_offset_of_planeManager_12(),
	InstantiateObject_t3169048492::get_offset_of_touchHandler_13(),
	InstantiateObject_t3169048492_StaticFields::get_offset_of_ObjectReference_14(),
	InstantiateObject_t3169048492::get_offset_of_LoadingReferenceTable_15(),
	InstantiateObject_t3169048492::get_offset_of_ConfigurationRequestURL_16(),
	InstantiateObject_t3169048492::get_offset_of_AssetsVersion_17(),
	InstantiateObject_t3169048492::get_offset_of_currentMatNum_18(),
	InstantiateObject_t3169048492::get_offset_of_SerializablePreviewPropertiesObject_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4473 = { sizeof (SerializablePreviewProperties_t1656375491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4473[1] = 
{
	SerializablePreviewProperties_t1656375491::get_offset_of_Version_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4474 = { sizeof (U3CLoadObjectU3Ec__Iterator0_t3760357558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4474[7] = 
{
	U3CLoadObjectU3Ec__Iterator0_t3760357558::get_offset_of_ObjectLink_0(),
	U3CLoadObjectU3Ec__Iterator0_t3760357558::get_offset_of_U3CObjectRequestU3E__0_1(),
	U3CLoadObjectU3Ec__Iterator0_t3760357558::get_offset_of_ARmode_2(),
	U3CLoadObjectU3Ec__Iterator0_t3760357558::get_offset_of_U24this_3(),
	U3CLoadObjectU3Ec__Iterator0_t3760357558::get_offset_of_U24current_4(),
	U3CLoadObjectU3Ec__Iterator0_t3760357558::get_offset_of_U24disposing_5(),
	U3CLoadObjectU3Ec__Iterator0_t3760357558::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4475 = { sizeof (U3CLoadVersionU3Ec__Iterator1_t1405279486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4475[5] = 
{
	U3CLoadVersionU3Ec__Iterator1_t1405279486::get_offset_of_U3CGetAssetConfigurationRequestU3E__0_0(),
	U3CLoadVersionU3Ec__Iterator1_t1405279486::get_offset_of_U24this_1(),
	U3CLoadVersionU3Ec__Iterator1_t1405279486::get_offset_of_U24current_2(),
	U3CLoadVersionU3Ec__Iterator1_t1405279486::get_offset_of_U24disposing_3(),
	U3CLoadVersionU3Ec__Iterator1_t1405279486::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4476 = { sizeof (InvokeButton_t2298937379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4476[1] = 
{
	InvokeButton_t2298937379::get_offset_of_button_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4477 = { sizeof (LeftSidePage_t1462537719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4477[4] = 
{
	LeftSidePage_t1462537719::get_offset_of_page_4(),
	LeftSidePage_t1462537719::get_offset_of_getActivate_5(),
	LeftSidePage_t1462537719::get_offset_of_speed_6(),
	LeftSidePage_t1462537719::get_offset_of_startTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4478 = { sizeof (MenuSidePage_t3698810816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4478[4] = 
{
	MenuSidePage_t3698810816::get_offset_of_page_4(),
	MenuSidePage_t3698810816::get_offset_of_getActivate_5(),
	MenuSidePage_t3698810816::get_offset_of_speed_6(),
	MenuSidePage_t3698810816::get_offset_of_startTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4479 = { sizeof (ModelMover_t1710746260), -1, sizeof(ModelMover_t1710746260_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4479[22] = 
{
	ModelMover_t1710746260::get_offset_of_VerticalAxisMoveable_4(),
	ModelMover_t1710746260::get_offset_of_enablePinchScaling_5(),
	ModelMover_t1710746260::get_offset_of_TargetRotationTransform_6(),
	ModelMover_t1710746260::get_offset_of_getPointer_7(),
	ModelMover_t1710746260_StaticFields::get_offset_of_Active_8(),
	ModelMover_t1710746260::get_offset_of_InputDetected_9(),
	ModelMover_t1710746260::get_offset_of_HorizontalMovementDetected_10(),
	ModelMover_t1710746260::get_offset_of_SensitivityThresholdPassed_11(),
	ModelMover_t1710746260::get_offset_of_rotY_12(),
	ModelMover_t1710746260::get_offset_of_VerticalAxisTransformer_13(),
	ModelMover_t1710746260::get_offset_of_LastInputPosition_14(),
	ModelMover_t1710746260::get_offset_of_HRotationVector_15(),
	ModelMover_t1710746260::get_offset_of_VRotationVector_16(),
	0,
	0,
	ModelMover_t1710746260::get_offset_of_touches_19(),
	ModelMover_t1710746260_StaticFields::get_offset_of_lastTouchCount_20(),
	ModelMover_t1710746260::get_offset_of_isFirstFrameWithTwoTouches_21(),
	ModelMover_t1710746260::get_offset_of_cachedTouchAngle_22(),
	ModelMover_t1710746260::get_offset_of_cachedTouchDistance_23(),
	ModelMover_t1710746260::get_offset_of_cachedAugmentationScale_24(),
	ModelMover_t1710746260::get_offset_of_cachedAugmentationRotation_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4480 = { sizeof (ObjectComponents_t1076493220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4480[8] = 
{
	ObjectComponents_t1076493220::get_offset_of_objectSelf_4(),
	ObjectComponents_t1076493220::get_offset_of_objectPivot_5(),
	ObjectComponents_t1076493220::get_offset_of_objectHolder_6(),
	ObjectComponents_t1076493220::get_offset_of_objectVisual_7(),
	ObjectComponents_t1076493220::get_offset_of_objectBoxCollider_8(),
	ObjectComponents_t1076493220::get_offset_of_objectRenderer_9(),
	ObjectComponents_t1076493220::get_offset_of_objectShadow_10(),
	ObjectComponents_t1076493220::get_offset_of_objectAnimator_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4481 = { sizeof (ProductPage_t3134713323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4481[16] = 
{
	ProductPage_t3134713323::get_offset_of_instantiateObject_4(),
	ProductPage_t3134713323::get_offset_of_getPointer_5(),
	ProductPage_t3134713323::get_offset_of_page_6(),
	ProductPage_t3134713323::get_offset_of_part2Page_7(),
	ProductPage_t3134713323::get_offset_of_threeDButton_8(),
	ProductPage_t3134713323::get_offset_of_partA_9(),
	ProductPage_t3134713323::get_offset_of_partB_10(),
	ProductPage_t3134713323::get_offset_of_getActivate_11(),
	ProductPage_t3134713323::get_offset_of_material1_12(),
	ProductPage_t3134713323::get_offset_of_returnPage_13(),
	ProductPage_t3134713323::get_offset_of_currentObjectName_14(),
	ProductPage_t3134713323::get_offset_of_mode_15(),
	ProductPage_t3134713323::get_offset_of_updateHeight_16(),
	ProductPage_t3134713323::get_offset_of_speed_17(),
	ProductPage_t3134713323::get_offset_of_startTime_18(),
	ProductPage_t3134713323::get_offset_of_offset_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4482 = { sizeof (U3CInvokeButtonU3Ec__Iterator0_t2072859715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4482[4] = 
{
	U3CInvokeButtonU3Ec__Iterator0_t2072859715::get_offset_of_U24this_0(),
	U3CInvokeButtonU3Ec__Iterator0_t2072859715::get_offset_of_U24current_1(),
	U3CInvokeButtonU3Ec__Iterator0_t2072859715::get_offset_of_U24disposing_2(),
	U3CInvokeButtonU3Ec__Iterator0_t2072859715::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4483 = { sizeof (RightSidePage_t131624286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4483[4] = 
{
	RightSidePage_t131624286::get_offset_of_page_4(),
	RightSidePage_t131624286::get_offset_of_getActivate_5(),
	RightSidePage_t131624286::get_offset_of_speed_6(),
	RightSidePage_t131624286::get_offset_of_startTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4484 = { sizeof (ScrollRectEx_t4149042633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4484[1] = 
{
	ScrollRectEx_t4149042633::get_offset_of_routeToParent_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4485 = { sizeof (U3COnInitializePotentialDragU3Ec__AnonStorey0_t250711124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4485[1] = 
{
	U3COnInitializePotentialDragU3Ec__AnonStorey0_t250711124::get_offset_of_eventData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4486 = { sizeof (U3COnDragU3Ec__AnonStorey1_t3609824325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4486[1] = 
{
	U3COnDragU3Ec__AnonStorey1_t3609824325::get_offset_of_eventData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4487 = { sizeof (U3COnBeginDragU3Ec__AnonStorey2_t3997952331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4487[1] = 
{
	U3COnBeginDragU3Ec__AnonStorey2_t3997952331::get_offset_of_eventData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4488 = { sizeof (U3COnEndDragU3Ec__AnonStorey3_t36866175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4488[1] = 
{
	U3COnEndDragU3Ec__AnonStorey3_t36866175::get_offset_of_eventData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4489 = { sizeof (ScrollSnapEx_t1558793980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4489[33] = 
{
	ScrollSnapEx_t1558793980::get_offset_of__scroll_rect_ex_4(),
	ScrollSnapEx_t1558793980::get_offset_of__scrollRectTransform_5(),
	ScrollSnapEx_t1558793980::get_offset_of__listContainerTransform_6(),
	ScrollSnapEx_t1558793980::get_offset_of__pages_7(),
	ScrollSnapEx_t1558793980::get_offset_of__startingPage_8(),
	ScrollSnapEx_t1558793980::get_offset_of__pageAnchorPositions_9(),
	ScrollSnapEx_t1558793980::get_offset_of__lerpTarget_10(),
	ScrollSnapEx_t1558793980::get_offset_of__lerp_11(),
	ScrollSnapEx_t1558793980::get_offset_of__listContainerMinPosition_12(),
	ScrollSnapEx_t1558793980::get_offset_of__listContainerMaxPosition_13(),
	ScrollSnapEx_t1558793980::get_offset_of__listContainerSize_14(),
	ScrollSnapEx_t1558793980::get_offset_of__listContainerRectTransform_15(),
	ScrollSnapEx_t1558793980::get_offset_of__listContainerCachedSize_16(),
	ScrollSnapEx_t1558793980::get_offset_of__itemSize_17(),
	ScrollSnapEx_t1558793980::get_offset_of__itemsCount_18(),
	ScrollSnapEx_t1558793980::get_offset_of__startDrag_19(),
	ScrollSnapEx_t1558793980::get_offset_of__positionOnDragStart_20(),
	ScrollSnapEx_t1558793980::get_offset_of__pageOnDragStart_21(),
	ScrollSnapEx_t1558793980::get_offset_of__fastSwipeTimer_22(),
	ScrollSnapEx_t1558793980::get_offset_of__fastSwipeCounter_23(),
	ScrollSnapEx_t1558793980::get_offset_of__fastSwipeTarget_24(),
	ScrollSnapEx_t1558793980::get_offset_of_NextButton_25(),
	ScrollSnapEx_t1558793980::get_offset_of_PrevButton_26(),
	ScrollSnapEx_t1558793980::get_offset_of_ItemsVisibleAtOnce_27(),
	ScrollSnapEx_t1558793980::get_offset_of_AutoLayoutItems_28(),
	ScrollSnapEx_t1558793980::get_offset_of_LinkScrolbarSteps_29(),
	ScrollSnapEx_t1558793980::get_offset_of_LinkScrolrectScrollSensitivity_30(),
	ScrollSnapEx_t1558793980::get_offset_of_UseFastSwipe_31(),
	ScrollSnapEx_t1558793980::get_offset_of_FastSwipeThreshold_32(),
	ScrollSnapEx_t1558793980::get_offset_of_onPageChange_33(),
	ScrollSnapEx_t1558793980::get_offset_of_direction_34(),
	ScrollSnapEx_t1558793980::get_offset_of_dotsPosition_35(),
	ScrollSnapEx_t1558793980::get_offset_of_fastSwipe_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4490 = { sizeof (ScrollDirection_t416655783)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4490[3] = 
{
	ScrollDirection_t416655783::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4491 = { sizeof (PageSnapChange_t2018612193), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4492 = { sizeof (SearchPage_t3430791159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4492[9] = 
{
	SearchPage_t3430791159::get_offset_of_page_4(),
	SearchPage_t3430791159::get_offset_of_inputField_5(),
	SearchPage_t3430791159::get_offset_of_brandContent_6(),
	SearchPage_t3430791159::get_offset_of_categoryContent_7(),
	SearchPage_t3430791159::get_offset_of_searching_8(),
	SearchPage_t3430791159::get_offset_of_getActivate_9(),
	SearchPage_t3430791159::get_offset_of_go_10(),
	SearchPage_t3430791159::get_offset_of_speed_11(),
	SearchPage_t3430791159::get_offset_of_startTime_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4493 = { sizeof (SortPage_t2404422720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4493[4] = 
{
	SortPage_t2404422720::get_offset_of_FilterPanel_4(),
	SortPage_t2404422720::get_offset_of_SortPanel_5(),
	SortPage_t2404422720::get_offset_of_toggles1_6(),
	SortPage_t2404422720::get_offset_of_toggles2_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4494 = { sizeof (SwipeDetector_t548578696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4494[7] = 
{
	SwipeDetector_t548578696::get_offset_of_getPointer_4(),
	SwipeDetector_t548578696::get_offset_of_productPage_5(),
	SwipeDetector_t548578696::get_offset_of_fingerDown_6(),
	SwipeDetector_t548578696::get_offset_of_fingerUp_7(),
	SwipeDetector_t548578696::get_offset_of_detectSwipeOnlyAfterRelease_8(),
	SwipeDetector_t548578696::get_offset_of_SWIPE_THRESHOLD_9(),
	SwipeDetector_t548578696::get_offset_of_doOnce_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4495 = { sizeof (UIController_t2237998930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4495[6] = 
{
	UIController_t2237998930::get_offset_of_SplashScreen_4(),
	UIController_t2237998930::get_offset_of_StartVideo_5(),
	UIController_t2237998930::get_offset_of_StartPanel_6(),
	UIController_t2237998930::get_offset_of_Back1_7(),
	UIController_t2237998930::get_offset_of_Back2_8(),
	UIController_t2237998930::get_offset_of_Panel1_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4496 = { sizeof (U3CStartSplashU3Ec__Iterator0_t1313349864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4496[4] = 
{
	U3CStartSplashU3Ec__Iterator0_t1313349864::get_offset_of_U24this_0(),
	U3CStartSplashU3Ec__Iterator0_t1313349864::get_offset_of_U24current_1(),
	U3CStartSplashU3Ec__Iterator0_t1313349864::get_offset_of_U24disposing_2(),
	U3CStartSplashU3Ec__Iterator0_t1313349864::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4497 = { sizeof (AboutManager_t2961629990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4497[1] = 
{
	AboutManager_t2961629990::get_offset_of_m_AboutScreenSample_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4498 = { sizeof (AboutScreenSample_t2468051143)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4498[15] = 
{
	AboutScreenSample_t2468051143::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4499 = { sizeof (AboutScreen_t2183797299), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
