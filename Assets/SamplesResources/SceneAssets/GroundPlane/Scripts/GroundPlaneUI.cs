﻿/*==============================================================================
Copyright (c) 2018 PTC Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other
countries.
==============================================================================*/
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Vuforia;
using System.Collections;

public class GroundPlaneUI : MonoBehaviour
{
    #region PRIVATE_MEMBERS

    [SerializeField] RectTransform productPanel;

    PlaneManager planeManager;
    ProductPlacement productPlacement;
    GraphicRaycaster graphicRayCaster;
    PointerEventData pointerEventData;
    EventSystem eventSystem;
    #endregion // PRIVATE_MEMBERS

    bool showMessage = false;
    bool notPlaced = true;

    [HideInInspector] public bool inVuforia = false;

    #region MONOBEHAVIOUR_METHODS
    void Start()
    {
        this.graphicRayCaster = FindObjectOfType<GraphicRaycaster>();
        this.eventSystem = FindObjectOfType<EventSystem>();
        this.planeManager = FindObjectOfType<PlaneManager>();
        this.productPlacement = FindObjectOfType<ProductPlacement>();

        DeviceTrackerARController.Instance.RegisterDevicePoseStatusChangedCallback(OnDevicePoseStatusChanged);

        TurnOffVuforia();
    }

    void Update()
    {
        if (showMessage)
        {
            StatusMessage.Instance.SetShowing(true);
        }
        else
        {
            StatusMessage.Instance.SetShowing(false);
            StatusMessage.Instance.Display("");
        }

        if (inVuforia)
        {
            if (this.productPlacement != null)
            {
                if (!productPlacement.IsPlaced)
                {
                    if (productPanel.localPosition.Equals(Vector2.zero))
                    {
                        showMessage = true;
                    }
                    else
                    {
                        showMessage = false;
                    }
                }
                else
                {
                    showMessage = false;
                }
            }
            else
            {
                showMessage = false;
            }
        }
        else
        {
            showMessage = false;
        }
    }

    IEnumerator OffVuforia()
    {
        while (VuforiaRuntime.Instance.InitializationState != VuforiaRuntime.InitState.INITIALIZED)
        {
            yield return null;
        }
        VuforiaBehaviour.Instance.enabled = false;
        inVuforia = false;
    }

    IEnumerator OnVuforia()
    {
        while (VuforiaRuntime.Instance.InitializationState != VuforiaRuntime.InitState.INITIALIZED)
        {
            yield return null;
        }
        VuforiaBehaviour.Instance.enabled = true;
        inVuforia = true;
    }

    public void TurnOffVuforia()
    {
        StartCoroutine(OffVuforia());
    }

    public void TurnOnVuforia()
    {
        StartCoroutine(OnVuforia());
    }

    void OnDestroy()
    {
        Debug.Log("OnDestroy() called.");

        DeviceTrackerARController.Instance.UnregisterDevicePoseStatusChangedCallback(OnDevicePoseStatusChanged);
    }
    #endregion // MONOBEHAVIOUR_METHODS


    #region PUBLIC_METHODS
    /// <summary>
    /// Resets the UI Buttons and the Initialized property.
    /// It is called by PlaneManager.ResetScene().
    /// </summary>

    public void ShowHideMessage(bool show)
    {
        notPlaced = show;
        showMessage = show;
    }

    public void Vibrate()
    {
        Handheld.Vibrate();
    }

    public void Reset()
    {

    }

    public bool IsCanvasButtonPressed()
    {
        pointerEventData = new PointerEventData(this.eventSystem)
        {
            position = Input.mousePosition
        };
        List<RaycastResult> results = new List<RaycastResult>();
        this.graphicRayCaster.Raycast(pointerEventData, results);

        bool resultIsButton = false;
        foreach (RaycastResult result in results)
        {
            if (result.gameObject.GetComponentInParent<Toggle>() ||
                result.gameObject.GetComponent<Button>())
            {
                resultIsButton = true;
                break;
            }
        }
        return resultIsButton;
    }
    #endregion // PUBLIC_METHODS

    #region VUFORIA_CALLBACKS

    void OnDevicePoseStatusChanged(TrackableBehaviour.Status status, TrackableBehaviour.StatusInfo statusInfo)
    {
        Debug.Log("GroundPlaneUI.OnDevicePoseStatusChanged(" + status + ", " + statusInfo + ")");

        string statusMessage = "";

        switch (statusInfo)
        {
            case TrackableBehaviour.StatusInfo.NORMAL:
                statusMessage = "Tap on Screen to Place Object";
                break;
            case TrackableBehaviour.StatusInfo.UNKNOWN:
                statusMessage = "Initializing Tracker - Scan For Surface";
                break;
            case TrackableBehaviour.StatusInfo.INITIALIZING:
                statusMessage = "Initializing Tracker - Scan For Surface";
                break;
            case TrackableBehaviour.StatusInfo.EXCESSIVE_MOTION:
                statusMessage = "Move slower";
                break;
            case TrackableBehaviour.StatusInfo.INSUFFICIENT_FEATURES:
                statusMessage = "Not enough visual features in the scene";
                break;
            case TrackableBehaviour.StatusInfo.RELOCALIZING:
                statusMessage = "Point camera to previous position to restore tracking";
                break;
            default:
                statusMessage = "";
                break;
        }

        StatusMessage.Instance.Display(statusMessage);
        // Uncomment the following line to show Status and StatusInfo values
        //StatusMessage.Instance.Display(status.ToString() + " -- " + statusInfo.ToString());
    }

    #endregion // VUFORIA_CALLBACKS
}
