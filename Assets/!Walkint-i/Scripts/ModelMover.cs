﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelMover : MonoBehaviour
{
    [SerializeField] bool VerticalAxisMoveable = false;
    [SerializeField] bool enablePinchScaling = false;
    [SerializeField] Transform TargetRotationTransform;
    public GetPointer getPointer;

    public static bool Active = true;
    bool InputDetected = false;
    bool HorizontalMovementDetected = false;
    bool SensitivityThresholdPassed = false;

    private float rotY;

    Transform VerticalAxisTransformer = null;
    Vector3 LastInputPosition = Vector3.zero;
    Vector3 HRotationVector = Vector3.zero;
    Vector3 VRotationVector = Vector3.zero;

    const float ScaleRangeMin = 1.0f;
    const float ScaleRangeMax = 2.5f;

    Touch[] touches;
    static int lastTouchCount;
    bool isFirstFrameWithTwoTouches;
    float cachedTouchAngle;
    float cachedTouchDistance;
    float cachedAugmentationScale;
    Vector3 cachedAugmentationRotation;

    public void ResetScale()
    {
        this.transform.localScale = Vector3.one;
    }

    void Start()
    {
        if (VerticalAxisMoveable)
        {
            VerticalAxisTransformer = transform.Find("VerticalMover");
        }

        this.cachedAugmentationScale = this.transform.localScale.x;
    }

    void Update()
    {
        this.transform.localPosition = Vector3.zero;

#if UNITY_EDITOR
        if (Input.GetMouseButton(0) && ModelMover.Active)
        {
            if (Mathf.Abs(Input.GetAxis("Mouse X")) > 0.1f || Mathf.Abs(Input.GetAxis("Mouse Y")) > 0.1f)
            {
                if (Mathf.Abs(Input.GetAxis("Mouse X")) > Mathf.Abs(Input.GetAxis("Mouse Y")))
                {
                    rotY = -Input.GetAxis("Mouse X") * 5.0f;
                    HorizontalMovementDetected = true;
                }
                else
                {
                    rotY = -Input.GetAxis("Mouse Y") * 5.0f;
                    HorizontalMovementDetected = false;
                }
                SensitivityThresholdPassed = true;
            }
            else
            {
                SensitivityThresholdPassed = false;
            }
            InputDetected = true;
        }
        else
        {
            InputDetected = false;
        }
#else
		if (Input.touchCount > 0) {
			var input = Input.GetTouch (0);

			if ((input.phase == TouchPhase.Moved || input.phase == TouchPhase.Stationary) && ModelMover.Active) {
				if (Mathf.Abs (input.deltaPosition.magnitude) > 0.1f) {
					if (Mathf.Abs (input.deltaPosition.x) > Mathf.Abs (input.deltaPosition.y)) {
						rotY = -input.deltaPosition.x * Time.deltaTime * 5.0f;
						HorizontalMovementDetected = true;
					} else {
						rotY = -input.deltaPosition.y * Time.deltaTime * 5.0f;
						HorizontalMovementDetected = false;
					}
					SensitivityThresholdPassed = true;
				} else {
					SensitivityThresholdPassed = false;
				}
				InputDetected = true;
			} else {
				InputDetected = false;
			}
		} else {
			InputDetected = false;
		}
#endif

        this.touches = Input.touches;

        if (Input.touchCount == 2)
        {
            float currentTouchDistance = Vector2.Distance(this.touches[0].position, this.touches[1].position);
            float diff_y = this.touches[0].position.y - this.touches[1].position.y;
            float diff_x = this.touches[0].position.x - this.touches[1].position.x;
            float currentTouchAngle = Mathf.Atan2(diff_y, diff_x) * Mathf.Rad2Deg;

            if (this.isFirstFrameWithTwoTouches)
            {
                this.cachedTouchDistance = currentTouchDistance;
                this.cachedTouchAngle = currentTouchAngle;
                this.isFirstFrameWithTwoTouches = false;
            }

            float angleDelta = currentTouchAngle - this.cachedTouchAngle;
            float scaleMultiplier = (currentTouchDistance / this.cachedTouchDistance);
            float scaleAmount = this.cachedAugmentationScale * scaleMultiplier;
            float scaleAmountClamped = Mathf.Clamp(scaleAmount, ScaleRangeMin, ScaleRangeMax);

            if (this.enablePinchScaling)
            {
                // Optional Pinch Scaling can be enabled via Inspector for this Script Component
                this.transform.localScale = new Vector3(scaleAmountClamped, scaleAmountClamped, scaleAmountClamped);
            }

        }
        else if (Input.touchCount < 2)
        {
            this.cachedAugmentationScale = this.transform.localScale.x;
            this.cachedAugmentationRotation = this.transform.localEulerAngles;
            this.isFirstFrameWithTwoTouches = true;
        }

        if (InputDetected)
        {
            if (getPointer.GetBoolean())
            {
                if (SensitivityThresholdPassed)
                {
                    if (HorizontalMovementDetected || !VerticalAxisMoveable)
                    {
                        HRotationVector = Vector3.up * rotY;
                        TargetRotationTransform.Rotate(HRotationVector);
                        this.transform.rotation = Quaternion.Lerp(this.transform.rotation, TargetRotationTransform.rotation, 5f * Time.deltaTime);
                    }
                    else
                    {
                        VRotationVector = Vector3.left * rotY;
                        VerticalAxisTransformer.Rotate(VRotationVector, Space.World);
                    }
                }
            }
        }
        else
        {
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, TargetRotationTransform.rotation, 5f * Time.deltaTime);
            VerticalAxisTransformer.localRotation = Quaternion.Lerp(VerticalAxisTransformer.localRotation, Quaternion.identity, 0.2f);
            //this.transform.Rotate (Vector3.up * -Time.deltaTime * 8.0f);
        }
    }
}
