﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ProductPage : MonoBehaviour
{
    InstantiateObject instantiateObject;

    public GetPointer getPointer;
    public GameObject page;
    public GameObject part2Page;
    public GameObject threeDButton;
    public GameObject partA;
    public GameObject partB;

    bool getActivate = true;

    public GameObject material1;
    GameObject returnPage = null;
    string currentObjectName = "";
    string mode = "";

    float updateHeight = 0;

    // Movement speed in units/sec.
    float speed = 3600f;

    // Time when the movement started.
    private float startTime;

    private void Start()
    {
        instantiateObject = FindObjectOfType<InstantiateObject>();

        page.transform.localPosition = Vector2.zero;

        partA.GetComponent<LayoutElement>().preferredHeight = partA.GetComponentInParent<Canvas>().gameObject.GetComponent<RectTransform>().sizeDelta.y - 288.0f;
        updateHeight = partA.GetComponent<LayoutElement>().preferredHeight;

        partB.GetComponent<LayoutElement>().preferredHeight = partA.GetComponentInParent<Canvas>().gameObject.GetComponent<RectTransform>().sizeDelta.y;
    }

    IEnumerator InvokeButton()
    {
        yield return new WaitForSeconds(0.5f);
        if (threeDButton.GetComponent<Button>() != null && threeDButton.GetComponent<Button>().onClick != null)
            threeDButton.GetComponent<Button>().onClick.Invoke();
    }

    private void Update()
    {
        // Distance moved = time * speed.
        float distCovered = (Time.time - startTime) * speed;

        // Fraction of journey completed = current distance divided by total distance.
        float fracJourney = distCovered / 1080f;

        // Set our position as a fraction of the distance between the markers.
        if (!getActivate)
        {
            if (!page.transform.localPosition.Equals(new Vector2(0, updateHeight)))
            {
                page.transform.localPosition = Vector2.Lerp(Vector2.zero, new Vector2(0, updateHeight), fracJourney);
            }
            //part2Page.SetActive(true);

            if (threeDButton.activeInHierarchy)
            {
                StartCoroutine(InvokeButton());
            }
        }
        else
        {
            if (!page.transform.localPosition.Equals(Vector2.zero))
            {
                page.transform.localPosition = Vector2.Lerp(new Vector2(0, updateHeight), Vector2.zero, fracJourney);
            }
            //part2Page.SetActive(false);
        }

#if UNITY_EDITOR
        if (getPointer.GetBoolean())
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (getActivate)
                {
                    GetPage(false);
                }
                else
                {
                    GetPage(true);
                }
            }
        }
#endif

        if (mode.Equals("sofa"))
        {
            material1.SetActive(true);
        }
        else
        {
            material1.SetActive(false);
        }

        ChangeMatSize(instantiateObject.GetMatNum());
    }

    float offset = 0;

    public bool GetmenuActivate()
    {
        return getActivate;
    }

    public void GetPage(bool activate)
    {
        getActivate = activate;
        startTime = Time.time;
    }

    public void SetReturnPage(GameObject gameObject)
    {
        returnPage = gameObject;
    }

    public void GetReturnPage()
    {
        if (returnPage.GetComponent<RightSidePage>() != null)
        {
            returnPage.GetComponent<RightSidePage>().GetPage(true);
        }
    }

    public void ChangeMatSize(int num)
    {
        foreach (Image images in material1.GetComponentsInChildren<Image>())
        {
            if (images.gameObject.name.Equals("Image" + num.ToString()))
            {
                images.rectTransform.sizeDelta = new Vector2(80, 80);
            }
            else
            {
                images.rectTransform.sizeDelta = new Vector2(60, 60);
            }
        }
    }

    public void ResetMatSize()
    {
        foreach (Image images in material1.GetComponentsInChildren<Image>())
        {
            if (images.gameObject.name.Equals("Image1"))
            {
                images.rectTransform.sizeDelta = new Vector2(80, 80);
            }
            else
            {
                images.rectTransform.sizeDelta = new Vector2(60, 60);
            }
        }

        instantiateObject.SetMatNum(1);
    }

    public void SpawnObject(string objectName)
    {
        switch (objectName)
        {
            case "sofa":
                currentObjectName = "http://galaxybattles.deekie.com/walkint-i/ios/objects/sofa.assetbundle";
                mode = objectName;
                break;
            case "chair":
                currentObjectName = "http://galaxybattles.deekie.com/walkint-i/ios/objects/chair2.assetbundle";
                mode = "chair2";
                break;
            case "table":
                currentObjectName = "http://galaxybattles.deekie.com/walkint-i/ios/objects/chair1.assetbundle";
                mode = "chair1";
                break;
        }
        instantiateObject.InstantiateModel(currentObjectName);
    }

    public void ReSpawnObject()
    {
        instantiateObject.InstantiateModel(currentObjectName);
    }

    public void SpawnAR()
    {
        instantiateObject.InstantiateAR(currentObjectName);
    }
}
