﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SortPage : MonoBehaviour
{
    [SerializeField] GameObject FilterPanel;
    [SerializeField] GameObject SortPanel;

    Toggle[] toggles1;
    Toggle[] toggles2;

    private void Start()
    {
        toggles1 = FilterPanel.GetComponentsInChildren<Toggle>();
        toggles2 = SortPanel.GetComponentsInChildren<Toggle>();
    }

    public void SetAllToggleOff()
    {
        foreach (Toggle toggle1 in toggles1)
        {
            toggle1.isOn = false;
        }

        foreach (Toggle toggle2 in toggles2)
        {
            toggle2.isOn = false;
        }
    }
}
