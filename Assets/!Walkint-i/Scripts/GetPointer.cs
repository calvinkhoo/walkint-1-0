﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GetPointer : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    bool pointerActivated = false;

    public bool GetBoolean()
    {
        return pointerActivated;
    }

    public void ChangeBoolean(bool boolean)
    {
        pointerActivated = boolean;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        pointerActivated = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        pointerActivated = false;
    }
}
