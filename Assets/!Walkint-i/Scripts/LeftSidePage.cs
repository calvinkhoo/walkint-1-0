﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftSidePage : MonoBehaviour
{
    public GameObject page;
    bool getActivate = false;

    // Movement speed in units/sec.
    float speed = 7560f;

    // Time when the movement started.
    private float startTime;

    public bool GetmenuActivate()
    {
        return getActivate;
    }

    public void GetPage(bool activate)
    {
        getActivate = activate;
        startTime = Time.time;
    }

    private void Start()
    {
        page.transform.localPosition = new Vector2(1080f, 0);
    }

    private void Update()
    {
        // Distance moved = time * speed.
        float distCovered = (Time.time - startTime) * speed;

        // Fraction of journey completed = current distance divided by total distance.
        float fracJourney = distCovered / 1080f;

        // Set our position as a fraction of the distance between the markers.
        if (!getActivate)
        {
            if (!page.transform.localPosition.Equals(new Vector2(1080f, 0)))
            {
                page.transform.localPosition = Vector2.Lerp(Vector2.zero, new Vector2(1080f, 0), fracJourney);
            }
        }
        else
        {
            if (!page.transform.localPosition.Equals(Vector2.zero))
            {
                page.transform.localPosition = Vector2.Lerp(new Vector2(1080f, 0), Vector2.zero, fracJourney);
            }
        }
    }
}
